<?php

/*
|--------------------------------------------------------------------------
| My Learning Routes
|--------------------------------------------------------------------------
|
| Here is where you can register  My Learning routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('assignment-data','MylearningController@assignmentDataAction');

Route::post('requirement-data', 'RequirementController@requirementDataAction');

Route::post('myenrollments-data','MylearningController@myenrollmentsDataAction');
//Assessment data
Route::post('learnassessments-data','MylearningController@learnassessmentsDataAction');

Route::post('submittraining','MylearningController@submittrainingAction');

Route::post('deletemyenrollprogram/id/{itemId}','MylearningController@deletemyenrollprogramAction');

//Learning Plan Data
Route::post('learning-data','MylearningController@learningDataAction');
//View Assessment  
Route::post('viewassessmentdata','SurveyController@viewassessmentAction');
Route::get('viewquizandsurvey/viewId/{viewId}','SurveyController@viewQuizSurvey');
Route::get('viewlistquizandsurvey/viewId/{viewId}','SurveyController@viewListAssessment');
Route::get('viewassessmentretake/viewId/{viewId}','SurveyController@viewListAssessmentRetake');
Route::get('viewlistresultlssessment/viewId/{viewId}','SurveyController@viewListResultAssessment');

Route::post('enrollelearning','SurveyController@enrollelearning');

//Submit Assessment
Route::post('submitassessment','SurveyController@submitassessmentAction');

Route::post('submitlistassessment','SurveyController@submitListAssessmentAction');
//Review Assessment
Route::post('reviewassessment','SurveyController@reviewassessmentAction');
//Training Catalog Data
Route::post('trainingcatolg-data','TrainingcatalogController@trainingcatolgdataarrAction');
//Training Catalog Filter List
Route::post('trainingcatalog-filterlist','TrainingcatalogController@trainingcatalogFilterlist');
//Training Catalog Filter 
Route::post('categoryfilter','TrainingcatalogController@categoryfilterAction');
//Training Catalog Advance Filter 
Route::post('advanceFilter','TrainingcatalogController@trainingCatalogAdvanceFilter');
//My Requirements
Route::post('myrequirements','RequirementController@myrequirements');





	
