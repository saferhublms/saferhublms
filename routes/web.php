<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/administrators', function () {
  return view('admin.index');
})->middleware('admin');

Route::get('/', function () {
    return view('user.index');
});



Route::post('api/updateMultipleTrainingStatus','Administrator\ElearningController@updateMultipleTrainingStatusAction');
Route::post('api/updateTrainingStatus','Administrator\ElearningController@updateTrainingStatusAction');
Route::post('api/login', 'LoginController@login');
Route::post('api/getAppMenus', 'MenuController@getAppMenus');
Route::post('api/getAppNav', 'MenuController@appsheadernav');
Route::post('api/getLeftNav', 'MenuController@leftnav');
Route::post('api/getFooterNav', 'MenuController@footernav');
Route::post('api/companyCmsInfo', 'MenuController@companyCmsInfo');
Route::post('api/transcript-data','TranscriptController@transcriptData');
Route::post('api/requirement-data', 'RequirementController@requirementDataAction');
Route::post('api/viewElearningTraning', 'ElearningmediaController@viewtrainingdetail');
Route::post('api/enrollelearning', 'ElearningmediaController@enrollelearning');
Route::post('api/mediaplayer', 'ElearningmediaController@mediaplayer');
Route::post('api/classroomtraining', 'ClassroomcourseController@classroomtrainingview');
Route::get('elearningmedia/mediaplayer/viewId/{viewId}', 'ElearningmediaController@mediaplayer');

Route::get('elearningmedia/embeddedplayer/viewId/{viewId}', 'ElearningmediaController@mediaplayer');
Route::get('elearningmedia/iframe-media/compid/{compid}/filesname/{filesname}/ext/{ext}', 'ElearningmediaController@iframemedia');
Route::get('elearningmedia/iframeplayer/viewId/{viewId}', 'ElearningmediaController@linkplayer');
Route::get('elearningmedia/scormplayer/param/{param}/viewId/{viewId}', 'ElearningmediaController@scormplayer');
Route::get('elearningmedia/loadscorm/param/{param}', 'ElearningmediaController@loadscorm');
 Route::post('elearningmedia/checkloginstatus/reqdata/{param}', 'ElearningmediaController@checkloginstatus');
//Route::get('logout','LoginController@logout');
Route::get('logout', 'Auth\LoginController@logout');


Route::post('api/inprogress-data','InProgressController@inprogressDataAction');	
Route::post('api/deleteelearningprogress','InProgressController@deleteelearningprogressAction');	
Route::post('api/mygrid-blended','TrainingcatalogController@mygridBlendedAction');
	
Route::post('elearningmedia/datamodel','ElearningmediaController@datamodel');	
Route::post('api/delete-blended','InProgressController@deletemyblendedprogramAction');	
Route::post('api/drop-classroom','ClassroomcourseController@confirmenrollAction');	
Route::post('api/supervisor-reports','SupervisiorController@supervisorReportsAction');	
//Route::get('elearningmedia/iframe-media', 'ElearningmediaController@iframemedia');

Route::get('/survey/viewassessment/user/{user}/token/{token}/red_page/learnassessments','SurveyController@viewassessmentAction');
Route::get('/survey/successfullsubmitasse/userid/{user}/token/{token}','SurveyController@successfullsubmitasseAction');
Route::get('elearningmedia/aiccplayer/param/{param}/viewId/{viewId}', 'ElearningmediaController@aiccplayer');
Route::get('elearningmedia/loadaicc/param/{param}', 'ElearningmediaController@loadaicc');

Route::post('classroomcourse/classroaster', 'ClassroomcourseController@classroaster');
Route::post('classroomcourse/classhandouts', 'ClassroomcourseController@classhandouts');
Route::post('classroomcourse/curriculams', 'ClassroomcourseController@curriculams');

Route::post('api/supervisor/trainingcatolgdataarr', 'SupervisiorController@trainingcatolgdataarrAction');
Route::post('/api/supervisor/categoryfillter', 'SupervisiorController@categoryfilterAction');
Route::post('classroomcourse/confirmenroll','ClassroomcourseController@confirmenrollAction');	
Route::post('/api/supervisor/assigntrainingtousers', 'SupervisiorController@assigntrainingtousersAction');
Route::post('/api/supervisor/supervisorlearningplan', 'SupervisiorController@supervisorlearningplanAction');
Route::post('classroomcourse/trainingrequestpopup', 'ClassroomcourseController@trainingrequestpopupAction');
Route::post('mylearning/get-transcript-history', 'TranscriptController@gettranscripthistory');
Route::get('mylearning/certificatedownload/id/{id}', 'TranscriptController@certificatedownload');
Route::post('index/getaboutus', 'MenuController@aboutAction');
Route::post('index/getannouncement', 'MenuController@getannouncement');
Route::post('index/helpAction', 'HelpController@helpAction');
Route::post('index/topicdetails', 'HelpController@topicdetails');
Route::post('index/allreferences', 'HelpController@allreferencesAction');
Route::get('index/mediaplayer/file_name/{file_name}', 'HelpController@mediaplayer');
Route::get('index/embeddedplayer/file_name/{file_name}', 'HelpController@embeddedplayer');
Route::post('index/getreference', 'HelpController@viewReferenceAction');
Route::get('elearningmedia/confirmation/viewId/{viewId}', 'ElearningmediaController@confirmation');
Route::post('elearningmedia/programinpercentage','ElearningmediaController@programinpercentageAction');
Route::post('elearningmedia/updatelearningcompletion','ElearningmediaController@updatelearningcompletion');
Route::post('supervisor/givecredit','SupervisiorController@givecreditAction');
Route::post('coursescredit/manuallyadditem','SupervisiorController@manuallyadditemAction');
Route::post('supervisor/givecreditforselected','SupervisiorController@givecreditforselectedAction');
Route::post('supervisor/addexistingitems','SupervisiorController@addexistingitemsAction');

/* supervisor/givecredit/checkeduser/1 */
Route::get('api/getDateFormate','LoginController@getdateformate');
//Sub Admin 
Route::post('/managesubadmindata','Administrator\SubadminController@getsubadmindataarrAction');
Route::post('/assignedGroupAction','Administrator\SubadminController@assignedGroupAction');
Route::post('/assignedCategoryAction','Administrator\SubadminController@assignedCategoryAction');
Route::post('/saveAssignedGroupAction','Administrator\SubadminController@saveAssignedGroupAction');
Route::post('/saveAssignedCategoryAction','Administrator\SubadminController@saveAssignedCategoryAction');


Route::get('api/getpagewithlable','Administrator\LabelController@getlabels');
Route::post('api/saveLabel','Administrator\LabelController@saveLabel');
