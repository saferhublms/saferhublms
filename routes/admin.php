<?php


//Training Type
Route::get('gettrainingtypeplusmediatype','Administrator\CommonController@gettrainingType');
Route::post('getAudioVideoPageData','Administrator\CommonController@getAudioVideoPageData');
//

Route::post('traininglibrary/view-library-name','Administrator\TraininglibraryController@viewLibraryNameAction');

//Super Admin Release
Route::post('surealease/allreleases','Administrator\SureleaseController@allreleasesAction');
Route::post('surealease/addeditreleasenotes','Administrator\SureleaseController@addeditreleasenotesAction');
Route::post('surealease/releasenotevalbyid','Administrator\SureleaseController@releasenotevalbyidAction');
Route::post('surealease/publish','Administrator\SureleaseController@publishAction');
Route::post('surealease/deleterelease','Administrator\SureleaseController@deletereleaseAction');
Route::post('/getAddUserViewData','Administrator\AdminController@adduserview');
Route::post('/adduser','Administrator\AdminController@addnewuser');
Route::post('/manageuserlist','Administrator\AdminController@manageuserlist');
Route::get('/getuserfieldname','Administrator\AdminController@getuserfieldname');
Route::post('/getedituserdetail','Administrator\AdminController@getedituserdetail');
Route::post('/updateuserdetail','Administrator\AdminController@updateuserdata');

//admin group 
Route::post('/managegroupsnew','Administrator\AdmingroupController@managegroupsnew');
Route::post('/getuserpop/{groupid}','Administrator\AdmingroupController@getuserpop');
Route::post('/addgroup','Administrator\AdmingroupController@addgroup');
Route::post('/editgroupdata/{groupid}','Administrator\AdmingroupController@editgroupdata');
Route::post('/editgrouppop','Administrator\AdmingroupController@editgrouppop');
Route::post('/automatedgroups/{operation}/{notchecked?}','Administrator\AdmingroupController@automatedgroups');
Route::post('/deletegroup','Administrator\AdmingroupController@deletegroup');
Route::post('/bulkoptgroup','Administrator\AdmingroupController@bulkoptgroup');
Route::post('/deleteusergroup','Administrator\AdmingroupController@deleteusergroup');
Route::post('/addusertoselectedgroups','Administrator\AdmingroupController@addusertoselectedgroups');
// Notification 
Route::post('notificationmanagements/allnotificationmgts','Administrator\NotificationmanagementsController@allnotificationmgtsAction');

Route::post('notificationmanagements/shownotification','Administrator\NotificationmanagementsController@shownotificationAction');
Route::post('notificationmanagements/edit','Administrator\NotificationmanagementsController@editAction');
Route::post('notificationmanagements/updatenotification','Administrator\NotificationmanagementsController@updatenotificationAction');


//System management
Route::post('systemmanagement/systemsetting','Administrator\SystemmanagementController@getSystemsetting');
Route::post('systemmanagement/systemmgt','Administrator\SystemmanagementController@systemmgtAction');
Route::post('systemmanagement/passwordsetting','Administrator\SystemmanagementController@passwordsettingAction');
Route::post('systemmanagement/myrequirementsetting','Administrator\SystemmanagementController@myrequirementsettingAction');
Route::post('systemmanagement/subadminpermissionsetting','Administrator\SystemmanagementController@subadminpermissionsettingAction');

//Reporting 
Route::post('reports/schedulereport','Administrator\ReportsController@schedulereportAction');

//Elearning Course Add
Route::post('generatereferancecode','Administrator\ElearningController@getReferenceCode');
Route::post('saveFirstCourseData','Administrator\ElearningController@saveFirstCourseData');
Route::post('uploadfile','Administrator\ElearningController@uploadfileAction');
Route::post('uploadHandout','Administrator\ElearningController@uploadHandoutAction');
Route::post('deletefile','Administrator\ElearningController@deletefileAction');
Route::post('saveCourse','Administrator\ElearningController@saveCourseAction');
Route::post('saveembededcode','Administrator\ElearningController@saveembededcode');
Route::post('getelearningdata','Administrator\ElearningController@getelearningdataAction');
Route::post('editCourse','Administrator\ElearningController@editCourseAction');
Route::post('deletecourse','Administrator\ElearningController@deletecourseAction');
//Elearning Course List
Route::post('elearning-courses/elearningList','Administrator\ElearningController@elearningListAction');
Route::post('removeHandout','Administrator\ElearningController@removeHandoutAction');
//Preview Data
Route::post('getpreviewdata','Administrator\ElearningController@getpreviewdataAction');
  
//ClassRoom Course  
Route::post('getClassRoomCourseData','Administrator\ClassroomController@getClassRoomCourseData');
Route::post('saveClassroomCourse','Administrator\ClassroomController@saveClassroomCourseAction');
Route::post('getClassroomdata','Administrator\ClassroomController@getClassroomdataAction');
Route::post('editClassroomCourse','Administrator\ClassroomController@editClassroomCourseAction');
Route::post('getAddClassdata','Administrator\ClassroomController@getAddClassdataAction');
Route::post('addCourseClass','Administrator\ClassroomController@addCourseClassAction');
Route::post('getcourseClasses','Administrator\ClassroomController@getcourseClassesAction');
Route::post('getClassData','Administrator\ClassroomController@getClassDataAction');
Route::post('deleteSchedule','Administrator\ClassroomController@deleteScheduleAction');
Route::post('editCourseClass','Administrator\ClassroomController@editCourseClassAction');

//Assessment 
Route::post('saveAssessment','Administrator\AssessmentController@saveAssessmentAction');
Route::post('getAssessmentData','Administrator\AssessmentController@getAssessmentDataAction');
Route::post('editAssessment','Administrator\AssessmentController@editAssessmentAction');

//Course Distribution
Route::post('getcoursedistributionDataAction','Administrator\CourseDistributionController@coursedistributionData');
Route::post('assignCourseToUserAndGroup','Administrator\CourseDistributionController@coursedistribution');
Route::post('getAssignedUsers','Administrator\CourseDistributionController@getAssignedUsers');
Route::post('getAssignedGroups','Administrator\CourseDistributionController@getAssignedGroups');
Route::post('unassignuser','Administrator\CourseDistributionController@unassignuserAction');
Route::post('unassigngroup','Administrator\CourseDistributionController@unassigngroupAction');

//Notifications
Route::post('notificationtemplate/getnotifications','Administrator\NotificationmanagementsController@getnotifications');
Route::post('notificationtemplate/updatestatus','Administrator\NotificationmanagementsController@updatestatusAction');
Route::post('notificationtemplate/resoredefault','Administrator\NotificationmanagementsController@resoredefaultAction');

