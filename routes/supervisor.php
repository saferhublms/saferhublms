<?php



Route::get('getSupervisorSetting','SupervisiorController@getSupervisorSetting');
Route::post('supervisortools','SupervisiorController@supervisortoolsAction');
Route::post('pendingpop1','SupervisiorController@pendingpop1Action');
Route::post('pendingpop','SupervisiorController@pendingpopAction');
Route::post('get-all-class','SupervisiorController@getAllClassAction');
Route::post('enroll-user-to-class','SupervisiorController@enrollUserToClassAction');
Route::post('userpopup','SupervisiorController@userpopupAction');
Route::post('getassignedgroups','SupervisiorController@getassignedgroupsAction');