<?php
	date_default_timezone_set('America/Chicago');
	//Log default
	function writelog($text){
		$myfile = fopen("../temp/aicc_logs.log", "a") or die("Unable to open file!");
		fwrite($myfile, $text);
		fclose($myfile);
	}
	
	$sequence_array = array();
	$correctAnsCount = 0;
	$incorrectAnsCount = 0;

	$command = $_REQUEST['command'];
	$session_id = $_REQUEST['session_id'];

	if(isset($_REQUEST['aicc_data'])){
		$aiccdata = $_REQUEST['aicc_data'];
	}else  if(isset($_REQUEST['AICC_Data'])){
		$aiccdata = $_REQUEST['AICC_Data'];
	}

	$scormsession =  $userdata = new stdClass;//$_SESSION->scorm ;

	$strvariables 	= base64_decode($session_id);
	$variablearray = explode('_#_', $strvariables); 
	
	$text = "\n------------\nRequest Log Time ".date('d-m-Y H:i:s')."\n------------\n";
	$text .= "Requested Data: ".print_r($_REQUEST,true)."\n";
	$text .= "Decode reqdata: ".print_r($variablearray,true)."\n";
	writelog($text);
	
	//print_r($variablearray);
	$user_id			= $variablearray[0];
	$user_name 			= $variablearray[1];
	$item_id 			= $variablearray[2];
	$file_id 	 		= $variablearray[3];
	$company_id 		= $variablearray[4];
	$preview	 		= $variablearray[5];
	$user_training_id	= $variablearray[6];
	$attempt			= isset($variablearray[7]) ? $variablearray[7] : 1;
	
	writelog("Session ID: ".print_r($variablearray,true). "\n\n");
	
	$sco = getScorm($item_id, $user_id, $file_id, $company_id );
	if (!empty($command)) {
    $command = strtolower($command);
    $mode = 'normal';
    $status = 'Not Initialized';
    //$attempt = 1;
       switch ($command) {
            case 'getparam':
					
			        if ($usertrack=scorm_get_tracks($item_id, $file_id, $user_id, $attempt, $user_training_id,$company_id)) {
                        $userdata = $usertrack;
                    } else {
                        $userdata->status = '';
                        $userdata->score_raw = '';
                    }
                    $userdata->student_id = $company_id."".$user_id;
                    $userdata->student_name = $user_name;
                    $userdata->mode = 'normal';
                    if ($userdata->mode == 'normal') {
                        $userdata->credit = 'credit';
                    } else {
                        $userdata->credit = 'no-credit';
                    }
					//print_r($sco);echo "sss:";
					$userdata->course_id = $sco->identifier;
					$userdata->datafromlms = isset($sco->datafromlms)?$sco->datafromlms:'';
					$userdata->masteryscore = isset($sco->masteryscore) && is_numeric($sco->masteryscore)?trim($sco->masteryscore):'';
					$userdata->max_time_allowed = '';
					$userdata->time_limit_action = '';

					echo "error=0\r\nerror_text=Successful\r\naicc_data=";
					echo "[Core]\r\n";
					echo 'Student_ID='.$userdata->student_id."\r\n";
					echo 'Student_Name='.$userdata->student_name."\r\n";
					if (isset($userdata->{'cmi.core.lesson_location'})) {
						echo 'Lesson_Location='.$userdata->{'cmi.core.lesson_location'}."\r\n";
					} else {
						echo 'Lesson_Location='."\r\n";
					}
					echo 'Credit='.$userdata->credit."\r\n";
					if (isset($userdata->status)) {
						if ($userdata->status == '') {
							$userdata->entry = ', ab-initio';
						} else {
							if (isset($userdata->{'cmi.core.exit'}) && ($userdata->{'cmi.core.exit'} == 'suspend')) {
								$userdata->entry = ', resume';
							} else {
								$userdata->entry = '';
							}
						}
					}
					if (isset($userdata->status)) {
						echo 'Lesson_Status='.$userdata->status.$userdata->entry."\r\n";
						$scormsession->scorm_lessonstatus = $userdata->status;
					} else {
						echo 'Lesson_Status=not attempted'.$userdata->entry."\r\n";
						$scormsession->scorm_lessonstatus = 'not attempted';
					}
					if (isset($userdata->score_raw)) {
						/* $max = '';
						$min = '';
						if (isset($userdata->{'cmi.core.score.max'}) && !empty($userdata->{'cmi.core.score.max'})) {
							$max = ', '.$userdata->{'cmi.core.score.max'};
							if (isset($userdata->{'cmi.core.score.min'}) && !empty($userdata->{'cmi.core.score.min'})) {
								$min = ', '.$userdata->{'cmi.core.score.min'};
							}
						}
						echo 'Score='.$userdata->score_raw.$max.$min."\r\n"; */
						echo 'Score='.$userdata->score_raw."\r\n";
					} else {
						echo 'Score='."\r\n";
					}
					if (isset($userdata->total_time)) {
						echo 'Time='.$userdata->total_time."\r\n";
					} else {
						echo 'Time='.'00:00:00'."\r\n";
					}
					echo 'Lesson_Mode='.$userdata->mode."\r\n";
					if (isset($userdata->{'cmi.suspend_data'})) {
						echo "[Core_Lesson]\r\n".rawurldecode($userdata->{'cmi.suspend_data'})."\r\n";
					} else {
						echo "[Core_Lesson]\r\n";
					}
					echo "[Core_Vendor]\r\n".$userdata->datafromlms."\r\n";
					echo "[Evaluation]\r\nCourse_ID = {".$userdata->course_id."}\r\n";
					echo "[Student_Data]\r\n";
					echo 'Mastery_Score='.$sco->masteryscore."\r\n";
					echo 'Max_Time_Allowed='.$userdata->max_time_allowed."\r\n";
					echo 'Time_Limit_Action='.$userdata->time_limit_action."\r\n";
            break;
			case 'putparam':
                    if (!empty($aiccdata)) {
                        $initlessonstatus = 'not attempted';
                        $lessonstatus = 'not attempted';
                        if (isset($scormsession->scorm_lessonstatus)) {
                            $initlessonstatus = $scormsession->scorm_lessonstatus;
                        }
                        $score = '';
                        $datamodel['lesson_location'] = 'cmi.core.lesson_location';
                        $datamodel['lesson_status'] = 'cmi.core.lesson_status';
                        $datamodel['score'] = 'cmi.core.score.raw';
                        $datamodel['time'] = 'cmi.core.session_time';
                        $datamodel['[core_lesson]'] = 'cmi.suspend_data';
                        $datamodel['[comments]'] = 'cmi.comments';
                        $datarows = explode("\r\n", $aiccdata);
                        while ((list(, $datarow) = each($datarows)) !== false) {
                            if (($equal = strpos($datarow, '=')) !== false) {
                                $element = strtolower(trim(substr($datarow, 0, $equal)));
                                $value = trim(substr($datarow, $equal+1));
                                if (isset($datamodel[$element])) {
                                    $element = $datamodel[$element];
                                    switch ($element) {
                                        case 'cmi.core.lesson_location':
											$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                        break;
                                        case 'cmi.core.lesson_status':
                                            $statuses = array(
                                                       'passed' => 'passed',
                                                       'completed' => 'completed',
                                                       'failed' => 'failed',
                                                       'incomplete' => 'incomplete',
                                                       'browsed' => 'browsed',
                                                       'not attempted' => 'not attempted',
                                                       'p' => 'passed',
                                                       'c' => 'completed',
                                                       'f' => 'failed',
                                                       'i' => 'incomplete',
                                                       'b' => 'browsed',
                                                       'n' => 'not attempted'
                                                       );
                                            $exites = array(
                                                       'logout' => 'logout',
                                                       'time-out' => 'time-out',
                                                       'suspend' => 'suspend',
                                                       'l' => 'logout',
                                                       't' => 'time-out',
                                                       's' => 'suspend',
                                                       );
                                            $values = explode(',', $value);
                                            $value = '';
                                            if (count($values) > 1) {
                                                $value = trim(strtolower($values[1]));
                                                $value = $value[0];
                                                if (isset($exites[$value])) {
                                                    $value = $exites[$value];
                                                }
                                            }
                                            if (empty($value) || isset($exites[$value])) {
                                                $subelement = 'cmi.core.exit';
												$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                            }
                                            $value = trim(strtolower($values[0]));
                                            $value = $value[0];
                                            if (isset($statuses[$value]) && ($mode == 'normal')) {
                                                $value = $statuses[$value];
												$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                            }
                                            $lessonstatus = $value;
                                        break;
                                        case 'cmi.core.score.raw':
											$value = trim($value);
											$score = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                            /* $values = explode(',', $value);
                                            if ((count($values) > 1) && ($values[1] >= $values[0]) && is_numeric($values[1])) {
                                                $subelement = 'cmi.core.score.max';
                                                $value = trim($values[1]);
												$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                                if ((count($values) == 3) && ($values[2] <= $values[0]) && is_numeric($values[2])) {
                                                    $subelement = 'cmi.core.score.min';
                                                    $value = trim($values[2]);
													$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                                }
                                            }

                                            $value = '';
                                            if (is_numeric($values[0])) {
                                                $value = trim($values[0]);
												$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                            } */
											
                                        break;
                                        case 'cmi.core.session_time':
                                             $scormsession->sessiontime = $value;
                                        break;
										case 'cmi.suspend_data':
                                             $id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                        break;
                                    }
                                }
                            } else {
                                if (isset($datamodel[strtolower(trim($datarow))])) {
                                    $element = $datamodel[strtolower(trim($datarow))];
                                    $value = '';
                                    while ((($datarow = current($datarows)) !== false) && (substr($datarow, 0, 1) != '[')) {
                                        $value .= $datarow."\r\n";
                                        next($datarows);
                                    }
                                    $value = rawurlencode($value);
									$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, $element, $value, $user_training_id, $company_id);
                                }
                            }
                        }
                        if (($mode == 'browse') && ($initlessonstatus == 'not attempted')) {
                            $lessonstatus = 'browsed';
							$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, 'cmi.core.lesson_status', 'browsed', $user_training_id, $company_id);
                        }
                        if ($mode == 'normal') {
                            //if ($sco = scorm_get_sco($scoid)) {
                                if (isset($sco->masteryscore) && is_numeric($sco->masteryscore)) {
                                    if ($score != '') { // $score is correctly initialized w/ an empty string, see above
                                        if ($score >= trim($sco->masteryscore)) {
                                            $lessonstatus = 'passed';
                                        } else {
                                            $lessonstatus = 'failed';
                                        }
                                    }
                                }
								$id = scorm_insert_track($user_id, $item_id, $file_id, $attempt, 'cmi.core.lesson_status', $lessonstatus, $user_training_id, $company_id);
                            //}
                        }
                    }
                    echo "error=0\r\nerror_text=Successful\r\n";
            break;
            case 'exitau':
                   echo "error=0\r\nerror_text=Successful\r\n";
            break;
            default:
                echo "error=1\r\nerror_text=Invalid Command\r\n";
            break;

        }
} else {
    if (empty($command)) {
        echo "error=1\r\nerror_text=Invalid Command\r\n";
    } else {
        echo "error=3\r\nerror_text=Invalid Session ID\r\n";
    }
}

	function scorm_insert_track($userid, $item_id, $file_id, $attempt, $element, $value, $user_training_id=0, $company_id) {
		
		//writelog("methode: scorm_insert_track\nParameters: item_id:$item_id, userid:$userid, file_id:$file_id, attempt: $attempt, element: $element, value: $value, user_training_id: $user_training_id, company_id: $company_id\n\n");
		
		$link = dbConnect();
		$id = null;
		$score = '';
		$sqlquery = "select * from scorm_score_data where user_id='{$userid}' AND item_id='{$item_id}' AND file_id='{$file_id}' AND attempt='{$attempt}' AND element='{$element}' AND user_training_id='{$user_training_id}'";
		
		//writelog("SQL:".$sqlquery."\n");

		$runquery = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
		
		
		$scormtracldata = array();
		if(mysqli_num_rows($runquery)>0){
			if ($element != 'x.start.time') { 
				$track=mysqli_fetch_object($runquery);
				if(isset($track->id) && $track->id!=""){
					$value = addslashes_js($value);					
					$sqlquery = "update scorm_score_data set attempt='".$track->attempt."',  value='".$value."', timemodified='".time()."' where id='".$track->id."' and company_id='{$company_id}' and element='".$track->element."'";
					//writelog("SQL:".$sqlquery."\n");
					mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
				}
			}		
		} else {
			$sqlquery = "insert into scorm_score_data(user_id, item_id,file_id, company_id,attempt, element,value, timemodified, user_training_id) values('".$userid."', '".$item_id."', '".$file_id."', '".$company_id."', '".$attempt."', '".$element."', '".addslashes_js($value)."', '".time()."', '".$user_training_id."')";
			//writelog("SQL:".$sqlquery."\n");
			mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
		}
		
		$value1 =addslashes_js($value);
		if (in_array($element, array('cmi.completion_status', 'cmi.core.lesson_status', 'cmi.success_status'))
			 && in_array($value, array('completed', 'passed', 'failed'))) {
			updateTrainingStatus($item_id, $userid, $file_id, $user_training_id, $company_id);
			$score = scorm_update_grades($item_id, $userid, $file_id, $user_training_id, $company_id);
		}
		if (strstr($element, '.score.raw')) {
			$score = scorm_update_grades($item_id, $userid, $file_id, $user_training_id, $company_id);
		}

		if($element == 'cmi.suspend_data'){
			updateTrainingProgressPercentage($item_id,$userid,$item_id,$file_id,$attempt,$element, $user_training_id,$company_id);
		}
		return $score;
	}


	
	function updateTrainingStatus($item_id, $user_id, $file_id, $user_training_id=0, $company_id){
		
		writelog("Methode: updateTrainingStatus\nParameters: item_id:$item_id, user_id:$user_id, file_id:$file_id, user_training_id:$user_training_id, $company_id\n");
		
		date_default_timezone_set('America/Los_Angeles');
		$link = dbConnect();
		$data = array();
		$sqlSelect = "select utc.user_training_id, utc.item_id, utc.learning_plan_id, utc.blended_program_id, utc.company_id, im.credit_value from user_training_catalog utc join item_master as im on im.item_id = utc.item_id where utc.item_id = '{$item_id}' AND utc.user_id = '{$user_id}' AND utc.user_training_id = '{$user_training_id}' AND utc.company_id = '{$company_id}'";	
		//writelog("SQL:".$sqlSelect."\n");
		$runquery = mysqli_query($link,$sqlSelect) or writelog("Error in SQL:".$sqlSelect."\nError msg:".mysqli_error()."\n\n");
		if($runquery){
			while ($row = @mysqli_fetch_assoc($runquery))
			{
				$data[] = $row;
			}	
		}
		
		if(count($data)>0){
			foreach($data as  $rowvalue ){
				$training_id = $rowvalue['user_training_id'];
				$item_id = $rowvalue['item_id'];
				$learning_plan_id = @$rowvalue['learning_plan_id'];
				$blended_id = @$rowvalue['blended_program_id'];
				$company_id = $rowvalue['company_id'];
				$credit_value = $rowvalue['credit_value'];
				$completion_time = date("Y-m-d");
				$transcript_id=0;
				$sqlcheck = "select * from user_transcript where user_id = '{$user_id}' AND item_id = '{$item_id}' and company_id = '{$company_id}' ";
				//writelog("SQL:".$sqlcheck."\n");
				$data1 = mysqli_query($link,$sqlcheck) or writelog("Error in SQL:".$sqlcheck."\nError msg:".mysqli_error()."\n\n");
		
				if(mysqli_num_rows($data1)>0){
					
					$update = mysqli_fetch_object($data1);
					$transcript_id= $update->transcript_id;
					$history_transcript_id = $update->transcript_id;
					$history_class_schedule_id = isset($update->class_schedule_id) ? $update->class_schedule_id : '0';
					$history_class_id = $update->class_id;
					$history_registration_id = $update->registration_id;
					$history_is_blended = $update->is_blended;
					$history_user_id = $update->user_id;
					$history_item_id = $update->item_id;
					$history_user_training_id = $update->user_training_id;
					$history_company_id = $update->company_id;
					$history_credit_value = $update->credit_value;
					$history_completion_date = $update->completion_date;
					$history_expiration_date = $update->expiration_date;
					$history_times = $update->times;
					$history_credit_from = $update->credit_from;
					$history_is_approved = isset($update->is_approved) ? $update->is_approved : '0';
					$history_is_active = $update->is_active;
					$history_is_expired = $update->is_expired;
					$history_last_modified_by = $update->last_modified_by;
					$history_last_modified_date = $update->last_modified_date;
					
					//User Transcript
					$sqlInsert = "insert into user_transcript_history (transcript_id, class_schedule_id, class_id, registration_id, is_blended, user_id, item_id, user_training_id, company_id, credit_value, completion_date, expiration_date, times, credit_from, is_approved, is_active, is_expired, last_modified_by) VALUES ('".$history_transcript_id."', '".$history_class_schedule_id."', '".$history_class_id."', '".$history_registration_id."', '".$history_is_blended."', ".$history_user_id.", ".$history_item_id.", ".$history_user_training_id.", ".$history_company_id.", '".$history_credit_value."', '".$history_completion_date."', '".$history_expiration_date."', '".$history_times."', '".$history_credit_from."', '".$history_is_approved."', '".$history_is_active."', '".$history_is_expired."', '".$history_last_modified_by."' )";
					//writelog("SQL:".$sqlInsert."\n");
					$runquery = mysqli_query($link,$sqlInsert) or writelog("Error in SQL:".$sqlInsert."\nError msg:".mysqli_error()."\n\n");
					
					//Update User Transcript
					if($item_id !="" && $user_id!="" && $user_training_id!=""){
						$updateTranscript = "update user_transcript set is_active= 1, completion_date = '".$completion_time."', credit_value = '".$credit_value."', user_training_id = '".$user_training_id."' WHERE transcript_id = ".$history_transcript_id." and company_id = '".$company_id."'";
						//writelog("SQL:".$updateTranscript."\n");
						$runquery = mysqli_query($link,$updateTranscript) or writelog("Error in SQL:".$updateTranscript."\nError msg:".mysqli_error()."\n\n");
					}
					
					//User training catalog
					$sqlSelect = "select * from user_training_catalog where item_id = '{$item_id}' AND user_id = '{$user_id}' AND user_training_id = '{$user_training_id}' AND company_id = '{$company_id}' and training_status_id=1 ";
					//writelog("SQL:".$sqlSelect."\n");
					$data1 = mysqli_query($link,$sqlSelect) or writelog("Error in SQL:".$sqlSelect."\nError msg:".mysqli_error()."\n\n");
				
					if(mysqli_num_rows($data1)>0){
						
						$data = mysqli_fetch_object($data1);
						
						$history_user_training_id = $data->user_training_id;
						$history_training_title = $data->training_title;
						$history_class_schedule_id = $data->class_schedule_id;
						$history_user_program_type_id = $data->user_program_type_id;
						$history_program_id = $data->program_id;
						$history_item_id = $data->item_id;
						$history_learning_plan_id = $data->learning_plan_id;
						$history_blended_program_id = $data->blended_program_id;
						$history_assignment_id = $data->assignment_id;
						$history_is_approved = $data->is_approved; 
						$history_is_enroll = $data->is_enroll;
						$history_user_id = $data->user_id; 
						$history_avg_rating = isset($data->avg_rating) ? $data->avg_rating : '0';
						$history_status_id = isset($data->status_id) ? $data->status_id : '0';
						$history_company_id = $data->company_id;
						$history_training_date = isset($data->training_date) ? $data->training_date : '0000-00-00';
						$history_trainig_time = isset($data->trainig_time) ? $data->trainig_time : '00:00:00';
						$history_session_id = $data->session_id;
						$history_training_status_id = $data->training_status_id;
						$history_training_type_id = $data->training_type_id;
						$history_last_modfied_by = $data->last_modfied_by; 
						$history_last_modified_time = $data->last_modified_time;
						$history_is_active = $data->is_active; 
						$history_created_date = $data->created_date;
						
						
						$sqlInsertIntoHistory = "INSERT INTO user_training_catalog_history(user_training_id, training_title, class_schedule_id, user_program_type_id, program_id, item_id, learning_plan_id, blended_program_id, assignment_id, is_approved, is_enroll, user_id, avg_rating, status_id, company_id, training_date, trainig_time, session_id, training_status_id, training_type_id, last_modfied_by, last_modified_time, is_active, created_date) VALUES('".$history_user_training_id."', '".$history_training_title."', '".$history_class_schedule_id."', '".$history_user_program_type_id."', '".$history_program_id."', '".$history_item_id."', '".$history_learning_plan_id."', '".$history_blended_program_id."', '".$history_assignment_id."', '".$history_is_approved."', '".$history_is_enroll."', '".$history_user_id."', '".$history_avg_rating."', '".$history_status_id."', '".$history_company_id."', '".$history_training_date."', '".$history_trainig_time."', '".$history_session_id."', '".$history_training_status_id."', '".$history_training_type_id."', '".$history_last_modfied_by."', '".$history_last_modified_time."', '".$history_is_active."', '".$history_created_date."')";
						mysqli_query($link,$sqlInsertIntoHistory) or writelog("Error in SQL:".$sqlInsertIntoHistory."\nError msg:".mysqli_error()."\n\n");
						//exit(print_r($data,true));
					}
					
					if($item_id !="" && $user_id!="" && $user_training_id!=""){
						$sqlquery = "update user_training_catalog set training_status_id = 1 where item_id = ".$item_id." AND user_id = ".$user_id." and user_training_id = ".$user_training_id;
						//writelog("SQL:".$sqlquery."\n");
						$runquery = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
					}
				}else{

					$sqlInsert = "insert into user_transcript ( transcript_id, class_id, registration_id, user_id, item_id, user_training_id, company_id, credit_value, completion_date, expiration_date, times, is_active, last_modified_by ) VALUES (NULL, '0', '".@$blended_id."',".$user_id.",".$item_id.",".$training_id.",".$company_id.",'".$credit_value."','".$completion_time."', '".$completion_time."',100,1,".$user_id.")";
					//exit($sqlInsert);
					//writelog("SQL:".$sqlInsert."\n");
					$runquery = mysqli_query($link,$sqlInsert) or writelog("Error in SQL:".$sqlInsert."\nError msg:".mysqli_error()."\n\n");
					$transcript_id=mysqli_insert_id($link);
					//////////////////////////this code updates the transcript_id column in sco_score_data
					/*
					
					if($transcript_id){
						$sqlquery="select scorm_data_id from sco_score_data where transcript_id='{$transcript_id}'";
						$sql_res = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
						if(mysqli_num_rows($sql_res)>0){
							$check_sco = mysqli_fetch_array($sql_res);
							if($check_sco==false){
								$sqlquery="select scorm_data_id from sco_score_data where user_id='{$user_id}' and item_id='{$item_id}' and user_training_id='{$user_training_id}'";
								$sql_res = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
								if(mysqli_num_rows($sql_res)>0){
									$scorm_data_id = mysqli_fetch_array($sql_res);
									if($scorm_data_id!=false && $scorm_data_id!='' && $scorm_data_id!=NULL){
										$sqlquery="update sco_score_data set transcript_id='{$transcript_id}' where scorm_data_id='".$scorm_data_id['scorm_data_id']."'";
										mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
									}
								}
							}
						}
					}
					*/
					///////////////////////
					if($item_id !="" && $user_id!="" && $user_training_id!=""){
						$sqlquery = "update user_training_catalog set training_status_id = 1 where item_id = ".$item_id." AND user_id = ".$user_id." and user_training_id = ".$user_training_id;
						//writelog("SQL:".$sqlquery."\n");
						$runquery = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError:".mysqli_error()."\n\n");
					}
				}
				if(($company_id == 43 || $company_id == 56) && $transcript_id!=0){
					updateLearningPlanCompletion($transcript_id, $item_id, $user_id, $company_id);
				}elseif(($company_id == 83 || $company_id == 84) && $transcript_id!=0){
				   updatestandardLearningPlanCompletion($transcript_id, $item_id, $user_id,$company_id);
				}
			}
		}	
	}
	
	function scorm_update_grades($item_id, $userid, $file_id, $user_training_id=0, $company_id) {
		//writelog("methode: scorm_update_grades\nParameters: item_id:$item_id, userid:$userid, file_id:$file_id, user_training_id: $user_training_id");
		$link = dbConnect();
		if ($item_id != null) {
			$usertrack = new stdClass;
			$usertrack->userid = $userid;
			$usertrack->file_id = $file_id;
			$usertrack->score_raw = '0';
			$usertrack->status = '1';
			$usertrack->total_time = '00:00:00';
			$usertrack->session_time = '00:00:00';
			$usertrack->timemodified = 0;
			$usertrack->statusscore = 'failed';
			$usertrack->ispassfail  = 0;
			$usertrack->maxscore	= 100;
			
			$sqlquery = "select * from scorm_score_data where user_id = '{$userid}' AND file_id= '{$file_id}' AND item_id = '{$item_id}' AND user_training_id = '{$user_training_id}'";
			//writelog("SQL:".$sqlquery."\n");
			$runquery=mysqli_query($link,$sqlquery);// or die(mysqli_error());
			while ($row = @mysqli_fetch_assoc($runquery))
			{
				$tracks[] = $row;
			}
			
			 if(count($tracks)>0){
				foreach ($tracks as $track) {
					$element = $track['element'];
					$value = stripslashes_safe($track['value']);
					$usertrack->{$element} = $value;
					//writelog(print_r($usertrack,true)."\n");
					switch ($element) {
							case 'cmi.core.lesson_status':
								$usertrack->statusscore = $track['value'];
								break;
							case 'cmi.completion_status':
									if ($track['value'] == 'not attempted') {
											$track['value'] = 'notattempted';
									}
									$usertrack->status = $track['value'];
							break;
							case 'cmi.core.score.raw':
								//$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
								$usertrack->score_raw = trim($track['value']);
								$usertrack->ispassfail  = 1;
								break;
							case 'cmi.score.raw':
									//$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
									$usertrack->score_raw = trim($track['value']);
									$usertrack->ispassfail  = 1;
									break;
							case 'cmi.core.session_time':
							case 'cmi.session_time':
									$usertrack->session_time = $track['value'];
							break;
							case 'cmi.core.total_time':
							case 'cmi.total_time':
									$usertrack->total_time = $track['value'];
							break;
							case 'cmi.score.max' :
								$maxscore = $track['value'];
								$usertrack->maxscore = $maxscore;
							break;		
							case 'cmi.success_status':	
							if($usertrack->score_raw == '0'){
								global $correctAnsCount;
								global $incorrectAnsCount;
								$maxscore = (isset($maxscore) ? $maxscore : 100);
								$usertrack->score_raw	= ($correctAnsCount / ($correctAnsCount + $incorrectAnsCount)) * $maxscore;
								$usertrack->ispassfail  = 1;
								$usertrack->statusscore = $track['value'];
							}
							break;
					}
					if (isset($track['timemodified']) && ($track['timemodified'] > $usertrack->timemodified)) {
							$usertrack->timemodified = $track['timemodified'];
					}
				}
			}
			
			return scorm_grade_item_update($item_id, $usertrack, $file_id, $userid, $user_training_id, $company_id);
		}
	}
	
	function updateTrainingProgressPercentage($item_id, $userid, $item_id, $file_id, $attempt, $element, $user_training_id, $company_id){
		
		//writelog("methode: updateTrainingProgressPercentage\nParameters: item_id:$item_id, userid:$userid, item_id:$item_id, file_id:$file_id, attempt:$attempt, element:$element, user_training_id:$user_training_id\n");
		$link = dbConnect();
		$sqlquery = "SELECT * FROM scorm_score_data WHERE user_id='{$userid}' AND item_id='{$item_id}' AND file_id='{$file_id}' AND attempt='{$attempt}' AND user_training_id='$user_training_id' AND (element='cmi.completion_status' OR element='cmi.core.lesson_status')";
		//writelog("SQL:".$sqlquery."\n");
		$res = mysqli_query($link,$sqlquery) or writelog("Error SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
		if(mysqli_num_rows($res)>0){
			$track1 = @mysqli_fetch_object($res);
		}else{ return false;}
		
		if($track1->value =='incomplete' || $track1->value =='failed'){
			$title = '';
			$sqlquery = "select title from uploaded_files WHERE files_id = '{$file_id}'";
			//writelog("SQL:".$sqlquery."\n");
			$runquery = mysqli_query($link,$sqlquery) or writelog("Error SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
			if(mysqli_num_rows($runquery)>0){
				$data1 = @mysqli_fetch_object($runquery);
				if(isset($data1->title)) $title = $data1->title;
			}
			$sqlquery = "SELECT * FROM scorm_score_data WHERE user_id='$userid' AND item_id='$item_id' AND file_id='$file_id' AND attempt='$attempt' AND user_training_id='$user_training_id' AND element='cmi.suspend_data'";
			
			//writelog("SQL:".$sqlquery."\n");
			$res = mysqli_query($link,$sqlquery) or writelog("Error SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
			
			if(mysqli_num_rows($res)>0){
				$track = @mysqli_fetch_object($res);
			}
			
			$progressdata = $track->value;
			$sqlquery = "SELECT * FROM scorm_score_data WHERE user_id=$userid AND item_id=$item_id AND file_id=$file_id AND attempt=$attempt AND user_training_id='$user_training_id' AND element='cmi.core.lesson_location'";
			//writelog("SQL:".$sqlquery."\n");
			$res = mysqli_query($link,$sqlquery) or writelog("Error SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
			if(mysqli_num_rows($res)>0){
				$trackloc = @mysqli_fetch_object($res);
			}
			
			$numberOfSlides = $trackloc->value;
			$numberOfSlides = 0;
			$vivewSlides = 0;
			$totalSlides = 0;
		
			$percentageProgress = 0;
			if(strstr($progressdata,'viewed')){
				if($progressdata != ''){
					$slideNum = 0;
					$progData1 = explode('|', $progressdata);
					$lastvived1 = $progData1[1];
					$slideNums = explode('=', $lastvived1);
					$slideNum = (int)$slideNums[1];
					$total1 = $progData1[2];
					$numTotal1 = explode('#', $total1);
					$slides = explode(',', $numTotal1[3]);
					$totalSlides = count($slides)-1;
					$vivewSlides = $slideNum;
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($slideNum/$totalSlides)*100);
				}
		    }else if(strstr($progressdata, 'toc')){
			   if($progressdata != ''){
					$slideNum = 0;
					$progData1 = explode(':', $progressdata);
					$lastvived1 = $progData1[0];
					$progData11 = explode('`', $lastvived1);
					$slides = $progData11[1];
					$numSlides1 = explode(',', $slides);
					$totalSlides = count($numSlides1);
					$vivedSlides = 0;
					for($x=0; $x<$totalSlides; $x++){
						if($numSlides1[$x]== '1'){
							$vivedSlides++;
						}
					}
					$vivewSlides = $vivedSlides;
					
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivedSlides/$totalSlides)*100);
				}

			}else if(strstr($progressdata,'Enone')){
				if($progressdata != ''){
					$slideNum = 0;
					$progData1 = explode('Enone', $progressdata);
					$totalSlides = count($progData1)-1;
					
					$progData2 = explode('P1Enone', $progressdata);
					$vivewSlides = count($progData2);
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivewSlides/$totalSlides)*100);
				}
			}else if(strstr($progressdata,'AA000A') || strstr($title, 'Adobe_Presenter_Quiz')){
				if($progressdata != ''){
					$numlen = strrpos($progressdata, "000");
					$str2 = substr($progressdata, 0, $numlen);
					$progData1 = explode('100', $str2);

					$progData2 = explode('1', $progData1[0]);
					$slidescount = count($progData2);

					$totalData1 = explode('0', $progData1[1]);
					$totalSlides = count($totalData1)+1;

					$vivewSlides = $slidescount - 1;
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivewSlides/$totalSlides)*100);
				}
			}else if(strstr($progressdata,'A%24n')){
				if($progressdata != ''){
					$raw_data = explode('A%24n',$progressdata);
					$vivewSlides = $totalSlides =0;
					foreach($raw_data as $key=>$value){
						$check_point = substr($value,0,2);
						if($check_point=='P1'){
							$vivewSlides++;
						}
						$totalSlides++;
					}
					
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivewSlides/$totalSlides)*100);
				}	
			}else{
				if($progressdata != ''){
					$slideNum = 0;
					$pos = strpos($progressdata, '$');
					if ($pos !== false) {
						$progData1 = explode('$', $progressdata);						
					}
					
					$pos = strpos($progressdata, '|');
					if ($pos !== false) {
						$progData1 = explode('|', $progressdata);						
					}
					
					$pos = strpos($progressdata, ':');
					if ($pos !== false) {
						$progData1 = explode(':', $progressdata);						
					}
					
					$lastvived1 = $totalSlides = $percentageProgress = 0;
					
					if(isset($progData1[0])) {
						$lastvived1 = (int)$progData1[0];
					}
					
					if(isset($progData1[1])){
						$pos = strpos($progData1[1], ',');
						if ($pos !== false) {
							$progData11 = explode(',', $progData1[1]);
							$totalSlides = count($progData11);
						}else{
							$totalSlides = strlen($progData1[1]);
						}
						if($totalSlides!=0){
							$percentageProgress = floor(($lastvived1/$totalSlides)*100);
						}
					}
				}
			}
			
		
			$sqlSelect = "select utc.user_training_id, utc.item_id, utc.learning_plan_id, utc.blended_program_id, utc.company_id, im.credit_value from user_training_catalog utc join item_master as im on im.item_id = utc.item_id where utc.item_id = '{$item_id}' AND utc.user_id = '{$userid}' AND user_training_id = '{$user_training_id}'";
			
			//writelog("SQL:".$sqlSelect."\n");
			
			$data = mysqli_query($link,$sqlSelect) or writelog("Error in SQL:".$sqlSelect."\nError msg:".mysqli_error()."\n\n");
		
		
			if(mysqli_num_rows($data)>0 && $percentageProgress > 0){
				while($rowvalue = mysqli_fetch_array($data)){
					$training_id = $rowvalue['user_training_id'];
					$item_id = $rowvalue['item_id'];
					
					$sqlquery = "update user_training_history set is_active = 0 where item_id='{$item_id}' AND user_id='{$userid}' and user_training_id='{$training_id}'";
					//writelog("SQL:".$sqlquery."\n");
					$runquery2 = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");

					$sqlInsert = "insert into user_training_history ( id, user_training_id, user_id, training_start_date_time, php_session_id, course_completion_percentage, item_id, is_active, suspend_data) VALUES (NULL, '".$training_id."', ".$userid.", now(), '', '".$percentageProgress."', ".$item_id.", '1', '".$progressdata."')";
					//writelog("SQL:".$sqlInsert."\n");
					$runquery1 = mysqli_query($link,$sqlInsert) or writelog("Error in SQL:".$sqlInsert."\nError msg:".mysqli_error()."\n\n");
				}
			}
		}
	}
	
	function scorm_grade_item_update($item_id, $usertrack=NULL,$file_id,$userid, $user_training_id, $company_id) {
		
		//writelog("methode: scorm_grade_item_update\nParameters: item_id:$item_id, usertrack:$usertrack, file_id:$file_id, userid:$userid, user_training_id:$user_training_id\n");
		
		$link = dbConnect();

		$status = 1;
		$totalQuestions = 100;
		
		$item_id = $item_id;
		$user_id = $userid;
		$status1 = $usertrack->statusscore;
		$value_str = $usertrack->score_raw;
		$value_arr = explode(",", $value_str);
		if(isset($value_arr[0]) && !isset($value_arr[1])){
			$value = (int)$value_arr[0];
		}elseif(isset($value_arr[0]) && isset($value_arr[1])){
			$value = (int)(($value_arr[0]/$value_arr[1])*100);
		}
		//writelog("score---$value--\n");
		if($value > 0){
			$maxScore = 100;
		
			$passscore = getpasscore($file_id, $item_id,$company_id );

			if($passscore > 0 || $usertrack->ispassfail == 1){
				if($value >= $passscore){
					$status = 1;
				}else{
					if($status1 != ''){
						$statuspass = array('pass','passed','completed'); 
						if($usertrack->ispassfail == 1 && in_array(strtolower($status1), $statuspass)){
							$status = 1;
						}else if($usertrack->ispassfail == 1){
							$status = 0;
						}else{
							$status = 2;
						}
					}else{
						$status = 2;
					}
				}
			}else{
				$status = 2;
			}
			
			if($company_id == 51 && $value >= 80){
				$status = 1;
			}


			$sqlcheck = "select * from sco_score_data where user_id = '{$user_id}' AND item_id = '{$item_id}' AND user_training_id = '{$user_training_id}' AND company_id = '{$company_id}'";
			//writelog("SELECT SQL:".$sqlcheck."\n");
			$runquerycheck = mysqli_query($link,$sqlcheck) or writelog("Error SQL:".$sqlcheck."\nError msg:".mysqli_error()."\n\n");
			
			if(mysqli_num_rows($runquerycheck)>0){
				if($user_training_id!="" && $user_id!="" && $item_id!=""){
					$sqldataInsert = "update sco_score_data set correct_answer = '".$value."', status ='".$status."' where user_training_id= '".$user_training_id."' AND user_id = '".$user_id."' AND item_id = '".$item_id."'";
					
					//writelog("update SQL:".$sqldataInsert."\n");
					$runqueryScore = mysqli_query($link,$sqldataInsert) or writelog("Error SQL:".$sqldataInsert."\nError msg:".mysqli_error()."\n\n");	
				}
			}else{
				$sqldataInsert = "insert into sco_score_data( scorm_data_id, user_training_id, user_id, item_id, max_score, total_questions, correct_answer, status, company_id) VALUES(NULL, '".$user_training_id."', '".$user_id."', '".$item_id."', '".$maxScore."', '".$totalQuestions."', '".$value."', '".$status."', '".$company_id."')";
				//writelog("INSERT SQL:".$sqldataInsert."\n");
				$runqueryScore = mysqli_query($link,$sqldataInsert) or writelog("Error SQL:".$sqldataInsert."\nError msg:".mysqli_error()."\n\n");
			}
		}
		return $value;
	}
	
	function getpasscore($file_id, $item_id,$company_id){
		
		//writelog("methode: getpasscore\nParameters: file_id:$file_id, item_id:$item_id\n");
		
		$link = dbConnect();
		$sqlquery = "select masteryscore, company_id from uploaded_files where files_id = '{$file_id}'"; 
		//writelog("SQL:".$sqlquery."\n");
		$data = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
		
		if(mysqli_num_rows($data)>0){
			while($value=mysqli_fetch_array($data)) {
				$masteryscore = $value['masteryscore'];
				$company_id = $value['company_id'];
			}
			if($company_id == '51'){
				$masteryscore = 80;
			}
			return $masteryscore;
		}
		return $masteryscore;
	}
	
	function stripslashes_safe($mixed) {
		
		//writelog("methode: stripslashes_safe\nParameters: mixed: $mixed\n");
		
		// there is no need to remove slashes from int, float and bool types
		if (empty($mixed)) {
			//nothing to do...
		} else if (is_string($mixed)) {
			if (ini_get_bool('magic_quotes_sybase')) { //only unescape single quotes
				$mixed = str_replace("''", "'", $mixed);
			} else { //the rest, simple and double quotes and backslashes
				$mixed = str_replace("\\'", "'", $mixed);
				$mixed = str_replace('\\"', '"', $mixed);
				$mixed = str_replace('\\\\', '\\', $mixed);
			}
		} else if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = stripslashes_safe($value);
			}
		} else if (is_object($mixed)) {
			$vars = get_object_vars($mixed);
			foreach ($vars as $key => $value) {
				$mixed->$key = stripslashes_safe($value);
			}
		}
		return $mixed;
	}

	function ini_get_bool($ini_get_arg) {
		
		//writelog("methode: ini_get_bool\nParameters: ini_get_arg: $ini_get_arg\n");
		
		$temp = ini_get($ini_get_arg);
		if ($temp == '1' or strtolower($temp) == 'on') {
			return true;
		}
		return false;
	}
	
	function getScorm($item_id, $userid, $file_id, $company_id ){
		
		//writelog("methode: getScorm\nParameters: item_id: $item_id, userid: $userid, file_id: $file_id\n");
		
		$link = dbConnect();
		$sqlquery = "select * from uploaded_files where files_id = $file_id"; 
		//writelog("SQL:".$sqlquery."\n");
		
		$data = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquerysqlquery."\nError msg:".mysqli_error()."\n\n");
		
		$sco = new stdClass;
		if(mysqli_num_rows($data)>0){
			//$data = json_decode(json_encode($data),true);
			while($value=mysqli_fetch_array($data)) {
				$sco->item_id = $item_id;
				$sco->course = $item_id;
				$sco->id = $value['files_id'];
				$sco->scorm = $item_id;
				$sco->manifest = $value['manifest'];
				$sco->organization = $value['organization'];
				$sco->parent = $value['parent_root'];
				$sco->identifier = $value['identifier'];
				$sco->launch = $value['launch'];
				$sco->package_type	= $value['package_type'];
				$sco->scormtype = $value['scorm_type'];
				$sco->title = $value['title'];
				//$filesname = $value['file_name'];
				//@$filedir = substr($filesname, 0, -4);
				//$sco->file_name = $filedir;
				$sco->file_name = $value['file_title'];
				$sco->name = $value['file_name'];
				$sco->reference = $value['title'];
				$sco->summary = $value['title'];
				$sco->version = $value['version'];
				$sco->masteryscore = $value['masteryscore'];
				
				$sco->maxgrade = 100;
				$sco->grademethod = 1;
				$sco->whatgrade = 0;
				$sco->auto = "0";
			}
			return $sco;
		}
		return null;    
	}
	
	function scorm_get_tracks($item_id, $file_id, $userid, $attempt='', $user_training_id,$company_id) {
		
		//writelog("methode: scorm_get_tracks\nParameters: item_id:$item_id, file_id:$file_id, userid:$userid, attempt:$attempt, user_training_id:$user_training_id\n");
		
		if (empty($attempt)) {
			$attempt = scorm_get_last_attempt($item_id,$userid, $user_training_id,$company_id);
		}
		$link = dbConnect();
		$sqlquery = "select * from scorm_score_data where user_id = '{$userid}' AND file_id= '{$file_id}'  AND attempt= '{$attempt}' AND user_training_id='{$user_training_id}'";
		
		//writelog("SQL:".$sqlquery."\n");
		
		$tracks = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");

		$usertrack = new stdClass;
		$usertrack->userid = $userid;
		$usertrack->file_id = $file_id;
		
		if(mysqli_num_rows($tracks)>0){
			$usertrack->score_raw = '';
			$usertrack->status = '';
			$usertrack->total_time = '00:00:00';
			$usertrack->session_time = '00:00:00';
			$usertrack->timemodified = 0;
			//$tracks = json_decode(json_encode($tracks),true);
			while ($track = mysqli_fetch_array($tracks)) {
					$element = $track['element'];
					$value = stripslashes_safe($track['value']);
					$usertrack->{$element} = $value;
					switch ($element) {
							case 'cmi.core.lesson_status':
							case 'cmi.completion_status':
									if ($track['value'] == 'not attempted') {
											$track['value'] = 'notattempted';
									}
									$usertrack->status = $track['value'];
							break;
							case 'cmi.core.score.raw':
							case 'cmi.score.raw':
									//$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
									$usertrack->score_raw = $track['value'];
									break;
							case 'cmi.core.session_time':
							case 'cmi.session_time':
									$usertrack->session_time = $track['value'];
							break;
							case 'cmi.core.total_time':
							case 'cmi.total_time':
									$usertrack->total_time = $track['value'];
							break;
					}
					if (isset($track['timemodified']) && ($track['timemodified'] > $usertrack->timemodified)) {
							$usertrack->timemodified = $track['timemodified'];
					}
			}
			if (is_array($usertrack)) {
					ksort($usertrack);
			}
			return $usertrack;
		} else {
			return false;
		}
	}
	
	function scorm_get_last_attempt($item_id, $userid, $user_training_id,$company_id) {
		
		//writelog("methode: scorm_get_last_attempt\nParameters: item_id:$item_id, userid:$userid, user_training_id:$user_training_id\n");
		
		$link = dbConnect();
		$condition = "where user_id = ".$userid;
		if($item_id != ''){
			$condition = $condition . ' and item_id = '.$item_id;
		}
		
		if($user_training_id != ''){
			$condition = $condition . ' and user_training_id = '.$user_training_id;
		}
		$sqlquery = "select max(attempt) as a from scorm_score_data ". $condition;
		
		//writelog("SQL:".$sqlquery."\n");
		
		$runquery = mysqli_query($link,$sqlquery) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysqli_error()."\n\n");
				
		if (mysqli_num_rows($runquery)>0) {
			$lastattempt = @mysqli_fetch_object($runquery);
			if (empty($lastattempt->a)) {
				return '1';
			} else {
				return $lastattempt->a;
			}
		} else {
			return false;
		}
	}
	
	
	function addslashes_js($var) {
		
		//writelog("methode: addslashes_js\nParameters: var:$var\n");
		
		if (is_string($var)) {
			$var = str_replace('\\', '\\\\', $var);
			$var = str_replace(array('\'', '"', "\n", "\r", "\0"), array('\\\'', '\\"', '\\n', '\\r', '\\0'), $var);
			$var = str_replace('</', '<\/', $var);   // XHTML compliance
		} else if (is_array($var)) {
			$var = array_map('addslashes_js', $var);
		} else if (is_object($var)) {
			$a = get_object_vars($var);
			foreach ($a as $key=>$value) {
			  $a[$key] = addslashes_js($value);  //storing values with in an array....
			}
			$var = (object)$a;    //converting array to an object by type casting..
		}
		return $var;
	}
	
	function dbConnect() {
		
		//writelog("methode: dbConnect\n");
		if(in_array($_SERVER['HTTP_HOST'],array('localhost', '127.0.0.1', 'local.torchlms.com'))){
			$dbname = "lmspro";
			$dbhost = "localhost";
			$dbuser = "root";
			$dbpass = "";
		}else{
			$dbname = "db_ieeerctest";
			$dbhost = "192.168.100.123";
			$dbuser = "ieeerctest";
			$dbpass = "Ayryt6G3Z";
		}
	
		global $link;
		$link = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
		return $link;
	}
	
	
	function updatestandardLearningPlanCompletion($user_transcript_id, $item_id, $user_id,$company_id){
					$link = dbConnect();
					 $lpitemssql = "select ulpr.id, ulpr.user_id, ulpr.group_id, ulpr.grp_position_id, ulpr.due_date, ulpr.completion_date, ulpr.expire_date, ulpr.no_of_times, ulpr.transcript_id, ulpr.assigned_date, ulpr.start_date, ulpr.end_date, um.email_id, um.user_name,ulpr.lp_training_item_id,ulpr.blended_program_id, ulpr.learning_plan_id, ulpr.created_date from user_lp_requirements ulpr 
					JOIN user_master um ON um.user_id = ulpr.user_id
					where ulpr.user_id = '$user_id' and ulpr.item_id = '$item_id' and ulpr.is_active = 1 and ulpr.company_id = '$company_id'  and ulpr.grp_position_id!=0";
					$lpuserrequirementresult = mysqli_query($link, $lpitemssql) or writelog("Error in SQL:".$lpitemssql."\nError msg:".mysqli_error()."\n\n");
					//print($lpuserrequirementresult->num_rows); exit;
				if($lpuserrequirementresult->num_rows>0)
				   {
				        while($lpuserrequirementval    = mysqli_fetch_assoc($lpuserrequirementresult))
						{

					         $ulpr_id 				= $lpuserrequirementval['id'];
							$user_id 				= $lpuserrequirementval['user_id'];
							$group_id 				= $lpuserrequirementval['group_id'];
							$grp_position_id 		= $lpuserrequirementval['grp_position_id'];
							$due_date 				= $lpuserrequirementval['due_date'];
							$completion_date_ulpr 	= $lpuserrequirementval['completion_date'];
							$expire_date1 			= $lpuserrequirementval['expire_date'];
							$no_of_times 			= ($lpuserrequirementval['no_of_times']=='')?0:$lpuserrequirementval['no_of_times'];
							$learning_plan_id		= $lpuserrequirementval['learning_plan_id'];
							$transcript_id_lp 		= $lpuserrequirementval['transcript_id'];
							$assigned_date 			= $lpuserrequirementval['assigned_date'];
							$start_date 			= $lpuserrequirementval['start_date'];
							$end_date	 			= $lpuserrequirementval['end_date'];
							$email_id 				= $lpuserrequirementval['email_id'];
							$user_name 				= $lpuserrequirementval['user_name'];
							$lp_training_item_id 	= $lpuserrequirementval['lp_training_item_id'];
							$blended_program_id 	= $lpuserrequirementval['blended_program_id'];
							$created_date 			= $lpuserrequirementval['created_date'];
															        
							//$plan_data['start_date'] 	= $grp_start_date;
							//$plan_data['end_date']	= $grp_end_date;
							$plan_data['ulpr_expire']	= $expire_date1;
							$plan_data['ulpr_due']		= $due_date;
							$plan_data['start_date'] 	= $start_date;
							$plan_data['end_date']		= $end_date;
							$plan_data['group_id']		= $group_id;
							$plan_data['user_id']		= $user_id;
							$plan_data['company_id']	= $company_id;
							$plan_data['item_id']		= $item_id;
							$plan_data['assigned_date']	= $assigned_date;
							$plan_data['no_of_times']	= $no_of_times;
							$plan_data['user_transcript_id']= $user_transcript_id;
							$plan_data['learning_plan_id']  = $learning_plan_id;
							$learning_plan_setting='';
							$company_setting_sql = "SELECT * FROM company_settings WHERE company_id='{$company_id}'";
							$company_setting_sql = mysqli_query($link,$company_setting_sql) or writelog("Error in SQL:".$company_setting_sql."\nError msg:".mysqli_error()."\n\n");
							$compsett = mysqli_fetch_assoc($company_setting_sql);
							$learning_plan_setting 	= $compsett['learning_plan_setting'];
							
			                if($learning_plan_setting==0)
							{
			        
								list($start_date,$assigned_date, $due_date, $expiration_date, $completion_date, $transcript_id, $on_date) = getDueDateSetting($plan_data,$company_id) ;
							}
							else
							{
							
							list($start_date,$assigned_date, $due_date, $expiration_date, $completion_date, $transcript_id, $on_date) = getDueDateSettingLpwiseNew($plan_data,$company_id) ;
						
							}
					if($on_date!='over'){

					if(($expiration_date != $expire_date1)&&($completion_date !=$completion_date_ulpr)&&($on_date != 'updatecompletion')){				

						$where = "id='".$ulpr_id."' and company_id = '".$company_id."' and user_id ='".$user_id."'";
						 $sqlupdate="update user_lp_requirements set is_active='0',completion_date='$completion_date',transcript_id='$transcript_id' where $where";
						mysqli_query($link, $sqlupdate) or writelog("Error in SQL:".$sqlupdate."\nError msg:".mysqli_error()."\n\n");
						
						$training_type_id 	=	1;
						$blended_program_id =	0;
						$assessment_id 		=	0;
						$completion_date 	=	'0000-00-00';							
						$transcript_id		=  	0;
						$no_of_times 					= 	$no_of_times+1;
						               
						$insert_sql = "insert into user_lp_requirements(transcript_id, completion_date, expire_date, due_date, no_of_times, user_id, group_id, learning_plan_id, training_type_id, lp_training_item_id, item_id, blended_program_id, assessment_id, assigned_date, start_date, end_date, company_id, is_active, created_date, grp_position_id) 
						values('$transcript_id', '$completion_date', '$expiration_date', '$due_date', '$no_of_times', '$user_id', '$group_id', '$learning_plan_id', '$training_type_id', '$lp_training_item_id', '$item_id', '$blended_program_id', '0', '$assigned_date', '$start_date', '$end_date', '$company_id', '1', '$created_date', '$grp_position_id')";
						mysqli_query($link, $insert_sql) or writelog("Error in SQL:".$insert_sql."\nError msg:".mysqli_error()."\n\n");
								
							
					}else{
					
					$updateUserLPData=array();
					if($expiration_date!='0000-00-00'  && $expiration_date >date("Y-m-d") ){

					if($transcript_id_lp=='0' && $completion_date_ulpr=='0000-00-00'){
						
						$where = "id='".$ulpr_id."' and company_id = '".$company_id."' and item_id = '".$item_id."' and user_id = '".$user_id."'";
						$sqlupdate="update user_lp_requirements set end_date='$end_date',completion_date='$completion_date',transcript_id='$transcript_id',expire_date='$expiration_date',due_date='$due_date' where $where";
						mysqli_query($link, $sqlupdate) or writelog("Error in SQL:".$sqlupdate."\nError msg:".mysqli_error()."\n\n");
					}else{
			
						$where = "id='".$ulpr_id."' and company_id = '".$company_id."' and item_id = '".$item_id."' and user_id = '".$user_id."'";
						 $sqlupdate="update user_lp_requirements set completion_date='$completion_date',transcript_id='$transcript_id' where $where";
						mysqli_query($link, $sqlupdate) or writelog("Error in SQL:".$sqlupdate."\nError msg:".mysqli_error()."\n\n");
					}
					}else{
						
						$where = "id='".$ulpr_id."' and company_id = '".$company_id."' and item_id = '".$item_id."' and user_id = '".$user_id."'";
						 $sqlupdate="update user_lp_requirements set completion_date='$completion_date',transcript_id='$transcript_id' where $where";
						  mysqli_query($link, $sqlupdate) or writelog("Error in SQL:".$sqlupdate."\nError msg:".mysqli_error()."\n\n");
					}						 
					}		
					}else{ 
						$end_date			=  date('Y-m-d');
						//$is_active			= '0';
						$where = "id='".$ulpr_id."' and company_id = '".$company_id."' and item_id = '".$item_id."' and user_id = '".$user_id."'";
						 $sqlupdate="update user_lp_requirements set end_date='$end_date',is_active='$is_active' where $where";
						mysqli_query($link, $sqlupdate) or writelog("Error in SQL:".$sqlupdate."\nError msg:".mysqli_error()."\n\n");
					} 
					}
				}
					
		}
		
	 function getDueDateSettingLpwiseNew($plan_data,$company_id) 
	 {
		
		$link = dbConnect();
		$start_date 		=	$plan_data['start_date'];
		$grp_end_date 		= 	$plan_data['end_date'];
		$group_id 			= 	$plan_data['group_id'];
		$user_id			= 	$plan_data['user_id'];
		$company_id			= 	$plan_data['company_id'];
		$item_id			= 	$plan_data['item_id'];
		$assigned_date		= 	$plan_data['assigned_date'];
		$ulpr_expire_date	=   $plan_data['ulpr_expire'];
		$no_times			=	$plan_data['no_of_times'];
		$user_transcript_id	=	$plan_data['user_transcript_id'];
		$ulpr_due			=	$plan_data['ulpr_due'];
		$learning_plan_id	=	$plan_data['learning_plan_id'];
		$due_date 			= '0000-00-00';
		$expire_date 	    = '0000-00-00';
		$completion_date 	= '0000-00-00';
		$transcript_id 		= 0;
		$duedays 			= 0;
		$cycle				= 1;
		$on_date			='';
		$utsqldata=array();
		 $utsql = "SELECT * from user_transcript ut where ut.user_id='$user_id' and ut.item_id='$item_id' and is_active = 1 and ut.transcript_id=$user_transcript_id";
		  $utsqldatanew = mysqli_query($link, $utsql) or die(mysqli_error());
		 
			if($utsqldatanew->num_rows!=0)
			{    
			           $utsqldataa = mysqli_fetch_assoc($utsqldatanew);
					     $data=array();
				         $data['completion_date'] 		= $utsqldataa['completion_date'];
						 $data['transcript_id'] 		= $utsqldataa['transcript_id']; 
						 $utsqldata[]=$data;	
			} 
		
		$sql = "select item_id from item_master where item_id='".$item_id."' and company_id='".$company_id."' and is_active=1";
		$get_data = mysqli_query($link,$sql) or die(mysqli_error());

		if($get_data->num_rows==1)
		{
			
			$sql = "select * from lp_due_date_settings as l where l.learning_plan_id=$learning_plan_id and l.is_active=1 and l.company_id=$company_id";
			$getDueDate = mysqli_query($link,$sql) or die(mysqli_error());
			if($getDueDate->num_rows>0)
			{   
			    $getDueDate = mysqli_fetch_assoc($getDueDate);
			
				 $recurr_from		= $getDueDate['recurring_from_id'];
                if($getDueDate['due_date_type_id']==3 || $getDueDate['due_date_type_id']==2)
				{   
			    $duedays 		    = $getDueDate['due_days']!=''?$getDueDate['due_days']:0;
				}
				$trainingstart_date = isset($getDueDate['start_date'])?$getDueDate['start_date']:'0000-00-00';
				
				$recurr_days		= $getDueDate['renewal_days'];
			

			   $expire_date_check='0000-00-00';
			   $end_type =$getDueDate['end_type_id'];
				

				if($getDueDate['due_date_type_id']==3)
				{      
						$completion_date 		= $utsqldata[0]['completion_date'];
						$transcript_id 		    = $utsqldata[0]['transcript_id']; 
					    $add_day=1;
						
					    $expire_date_check = date ('Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $add_day day" ) );
					
				  if($expire_date_check==date ('Y-m-d'))
					  { 
					
						if($completion_date!='0000-00-00' && $transcript_id!=0){
						   
							 
						   if($end_type!=0 && $end_type!='')
							{ 
								$assigned_date=$ulpr_expire_date;
								
								if($recurr_from==3 )
								{
								  
								   if($getDueDate['end_type_id']==1)
								   {    if($grp_end_date!='0000-00-00'){
											  if(strtotime($grp_end_date) >= strtotime(date('Y-m-d'))){
											   
												
												   $due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
												   $expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
											 }else{
													$on_date='over';
											 }
										 }else{
												   $due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
												   $expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
										 }
								   }else if($getDueDate['end_type_id']==2)
									{ 
										$end_date =$getDueDate['end_days'];
										if(strtotime($end_date) >= strtotime(date('Y-m-d'))){ 
											
												$due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
												$expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
										}else{
										
												$on_date='over';
										}
									}
									else
									{
										   if($no_times < $getDueDate['end_days'] ){
										  
												$due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
												$expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
											}else{
											
												$on_date='over';
											}
									}							
								}
							else{

								 if($getDueDate['end_type_id']==1)
								{
								  if($grp_end_date!='0000-00-00'){
									  if(strtotime($grp_end_date) >= strtotime(date('Y-m-d'))){
										   
												$due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
												$expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
										 }else{
												$on_date='over';
										 }
									}else{
												$due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
												$expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );							
									}
								}
								else if($getDueDate['end_type_id']==2)
								{
									$end_date =$getDueDate['end_days'];
									if(strtotime($end_date) >= strtotime(date('Y-m-d'))){
										$due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
										$expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
									}else{
										$on_date='over';
									}
								}
								else
								{
									if($no_times < $getDueDate['end_days'] ){
										$due_date = date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $duedays day" ) );
										$expire_date =date ( 'Y-m-d', strtotime ( date ( "Y-m-d", strtotime ( $ulpr_expire_date ) ) . "+ $recurr_days day" ) );
									}
									else{
										$on_date='over';
									}
								}
								
						  }
						}
						
					  }
					  }else{
						 
						  $assigned_date='0000-00-00';
						  $due_date		='0000-00-00';
						  $expire_date	='0000-00-00';
						  $start_date	='0000-00-00';
							if($utsqldata){
							   if($recurr_from==3 )
								{
									  if($no_times==1 && $ulpr_expire_date=='0000-00-00'){
										 $due_date 		= $ulpr_due;
									   if($getDueDate['end_type_id']==1)
									   {   
											if($grp_end_date!='0000-00-00'){
												if(strtotime($grp_end_date) >= strtotime(date('Y-m-d'))){
											   
												 $expire_date 	= date ( "Y-m-d", strtotime ( date ( "Y-m-d", strtotime ($completion_date) ) . "+ $recurr_days day" ) );
																		
												 $on_date='updatecompletion';
											 }else{
												$on_date='over';
											 }
										 }else{
											$expire_date 	= date ( "Y-m-d", strtotime ( date ( "Y-m-d", strtotime ($completion_date) ) . "+ $recurr_days day" ) );
											$on_date='updatecompletion';
										 }
									   }else if($getDueDate['end_type_id']==2)
										{ 
											$end_date =$getDueDate['end_days'];
											if(strtotime($end_date) >= strtotime(date('Y-m-d'))){ 
												   $expire_date 	= date ( "Y-m-d", strtotime ( date ( "Y-m-d", strtotime ($completion_date) ) . "+ $recurr_days day" ) );										
												   $on_date='updatecompletion';
											}else{
											
											   $on_date='over';
											}
										}
										else
										{
											   if($no_times < $getDueDate['end_days'] ){
											   $expire_date 	= date ( "Y-m-d", strtotime ( date ( "Y-m-d", strtotime ($completion_date) ) . "+ $recurr_days day" ) );
												
												 $on_date='updatecompletion';
												}else{
												
												 $on_date='over';
												}
										}
										$completion_date 	= $utsqldata[0]['completion_date'];
										$transcript_id 		= $utsqldata[0]['transcript_id'];
									}else{
										$completion_date 	= $utsqldata[0]['completion_date'];
										$transcript_id 		= $utsqldata[0]['transcript_id'];
										$on_date='updatecompletion';
									}
								  //						
								}else{
								   $completion_date 	= $utsqldata[0]['completion_date'];
								   $transcript_id 		= $utsqldata[0]['transcript_id'];
								   $on_date='updatecompletion';
								}
							}
						  else{
								$completion_date='0000-00-00'; $transcript_id=0;
								$on_date='updatecompletion';
						  } 
                   } 				  
				   }else{
							$start_date	='0000-00-00'; 
							$assigned_date='0000-00-00';
							$due_date		='0000-00-00';
							$expire_date	='0000-00-00';
						if($utsqldata){
							$completion_date 	= $utsqldata[0]['completion_date'];
							$transcript_id 		= $utsqldata[0]['transcript_id'];
						}else{
							$completion_date='0000-00-00'; $transcript_id=0;
						}	
							$on_date='updatecompletion';
				 }
				 
			}
			
			
		}
		//echo "$start_date,$assigned_date, $due_date, $expire_date, $completion_date, $transcript_id, $on_date"; exit;
		return array($start_date,$assigned_date, $due_date, $expire_date, $completion_date, $transcript_id, $on_date);
		////////
	}
	
	function updateLearningPlanCompletion($user_transcript_id, $item_id, $user_id, $company_id)
	{	
	
		$link = dbConnect();

		$lpitemssql = "select im.training_type_id, im.item_id, im.item_name, dds.due_date_type_id, dds.no_of_days, dds.recurring_type_id, dds.recurrence, dds.end_type_id from item_master im 
		join due_date_settings dds on dds.training_id = im.item_id and dds.training_type_id = im.training_type_id where im.is_active = 1 and im.company_id = '$company_id' and im.item_id = '$item_id'";
		$lpitemsresult = mysqli_query($link,$lpitemssql) or die(mysqli_error());
		
		if(mysqli_num_rows($lpitemsresult)>0)
		{
			
			
			while($lpitemsval = mysqli_fetch_array($lpitemsresult))
			{
				$training_type_id 		= $lpitemsval['training_type_id'];
				$item_id 				= $lpitemsval['item_id'];
				$item_name 				= $lpitemsval['item_name'];
				$due_date_type_id 		= $lpitemsval['due_date_type_id'];
				$no_of_days 			= $lpitemsval['no_of_days'];
				$recurring_type_id 		= $lpitemsval['recurring_type_id'];
				$recurrence 			= $lpitemsval['recurrence'];
				$end_type_id 			= $lpitemsval['end_type_id'];
				
				$recurrencej 		= json_decode($recurrence,true);
				$recur_year        	= $recurrencej['year']!=''?$recurrencej['year']:0;
				$recur_month       	= $recurrencej['months']!=''?$recurrencej['months']:0;
				$recur_day         	= $recurrencej['day']!=''?$recurrencej['day']:0;
				$daysexp 			= $recurrencej['days']!=''?$recurrencej['days']:0;
				$get_days    		= $recur_year*365+$recur_month*30+$recur_day;
				$recorancedays		= $get_days+$daysexp;
				
				$lpuserrequirementsql = "select ulpr.id, ulpr.user_id, ulpr.group_id, ulpr.grp_position_id, ulpr.due_date, ulpr.completion_date, ulpr.expire_date, ulpr.no_of_times, ulpr.transcript_id, ulpr.assigned_date, ulpr.start_date, ulpr.end_date, um.email_id, um.user_name,ulpr.lp_training_item_id,ulpr.blended_program_id, ulpr.learning_plan_id, ulpr.created_date from user_lp_requirements ulpr 
				JOIN user_master um ON um.user_id = ulpr.user_id
				where ulpr.user_id = '$user_id' and ulpr.item_id = '$item_id' and ulpr.is_active = 1 and ulpr.company_id = '$company_id'";
				$lpuserrequirementresult = mysqli_query($link,$lpuserrequirementsql) or die(mysqli_error());
		
				if(mysqli_num_rows($lpuserrequirementresult)>0)
				{
						
					while($lpuserrequirementval = mysqli_fetch_array($lpuserrequirementresult))
					{

						$user_lp_req_id 		= $lpuserrequirementval['id'];
						$user_id 				= $lpuserrequirementval['user_id'];
						$group_id 				= $lpuserrequirementval['group_id'];
						$grp_position_id 		= $lpuserrequirementval['grp_position_id'];
						$due_date 				= $lpuserrequirementval['due_date'];
						$completion_date1 		= $lpuserrequirementval['completion_date'];
						$expire_date1 			= $lpuserrequirementval['expire_date'];
						$no_of_times 			= ($lpuserrequirementval['no_of_times']=='')?0:$lpuserrequirementval['no_of_times'];
						$learning_plan_id		= $lpuserrequirementval['learning_plan_id'];
						$transcript_id 			= $lpuserrequirementval['transcript_id'];
						$assigned_date 			= $lpuserrequirementval['assigned_date'];
						$start_date 			= $lpuserrequirementval['start_date'];
						$end_date	 			= $lpuserrequirementval['end_date'];
						$email_id 				= $lpuserrequirementval['email_id'];
						$user_name 				= $lpuserrequirementval['user_name'];
						$lp_training_item_id 	= $lpuserrequirementval['lp_training_item_id'];
						$blended_program_id 	= $lpuserrequirementval['blended_program_id'];
						$created_date 			= $lpuserrequirementval['created_date'];

						$requirementcompletionsql = "select ut.transcript_id, ut.item_id, ut.completion_date, ut.user_training_id from user_transcript ut where ut.company_id = '$company_id' and ut.item_id = '$item_id' and ut.user_id = '$user_id' and ut.is_active = 1 and ut.transcript_id = '$user_transcript_id' group by ut.user_id,ut.item_id ";
						$requirementcompletionresult = mysqli_query($link,$requirementcompletionsql) or die(mysqli_error());
						if(mysqli_num_rows($requirementcompletionresult)>0)
						{

							$ut_completion_date 	= '';
							while($requirementcompletionval = mysqli_fetch_array($requirementcompletionresult))
							{	
								$ut_completion_date 	= $requirementcompletionval['completion_date'];
							}
							$next_expiration_date	= '0000-00-00';
							
							if($recorancedays > 0){
								$next_expiration_date	= date ('Y-m-d', strtotime(date("Y-m-d", strtotime($ut_completion_date)). "+$recorancedays day" ));
							}	
							
							if($lpuserrequirementval['completion_date'] != $ut_completion_date && $lpuserrequirementval['completion_date'] !='0000-00-00'){
											
								$where = "id = '$user_lp_req_id' and user_id = '$user_id' and item_id = '$item_id' and learning_plan_id = '$learning_plan_id' and  grp_position_id = '$grp_position_id'";
								$sql="update user_lp_requirements set is_active=0 where $where";
								mysqli_query($link,$sql) or die(mysqli_error());
								
								$no_of_times = $no_of_times+1;
								
								$insert_sql = "insert into user_lp_requirements(transcript_id, completion_date, expire_date, due_date, no_of_times, user_id, group_id, learning_plan_id, training_type_id, lp_training_item_id, item_id, blended_program_id, assessment_id, assigned_date, start_date, end_date, company_id, is_active, created_date, grp_position_id) 
								values('$user_transcript_id', '$ut_completion_date', '$next_expiration_date', '$due_date', '$no_of_times', '$user_id', '$group_id', '$learning_plan_id', '$training_type_id', '$lp_training_item_id', '$item_id', '$blended_program_id', '', '$assigned_date', '$start_date', '$end_date', '$company_id', '1', '$created_date', '$grp_position_id')";
								mysqli_query($link,$insert_sql) or die(mysqli_error());	
								
							}else{
							
								$where = "id = '$user_lp_req_id' and user_id = '$user_id' and item_id = '$item_id' and learning_plan_id = '$learning_plan_id' and  grp_position_id = '$grp_position_id'";
								
								$sql="update user_lp_requirements set is_active=1, expire_date='$next_expiration_date',completion_date='$ut_completion_date',transcript_id='$user_transcript_id' where $where";
								mysqli_query($link,$sql) or die(mysqli_error());
							
							}
							
						}
					}
				}
			}
		}
	}
?>
