<?php
	date_default_timezone_set('America/Chicago');
	//Log default
	function writelog($text){
		$myfile = fopen("../temp/aicc_logs.log", "a") or die("Unable to open file!");
		fwrite($myfile, $text);
		fclose($myfile);
	}
	
	$sequence_array = array();
	$correctAnsCount = 0;
	$incorrectAnsCount = 0;

	$command = $_REQUEST['command'];
	$session_id = $_REQUEST['session_id'];

	if(isset($_REQUEST['aicc_data'])){
		$aiccdata = $_REQUEST['aicc_data'];
	}else  if(isset($_REQUEST['AICC_Data'])){
		$aiccdata = $_REQUEST['AICC_Data'];
	}

	$scormsession =  $userdata = new stdClass;

	$strvariables 	= base64_decode($session_id);
	$variablearray = explode('_#_', $strvariables); 
	
	$text = "\n------------\nReview Course--------Request Log Time ".date('d-m-Y H:i:s')."\n------------\n";
	$text .= "Requested Data: ".print_r($_REQUEST,true)."\n";
	$text .= "Decode reqdata: ".print_r($variablearray,true)."\n";
	writelog($text);
	
	//print_r($variablearray);
	$user_id			= $variablearray[0];
	$user_name 			= $variablearray[1];
	$item_id 			= $variablearray[2];
	$file_id 	 		= $variablearray[3];
	$company_id 		= $variablearray[4];
	$preview	 		= $variablearray[5];
	$user_training_id	= $variablearray[6];
	$attempt			= isset($variablearray[7]) ? $variablearray[7] : 1;
	
	writelog("Session ID: ".print_r($variablearray,true). "\n\n");
	
	$sco = getScorm($item_id, $user_id, $file_id, $company_id );
	if (!empty($command)) {
    $command = strtolower($command);
    $mode = 'normal';
    $status = 'Not Initialized';
    //$attempt = 1;
       switch ($command) {
            case 'getparam':
					
			        if ($usertrack=scorm_get_tracks($item_id, $file_id, $user_id, $attempt, $user_training_id,$company_id)) {
                        $userdata = $usertrack;
                    } else {
                        $userdata->status = '';
                        $userdata->score_raw = '';
                    }
                    $userdata->student_id = $company_id."".$user_id;
                    $userdata->student_name = $user_name;
                    $userdata->mode = 'normal';
                    if ($userdata->mode == 'normal') {
                        $userdata->credit = 'credit';
                    } else {
                        $userdata->credit = 'no-credit';
                    }
					//print_r($sco);echo "sss:";
					$userdata->course_id = $sco->identifier;
					$userdata->datafromlms = isset($sco->datafromlms)?$sco->datafromlms:'';
					$userdata->masteryscore = isset($sco->masteryscore) && is_numeric($sco->masteryscore)?trim($sco->masteryscore):'';
					$userdata->max_time_allowed = '';
					$userdata->time_limit_action = '';

					echo "error=0\r\nerror_text=Successful\r\naicc_data=";
					echo "[Core]\r\n";
					echo 'Student_ID='.$userdata->student_id."\r\n";
					echo 'Student_Name='.$userdata->student_name."\r\n";
					if (isset($userdata->{'cmi.core.lesson_location'})) {
						echo 'Lesson_Location='.$userdata->{'cmi.core.lesson_location'}."\r\n";
					} else {
						echo 'Lesson_Location='."\r\n";
					}
					echo 'Credit='.$userdata->credit."\r\n";
					if (isset($userdata->status)) {
						if ($userdata->status == '') {
							$userdata->entry = ', ab-initio';
						} else {
							if (isset($userdata->{'cmi.core.exit'}) && ($userdata->{'cmi.core.exit'} == 'suspend')) {
								$userdata->entry = ', resume';
							} else {
								$userdata->entry = '';
							}
						}
					}
					if (isset($userdata->status)) {
						echo 'Lesson_Status='.$userdata->status.$userdata->entry."\r\n";
						$scormsession->scorm_lessonstatus = $userdata->status;
					} else {
						echo 'Lesson_Status=not attempted'.$userdata->entry."\r\n";
						$scormsession->scorm_lessonstatus = 'not attempted';
					}
					if (isset($userdata->score_raw)) {
						/* $max = '';
						$min = '';
						if (isset($userdata->{'cmi.core.score.max'}) && !empty($userdata->{'cmi.core.score.max'})) {
							$max = ', '.$userdata->{'cmi.core.score.max'};
							if (isset($userdata->{'cmi.core.score.min'}) && !empty($userdata->{'cmi.core.score.min'})) {
								$min = ', '.$userdata->{'cmi.core.score.min'};
							}
						}
						echo 'Score='.$userdata->score_raw.$max.$min."\r\n"; */
						echo 'Score='.$userdata->score_raw."\r\n";
					} else {
						echo 'Score='."\r\n";
					}
					if (isset($userdata->total_time)) {
						echo 'Time='.$userdata->total_time."\r\n";
					} else {
						echo 'Time='.'00:00:00'."\r\n";
					}
					echo 'Lesson_Mode='.$userdata->mode."\r\n";
					if (isset($userdata->{'cmi.suspend_data'})) {
						echo "[Core_Lesson]\r\n".rawurldecode($userdata->{'cmi.suspend_data'})."\r\n";
					} else {
						echo "[Core_Lesson]\r\n";
					}
					echo "[Core_Vendor]\r\n".$userdata->datafromlms."\r\n";
					echo "[Evaluation]\r\nCourse_ID = {".$userdata->course_id."}\r\n";
					echo "[Student_Data]\r\n";
					echo 'Mastery_Score='.$sco->masteryscore."\r\n";
					echo 'Max_Time_Allowed='.$userdata->max_time_allowed."\r\n";
					echo 'Time_Limit_Action='.$userdata->time_limit_action."\r\n";
            break;
			case 'putparam':
                    echo "error=0\r\nerror_text=Successful\r\n";
            break;
            case 'exitau':
                   echo "error=0\r\nerror_text=Successful\r\n";
            break;
            default:
                echo "error=1\r\nerror_text=Invalid Command\r\n";
            break;

        }
} else {
    if (empty($command)) {
        echo "error=1\r\nerror_text=Invalid Command\r\n";
    } else {
        echo "error=3\r\nerror_text=Invalid Session ID\r\n";
    }
}

	
	function scorm_grade_item_update($item_id, $usertrack=NULL,$file_id,$userid, $user_training_id, $company_id) {
		
		//writelog("methode: scorm_grade_item_update\nParameters: item_id:$item_id, usertrack:$usertrack, file_id:$file_id, userid:$userid, user_training_id:$user_training_id\n");
		
		$link = dbConnect();

		$status = 1;
		$totalQuestions = 100;
		
		$item_id = $item_id;
		$user_id = $userid;
		$status1 = $usertrack->statusscore;
		$value_str = $usertrack->score_raw;
		$value_arr = explode(",", $value_str);
		if(isset($value_arr[0]) && !isset($value_arr[1])){
			$value = (int)$value_arr[0];
		}elseif(isset($value_arr[0]) && isset($value_arr[1])){
			$value = (int)(($value_arr[0]/$value_arr[1])*100);
		}
		//writelog("score---$value--\n");
		if($value > 0){
			$maxScore = 100;
		
			$passscore = getpasscore($file_id, $item_id,$company_id );

			if($passscore > 0 || $usertrack->ispassfail == 1){
				if($value >= $passscore){
					$status = 1;
				}else{
					if($status1 != ''){
						$statuspass = array('pass','passed','completed'); 
						if($usertrack->ispassfail == 1 && in_array(strtolower($status1), $statuspass)){
							$status = 1;
						}else if($usertrack->ispassfail == 1){
							$status = 0;
						}else{
							$status = 2;
						}
					}else{
						$status = 2;
					}
				}
			}else{
				$status = 2;
			}
			
			if($company_id == 51 && $value >= 80){
				$status = 1;
			}


			$sqlcheck = "select * from sco_score_data where user_id = '{$user_id}' AND item_id = '{$item_id}' AND user_training_id = '{$user_training_id}' AND company_id = '{$company_id}'";
			//writelog("SELECT SQL:".$sqlcheck."\n");
			$runquerycheck = mysql_query($sqlcheck,$link) or writelog("Error SQL:".$sqlcheck."\nError msg:".mysql_error()."\n\n");
			
			if(mysql_num_rows($runquerycheck)>0){
				if($user_training_id!="" && $user_id!="" && $item_id!=""){
					$sqldataInsert = "update sco_score_data set correct_answer = '".$value."', status ='".$status."' where user_training_id= '".$user_training_id."' AND user_id = '".$user_id."' AND item_id = '".$item_id."'";
					
					//writelog("update SQL:".$sqldataInsert."\n");
					$runqueryScore = mysql_query($sqldataInsert,$link) or writelog("Error SQL:".$sqldataInsert."\nError msg:".mysql_error()."\n\n");	
				}
			}else{
				$sqldataInsert = "insert into sco_score_data( scorm_data_id, user_training_id, user_id, item_id, max_score, total_questions, correct_answer, status, company_id) VALUES(NULL, '".$user_training_id."', '".$user_id."', '".$item_id."', '".$maxScore."', '".$totalQuestions."', '".$value."', '".$status."', '".$company_id."')";
				//writelog("INSERT SQL:".$sqldataInsert."\n");
				$runqueryScore = mysql_query($sqldataInsert,$link) or writelog("Error SQL:".$sqldataInsert."\nError msg:".mysql_error()."\n\n");
			}
		}
		return $value;
	}
	
	function getpasscore($file_id, $item_id,$company_id){
		
		//writelog("methode: getpasscore\nParameters: file_id:$file_id, item_id:$item_id\n");
		
		$link = dbConnect();
		$sqlquery = "select masteryscore, company_id from uploaded_files where files_id = '{$file_id}'"; 
		//writelog("SQL:".$sqlquery."\n");
		$data = mysql_query($sqlquery,$link) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysql_error()."\n\n");
		
		if(mysql_num_rows($data)>0){
			while($value=mysql_fetch_array($data)) {
				$masteryscore = $value['masteryscore'];
				$company_id = $value['company_id'];
			}
			if($company_id == '51'){
				$masteryscore = 80;
			}
			return $masteryscore;
		}
		return $masteryscore;
	}
	
	function stripslashes_safe($mixed) {
		
		//writelog("methode: stripslashes_safe\nParameters: mixed: $mixed\n");
		
		// there is no need to remove slashes from int, float and bool types
		if (empty($mixed)) {
			//nothing to do...
		} else if (is_string($mixed)) {
			if (ini_get_bool('magic_quotes_sybase')) { //only unescape single quotes
				$mixed = str_replace("''", "'", $mixed);
			} else { //the rest, simple and double quotes and backslashes
				$mixed = str_replace("\\'", "'", $mixed);
				$mixed = str_replace('\\"', '"', $mixed);
				$mixed = str_replace('\\\\', '\\', $mixed);
			}
		} else if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = stripslashes_safe($value);
			}
		} else if (is_object($mixed)) {
			$vars = get_object_vars($mixed);
			foreach ($vars as $key => $value) {
				$mixed->$key = stripslashes_safe($value);
			}
		}
		return $mixed;
	}

	function ini_get_bool($ini_get_arg) {
		
		//writelog("methode: ini_get_bool\nParameters: ini_get_arg: $ini_get_arg\n");
		
		$temp = ini_get($ini_get_arg);
		if ($temp == '1' or strtolower($temp) == 'on') {
			return true;
		}
		return false;
	}
	
	function getScorm($item_id, $userid, $file_id, $company_id ){
		
		//writelog("methode: getScorm\nParameters: item_id: $item_id, userid: $userid, file_id: $file_id\n");
		
		$link = dbConnect();
		$sqlquery = "select * from uploaded_files where files_id = $file_id"; 
		//writelog("SQL:".$sqlquery."\n");
		
		$data = mysql_query($sqlquery,$link) or writelog("Error in SQL:".$sqlquerysqlquery."\nError msg:".mysql_error()."\n\n");
		//print_r($data);
		$sco = new stdClass;
		if(mysql_num_rows($data)>0){
			//$data = json_decode(json_encode($data),true);
			while($value=mysql_fetch_array($data)) {
				$sco->item_id = $item_id;
				$sco->course = $item_id;
				$sco->id = $value['files_id'];
				$sco->scorm = $item_id;
				$sco->manifest = $value['manifest'];
				$sco->organization = $value['organization'];
				$sco->parent = $value['parent_root'];
				$sco->identifier = $value['identifier'];
				$sco->launch = $value['launch'];
				$sco->package_type	= $value['package_type'];
				$sco->scormtype = $value['scorm_type'];
				$sco->title = $value['title'];
				$filesname = $value['file_name'];
				@$filedir = substr($filesname, 0, -4);
				$sco->file_name = $filedir;
				$sco->name = $value['file_name'];
				$sco->reference = $value['title'];
				$sco->summary = $value['title'];
				$sco->version = $value['version'];
				$sco->masteryscore = $value['masteryscore'];
				
				$sco->maxgrade = 100;
				$sco->grademethod = 1;
				$sco->whatgrade = 0;
				$sco->auto = "0";
			}
			return $sco;
		}
		return null;    
	}
	
	function scorm_get_tracks($item_id, $file_id, $userid, $attempt='', $user_training_id,$company_id) {
		
		//writelog("methode: scorm_get_tracks\nParameters: item_id:$item_id, file_id:$file_id, userid:$userid, attempt:$attempt, user_training_id:$user_training_id\n");
		
		if (empty($attempt)) {
			$attempt = scorm_get_last_attempt($item_id,$userid, $user_training_id,$company_id);
		}
		$link = dbConnect();
		$sqlquery = "select * from scorm_score_data where user_id = '{$userid}' AND file_id= '{$file_id}'  AND attempt= '{$attempt}' AND user_training_id='{$user_training_id}'";
		
		//writelog("SQL:".$sqlquery."\n");
		
		$tracks = mysql_query($sqlquery,$link) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysql_error()."\n\n");

		$usertrack = new stdClass;
		$usertrack->userid = $userid;
		$usertrack->file_id = $file_id;
		
		if(mysql_num_rows($tracks)>0){
			$usertrack->score_raw = '';
			$usertrack->status = '';
			$usertrack->total_time = '00:00:00';
			$usertrack->session_time = '00:00:00';
			$usertrack->timemodified = 0;
			//$tracks = json_decode(json_encode($tracks),true);
			while ($track = mysql_fetch_array($tracks)) {
					$element = $track['element'];
					$value = stripslashes_safe($track['value']);
					$usertrack->{$element} = $value;
					switch ($element) {
							case 'cmi.core.lesson_status':
							case 'cmi.completion_status':
									if ($track['value'] == 'not attempted') {
											$track['value'] = 'notattempted';
									}
									$usertrack->status = $track['value'];
							break;
							case 'cmi.core.score.raw':
							case 'cmi.score.raw':
									//$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
									$usertrack->score_raw = $track['value'];
									break;
							case 'cmi.core.session_time':
							case 'cmi.session_time':
									$usertrack->session_time = $track['value'];
							break;
							case 'cmi.core.total_time':
							case 'cmi.total_time':
									$usertrack->total_time = $track['value'];
							break;
					}
					if (isset($track['timemodified']) && ($track['timemodified'] > $usertrack->timemodified)) {
							$usertrack->timemodified = $track['timemodified'];
					}
			}
			if (is_array($usertrack)) {
					ksort($usertrack);
			}
			return $usertrack;
		} else {
			return false;
		}
	}
	
	function scorm_get_last_attempt($item_id, $userid, $user_training_id,$company_id) {
		
		//writelog("methode: scorm_get_last_attempt\nParameters: item_id:$item_id, userid:$userid, user_training_id:$user_training_id\n");
		
		$link = dbConnect();
		$condition = "where user_id = ".$userid;
		if($item_id != ''){
			$condition = $condition . ' and item_id = '.$item_id;
		}
		
		if($user_training_id != ''){
			$condition = $condition . ' and user_training_id = '.$user_training_id;
		}
		$sqlquery = "select max(attempt) as a from scorm_score_data ". $condition;
		
		//writelog("SQL:".$sqlquery."\n");
		
		$runquery = mysql_query($sqlquery,$link) or writelog("Error in SQL:".$sqlquery."\nError msg:".mysql_error()."\n\n");
				
		if (mysql_num_rows($runquery)>0) {
			$lastattempt = @mysql_fetch_object($runquery);
			if (empty($lastattempt->a)) {
				return '1';
			} else {
				return $lastattempt->a;
			}
		} else {
			return false;
		}
	}
	
	
	function addslashes_js($var) {
		
		//writelog("methode: addslashes_js\nParameters: var:$var\n");
		
		if (is_string($var)) {
			$var = str_replace('\\', '\\\\', $var);
			$var = str_replace(array('\'', '"', "\n", "\r", "\0"), array('\\\'', '\\"', '\\n', '\\r', '\\0'), $var);
			$var = str_replace('</', '<\/', $var);   // XHTML compliance
		} else if (is_array($var)) {
			$var = array_map('addslashes_js', $var);
		} else if (is_object($var)) {
			$a = get_object_vars($var);
			foreach ($a as $key=>$value) {
			  $a[$key] = addslashes_js($value);  //storing values with in an array....
			}
			$var = (object)$a;    //converting array to an object by type casting..
		}
		return $var;
	}
	
	function dbConnect() {
		
		//writelog("methode: dbConnect\n");
		if(in_array($_SERVER['HTTP_HOST'],array('localhost', '127.0.0.1', 'local.torchlms.com'))){
			$dbname = "lmspro";
			$dbhost = "localhost";
			$dbuser = "root";
			$dbpass = "";
		}else{
			$dbname = "db_torchpro";
			$dbhost = "192.168.100.123";
			$dbuser = "torchproadmin";
			$dbpass = "0oy6UF2Z8";
		}
		global $link;
		$link = mysql_connect($dbhost,$dbuser,$dbpass);
		mysql_select_db($dbname,$link);
		return $link;
	}
	
	function updateLearningPlanCompletion($user_transcript_id, $item_id, $user_id, $company_id)
	{	
		//writelog("Methode: updateLearningPlanCompletion\nParameters: user_transcript_id:$user_transcript_id, item_id:$item_id, user_id:$user_id, company_id:$company_id\n");
		
		$link = dbConnect();

		$lpitemssql = "select im.training_type_id, im.item_id, im.item_name, dds.due_date_type_id, dds.no_of_days, dds.recurring_type_id, dds.recurrence, dds.end_type_id from item_master im join due_date_settings dds on dds.training_id = im.item_id and dds.training_type_id = im.training_type_id where im.is_active = 1 and im.company_id = '$company_id' and im.item_id = '$item_id'";
		//$lpitemsresult =$db->fetchAll($lpitemssql);
		
		//writelog("SQL:".$lpitemssql."\n");
		$lpitemsresult = mysql_query($lpitemssql, $link) or die(mysql_error());
		
		if(mysql_num_rows($lpitemsresult)>0)
		{
			while($lpitemsval = mysql_fetch_array($lpitemsresult))
			{
				$training_type_id 		= $lpitemsval['training_type_id'];
				$item_id 				= $lpitemsval['item_id'];
				$item_name 				= $lpitemsval['item_name'];
				$due_date_type_id 		= $lpitemsval['due_date_type_id'];
				$no_of_days 			= $lpitemsval['no_of_days'];
				$recurring_type_id 		= $lpitemsval['recurring_type_id'];
				$recurrence 			= $lpitemsval['recurrence'];
				$end_type_id 			= $lpitemsval['end_type_id'];
				
				$recurrencej 		= json_decode($recurrence,true);
				$recur_year        	= $recurrencej['year']!=''?$recurrencej['year']:0;
				$recur_month       	= $recurrencej['months']!=''?$recurrencej['months']:0;
				$recur_day         	= $recurrencej['day']!=''?$recurrencej['day']:0;
				$daysexp 			= $recurrencej['days']!=''?$recurrencej['days']:0;
				$get_days    		= $recur_year*365+$recur_month*30+$recur_day;
				$recorancedays		= $get_days+$daysexp;
		
				$lpuserrequirementsql = "select ulpr.id, ulpr.user_id, ulpr.group_id, ulpr.grp_position_id, ulpr.due_date, ulpr.completion_date, ulpr.expire_date, ulpr.no_of_times, ulpr.transcript_id, ulpr.assigned_date, ulpr.start_date, ulpr.end_date, um.email_id, um.user_name,ulpr.lp_training_item_id,ulpr.blended_program_id, ulpr.learning_plan_id, ulpr.created_date from user_lp_requirements ulpr 
				JOIN user_master um ON um.user_id = ulpr.user_id
				where ulpr.user_id = '$user_id' and ulpr.item_id = '$item_id' and ulpr.is_active = 1 and ulpr.company_id = '$company_id'";
				//$lpuserrequirementresult =$db->fetchAll($lpuserrequirementsql);
				//writelog("SQL:".$lpuserrequirementsql."\n");
				$lpuserrequirementresult = mysql_query($lpuserrequirementsql, $link) or die(mysql_error());
		
				if(mysql_num_rows($lpuserrequirementresult)>0)
				{
					while($lpuserrequirementval = mysql_fetch_array($lpuserrequirementresult))
					{
					//foreach($lpuserrequirementresult as $lpuserrequirementval){
						//print_r($lpuserrequirementval);
						$user_lp_req_id 		= $lpuserrequirementval['id'];
						$user_id 				= $lpuserrequirementval['user_id'];
						$group_id 				= $lpuserrequirementval['group_id'];
						$grp_position_id 		= $lpuserrequirementval['grp_position_id'];
						$due_date 				= $lpuserrequirementval['due_date'];
						$completion_date 		= $lpuserrequirementval['completion_date'];
						$expire_date 			= $lpuserrequirementval['expire_date'];
						$no_of_times 			= ($lpuserrequirementval['no_of_times']=='')?0:$lpuserrequirementval['no_of_times'];
						$learning_plan_id		= $lpuserrequirementval['learning_plan_id'];
						$transcript_id 			= $lpuserrequirementval['transcript_id'];
						$assigned_date 			= $lpuserrequirementval['assigned_date'];
						$start_date 			= $lpuserrequirementval['start_date'];
						$end_date	 			= $lpuserrequirementval['end_date'];
						$email_id 				= $lpuserrequirementval['email_id'];
						$user_name 				= $lpuserrequirementval['user_name'];
						$lp_training_item_id 	= $lpuserrequirementval['lp_training_item_id'];
						$blended_program_id 	= $lpuserrequirementval['blended_program_id'];
						$created_date 			= $lpuserrequirementval['created_date'];
						
						$requirementcompletionsql = "select ut.transcript_id, ut.item_id, ut.completion_date, ut.user_training_id from user_transcript ut where ut.company_id = '$company_id' and ut.item_id = '$item_id' and ut.user_id = '$user_id' and ut.is_active = 1 and ut.transcript_id = '$user_transcript_id' group by ut.user_id,ut.item_id ";
						//$requirementcompletionresult =$db->fetchAll($requirementcompletionsql);
						$requirementcompletionresult = mysql_query($requirementcompletionsql, $link) or die(mysql_error());
						if(mysql_num_rows($requirementcompletionresult)>0)
						{
							$ut_transcript_id 		= '';
							$ut_item_id 			= '';
							$ut_completion_date 	= '';
							$ut_user_training_id 	= '';
							while($requirementcompletionval = mysql_fetch_array($requirementcompletionresult))
							{	
								//foreach($requirementcompletionresult as $requirementcompletionval){
								$ut_transcript_id 		= $requirementcompletionval['transcript_id'];
								$ut_item_id 			= $requirementcompletionval['item_id'];
								$ut_completion_date 	= $requirementcompletionval['completion_date'];
								$ut_user_training_id 	= $requirementcompletionval['user_training_id'];
							}
							$next_expiration_date	= '0000-00-00';
							$next_due_date			= '0000-00-00';
							if($recorancedays > 0){
								$next_expiration_date	= date ('Y-m-d', strtotime(date("Y-m-d", strtotime($ut_completion_date)). "+$recorancedays day" ));
								$next_due_date 			= date ('Y-m-d', strtotime(date ("Y-m-d", strtotime($next_expiration_date)) . "+ $no_of_days day" ) );
							}	
						
							$where = "id = '$user_lp_req_id' and user_id = '$user_id' and item_id = '$item_id' and learning_plan_id = '$learning_plan_id'";
							$sql="update user_lp_requirements set is_active=0,expire_date='$expire_date',completion_date='$completion_date' where $where";
							//$db->query($sql);
							mysql_query($sql, $link) or die(mysql_error());
							
							$no_of_times = $no_of_times+1;
							
							$insert_sql = "insert into user_lp_requirements(transcript_id, completion_date, expire_date, due_date, no_of_times, user_id, group_id, learning_plan_id, training_type_id, lp_training_item_id, item_id, blended_program_id, assessment_id, assigned_date, start_date, end_date, company_id, is_active, created_date) values('$ut_transcript_id', '$ut_completion_date', '$next_expiration_date', '$next_due_date', '$no_of_times', '$user_id', '$group_id', '$learning_plan_id', '$training_type_id', '$lp_training_item_id', '$item_id', '$blended_program_id', '', '$assigned_date', '$start_date', '$end_date', '$company_id', '1', '$created_date')";mysql_query($insert_sql, $link) or die(mysql_error());		
						}
					}
				}
			}
		}
	}
?>
