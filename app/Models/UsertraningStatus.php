<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsertraningStatus extends Model
{
    protected $table = 'user_training_status';
    public $timestamps=false;
}
