<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserassessmentMaster extends Model
{ 
    protected $table = 'user_assessment_master';
	public $timestamps=false;
	protected $fillable=['course_id','user_id','question_id','answer','descriptive','status_id','last_updated_date','created_date','user_assessment_prop_id'];
	
	public $primaryKey='user_assessment_master_id';
   
}
