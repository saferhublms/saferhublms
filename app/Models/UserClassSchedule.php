<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserClassSchedule extends Model
{
    //
	public $table='user_class_schedule';
	public $primaryKey='user_schedule_id';
	public $timestamps=false;
}
