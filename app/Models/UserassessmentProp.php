<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserassessmentProp extends Model
{
   protected $table='user_assessment_prop';
   public $timestamps=false;
   public $primaryKey='user_assessment_prop_id';
   
   protected $fillable=['course_id','company_id','user_id','status_id','total_questions','attempted_questions','correct_answers','is_completed','attempt_taken','no_of_times','last_updated_date','user_training_id'];
}
