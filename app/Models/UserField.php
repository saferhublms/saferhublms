<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserField extends Model
{
    //
	public $table="user_field";
	public $timestamps=false;
}
