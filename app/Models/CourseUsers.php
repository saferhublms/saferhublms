<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseUsers extends Model
{
    //
		protected $table="course_users";
		public $primaryKey="course_users_id";
		protected $fillable=[
		'course_id', 
		'user_id',
		'company_id',
		'due_date',
		'status_id',
		'last_updated_by',
		'created_by',
		'created_date',
		'is_mandatory'
		];
		public $timestamps=false;
}
