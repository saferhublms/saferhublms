<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LearningPlanTrainingItem extends Model
{
    protected $table='learning_plan_training_item';
	
	public $primaryKey='training_item_id';
	public $timestamps=false;
}
