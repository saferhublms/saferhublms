<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSupervisiorApprove extends Model
{
    protected $table='user_supervisior_approve';
	protected $fillable=['user_id','training_catalog_id','supervisior_id','company_id','is_approve'];
	public $primaryKey='id';
	public $timestamps=false;
}
