<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HandoutsMaster extends Model
{
    //
	 protected $table='handouts_master';
	 public $timestamps=false;
}
