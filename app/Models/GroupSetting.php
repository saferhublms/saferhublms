<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupSetting extends Model
{
    public $table="group_setting";
	protected $primaryKey="group_setting_id";
	public $timestamps=false;
	protected $fillable=['company_id','division','location','area','user_company_name','first_word','last_word','exact_word','department','band'];
}
