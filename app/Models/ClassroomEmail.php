<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomEmail extends Model
{
    //
	protected $table='classroom_email';
	public $timestamps=false;
	protected $fillable=['course_id','class_id','user_id','company_id','email_notification_id','send_status','training_start_date','training_end_date','timezone_name','added_date'];
}
