<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingCategory extends Model
{
   protected $table = 'training_category';
   public $timestamps=false;
   public $primaryKey='training_category_id';
}
