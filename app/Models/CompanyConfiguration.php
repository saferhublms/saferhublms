<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyConfiguration extends Model
{
    //
	protected $table = 'company_configuration';
	public $timestamps=false;
}
