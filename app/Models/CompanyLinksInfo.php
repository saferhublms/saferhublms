<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyLinksInfo extends Model
{
    protected $table='company_links_info';
}
