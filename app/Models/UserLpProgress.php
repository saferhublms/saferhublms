<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLpProgress extends Model
{
    protected $table = 'user_learning_plan_progress';
	protected $primaryKey = 'id';
}
