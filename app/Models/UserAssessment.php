<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAssessment extends Model
{
    public $table='user_assessment';
	public $primaryKey='user_assessment_id';
	public $timestamps=false;
	protected $fillable=['assess_id','schedule_id','blended_prog_id','due_date','company_id','user_id','is_delete','attempt_taken','assigned_date','date_sent','last_updated_date'];
}
