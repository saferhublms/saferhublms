<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocSlide extends Model
{
    protected $table='doc_slides';
	public $primaryKey='slide_id';
	public $timestamps=false;
	public $fillable=['files_id','slide','slide_name','slide_audio','slide_duration','slide_order','last_updated'];
}
