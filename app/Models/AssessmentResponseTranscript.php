<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentResponseTranscript extends Model
{
    public $table='assessment_response_transcript';
	public $primaryKey='id';
	public $timestamps=false;
	protected $fillable=['user_assess_resp_id','course_id','transcript_id','score','status','created_date'];
	
	
}
