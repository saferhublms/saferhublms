<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseTargetAudience extends Model
{
	public $table='course_target_audience';
    public $primaryKey='course_target_audience_id';

	protected $fillable=[
	            'course_id', 
	            'group_id',
	            'user_id',
				'created_by',
				'updated_by',
				'company_id'
				
				];
}
