<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersAssessmentResponse extends Model
{
    protected $table = 'users_assessment_response';
	public $timestamps=false;
	public $primaryKey='id';
	protected $fillable=['course_id','user_assessment_prop_id','user_id','company_id','user_responce','status','created_date'];
}
