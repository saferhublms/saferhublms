<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobTitleGroup extends Model
{
    //
	public $timestamps=false;
    protected $table='job_title_group';
	public $primaryKey='job_id';
	protected $fillable=['company_id','jobtitle_group','type','description','is_active'];
}
