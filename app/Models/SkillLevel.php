<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillLevel extends Model
{
     protected $table='skill_level';
	public $primaryKey='skill_level_id';
	public $timestamps=false;
}
