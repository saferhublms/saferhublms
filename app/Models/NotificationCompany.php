<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationCompany extends Model
{
    //
	 protected $table='notification_company';
	 public $timestamps=false;
	 public $fillable=['notification_id','is_active','is_default','company_id','time'];
}
