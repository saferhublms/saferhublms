<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubadminPermissions extends Model
{
    protected $table = 'subadmin_permission';
	protected $primaryKey = 'id';
	public $fillable=['user_based_on','user_edit_permission','user_based_on_instructor','user_edit_permission_instructor','company_id'];
	public $timestamps=false;
}
