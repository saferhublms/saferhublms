<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignmentUser extends Model
{
    public $table='assignment_users';
	public $primaryKey='assignment_usr_id';
	public $timestamps=false;
	protected $fillable=['assignment_id','user_id','is_active','created_date'];
}
