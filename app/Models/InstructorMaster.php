<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstructorMaster extends Model
{
    protected $table='instructor_master';
	protected $fillable=['instructor_name','user_id','is_internal','company_id','is_active','last_updated_by','created_date'];
	public $primaryKey='instructor_id';
	public $timestamps=false;
}
