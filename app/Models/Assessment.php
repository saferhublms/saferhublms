<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    protected $table = 'assessment';
	public $timestamps=false;
	public $primaryKey='id';
	protected $fillable=['course_id','company_id','attempts','score_type_id','pass_percentage','last_updated_date','created_date','allow_assessment','created_by','last_updated_by','time_limit','randomize_ques','post_quiz_action','post_quiz_message','training_code','allow_transcript','hide_assessonrequ'];
}
