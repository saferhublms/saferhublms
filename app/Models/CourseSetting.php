<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseSetting extends Model
{
    public $primaryKey='course_id';
	
	protected $fillable=[
	            'course_id', 
	            'ilt_assessment_id',
				'required_approval',
				'enrollment_type',
				'is_unenrollment',
				'created_by',
				'updated_by',
				'company_id'
				];
}
