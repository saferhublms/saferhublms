<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailNotification extends Model
{
    protected $table='email_notification';
	public $primaryKey='id';
	public $timestamps=false;
	public $fillable=['email_id','company_id','send_date','subject','email_content','last_updated_date'];
	
}
