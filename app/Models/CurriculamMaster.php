<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurriculamMaster extends Model
{
    //
	public $timestamps=false;
    public $table='curriculam_master';
	public $primaryKey='curriculam_id';
}
