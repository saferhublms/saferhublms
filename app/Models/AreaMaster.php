<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaMaster extends Model
{
    //
	public $table="area_master";
	protected $primaryKey="area_id";
	public $timestamps=false;
	 
}
