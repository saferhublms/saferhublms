<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomNotificationFields extends Model
{
    protected $table='custom_notification_fields';
	public $timestamps=false;
}
