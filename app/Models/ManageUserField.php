<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManageUserField extends Model
{
    //
	public $table="manage_user_field";
	public $timestamps=false;
}
