<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTranscript extends Model
{
    protected $table='user_transcript';
	protected $fillable=['class_id','training_type_id','user_id','course_id','user_training_id','company_id','credit_value','completion_date','times','credit_from','is_approved','status_id','expiration_date','last_modified_by','last_modified_date'];
	public $primaryKey='transcript_id';
	public $timestamps=false;
}
