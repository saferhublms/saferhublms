<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTrainingHistory extends Model
{
    protected $table='user_training_history';
	protected $fillable=['user_id','user_training_id','completion_date','training_start_date_time','training_end_date_time','php_session_id','course_completion_percentage','course_id','status_id'];
	public $primaryKey='id';
	public $timestamps=false;
}
