<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassMaster extends Model
{
	public $timestamps=false;
    public $table='class_master';
	public $primaryKey='class_id';
	protected $fillable=['item_id' ,
					'class_name',
					'item_type_id' ,
					'delivery_type_id',
					'status_id',
					'maximum_seat',
					'current_enroll',
					'is_allowed_waiting',
					'published_date',
					'virtual_session_information',
					'is_active',
					'is_email_checked',
					'is_automatic_require_post_class_evaluation',
					'mail_send',
					'is_require_response_for_credit',
					'avg_rating',
					'evaluation_pref_send_same_day',
					'no_of_days',
					'publish_days_prior_to_sessions',
					'hard-copy_rating',
					'evaluation_file_name',
					'company_id',
					'created_by',
					'updated_by',
					'last_modified',
					'start_date_class',
					'endate_date',
					'end_type',
					'publish_all_reccurence',
					'recurrence_enable',
					'course_id'];
}
