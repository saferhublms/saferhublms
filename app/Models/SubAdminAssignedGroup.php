<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubAdminAssignedGroup extends Model
{
    //
	public $table="sub_admin_assigned_group";
	public $timestamps=false;
	protected $fillable=['group_id','user_id','is_active','created_date'];
}
