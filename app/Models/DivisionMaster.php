<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DivisionMaster extends Model
{
    //
	public $table="division_master";
	protected $primaryKey="division_id";
	public $timestamps=false;
	protected $fillable=['division_name','company_id','last_updated_time','is_active','is_delete','description'];
}
