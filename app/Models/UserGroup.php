<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table='user_group';
	public $primaryKey='user_group_id';
	public $timestamps=false;
	protected $fillable=['user_id','group_id','is_active','auto_group','ref_code','ref_id','group_from','created_by','updated_by','is_update','created_date','last_updated_date'];
}
