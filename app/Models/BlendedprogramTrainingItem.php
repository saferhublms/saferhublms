<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlendedprogramTrainingItem extends Model
{
    //
	public $table='blendedprogram_training_items';
	protected $fillable=['blendedprogram_id','training_type_id','item_id','sequence','blended_program_id','assessment_id','due_date','is_active','is_delete'];
	public $timestamps=false;
	public $primaryKey='training_item_id';
}
