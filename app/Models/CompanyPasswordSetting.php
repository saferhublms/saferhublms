<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPasswordSetting extends Model
{
		protected $table = 'company_password_setting';
		public $timestamps=false;
		public $fillable=['custom_pass_setting','password_expiration_time','password_length','password_complexity','maximum_invalid_hit','locked_password_time','company_id','created_by','created_date'];
	  
}
