<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubAdminAssignedCategory extends Model
{
    protected $table='sub_admin_assigned_category';
	protected $fillable=['category_id','user_id','is_active'];
	public $primaryKey='id';
	public $timestamps=false;
}
