<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuperAdminReleasenote extends Model
{
	protected $guarded = [];
    public $table='super_admin_releasenotes';
	public $timeStamps=false;
	protected $fillable=array('title','description','short_desc','maintenance_msg','version','start_hrtime','start_amorpm','end_hrtime','end_amorpm','timezone','release_date');
	
}
