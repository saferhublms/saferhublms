<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationContents extends Model
{
    protected $table='notification_content';
	public $timestamps=false;
}
