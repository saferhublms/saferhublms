<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomEnrollList extends Model
{
    //
	public $timestamps=false;
	public $table='classroom_enroll_list';
	
}
