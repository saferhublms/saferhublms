<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyCmsInfo extends Model
{

	protected $table = 'company_cms_info';
	public $timestamps=false;
}
