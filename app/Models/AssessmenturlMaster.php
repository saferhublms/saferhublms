<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmenturlMaster extends Model
{
    protected $table = 'assessment_url_master';
	public $timestamps=false;
	public $primaryKey='assess_url_id';
}
