<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlendedItemCompletion extends Model
{
    //
	protected $table='blended_item_completion';
	public $timestamps=false;
	protected $fillable=['user_training_id' ,
					'user_id',
					'blended_program_id' ,
					'item_id',
					'company_id',
					'complete_status'
					];
}
