<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubmitTrainingMaster extends Model
{
    //
	protected $table = 'submit_training_master';
	public $primaryKey = 'submit_training_id';
	public $timestamps=false;
	protected $fillable=[
						'title' ,
						'description',
						'user_id' ,
						'company_id',
						'media_format',
						'attach_file_path',
						'completion_date',
						'credit_value',
						'last_updated_date',
						'created_date',
						'status_id',
						'is_active'
					  ];
}
