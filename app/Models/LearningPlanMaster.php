<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LearningPlanMaster extends Model
{
     protected $table='learning_plan_master';
	public $primaryKey='learning_plan_id';
	public $timestamps=false;
}
