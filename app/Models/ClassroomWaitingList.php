<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomWaitingList extends Model
{

	public $timestamps=false;
    public $table='classroom_waiting_list';
}
