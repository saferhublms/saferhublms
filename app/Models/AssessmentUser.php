<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentUser extends Model
{
    public $table='assessment_users';
	public $primaryKey='id';
}
