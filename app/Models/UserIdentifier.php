<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserIdentifier extends Model
{
    //
	protected $table='user_identifier';
	public $primaryKey='user_unique_id';
	public $timestamps=false;
	protected $fillable=['company_id','user_id','user_identity'];
}
