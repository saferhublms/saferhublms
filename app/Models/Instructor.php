<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    protected $table='instructor_master';
	public $primaryKey='instructor_id';
}
