<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyAnnouncement extends Model
{
    //
	protected $table='company_announcement';
	protected $primaryKey='announcement_id';
	public $timestamps=false;
}
