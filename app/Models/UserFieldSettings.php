<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFieldSettings extends Model
{
    //
	public $table="user_field_settings";
	public $timestamps=false;
}
