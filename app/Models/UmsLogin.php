<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UmsLogin extends Model
{
    //
	protected $table = 'ums_login';
    CONST UPDATED_AT='last_updated';
    CONST CREATED_AT='created_date';
	protected $primaryKey = 'ums_id';
}
