<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandLevelMaster extends Model
{
    //
	public $table="band_level_master";
	protected $primaryKey="band_id";
	public $timestamps=false;
	protected $fillable=['band_name','company_id','is_active','is_delete','created_by'];
}
