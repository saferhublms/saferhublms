<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSupervisor extends Model
{
    public $table='user_supervisor';
	protected $fillable=['user_id','supervisor_id','is_active','start_time','end_time','last_updated_time','company_id'];
	public $primaryKey='user_supervisor_id';
	public $timestamps=false;
}
