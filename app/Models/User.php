<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Model implements AuthenticatableContract
{
	use Authenticatable;
	protected $table = 'user_master';
    CONST UPDATED_AT='last_modified';
    CONST CREATED_AT='created_date';
	protected $primaryKey = 'user_id';
	//protected $remember_token=false;
	protected $fillable = ['login_id','login_name','password','user_name','first_name','middle_name','last_name','language_id','user_pic_path','is_reset','company', 'expiration_date', 'division_name', 'area','location','address1','address2','area_code','phone_no','email_id','country','is_active','is_delete','company_id','role_id','is_approved','registration_date','mac_id','mac_password','is_user_internal','last_updated_by','last_updated_time','change_pass_date','created_date','is_admin','salt_key','timezone_id'];
	 /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	/* public function user_details()
    {
        return $this->hasOne('App\Models\UserDetail','user_id');
    } */

}
