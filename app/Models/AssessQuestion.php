<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessQuestion extends Model
{
    protected $table = 'assess_question';
	public $timestamps=false;
	public $primaryKey='ques_id';
}
