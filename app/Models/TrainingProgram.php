<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingProgram extends Model
{
	public $primaryKey='training_program_id';
	public $timestamps=false;
    protected $table='training_program';
	protected $fillable=['item_id','training_type_id',
				'class_id',
				'company_id',
				'blended_program_id',
				'training_title',
				'description',
				'is_active',
				'require_supervisor_approval',
				'avg_rating'];
}
