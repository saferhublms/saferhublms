<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoScoreData extends Model
{
    protected $table='sco_score_data';
	protected $fillable=['user_id','max_score','total_questions','correct_answer','item_id','user_training_id','transcript_id','status','company_id'];
	public $primaryKey='scorm_data_id';
	public $timestamps=false;
}
