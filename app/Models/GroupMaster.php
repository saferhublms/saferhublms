<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMaster extends Model
{
    //
	public $table="group_master";
	protected $primaryKey="group_id";
	public $timestamps=false;
	protected $fillable=['group_name','description','company_id','is_active','autogroup','created_by','last_updated_time','is_delete','created_date'];
}
