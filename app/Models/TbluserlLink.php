<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TbluserlLink extends Model
{
    protected $table='tbl_user_link';
	public $primaryKey='id';
	public $timestamps=false;
	public $fillable=['send_URL','training_catalog_id','company_id','randomnumber','user_id','pagename','parameter','added_date'];
}
