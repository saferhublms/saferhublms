<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTrainingCatalogHistory extends Model
{
    //
	protected $table='user_training_catalog_history';
    public $timestamps=false;
	protected $fillable=[
						'user_training_id',
						'class_id',
						'course_id',
						'is_approved',
						'is_enroll',
						'user_id',
						'company_id',
						'training_status_id',
						'training_type_id',
						'last_modfied_by',
						'last_modified_time',
						'created_date',
						'status_id',
						'duration',
						'duration_log'
				];
}
