<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserJobtitle extends Model
{
    //
	protected $table='user_jobtitle';
	public $primaryKey='user_jobtitle_id';
	public $timestamps=false;
	protected $fillable=['user_id','job_id','position_id','sub_organization_id','start_date_time','end_date_time','is_active','last_updated_time'];
}
