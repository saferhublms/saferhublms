<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlendedUserComplation extends Model
{
	
	public $table='blended_user_complation';
	protected $fillable=['user_id','blended_id','company_id','no_items','completion_date','is_active','last_modify_date'];
	public $timestamps=false;
    //
}
