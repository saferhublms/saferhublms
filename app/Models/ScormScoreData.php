<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScormScoreData extends Model
{
    //
	protected $table = 'scorm_score_data';
	public $timestamps=false;
	protected $fillable=[
						'user_id' ,
						'user_training_id',
						'item_id' ,
						'file_id',
						'company_id',
						'element',
						'value',
						'timemodified',
						'attempt'
					  ];
}
