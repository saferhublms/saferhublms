<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillMaster extends Model
{
    protected $table='skills_master';
	public $primaryKey='skill_id';
	public $timestamps=false;
}
