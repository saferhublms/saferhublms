<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefDocuments extends Model
{
    //
	protected $table='ref_documents';
	protected $primaryKey='ref_document_id';
	public $timestamps=false;
	
}
