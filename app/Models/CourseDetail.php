<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseDetail extends Model
{
    protected $table="course_details";

	protected $fillable=[
	            'course_id', 
	            'company_id',
				'lang_id',
				'course_name',
				'course_title',
				'course_description',
				'prerequeisites',
				'created_by',
				'updated_by',
				
				
				
				];
}
