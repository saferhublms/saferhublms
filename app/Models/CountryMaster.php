<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryMaster extends Model
{
    //
	protected $table="country_master";
	public $primaryKey='countryid';
	public $timestamps=false;
}
