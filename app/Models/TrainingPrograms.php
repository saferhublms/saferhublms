<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingPrograms extends Model
{
    protected $table='training_programs';
	protected $fillable=['training_program_code','training_type',
				'is_internal',
				'media_type_id',
				'company_id',
				'status_id',
				'created_by',
				'updated_by',
				'company_id'
				];
}
