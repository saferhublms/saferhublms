<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursePermission extends Model
{
    public $table='course_edit_permission';
    public $primaryKey='course_edit_permission_id';
	
	protected $fillable=[
	            'course_id', 
	            'group_id',
	            'user_id',
				'created_by',
				'updated_by',
				'company_id'
				
				];
}
