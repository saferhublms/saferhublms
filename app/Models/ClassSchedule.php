<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    protected $table='class_schedule';
	public $primaryKey='class_schedule_id';
	protected $fillable=['schedule_name','class_id','recursive_date','max_seat','is_active','no_of_enrolled_seat','no_of_waiting_seat','company_id','is_deleted','publish_date'];
	public $timestamps=false;
}
