<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyAboutUs extends Model
{
    //
	protected $table='company_about_us';
	protected $primaryKey='page_id';
	public $timestamps=false;
}
