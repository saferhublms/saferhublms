<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCustomValue extends Model
{
    protected $table='user_custom_value';
	public $primaryKey='id';
	public $timestamps=false;
	protected $fillable=['custom_field_id','field_value','user_id','company_id','last_updated'];
}
