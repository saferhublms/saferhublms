<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupreAdminReleasenote extends Model
{
    protected $table = 'super_admin_releasenotes';
     public $timestamps=false;
}
