<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseSkillLevelCredit extends Model
{
     protected $table="course_skill_level_credit";
	
	public $primaryKey='course_skill_level_credit_id';
}
