<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionScedule extends Model
{
	protected   $table='session_scedule';
	public 		$primaryKey='schedule_id';
	public 		$timestamps=false;
}
