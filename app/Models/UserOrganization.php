<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrganization extends Model
{
	protected $table   ='user_organization';
	public $primaryKey ='user_organization_id';
	public $timestamps =false;
	protected $fillable=['user_id','sub_organization_id','is_active','organization_code','start_date_time','end_date_time','last_updated_time'];
}
