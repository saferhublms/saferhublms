<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassMaterialSettings extends Model
{
    //
	public $timestamps=false;
    public $table='class_material_settings';
	public $primaryKey='material_settings_id';
}
