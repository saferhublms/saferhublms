<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassTimezones extends Model
{
    protected $table='class_timezones';
	public $primaryKey='class_timezone_id';
	
	public $timestamps=false;
}
