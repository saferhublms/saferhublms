<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessAttachment extends Model
{
    protected $table='assess_attachment';
	public $primaryKey='id';
	public $timestamps=false;
}
