<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseTag extends Model
{
    public $primaryKey='course_tag_id';
	public $timestamps=false;
	protected $fillable=[
	            'course_id', 
	            'tags',
				
				];
}
