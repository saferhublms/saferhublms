<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTranscriptHistory extends Model
{
    //
	protected $table='user_transcript_history';
	protected $fillable=['transcript_id','class_id','training_type_id','user_id','course_id','user_training_id','company_id','credit_value','completion_date','times','credit_from','is_approved','status_id','expiration_date','last_modified_by','last_modified_date'];
	public $timestamps=false;
	
}
