<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    //
	protected $table = 'user_info';
    CONST UPDATED_AT='last_updated_date';
    CONST CREATED_AT='created_date';
	protected $primaryKey = 'user_info_id';
}
