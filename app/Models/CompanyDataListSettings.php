<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyDataListSettings extends Model
{
    //
	protected $table='company_datalist_settings';
	public $timestamps=false;
}
