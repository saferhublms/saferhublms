<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMaster extends Model
{
    protected $table='user_master';
	protected $fillable = ['login_id','login_name','password','user_name','first_name','middle_name','last_name','language_id','user_pic_path','is_reset','company', 'expiration_date', 'division_name', 'area','location','address1','address2','area_code','phone_no','email_id','country','is_active','is_delete','company_id','role_id','is_approved','registration_date','mac_id','mac_password','is_user_internal','last_updated_by','last_updated_time','change_pass_date','created_date','is_admin','salt_key'];
	public $primaryKey='user_id';
	public $timestamps=false;
}
