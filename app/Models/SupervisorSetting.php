<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupervisorSetting extends Model
{
    public $table='supervisor_setting';
	public $timestamps=false;
	protected $fillable=['company_id','supervisor_id','notify_update','auto_approve','created_date','last_updated_date'];
}
