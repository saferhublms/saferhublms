<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyCmsLearningPlan extends Model
{
    protected $table='company_cms_learning_plan';
}
