<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseGroups extends Model
{
    //
	protected $table="course_groups";
	public $primaryKey="course_groups_id";
	protected $fillable=[
			'course_id', 
			'group_id',
			'company_id',
			'due_date',
			'status_id',
			'last_updated_by',
			'created_by',
			'created_date',
			'is_mandatory'
			];
	public $timestamps=false;
}
