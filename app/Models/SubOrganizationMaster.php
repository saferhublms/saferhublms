<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubOrganizationMaster extends Model
{
    //
	protected $table		="sub_organization_master";
	public    $primaryKey	='sub_organization_id';
	public    $timestamps	=false;
	protected $fillable=['sub_organization_name','sub_organization_code','sub_organization_logo','app_permission','company_id','last_updated_time','is_active','is_delete'];
}
