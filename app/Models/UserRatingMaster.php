<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRatingMaster extends Model
{
    public $table='user_rating_master';
	protected $fillable=['user_training_id','user_id','course_id','class_id','item_type','instructor_id','random_number','class_rate','instructor_rate','company_id','comment','status_id','last_updated_date'];
	public $timestamps=false;
	public $primaryKey='id';
}
