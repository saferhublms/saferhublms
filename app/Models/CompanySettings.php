<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanySettings extends Model
{
    //
	
	protected $table = 'company_settings';
	public $timestamps=false;
	protected $fillable=['input_training_records','require_supervisor_approval','automated_registration','update_user_info','training_feedback','classroom_feedback','can_edit_skills','only_instructor_create_blog','change_username_password','supervisor_training_view','user_layout','admin_email','company_name','promote_user','downgrade_role','rating','approval_mailid','welcome_user_mail','is_fail_transcript','last_updated_date','last_updated_by','company_id','created_date'];

}
