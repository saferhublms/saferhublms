<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDivision extends Model
{
    //
	protected $table='user_division';
	public $primaryKey='user_division_id';
	public $timestamps=false;
	protected $fillable=['user_id','division_id','is_active','start_date_time','end_date_time','last_updated_time'];
}
