<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingCatalogTranscript extends Model
{
    protected $table='training_catalog_transcript';
	public $primaryKey='training_transcript_id';
	public $timestamps=false;
	public $fillable=['class_schedule_id','class_id','item_id','registration_id','user_id','user_training_id','company_id','credit_value','completion_date','expiration_date','times','is_approved','is_active','last_modified_by'];
}
