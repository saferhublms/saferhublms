<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessanswertypeMaster extends Model
{
    protected $table = 'assess_answer_type_master';
	public $timestamps=false;
	public $primaryKey='answer_id';
}
