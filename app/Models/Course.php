<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	
    protected $table="courses";
	public $primaryKey='course_id';
	protected $fillable=[
	            'training_program_code', 
	            'course_image',
				'credit_value',
				'points',
				'average_rating',
				'item_cost',
				'media_library_id',
				'company_id',
				'status_id',
				'created_by',
				'updated_by',
				'created_date_time',
				'last_updated_time',
				'published_date',
				'certificate_template_id',
				
				
				];
}
