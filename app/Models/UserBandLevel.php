<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBandLevel extends Model
{
    //
		protected $table='user_band_level';
		public $primaryKey='user_band_id';
		public $timestamps=false;
		protected $fillable=['user_id','band_id','is_active','company_id','created_date','updated_date'];
}
