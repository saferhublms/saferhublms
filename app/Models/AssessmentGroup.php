<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentGroup extends Model
{
    public $table='assessment_groups';
	public $primaryKey='id';
}
