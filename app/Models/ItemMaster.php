<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemMaster extends Model
{
	public $timestamps=false;
    protected $table='item_master';
	public $primaryKey='item_id';
	protected $fillable=['item_name','item_type_id','training_type_id','title','description','file_id','is_active','status_id','company_id','item_cost','training_code','credit_value','average_rating','certificate_template_id','created_by','created_date'];
}
