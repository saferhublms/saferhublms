<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationMaster extends Model
{
    //
	public $table="location_master";
	protected $primaryKey="location_id";
	public $timestamps=false;
	 
}
