<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserJobtitles extends Model
{
    protected $table='user_jobtitle';
	protected $fillable=['user_id','job_id','position_id','sub_organization_id','start_date_time','end_date_time','last_updated_time'];
	public $primaryKey='user_jobtitle_id';
	public $timestamps=false;
}
