<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingLibrary extends Model
{
     protected $table = 'training_library';
     public $timestamps=false;
}
