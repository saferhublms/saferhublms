<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLpRequirements extends Model
{
    protected $table = 'user_lp_requirements';
    public $timestamps=false;
	protected $primaryKey = 'id';
}
