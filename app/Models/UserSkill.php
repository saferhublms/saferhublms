<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model
{
    //
	protected $table='user_skill';
	protected $primaryKey='user_skill_id';
	protected $fillable=['skill_id','skill_rating','user_id','level_id','course_id','training_type','expiration_date','is_active'];
	public $timestamps=false;
}
