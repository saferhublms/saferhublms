<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsertrainingCatalog extends Model
{
    protected $table='user_training_catalog';
    public $timestamps=false;
	public $primaryKey='user_training_id';
	protected $fillable=[
				'class_id',
				'course_id',
				'is_approved',
				'is_enroll',
				'user_id',
				'company_id',
				'training_status_id',
				'training_type_id',
				'last_modfied_by',
				'last_modified_time',
				'created_date',
				'status_id',
				'duration',
				'duration_log'
			];
}
