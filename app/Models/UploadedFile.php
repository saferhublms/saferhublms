<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
	public $primaryKey='files_id';
	public $timestamps=false;
    protected $fillable=['file_name','file_path','format_id','file_size','company_id','is_active','uploaded_date','package_type','data1','data2','version','organization','title','identifier','launch','scorm_type','parent_root','manifest','last_updated','masteryscore','embedded_code','url','media_type','media_title','added_status','played_status','file_title','media_id','is_lib'];
}
