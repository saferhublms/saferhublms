<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemMasterTrainingcategory extends Model
{
    public $table='item_master_trainingcategory';
	public $primaryKey='item_master_trainingcategory_id';
	protected $fillable=['item_id','training_category_id','is_active','created_date','company_id'];
	public $timestamps=false;
}
