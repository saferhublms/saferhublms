<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationMaster extends Model
{
    protected $table='notification_master';
	public $timestamps=false;
}
