<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageLabel extends Model
{
    public $timestamps=false;
	public $primaryKey='page_label_id';
}
