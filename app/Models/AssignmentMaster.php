<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignmentMaster extends Model
{
    public $table='assignment_master';
	public $primaryKey='assignment_id';
	public $timestamps=false;
	protected $fillable=['company_id','training_program_id','due_date','assignment_start_date','group_id','is_active','created_date','created_by'];
}
