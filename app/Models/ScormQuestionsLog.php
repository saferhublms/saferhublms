<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScormQuestionsLog extends Model
{
	protected $table = 'scorm_questions_log';
	public $timestamps=false;
	protected $fillable=[
						'item_id' ,
						'user_id',
						'file_id' ,
						'company_id',
						'questions_list',
						'total_questions',
						'total_correct_questions',
						'total_incorrect_questions',
						'user_training_id'
					  ];
    //
}
