<?php

namespace App\Repositories;

class AiccparserRepository extends BaseRepository
{
	
    public function get_launch_file_name($file_name){
		return substr($file_name,strripos($file_name,"/")+1);
	}

	public function read_aicc_crs_file($aicc_au_file_path,$key_value_array){
		$crs_reg_exp = "/(.*)=(.*)/";
		$handle = fopen($aicc_au_file_path, "r");
		if ($handle) {
			while(! feof($handle))
			{
				$line = fgets($handle);
				if(preg_match($crs_reg_exp,$line)){
					preg_match($crs_reg_exp,$line,$match);
					$key_value_array[strtolower(trim($match[1]))] = trim($match[2]);	
				}
				 
			}
		} else {
			// error opening the file.
		}
		return $key_value_array;

	}

	public function read_aicc_au_file($aicc_au_file_path,$key_value_array){
		$handle = fopen($aicc_au_file_path, "r");
		if ($handle) {
			$key_line = fgets($handle);
			$value_line = fgets($handle);
			$key_array = explode ( '","' , $key_line );
			$value_array = explode ( '",' , $value_line );
			//echo "key_array:".count($key_array)."==value_array:".count($value_array);
			//preg_match_all('/("[^",]*")|[^",]*|[^,]*/i',$value_line,$matches);
			//$keywords = preg_split('/^\",|^,+/', $value_line);
			//preg_match_all('/\",\"|[0-9|\S|\"],[0-9|\S|\"]+/', $value_line, $keywords);
			if(count($key_array)!=count($value_array)){
				$temp_value_array = array();
				foreach($value_array as $val){
					$val = trim($val,"\"\r\t\n");
					$val_arr = explode(",",$val);
					if(is_array($val_arr)){
						$valid = true;
						foreach($val_arr as $value){
							if($value==""){
								$valid = true;
							}elseif(is_numeric($value)) {
								$valid = true;
							}else{
								$valid = false;
								break;
							}
						}
						if($valid){
							foreach($val_arr as $value){
								$temp_value_array[] = $value;
							}
						}else{
							$temp_value_array[] = $val;
						}
					}else{
						$temp_value_array[] = $val;
					} 
				}
				$value_array = $temp_value_array;
			}
			//print_r($temp_value_array);
			//echo "<pre>".print_r($matches,true);
			foreach($key_array as $index => $key){
				$key_value_array[strtolower(trim($key,"\"\r\t\n"))] = trim($value_array[$index],"\"\r\t\n");
			}
			
		} else {
			// error opening the file.
		}
		return $key_value_array;
	}

	public function check_aicc_file($files, $reg_exp_aicc_course){
		foreach ($files['files'] as $value){
			if(preg_match($reg_exp_aicc_course,strtolower($value))){
				return $value;
			}
		}
	}

	/** 
	 * Finds path, relative to the given root folder, of all files and directories in the given directory and its sub-directories non recursively. 
	 * Will return an array of the form 
	 * array( 
	 *   'files' => [], 
	 *   'dirs'  => [], 
	 * ) 
	 * @author sreekumar 
	 * @param string $root 
	 * @result array 
	 */ 
	public function read_all_files($root = '.'){ 
	  $files  = array('files'=>array(), 'dirs'=>array()); 
	  $directories  = array(); 
	  $last_letter  = $root[strlen($root)-1]; 
	  $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR; 
	  
	  $directories[]  = $root; 
	  
	  while (sizeof($directories)) { 
		$dir  = array_pop($directories); 
		if ($handle = opendir($dir)) { 
		  while (false !== ($file = readdir($handle))) { 
			if ($file == '.' || $file == '..') { 
			  continue; 
			} 
			$file  = $dir.$file; 
			if (is_dir($file)) { 
			  $directory_path = $file.DIRECTORY_SEPARATOR; 
			  array_push($directories, $directory_path); 
			  $files['dirs'][]  = $directory_path; 
			} elseif (is_file($file)) { 
			  $files['files'][]  = $file; 
			} 
		  } 
		  closedir($handle); 
		} 
	  } 
	  //echo "<pre> files --> "; print_r($files);
	  return $files; 
	} 
}
