<?php

namespace App\Repositories;

class Xml2ArrayRepository extends BaseRepository
{
	
   var $arrOutput = array();
   var $resParser;
   var $strXmlData;

   /**
   * Convert a utf-8 string to html entities
   *
   * @param string $str The UTF-8 string
   * @return string
   */
   
   function utf8_to_entities($str)
   {
       global $CFG;
	   $entities = '';
       $values = array();
       $lookingfor = 1;
       return $str;
   }

   /**
   * Parse an XML text string and create an array tree that rapresent the XML structure
   *
   * @param string $strInputXML The XML string
   * @return array
   */
   function parse($strInputXML)
   {
	   $this->resParser = xml_parser_create ('UTF-8');   // returns resource id (handle) for further use
	   xml_set_object($this->resParser,$this);		   
	   xml_set_element_handler($this->resParser, "tagOpen", "tagClosed");
	   xml_set_character_data_handler($this->resParser, "tagData");   
	   $this->strXmlData = xml_parse($this->resParser,$strInputXML);		   
	   if(!$this->strXmlData){			   
		  die(sprintf("XML error: %s at line %d", xml_error_string(xml_get_error_code($this->resParser)), xml_get_current_line_number($this->resParser)));
	   }		
	   xml_parser_free($this->resParser);   // free the parser in concern		   
	   return $this->arrOutput;
   }

   function tagOpen($parser, $name, $attrs)
   {
       $tag=array("name"=>$name,"attrs"=>$attrs);
       array_push($this->arrOutput,$tag);
   }

   function tagData($parser, $tagData)
   {
	    if(trim($tagData))
		{
           if(isset($this->arrOutput[count($this->arrOutput)-1]['tagData'])) {
               $this->arrOutput[count($this->arrOutput)-1]['tagData'] .= $this->utf8_to_entities($tagData);
           } else {
               $this->arrOutput[count($this->arrOutput)-1]['tagData'] = $this->utf8_to_entities($tagData);
           }
       }
   }

   function tagClosed($parser, $name)
   {
       $this->arrOutput[count($this->arrOutput)-2]['children'][] = $this->arrOutput[count($this->arrOutput)-1];
       array_pop($this->arrOutput);
	   
   }
}
