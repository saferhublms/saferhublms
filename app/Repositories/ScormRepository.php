<?php

namespace App\Repositories;
use DB;
use App\Models\ScormScoreData;
use App\Models\ScoScoreData;
use App\Models\Course;
use App\Models\UserTranscript;
use App\Models\UserTranscriptHistory;
use App\Models\UserTrainingHistory;
use App\Models\UsertrainingCatalog;
use App\Models\UserTrainingCatalogHistory;
use App\Http\Traits\Trainingprogram;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Trainingcatalog;
use App\Models\UploadedFile;
use App\Models\ScormQuestionsLog;
use \SimpleXMLElement;
use Parser;
use Session;
use CommonFunctions;
use \stdClass;
define('UPDATE_EVERYDAY', '2');
define('UPDATE_EVERYTIME', '3');
define('SCO_ALL', 0);
define('SCO_DATA', 1);
define('SCO_ONLY', 2);
define('GRADESCOES', '0');
define('GRADEHIGHEST', '1');
define('GRADEAVERAGE', '2');
define('GRADESUM', '3');
define('HIGHESTATTEMPT', '0');
define('AVERAGEATTEMPT', '1');
define('FIRSTATTEMPT', '2');
define('LASTATTEMPT', '3');
define('TOCJSLINK', 1);
define('TOCFULLURL', 2); 

class ScormRepository extends BaseRepository
{
    public function __construct(ScormScoreData $ScormScoreData,UserTrainingHistory $UserTrainingHistory,Course $Course,UserTranscriptHistory $UserTranscriptHistory,UserTranscript $UserTranscript,UsertrainingCatalog $UsertrainingCatalog,UserTrainingCatalogHistory $UserTrainingCatalogHistory,ScoScoreData $ScoScoreData,ScormQuestionsLog $ScormQuestionsLog)
    {
        $this->ScormScoreData = $ScormScoreData;
        $this->UserTrainingHistory = $UserTrainingHistory;
        $this->Course = $Course;
        $this->UserTranscriptHistory = $UserTranscriptHistory;
        $this->UserTranscript = $UserTranscript;
        $this->UsertrainingCatalog = $UsertrainingCatalog;
        $this->UserTrainingCatalogHistory = $UserTrainingCatalogHistory;
        $this->ScoScoreData = $ScoScoreData;
        $this->ScormQuestionsLog = $ScormQuestionsLog;
     	// $this->industry = $industry;
	}
    
	public function getScorm($course_id, $user_id, $file_id)
	{
		$param_arr=array();
		$sql = "select * from uploaded_files where files_id =:file_id";
		$param_arr['file_id']=$file_id;
		$data=DB::select(DB::raw($sql) ,$param_arr);
		
		$sco = new \stdClass();
		if($data){
			$data = json_decode(json_encode($data),true);
			foreach ($data as $value) {
				$sco->course_id = $course_id;
				$sco->course = $course_id;
				$sco->id = $value['files_id'];
				$sco->scorm = $course_id;
				$sco->manifest = $value['manifest'];
				$sco->organization = $value['organization'];
				$sco->parent = $value['parent_root'];
				$sco->identifier = $value['identifier'];
				$sco->launch = $value['launch'];
				$sco->package_type	= $value['package_type'];
				$sco->scormtype = $value['scorm_type'];
				$sco->title = $value['title'];
				$filesname = $value['file_name'];
				@$filedir = substr($filesname, 0, -4);
				$sco->file_name = $filedir;
				$sco->name = $value['file_name'];
				$sco->reference = $value['title'];
				$sco->summary = $value['title'];
				$sco->version = $value['version'];
				$sco->masteryscore = $value['masteryscore'];
				
				$sco->maxgrade = 100;
				$sco->grademethod = 1;
				$sco->whatgrade = 0;
				$sco->auto = "0";
			}
			return $sco;
		}
		return null;    
	}
	
	public function random_string ($length=15) {
		$pool  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pool .= 'abcdefghijklmnopqrstuvwxyz';
		$pool .= '0123456789';
		$poollen = strlen($pool);
		mt_srand ((double) microtime() * 1000000);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($pool, (mt_rand()%($poollen)), 1);
		}
		return $string;
	}
	
	public function scorm_get_tracks($course_id, $file_id, $userid, $attempt='', $user_training_id) {
	
		if (empty($attempt)) {
			$attempt = $this->scorm_get_last_attempt($course_id,$userid, $user_training_id);
		}
//item_id
		$param_arr1=array();
	    $sql = "select * from scorm_score_data where user_id = :user_id AND file_id= :file_id  AND attempt = :attempt AND user_training_id=:user_training_id";
        $param_arr1['user_id']			= $userid;
        $param_arr1['file_id']			= $file_id;
        $param_arr1['attempt']			= $attempt;
        $param_arr1['user_training_id']	= $user_training_id;
		$tracks=DB::select(DB::raw($sql) ,$param_arr1);

        $usertrack = new \stdClass();
		$usertrack->userid = $userid;
		$usertrack->file_id = $file_id;
		
		if($tracks){
			$usertrack->score_raw = '';
			$usertrack->status = '';
			$usertrack->total_time = '00:00:00';
			$usertrack->session_time = '00:00:00';
			$usertrack->timemodified = 0;
			$tracks = json_decode(json_encode($tracks),true);
			foreach ($tracks as $track) {
					$element = $track['element'];
					$value = $this->stripslashes_safe($track['value']);
					$usertrack->{$element} = $value;
					switch ($element) {
							case 'cmi.core.lesson_status':
							case 'cmi.completion_status':
									if ($track['value'] == 'not attempted') {
											$track['value'] = 'notattempted';
									}
									$usertrack->status = $track['value'];
							break;
							case 'cmi.core.score.raw':
							case 'cmi.score.raw':
									$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
									break;
							case 'cmi.core.session_time':
							case 'cmi.session_time':
									$usertrack->session_time = $track['value'];
							break;
							case 'cmi.core.total_time':
							case 'cmi.total_time':
									$usertrack->total_time = $track['value'];
							break;
					}
					if (isset($track['timemodified']) && ($track['timemodified'] > $usertrack->timemodified)) {
							$usertrack->timemodified = $track['timemodified'];
					}
			}
			
			if (is_array($usertrack)) {
					ksort($usertrack);
			}
			
			return $usertrack;
		} else {
			return false;
		}
	}
	
	public function scorm_get_last_attempt($course_id, $userid, $user_training_id) {
		
        $param_arr=array();
		$condition = " where user_id = :user_id ";
		$param_arr['user_id']=$userid;
		
		if($course_id != ''){
			$condition = $condition . ' and item_id = :item_id';
			$param_arr['item_id']=$course_id;
		}

		$sqlquery = "select max(attempt) as a from scorm_score_data ". $condition;

		$lastattempt=DB::select(DB::raw($sqlquery) ,$param_arr);

		if (count($lastattempt)) {
			if (empty($lastattempt[0]->a)) {
				return '1';
			} else {
				return $lastattempt[0]->a;
			}
		} else {
			return false;
		}
	}
	
	public function stripslashes_safe($mixed) {
		// there is no need to remove slashes from int, float and bool types
		if (empty($mixed)) {
			//nothing to do...
		} else if (is_string($mixed)) {
			if ($this->ini_get_bool('magic_quotes_sybase')) { //only unescape single quotes
				$mixed = str_replace("''", "'", $mixed);
			} else { //the rest, simple and double quotes and backslashes
				$mixed = str_replace("\\'", "'", $mixed);
				$mixed = str_replace('\\"', '"', $mixed);
				$mixed = str_replace('\\\\', '\\', $mixed);
			}
		} else if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = $this->stripslashes_safe($value);
			}
		} else if (is_object($mixed)) {
			$vars = get_object_vars($mixed);
			foreach ($vars as $key => $value) {
				$mixed->$key = $this->stripslashes_safe($value);
			}
		}
		return $mixed;
	}

	public function ini_get_bool($ini_get_arg) {
		$temp = ini_get($ini_get_arg);
		if ($temp == '1' or strtolower($temp) == 'on') {
			return true;
		}
		return false;
	}
	
	public function addslashes_js($var) {
		if (is_string($var)) {
			$var = str_replace('\\', '\\\\', $var);
			$var = str_replace(array('\'', '"', "\n", "\r", "\0"), array('\\\'', '\\"', '\\n', '\\r', '\\0'), $var);
			$var = str_replace('</', '<\/', $var);   // XHTML compliance
		} else if (is_array($var)) {
			$var = array_map(array($this,'addslashes_js'), $var);
		} else if (is_object($var)) {
			$a = get_object_vars($var);
			foreach ($a as $key=>$value) {
			  $a[$key] = $this->addslashes_js($value);  //storing values with in an array....
			}
			$var = (object)$a;    //converting array to an object by type casting..
		}
		return $var;
	}
	
	public function scorm_reconstitute_array_element($sversion, $userdata, $element_name, $children) {
		
		// reconstitute comments_from_learner and comments_from_lms
		$current = '';
		$current_subelement = '';
		$current_sub = '';
		$count = 0;
		$count_sub = 0;
		$scormseperator = '_';
		if ($sversion == 'scorm_13') { //scorm 1.3 elements use a . instead of an _
			$scormseperator = '.';
		}
		
		// filter out the ones we want
		$element_list = array();
		foreach($userdata as $element => $value){
			if (substr($element,0,strlen($element_name)) == $element_name) {
				$element_list[$element] = $value;
			}
		}

		// sort elements in .n array order
		uksort($element_list,  array($this,"scorm_element_cmp"));

		// generate JavaScript
		foreach($element_list as $element => $value){
			if ($sversion == 'scorm_13') {
				$element = preg_replace('/\.(\d+)\./', ".N\$1.", $element);
				preg_match('/\.(N\d+)\./', $element, $matches);
			} else {
				$element = preg_replace('/\.(\d+)\./', "_\$1.", $element);
				preg_match('/\_(\d+)\./', $element, $matches);
			}
			if (count($matches) > 0 && $current != $matches[1]) {
				if ($count_sub > 0) {
					echo '    '.$element_name.$scormseperator.$current.'.'.$current_subelement.'._count = '.$count_sub.";\n";
				}
				$current = $matches[1];
				$count++;
				$current_subelement = '';
				$current_sub = '';
				$count_sub = 0;
				$end = strpos($element,$matches[1])+strlen($matches[1]);
				$subelement = substr($element,0,$end);
				echo '    '.$subelement." = new Object();\n";
				// now add the children
				foreach ($children as $child) {
					echo '    '.$subelement.".".$child." = new Object();\n";
					echo '    '.$subelement.".".$child."._children = ".$child."_children;\n";
				}
			}

			// now - flesh out the second level elements if there are any
			if ($sversion == 'scorm_13') {
				$element = preg_replace('/(.*?\.N\d+\..*?)\.(\d+)\./', "\$1.N\$2.", $element);
				preg_match('/.*?\.N\d+\.(.*?)\.(N\d+)\./', $element, $matches);
			} else {
				$element = preg_replace('/(.*?\_\d+\..*?)\.(\d+)\./', "\$1_\$2.", $element);
				preg_match('/.*?\_\d+\.(.*?)\_(\d+)\./', $element, $matches);
			}

			// check the sub element type
			if (count($matches) > 0 && $current_subelement != $matches[1]) {
				if ($count_sub > 0) {
					echo '    '.$element_name.$scormseperator.$current.'.'.$current_subelement.'._count = '.$count_sub.";\n";
				}
				$current_subelement = $matches[1];
				$current_sub = '';
				$count_sub = 0;
				$end = strpos($element,$matches[1])+strlen($matches[1]);
				$subelement = substr($element,0,$end);
				echo '    '.$subelement." = new Object();\n";
			}

			// now check the subelement subscript
			if (count($matches) > 0 && $current_sub != $matches[2]) {
				$current_sub = $matches[2];
				$count_sub++;
				$end = strrpos($element,$matches[2])+strlen($matches[2]);
				$subelement = substr($element,0,$end);
				echo '    '.$subelement." = new Object();\n";
			}

			echo '    '.$element.' = \''.$value."';\n";
		}
		if ($count_sub > 0) {
			echo '    '.$element_name.$scormseperator.$current.'.'.$current_subelement.'._count = '.$count_sub.";\n";
		}
		if ($count > 0) {
			echo '    '.$element_name.'._count = '.$count.";\n";
		}
	}

	public  function scorm_element_cmp($a, $b) {
		$left = $right = 0;
		preg_match('/.*?(\d+)\./', $a, $matches);
		if(isset($matches[1])) $left = intval($matches[1]);
		preg_match('/.?(\d+)\./', $b, $matches);
		if(isset($matches[1])) $right = intval($matches[1]);
		if ($left < $right) {
			return -1; // smaller
		} elseif ($left > $right) {
			return 1;  // bigger
		} else {
			// look for a second level qualifier eg cmi.interactions_0.correct_responses_0.pattern
			if (preg_match('/.*?(\d+)\.(.*?)\.(\d+)\./', $a, $matches)) {
				$leftterm = intval($matches[2]);
				$left = intval($matches[3]);
				if (preg_match('/.*?(\d+)\.(.*?)\.(\d+)\./', $b, $matches)) {
					$rightterm = intval($matches[2]);
					$right = intval($matches[3]);
					if ($leftterm < $rightterm) {
						return -1; // smaller
					} elseif ($leftterm > $rightterm) {
						return 1;  // bigger
					} else {
						if ($left < $right) {
							return -1; // smaller
						} elseif ($left > $right) {
							return 1;  // bigger
						}
					}
				}
			}
			// fall back for no second level matches or second level matches are equal
			return 0;  // equal to
		}
	}
/* 	public function scorm_insert_track($userid, $item_id, $file_id, $attempt, $element, $value, $user_training_id=0, $company_id) {
		$id = null;
		$track = $this->get_record_select('scorm_score_data'," user_id=$userid AND item_id=$item_id AND file_id=$file_id AND attempt=$attempt AND element='$element' AND user_training_id='$user_training_id'");
		
		$scormtracldata = array();
		if ($track) {
			if ($element != 'x.start.time') { 
				$scormtracldata['attempt'] 		=  $track->attempt;
				$scormtracldata['element'] 		=  $track->element;
				$scormtracldata['value'] 		=  $this->addslashes_js($value);
				$scormtracldata['timemodified'] =  time();
				$this->db->update('scorm_score_data', $scormtracldata, "id='".$track->id."'");
			}
			
		} else {
			$scormtracldata['user_id'] 			=  $userid;
			$scormtracldata['item_id'] 			=  $item_id;
			$scormtracldata['file_id'] 			=  $file_id;
			$scormtracldata['company_id'] 		=  $company_id;
			$scormtracldata['attempt'] 			=  $attempt;
			$scormtracldata['element'] 			=  $element;
			$scormtracldata['value'] 			=  $this->addslashes_js($value);
			$scormtracldata['timemodified'] 	=  time();
			$scormtracldata['user_training_id'] =  $user_training_id;			
			$this->db->insert('scorm_score_data', $scormtracldata);
		}
		
		
		$getval = $this->getPassfailsettings($company_id);
		
		if($getval == 0){
			$arr = 	array('completed', 'passed', 'failed');
		}else{
		
		$arr =	array('completed', 'passed');
		}
		
		
		if (in_array($element, array('cmi.completion_status', 'cmi.core.lesson_status', 'cmi.success_status'))
			 && in_array($scormtracldata['value'], $arr)) {
			$this->updateTrainingStatus($item_id, $userid, $file_id, $user_training_id, $company_id);
			$this->scorm_update_grades($item_id, $userid, $file_id, $user_training_id, $company_id);
		}
		if (strstr($element, '.score.raw')) {
			$this->scorm_update_grades($item_id, $userid, $file_id, $user_training_id, $company_id);
		}

		if($element == 'cmi.suspend_data'){
			$this->updateTrainingProgressPercentage($item_id,$userid,$item_id,$file_id,$attempt,$element, $user_training_id);
		}
		
		if(in_array($element,array('cmi.core.total_time', 'cmi.total_time'))){
			$update_utc_query = "UPDATE user_training_catalog SET duration = '".$this->addslashes_js($value)."' WHERE item_id = '".$item_id."' AND user_id = '".$userid."' AND user_training_id = '".$user_training_id."'  AND company_id = '".$company_id."' ";
			$this->db->query($update_utc_query);
		}
		
		return true;
	} */
	public function scorm_insert_track($userid, $course_id, $file_id, $attempt, $element, $value, $user_training_id=0, $company_id) {
		
		$id = null;
		$track = $this->get_record_select('scorm_score_data'," user_id=$userid AND item_id=$course_id AND file_id=$file_id AND attempt=$attempt AND element='$element' AND user_training_id='$user_training_id'");
	   
		$scormtracldata = array();
		
		if (count($track)) { 
			if ($element != 'x.start.time') { 
				$scormtracldata['attempt'] 		=  $track[0]->attempt;
				$scormtracldata['element'] 		=  $track[0]->element;
				$scormtracldata['value'] 		=  $this->addslashes_js($value);
				$scormtracldata['timemodified'] =  time();	
				$this->ScormScoreData->where('id','=',$track[0]->id)->update($scormtracldata);
			}
			
		} else {
			$scormtracldata['user_id'] 			=  $userid;
			$scormtracldata['item_id'] 			=  $course_id;
			$scormtracldata['file_id'] 			=  $file_id;
			$scormtracldata['company_id'] 		=  $company_id;
			$scormtracldata['attempt'] 			=  $attempt;
			$scormtracldata['element'] 			=  $element;
			$scormtracldata['value'] 			=  $this->addslashes_js($value);
			$scormtracldata['timemodified'] 	=  time();
			$scormtracldata['user_training_id'] =  $user_training_id;
            $this->ScormScoreData->insert($scormtracldata);	
		}
		
			$getval = $this->getPassfailsettings($company_id);

			if($getval == 0){
				$arr = 	array('completed', 'passed', 'failed');
			}else{

			$arr =	array('completed', 'passed');
			}
		
		if (in_array($element, array('cmi.completion_status', 'cmi.core.lesson_status', 'cmi.success_status'))
			 && in_array($scormtracldata['value'],$arr)) {
			 
			$this->updateTrainingStatus($course_id, $userid, $file_id, $user_training_id, $company_id);
			$this->scorm_update_grades($course_id, $userid, $file_id, $user_training_id, $company_id);
		}
		
		if (strstr($element, '.score.raw')) {
			$this->scorm_update_grades($course_id, $userid, $file_id, $user_training_id, $company_id);
		}

		if($element == 'cmi.suspend_data'){
			 $this->updateTrainingProgressPercentage($course_id,$userid,$file_id,$attempt,$element, $user_training_id);
		}
		
		return true;
	}
	
	public function getPassfailsettings($compid){
		$is_fail_transcript = 0;
		$data1 = array();
		
		$sqlSelect = "select is_fail_transcript from company_settings where company_id = :company_id"; 
		$param_arr['company_id']		=$compid;
		$data1=DB::select(DB::raw($sqlSelect) ,$param_arr);
	
		if($data1){
			foreach($data1 as  $rowvalue1){
				$is_fail_transcript = $rowvalue1->is_fail_transcript;
			}
		}
		return $is_fail_transcript;
	}
	
	public function get_record_select($tableName, $condition, $field='*'){
		if($condition){
			$selectclause = 'where '.$condition;
		}
		$sqlquery = 'select '. $field .' from '. $tableName.' where '.$condition;
		$runquery=DB::select($sqlquery);
		if(count($runquery)){
			return $runquery;
		}else{
			return null;
		}
	}
	
	public function updateTrainingStatus($course_id, $userid, $file_id, $usertrainingid=0, $compid)
	{

			$userlearningplan = Usertrainingcatalogs::getTrainingCatalogId($usertrainingid,$userid,$compid);
		
			if(count($userlearningplan) == 0)
			{	
				$programsschedule= array();
				$lastId = Trainingcatalog::EnrolledForElearningLearningplan($course_id, $userid, $userprogramtypeid, $compid);
				$usertrainingid = $lastId;
			}
			else
			{
				$usertrainingid = $userlearningplan[0]['user_training_id'];
			}
          
		

			 //update and insert data into user_training_history table
			$this->updateInsertUserTrainingHistory($usertrainingid, $course_id, $userid,$file_id);
			
			 $creditdata=$this->Course::where('course_id','=',$course_id)->where('status_id','=',1)->where('company_id','=',$compid)->first();
			 $credit_value= 0;
			 if($creditdata){
				$credit_value= $creditdata->credit_value;
			 }
			//update data into user_trascript table
			$this->updateDataIntoTrascript($credit_value,$course_id,$userid,$usertrainingid,$compid);
			
			//update and insert data into user_training_catalog table
			$this->updateInsertUserTrainingCatalog($course_id, $compid, $userid,$usertrainingid);


			CommonFunctions::getCourseSkillLevel($course_id, $compid,$userid,1);
			//$this->ScoScoreData
		
	}

 
	
	
	public function updateInsertUserTrainingHistory($usertrainingid, $course_id, $userid,$fileid)
	{
		
		$recordss=$this->UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('user_training_id','=',$usertrainingid)->where('status_id','=',1)->first();
		
		if($recordss)
		{

			$datatrans['user_training_id']=$usertrainingid;
			$datatrans['course_id']=$course_id;
			$datatrans['user_id']=$userid;
			$datatrans['status_id']=0;
			$this->UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->update($datatrans);
		}
			$launchtime	=$this->ScormScoreData::where('item_id','=',$course_id)->where('user_training_id','=',$usertrainingid)->where('user_id','=',$userid)->where('element','=','x.start.time')->select('timemodified')->first();
			$launchtime=isset($launchtime->timemodified)?date('Y-m-d H:i:s',$launchtime->timemodified):'0000-00-00';
			$data1['user_training_id']=$usertrainingid;
			$data1['course_id']=$course_id;
			$data1['user_id']=$userid;
			$data1['course_completion_percentage']=100;
			$data1['training_end_date_time']=date('Y-m-d H:i:s');
			$data1['training_start_date_time']=$launchtime;
			$data1['php_session_id']=Session::getId();
			$this->UserTrainingHistory::insert($data1);
	}
	
	public function updateDataIntoTrascript($credit_value,$course_id,$userid,$usertrainingid,$compid)
	{
		
		$data['class_id']=0;
		$data['training_type_id']=1;
		$data['user_id']=$userid;
		$data['course_id']=$course_id;
		$data['user_training_id']=$usertrainingid;
		$data['company_id']=$compid;
		$data['credit_value']=$credit_value;
		$data['completion_date']=date('Y-m-d');
		$data['times']=1;
		$data['credit_from']=0;
		$data['is_approved']=1;
		$data['status_id']=1;
		$data['last_modified_by']=$userid;
		$data['expiration_date'] = '0000-00-00';
		
		$currentDate = date('Y-m-d');
		$transcriptsdata = $this->UserTranscript::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->first();
		
		$utid = 0;
		if($transcriptsdata)
		{   
	        $transcriptsdata=$transcriptsdata->toArray();
			$utid = $transcriptsdata['transcript_id'];
			$times = $transcriptsdata['times'];
			$datat['completion_date']= date('Y-m-d');
			if(!$data['expiration_date'])
			{ 
			 $data['expiration_date'] = '0000-00-00'; 
		    }else{
			   $datat['expiration_date'] = $data['expiration_date'];
			}
			$datat['status_id']=1;
			$datat['times']=$times+1;
			$datat['user_training_id']=$usertrainingid;
			
			$this->UserTranscript::where('course_id','=',$course_id)->where('user_id','=',$userid)->where('company_id','=',$compid)->update($datat);
			
			$this->UserTranscriptHistory::insert($transcriptsdata);

		}
		else
		{
			if(!$data['expiration_date'])
			{
		     $data['expiration_date'] = '0000-00-00'; 
		    }
		    $utid = $this->UserTranscript::insertGetId($data);
			/* $data['transcript_id']=$transcript_id;
		    UserTranscriptHistory::insert($data); */
			
		}
		$this->updateInsertScoScoreData($utid,$userid,$usertrainingid,$course_id);
		
	}
	 public function updateInsertScoScoreData($transcript_id,$user_id,$training_id,$item_id){
		
		$check_sco=$this->ScoScoreData::where('transcript_id','=',$transcript_id)->select('scorm_data_id')->first();
		if(!$check_sco){
			$scorm_data_id=$this->ScoScoreData::where('user_id','=',$user_id)->where('item_id','=',$item_id)->where('user_training_id','=',$training_id)->select('scorm_data_id')->first();
			if($scorm_data_id){
                $scorm_data_id=$scorm_data_id->scorm_data_id;
				$sco_score_arr=array();
				$sco_score_arr['transcript_id']	    =$transcript_id;
                $this->ScoScoreData::where('scorm_data_id','=',$scorm_data_id)->update($sco_score_arr);
			}
		}
	}
	
	public function updateInsertUserTrainingCatalog($course_id, $compid, $userid,$usertrainingid){
		$data['training_status_id']	=	1;	
		$this->UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->update($data);
		
	    $dataall=$this->UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->first()->toArray();
		$this->UserTrainingCatalogHistory::insert($dataall);
		
		$this->UsertrainingCatalog::where('user_training_id','!=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->where('training_status_id','=',1)->delete();
		
	}
	
	/* public function updateTrainingStatusOLD($course_id, $user_id, $file_id, $user_training_id=0, $company_id){
        $param_arr=array();
		$sqlSelect = "select utc.user_training_id, utc.item_id, utc.learning_plan_id, utc.blended_program_id, utc.company_id, im.credit_value from user_training_catalog utc join item_master as im on im.item_id = utc.item_id where utc.item_id = :item_id AND utc.user_id = :user_id AND utc.user_training_id = :user_training_id AND utc.company_id = :company_id ";

		$param_arr['item_id']			=$item_id;
		$param_arr['user_id']			=$user_id;
		$param_arr['user_training_id']	=$user_training_id;
		$param_arr['company_id']		=$company_id;
		$data=DB::select(DB::raw($sqlSelect) ,$param_arr);
		
		date_default_timezone_set('America/Los_Angeles');
		if(count($data)>0){
			$data = json_decode(json_encode($data),true);
			foreach($data as  $rowvalue ){
				$training_id = $rowvalue['user_training_id'];
				$item_id = $rowvalue['item_id'];
				$learning_plan_id = @$rowvalue['learning_plan_id'];
				$blended_id = @$rowvalue['blended_program_id'];
				$company_id = $rowvalue['company_id'];
				$credit_value = $rowvalue['credit_value'];
				$completion_time = date("Y-m-d");
				
				$param_arr1=array();
				$sqlcheck = "select * from user_transcript where user_id = :user_id AND item_id = :item_id and company_id = :company_id";
				$param_arr1['user_id']			=$user_id;
				$param_arr1['item_id']			=$item_id;
				$param_arr1['company_id']		=$company_id;
				$update =DB::select(DB::raw($sqlcheck) ,$param_arr1);
				
				
				$transcript_id = 0;
				
				if(isset($update[0]->transcript_id)){
					$update=$update[0];
					
					$transcript_id = $update->transcript_id;
					$history_transcript_id = $update->transcript_id;
					$history_class_schedule_id = isset($update->class_schedule_id) ? $update->class_schedule_id : '0';
					$history_class_id = $update->class_id;
					$history_registration_id = $update->registration_id;
					$history_is_blended = $update->is_blended;
					$history_user_id = $update->user_id;
					$history_item_id = $update->item_id;
					$history_user_training_id = $update->user_training_id;
					$history_company_id = $update->company_id;
					$history_credit_value = $update->credit_value;
					$history_completion_date = $update->completion_date;
					$history_expiration_date = $update->expiration_date;
					$history_times = $update->times;
					$history_credit_from = $update->credit_from;
					$history_is_approved = isset($update->is_approved) ? $update->is_approved : '0';
					$history_is_active = $update->is_active;
					$history_is_expired = $update->is_expired;
					$history_last_modified_by = $update->last_modified_by;
					$history_last_modified_date = $update->last_modified_date;
					
					//User Transcript
					$sqlInsert = "insert into user_transcript_history (transcript_id, class_schedule_id, class_id, registration_id, is_blended, user_id, item_id, user_training_id, company_id, credit_value, completion_date, expiration_date, times, credit_from, is_approved, is_active, is_expired, last_modified_by) VALUES ('".$history_transcript_id."', '".$history_class_schedule_id."', '".$history_class_id."', '".$history_registration_id."', '".$history_is_blended."', ".$history_user_id.", ".$history_item_id.", ".$history_user_training_id.", ".$history_company_id.", '".$history_credit_value."', '".$history_completion_date."', '".$history_expiration_date."', '".$history_times."', '".$history_credit_from."', '".$history_is_approved."', '".$history_is_active."', '".$history_is_expired."', '".$history_last_modified_by."' )";
					
					//insert..
					DB::insert($sqlInsert);
				
					
					//Update User Transcript
					$updateTranscript = "update user_transcript set is_active= 1, completion_date = '".$completion_time."', credit_value = '".$credit_value."', user_training_id = ".$user_training_id." WHERE transcript_id = 
					".$history_transcript_id." and company_id = ".$company_id;
					//update..
					DB::update($updateTranscript);
			
				
					//User training catalog

					$user_training_arr=array();
					$sqlSelect = "select * from user_training_catalog where item_id = :item_id AND user_id = :user_id AND user_training_id = :user_training_id AND company_id = :company_id and training_status_id=1";
					$user_training_arr['item_id']			=$item_id;
					$user_training_arr['user_id']			=$user_id;
					$user_training_arr['user_training_id']	=$user_training_id;
					$user_training_arr['company_id']		=$company_id;
					$data =DB::select(DB::raw($sqlSelect) ,$user_training_arr);
				
					if(count($data)){
						
						$history_user_training_id = $data[0]->user_training_id;
						$history_training_title = $data[0]->training_title;
						$history_class_schedule_id = $data[0]->class_schedule_id;
						$history_user_program_type_id = $data[0]->user_program_type_id;
						$history_program_id = $data[0]->program_id;
						$history_item_id = $data[0]->item_id;
						$history_learning_plan_id = $data[0]->learning_plan_id;
						$history_blended_program_id = $data[0]->blended_program_id;
						$history_assignment_id = $data[0]->assignment_id;
						$history_is_approved = $data[0]->is_approved; 
						$history_is_enroll = $data[0]->is_enroll;
						$history_user_id = $data[0]->user_id; 
						$history_avg_rating = isset($data[0]->avg_rating) ? $data[0]->avg_rating : 0;
						$history_status_id = isset($data[0]->status_id) ? $data[0]->status_id : 0;
						$history_company_id = $data[0]->company_id;
						$history_training_date = isset($data[0]->training_date) ? $data[0]->training_date : '0000-00-00';
						$history_trainig_time = isset($data[0]->trainig_time) ? $data[0]->trainig_time : '00:00:00';
						$history_session_id = $data[0]->session_id;
						$history_training_status_id = $data[0]->training_status_id;
						$history_training_type_id = $data[0]->training_type_id;
						$history_last_modfied_by = $data[0]->last_modfied_by; 
						$history_last_modified_time = $data[0]->last_modified_time;
						$history_is_active = $data[0]->is_active; 
						$history_created_date = $data[0]->created_date;
						
						
						$sqlInsertIntoHistory = "INSERT INTO user_training_catalog_history(user_training_id, training_title, class_schedule_id, user_program_type_id, program_id, item_id, learning_plan_id, blended_program_id, assignment_id, is_approved, is_enroll, user_id, avg_rating, status_id, company_id, training_date, trainig_time, session_id, training_status_id, training_type_id, last_modfied_by, last_modified_time, is_active, created_date) VALUES('".$history_user_training_id."', '".$history_training_title."', '".$history_class_schedule_id."', '".$history_user_program_type_id."', '".$history_program_id."', '".$history_item_id."', '".$history_learning_plan_id."', '".$history_blended_program_id."', '".$history_assignment_id."', '".$history_is_approved."', '".$history_is_enroll."', '".$history_user_id."', '".$history_avg_rating."', '".$history_status_id."', '".$history_company_id."', '".$history_training_date."', '".$history_trainig_time."', '".$history_session_id."', '".$history_training_status_id."', '".$history_training_type_id."', '".$history_last_modfied_by."', '".$history_last_modified_time."', '".$history_is_active."', '".$history_created_date."')";
						DB::insert($sqlInsertIntoHistory);
						
						
					}
					
					$sqlquery = "update user_training_catalog set training_status_id = 1 where item_id = ".$item_id." AND user_id = ".$user_id." and user_training_id = ".$user_training_id;
					DB::update($sqlquery);
					
					
				}else{
					$user_transcript_arr=array();
					
					
					 $user_transcript_arr['registration_id']=@$blended_id;
					 $user_transcript_arr['user_id']=$user_id;
					 $user_transcript_arr['item_id']=$item_id;
					 $user_transcript_arr['user_training_id']=$training_id;
					 $user_transcript_arr['company_id']=$company_id;
					 $user_transcript_arr['credit_value']=$credit_value;
					 $user_transcript_arr['completion_date']=$completion_time;
					 $user_transcript_arr['expiration_date']='';
					 $user_transcript_arr['times']=100;
					 $user_transcript_arr['is_active']=1;
					 $user_transcript_arr['last_modified_by']=$user_id;
					 
					 $transcript_id = DB::table('user_transcript')->insertGetId($user_transcript_arr);
					
					//this code updates the transcript_id column in sco_score_data
					
					$sql=NULL;
					$check_sco=ScoScoreData::where('transcript_id','=',$transcript_id)->select('scorm_data_id')->first();

					if($check_sco==false){
						
						$scorm_data_id=ScoScoreData::where('user_id','=',$user_id)->where('item_id','=',$item_id)->where('user_training_id','=',$training_id)->select('scorm_data_id')->first();

						if($scorm_data_id!=false && $scorm_data_id!='' && $scorm_data_id!=NULL){
							
							$sco_score_arr=array();
							$sql="update sco_score_data set transcript_id=:transcript_id where scorm_data_id=:scorm_data_id";
							$sco_score_arr['transcript_id']	    =$transcript_id;
							$sco_score_arr['scorm_data_id']		=$scorm_data_id['scorm_data_id'];
							DB::update(DB::raw($sql) ,$sco_score_arr);
							
						}
					}
					
					$sqlquery = "update user_training_catalog set training_status_id = 1 where item_id = ".$item_id." AND user_id = ".$user_id." and user_training_id = ".$user_training_id;
					//update..
					DB::update($sqlquery);
					
				}
				

			}
		}	
	} */
	
	public function scorm_update_grades($item_id, $userid, $file_id, $user_training_id=0, $company_id) {
		if ($item_id != null) {
			
			$sco_score_arr	=array();
			$sql = "select * from scorm_score_data where user_id = :user_id AND file_id= :file_id AND item_id = :item_id AND user_training_id = :user_training_id";
			
			$sco_score_arr['user_id']	    	=$userid;
			$sco_score_arr['file_id']			=$file_id;
			$sco_score_arr['item_id']			=$item_id;
			$sco_score_arr['user_training_id']	=$user_training_id;
			$tracks = DB::select(DB::raw($sql) ,$sco_score_arr);
			
			$usertrack = new stdClass;
			$usertrack->userid = $userid;
			$usertrack->file_id = $file_id;
			$usertrack->score_raw = '0';
			$usertrack->status = '1';
			$usertrack->total_time = '00:00:00';
			$usertrack->session_time = '00:00:00';
			$usertrack->timemodified = 0;
			$usertrack->statusscore = 'failed';
			$usertrack->ispassfail  = 0;
			$usertrack->maxscore	= 100;
			
			if($tracks){
				$tracks = json_decode(json_encode($tracks),true);
				
				foreach ($tracks as $track) {
					$element = $track['element'];
					$value = $this->stripslashes_safe($track['value']);
					$usertrack->{$element} = $value;
					switch ($element) {
							case 'cmi.core.lesson_status':
								$usertrack->statusscore = $track['value'];
								break;
							case 'cmi.completion_status':
									if ($track['value'] == 'not attempted') {
											$track['value'] = 'notattempted';
									}
									$usertrack->status = $track['value'];
							break;
							case 'cmi.core.score.raw':
								$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
								$usertrack->ispassfail  = 1;
								break;
							case 'cmi.score.raw':
									$usertrack->score_raw = (float) sprintf('%2.2f', $track['value']);
									$usertrack->ispassfail  = 1;
									break;
							case 'cmi.core.session_time':
							case 'cmi.session_time':
									$usertrack->session_time = $track['value'];
							break;
							case 'cmi.core.total_time':
							case 'cmi.total_time':
									$usertrack->total_time = $track['value'];
							break;
							case 'cmi.score.max' :
								$maxscore = $track['value'];
								$usertrack->maxscore = $maxscore;
							break;		
							case 'cmi.success_status':	
							if($usertrack->score_raw == '0'){
								global $correctAnsCount;
								global $incorrectAnsCount;
								$maxscore = (isset($maxscore) ? $maxscore : 100);
								$usertrack->score_raw	= ($correctAnsCount / ($correctAnsCount + $incorrectAnsCount)) * $maxscore;
								$usertrack->ispassfail  = 1;
								$usertrack->statusscore = $track['value'];
							}
							break;
					}
					if (isset($track['timemodified']) && ($track['timemodified'] > $usertrack->timemodified)) {
							$usertrack->timemodified = $track['timemodified'];
					}
				}
			}
			
			$grades = $usertrack->score_raw;
			
		  
		   $this->scorm_grade_item_update($item_id, $usertrack, $file_id, $userid, $user_training_id, $company_id);
		}
	}
	
	public function scorm_grade_item_update($item_id, $usertrack=NULL,$file_id,$userid, $user_training_id, $company_id){
		
		$status = 1;
		$item_id = $item_id;
		$user_id = $userid;
		
		$status1 = $usertrack->statusscore;
		
		$totalQuestions = 100;
		$value = $usertrack->score_raw;
		if($value > 0){
			$maxScore = 100;
			
			
			$passscore = $this->getpasscore($file_id, $item_id);
		  
			if($passscore > 0 || $usertrack->ispassfail == 1){
				if($value >= $passscore){
					$status = 1;
				}else{
					if($status1 != ''){
						$statuspass = array('pass','passed','completed'); 
						if($usertrack->ispassfail == 1 && in_array(strtolower($status1), $statuspass)){
							$status = 1;
						}else if($usertrack->ispassfail == 1){
							$status = 0;
						}else{
							$status = 2;
						}
					}else{
						$status = 2;
					}
				}
			}else{
				$status = 2;
			}
			
           $runquerycheck=$this->ScoScoreData::where('user_id','=',$user_id)->where('item_id','=',$item_id)->where('user_training_id','=',$user_training_id)->first();
		 
			if($runquerycheck){
				
				$sqldataInsert = "update sco_score_data set correct_answer = '".$value."', status ='".$status."' where user_training_id= '".$user_training_id."' AND user_id = '".$user_id."' AND item_id = '".$item_id."'";
				DB::update($sqldataInsert);
				
			}else{
				$sqldataInsert = "insert into sco_score_data(scorm_data_id, user_training_id, user_id, item_id, max_score, total_questions, correct_answer, status, company_id) VALUES(NULL, '".$user_training_id."', '".$user_id."', '".$item_id."', '".$maxScore."', '".$totalQuestions."', '".$value."', '".$status."', '".$company_id."')";
				DB::insert($sqldataInsert);
				
			}
		}
	
		return null;
	}
	
	public function getpasscore($file_id){
        $files_array=array();
		$sqlQuery = "select masteryscore, company_id from uploaded_files where files_id = :file_id"; 
		$files_array['file_id']=$file_id;
		$data = DB::select(DB::raw($sqlQuery) ,$files_array);
		
		if($data){
			$data = json_decode(json_encode($data),true);
			foreach ($data as $value) {
				$masteryscore = $value['masteryscore'];
				$company_id = $value['company_id'];
			}
			return $masteryscore;
		}
		return $masteryscore;
	}
	
	public function updateTrainingProgressPercentage($item_id, $userid, $file_id, $attempt, $element, $user_training_id){
	
		 $track1 = $this->get_record_select('scorm_score_data'," user_id=$userid AND item_id=$item_id AND file_id=$file_id AND attempt=$attempt AND user_training_id='$user_training_id' AND element='cmi.completion_status'");
		
		if($track1 == ''){
			$track1 = $this->get_record_select('scorm_score_data'," user_id=$userid AND item_id=$item_id AND file_id=$file_id AND attempt=$attempt AND user_training_id='$user_training_id' AND element='cmi.core.lesson_status'");
		} 

		
		 if($track1[0]->value =='incomplete' || $track1[0]->value =='failed'){
		  $data1 =UploadedFile::where('files_id','=',$file_id)->select('title')->first();

		if(isset($data1->title)) $title = $data1->title;
		
		$track = $this->get_record_select('scorm_score_data'," user_id=$userid AND item_id=$item_id AND file_id=$file_id AND attempt=$attempt AND user_training_id='$user_training_id' AND element='cmi.suspend_data'");
		
		$trackloc = $this->get_record_select('scorm_score_data'," user_id=$userid AND item_id=$item_id AND file_id=$file_id AND attempt=$attempt AND user_training_id='$user_training_id' AND element='cmi.core.lesson_location'");
		
		$numberOfSlides = 0;
		$vivewSlides = 0;
		$totalSlides = 0;
		if($trackloc != ''){
			$numberOfSlides = $trackloc[0]->value;
		}
		
		$progressdata = isset($track[0]->value) ? $track[0]->value : '';
		$percentageProgress = 0;
		if(strstr($progressdata,'viewed')){
				if($progressdata != ''){
					$slideNum = 0;
					$progData1 = explode('|', $progressdata);
					$lastvived1 = $progData1[1];
					$slideNums = explode('=', $lastvived1);
					$slideNum = (int)$slideNums[1];
					$total1 = $progData1[2];
					$numTotal1 = explode('#', $total1);
					$slides = explode(',', $numTotal1[3]);
					$totalSlides = count($slides)-1;
					$vivewSlides = $slideNum;
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($slideNum/$totalSlides)*100);
				}
		   }else if(strstr($progressdata, 'toc')){
			   if($progressdata != ''){
					$slideNum = 0;
					$progData1 = explode(':', $progressdata);
					$lastvived1 = $progData1[0];
					$progData11 = explode('`', $lastvived1);
					$slides = $progData11[1];
					$numSlides1 = explode(',', $slides);
					$totalSlides = count($numSlides1);
					$vivedSlides = 0;
					for($x=0; $x<$totalSlides; $x++){
						if($numSlides1[$x]== '1'){
							$vivedSlides++;
						}
					}
					$vivewSlides = $vivedSlides;
					
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivedSlides/$totalSlides)*100);
				}

		}else if(strstr($progressdata,'Enone')){
			if($progressdata != ''){
					$slideNum = 0;
					$progData1 = explode('Enone', $progressdata);
					$totalSlides = count($progData1)-1;
					
					$progData2 = explode('P1Enone', $progressdata);
					$vivewSlides = count($progData2);
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivewSlides/$totalSlides)*100);
				}
		}else if(strstr($progressdata,'AA000A') || strstr($title, 'Adobe_Presenter_Quiz')){
			if($progressdata != ''){
					$numlen = strrpos($progressdata, "000");
					$str2 = substr($progressdata, 0, $numlen);
					$progData1 = explode('100', $str2);

					$progData2 = explode('1', $progData1[0]);
					$slidescount = count($progData2);

					$totalData1 = explode('0', $progData1[1]);
					$totalSlides = count($totalData1)+1;

					$vivewSlides = $slidescount - 1;
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivewSlides/$totalSlides)*100);
				}
		}else if(strstr($progressdata,'A%24n')){
			if($progressdata != ''){
					$raw_data = explode('A%24n',$progressdata);
					$vivewSlides = $totalSlides =0;
					foreach($raw_data as $key=>$value){
						$check_point = substr($value,0,2);
						if($check_point=='P1'){
							$vivewSlides++;
						}
						$totalSlides++;
						
					}
					
					if($vivewSlides > $totalSlides){
						$vivewSlides = $totalSlides;
					}
					$percentageProgress = floor(($vivewSlides/$totalSlides)*100);
				}
		}else{
				if($progressdata != ''){
					$slideNum = 0;
					$pos = strpos($progressdata, '$');
					if ($pos !== false) {
						$progData1 = explode('$', $progressdata);						
					}
					
					$pos = strpos($progressdata, '|');
					if ($pos !== false) {
						$progData1 = explode('|', $progressdata);						
					}
					
					$pos = strpos($progressdata, ':');
					if ($pos !== false) {
						$progData1 = explode(':', $progressdata);						
					}
					
					$lastvived1 = $totalSlides = $percentageProgress = 0;
					
					if(isset($progData1[0])) {
						$lastvived1 = (int)$progData1[0];
					}
					
					if(isset($progData1[1])){
						$pos = strpos($progData1[1], ',');
						if ($pos !== false) {
							$progData11 = explode(',', $progData1[1]);
							$totalSlides = count($progData11);
						}else{
							$totalSlides = strlen($progData1[1]);
						}
						if($totalSlides!=0){
							$percentageProgress = floor(($lastvived1/$totalSlides)*100);
						}
					}
				}
		} 
		
		if($percentageProgress){
        $this->programinpercentageAction($user_training_id,$userid,$item_id,$percentageProgress,$file_id,$attempt);   
		}
		/* $item_array=array();
		$sqlQuery = "select utc.user_training_id, utc.item_id, utc.learning_plan_id, utc.blended_program_id, utc.company_id, im.credit_value from user_training_catalog utc join item_master as im on im.item_id = utc.item_id where utc.item_id = :item_id AND utc.user_id =:user_id AND user_training_id = :user_training_id"; 
		$item_array['item_id']			=$item_id;
		$item_array['user_id']			=$userid;
		$item_array['user_training_id']	=$user_training_id;
		$data = DB::select(DB::raw($sqlQuery) ,$item_array); */
		
		/* if(count($data)>0 && $percentageProgress > 0){
				$data = json_decode(json_encode($data),true);
				foreach($data as  $rowvalue ){
					$training_id = $rowvalue['user_training_id'];
					$item_id = $rowvalue['item_id'];
					$learning_plan_id = @$rowvalue['learning_plan_id'];
					$blended_id = @$rowvalue['blended_program_id'];
					$company_id = $rowvalue['company_id'];
					$credit_value = $rowvalue['credit_value'];
					$completion_time = time();
					date_default_timezone_set('America/Los_Angeles');
					
					$trainingstartdatetime = time();
					$sqlquery = "update user_training_history set is_active = 0 where item_id = ".$item_id." AND user_id = ".$userid." and user_training_id = ".$training_id;
					DB::update($sqlquery);
					
					
					$sqlInsert = "insert into user_training_history (id,user_training_id,user_id,training_start_date_time,php_session_id,course_completion_percentage,item_id)
					VALUES (NULL,'".@$training_id."',".$userid.",now(),'','".$percentageProgress."',".$item_id.")";
					DB::insert($sqlInsert);
					
					
				   
				}
				
			} */
		} 
		
	}
	public function programinpercentageAction($usertrainingid,$userid,$course_id,$percentage,$file_id,$attempt)
	{
	
	    $timemodified=$this->ScormScoreData::where('user_id','=',$userid)->where('item_id','=',$course_id)->where('user_training_id','=',$usertrainingid)
		->where('element','=','x.start.time')->where('file_id','=',$file_id)->where('attempt','=',$attempt)->select('timemodified')->first();
	    $launchtime=isset($timemodified->timemodified) ? date('Y-m-d H:i:s',$timemodified->timemodified) : '0000-00-00';
	  
		$data['training_status_id']=3;
		$data['is_enroll']=1; 
		$this->UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->update($data);
        $records=$this->UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('status_id','=',1)->first();
           
		$data1['user_training_id']=$usertrainingid;
		$data1['user_id']=$userid;
		$data1['course_id']=$course_id;
		$data1['training_start_date_time']=$launchtime;
		$data1['course_completion_percentage']=$percentage;
		$data1['training_end_date_time']=date('Y-m-d H:i:s');
		$data1['php_session_id']=Session::getId();
		$data1['status_id']=1;
		if(count($records)==0){
			$this->UserTrainingHistory::insert($data1);
		
		}else{
			$datah['status_id']=0;
			$datah['php_session_id']=Session::getId();
			$this->UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->update($datah);
			$this->UserTrainingHistory::insert($data1);
		}
	}
	
	//-------------------//
	/* 
	
	public function updateUserRequirementData($item_id, $userid, $compid, $statusval){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		$sqlquery="select SPLIT_STR(im.training_code,'#_',2) as part_number from item_master as im where im.item_id='$item_id' and im.company_id=$compid and im.is_active=1";
		$datapart =$db->fetchRow($sqlquery);
		$part_number=$datapart['part_number'];
		

		$detailpass=$this->checkUserRequiremntDetail($part_number,$userid,$compid);
		

		if($statusval == 'failed'){
		$test_passed = 0;
		}else{
		$test_passed = 1;
		}
		
		$model = new Model_launchtraining_Launchtraining();
		
		$datainfor['test_passed']      = $test_passed;
		$datainfor['completion_date']  = date('Y-m-d');
		$whereinfo = "user_id = '$userid' and company_id = '$compid' and part_number='$part_number'";
		$model->updateanywhere('user_requirement_detail' , $datainfor , $whereinfo);
	     return 'success';
	}
	
	public function checkUserRequiremntDetail($part_number,$uid,$company_id)
	{
	     $db = Zend_Db_Table::getDefaultAdapter();
		 $db->setFetchMode(Zend_Db::FETCH_ASSOC);
		 $sqlquery="select u.*,SPLIT_STR(ui.user_identity,'#_',2) as customer_no from user_requirement_detail as u 
		 join user_identifier as ui on ui.user_id=u.user_id and ui.company_id=$company_id
		 where u.user_id=$uid and u.company_id=$company_id and u.is_active=1 and u.part_number='$part_number'";
		 $data =$db->fetchRow($sqlquery);
	     return $data;
	}
	
	 */
	 
	 public function updateCMIInteractionsQuesAns($userid,$item_id,$file_id,$questionAnsArray, $correctAnsCount, $incorrectAnsCount, $total_questions, $user_training_id, $company_id){
	
		if($questionAnsArray != null){
			$this->updateScromQuestionAnsResult($item_id, $userid,$file_id,$questionAnsArray, $correctAnsCount, $incorrectAnsCount, $total_questions, $user_training_id, $company_id);
		}
	}

	public function updateScromQuestionAnsResult($item_id, $userid, $file_id, $questionAnsArray, $correctAnsCount, $incorrectAnsCount, $total_questions, $user_training_id=0, $company_id)
	{
		
		$qtrack = null;
		$param_array=array();
        $qtrack=$this->ScormQuestionsLog::where('item_id','=',$item_id)->where('user_id','=',$userid)->where('file_id','=',$file_id)->where('user_training_id','=',$user_training_id)->select('id')->first();
		
		 if($qtrack){
			$scormtracldata['questions_list'] =  json_encode($questionAnsArray);
			$scormtracldata['total_questions'] =  $total_questions;
			$scormtracldata['total_correct_questions'] =  $correctAnsCount;
			$scormtracldata['total_incorrect_questions'] =  $incorrectAnsCount;
		
			$this->ScormQuestionsLog::where('id','=',$qtrack->id)->update($scormtracldata); 
		}else{	
			 $scormtracldata['user_id'] =  $userid;
			$scormtracldata['item_id'] =  $item_id;
			$scormtracldata['file_id'] =  $file_id;
			$scormtracldata['company_id'] =  $company_id;
			$scormtracldata['questions_list'] =  json_encode($questionAnsArray);
			$scormtracldata['total_questions'] =  $total_questions;
			$scormtracldata['total_correct_questions'] =  $correctAnsCount;
			$scormtracldata['total_incorrect_questions'] =  $incorrectAnsCount;
			$scormtracldata['user_training_id'] =  $user_training_id;
			$this->ScormQuestionsLog::insert($scormtracldata);  
		}  
	}
	
	public function scorm_get_resources($blocks) {
		$resources = array();
		foreach ($blocks as $block) {
			if ($block['name'] == 'RESOURCES') {
				foreach ($block['children'] as $resource) {
					if ($resource['name'] == 'RESOURCE'){
						$resources[$this->addslashes_js($resource['attrs']['IDENTIFIER'])] = $resource['attrs'];
					}
				}
			}
		}
		return $resources;
	}
	
	public function scorm_get_manifest($blocks,$scoes) {
		
		static $parents = array();
		static $resources;
		static $manifest;
		static $organization;
		
		if (count($blocks) > 0) {
			foreach ($blocks as $block){
				switch ($block['name']){						
					case 'METADATA':
						if (isset($block['children'])){					
							foreach ($block['children'] as $metadata) {
								if ($metadata['name'] == 'SCHEMAVERSION' ||$metadata['name'] == 'schemaversion') {
									if (empty($scoes->version)) {
										if (isset($metadata['tagData']) && (preg_match("/^(1\.2)$|^(CAM )?(1\.3)$/",$metadata['tagData'],$matches))) {										
											$scoes->version = 'SCORM_'.$matches[count($matches)-1];
										} else {										
											if (isset($metadata['tagData']) && (preg_match("/^2004 (3rd|4th) Edition$/",$metadata['tagData'],$matches))) {
												$scoes->version = 'SCORM_1.3';
											} else {
												$scoes->version = 'SCORM_1.2';
											}
										}
									}else{
										$scoes->version = 'SCORM_1.5';
									}
								}
							}
						}
					break;
					case 'MANIFEST':
						$manifest = $this->addslashes_js($block['attrs']['IDENTIFIER']);
						$organization = '';
						$resources = array();
						$resources = $this->scorm_get_resources($block['children']);
						$scoes = $this->scorm_get_manifest($block['children'],$scoes);
						if (count($scoes->elements) <= 0) {
							foreach ($resources as $item => $resource) {
								if (!empty($resource['HREF'])) {
									$sco = new \stdClass();
									$sco->identifier = $item;
									$sco->title = $item;
									$sco->parent = '/';
									$sco->launch = $this->addslashes_js($resource['HREF']);
									$sco->scormtype = $this->addslashes_js($resource['ADLCP:SCORMTYPE']);
									$scoes->elements[$manifest][$organization][$item] = $sco;
								}
							}
						}
					break;
					case 'ORGANIZATIONS':
						if (!isset($scoes->defaultorg) && isset($block['attrs']['DEFAULT'])){
							$scoes->defaultorg = $this->addslashes_js($block['attrs']['DEFAULT']);
						}
						
						$scoes = $this->scorm_get_manifest($block['children'],$scoes);
					break;
					case 'ORGANIZATION':
						$identifier = $this->addslashes_js($block['attrs']['IDENTIFIER']);
						$organization = '';
						@$scoes->elements[$manifest][$organization][$identifier]->parent = '/';
						@$scoes->elements[$manifest][$organization][$identifier]->launch = '';
						@$scoes->elements[$manifest][$organization][$identifier]->scormtype = '';

						$parents = array();
						$parent = new \stdClass();
						$parent->identifier = $identifier;
						$parent->organization = $organization;
						array_push($parents, $parent);
						$organization = $identifier;
						$scoes = $this->scorm_get_manifest($block['children'], $scoes);
						array_pop($parents);
					break;
					case 'ITEM':
						$parent = array_pop($parents);
						array_push($parents, $parent);

						$identifier = $this->addslashes_js($block['attrs']['IDENTIFIER']);
						@$scoes->elements[$manifest][$organization][$identifier]->identifier = $identifier;
						@$scoes->elements[$manifest][$organization][$identifier]->parent = $parent->identifier;
						if (!isset($block['attrs']['ISVISIBLE'])) {
							$block['attrs']['ISVISIBLE'] = 'true';
						}
						$scoes->elements[$manifest][$organization][$identifier]->isvisible = $this->addslashes_js($block['attrs']['ISVISIBLE']);
						if (!isset($block['attrs']['PARAMETERS'])) {
							$block['attrs']['PARAMETERS'] = '';
						}
						$scoes->elements[$manifest][$organization][$identifier]->parameters = $this->addslashes_js($block['attrs']['PARAMETERS']);
						if (!isset($block['attrs']['IDENTIFIERREF'])) {
							$scoes->elements[$manifest][$organization][$identifier]->launch = '';
							$scoes->elements[$manifest][$organization][$identifier]->scormtype = 'asset';
						} else {
							$idref = $this->addslashes_js($block['attrs']['IDENTIFIERREF']);
							$base = '';
							if (isset($resources[$idref]['XML:BASE'])) {
								$base = $resources[$idref]['XML:BASE'];
								//echo $base;
								//exit("base...");
							}
							$scoes->elements[$manifest][$organization][$identifier]->launch = $this->addslashes_js($base.$resources[$idref]['HREF']);
							if (empty($resources[$idref]['ADLCP:SCORMTYPE'])) {
								$resources[$idref]['ADLCP:SCORMTYPE'] = 'asset';
							}
							$scoes->elements[$manifest][$organization][$identifier]->scormtype = $this->addslashes_js($resources[$idref]['ADLCP:SCORMTYPE']);
						}


						///Assigning values for further user.
						$parent = new \stdClass();
						$parent->identifier = $identifier;
						$parent->organization = $organization;
						array_push($parents, $parent);
						$scoes = $this->scorm_get_manifest($block['children'],$scoes);
						array_pop($parents);
					break;
					case 'TITLE':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->title = $this->addslashes_js($block['tagData']);
					break;
					case 'ADLCP:PREREQUISITES':
						if ($block['attrs']['TYPE'] == 'aicc_script') {
							$parent = array_pop($parents);
							array_push($parents, $parent);
							if (!isset($block['tagData'])) {
								$block['tagData'] = '';
							}
							$scoes->elements[$manifest][$parent->organization][$parent->identifier]->prerequisites = $this->addslashes_js($block['tagData']);						
						}
					break;
					case 'ADLCP:MAXTIMEALLOWED':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->maxtimeallowed = $this->addslashes_js($block['tagData']);
					break;
					case 'ADLCP:TIMELIMITACTION':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->timelimitaction = $this->addslashes_js($block['tagData']);
					break;
					case 'ADLCP:DATAFROMLMS':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->datafromlms = $this->addslashes_js($block['tagData']);
					break;
					case 'ADLCP:MASTERYSCORE':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->masteryscore = $this->addslashes_js($block['tagData']);
					break;
					case 'ADLCP:COMPLETIONTHRESHOLD':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->threshold = $this->addslashes_js($block['tagData']);
					break;
					case 'ADLNAV:PRESENTATION':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!empty($block['children'])) {
							foreach ($block['children'] as $adlnav) {
								if ($adlnav['name'] == 'ADLNAV:NAVIGATIONINTERFACE') {
									foreach ($adlnav['children'] as $adlnavInterface) {
										if ($adlnavInterface['name'] == 'ADLNAV:HIDELMSUI') {
											if ($adlnavInterface['tagData'] == 'continue') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hidecontinue = 1;
											}
											if ($adlnavInterface['tagData'] == 'previous') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideprevious = 1;
											}
											if ($adlnavInterface['tagData'] == 'exit') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideexit = 1;
											}
											if ($adlnavInterface['tagData'] == 'exitAll') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideexitall = 1;
											}
											if ($adlnavInterface['tagData'] == 'abandon') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideabandon = 1;
											}
											if ($adlnavInterface['tagData'] == 'abandonAll') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideabandonall = 1;
											}
											if ($adlnavInterface['tagData'] == 'suspendAll') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hidesuspendall = 1;
											}
										}
									}
								}
							}
						}
					break;
					case 'IMSSS:SEQUENCING':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!empty($block['children'])) {
							foreach ($block['children'] as $sequencing) {
								if ($sequencing['name']=='IMSSS:CONTROLMODE') {
									if (isset($sequencing['attrs']['CHOICE'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->choice = $sequencing['attrs']['CHOICE'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['CHOICEEXIT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->choiceexit = $sequencing['attrs']['CHOICEEXIT'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['FLOW'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->flow = $sequencing['attrs']['FLOW'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['FORWARDONLY'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->forwardonly = $sequencing['attrs']['FORWARDONLY'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['USECURRENTATTEMPTOBJECTINFO'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->usecurrentattemptobjectinfo = $sequencing['attrs']['USECURRENTATTEMPTOBJECTINFO'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['USECURRENTATTEMPTPROGRESSINFO'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->usecurrentattemptprogressinfo = $sequencing['attrs']['USECURRENTATTEMPTPROGRESSINFO'] == 'true'?1:0;
									}
								}
								if ($sequencing['name']=='ADLSEQ:CONSTRAINEDCHOICECONSIDERATIONS') {
									if (isset($sequencing['attrs']['CONSTRAINCHOICE'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->constrainChoice = $sequencing['attrs']['CONSTRAINCHOICE'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['PREVENTACTIVATION'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->preventactivation = $sequencing['attrs']['PREVENTACTIVATION'] == 'true'?1:0;
									}
								}
								if ($sequencing['name']=='IMSSS:OBJECTIVES') {
									$objectives = array();
									foreach ($sequencing['children'] as $objective) {
										$objectivedata = new \stdClass();
										$objectivedata->primaryobj = 0;
										switch ($objective['name']) {
											case 'IMSSS:PRIMARYOBJECTIVE':
												$objectivedata->primaryobj = 1;
											case 'IMSSS:OBJECTIVE':
												$objectivedata->satisfiedbymeasure = 0;
												if (isset($objective['attrs']['SATISFIEDBYMEASURE'])) {
													$objectivedata->satisfiedbymeasure = $objective['attrs']['SATISFIEDBYMEASURE']== 'true'?1:0;
												}
												$objectivedata->objectiveid = '';
												if (isset($objective['attrs']['OBJECTIVEID'])) {
													$objectivedata->objectiveid = $objective['attrs']['OBJECTIVEID'];
												}
												$objectivedata->minnormalizedmeasure = 1.0;
												if (!empty($objective['children'])) {
													$mapinfos = array();
													foreach ($objective['children'] as $objectiveparam) {
														if ($objectiveparam['name']=='IMSSS:MINNORMALIZEDMEASURE') {
															if (isset($objectiveparam['tagData'])) {
																$objectivedata->minnormalizedmeasure = $objectiveparam['tagData'];
															} else {
																$objectivedata->minnormalizedmeasure = 0;
															}
														}
														if ($objectiveparam['name']=='IMSSS:MAPINFO') {
															$mapinfo = new \stdClass();
															$mapinfo->targetobjectiveid = '';
															if (isset($objectiveparam['attrs']['TARGETOBJECTIVEID'])) {
																$mapinfo->targetobjectiveid = $objectiveparam['attrs']['TARGETOBJECTIVEID'];
															}
															$mapinfo->readsatisfiedstatus = 1;
															if (isset($objectiveparam['attrs']['READSATISFIEDSTATUS'])) {
																$mapinfo->readsatisfiedstatus = $objectiveparam['attrs']['READSATISFIEDSTATUS'] == 'true'?1:0;
															}
															$mapinfo->writesatisfiedstatus = 0;
															if (isset($objectiveparam['attrs']['WRITESATISFIEDSTATUS'])) {
																$mapinfo->writesatisfiedstatus = $objectiveparam['attrs']['WRITESATISFIEDSTATUS'] == 'true'?1:0;
															}
															$mapinfo->readnormalizemeasure = 1;
															if (isset($objectiveparam['attrs']['READNORMALIZEDMEASURE'])) {
																$mapinfo->readnormalizemeasure = $objectiveparam['attrs']['READNORMALIZEDMEASURE'] == 'true'?1:0;
															}
															$mapinfo->writenormalizemeasure = 0;
															if (isset($objectiveparam['attrs']['WRITENORMALIZEDMEASURE'])) {
																$mapinfo->writenormalizemeasure = $objectiveparam['attrs']['WRITENORMALIZEDMEASURE'] == 'true'?1:0;
															}
															array_push($mapinfos,$mapinfo);
														}
													}
													if (!empty($mapinfos)) {
														$objectivedata->mapinfos = $mapinfos;
													}
												}
											break;
										}
										array_push($objectives,$objectivedata);
									}
									$scoes->elements[$manifest][$parent->organization][$parent->identifier]->objectives = $objectives;
								}
								if ($sequencing['name']=='IMSSS:LIMITCONDITIONS') {
									if (isset($sequencing['attrs']['ATTEMPTLIMIT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->attemptLimit = $sequencing['attrs']['ATTEMPTLIMIT'];
									}
									if (isset($sequencing['attrs']['ATTEMPTABSOLUTEDURATIONLIMIT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->attemptAbsoluteDurationLimit = $sequencing['attrs']['ATTEMPTABSOLUTEDURATIONLIMIT'];
									}
								}
								if ($sequencing['name']=='IMSSS:ROLLUPRULES') {
									if (isset($sequencing['attrs']['ROLLUPOBJECTIVESATISFIED'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->rollupobjectivesatisfied = $sequencing['attrs']['ROLLUPOBJECTIVESATISFIED'] == 'true'?1:0;;
									}
									if (isset($sequencing['attrs']['ROLLUPPROGRESSCOMPLETION'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->rollupprogresscompletion = $sequencing['attrs']['ROLLUPPROGRESSCOMPLETION'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['OBJECTIVEMEASUREWEIGHT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->objectivemeasureweight = $sequencing['attrs']['OBJECTIVEMEASUREWEIGHT'];
									}

									if (!empty($sequencing['children'])){
										$rolluprules = array();
										foreach ($sequencing['children'] as $sequencingrolluprule) {
											if ($sequencingrolluprule['name']=='IMSSS:ROLLUPRULE' ) {
												$rolluprule = new \stdClass();
												$rolluprule->childactivityset = 'all';
												if (isset($sequencingrolluprule['attrs']['CHILDACTIVITYSET'])) {
													$rolluprule->childactivityset = $sequencingrolluprule['attrs']['CHILDACTIVITYSET'];
												}
												$rolluprule->minimumcount = 0;
												if (isset($sequencingrolluprule['attrs']['MINIMUMCOUNT'])) {
													$rolluprule->minimumcount = $sequencingrolluprule['attrs']['MINIMUMCOUNT'];
												}
												$rolluprule->minimumpercent = 0.0000;
												if (isset($sequencingrolluprule['attrs']['MINIMUMPERCENT'])) {
													$rolluprule->minimumpercent = $sequencingrolluprule['attrs']['MINIMUMPERCENT'];
												}
												if (!empty($sequencingrolluprule['children'])) {
													foreach ($sequencingrolluprule['children'] as $rolluproleconditions) {
														if ($rolluproleconditions['name']=='IMSSS:ROLLUPCONDITIONS') {
															$conditions = array();
															$rolluprule->conditioncombination = 'all';
															if (isset($rolluproleconditions['attrs']['CONDITIONCOMBINATION'])) {
																$rolluprule->conditioncombination = $rolluproleconditions['attrs']['CONDITIONCOMBINATION'];
															}
															foreach ($rolluproleconditions['children'] as $rolluprulecondition) {
																if ($rolluprulecondition['name']=='IMSSS:ROLLUPCONDITION') {
																	$condition = new \stdClass();
																	if (isset($rolluprulecondition['attrs']['CONDITION'])) {
																		$condition->cond = $rolluprulecondition['attrs']['CONDITION'];
																	}
																	$condition->operator = 'noOp';
																	if (isset($rolluprulecondition['attrs']['OPERATOR'])) {
																		$condition->operator = $rolluprulecondition['attrs']['OPERATOR'];
																	}
																	array_push($conditions,$condition);
																}
															}
															$rolluprule->conditions = $conditions;
														}
														if ($rolluproleconditions['name']=='IMSSS:ROLLUPACTION') {
															$rolluprule->rollupruleaction = $rolluproleconditions['attrs']['ACTION'];
														}
													}
												}
												array_push($rolluprules, $rolluprule);
											}
										}
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->rolluprules = $rolluprules;
									}
								}

								if ($sequencing['name']=='IMSSS:SEQUENCINGRULES') {
									if (!empty($sequencing['children'])) {
										$sequencingrules = array();
										foreach ($sequencing['children'] as $conditionrules) {
											$conditiontype = -1;
											switch($conditionrules['name']) {
												case 'IMSSS:PRECONDITIONRULE':
													$conditiontype = 0;
												break;
												case 'IMSSS:POSTCONDITIONRULE':
													$conditiontype = 1;
												break;
												case 'IMSSS:EXITCONDITIONRULE':
													$conditiontype = 2;
												break;
											}
											if (!empty($conditionrules['children'])) {
												$sequencingrule = new \stdClass();
												foreach ($conditionrules['children'] as $conditionrule) {
													if ($conditionrule['name']=='IMSSS:RULECONDITIONS') {
														$ruleconditions = array();
														$sequencingrule->conditioncombination = 'all';
														if (isset($conditionrule['attrs']['CONDITIONCOMBINATION'])) {
															$sequencingrule->conditioncombination = $conditionrule['attrs']['CONDITIONCOMBINATION'];
														}
														foreach ($conditionrule['children'] as $rulecondition) {
															if ($rulecondition['name']=='IMSSS:RULECONDITION') {
																$condition = new \stdClass();
																if (isset($rulecondition['attrs']['CONDITION'])) {
																	$condition->cond = $rulecondition['attrs']['CONDITION'];
																}
																$condition->operator = 'noOp';
																if (isset($rulecondition['attrs']['OPERATOR'])) {
																	$condition->operator = $rulecondition['attrs']['OPERATOR'];
																}
																$condition->measurethreshold = 0.0000;
																if (isset($rulecondition['attrs']['MEASURETHRESHOLD'])) {
																	$condition->measurethreshold = $rulecondition['attrs']['MEASURETHRESHOLD'];
																}
																$condition->referencedobjective = '';
																if (isset($rulecondition['attrs']['REFERENCEDOBJECTIVE'])) {
																	$condition->referencedobjective = $rulecondition['attrs']['REFERENCEDOBJECTIVE'];
																}
																array_push($ruleconditions,$condition);
															}
														}
														$sequencingrule->ruleconditions = $ruleconditions;
													}
													if ($conditionrule['name']=='IMSSS:RULEACTION') {
														$sequencingrule->action = $conditionrule['attrs']['ACTION'];
													}
													$sequencingrule->type = $conditiontype;
												}
												array_push($sequencingrules,$sequencingrule);
											}
										}
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->sequencingrules = $sequencingrules;
									}
								}
							}
						}
					break;
				}
			}
		}
		return $scoes;
	}
	
	public function get_scorm_threshold_value($base_url, $threshold='cca')
	{
		$url = $base_url."imsmanifest.xml";
		$threshold_value = 100;
		$folder_name = '';
		$manifest_array = $this->parse_xml_file($url);
		
		$identifierref = (isset($manifest_array['organizations']['organization']['item']['identifierref'])) ? $manifest_array['organizations']['organization']['item']['identifierref'] : '';
		if(!empty($identifierref))
		{
			$identifierref_arr = explode("_",$identifierref);
			if(in_array("enus",$identifierref_arr))
			{
				$content_dir = $base_url."Content/scp/";
				$dir = new DirectoryIterator($content_dir);
				foreach ($dir as $fileinfo)
				{
					if(!$fileinfo->isDot())
					{
						$folder_name = $fileinfo->getFilename();
						if(is_dir($content_dir.$folder_name) && strstr($folder_name, 'SCP_V'))
						{
							$folder_name = $fileinfo->getFilename();
							break;
						}
					}
				}
				
				if(!empty($folder_name))
				{
					$txtProperties = file_get_contents($base_url."Content/scp/$folder_name/PagePlayer.properties");
					$res = $this->parse_properties($txtProperties);
					if($threshold=='cca'){
						$threshold_value =  $res['CCA_COMPLETION_THRESHOLD'];
					}else{
						$threshold_value =  $res['E3_LMS_COMPLETION_THRESHOLD'];
					}
				}
			}			
		}	
		
		return $threshold_value;
	}

public function parse_properties($txtProperties)
	{
		$result = array();
		$lines = explode("\n", $txtProperties);
		$key = "";

		$isWaitingOtherLine = false;
		foreach($lines as $i=>$line)
		{
			if(empty($line) || (!$isWaitingOtherLine && strpos($line,"#") === 0)) continue;

			if(!$isWaitingOtherLine) {
				$key = substr($line,0,strpos($line,'='));
				$value = substr($line,strpos($line,'=') + 1, strlen($line));
			}
			else {
				$value .= $line;
			}

			/* Check if ends with single '\' */
			if(strrpos($value,"\\") === strlen($value)-strlen("\\")) {
				$value = substr($value, 0, strlen($value)-1)."\n";
				$isWaitingOtherLine = true;
			}
			else {
				$isWaitingOtherLine = false;
			}

			$result[$key] = $value;
			unset($lines[$i]);
		}

		return $result;
	}
	
	
	public function parse_xml_file($url)
	{ 
	    $xml_str = file_get_contents($url);
		$output =Parser::xml($xml_str);
		return $output;
	}
}
