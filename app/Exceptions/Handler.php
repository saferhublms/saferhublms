<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Request;
use Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \Illuminate\Contracts\Encryption\DecryptException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
		//return parent::render($request, $exception);
		if (strpos(Request::url(),'/elearningmedia/checkloginstatus/reqdata') !== false) {
			return \Response::json(['status' =>  408,"message"=>'Unauthenticated']);
			
		}else{
			if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
					return \Response::json(['status' =>  408,"message"=>'Unauthenticated']);
				}else{ 
				    return parent::render($request, $exception);
				}
            //return parent::render($request, $exception);
		}
		
		//return \Response::json(['status' =>  408,"message"=>"error"]);
        //return parent::render($request, $exception);
		/* Log::info($exception->getMessage(), [
			'url' => Request::url(),
			'input' => Request::all()
		]);
		return parent::report($exception); */
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
