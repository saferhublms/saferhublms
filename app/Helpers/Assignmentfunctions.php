<?php
namespace App\Helpers;
use DB,Mail,CommonHelper;
use App\Http\Traits\Assignment;

class Assignmentfunctions 
{  
       public static function addNewAssignment($training_program_id, $duedate1, $compid, $userid, $AssessStartdate=''){
		$duedate						= date("Y-m-d", strtotime($duedate1));
		if(!empty($AssessStartdate)) 
		{
			$AssessStartdate = date("Y-m-d", strtotime($AssessStartdate));
		}
		else{
			$AssessStartdate = '0000-00-00';
		}		
		$data['company_id'] 			= $compid;
		$data['training_program_id'] 	= $training_program_id;
		$data['due_date'] 				= $duedate;
		$data['assignment_start_date'] 	= $AssessStartdate;
		$data['group_id'] 				= '0';
		$data['is_active'] 				= 1;
		$data['created_date'] 			= date('Y-m-d H:i:s');		
		$data['created_by'] 			= $userid;
		$lastassignmentid 				= Assignment::lastinsertid($data);
		return $lastassignmentid;
	}
	
	
	public static function assignUser($userid,$training_program_id,$assignmentid, $compid ){
		$assignmentmailarray = array();
		$data1 									= array();
		$data1['assignment_id'] 				= $assignmentid;
		$data1['user_id'] 						= $userid;
		$data1['is_active'] 					= 1;
		$data1['created_date'] 					= date('Y-m-d');
		$last 									= Assignment::lastinsertassginmentuser($data1);
		$getItemid=Assignment::getItemidreturnnew($assignmentid);
		
		$item_id=isset($getItemid[0]->item_id)?$getItemid[0]->item_id:'';
		if($item_id!='')
		{
		$transcript=Assignment::checktranscriptitem($item_id,$userid);
		if(count($transcript) == 0)
		{
		$assignmentmailarray['assignment_id']	= $assignmentid;
		$assignmentmailarray['user_id']			= $userid;
		}
		}
		
		$blended_program_id=isset($getItemid[0]->blended_program_id) ? $getItemid[0]->blended_program_id:'';
		if($blended_program_id!='')
		{
		$transcript=Assignment::checktranscriptblend($blended_program_id,$userid);
		if(count($transcript) == 0)
		{
		$assignmentmailarray['assignment_id']	= $assignmentid;
		$assignmentmailarray['user_id']			= $userid;
		}
		}
		
		return $assignmentmailarray;
	}
	
	
	 public static function updateUser($userid,$training_program_id,$assignmentid, $compid){
	
		$data1 						= array();
		$data1['assignment_id'] 	= $assignmentid;
		$data1['is_active'] 		= 1;
		$data1['created_date'] 		= date('Y-m-d');
		$last 						= Assignment::updateassignusers($data1, $userid, $training_program_id, $compid);
			
	}

}