<?php
namespace App\Helpers;
use App\Models\CompanySettings;
use App\Models\CompanyCmsInfo;
use App\Models\UserOrganization;
use App\Models\SuperAdminReleasenote;
use App\Models\CompanyLinksInfo;
use App\Models\TrainingCategory;
use App\Models\UserMaster;
use App\Models\TrainingType;
use App\Models\ManageUserField;
use App\Models\UserField;
use App\Models\Instructor;
use App\Models\CompanyPasswordSetting;
use App\Models\UserSkill;
use App\Models\SkillLevel;
use App\Http\Traits\Admin;
use App\Http\Traits\Users;
use App\Http\Traits\Loginfunction;
use App\Http\Traits\Traningcategory;
use DB,CommonHelper;
use Auth;
use App\Models\SubAdminAssignedGroup;
use App\Models\GroupMaster;
use App\Models\SubAdminAssignedCategory;
use App\Models\UserDivision;
use App\Models\DocSlide;

class CommonFunctions {
       
	   use Admin,Loginfunction,Users;

        public static function getFromCompanyName($company_id)
		{
			$company_name=Admin::getcompanyname($company_id);
			$company_details = CommonFunctions::getCompanySettings($company_id);
			$company = ($company_details['company_name'] !='') ? $company_details['company_name'] : $company_name[0]->company_name;
			return $company;
		}
		
		public static function getCompanySettings($company_id)
		{
			$company_setting_result = CompanySettings::wherecompanyId($company_id)->first();
			return $company_setting_result;
		}
		
		
		public static function getFromEmailId($company_id)
		{
			$company_details = CommonFunctions::getCompanySettings($company_id);
			return (isset($company_details['admin_email'])) ? $company_details['admin_email'] : "noreply@torchlms.com";
		}
		
		public static function getCompanyDefaultEmails($compid=0)
		{   
			 if($compid==0){
				 $fetchAll = CommonFunctions::getaccessurl();
				 if ($fetchAll != 0 ){
					$company_id = $fetchAll['company_id'];
					$company_details = CommonFunctions::getCompanySettings($company_id);
					return isset($company_details['default_emails']) ? $company_details['default_emails']: 'blank';
				}else{
					return 'blank';
				}
			 }else{
				 $company_details = $CommonFunctions::getCompanySettings($compid);
				 return isset($company_details['default_emails']) ? $company_details['default_emails']: 'blank';
			}
		}
		
		
		public static function getaccessurl()
		{
			 $url	=	$_SERVER['HTTP_HOST'];
             $pattern=	'/^www/';
			 preg_match($pattern, substr($url,0), $matches, PREG_OFFSET_CAPTURE);
			 if(count($matches)==0){
				$accessurl = $_SERVER['HTTP_HOST'];
			 }
			 if(count($matches)>0)
			 {
				$accessurl = substr($url, strpos($url, "www.") + 4);
			 }
			 $checkaccessurl = Loginfunction::checkaccessurl($accessurl);
			 if (count($checkaccessurl) > 0) {
				$company_id = $checkaccessurl[0]->company_id;
				return array('company_id' => $company_id, 'is_remote_authenticated' => $checkaccessurl[0]->is_remote_authenticated, 'is_maintainance' => $checkaccessurl[0]->is_maintainance);
			}
			if (count($checkaccessurl) == 0) {
				$msg = "Company is not authorised yet!";
				return array('company_id' => 0, 'message' => $msg);
			}
			$urlParts = explode('.', $_SERVER['HTTP_HOST']);
			exit;
			
		}
		
		public static function getCompanyInfo()
	    {
			$result = array();	
		    $fetchAll = CommonFunctions::getaccessurl();
			if ($fetchAll != 0 ){
				$company_id = $fetchAll['company_id'];
			}
			
			if($company_id!=0){
				$result = CompanyCmsInfo::wherecompanyId($company_id)->whereisActive(1)->get();
			}
			$currentversion = SuperAdminReleasenote::whereisActive(1)->orderBy('id','desc')->limit(1)->first();
			if($currentversion) {
				$result['version']=$currentversion['version'];
				}
			else{
				$result['version']="2.0";	
			}
				
			
			return $result;
		
		}
		
		public static function fetchlinkinfo($compid)
		{
			$result=CompanyLinksInfo::wherecompanyId($compid)->get();
			return $result;
		}
		
		public static function fetchUserOranizationIds($user_id)
		{		
			$get_SubOrgId=UserOrganization::where('user_id','=',$user_id)->where('is_active','=',1)->select(DB::raw('group_concat(distinct sub_organization_id) as sub_organization_id'))->first();
			$SubOrgId ='';
			if($get_SubOrgId){
			  $SubOrgId =$get_SubOrgId['sub_organization_id'];
			}
			return $SubOrgId;
		}
		
		public static function getTraningCategory($compid)
		{
			$traningcategoryType=TrainingCategory::wherecompanyId($compid)->whereisActive(1)->whereisDelete(0)->orderBy('category_name','asc')->get();
			$TraningcategoryArray = array();
			foreach ($traningcategoryType as $row)
			{
				$TraningcategoryArray[]=array('training_category_id'=>$row['training_category_id'],'category_name'=>$row['category_name']);
			}
			return $TraningcategoryArray;
		}
		
		/**
		 * get_training_type() - Method to retrive training types.
		 *
		 * Created By: Rishabh ANand
		 * Created Date: 11 January, 2013
		 */	
		public static function get_training_type()
		{
			$Trainingtypes=TrainingType::get()->toArray();
			$Trainingtypesarray = array();
			if (count($Trainingtypes) > 0) {
				foreach ($Trainingtypes as $rows) {
					$Trainingtypesarray[]=array('training_type_id'=>$rows['training_type_id'],'training_type'=>$rows['training_type']);
					
				}
			}
			return $Trainingtypesarray;
		}
		
		public static function getTraningCategoryforsupervisortool($compid,$userid,$roleid){
		$traningcategoryType = Traningcategory::trainingcategoryforuser($compid,$userid,$roleid);
		$TraningcategoryArray = array();
		foreach ($traningcategoryType as $row)
		{
			$TraningcategoryArray[] = array("catname"=> $row['category_name'],'catid'=>CommonHelper::getEncode($row['training_category_id']));
		}
		return $TraningcategoryArray;
	}
	
	
	public static function getInstructorList($compid,$roleid,$userid)
	{
		if($roleid!=1){
			$sql = "select um.* from user_master as um 
			join user_group ug on ug.user_id=um.user_id
			join group_master gm on gm.group_id=ug.group_id
			where ug.group_id in (select CONCAT(saag.group_id) from sub_admin_assigned_group saag where saag.user_id=:userId and saag.is_active=1) 
			and um.is_active= 1 and um.is_approved=1 and um.is_delete=0 and um.role_id in (1,5, 3) and um.company_id=:compId1 and gm.company_id=:compId2 order by um.user_name";
			$sel_arr['userId'] = $userid;
			$sel_arr['compId1'] = $compid;
			$sel_arr['compId2'] = $compid;
			$result = DB::select(DB::raw($sql), $sel_arr);
			
		}else{
			$result = UserMaster::where('is_active',1)->where('is_approved',1)->where('is_delete',0)->whereIn('role_id',[1,3,5])->where('company_id',$compid)->select('user_master.*')->get();
		}
		$instructorArray =array();
		foreach($result as $row){
			$instructorArray[] =array("user_id"=> $row['user_id'],'user_name'=>$row['user_name']);
			
		}
		return $instructorArray;
		
	}
	public static function getUserRole($user_id)
	{
		$result=UserMaster::where('user_id',$user_id)->select('role_id')->first();
		return $result;
	}
		
	public static function getELearnigFeedback($company_id)
	{
		$company_details = CommonFunctions::getCompanySettings($company_id);
		return $company_details->training_feedback;
	}
		
   /**
     * getCourseSkillLevel() - Method is use for check course skill credit data.
     *
	 * Created By: Siddharth Kushwah
	 * Created Date: 11/14/2017
     */	
	public static function getCourseSkillLevel($item_id, $compid,$userid,$type){
	
			$sql_query = "select sm.skill_id,sm.skill_name,sl.level_id,cslc.course_credit,sl.skill_credit,cslc.course_id,cslc.training_type from course_skill_level_credit as cslc
			join skills_master as sm on sm.skill_id=cslc.skill_id and sm.is_active=1 and sm.company_id=:company_id
			join skill_level as sl on sl.skill_id=sm.skill_id and sl.level_id=cslc.level_id
			where cslc.course_id=:course_id and cslc.training_type=:training_type";
			$param=array();
			$param['company_id']=$compid;
			$param['course_id']=$item_id;
			$param['training_type']=$type;
			$dataskill = DB::select(DB::raw($sql_query), $param);
			
			foreach($dataskill as $allskillarr){

				$datacreate['skill_id']=$allskillarr->skill_id;
				$datacreate['skill_rating']=1;
				$datacreate['user_id']=$userid;
				$datacreate['level_id']=$allskillarr->level_id;
				$datacreate['course_id']=$item_id;
				$datacreate['expiration_date']='0000-00-00';
				$datacreate['is_active']=1;
				$datacreate['training_type']=$type;
	
				$skill_id=$allskillarr->skill_id;
				$level_id=$allskillarr->level_id;
				
				$sel_arr=array();
				$wherestr="skill_id=:skill_id and level_id=:level_id  and user_id=:user_id and course_id=:item_id and is_active=1";
				$sel_arr['skill_id'] = $skill_id;
				$sel_arr['level_id'] = $level_id;
				$sel_arr['user_id'] = $userid;
				$sel_arr['item_id'] = $item_id;
			
				$datacheckskill=CommonFunctions::checkuserdataskill($wherestr,$sel_arr);
                 
				if(count($datacheckskill)==0){
					 UserSkill::insert($datacreate);
					// $model->lastinsertid('user_skill', $datacreate);
					/* $where	 = array();
					$where[] = $where;
					$model->updateanywhere('user_skill', $datacreate,$where); */
				}
				/* else{
				     $model->lastinsertid('user_skill', $datacreate);
					 
				}	 */

			}
	}
	
	/**
     * checkdataskillcredit() - Method is use for check course skill credit data.
     *
	 * Created By: Siddharth Kushwah
	 * Created Date: 11/14/2017
     */	
	public static function checkuserdataskill($wherestr,$sel_arr){
		
		$sql_query = "select * from user_skill where $wherestr";
		$result = DB::select(DB::raw($sql_query), $sel_arr);
		return $result;
	}
	
	
	public static function getFieldName($compid)
	{
		$result=ManageUserField::join('user_field','user_field.id','=','manage_user_field.user_field_id')->where('manage_user_field.company_id','=',$compid)->where('manage_user_field.is_active','=',1)->where('manage_user_field.is_delete','=',0)->orderby('manage_user_field.user_field_id','ASC')->pluck('manage_user_field.field_name','user_field.table_column');

		if(count($result)==0)
		{
			
			$result = UserField::orderby('id','asc')->pluck('default_name','table_column');
		}		
		return $result;
	}
	
	public static function getCompanySettingGroups($company_id)
	{
		
		$sql = "SELECT ca.action_code, if(car.role_ids!='',car.role_ids,'0')  AS role_ids FROM company_action ca 
			left join company_action_role car on car.action_code = ca.action_code and car.company_id='$company_id' AND car.is_active=1";
		
		$res = DB::select($sql);
		$permission = array();
		if($res)
		{
			foreach($res as $rep){
				$permission[$rep->action_code] = explode(",",$rep->role_ids);
			}
			return $permission;
		}
	}
	
	public static function fetchCompPasswordsettingData($compid){
		$result=CompanyPasswordSetting::where('company_id','=',$compid)->get();
		return $result;
	}
	public static function customlitag($PasswordValidateSetting)
	{
		if(count($PasswordValidateSetting) > 0)
		{
			$passlen = $PasswordValidateSetting[0]->password_length ;
			$passcomplexity = $PasswordValidateSetting[0]->password_complexity ;
		}
		else
		{
			$passlen = 4;
			$passcomplexity = 1;
		}
		
		$tag_str='';
		if($passcomplexity == 2)
		{
			$valdation = array("Alpha"=>"At least <strong>one letter.</strong>","number"=>"At least <strong>one number.</strong>", "length"=>"Password length must be <strong>".$passlen." characters or more.</strong>");
		}
		elseif($passcomplexity == 3)
		{
			$valdation = array("Alpha"=>"At least <strong> one  letter.</strong>","number"=>"At least <strong>one number.</strong>","special"=>"At least <strong>one special character.</strong>", "length"=>"Password length must be <strong>".$passlen." characters or more.</strong>");
		}
		elseif($passcomplexity == 4)
		{
			$valdation = array("letter"=>"At least <strong> one lower case letter. </strong>", "capital"=>"At least <strong>one upper case letter. </strong>", "number"=>"At least <strong>one number.</strong>","length"=>"Password length must be <strong>".$passlen." characters or more.</strong>");
		}
		elseif($passcomplexity == 5)
		{
			$valdation = array("letter"=>"At least <strong>one lower case letter.</strong>", "capital"=>"At least <strong>one upper case letter.</strong>", "number"=>"At least  <strong>one number.</strong>","special"=>"At least <strong>one special character.</strong>", "length"=>"Password length must be <strong>".$passlen." characters or more.</strong>");
		}
		elseif($passcomplexity == 1)
		{
			$valdation = array("length"=>" Password must contain at least <strong>".$passlen." characters.</strong>");
		}

		foreach($valdation as $x => $x_value) {
			$tag_str.='<li class="invalid" id="'.$x.'">'.$x_value.'</li>';
		}
		return $tag_str;
	}
	
	public static function skilllevelmethod($compid,$skill_id)
	{
		$result=SkillLevel::where('company_id',$compid)->where('skill_id',$skill_id)->select('level_id','skill_credit')->get();
		return $result;
		
	}
	
	
	
	
	/**
     * getSubAdminAssignedGroups() - Method to assigned group id of the sub-admin.
     *
	 * Created By: prabhakar 
	 * Created Date: 14 /2/ 2018
     */	
	public static function getSubAdminAssignedGroups($sub_admin_id)
	{
		$group_id=array();
		$assigned_group_name_result=array();
		if($sub_admin_id)
		{
		 $assigned_group_name_result = SubAdminAssignedGroup::where('user_id','=',$sub_admin_id)->where('is_active','=',1)->get();
		}
		else
		{
			$assigned_group_name_result=array();
		}
		if(count($assigned_group_name_result))
		{
		 foreach($assigned_group_name_result as $row) $group_id[] = $row->group_id;
		}
		return $group_id;
	}	
	
	/**
     * getSubAdminAssignedCategory() - Method to retrive assigned category id of the sub-admin.
     
     */	
	public static function getSubAdminAssignedCategory($sub_admin_id)
	{
		$groupcat_id=array();
		$assigned_group_name_result=array();
		if($sub_admin_id)
		{
		 $assigned_group_name_result = SubAdminAssignedCategory::where('user_id','=',$sub_admin_id)->get();
		}
		else
		{
			$assigned_group_name_result=array();
		}
		if(count($assigned_group_name_result))
		{
		 foreach($assigned_group_name_result as $row)
		 {
		 $groupcat_id[] = $row->category_id;
		 }
		}
		return $groupcat_id;
	}
	
	public static function convert2pdf($filename,$path,$last_resourse_id,$image_status)
    {
		$ppt_file = $path.$filename;
		chdir($path);
		$extension = CommonFunctions::getextension($filename);
		$pdffilename = CommonFunctions::getfilebasename($filename).'.pdf';
		if($extension != 'pdf')
		{
			$cmd = 'sh /usr/bin/libreoffice --headless --invisible --norestore -convert-to pdf --outdir "'.$path .'" "'.$ppt_file.'"';
			shell_exec("export HOME=/tmp && $cmd");
			
		}
		
		if($image_status=="yes") {
			CommonFunctions::pdf2images($filename,$path,$last_resourse_id);
		}
		
    }
	
	public static function getextension($filename)
	{
		$file_extension_arr = explode(".", $filename);
		return $extension = strtolower(end($file_extension_arr));
	}
	

	public static function getUserInfo($company_id,$user_id,$viewuserId,$roleid,$userType,$sub_org_id,$subadminbasedon=0,$instructorbasedon=0,$start,$length,$orderType,$orderColumn,$searchtext)
	{ 
		$dataArray=array();
		$fetchusers = CommonFunctions::getusersall($company_id,$user_id,'',$roleid,$userType,$sub_org_id,$subadminbasedon,$instructorbasedon,$start,$length,$orderType,$orderColumn,$searchtext);
		
		if($fetchusers[0]){
			foreach($fetchusers[0]->toArray() as $val)
			{
				$user_id = $val['user_id'];
				if(stripcslashes($val['user_name'])==''){$user_name ='';}
				else{$user_name = stripcslashes($val['user_name']);}
				
				$login_id = stripcslashes($val['login_id']);
				$division_name = stripcslashes($val['dname']);
		
				$area = stripcslashes($val['area_name']);
				$location = stripcslashes($val['location_name']);
				$subOrgName = stripslashes($val['sname']);
				$delete = stripcslashes($val['is_delete']);		
				$value = CommonHelper::getEncode($val['user_id']);			
				if($delete==1){
					$button = "<div class=button_lblue_r4 onclick=deletedusers('1','".$val['user_id']."') >Restore<span>&#187;</span></div>  <div class=button_lblue_r4  ng-click=deletedusers('2','".$val['user_id']."') >Remove<span>&#187;</span></div>";
				}else{
					$button = '<div class="button_lblue_r4" ng-click="viewuser(\''.$value.'\')">View <span>&#187;</span></div>';
				}
				$dataArray[] = array('id'=>$user_id,'name'=>$user_name,'user_name'=>$login_id,'division_name'=>$division_name,'area'=>$area, 'location'=>$location, 'subOrgName'=>$subOrgName,'button' =>$button);
			}
	}
	  
		return array($dataArray,$fetchusers[1]);
	}

	public static function getfilebasename($filename)
	{
		$extension = CommonFunctions::getextension($filename);
		$ext         = '.'.$extension ;
		return $pdfname  = basename($filename , $ext); 
	}

	public static function getusersall($company_id,$user_id,$viewuserId,$roleid,$userType,$suborgId=0,$subadminbasedon=0,$instructorbasedon=0,$start,$length,$orderType,$orderColumn,$searchtext)
	{
		$company_id=addslashes($company_id);
		$user_id=addslashes($user_id);
		$viewuserId=addslashes($viewuserId);
		$roleid=addslashes($roleid);
		$userType=addslashes($userType);
		
		if($viewuserId != '')
		$where = " and um.user_id='$viewuserId' ";
		else
		$where = " ";
		if($userType=='0')
		$and = " AND  um.is_active = 1 AND um.is_delete = 0 ";
		elseif($userType=='1')
		$and = " AND  um.is_active = 0 AND um.is_delete = 0 ";
		elseif($userType=='2')
		$and = " AND  um.is_delete = 1 ";
		else
		$and = "";	
		
			if($roleid == 5 || $roleid == 3 )
			{
				if(($roleid == 5 && $subadminbasedon == 1) || ($roleid == 3 && $instructorbasedon == 1))
				{
					$sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as jobTitles,am.area_name,lm.location_name,group_concat(DISTINCT som.sub_organization_name) as sname ,mrf.field_name,rm.role_name FROM user_master um 
					left join user_group ug on ug.user_id = um.user_id and ug.is_active=1
					left join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0 and gm.company_id='$company_id'
					left join user_jobtitle ujt on ujt.user_id=um.user_id and ujt.is_active = 1
					left join job_title_group jtg on jtg.job_id=ujt.job_id AND jtg.is_active =1 and jtg.company_id='$company_id'
					join role_master rm on um.role_id=rm.role_id
					left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id='$company_id'
					left join area_master am on am.area_id=um.area and am.company_id='$company_id'
					left join location_master lm on lm.location_id=um.location and lm.company_id='$company_id'
					join user_organization uo on uo.user_id=um.user_id and uo.is_active = 1
					join sub_organization_master som on som.sub_organization_id=uo.sub_organization_id and som.is_active = 1 and som.is_delete = 0 and som.company_id = '$company_id'
					where  som.sub_organization_id = $suborgId
					and um.company_id = '$company_id' $and $where and um.is_user_internal!= 1 group by um.user_id order by um.user_name ASC";
				}
				else
				{
					$sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as jobTitles,am.area_name,lm.location_name,group_concat(DISTINCT som.sub_organization_name) as sname ,mrf.field_name,rm.role_name FROM user_master um 
					join user_group ug on ug.user_id = um.user_id and ug.is_active=1
					join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0 and gm.company_id='$company_id'
					left join user_jobtitle ujt on ujt.user_id=um.user_id and ujt.is_active = 1
					left join job_title_group jtg on jtg.job_id=ujt.job_id AND jtg.is_active =1 and jtg.company_id='$company_id'
					join role_master rm on um.role_id=rm.role_id
					left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id='$company_id'
					left join area_master am on am.area_id=um.area and am.company_id='$company_id'
					left join location_master lm on lm.location_id=um.location and lm.company_id='$company_id'
					left join user_organization uo on uo.user_id=um.user_id and uo.is_active = 1
					left join sub_organization_master som on som.sub_organization_id=uo.sub_organization_id and som.is_active = 1 and som.is_delete = 0 and som.company_id = '$company_id'
					where  ug.group_id IN (SELECT CONCAT( group_id ) AS groups FROM sub_admin_assigned_group saag WHERE saag.user_id ='$user_id' AND saag.is_active =1) 
					and um.company_id = '$company_id' $and $where and um.is_user_internal!= 1 group by um.user_id order by um.user_name ASC";
				}
				
			}
			else
			{
				 $column='';
			
				if($orderColumn==2){
					$column='user_master.user_name';
				}
				elseif($orderColumn==3){
					$column='user_master.login_id';
				}elseif($orderColumn==4){
					$column='division_master.division_name';
				}elseif($orderColumn==5){
					$column='area_master.area_name';
				}elseif($orderColumn==6){
					$column='location_master.location_name';
				}elseif($orderColumn==7){
					$column='sub_organization_master.sub_organization_name';
				}
				
		        $active=1;
				$querydata=UserMaster::leftjoin('user_jobtitle',function($join) use($active){
					$join->on('user_jobtitle.user_id','=','user_master.user_id')
					    ->where('user_jobtitle.is_active','=',$active);
				})->leftjoin('job_title_group',function($join) use($active,$company_id){
					$join->on('job_title_group.job_id','=','user_jobtitle.job_id')
					->where('job_title_group.is_active','=',$active)
					->where('job_title_group.company_id','=',$company_id);
				})
				->join('role_master','role_master.role_id','=','user_master.role_id')
				->leftjoin('manage_role_field',function($join) use($company_id){
					$join->on('manage_role_field.role_id','=','role_master.role_id')
					->where('manage_role_field.company_id','=',$company_id);
				})
				->leftjoin('area_master',function($join) use($company_id){
					$join->on('area_master.area_id','=','user_master.area')
					->where('area_master.company_id','=',$company_id);
				})
				->leftjoin('location_master',function($join) use($company_id){
					$join->on('location_master.location_id','=','user_master.location')
					->where('location_master.company_id','=',$company_id);
				})
				->leftjoin('user_organization',function($join) use($active){
					$join->on('user_organization.user_id','=','user_master.user_id')
					->where('user_organization.is_active','=',$active);
				})
				->leftjoin('user_division',function($join) use($active){
					$join->on('user_division.user_id','=','user_master.user_id')
					->where('user_division.is_active','=',$active);
				})
				->leftjoin('division_master',function($join) use($company_id){
				$join->on('division_master.division_id','=','user_division.division_id')
				->where('division_master.company_id','=',$company_id)
				->where('division_master.is_active','=',1);
				})
				->leftjoin('sub_organization_master',function($join) use($active,$company_id){
					$join->on('sub_organization_master.sub_organization_id','=','user_organization.sub_organization_id')
					->where('sub_organization_master.is_active','=',$active)
					->where('sub_organization_master.is_delete','=',0)
					->where('sub_organization_master.company_id','=',$company_id);
				})->where('user_master.company_id','=',$company_id)
				->where('user_master.is_approved','=',1)
				->where('user_master.is_user_internal','!=',1);
				if($viewuserId != ''){
					$querydata=$querydata->where('user_master.user_id','=',$viewuserId);
				}
				
				if($userType=='0'){
					$querydata=$querydata->where('user_master.is_active','=',1)->where('user_master.is_delete','=',0);
				}
				elseif($userType=='1'){
					$querydata=$querydata->where('user_master.is_active','=',0)->where('user_master.is_delete','=',0);
				}
				elseif($userType=='2'){
					$querydata->where('user_master.is_delete','=',1);
				}
				
				if($viewuserId!=''){
					$allrecords=$querydata->select(DB::raw('user_master.*,group_concat(job_title_group.jobtitle_group) as jobTitles,group_concat(DISTINCT sub_organization_master.sub_organization_name) as sname ,area_master.area_name,location_master.location_name,role_master.role_name,manage_role_field.field_name,GROUP_CONCAT(DISTINCT division_master.division_name) AS dname'))->groupby('user_master.user_id')
				   ->first();
				   $totalcount=1;
				   $selecteduserid=CommonHelper::getEncode($allrecords->user_id);
				   return array($allrecords,$totalcount,$selecteduserid);
				}else{
					$querydata=$querydata->select(DB::raw('user_master.*,group_concat(job_title_group.jobtitle_group) as jobTitles,group_concat(DISTINCT sub_organization_master.sub_organization_name) as sname ,area_master.area_name,location_master.location_name,role_master.role_name,manage_role_field.field_name,GROUP_CONCAT(DISTINCT division_master.division_name) AS dname'))->groupby('user_master.user_id')
					;
					
					if($searchtext){
					$querydata=$querydata->where(function($query) use($searchtext){
					$query->where('user_master.user_name','like','%'.$searchtext.'%')
					->orWhere('sub_organization_master.sub_organization_name','like','%'.$searchtext.'%');
					});
					} 
					
					$totalcount=$querydata->get();
					
					if($column){
					    $allrecords=$querydata->orderBy($column,$orderType)->skip($start)->take($length)->get();;
					}else{
						$allrecords=$querydata->skip($start)->take($length)->get();
					}
					return array($allrecords,$totalcount);
				}
			}
		return array($allrecords,$totalcount);
	}

	 public static function pdf2images($filename,$path,$last_resourse_id)
    {
		$pdir             = $path.$last_resourse_id ;
		$thumb            = $path.$last_resourse_id.'/thumbs' ;
		$medium            = $path.$last_resourse_id.'/medium' ;
		
		if(!is_dir($pdir))
        {
            mkdir($pdir, 0777, true); 
			
			if(is_dir($pdir))
			{
				$startPage     = 1;
				$endPage       = 1000; 
				chdir($path);
				
				$pdffilename = CommonFunctions::getfilebasename($filename).'.pdf' ; 
				$source      = $path.$pdffilename ;
				
				if(file_exists($source))
				{
					$target  = $pdir.'/';
					
					$command = '/usr/bin/gs -dNOPAUSE -sDEVICE=jpeg -dFirstPage='.$startPage.' -dLastPage='.$endPage.' -sOutputFile='.$target.'Slide%d.jpg -dJPEGQ=100 -r300x300 -q "'.$source.'" -c quit'; 

					exec($command) ;
					
					if(!is_dir($thumb))
					{
						mkdir($medium,0777,true); //So you get the sticky bit set
						mkdir($thumb,0777,true); //So you get the sticky bit set
						$dir_handle = opendir($pdir) or die("Unable to open");
						
						while($file = readdir($dir_handle))
						{
							if($file!="." && $file!=".." && !is_dir("$pdir/$file"))
							{
								//Thumb create, insert into data base
								$ext_  = pathinfo($file);
								$ext1  = $ext_['extension'];
								$ext   = strtolower($ext1);
								$image = $pdir.'/'.$file;
								$destination = $thumb.'/'.$file;
								$medium_destination = $medium.'/'.$file;
								
								CommonFunctions::make_thumb($image, $destination, "100px", "80px", $ext);
								CommonFunctions::make_thumb($image, $medium_destination, "800px", "650px", $ext);

								$slide_name  = $file; 
								$temparr     = explode('.',$file);
								$pptfile     = explode('lide',$temparr['0']);
								$slideindex  = $pptfile['1'] ;

								$dataForPptSlide  = array();
								$ext = array_pop( $temparr );
								$name = implode( '.', $temparr );
								$dataForPptSlide['files_id']         = $last_resourse_id;
								$dataForPptSlide['slide']       	 = $slide_name;
								$dataForPptSlide['slide_name']       = $name;
								$dataForPptSlide['slide_order']      = $slideindex;
								DocSlide::create($dataForPptSlide);
							}
						}
					}
				}
			}
		}
    }

	

	public static  function make_thumb($src, $destination, $desired_width, $new_h, $ext)
    {
        if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext))
        $src_img = imagecreatefromjpeg($src);

        if(!strcmp("png",$ext))
			$src_img = imagecreatefrompng($src);
			
        if(!strcmp('gif', $ext))
			$src_img = imagecreatefromgif($src);

        /* read the source image */
        $width = imagesx($src_img);
        $height = imagesy($src_img);

        $ratio1=$width/$desired_width;
        $ratio2=$height/$new_h;
        if($ratio1>$ratio2) {
			$thumb_w=$desired_width;
			$thumb_h=$height/$ratio1;
        }
        else {
			$thumb_h=$new_h;
			$thumb_w=$width/$ratio2;
        }

        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = @ImageCreateTrueColor($thumb_w,$thumb_h);
        imagealphablending($desired_height, false);
        imagesavealpha( $desired_height, true );
        /* copy source image at a resized size */
        @imagecopyresampled($desired_height, $src_img, 0, 0, 0, 0, $thumb_w,$thumb_h, $width, $height);

        /* create the physical thumbnail image to its destination */
        if(!strcmp("png",$ext))
			imagepng($desired_height,$destination);
        else
			imagejpeg($desired_height,$destination);
    }
	
	
	public static function getallgroup($compid,$subAdminId,$roleid)
	{
		$roleArr=array(1,6);
		if(!in_array($roleid,$roleArr)){
			$result=GroupMaster::join('sub_admin_assigned_group as saag','saag.group_id','=','group_master.group_id')
			           ->where('saag.user_id',$subAdminId)->select('group_master.group_id','group_master.group_name')
					   ->get();
		}else{
			$result=GroupMaster::where('company_id',$compid)->where('is_active',1)->where('is_delete',0)->orderBy('group_name','asc')->select('group_id','group_name')->get();
		}
		return $result;
	}
	
	public static function getallusers($compId)
	{
         $allUsers=UserMaster::where('company_id',$compId)->where('is_active',1)->where('is_delete',0)->select('user_id','user_name')->where('role_id','<>',1)->get();
		 return $allUsers;
	}
	
 public static function getusertoedit($company_id,$user_id,$editserId,$roleid,$suborgId=0,$subadminbasedon =0)
	{
		if($roleid == 5)
		{
			if($subadminbasedon == 1)
			{
				$sql = "SELECT um.*,gm.group_name,gm.group_id,jtg.job_id,jtg.jobtitle_group	FROM user_master um
				left join user_jobtitle ujt on ujt.user_id=um.user_id
				left join job_title_group jtg on jtg.job_id=ujt.job_id and jtg.company_id='$company_id'
				join user_group ug on ug.user_id='$user_id' and ug.is_active =1
				join group_master gm on ug.group_id=gm.group_id and gm.company_id='$company_id' and gm.is_active=1 and gm.is_delete=0
				join user_organization uo on uo.user_id=um.user_id and uo.is_active = 1
				join sub_organization_master som on som.sub_organization_id=uo.sub_organization_id and som.is_active = 1 and som.is_delete = 0 and som.company_id = '$company_id'
				where um.company_id = '$company_id' and um.is_approved = 1 and um.user_id='$user_id' and som.sub_organization_id = $suborgId group by um.user_id order by um.user_name ASC";
			}
			else
			{
				$sql = "SELECT um.*,gm.group_name,gm.group_id,jtg.job_id,jtg.jobtitle_group	FROM user_master um
				left join user_jobtitle ujt on ujt.user_id=um.user_id
				left join job_title_group jtg on jtg.job_id=ujt.job_id and jtg.company_id='$company_id'
				join user_group ug on ug.user_id='$user_id' and ug.is_active =1
				join group_master gm on ug.group_id=gm.group_id and gm.company_id='$company_id' and gm.is_active=1 and gm.is_delete=0
				where ug.group_id in (select saag.group_id as groups from sub_admin_assigned_group saag where saag.user_id=$editserId and saag.is_active=1 )
				and um.company_id = '$company_id' and um.is_approved = 1 and um.user_id='$user_id' group by um.user_id order by um.user_name ASC";
			}
		}
		else
		{
			 $user_rows=UserMaster::where('company_id','=',$company_id)->where('is_approved','=',1)->where('user_id','=',$user_id)
			 ->select(DB::raw('login_id as loginname,password,user_name,first_name,middle_name,last_name,user_pic_path,CAST(country AS CHAR) as country,phone_no,email_id,CAST(role_id AS CHAR) as rolename,CAST(is_active AS CHAR) as status_id,is_approved'),'timezone_id')
			 ->first(); 
			 $user_rows->timezone_id=(string)$user_rows->timezone_id;
			 return $user_rows;
			//$sql = "SELECT um.* FROM  user_master um where um.company_id='$company_id' and um.is_approved=1 and um.user_id='$user_id'";
		}
	}

}