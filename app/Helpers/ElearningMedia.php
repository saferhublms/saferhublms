<?php
namespace App\Helpers;
use App\Models\UserTranscript;
use App\Models\UsertrainingCatalog;
use App\Http\Traits\Learningplan;

class ElearningMedia 
{
	public static function checkTrainingExpiration($item_id, $userid, $compid, $trainingprogram_id){
	
		$expiration_status = 'new';
		
		$ut_res = ElearningMedia::getCompletionDate($item_id, $userid, $compid);
		
		if($ut_res){
			$date_completion = $ut_res['completion_date'];
			
			$currentDate = date('m/d/Y');
			$expiringDate = ElearningMedia::getExpirationDate($item_id, $date_completion);
			if($currentDate>$expiringDate){	
				$expiration_status = 'expired';
			}else{
			
				$utc_res = ElearningMedia::getRetakeUserTrainingId($item_id,$userid,$compid,$trainingprogram_id);
										
				if($utc_res){
					$expiration_status = 'new';
				}else{
					$expiration_status = 'retake';
				}
				
			}
			
		}
		return $expiration_status;
	}
	
	
	public static function getCompletionDate($item_id, $userid, $compid){
		$result = UserTranscript::where('item_id','=',$item_id)->where('user_id','=',$userid)->orderBy('transcript_id', 'desc')->first();
	     return $result;
   }
	
	
	public static function getExpirationDate($item_id, $date_completion){
		$expirationSettings 	= Learningplan::expirationSettings ( '', $item_id );
		$expire_enable 			= $expiredays = 0;
		
		if(count($expirationSettings) > 0 ){
			$expire_enable 			= (isset ( $expirationSettings [0]->expire_enable )) ? $expirationSettings [0]->expire_enable : 0;
			$expiredays 			= (isset ( $expirationSettings [0]->expiredays ) && $expire_enable == 1) ? $expirationSettings [0]->expiredays : 'Never';
			$duedays 				= (isset ( $expirationSettings [0]->duedays ) && $expire_enable == 1) ? $expirationSettings [0]->duedays : 'Never';
		}
		$expiringDate = date ( 'm/d/Y', strtotime ( date ( "Y-m-d", strtotime ( $date_completion ) ) . "+ $expiredays day" ) );
		
		return $expiringDate ;
	}
	
	
	public static function getRetakeUserTrainingId($item_id,$userid,$compid,$trainingprogram_id){
		
		$result = UsertrainingCatalog::where('item_id','=',$item_id)->where('user_id','=',$userid)->where('company_id','=',$compid)->where('program_id','=',$trainingprogram_id)->orderBy('user_training_id', 'desc')->take(1)->get();
		return $result;

	}
	
}