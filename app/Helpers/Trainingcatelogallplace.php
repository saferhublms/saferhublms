<?php
namespace App\Helpers;

use App\Http\Traits\Trainingprogram;
use CommonHelper,DateTime;
use URL;

class Trainingcatelogallplace 
{  
    use Trainingprogram;
   public static function supervisor_return_html($programs,$roleid,$compid,$edituserid){
	    $baseUrl = URL::to('/');
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$nodisplaycompany=array();
		$nodisplaycompany[]=30;
		
		$counted='';
		$numtraingItems=0;
		$height = '100px';
		$lineheight = '145px';
		if($counted <5)
		{
			$height = '120px';
			$lineheight = '165px';
		}
		else if($counted >5 && $counted <25)
		{
			$h11 = $counted*25;
			$height = $h11.'px';
			$h12 = $h11 + 45;
			$lineheight = $h12.'px';
		}
		else
		{
			$height = '550px';
			$lineheight = '595px';
		}
		
	
		$dataArray = array();
		if (count($programs) > 0) 
		{
			foreach ($programs as $program1) 
			{			
			
				if(count($program1) > 0){
					for($i=0;$i<count($program1);$i++){
						
						if ($program1[$i]['training_type_id'] == 4) 
						{							
							$item_name = stripcslashes($program1[$i]['title']);
						}else if ($program1[$i]['training_type_id'] == 1) 
						{
							$item_name = stripcslashes($program1[$i]['item_name']);
						}
						else if ($program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 ) 
						{
							$item_name = stripcslashes($program1[$i]['class_name']);
						}
						
						if ($program1[$i]['training_type_id'] == 1) 
						{
							$scheduleid=0;
							$start_date = "On Demand";
						}
						elseif ($program1[$i]['training_type_id'] == 4) 
						{
							$start_date = "--";
						}
						else if ($program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 ) 
						{
							if($program1[$i]['recursive_date']!='')
							{
								$start_date = date($Getdate_formatecomapny,strtotime(@$program1[$i]['recursive_date']));
							}
							else
							{
								$start_date ='';
							}
						}
						

							if ($program1[$i]['training_type_id'] == 4) 
							{
								$credit_value = "--";
							} 
							else if ($program1[$i]['training_type_id'] == 1 || $program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 ) 
							{
								$credit_value = $program1[$i]['credit_value'];
							}
						
						
					
							if ($program1[$i]['training_type_id'] == 4) 
							{
								$item_type = "Blended Program";
							} 
							else if ($program1[$i]['training_type_id'] == 1 || $program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 )
							{
								$item_type = $program1[$i]['itemtype'];
							}
						
						
						if(isset($program1[$i]['training_code']) && ($program1[$i]['training_code'] != ''))
						{
							$courseFullId = $program1[$i]['training_code'] ;
							$courseIdarray = explode('_' , $courseFullId) ;
							if(count($courseIdarray) > 0)
							{
								$courseId = $courseIdarray[2];
							}
							else
							{
								$courseId = $courseFullId ;
							}
							
						}
						else
						{
							$courseId = "--" ;
						}
						
						if(in_array($program1[$i]['training_type_id'], array(2,3)) && $start_date != ''){
							$date = DateTime::createFromFormat($Getdate_formatecomapny, $start_date);
							$start_dateinymd 	= 		$date->format('Y-m-d');
							
							if((strtotime($start_dateinymd)) >= (strtotime(date('Y-m-d'))))
							{ 
								$dataArray[] = array("id"=> $edituserid[0].'||'.CommonHelper::getEncode($program1[$i]['training_program_id']),"data"=>array(0,$item_name, $courseId, $start_date, $credit_value, $item_type));		
							}
						}else{
							$dataArray[] = array("id"=>$edituserid[0].'||'.CommonHelper::getEncode($program1[$i]['training_program_id']),"data"=>array(0,$item_name, $courseId, $start_date, $credit_value, $item_type));		
						}
					}	
				}
			}
		}
		return $dataArray;
   }
   
         /*  public static function getClasssesCatelog($compid,$userid,$roleid)
			{	
				$getclassarray=Trainingprograms::getClassroomdcatalog($compid,$userid,$roleid);
				return $getclassarray;
			}
			
			public static function getClasssesCatelogFilter($compid,$userid,$roleid,$cattype,$traingTypeId='')
			{	
				$getclassarray=Trainingprograms::getClassroomdcatalogFilter($compid,$userid,$roleid,$cattype,$traingTypeId);
				return $getclassarray;
			} */


           /*  public static function getBlendedCatelog($compid,$userid,$roleid)
			{
				$getblendedarray=Trainingprograms::getblendedcatalog($compid,$userid,$roleid);
				return $getblendedarray;
			} */
			/* public static function getBlendedCatelogFilter($compid,$userid,$roleid,$cattype)
			{

				$getblendedarray=Trainingprograms::getblendeddataFilter($compid,$userid,$roleid,$cattype);
				return $getblendedarray;
			} */
	
			
			public static function getTrainingCatalogFilter($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext='',$traingTypeId,$categoryId)
			{ 
				$getlearningarray=Trainingprogram::getelearningdata($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$traingTypeId,$categoryId,'');
				return $getlearningarray;	
			}
			
			public static function getElearniingCatelog($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext='')
			{ 
				$getlearningarray=Trainingprogram::getelearningdata($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,'','','');
				return $getlearningarray;	
			}
			
			/* public static function getElearniingCatelogFilter($compid,$userid,$roleid,$cattype)
			{
				$getElearniingarray=Trainingprograms::getelearningdataFilter($compid,$userid,$roleid,$cattype);
				return $getElearniingarray;	
			} */
	
			public static function getTrainingCatalogAdvanceFilter($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$filterdata)
			{
				
				$getlearningarray=Trainingprogram::getelearningdata($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,'','',$filterdata);
				return $getlearningarray;	
				
			}
			
			public static function return_html($userid,$program1,$roleid,$compid,$itemTypeId=0,$categoryTypeID=0)
			{
				//echo '<pre>'; print_r($program1); exit;
				if($itemTypeId){$itemTypeId=CommonHelper::getEncode($itemTypeId);}
				if($categoryTypeID){$categoryTypeID=CommonHelper::getEncode($categoryTypeID);}
				 $baseUrl='/#';
                 $CssData = CommonHelper::getcompanycss($compid);
			     $Getdate_formatecomapny = CommonHelper::getdate($compid);
                 $buttonclass = 'button_lblue_r4';
				 if(count($CssData)) {
					if(!empty($CssData[0]->button_color))
						$buttonclass = $CssData[0]->button_color;
				 }


					$counted='';
					$numtraingItems=0;
					$height = '100px';
					$lineheight = '145px';
					if($counted <5)
					{
						$height = '120px';
						$lineheight = '165px';
					}
					else if($counted >5 && $counted <25)
					{
						$h11 = $counted*25;
						$height = $h11.'px';
						$h12 = $h11 + 45;
						$lineheight = $h12.'px';
					}
					else
					{
						$height = '550px';
						$lineheight = '595px';
					}
					
                  $dataArray = array();
                  if (count($program1) > 0)  {
                                       $program1=$program1->toArray();
									    for($i=0;$i<count($program1);$i++)
										{
											$program1[$i]=(array)$program1[$i];
										
											/* if ($program1[$i]['training_type_id'] == 4) 
											{							
												$item_name = stripcslashes($program1[$i]['title']);
											}else if ($program1[$i]['training_type_id'] == 1) 
											{
												
												$course_name = isset($program1[$i]['course_name']) ? stripcslashes($program1[$i]['course_name']) : '';
												$course_title = isset($program1[$i]['course_title']) ? stripcslashes($program1[$i]['course_title']) : '';
												$item_name =$course_name.' '.$course_title;
											}
											else if ($program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 ) 
											{
												$item_name = stripcslashes($program1[$i]['class_name']);
											} */
											$course_name = isset($program1[$i]['course_name']) ? stripcslashes($program1[$i]['course_name']) : '';
											$course_title = isset($program1[$i]['course_title']) ? stripcslashes($program1[$i]['course_title']) : '';
											$item_name =$course_name.' '.$course_title;
											
											if ($program1[$i]['training_type_id'] == 1) 
											{
												$scheduleid=0;
												$start_date = $program1[$i]['recursive_date'];
											}
											elseif ($program1[$i]['training_type_id'] == 5) 
											{
												$start_date =$program1[$i]['recursive_date'];
											}
											else if ($program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 ) 
											{
												if($program1[$i]['recursive_date']!='')
												{
													$start_date = date($Getdate_formatecomapny,strtotime($program1[$i]['recursive_date']));	
												}
												else
												{
													$start_date ='';
												}
											}
											/* if ($program1[$i]['training_type_id'] == 4) 
											{
												$credit_value = "--";
												$item_type = "Blended Program";
											}  */
										  // if ($program1[$i]['training_type_id'] == 1 || $program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 ) 
											//{
											   	 $credit_value = $program1[$i]['credit_value'];
												 $item_type = $program1[$i]['training_type'];
											//}
											
											if($program1[$i]['training_type_id']==5)
											{
												 $viewId = ''.$userid.'#'.$roleid.'||training_catalog||'.$program1[$i]['training_program_code'].'';
												// $encoded_viewId = CommonHelper::getEncode($viewId);
				                                 $encoded_viewId = $program1[$i]['training_program_code'];
                                                $url ='';
												$view_button = '<a class="change_col '.$buttonclass.'" href="' . $baseUrl . '/course/viewassessment/'.$encoded_viewId.'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
												
												//$url ='/survey/viewassessment/user/'.$encodeduserid.'/token/'.$token.'/red_page/learnassessments';
											}
											else if ($program1[$i]['training_type_id'] == 1) 
											{
												
												//$viewId = ''.$userid.'#'.$roleid.'||training_catalog||'.$program1[$i]['training_program_code'].'';
												//$encoded_viewId = CommonHelper::getEncode($viewId);
												$token=$program1[$i]['training_program_code'];
												$url='/elearningmedia/training/'.$token;
												$view_button = '<a class="change_col '.$buttonclass.'" href="' . $baseUrl . '/elearningmedia/training/'.$token.'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
											}
											else if ($program1[$i]['training_type_id'] == 2 || $program1[$i]['training_type_id'] == 3 )
											{
												
											//	$viewId = ''.$userid.'#'.$roleid.'||training_catalog||'.$program1[$i]['item_id'].'||'.$program1[$i]['class_id'].'||'.$program1[$i]['training_type_id'];
												//$encoded_viewId = CommonHelper::getEncode($viewId);
												$token=$program1[$i]['training_program_code'];
												$url='#/classroomcourse/training/'.$token.'/'.$program1[$i]['class_id'];
												//$url='/classroomcourse/training/viewId/'.$encoded_viewId.'/itemTypeId/'.$itemTypeId.'/categoryTypeID/'.$categoryTypeID;
												$view_button = '<a class="change_col '.$buttonclass.'" href="' . $url.'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
											}
											 if(in_array($program1[$i]['training_type_id'], array(2,3)) && $start_date != '')
											   {	
												$date = DateTime::createFromFormat($Getdate_formatecomapny, $start_date);
												$start_dateinymd 	= 		$date->format('Y-m-d');
												
												if((strtotime($start_dateinymd)) >= (strtotime(date('Y-m-d'))))
												{ 	
													$start_date = date($Getdate_formatecomapny,strtotime($program1[$i]['recursive_date']));
													$dataArray[] = array("item_name"=>$item_name, "start_date"=>$start_date, "credit_value"=>$credit_value, "item_type"=>$item_type, "view"=>$view_button,'viewHref'=>$url);	
												}
											 }
											else
											{
												$dataArray[] =  array("item_name"=>$item_name, "start_date"=>$start_date, "credit_value"=>$credit_value, "item_type"=>$item_type, "view"=>$view_button,'viewHref'=>$url);	
											}
							
										}

				  }
				  return $dataArray;
				 
			}		

}