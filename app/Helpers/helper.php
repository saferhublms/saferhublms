<?php
namespace App\Helpers;
use DB;
use Cache;
use Session;
use Auth;
use App\Models\User;
use App\Models\MediaType;
use App\Models\UploadedFile;
use App\Models\CompanyCss;
use App\Models\CertificateContent;
use App\Models\TrainingPrograms;
use App\Http\Traits\Item;
use App\Http\Traits\uploadfiles;
use App\Http\Traits\Admin;
use App\Http\Traits\Usertrainingcatalogs;
use stdClass,CommonFunctions,ZipArchive,DateTime,DateTimeZone;

class CommonHelper {


    public static function encode_token($id)
	{ 
		//$make_token=base64_encode($id);
		$make_token=encrypt($id);//cahnged on 12-09-2017
		return $make_token;
	}
	
	public static function auth_status($token)
	{
		try {
			$make_id = decrypt($token);
			if(is_numeric($make_id)){
			   $users_table=User::whereUserId($make_id)->first();
				if($users_table){
					return 1;
				}
				else{
					return 0;
				}
			}else{
				return 0;
				}	
		}catch (DecryptException $e) {
			 return 0;
		}


		//$make_id=base64_decode($token,true);
		
		
	}
	
	
	public static function token_decode($token)
	{
		try {
			$make_id=decrypt($token);
			return $make_id;
		} catch (DecryptException $e) {
		    return 0;
	    }
		
	}
	
	public static function authtokenuserdtail($make_id)
	{
		   $users_table=User::whereUserId($make_id)->first();
			return $users_table;
		
	}
	
	
	public static function random_string1($str_len=30) 
	{
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$result = '';
		for ($i = 0; $i < $str_len; $i++){
			$result .= $characters[mt_rand(0, 61)];
		}
		return $result;
	}

       public static function objecttoarray($array)
         {
			 $make_arr=array();
			 for($i=0;$i<count($array);$i++)
			 {
				 $make_arr[]=$array[$i]->make_id;
			 }
			 return $make_arr;
	
          }
   /**
     * getMenuList() - Method to get menu list 
     * Used : uses in the left menu navigation and manage menu items controller for super admin
     */
	public static function getMenuList($parent_id=0, $is_active="", $menu_type_id="", $grid="yes"){
		
		//Get Company id and Role id
		$identity=Auth::user();
		
		$role_id =   $identity->role_id;
		$company_id = $identity->company_id;
		
      
		$sel_arr = array();
		if($grid=="yes") {
			$cats_sql = "SELECT mi.menu_item_id, mi.parent_id, mi.menu_name, mi.description, mi.menu_url, mi.menu_icon, mi.is_active, mi.menu_type_id, mt.menu_type, mi.pref FROM menu_item AS mi LEFT JOIN menu_type AS mt ON (mt.menu_type_id=mi.menu_type_id) WHERE mi.parent_id=$parent_id ";
		}else {
			$cats_sql = "SELECT mi.menu_item_id, mi.parent_id, mi.menu_name, mi.description, mi.menu_url, mi.menu_icon, mi.is_active, mi.menu_type_id, mt.menu_type FROM menu_setting AS ms LEFT JOIN menu_item AS mi ON(mi.menu_item_id=ms.menu_id) LEFT JOIN menu_type AS mt ON (mt.menu_type_id=mi.menu_type_id) WHERE ms.company_id='$company_id' AND ms.user_role_id=$role_id AND mi.parent_id=$parent_id " ;	
		}
		
		if($is_active!="") {
			$cats_sql .= " AND mi.is_active=$is_active ";
			
		}
		
		if($menu_type_id!="") {
			$cats_sql .= " AND mi.menu_type_id=$menu_type_id ";
			
		}
		
		$cats_sql .= "ORDER BY mi.pref ";
		return  DB::select(DB::raw($cats_sql));
		//return  $data;
				
	}
	
	
	/**
     * getCompanySettingGroups() - Method to get company groups settings  
     * Param : company_id
	 * Created By: Siddharth Kushwah
	 * Created Date: 21 Dec, 2017
     */
	public static function getCompanySettingGroups($company_id)
	{
		$sql = "SELECT ca.action_code, if(car.role_ids!='',car.role_ids,'0')  AS role_ids FROM company_action ca 
			left join company_action_role car on car.action_code = ca.action_code and car.company_id='$company_id' AND car.is_active=1";
		$res  =DB::select(DB::raw($sql));
		
		$permission = array();
		if($res)
		{
			foreach($res as $rep){
				$permission[$rep->action_code] = explode(",",$rep->role_ids);
			}
			
		}
		return $permission;
	}
	
	
		
	
		public static function create_salt()
		{
			return CommonHelper::getEncode(mcrypt_create_iv(8, MCRYPT_DEV_URANDOM), 'no');
		}
		
		public static function getEncode($param, $add_salt_key='yes', $salt_key='')
		{
			if($add_salt_key=='yes'){
				$result = str_replace(array('+','/','='),array('-','_',''),base64_encode($param));
				if($salt_key!=''){

					$result = $salt_key.$salt_key.$result.$salt_key;
				}else{
					$result = CommonHelper::randLetter().CommonHelper::randLetter().$result.CommonHelper::randLetter();
				}
			}else{
				$result = base64_encode($param);
			}
			return $result;
		}
		
		public static function randLetter()
		{
			$int = rand(0,51);
			$a_z = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$rand_letter = $a_z[$int];
			return $rand_letter;
		}
		
		
		public static function getdate($company_id)
		{
			//get date from data
			
			$Getdate_formate = "select company_date_formate from company_cms_info where company_id = '$company_id' and is_active = 1";
			$company_dateformate = DB::select($Getdate_formate);
			$result = $company_dateformate[0]->company_date_formate!='' ? $company_dateformate[0]->company_date_formate :'MM/DD/YYYY' ;
			return $result;
		}

		
		
		public static function getDecode($param, $add_salt_key='yes', $salt_key='')
		{
			$data = '';
			if($add_salt_key=='yes'){
				if($salt_key==''){
					$param = substr($param, 2, -1);
				}
				else{
					$param = substr($param, 1, -1);
				}
				$data = str_replace(array('-','_'),array('+','/'),$param);
				$mod4 = strlen($data) % 4;
				if ($mod4) {
					$data .= substr('====', $mod4);
				}
				$result = base64_decode($data);	
				if(CommonHelper::checkDecode($data)){
					return $result;
				}else
				{
					return redirect('/');
				}		
			}else{
				$result = base64_decode($param);
				return $result ;
			}			
		}
		
		public static function checkDecode($data)
		{
		// Check if there are valid base64 characters
		if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $data)) return false;

		// Decode the string in strict mode and check the results
		$decoded = base64_decode($data, true);
		if(false === $decoded)  return false;

		// Encode the string again
		if(base64_encode($decoded) != $data)  return false;

		return true;
		
		}
		 public static function uploadAction($fileData)
			{
				$compid = 1;
				$userid = 1;
				$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/user/uploadfiles';	
				$fileName=$fileData->getClientOriginalName();
				$file = $compid.rand(10,100).$fileName;
				if($fileData->move($uploaddir,$file)) {
					$data=array();
					$data['file_name'] = $fileName;
					$data['file_path'] = $file;
					$data['format_id'] = 1;
					$data['file_size'] = 200;
					$data['company_id'] = $compid;
					$data['is_active'] = 1;
					$uploadSave=UploadedFile::create($data);
					return $uploadSave->files_id;
				}
			}

		
	  public static function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }	
		
	public Static function encode($value){
	    $skey = "your123SecretKey";
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim(CommonHelper::safe_b64encode($crypttext));
    }
	
	
	  public static function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
	
	
	 public static function decode($value){
		$skey = "your123SecretKey";
        if(!$value){return false;}
        $crypttext = CommonHelper::safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    
public static function prevent_from_sql_injection($str, $check_cond=null)
	{
		$sql_error = 'no error';
		
		if(preg_match('/\b(LOAD_FILE|DUMPFILE|AES_DECRYPT|AES_ENCRYPT)\b/i', strtoupper($str)))
		{
			$sql_error = 'no sqli operators';
		}
		elseif(preg_match('/(UNI\*\*ON|1 OR 1=1|1 AND 1=1|1 EXEC XP_)/', strtoupper($str)))
		{
			$sql_error = 'or';
		}
		elseif(preg_match('/(SELECT\s[\w\*\)\(\,\s]+\sFROM\s[\w]+)| (SELECT\s[\w\*\)\(\,\s]+\sFROM\s[\w]+)| (UPDATE\s[\w]+\sSET\s[\w\,\'\=]+)| (INSERT\sINTO\s[\d\w]+[\s\w\d\)\(\,]*\sVALUES\s\([\d\w\'\,\)]+)| (DELETE\sFROM\s[\d\w\'\=]+)/', strtoupper($str)))
		{ 
			$sql_error = 'SELECT';
		} 
		elseif(preg_match('/\b(SCRIPT)|(%3c)|(%3e)|(SELECT)|(UPDATE)|(INSERT)|(DELETE)|(GRANT)|(REVOKE)|(UNION)|(TRUNCATE)|(DROP)|(&amp;lt;)|(&amp;gt;)\b/', strtoupper($str)))
		{ 
			$sql_error = 'script';
		}
		if(isset($check_cond) && $check_cond=='yes')
		{
			if(preg_match('/(<[^>]+)(autofocus|alt|checked|disabled|URL|form|formaction|placeholder|src|img|input|onfocus)=.*/i', $str))
			{ $sql_error = 'error'; }// no quotes
			elseif(preg_match('/[\/\\\\]/', $str))
			{  }// no slashes
			elseif(preg_match('/\b(and|or|null|not|sleep)\b/i', $str))
			{ $sql_error = 'error'; }// no sqli boolean keywords
			elseif(preg_match('/waitfor delay/i', $str))
			{ $sql_error = 'error'; }// no sqli boolean keywords
		}
		return $sql_error;
	}
	
    public static function simpleDateToPhpDate($date)
	{
		$newDate=explode(' ',$date);
		if(count($newDate)>2){
			return $newDate[2]."-".$newDate[1]."-".$newDate[3];
		}else{
			return date('d-M-Y');
		}
		 
		
	}
	
	
	
	public static function getcompanycss($compid){
		
		
		$result = CompanyCss::where('company_id','=',$compid)->select('button_color','footer_bg','footer_link_bg','header_bg','header_text','header_title','template_id')->get();
		if(count($result)==0){
			$result[0] =  new \stdClass();
			$result[0]->header_bg='';
			$result[0]->header_text='';
			$result[0]->header_title='';
			$result[0]->button_color='';
			$result[0]->footer_link_bg='';
			$result[0]->template_id='';
			$result[0]->footer_bg='';
		}
		
		return $result;
	}
	public static function getMediaFileDetails($userid,$company_id,$item_id,$courseDetailArr){
		$filedir = $filepath = $filesname = $fileDetails = $scormDetails = $userDetails = $files = $usersDetails =$media_id = '';
		$usertrainingid=$support_formats='';
		$file_id = Item::getfileid($item_id,$company_id);
		$is_lib='';
		$courseDetailArr['file_id'] = $file_id;
		$userprogramtypeid = $courseDetailArr['training_type_id'];
		$training_program_id = $courseDetailArr['training_program_id'];
		if($file_id!=0){
		$fileDetails = uploadfiles::getfilename($file_id,$company_id);
		$userDetails = Admin::getUserName($userid, $company_id);
		if(count($fileDetails))
			{
				$filesname = $fileDetails['file_name'];
				$is_lib = $fileDetails['is_lib'];
				$media_id = $fileDetails['media_id'];
				$masteryscore = ($fileDetails['masteryscore'] > 0) ? $fileDetails['masteryscore'] : ' '  ;
				$filedir = substr($filesname, 0, -4);
				$filepath = explode('.', $filesname);
				$support_formats = explode("|",$fileDetails['support_formats']);
			}
		
			
		}

        $courseDetailArr['media_id'] 				= 	$media_id;
		$courseDetailArr['filedir'] 				= 	$filedir;
		$courseDetailArr['filepath'] 				= 	$filepath;
		$courseDetailArr['filesname'] 				= 	$filesname;
		$courseDetailArr['fileDetails'] 			= 	$fileDetails;
		$courseDetailArr['userDetails'] 			= 	$userDetails;
		$courseDetailArr['passingscore'] 			= 	$masteryscore;
		$courseDetailArr['support_formats'] 		= 	$support_formats;
		$courseDetailArr['is_lib'] 		= 	$is_lib;
		
		
		return $courseDetailArr;		
		
	}
	
	
	
	public static function blendedutcentry($blendedprogid,$userid,$compid)
	{	
		if($blendedprogid!=0)
		{

			$checkBlendedEntry = Usertrainingcatalogs::checkBlendedEntry($blendedprogid,$compid);
			if(count($checkBlendedEntry)<1)
			{
				$data = array(
					'user_id'               => $userid,
					'program_id'          	=> 0,
					'is_enroll'             => 1,
					'is_approved'           => 1,
					'training_type_id'      => 4,
					'user_program_type_id'	=> 0,
					'item_id'           	=> 0,
					'company_id'            => $compid,
					'training_status_id'    => 3,
					'blended_program_id'    => $blendedprogid,
					'learning_plan_id'    	=> 0,
					'session_id'       		=> '',
					'last_modfied_by'     	=> $userid,
					'created_date'    		=> date('Y-m-d H-i-s'),
				);
				Usertrainingcatalogs::insertusertrainigcatalog($data);
			}
		}		
	}
	
	public static function getdatecalenderformate($Getdate_formatecomapny)
	{
		$replaceyformate= str_replace('Y',"yy",$Getdate_formatecomapny);
		$replacemothfor= str_replace('m',"mm",$replaceyformate);
		$calenderdateformate= str_replace('d',"dd",$replacemothfor);
		return $calenderdateformate;
	}
	
	public static function getdatecalenderformateAngular($Getdate_formatecomapny)
	{
		$replaceyformate= str_replace('Y',"yyyy",$Getdate_formatecomapny);
		$replacemothfor= str_replace('m',"MM",$replaceyformate);
		$calenderdateformate= str_replace('d',"dd",$replacemothfor);
		$calenderdateformate= str_replace('H',"HH",$replacemothfor);
		$calenderdateformate= str_replace('i',"mm",$replacemothfor);
		return $calenderdateformate;
	}
	
	public static function getmediatypes($media_id = null, $media_name = null)
	{
		if($media_id){
			$result=MediaType::where('media_status','yes')->where('media_id',$media_id)->first();
			if($media_name){
				return $result->media_name;
			}else{
				return $result->support_formats;
			}
		}else{
			$result=MediaType::where('media_status','yes')->orderBy('media_name','asc')->get();
			return $result;
		}
	}
	
	public static function getcertificates($compid) 
	{
		$result=array();
		if(isset($compid))
		{
			$result=CertificateContent::where('certificate_content.status','=','1')
				->where('certificate_content.company_id','=',$compid)
				->orderBy('certificate_content.title','ASC')
				->select('id','title')
				->get()->toArray();
		}
		
		if(count($result))
		{
			foreach($result as $key=>$value){
				
				//$encodedId=CommonFunctions::getEncode($value['id']);
				$result[$key]['id']=$value['id'];
				 			
			}
			return $result;	
			
		}else{
			return array(); 
		}
		
		
		
	}
	
	public static function trainingCode($request){
		$returnCodeGet=CommonHelper::trainingCodeString($request);
		$trainingCodeExist=TrainingPrograms::where('training_program_code',$returnCodeGet)->first();
		if($trainingCodeExist){
				CommonHelper::trainingCode($request);
			}else{
				return $returnCodeGet;
			}
		
	}
	
	
	public static function trainingCodeString($request)
	{
		$mediaType=$request->input('mediaType');
		$trainingType=$request->input('trainingType');
		$compid	= Auth::user()->company_id;
		if(strlen($mediaType)<2){
			$mediaType='0'.$mediaType;
		}
		if(strlen($compid)<4){
			$compid=str_pad($compid,4,"0",STR_PAD_LEFT);
		}
		$returnString='1'.$trainingType.$mediaType.$compid.sprintf("%08d", mt_rand(1, 99999999));
		return $returnString;
	}
	
	public static function random_string ($length=15) {
		$pool  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pool .= 'abcdefghijklmnopqrstuvwxyz';
		$pool .= '0123456789';
		$poollen = strlen($pool);
		mt_srand ((double) microtime() * 1000000);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($pool, (mt_rand()%($poollen)), 1);
		}
		return $string;
	}

		public static function unzipmediafile($filname,$filname2,$path){
				$zip = new \ZipArchive;
				$res = $zip->open($path.$filname);
				if ($res === TRUE) {
				$zip->extractTo("/administrator");
				$zip->close();
				} 
			}
			
	public static function unrarmediafile($filname,$filname2,$path){
		$rar_file = rar_open($path.$filname);
		$entries = rar_list($rar_file);
		foreach ($entries as $entry) {
			$entry->extract($path.$filname2);
		}
		rar_close($rar_file);
	}
	
	public static function currentTimezone()
	{
		$ip_address = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');
		// Get JSON object
		$jsondata = file_get_contents("http://timezoneapi.io/api/ip/?".$ip_address);
		// Decode
		$data = json_decode($jsondata, true);
		// Request OK?
		if($data['meta']['code'] == '200'){
			if(isset($data['data']['timezone'])){
				return $data['data']['timezone']['id'];
			}else{
				return 'UTC';
			}
	
	}else{
		return 'UTC';
	}
	}
	
	public static function getUserTimeZone($userId)
	{
		$userTimeZone=User::join('class_timezones','class_timezones.class_timezone_id','=','user_master.timezone_id')
		              ->select('timezone_regions')->first();
					  
		return ($userTimeZone)?$userTimeZone->timezone_regions:'UTC';
	}
	
	public static function convertDateToSpecificTimezone($date,$userTimeZone)
	{
		$date = new DateTime($date, new DateTimeZone(config('app.timezone')));
		$date->setTimezone(new DateTimeZone($userTimeZone));
		$newDueDate=$date->format('Y-m-d');
		return $newDueDate;
	}
	
}