<?php
namespace App\Helpers;

use App\Http\Traits\Admin;
use App\Http\Traits\Coursecredits;
use App\Http\Traits\Sessionschedules;
use App\Http\Traits\Sendmail;
use App\Models\CompanySettings;
use App\Models\TbluserlLink;
use App\Models\UserMaster;
use CommonHelper,URL,CommonFunctions;

class Mailnotification 
{
	use Admin,Coursecredits,Sendmail;
	
	public static $notificationvariable = array("username" => "" ,"trainingclassname" => "" ,"trdate" => "" ,"sendscedule" => "" ,"urlclicklink" => "" ,"starthr" => "" ,"startmin" => "" ,"timeformat" => "","virtualSessionInfo" => "" ,"INstructorname" => "" ,"location" => "" ,"companyname" => "" ,"supervisorusername" => "" , "usermailid" => "" , "companyurl" => "" , "assessmentname" => "" , "login_id" => "" , "password" => "" , "index" => "" , "title" => "" , "duedate" => "" , "assignmentname" => "" , "prerequisite" => "" , "cost" => "" , "topic" => "" , "posted_by" => "" , "date_of_completion" => "", "customlink"=> "" , "courseid"=> "" , "InstructorloginId"=> "" , "timezone_name"=>"" , "expiredate" => "" , "score" => "", "course_detail" => "", "learning_plan_name" => ""); 
    
	
	public static function emailforManualAddedItem($trainingitem)
	{
		$notificationvariable = array("username" => "" ,"trainingclassname" => "" ,"trdate" => "" ,"sendscedule" => "" ,"urlclicklink" => "" ,"starthr" => "" ,"startmin" => "" ,"timeformat" => "","virtualSessionInfo" => "" ,"INstructorname" => "" ,"location" => "" ,"companyname" => "" ,"supervisorusername" => "" , "usermailid" => "" , "companyurl" => "" , "assessmentname" => "" , "login_id" => "" , "password" => "" , "index" => "" , "title" => "" , "duedate" => "" , "assignmentname" => "" , "prerequisite" => "" , "cost" => "" , "topic" => "" , "posted_by" => "" , "date_of_completion" => "", "customlink"=> "" , "courseid"=> "" , "InstructorloginId"=> "" , "timezone_name"=>"" , "expiredate" => "" , "score" => "", "course_detail" => "", "learning_plan_name" => ""); 
		$userid	=	1;
		$compid	=	1;
		$companyname = Mailnotification::getFromCompanyName($compid);
		$userinfo 	= Coursecredits::getUserInfo($compid,$userid);
		$emailid = $userinfo[0]->email_id;
		$emailname = $userinfo[0]->user_name;
		$user_name = $userinfo[0]->user_name;
		
		$notificationvariable["username"]=$user_name;
		$notificationvariable["trainingclassname"]=$trainingitem['training_title'];
		$notificationvariable["virtualSessionInfo"]= $trainingitem['training_desc'];
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["toemailid"]=$emailid;
		$notificationvariable["user_id"]=$userid;
		$notificationvariable["credit"] = $trainingitem['training_credit'];
		$notificationvariable["date_of_completion"] = $trainingitem['completion_date'];
		$notificationvariable["starthr"] = $trainingitem['completion_date'];
		$notificationvariable["score"] = $trainingitem['training_score'];
		$notificationvariable["company_id"]=$trainingitem['company_id'];
		
		$getNotificationContent=Sendmail::getNotificationContent(30, $compid, $notificationvariable);
	}
	
	
	public static function getFromCompanyName($company_id)
	{
		$company_name=Admin::getcompanyname($company_id);	
		$company_details=CompanySettings::wherecompanyId($company_id)->first();
		$cmp_name = ($company_details['company_name'] != '') ? $company_details['company_name'] : $company_name[0]->company_name;
		
		return  $cmp_name;
	}
	
	
	/* Here Mail send To assignment user and group user */
	public static function AssignmentMailsendtoAllUser($assignArray,$roleid){
		for($k=0;$k<count($assignArray);$k++){
			$assignmentId=$assignArray[$k]['assignment_id'];
			$user_id 	= $assignArray[$k]['user_id'];
			$getAlltrainingIdnadcompaynid=Sendmail::AssignmentRecord($assignmentId);
			$companyid=$getAlltrainingIdnadcompaynid[0]->company_id;
			$training_program_id=$getAlltrainingIdnadcompaynid[0]->training_program_id;
			$due_date=$getAlltrainingIdnadcompaynid[0]->due_date;
			$assignment_start_date=$getAlltrainingIdnadcompaynid[0]->assignment_start_date;
			$created_date=$getAlltrainingIdnadcompaynid[0]->created_date;
			$getTrainingrecord=Sendmail::TrainingProgramRecord($training_program_id);
			$item_id=$getTrainingrecord[0]->item_id;
			$class_id=$getTrainingrecord[0]->class_id;
			$blended_program_id=$getTrainingrecord[0]->blended_program_id;
			if($item_id!=0 && $item_id!=''){
				$getItemDetails=Sendmail::ItemRecord($item_id);
				$title=$getItemDetails[0]->title;
				$description=$getItemDetails[0]->description;
			}
			if($blended_program_id!=0 && $blended_program_id!=''){
				$getItemDetails=Sendmail::BlenderRecord($blended_program_id);	
				
				$title=$getItemDetails[0]->title;
				$description=$getItemDetails[0]->description;
				
			}
			
			if($class_id != '' && $class_id != 0){
				$getschdulename=Sendmail::getschedulenamefromscheduleclass($class_id);
				$scheduleid=0;
				if(count($getschdulename)>0){
					$trainingClassName=$getschdulename[0]->schedule_name;
					$scheduleid=$getschdulename[0]->class_schedule_id;
				}
			}else{
				$trainingClassName=$title;
			}
			
			if($k==0)
			{	
				$companyname=Mailnotification::getFromCompanyName($companyid);				
				$fromname = $companyname;
				$from = Mailnotification::getFromEmailIds($companyid);
				$subject = "New Learning Assignment";
			}
			
			$useremailget=Sendmail::getuseremail($user_id,$companyid);
			if(count($useremailget)>0){
				foreach($useremailget as $valret){
					if (is_object($valret)) {
						$username=$valret->user_name;
						$useremail=$valret->email_id;
					}
					else{
						$username=$valret['user_name'];
						$useremail=$valret['email_id'];
					}						
				}
				
				$datalink=array();
				$datalink['training_catalog_id']=0;
				$datalink['company_id']=$companyid;
				$datalink['user_id']=$user_id;						
				$randnumber=time().$assignmentId;
				$datalink['randomnumber']=$randnumber;
				$roleid = $useremailget[0]->role_id;
				$snurl=$randnumber."_".$companyid."_".$user_id;
				$paramurl=md5($snurl);
				$datalink['send_URL']=$paramurl;
				if($blended_program_id!=0 && $blended_program_id!='')
				{
					$viewId = ''.$user_id.'#'.$roleid.'||training_catalog||'.$blended_program_id.'||1';
					$encoded_viewId = CommonHelper::getEncode($viewId);
						
						
					$datalink['pagename']="trainingcatalog/blendedprogram";
					$datalink['parameter']="viewId/$encoded_viewId";
				}else{
					if($class_id!=0 && $class_id!='')
					{
						$viewId = ''.$user_id.'#'.$roleid.'||training_catalog||'.$item_id.'||'.$class_id.'';
						$encoded_viewId = CommonHelper::getEncode($viewId);
								
						$datalink['pagename']="classroomcourse/training";
						$datalink['parameter']="viewId/$encoded_viewId";
					}else{
						$viewId = ''.$user_id.'#'.$roleid.'||training_catalog||'.$training_program_id.'';
						$encoded_viewId = CommonHelper::getEncode($viewId);
								
						$datalink['pagename']="elearningmedia/training";
						$datalink['parameter']="viewId/$encoded_viewId";
					}
				}
				
				$id2=Sendmail::insertanywhere("tbl_user_link",$datalink);
				$toemailid = $useremail;
				$companyurl=$_SERVER["HTTP_HOST"];
				$baseUrl = URL::to('/');
				$urlsend='<a href="http://'.$companyurl.$baseurl.'/linkurlsend/sendurlauth/Param/'.$paramurl.'">View Assignment</a>'; 
				$custom='/linkurlsend/sendurlauth/Param/'.$paramurl; 
				$starthr=date('m/d/Y',strtotime($due_date));
				$assignment_start_date=date('m/d/Y',strtotime($assignment_start_date));
				$this->notificationvariable["username"]=$username;
				$this->notificationvariable["assignmentname"]=$trainingClassName;						
				$this->notificationvariable["duedate"]=$starthr;
				$this->notificationvariable["starthr"]=$assignment_start_date;
				$this->notificationvariable["urlclicklink"]=$urlsend;
				$this->notificationvariable["customlink"]=$custom;
			    $this->notificationvariable["virtualSessionInfo"]=$description; 
				$this->notificationvariable["companyname"]=$companyname;
				$this->notificationvariable["toemailid"]=$toemailid;      
				$getNotificationContent=Sendmail::getNotificationContent(13,$companyid,$this->notificationvariable);
			}
		}
	}
	
	
	
	public static function getFromEmailIds($company_id){
		 $company_details = CompanySettings::where('company_id','=',$company_id)->first()->toArray();
		return  (isset($company_details['admin_email'])) ? $company_details['admin_email'] : "noreply@torchlms.com";
	}
	
	public static function employeenrolmentRequested($usertrainingid,$paramurl, $class_detail=array())
	{
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);
		$userid=$getAlldetails[0]->user_id;
		$course_id=$getAlldetails[0]->course_id;
		
		$company_id=$getAlldetails[0]->company_id;
		$item_name=$getAlldetails[0]->item_name;
		$class_name=$getAlldetails[0]->class_name;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=$getAlldetails[0]->class_name;
		$class_id=$getAlldetails[0]->class_id;
		$useremail=$getAlldetails[0]->email_id;
		$username=$getAlldetails[0]->user_name;
		$supervisor_id=$getAlldetails[0]->supervisor_id;
		$companyname=Mailnotification::getFromCompanyName($company_id);	
		$companyurl=$getAlldetails[0]->app_url;
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$company_id);
		$supervisorusername=(count($getsupervisiorDetail)>0) ? $getsupervisiorDetail[0]->user_name :'';
		$courseDes=Sendmail::getCourseDes($course_id,$company_id);
		$classinfoonitemid = Sendmail::classinfoonitemid($class_detail['class_id'] , $company_id);
		if($classinfoonitemid->delivery_type_id == 2)
		{
			
			$description=$courseDes->course_description;
			$virtualSessionInfo=$classinfoonitemid->virtual_session_information;
		}
		else
		{
			$description=$courseDes->course_description;		
		}
		
		$sendscedule='';
		$totalduration=0;
		if($class_id!=0)
		{
			$sendscedule  = Mailnotification::getClassAllSessions($class_id, $company_id);
			$totalduration= Mailnotification::getTotalduration($class_id, $company_id);
		}	
		
		$baseurl = URL::to('/');
		$urlclicklink='<a href="http://'.$companyurl.$baseurl.'/linkurlsend/sendurlauth/Param/'.@$paramurl.'"?height=400&width=625&inlineId=hiddenModalContent&modal=false" class="thickbox">Click here</a>';
		$custom='/linkurlsend/sendurlauth/Param/'.@$paramurl;

		$all_sessions_res=Sendmail::getScheduleStartDate($class_id ,$company_id);
		$all_sessions = json_decode(json_encode($all_sessions_res),true);
		$currentdate = date('Y-m-d',time());
		
		$total_session = count($all_sessions);
		$i=1;
		foreach($all_sessions as $classstartdate)
		{
			if($currentdate <= $classstartdate['dayid'])
			{
				$start_date = date('m/d/Y',strtotime($classstartdate['dayid']));
				$timezone_name = $classstartdate['timezone_name'];
				$timezone_regions = $classstartdate['timezone_regions'];
				$timezone_dst = $classstartdate['timezone_dst'];
				$utc_offset = $classstartdate['utc_offset'];
				$utc_offset_dst = $classstartdate['utc_offset_dst'];
				$dureation_hrs = $classstartdate['ses_length_hr'];
				$dureation_min = $classstartdate['ses_length_min'];
				
				$start_time_hr = $classstartdate['start_time_hr'];
				$start_time_min = $classstartdate['start_time_min'];
				$timeformat = $classstartdate['timeformat']; //AM PM
				$INstructorname = $classstartdate['user_name'];
				$schedule_id = $classstartdate['schedule_id'];
				
				$location = ($classstartdate['location']!='') ? $classstartdate['location'] : '';
			
				$notificationvariable=self::$notificationvariable;
				
				$notificationvariable["username"]=$username;
				$notificationvariable["trainingclassname"]=stripslashes(stripslashes($class_name));
				$notificationvariable["trdate"]=$start_date;
				$notificationvariable["item_name"]=$item_name;
				$notificationvariable["schedule_id"]=$schedule_id;
				$notificationvariable["sendscedule"]=$sendscedule;
				$notificationvariable["totalduration"]=$totalduration;
				$notificationvariable["urlclicklink"]=$urlclicklink;
				$notificationvariable["customlink"]=$custom;
				$notificationvariable["dureation_hrs"]=$dureation_hrs;
				$notificationvariable["dureation_min"]=$dureation_min;
				$notificationvariable["starthr"]=sprintf("%02s:", $start_time_hr);
				$notificationvariable["startmin"]=sprintf("%02s", $start_time_min);
				$notificationvariable["timeformat"]=$timeformat;
				$notificationvariable["timezone_name"]=$timezone_name;
				$notificationvariable["timezone_regions"]=$timezone_regions;
				$notificationvariable["timezone_dst"]=$timezone_dst;
				$notificationvariable["utc_offset"]=$utc_offset;
				$notificationvariable["utc_offset_dst"]=$utc_offset_dst;
				$notificationvariable["INstructorname"]=$INstructorname;
				$notificationvariable["location"]=$location;
				$notificationvariable["companyname"]=$companyname;
				$notificationvariable["supervisorusername"]=$supervisorusername;
				$notificationvariable["toemailid"]=$useremail;
				$notificationvariable["course_id"]=$course_id;
				$notificationvariable["class_id"]=$class_detail['class_id'];
				$notificationvariable["user_id"]=$class_detail['user_id'];
				$notificationvariable["company_id"]=$class_detail['company_id'];
				$notificationvariable["start_time_hr"]=$start_time_hr;
				$notificationvariable["start_time_min"]=$start_time_min;
				$notificationvariable["total_session"]="$i/$total_session";
			
				if($classinfoonitemid->delivery_type_id == 2)
				{   
					$notificationvariable["virtualSessionInfo"]=stripslashes($virtualSessionInfo);
					$notificationvariable["description"]=stripslashes($description);
					$getNotificationContent=Sendmail::getIcalNotificationContent(38, $company_id, $notificationvariable);
				}
				else
				{
					$notificationvariable["description"]=stripslashes($description);
					$getNotificationContent=Sendmail::getIcalNotificationContent(7, $company_id, $notificationvariable);
				}
			}
			$i++;
		}
	}
	
	public static function getClassAllSessions($sessionid, $company_id)
	{
		
		$programsschedule = Sendmail::allSessionsForClassMail($sessionid,$company_id);
		
		$Getdate_formatecomapny = CommonHelper::getdate($company_id);
		$k=1;					
		$sendscedule = '';
		if(count($programsschedule)>0)
		{
			foreach($programsschedule as $key => $value)
			{
				$endtimehr=$value->start_time_hr+$value->ses_length_hr;
				$endtimemin=$value->start_time_min+$value->ses_length_min;
				if($value->timeformat=='PM' && $value->start_time_hr!=12)
				{
					$starthour=$value->start_time_hr+12;
				}
				elseif($value->timeformat=='AM' && $value->start_time_hr==12)
				{
					$starthour=$value->start_time_hr+12;
				}
				else
				{
					$starthour=$value->start_time_hr;
				}
				$starttimeval=$starthour.":".$value->start_time_min;
				$endtimehr=$starthour+$value->ses_length_hr;
				$endtimemin=$value->start_time_min+$value->ses_length_min;
				$nextEndtime=date('h:i A',mktime($endtimehr, $endtimemin, 0, 0, 0, 0000));
				$starttimeclass=date('h:i ',strtotime($value->start_time_hr.":".$value->start_time_min));
				$starttimeclass=$starttimeclass." ".$value->timeformat;
				
				@$timezone=$value->time_zone;
				if(@$timezone=='0'){$timezone="";}
				if(@$timezone=='1'){$timezone="EST";}
				if(@$timezone=='2'){$timezone="MST";}
				if(@$timezone=='3'){$timezone="PST";}
				if(@$timezone=='4'){$timezone="AKST";}
				if(@$timezone=='5'){$timezone="HST";}
				if(@$timezone=='6'){$timezone="CST";}
				if($timezone!=''){$timezone='('.$timezone.')';}
				if(strlen($value->start_time_hr)==1){$value->start_time_hr='0'.$value->start_time_hr;}
				if(strlen($value->start_time_min)==1){$value->start_time_min='0'.$value->start_time_min;}
				$sendscedule.="<br>Session $k: ".date($Getdate_formatecomapny,strtotime($value->dayid)).' '.@$value->start_time_hr.':'.$value->start_time_min.' '.$value->timeformat.' - '.@$nextEndtime.' '.@$format;
				$k++; 
			}
		}
		return $sendscedule;
	}
	
	public static function getTotalduration($sessionid, $company_id)
	{
		
		$programsschedule = Sendmail::allSessionsForClassMail($sessionid,$company_id);	
		$convertedhour=0;
		$totalmin=0;
		if(count($programsschedule)>0)
		{
		foreach($programsschedule as $key => $value)
			{
			    $hour=$value->ses_length_hr*60;
			    $min=$value->ses_length_min;
				$totalmin +=$hour+$min;
				$convertedhour=$totalmin/60;
			}
		}
		$mainresult=0;
		if(strpos($convertedhour,'.')!==false){
	     $res=explode('.',$convertedhour);
		 $finalhour=$res[0];
		 $finalmin='.'.$res[1];
		 $min=$finalmin*60;
		 $mainresult=$finalhour.':'.$min.'( H:M)';
		}else{
		 $mainresult=$convertedhour.'( H:M)';
		}
		return $mainresult;
	}
	
	public static function employeenrolment($usertrainingid,$paramurl)
	{ 
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);		
		$userid=$getAlldetails[0]->user_id;
		$course_id=$getAlldetails[0]->course_id;
		$company_id=$getAlldetails[0]->company_id;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=$getAlldetails[0]->class_name;
		$trainingclass_id=$getAlldetails[0]->class_id;
		$class_id=$getAlldetails[0]->class_id;
		$useremail=$getAlldetails[0]->email_id;
		$username=$getAlldetails[0]->user_name;
		$supervisor_id=$getAlldetails[0]->supervisor_id;
		$companyname=Mailnotification::getFromCompanyName($company_id);	
		$companyurl=$getAlldetails[0]->app_url;
		
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$company_id);
		$supervisorusername=$getsupervisiorDetail[0]->user_name;

	    $virtualSessionInfo='';
		$description='';
		if(isset($course_id) && isset($company_id)){
			$courseDes=Sendmail::getCourseDes($course_id,$company_id);
			$classinfoonitemid = Sendmail::classinfoonitemid($trainingclass_id , $company_id);
			if($classinfoonitemid->delivery_type_id == 2)
		      {
			
				$description=$courseDes->course_description;
				$virtualSessionInfo=$classinfoonitemid->virtual_session_information;
		      }
		       else
		      {
			    $description=$courseDes->course_description;		
		      }
		}
		$sendscedule='';
		if($class_id!=0)
		{
			$sendscedule = Mailnotification::getClassAllSessions($class_id, $company_id);
		}
		
		
		$baseurl = URL::to('/');
		$urlclicklink='<a href="http://'.$companyurl.$baseurl.'/linkurlsend/sendurlauth/Param/'.@$paramurl.'"?height=400&width=625&inlineId=hiddenModalContent&modal=false" class="thickbox">Click here</a>';		
		$custom='/linkurlsend/sendurlauth/Param/'.@$paramurl;
	
		$all_sessions_res=Sendmail::getScheduleStartDate($class_id ,$company_id);
		$all_sessions = json_decode(json_encode($all_sessions_res),true);
		$currentdate = date('Y-m-d',time());
		
		$total_session = count($all_sessions);
		$i=1;
		foreach($all_sessions as $classstartdate){
		
			$start_date = date('m/d/Y',strtotime($classstartdate['dayid']));
			$timezone_name = $classstartdate['timezone_name'];
			$timezone_regions = $classstartdate['timezone_regions'];
			$timezone_dst = $classstartdate['timezone_dst'];
			$utc_offset = $classstartdate['utc_offset'];
			$utc_offset_dst = $classstartdate['utc_offset_dst'];
			$dureation_hrs = $classstartdate['ses_length_hr'];
			$dureation_min = $classstartdate['ses_length_min'];
			$start_time_hr = $classstartdate['start_time_hr'];
			$start_time_min = $classstartdate['start_time_min'];
			$timeformat = $classstartdate['timeformat']; //AM PM
			$INstructorname = $classstartdate['user_name'];
			$location = ($classstartdate['location']!=0) ? $classstartdate['location'] : '';
			$schedule_id = $classstartdate['schedule_id'];
			
			$notificationvariable=self::$notificationvariable;
			
			$notificationvariable["username"]=$username;
			$notificationvariable["trainingclassname"]=stripslashes(stripslashes($trainingClassName));
			$notificationvariable["trdate"]=$start_date;
			$notificationvariable["schedule_id"]=$schedule_id;
			$notificationvariable["sendscedule"]=$sendscedule;
			$notificationvariable["urlclicklink"]=$urlclicklink;
			$notificationvariable["customlink"]=$custom;
			$notificationvariable["dureation_hrs"]=$dureation_hrs;
			$notificationvariable["dureation_min"]=$dureation_min;
			$notificationvariable["starthr"]=sprintf("%02s:", $start_time_hr);
			$notificationvariable["startmin"]=sprintf("%02s", $start_time_min);
			$notificationvariable["timeformat"]=$timeformat;
			$notificationvariable["timezone_name"]=$timezone_name;
			$notificationvariable["timezone_regions"]=$timezone_regions;
			$notificationvariable["timezone_dst"]=$timezone_dst;
			$notificationvariable["utc_offset"]=$utc_offset;
			$notificationvariable["utc_offset_dst"]=$utc_offset_dst;
			$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
			$notificationvariable["description"]=$description;
			$notificationvariable["INstructorname"]=$INstructorname;
			$notificationvariable["location"]=$location;
			$notificationvariable["companyname"]=$companyname;
			$notificationvariable["supervisorusername"]=$supervisorusername;
			$notificationvariable["toemailid"]=$useremail;
			$notificationvariable["course_id"]=$course_id;
			$notificationvariable["class_id"]=$trainingclass_id;
			$notificationvariable["user_id"]=$userid;
			$notificationvariable["company_id"]=$company_id;
			$notificationvariable["start_time_hr"]=$start_time_hr;
			$notificationvariable["start_time_min"]=$start_time_min;
			$notificationvariable["total_session"]="$i/$total_session";
			$i++;

		}
		
		$getNotificationContent=Sendmail::getNotificationContent(2, $company_id, $notificationvariable);
	}
	
	
	public static function supervisioAprovesendmail($usertrainingid,$paramurl)
	{
		$baseurl = URL::to('/');
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);
		$userid=$getAlldetails[0]->user_id;
		$course_id=$getAlldetails[0]->course_id;
		
		$company_id=$getAlldetails[0]->company_id;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=$getAlldetails[0]->class_name;
		$trainingclass_id=$getAlldetails[0]->class_id;
		$class_id=$getAlldetails[0]->class_id;		
		$useremail=$getAlldetails[0]->email_id;
		$username=$getAlldetails[0]->user_name;
		 $supervisor_id=$getAlldetails[0]->supervisor_id;
		$companyname=Mailnotification::getFromCompanyName($company_id);	
		$companyurl=$getAlldetails[0]->app_url;		
		
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$company_id);		
		$supervisorusername=$getsupervisiorDetail[0]->user_name;
		$useremailS=$getsupervisiorDetail[0]->email_id;
		$usernameS=$getsupervisiorDetail[0]->user_name;		
		$courseDes=Sendmail::getCourseDes($course_id,$company_id);
		$classinfoonitemid = Sendmail::classinfoonitemid($trainingclass_id , $company_id);
		$virtualSessionInfo='';
		$description='';
		if($classinfoonitemid->delivery_type_id == 2)
		{
				$description=$courseDes->course_description;
				$virtualSessionInfo=$classinfoonitemid->virtual_session_information;
		}
		else
		{
			$description=$courseDes->description;		
		}
		$companyname=Mailnotification::getFromCompanyName($company_id);	
		$programsschedule = Sendmail::allSessionsForClassMail($class_id,$company_id);
		$k=1;					
		$sendscedule='';
		foreach($programsschedule as $key => $value)
		{
			$endtimehr=$value->start_time_hr+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			if($value->timeformat=='PM' && $value->start_time_hr!=12)
			{
				$starthour=$value->start_time_hr+12;
			}
			elseif($value->timeformat=='AM' && $value->start_time_hr==12)
			{
				$starthour=$value->start_time_hr+12;
			}
			else
			{
				$starthour=$value->start_time_hr;
			}
			$starttimeval=$starthour.":".$value->start_time_min;
			$endtimehr=$starthour+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			$nextEndtime=date('h:i A',mktime($endtimehr, $endtimemin, 0, 0, 0, 0000));
			$starttimeclass=date('h:i ',strtotime($value->start_time_hr.":".$value->start_time_min));
			$starttimeclass=$starttimeclass." ".$value->timeformat;
			
			@$timezone=$value->time_zone;
			if(@$timezone==0){$timezone="";}
			if(@$timezone==1){$timezone="EST";}
			if(@$timezone==2){$timezone="MST";}
			if(@$timezone==3){$timezone="PST";}
			if(@$timezone=='4'){$timezone="AKST";}
			if(@$timezone=='5'){$timezone="HST";}
			if(@$timezone=='6'){$timezone="CST";}
			if($timezone!=''){$timezone='('.$timezone.')';}
			if(strlen($value->start_time_hr)==1){$value->start_time_hr='0'.$value->start_time_hr;}
			if(strlen($value->start_time_min)==1){$value->start_time_min='0'.$value->start_time_min;}
			if($k==1)
			{
				$sendscedule.= date('m/d/Y',strtotime($value->dayid));
				$trdate=date('m/d/Y',strtotime($value->dayid));
				$starthr=@$value->start_time_hr;
				$startmin=$value->start_time_min;  
				$timeformat=$value->timeformat;  
				if($value->instructor_id!=0)
				{
					$instname=Sendmail::getinstructorname($value->instructor_id);	
					$INstructorname=@$instname->user_name;
					$INstructorname=stripslashes($INstructorname);
				}
				else{
					$INstructorname='';
				}
			}
			$sendscedule.="<br>Session $k: ".date('m/d/Y',strtotime($value->dayid)).' '.@$value->start_time_hr.':'.$value->start_time_min.' '.$value->timeformat.' - '.@$nextEndtime.' '.@$format;
			$k++; 
		} 
		
		$trainingClassName=stripslashes($trainingClassName);
	
		$viewId = $supervisor_id;
		$encoded_viewId = CommonHelper::getEncode($viewId);	

		$datalink['send_URL']=md5($paramurl);
		$datalink['training_catalog_id']=$usertrainingid;
		$datalink['company_id']=$company_id;
		$datalink['user_id']=$supervisor_id;
		$datalink['pagename']="supervisor/supervisortools";
		$datalink['parameter']="";
        TbluserlLink::insert($datalink);
		
		$urlclicklink='Please approve or deny this request by: <a href="http://'.$companyurl.$baseurl.'/linkurlsend/sendurlauth/Param/'.@md5($paramurl).'">click here</a>';
		$custom='/linkurlsend/sendurlauth/Param/'.@md5($paramurl);
		 $toemailid  = $useremailS;
		 $notificationvariable=self::$notificationvariable;
		$notificationvariable["username"]=$username; 
		$notificationvariable["trainingclassname"]=stripslashes(stripslashes($trainingClassName));
		$notificationvariable["sendscedule"]=$sendscedule;
		$notificationvariable["urlclicklink"]=$urlclicklink;
		$notificationvariable["customlink"]=$custom;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["supervisorusername"]=$supervisorusername;
		$notificationvariable["toemailid"]=$toemailid;
		$notificationvariable["virtualSessionInfo"]=stripslashes($virtualSessionInfo);
		$notificationvariable["description"]=stripslashes($description);
		
		if($classinfoonitemid->delivery_type_id == 2)
		{
				$getNotificationContent=Sendmail::getNotificationContent(39,$company_id,$notificationvariable);
		}
		else
		{
				$getNotificationContent=Sendmail::getNotificationContent(6,$company_id,$notificationvariable);		
		}
	}
	
	public static function mailToSupervisorForClassApprove($usertrainingid)
	{
        $baseurl = URL::to('/');
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);
		$userid=$getAlldetails[0]->user_id;
		$course_id=$getAlldetails[0]->course_id;
		$company_id=$getAlldetails[0]->company_id;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=$getAlldetails[0]->class_name;
		$trainingclass_id=$getAlldetails[0]->class_id;
		$useremailget=Sendmail::getuseremail($userid,$company_id);
		$emailid=$useremailget[0]->email_id;
		$username=$useremailget[0]->user_name;
		$supervisor_id=$useremailget[0]->supervisor_id;
		$class_id=$getAlldetails[0]->class_id;
		$getsupervisiorDetail=Sendmail::getsuperemail($supervisor_id,$userid,$company_id);
		$supervisorusername=$getsupervisiorDetail->user_name;
		$supervisor_email_id=$getsupervisiorDetail->email_id;
		$companyurl=$getAlldetails[0]->app_url;	
		$companyname=Mailnotification::getFromCompanyName($company_id);			
		$trainingClassName=stripslashes($trainingClassName);		
		$toemailid =  $supervisor_email_id ;
	
		$virtualSessionInfo='';

		if(isset($course_id) && isset($company_id)){
			$courseDes=Sendmail::getCourseDes($course_id,$company_id);
			$classinfoonitemid = Sendmail::classinfoonitemid($trainingclass_id , $company_id);
			if($classinfoonitemid->delivery_type_id == 2)
			{
					$virtualSessionInfo=$courseDes->course_description ." " . "<p>Virtual Session Information:</p>". $classinfoonitemid->virtual_session_information;
			}
			else
			{
				$virtualSessionInfo=$courseDes->course_description;
			}
		}
		$sendscedule='';
		if($class_id!=0)
		{
			$sendscedule =Mailnotification::getClassAllSessions($class_id, $company_id);
		}
		
		$custom='/linkurlsend/sendurlauth/Param/'.@$paramurl;
		
		$all_sessions_res=Sendmail::getScheduleStartDate($class_id ,$company_id);
		$all_sessions = json_decode(json_encode($all_sessions_res),true);
		$currentdate = date('Y-m-d',time());
		
		$total_session = count($all_sessions);
		$i=1;
		foreach($all_sessions as $classstartdate){
		
			$start_date = date('m/d/Y',strtotime($classstartdate['dayid']));
			$timezone_name = $classstartdate['timezone_name'];
			$timezone_regions = $classstartdate['timezone_regions'];
			$timezone_dst = $classstartdate['timezone_dst'];
			$utc_offset = $classstartdate['utc_offset'];
			$utc_offset_dst = $classstartdate['utc_offset_dst'];
			$dureation_hrs = $classstartdate['ses_length_hr'];
			$dureation_min = $classstartdate['ses_length_min'];
			$start_time_hr = $classstartdate['start_time_hr'];
			$start_time_min = $classstartdate['start_time_min'];
			$timeformat = $classstartdate['timeformat']; //AM PM
			$INstructorname = $classstartdate['user_name'];
			$location = ($classstartdate['location']!=0) ? $classstartdate['location'] : '';
			$schedule_id = $classstartdate['schedule_id'];
			 $notificationvariable=self::$notificationvariable;
			$notificationvariable["username"]=$username ;
			$notificationvariable["trainingclassname"]=stripslashes(stripslashes($trainingClassName)) ;
			$notificationvariable["trdate"]=$start_date;
			$notificationvariable["schedule_id"]=$schedule_id;
			$notificationvariable["sendscedule"]=$sendscedule ;
			$notificationvariable["customlink"]=$custom ;
			$notificationvariable["dureation_hrs"]=$dureation_hrs ;
			$notificationvariable["dureation_min"]=$dureation_min ;
			$notificationvariable["starthr"]=sprintf("%02s:", $start_time_hr);
			$notificationvariable["startmin"]=sprintf("%02s", $start_time_min);
			$notificationvariable["timeformat"]=$timeformat;
			$notificationvariable["timezone_name"]=$timezone_name;
			$notificationvariable["timezone_regions"]=$timezone_regions;
			$notificationvariable["timezone_dst"]=$timezone_dst;
			$notificationvariable["utc_offset"]=$utc_offset;
			$notificationvariable["utc_offset_dst"]=$utc_offset_dst;
			$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo ;
			$notificationvariable["description"]=$virtualSessionInfo ;
			$notificationvariable["INstructorname"]=$INstructorname ;
			$notificationvariable["location"]=$location ;
			$notificationvariable["companyname"]=$companyname;
			$notificationvariable["supervisorusername"]=$supervisorusername ;
			$notificationvariable["toemailid"]=$toemailid ;
			$notificationvariable["course_id"]=$course_id;
			$notificationvariable["class_id"]=$trainingclass_id;
			$notificationvariable["user_id"]=$supervisor_id;//$supervisor_id
			$notificationvariable["company_id"]=$company_id;
			$notificationvariable["start_time_hr"]=$start_time_hr;
			$notificationvariable["start_time_min"]=$start_time_min;
			$notificationvariable["total_session"]="$i/$total_session";
			
			$i++;
			$getNotificationContent=Sendmail::getIcalNotificationContent(3, $company_id, $notificationvariable);
		}
	
	}
	
	public static function employeenrolmentWaitinglist($usertrainingid,$paramurl)
	{ 

		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);
		$userid				=	$getAlldetails[0]->user_id;
		$course_id			=	$getAlldetails[0]->course_id;
		$company_id			=	$getAlldetails[0]->company_id;
		$last_modfied_by	=	$getAlldetails[0]->last_modfied_by;
		$trainingClassName	=	$getAlldetails[0]->class_name;
		$class_id			=	$getAlldetails[0]->class_id;		
	
		$classstartdate=Sendmail::getLateststartdate($class_id ,$company_id);	
		
		$useremail=$getAlldetails[0]->email_id;
		$username=$getAlldetails[0]->user_name;
		$supervisor_id=$getAlldetails[0]->supervisor_id;
		$companyname=Mailnotification::getFromCompanyName($company_id);	
		$companyurl=$getAlldetails[0]->app_url;	
		
		
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$company_id);
		$supervisorusername=(isset($getsupervisiorDetail[0]->user_name)) ? $getsupervisiorDetail[0]->user_name : '';		
		$courseDes=Sendmail::getCourseDes($course_id,$company_id);
		$classinfoonitemid = Sendmail::classinfoonitemid($class_id , $company_id);
		if($classinfoonitemid->delivery_type_id == 2)
		{
				$virtualSessionInfo=$courseDes->course_description ." " . "<p>Virtual Session Information:</p>". $classinfoonitemid->virtual_session_information;
		}
		else
		{
			    $virtualSessionInfo=$courseDes->course_description;
		}
	
		$programsschedule = Sendmail::allSessionsForClassMail($class_id,$company_id);
		$k=1;					
		$sendscedule='';
			
		foreach($programsschedule as $key => $value)
		{
			$endtimehr=$value->start_time_hr+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			if($value->timeformat=='PM' && $value->start_time_hr!=12)
			{
				$starthour=$value->start_time_hr+12;
			}
			elseif($value->timeformat=='AM' && $value->start_time_hr==12)
			{
				$starthour=$value->start_time_hr+12;
			}
			else
			{
				$starthour=$value->start_time_hr;
			}
			$starttimeval=$starthour.":".$value->start_time_min;
			$endtimehr=$starthour+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			$nextEndtime=date('h:i A',mktime($endtimehr, $endtimemin, 0, 0, 0, 0000));
			$starttimeclass=date('h:i ',strtotime($value->start_time_hr.":".$value->start_time_min));
			$starttimeclass=$starttimeclass." ".$value->timeformat;			
			@$timezone=$value->time_zone;
			if(@$timezone==0){$timezone="";}
			if(@$timezone==1){$timezone="EST";}
			if(@$timezone==2){$timezone="MST";}
			if(@$timezone==3){$timezone="PST";}
			if(@$timezone=='4'){$timezone="AKST";}
			if(@$timezone=='5'){$timezone="HST";}
			if(@$timezone=='6'){$timezone="CST";}
			if($timezone!=''){$timezone='('.$timezone.')';}
			if(strlen($value->start_time_hr)==1){$value->start_time_hr='0'.$value->start_time_hr;}
			if(strlen($value->start_time_min)==1){$value->start_time_min='0'.$value->start_time_min;}
			if($k==1)
			{
				$trdate=date('m/d/Y',strtotime($value->dayid));
				$starthr=@$value->start_time_hr;
				$startmin=$value->start_time_min;  
				$timeformat=$value->timeformat;  
				if($value->instructor_id!=0)
				{
					$instname=Sendmail::getinstructorname($value->instructor_id);	
					$INstructorname=$instname->user_name;
					$INstructorname=stripslashes($INstructorname);
				}
				else{
					$INstructorname='';
				} 
			}
			$sendscedule.="<br>Session $k: ".date('m/d/Y',strtotime($value->dayid)).' '.@$value->start_time_hr.':'.$value->start_time_min.' '.$value->timeformat.' - '.@$nextEndtime.' '.@$format;
			$k++; 
			$location = $value->location;
		}	
		$baseurl = URL::to('/');
		$urlclicklink='<a href="http://'.$companyurl.$baseurl.'/linkurlsend/sendurlauth/Param/'.@$paramurl.'"?height=400&width=625&inlineId=hiddenModalContent&modal=false" class="thickbox">Click here</a>';
		$custom='/linkurlsend/sendurlauth/Param/'.@$paramurl;
		$currentdate = date('Y-m-d',time());
		
		$toemailid = $useremail;
		$classstartdate = json_decode(json_encode($classstartdate),true);
		
		if($currentdate <= $classstartdate[0]['dayid']){
		$notificationvariable=self::$notificationvariable;
		$notificationvariable["username"]=$username;
		$notificationvariable["trainingclassname"]=$trainingClassName;
		$notificationvariable["trdate"]=@date('m/d/Y',strtotime($classstartdate[0]['dayid']));
		$notificationvariable["sendscedule"]=$sendscedule;
		$notificationvariable["urlclicklink"]=$urlclicklink;
		$notificationvariable["customlink"]=$custom;
		$notificationvariable["starthr"]=$starthr;
		$notificationvariable["startmin"]=$startmin;
		$notificationvariable["timeformat"]=$timeformat;
		$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
		$notificationvariable["INstructorname"]=$INstructorname;
		$notificationvariable["location"]=$location;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["supervisorusername"]=$supervisorusername;
		$notificationvariable["toemailid"]=$toemailid;

		$getNotificationContent=Sendmail::getNotificationContent(10,$company_id,$notificationvariable);
		}
	}
	
	public static function enrolmentDrop($usertrainingid)
	{
		
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);
		$userid=$getAlldetails[0]->user_id;
		$course_id=$getAlldetails[0]->course_id;
		$class_id=$getAlldetails[0]->class_id;
		$company_id=$getAlldetails[0]->company_id;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=stripslashes(stripslashes($getAlldetails[0]->class_name));
		
		$class_id=$getAlldetails[0]->class_id;		
		$username=$getAlldetails[0]->user_name;
		$useremail=$getAlldetails[0]->email_id;
		$supervisor_id=$getAlldetails[0]->supervisor_id;		
		$getsupervisiorDetail=Sendmail::getuseremail($userid,$company_id);
		$supervisorusername=$getsupervisiorDetail[0]->user_name;
		$useremailS=$getsupervisiorDetail[0]->email_id;
		$usernameS=$getsupervisiorDetail[0]->user_name;		
		$courseDes=Sendmail::getCourseDes($course_id,$company_id);
		$virtualSessionInfo=$courseDes->course_description;			
		$companyname=Mailnotification::getFromCompanyName($company_id);
	
		$toemailid = $useremail;
		 $notificationvariable=self::$notificationvariable;
		 $notificationvariable["username"]=$username ;
		 $notificationvariable["course_id"]=$course_id ;
		 $notificationvariable["class_id"]=$class_id ;
		 $notificationvariable["schedule_id"]='' ;
		 $notificationvariable["user_id"]=$userid ;
		 $notificationvariable["company_id"]=$company_id ;
		 $notificationvariable["trainingclassname"]=$trainingClassName ;
		 $notificationvariable["virtualSessionInfo"]=$virtualSessionInfo ;
		 $notificationvariable["description"]=$virtualSessionInfo ;
		 $notificationvariable["companyname"]=$companyname;
		 $notificationvariable["supervisorusername"]=$supervisorusername ;
	   	 $notificationvariable["toemailid"]=$toemailid ;
		$getNotificationContent=Sendmail::getNotificationContent(9, $company_id, $notificationvariable);
		
	}
	
	
	public static function courseRequestmail($itemArr,$userid,$compid){
	
		
		$userDetail=Sendmail::getuseremail($userid,$compid);
		$username=$userDetail[0]->user_name;
		$usermailid=$userDetail[0]->email_id;
		$supervisor_id=$userDetail[0]->supervisor_id;
		
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$compid);
		$supervisorusername=$getsupervisiorDetail[0]->user_name;
		
		$trainingCourseName=$itemArr[0]['item_name'];
		
		$companyname = Mailnotification::getFromCompanyName($compid);	
		
		$toemailid=$getsupervisiorDetail[0]->email_id;
			 $notificationvariable=self::$notificationvariable;
			$notificationvariable["username"]=$username;
			$notificationvariable["companyname"]=$companyname;	
			$notificationvariable["usermailid"]=$usermailid ;	
			$notificationvariable["supervisorusername"]=$supervisorusername ;
		
			$notificationvariable["toemailid"]=$toemailid;
			$notificationvariable["trainingclassname"]=$trainingCourseName ;
			
			$getNotificationContent=Sendmail::getNotificationContent(26,$compid,$notificationvariable);
	
	}
	
	public static function CourseEnrollmentApprovalSubmittraing($usertrainingid, $itemtype)
	{
		$getAlldetails=Sendmail::usertrainingcatelogforsubmittraining($usertrainingid);  
        $userid=$getAlldetails[0]->user_id;
		$item_id=$getAlldetails[0]->item_id;
		$program_id=$getAlldetails[0]->program_id;
		$company_id=$getAlldetails[0]->company_id;
		$class_id=$getAlldetails[0]->class_id;
		$trainingClassName=$getAlldetails[0]->class_name;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;		
		$sessionid=$getAlldetails[0]->session_id;
		$completiondate1=$getAlldetails[0]->completion_date;
		$completiondate = date('m/d/Y',strtotime($completiondate1));
		
		$useremailget=Sendmail::getuseremail($userid,$company_id);
		$emailid=$useremailget[0]->email_id;
		$username=$useremailget[0]->user_name;
		$supervisor_id=$useremailget[0]->supervisor_id;		
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$company_id);
		$supervisorusername=$getsupervisiorDetail[0]->user_name;	
		$courseDes=Sendmail::getCourseDes($item_id,$company_id);
		$virtualSessionInfo=($courseDes)?$courseDes->description:"";			
		$companyname=CommonFunctions::getFromCompanyName($company_id);
        $programsschedule = Sendmail::allSessionsForClassMail($sessionid,$company_id);		
		$k=1;					
		$sendscedule='';
		foreach($programsschedule as $key => $value)
		{
			$endtimehr=$value->start_time_hr+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			if($value->timeformat=='PM' && $value->start_time_hr!=12) {
				$starthour=$value->start_time_hr+12;
			}
			elseif($value->timeformat=='AM' && $value->start_time_hr==12) {
				$starthour=$value->start_time_hr+12;
			}
			else {
				$starthour=$value->start_time_hr;
			}
			$starttimeval=$starthour.":".$value->start_time_min;
			$endtimehr=$starthour+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			$nextEndtime=date('h:i A',mktime($endtimehr, $endtimemin, 0, 0, 0, 0000));
			$starttimeclass=date('h:i ',strtotime($value->start_time_hr.":".$value->start_time_min));
			$starttimeclass=$starttimeclass." ".$value->timeformat;
			@$timezone=$value->time_zone;
			if(@$timezone==0){$timezone="";}
			if(@$timezone==1){$timezone="EST";}
			if(@$timezone==2){$timezone="MST";}
			if(@$timezone==3){$timezone="PST";}
			if(@$timezone=='4'){$timezone="AKST";}
			if(@$timezone=='5'){$timezone="HST";}
			if(@$timezone=='6'){$timezone="CST";}
			if($timezone!=''){$timezone='('.$timezone.')';}
			if(strlen($value->start_time_hr)==1){$value->start_time_hr='0'.$value->start_time_hr;}
			if(strlen($value->start_time_min)==1){$value->start_time_min='0'.$value->start_time_min;}
			if($k==1)
			{
				$trdate=date('m/d/Y',strtotime($value->dayid));
				$starthr=@$value->start_time_hr;
				$startmin=$value->start_time_min;  
				$timeformat=$value->timeformat;  
				if($value->instructor_id!=0)
				{
					$instname=Sendmail::getinstructorname($value->instructor_id);	
					$INstructorname=$instname[0]->instructor_name;
					$INstructorname=stripslashes($INstructorname);
				}
				else
				{
					$INstructorname='';
				} 
			}
			$sendscedule.="<br>Session $k: ".date('m/d/Y',strtotime($value->dayid)).' '.@$value->start_time_hr.':'.$value->start_time_min.' '.$value->timeformat.' - '.@$nextEndtime.' '.@$format;
			$k++; 
			$location = $value->location;
		}
		$trainingClassName=stripslashes($trainingClassName);	
		$getcredit=Sendmail::getcreditval($usertrainingid);
		$credit_value=$getcredit[0]->credit_value;	

		$toemailid = $emailid;
		$notificationvariable=self::$notificationvariable;
		$notificationvariable["username"]=$username;
		$notificationvariable["trainingclassname"]=$trainingClassName;
		$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["supervisorusername"]=$supervisorusername;
	   	$notificationvariable["toemailid"]=$toemailid;
		$notificationvariable["date_of_completion"]=$completiondate;		
		$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
		$getNotificationContent=Sendmail::getNotificationContent(24,$company_id,$notificationvariable);
	}
	
	public static function CourseEnrollmentApproval($usertrainingid)
	{
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);
		$userid=$getAlldetails[0]->user_id;
		$item_id=$getAlldetails[0]->item_id;
		$program_id=$getAlldetails[0]->program_id;
		$company_id=$getAlldetails[0]->company_id;
		$class_schedule_id=$getAlldetails[0]->class_schedule_id;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=$getAlldetails[0]->class_name;
		$trainingclass_id=$getAlldetails[0]->class_id;
		$useremailget=Sendmail::getuseremail($userid,$company_id);
		$emailid=$useremailget[0]->email_id;
		$username=$useremailget[0]->user_name;
		$supervisor_id=$useremailget[0]->supervisor_id;
		$sessionid=$getAlldetails[0]->session_id;
		$getsupervisiorDetail=Sendmail::getsuperemail($supervisor_id,$userid,$company_id);
		$supervisorusername=$getsupervisiorDetail->user_name;
		$courseDes=Sendmail::getCourseDes($item_id,$company_id);
		$virtualSessionInfo=$courseDes->description;
		$companyurl=$getAlldetails[0]->app_url;	
		$companyname=CommonFunctions::getFromCompanyName($company_id);
        $trainingClassName=stripslashes($trainingClassName);		
		$toemailid =  $emailid;		
		$classstartdate=Sessionschedules::getLateststartdate($sessionid ,$company_id);
		$classstartdate = json_decode(json_encode($classstartdate),true);
		$virtualSessionInfo='';
		
		if(isset($item_id) && isset($company_id)) {
			$courseDes=Sendmail::getCourseDes($item_id,$company_id);
			$classinfoonitemid = Sendmail::classinfoonitemid($trainingclass_id , $company_id);
			if($classinfoonitemid->delivery_type_id == 2) {
				$virtualSessionInfo=$courseDes->description ." " . "<p>Virtual Session Information:</p>". $classinfoonitemid->virtual_session_information;
			}else {
				$virtualSessionInfo=$courseDes->description;
			}
		}
		$sendscedule='';
		if($sessionid!=0)
		{
			$sendscedule = Mailnotification::getClassAllSessions($sessionid, $company_id);
		}
		
		$custom='/linkurlsend/sendurlauth/Param/'.@$paramurl;
		$all_sessions_res=Sessionschedules::getScheduleStartDate($sessionid ,$company_id);
		$all_sessions = json_decode(json_encode($all_sessions_res),true);
		$currentdate = date('Y-m-d',time());
		
		$total_session = count($all_sessions);
		$i=1;
		foreach($all_sessions as $classstartdate){
			$start_date = date('m/d/Y',strtotime($classstartdate['dayid']));
			$timezone_name = $classstartdate['timezone_name'];
			$timezone_regions = $classstartdate['timezone_regions'];
			$timezone_dst = $classstartdate['timezone_dst'];
			$utc_offset = $classstartdate['utc_offset'];
			$utc_offset_dst = $classstartdate['utc_offset_dst'];
			$dureation_hrs = $classstartdate['ses_length_hr'];
			$dureation_min = $classstartdate['ses_length_min'];
			$start_time_hr = $classstartdate['start_time_hr'];
			$start_time_min = $classstartdate['start_time_min'];
			$timeformat = $classstartdate['timeformat']; //AM PM
			$INstructorname = $classstartdate['user_name'];
			$location = ($classstartdate['location']!=0) ? $classstartdate['location'] : '';
			$schedule_id = $classstartdate['schedule_id'];
			
			$notificationvariable=self::$notificationvariable;
			
			$notificationvariable["username"]=$username;
			$notificationvariable["trainingclassname"]=stripslashes(stripslashes($trainingClassName));
			$notificationvariable["trdate"]=$start_date;
			$notificationvariable["schedule_id"]=$schedule_id;
			$notificationvariable["sendscedule"]=$sendscedule;
			$notificationvariable["customlink"]=$custom;
			$notificationvariable["dureation_hrs"]=$dureation_hrs;
			$notificationvariable["dureation_min"]=$dureation_min;
			$notificationvariable["starthr"]=sprintf("%02s:", $start_time_hr);
			$notificationvariable["startmin"]=sprintf("%02s", $start_time_min);
			$notificationvariable["timeformat"]=$timeformat;
			$notificationvariable["timezone_name"]=$timezone_name;
			$notificationvariable["timezone_regions"]=$timezone_regions;
			$notificationvariable["timezone_dst"]=$timezone_dst;
			$notificationvariable["utc_offset"]=$utc_offset;
			$notificationvariable["utc_offset_dst"]=$utc_offset_dst;
			$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
			$notificationvariable["INstructorname"]=$INstructorname;
			$notificationvariable["location"]=$location;
			$notificationvariable["companyname"]=$companyname;
			$notificationvariable["supervisorusername"]=$supervisorusername;
			$notificationvariable["toemailid"]=$toemailid;
			$notificationvariable["item_id"]=$item_id;
			$notificationvariable["class_id"]=$trainingclass_id;
			$notificationvariable["user_id"]=$userid;
			$notificationvariable["company_id"]=$company_id;
			$notificationvariable["start_time_hr"]=$start_time_hr;
			$notificationvariable["start_time_min"]=$start_time_min;
			$notificationvariable["total_session"]="$i/$total_session";
			$i++;
			$getNotificationContent=Sendmail::getIcalNotificationContent(22, $company_id, $notificationvariable);
			
		}
		
	}
	
	public static function CourseEnrollmentDenySubmittraing($usertrainingid)
	{  
	    $getAlldetails=Sendmail::usertrainingcatelogforsubmittraining($usertrainingid);
		$userid=$getAlldetails[0]->user_id;
		$item_id=$getAlldetails[0]->item_id;
		$program_id=$getAlldetails[0]->program_id;
		$company_id=$getAlldetails[0]->company_id;
		$class_id=$getAlldetails[0]->class_id;
		$last_modfied_by=$getAlldetails[0]->last_modfied_by;
		$trainingClassName=$getAlldetails[0]->class_name;
		$sessionid=$getAlldetails[0]->session_id;	
         $completiondate1=$getAlldetails[0]->completion_date;
		 $completiondate = date('m/d/Y',strtotime($completiondate1));	

		$useremailget=Sendmail::getuseremail($userid,$company_id);
		$emailid=$useremailget[0]->email_id;
		$username=$useremailget[0]->user_name;
		
		$supervisor_id=$useremailget[0]->supervisor_id;	
		$getsupervisiorDetail=Sendmail::getuseremail($supervisor_id,$company_id);
		//print_r($getsupervisiorDetail);exit;
		$supervisorusername=$getsupervisiorDetail[0]->user_name;		
		$courseDes=Sendmail::getCourseDes($item_id,$company_id);
		$virtualSessionInfo=$courseDes->description;			
		$companyname=CommonFunctions::getFromCompanyName($company_id);	
		$programsschedule = Sendmail::allSessionsForClassMail($sessionid,$company_id);
		$k=1;					
		$sendscedule='';
		foreach($programsschedule as $key => $value)
		{
			$endtimehr=$value->start_time_hr+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			if($value->timeformat=='PM' && $value->start_time_hr!=12) {
				$starthour=$value->start_time_hr+12;
			}
			elseif($value->timeformat=='AM' && $value->start_time_hr==12) {
				$starthour=$value->start_time_hr+12;
			}
			else {
				$starthour=$value->start_time_hr;
			}
			$starttimeval=$starthour.":".$value->start_time_min;
			$endtimehr=$starthour+$value->ses_length_hr;
			$endtimemin=$value->start_time_min+$value->ses_length_min;
			$nextEndtime=date('h:i A',mktime($endtimehr, $endtimemin, 0, 0, 0, 0000));
			$starttimeclass=date('h:i ',strtotime($value->start_time_hr.":".$value->start_time_min));
			$starttimeclass=$starttimeclass." ".$value->timeformat;
			@$timezone=$value->time_zone;
			if(@$timezone==0){$timezone="";}
			if(@$timezone==1){$timezone="EST";}
			if(@$timezone==2){$timezone="MST";}
			if(@$timezone==3){$timezone="PST";}
			if(@$timezone=='4'){$timezone="AKST";}
			if(@$timezone=='5'){$timezone="HST";}
			if(@$timezone=='6'){$timezone="CST";}
			if($timezone!=''){$timezone='('.$timezone.')';}
			if(strlen($value->start_time_hr)==1){$value->start_time_hr='0'.$value->start_time_hr;}
			if(strlen($value->start_time_min)==1){$value->start_time_min='0'.$value->start_time_min;}
			if($k==1)
			{
				$trdate=date('m/d/Y',strtotime($value->dayid));
				$starthr=@$value->start_time_hr;
				$startmin=$value->start_time_min;  
				$timeformat=$value->timeformat;  
				if($value->instructor_id!=0)
				{
					$instname=Sendmail::getinstructorname($value->instructor_id);	
					$INstructorname=$instname[0]->instructor_name;
					$INstructorname=stripslashes($INstructorname);
				}
				else
				{
					$INstructorname='';
				} 
			}
			
			$sendscedule.="<br>Session $k: ".date('m/d/Y',strtotime($value->dayid)).' '.@$value->start_time_hr.':'.$value->start_time_min.' '.$value->timeformat.' - '.@$nextEndtime.' '.@$format;
			$k++; 
			$location = $value->location;
			
		}
		
		$trainingClassName=stripslashes($trainingClassName);
		$getcredit=Sendmail::getcreditval($usertrainingid);
		$toemailid = $emailid;
		$notificationvariable=self::$notificationvariable;
		$notificationvariable["username"]=$username;
		$notificationvariable["trainingclassname"]=$trainingClassName;
		$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["supervisorusername"]=$supervisorusername;
	   	$notificationvariable["toemailid"]=$toemailid;
		$notificationvariable["date_of_completion"]=$completiondate;		
		$notificationvariable["virtualSessionInfo"]=$virtualSessionInfo;
		$getNotificationContent=Sendmail::getNotificationContent(25,$company_id,$notificationvariable);
		
	}
	
	public static function CourseEnrollmentDeny($usertrainingid)
	{
		$getAlldetails=Sendmail::usertrainingcatelog($usertrainingid);	
		$userid=($getAlldetails)?$getAlldetails[0]->user_id:'';
		$item_id=($getAlldetails)?$getAlldetails[0]->item_id:'';
		$program_id=($getAlldetails)?$getAlldetails[0]->program_id:'';
		$company_id=($getAlldetails)?$getAlldetails[0]->company_id:'';
		$class_id=($getAlldetails)?$getAlldetails[0]->class_id:'';
		$last_modfied_by=($getAlldetails)?$getAlldetails[0]->last_modfied_by:'';
		$trainingClassName=($getAlldetails)?$getAlldetails[0]->class_name:'';
		$class_id=($getAlldetails)?$getAlldetails[0]->class_id:'';	
		$useremailget=Sendmail::getuseremail($userid,$company_id);
		$emailid=$useremailget[0]->email_id;
		$username=$useremailget[0]->user_name;
		$supervisor_id=$useremailget[0]->supervisor_id;		
		$getsupervisiorDetail=Sendmail::getsuperemail($supervisor_id,$userid,$company_id);
		
		$supervisorusername=$getsupervisiorDetail->user_name;		
		$courseDes=Sendmail::getCourseDes($item_id,$company_id);
		$virtualSessionInfo=$courseDes->description;			
		$companyname=CommonFunctions::getFromCompanyName($company_id);	
		
		$trainingClassName=stripslashes($trainingClassName);
		$toemailid = $emailid;
		
	
		$ScheduleStartDate=Sendmail::getLatestScheduleStartDate($class_id,$company_id);
		
		$start_date = date('m/d/Y',strtotime($ScheduleStartDate[0]['dayid']));
		$notificationvariable=self::$notificationvariable;
		$notificationvariable["trdate"]=$start_date;
		$notificationvariable["username"]=$username;
		$notificationvariable["trainingclassname"]=$trainingClassName;
		$notificationvariable["trdate"]=$start_date;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["supervisorusername"]=$supervisorusername;
		$notificationvariable["toemailid"]=$toemailid;		
		$getNotificationContent=Sendmail::getNotificationContent(8,$company_id,$notificationvariable);
		
	}
	
	public static function welcomeemail($userid,$compid,$emailid,$username,$password,$notificationid="")
	{
	
		$userDetail =UserMaster::where('user_id','=',$userid)->where('company_id','=',$compid)->first();
		if(!isset($userDetail->login_id))
		{
			$loginname = $username;
		}
		else
		{
			$loginname = $userDetail->login_id;
		}
		$notificationid=27;
		$companyurl1=$_SERVER["HTTP_HOST"];
		$baseUrl = URL::to('/');
		$companyurl='<a href="https://'.$companyurl1.'/'.$baseUrl.'">https://'.$companyurl1.'</a>';
		
        if($password=="********"){
		  $password = CommonHelper::getDecode(str_replace($userDetail->salt_key,'',$userDetail->password), 'no');	
	    }
		
		$companyname = Mailnotification::getFromCompanyName($compid);
		$subject = 'Welcome Email';
		$fromname = $companyname;
		$emailname = 'Welcome mail Notification';
		$notificationvariable=self::$notificationvariable;
		$notificationvariable["username"]=$username;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["toemailid"]=$emailid;
		$notificationvariable["companyurl"]=$companyurl;
		$notificationvariable["login_id"]=$loginname;
		$notificationvariable["password"]=$password;
		$welcomeNotificationContent=Sendmail::getNotificationContent($notificationid,$compid,$notificationvariable);
	}
	
	public static function UserAprovalbyAdmin($userid,$compid)
	{
	
		$companyname = Mailnotification::getFromCompanyName($compid);
		$userDetail=UserMaster::where('user_id','=',$userid)->where('company_id','=',$compid)->first();
		$username=$userDetail->user_name;
		$toemailid=$userDetail->email_id;	
		$loginname = $userDetail->login_id;
		$password = CommonHelper::getDecode(str_replace($userDetail->salt_key,'',$userDetail->password), 'no');			
	    $notificationvariable=self::$notificationvariable;
		$notificationvariable["username"]=$username;
		$notificationvariable["login_id"]=$loginname;
		$notificationvariable["companyname"]=$companyname;
		$notificationvariable["password"]= $password;
		$notificationvariable["toemailid"]=$toemailid;				
		$getNotificationContent=Sendmail::getNotificationContent(20,$compid,$notificationvariable);
	}
	
}