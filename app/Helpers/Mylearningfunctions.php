<?php
namespace App\Helpers;

use App\Models\AssignmentMaster;
use App\Models\ItemMaster;
use App\Models\BlendedProgramItem;
use App\Models\TrainingType;
use App\Models\CompanySettings;
use App\Http\Traits\Assignment;
use App\Http\Traits\Trainingprograms;
use App\Http\Traits\Contentmanagement;
use App\Http\Traits\ClassSchedules;
use App\Http\Traits\Requirement;
use CommonHelper;
use App\Http\Traits\Trainingcatalog;
use App\Http\Traits\Usertrainingcatalogs;


class Mylearningfunctions 
{
	 use Assignment,Trainingprograms,ClassSchedules,Trainingcatalog,Requirement;
	 
	 public static function getAllAssignmentArr($userid, $compid)
	   {
		   
		   $assignments = Assignment::getAssignmenttouser($compid,$userid);
		   $assgnmntMainArr = array();
		   if(count($assignments)>0) {
			    foreach($assignments as $assignment)
				{
					$assgnmntArr = array();
				    $training_type_id = $assignment['training_type_id'];
					if($training_type_id == 1) {
						
						// assignment_start_date
						$item_id = $assignment['item_id'];
						$assgnmntArr['assignment_id'] 			= $assignment['assignment_id'];
						$assgnmntArr['item_id'] 				= $assignment['item_id'];
						$assgnmntArr['due_date'] 				= $assignment['due_date'];
						$assgnmntArr['assignment_start_date']   = $assignment['assignment_start_date'];
						$assgnmntArr['training_program_id'] 	= $assignment['training_program_id'];
						$assgnmntArr['training_type_id'] 		= $assignment['training_type_id'];
						$assgnmntArr['assigned_date'] 			= $assignment['created_date'];
						$assgnmntArr['scheduleid'] 				= '';
						$assgnmntArr['blended_item_id'] 		= '';
						$assgnmntArr['assignmentName'] 			= $assignment['training_title'];
						$assgnmntArr['training_type'] 			= $assignment['training_type'];	

                        $checkUserItemCompleted = Assignment::checkUserItemCompleted($userid, $item_id);

                        if(count($checkUserItemCompleted) > 0) {
							  $scoscore = Assignment::geteLearningScore($userid, $compid, $item_id);
							  $assgnmntArr['credit_value'] = $checkUserItemCompleted[0]->credit_value;
							  $passfailstatus = 'Complete';
							  $assgnmntArr['completion_date'] = $checkUserItemCompleted[0]->completion_date;
							  
							  if(count($scoscore)) {
									if($scoscore[0]->sco_score_status == '2') {
										$passfailstatus = 'Complete';
									}else {
										$passfailstatus = ($scoscore[0]->sco_score_status == 1) ? 'Pass' : 'Fail';
									}
								}else {
									$passfailstatus = 'Complete';
								}

							   if(strcmp($passfailstatus, 'Fail') != 0) {
									$assgnmntArr['imagname'] = "u184.png";
									$assgnmntArr['percentage'] = 100;
									$assgnmntArr['assignmentstatus'] = 'Completed';
								}
								else{
									if(strtotime($assignment['due_date']) >= time()){
										$assgnmntArr['imagname'] = "flag_red.png";
									}else{
										$assgnmntArr['imagname'] = "flag_orange.png";
									}
									$assgnmntArr['credit_value'] = $assignment['credit_value'];
									
									$percentage = Assignment::usertraininghistoryDB($userid,$item_id);
									$assgnmntArr['percentage'] = (count($percentage)==0) ? '0' : $percentage[0]->course_completion_percentage;
									$assgnmntArr['assignmentstatus'] = (count($percentage)==0) ? 'Incomplete' : 'In-Progress';
								}
					     }else{
							if(strtotime($assignment['due_date']) >= time()){
								$assgnmntArr['imagname'] = "flag_red.png";
							}else{
								$assgnmntArr['imagname'] = "flag_orange.png";
							}
							$assgnmntArr['credit_value'] = $assignment['credit_value'];
							$where = "user_id='$userid' AND item_id='$item_id' AND is_active=1";
							$percentage = Assignment::usertraininghistoryDB($userid,$item_id);
							$assgnmntArr['percentage'] = (count($percentage)==0) ? '0' : $percentage[0]->course_completion_percentage;
							$assgnmntArr['assignmentstatus'] = (count($percentage)==0) ? 'Incomplete' : 'In-Progress';
							$assgnmntArr['completion_date'] =          '0000-00-00';
						}
                        	$assgnmntMainArr[] = $assgnmntArr;					
					
						
					}
					
					
					if($training_type_id == 2 || $training_type_id == 3) {
					            $item_id = $assignment['item_id'];
								$classid = $assignment['class_id'];
								$assgnmntArr['assignment_id'] 			= $assignment['assignment_id'];
								$assgnmntArr['item_id'] 				= $assignment['item_id'];
								$assgnmntArr['due_date'] 				= $assignment['due_date'];
								$assgnmntArr['assignment_start_date']   = $assignment['assignment_start_date'];
								$assgnmntArr['training_program_id'] 	= $assignment['training_program_id'];
								$assgnmntArr['training_type_id'] 		= $assignment['training_type_id'];
								$assgnmntArr['assigned_date'] 			= $assignment['created_date'];
								$assgnmntArr['class_id'] 				= $assignment['class_id'];
								
								if($classid!=0){
									$fetchdates=Trainingprograms::fetchdatesforclass($classid,$item_id);
									$recursivedate=$fetchdates[0]->startdate;
									$schedule=ClassSchedules::getscheduleid($classid, $recursivedate, $compid);
									if(count($schedule)>0) {
										$assgnmntArr['scheduleid'] = $classid[0]->class_schedule_id;
										$assgnmntArr['assignmentName'] = $schedule[0]->class_name;
									}else{
										$scheduleName=ClassSchedules::getClsscheduleid($classid, $compid);
										$assgnmntArr['scheduleid'] = (isset($scheduleName[0]->class_schedule_id)) ? $scheduleName[0]->class_schedule_id: ''; 
										$assgnmntArr['assignmentName'] = (isset($scheduleName[0]->class_name)) ? $scheduleName[0]->class_name: ''; 
									}
								}
								$assgnmntArr['blended_item_id'] 		= '';
								$assgnmntArr['training_type'] 			= $assignment['training_type'];
								$checkUserItemCompleted = Assignment::checkUserItemCompleted($userid, $item_id);
								if(count($checkUserItemCompleted) > 0)
								{
									$assgnmntArr['imagname'] 				= "u184.png";
									$assgnmntArr['credit_value'] 			= $checkUserItemCompleted[0]->credit_value;
									$assgnmntArr['percentage'] 				= 100;
									$assgnmntArr['assignmentstatus'] 		= 'Completed';
									$assgnmntArr['completion_date']         = $checkUserItemCompleted[0]->completion_date;		
								}
								else
								{
									if(strtotime($assignment['due_date']) >= time()){
										$assgnmntArr['imagname'] = "flag_red.png";
									}else{
										$assgnmntArr['imagname'] = "flag_orange.png";
									}
									$assgnmntArr['credit_value'] 			= $assignment['credit_value'];
									$assgnmntArr['percentage'] 				= 0;
									$assgnmntArr['assignmentstatus'] 		= 'Incomplete';
									$assgnmntArr['completion_date'] =          '0000-00-00';
								}
								if($assgnmntArr['assignmentName']!=''){
								$assgnmntMainArr[] = $assgnmntArr;
								}
								
								
				     }
					 
					 
					 if($training_type_id == 4) {
					         $blended_item_id = $assignment['blended_item_id'];
							 $getBlendedData = Assignment::getBlendedData($userid, $blended_item_id, $assignment['due_date']);
							 if(count($getBlendedData) > 0)
							 {						
								$assgnmntArr['assignment_id'] 			= $assignment['assignment_id'];
								$assgnmntArr['item_id'] 				= '';
								$assgnmntArr['due_date'] 				= $assignment['due_date'];
								$assgnmntArr['assignment_start_date']   = $assignment['assignment_start_date'];
								$assgnmntArr['training_program_id'] 	= $assignment['training_program_id'];
								$assgnmntArr['training_type_id'] 		= $assignment['training_type_id'];
								$assgnmntArr['assigned_date'] 			= $assignment['created_date'];
								$assgnmntArr['assignmentName'] 			= $assignment['training_title'];
								$assgnmntArr['scheduleid']				= '';
								$assgnmntArr['blended_item_id'] 		= $assignment['blended_item_id'];
								$assgnmntArr['training_type'] 			= $assignment['training_type'];										
								$assgnmntArr['percentage'] 				= $getBlendedData['percent'];
								$assgnmntArr['imagname'] 				= $getBlendedData['imagname'];
								$assgnmntArr['credit_value'] 			= $getBlendedData['credit_value'];
								$assgnmntArr['assignmentstatus'] 		= $getBlendedData['assignmentstatus'];
								$assgnmntArr['completion_date'] 		= '0000-00-00';
								$assgnmntMainArr[] = $assgnmntArr;
							}
					
				    }
				}
		   
		   }
		   return $assgnmntMainArr;
		   
	   }
	   
	   public static function getViewData($compId)
	   {
		   $viewDescription=Contentmanagement::fetchcmslearningplaninfo($compId);
		   return  $viewDescription;
	   }
	   
	   public static function getAllMyEnrollments($userid, $compid, $flag = 0) 
	   {
		   $Getdate_formatecomapny = CommonHelper::getdate($compid);
		   $myenrollmentsMainArr = array();
		   if($flag == 1) {
				$allMyenrollments = Trainingcatalog::getMyClassScheduleDetailsforinprogress($compid, $userid);
			}else {
				$allMyenrollments = Trainingcatalog::getMyClassScheduleDetails($compid, $userid);
			}
			if(count($allMyenrollments) > 0)
		   {
			   foreach($allMyenrollments as $myenrollments )
				{
					$myenrollmentsArr = array();
					$classid = $myenrollments->class_id;
					$itemid = $myenrollments->item_id;
					$myenrollmentsArr['user_training_id'] 	= 	$myenrollments->user_training_id;
					$myenrollmentsArr['item_id'] 			= 	$myenrollments->item_id;
					$myenrollmentsArr['itmname'] 			= 	$myenrollments->schedule_name;
					$myenrollmentsArr['start_date'] 		= 	date('m/d/Y',strtotime($myenrollments->recursive_date));
					$starttimehr=$myenrollments->start_time_hr;
					$starttimemin=$myenrollments->start_time_min;
					if(strlen($starttimehr)==1){$starttimehr='0'.$starttimehr;}
					if(strlen($starttimemin)==1){$starttimemin='0'.$starttimemin;}
					if($starttimehr!='' || $starttimehr!=0)
					{
						$myenrollmentsArr['start_time']		=	$starttimehr.':'.@$starttimemin.' '.$myenrollments->timeformat;
					}
					$myenrollmentsArr['creditvalue'] 		= 	$myenrollments->credit_value;
					$myenrollmentsArr['traininglocation'] 	= 	($myenrollments->location != '0') ? $myenrollments->location : '';
					$myenrollmentsArr['training_status_id'] = 	$myenrollments->training_status_id;
					$myenrollmentsArr['user_status'] 		= 	$myenrollments->user_status;
					$myenrollmentsArr['training_type_id'] 	= 	$myenrollments->training_type_id;
					$myenrollmentsArr['program_id'] 		= 	$myenrollments->training_program_id;
					$myenrollmentsArr['class_schedule_id'] 	= 	$myenrollments->class_schedule_id;
					$myenrollmentsArr['class_id'] 			= 	$myenrollments->class_id;
					$myenrollmentsArr['training_type'] 		= 	$myenrollments->training_type;
					$myenrollmentsArr['description'] 		= 	$myenrollments->description;
					$myenrollmentsArr['training_code'] 		= 	$myenrollments->training_code;
					$myenrollmentsArr['category_name'] 		= 	$myenrollments->category_name;
					$myenrollmentsArr['supervisor_approval']= 	$myenrollments->require_supervisor_aproval_status;
					$myenrollmentsMainArr[] = $myenrollmentsArr;
				}
			   
		   }
		   return $myenrollmentsMainArr;
	   }
	   
	   public static function checkForSubmitTraining($compid)
	   {
			$result=CompanySettings::where('company_id','=', $compid)->get();
			return $result;
			
	   }
	   
	   public static function prevent_from_sql_injection($str, $check_cond=null)
		{
			$sql_error = 'no error';
			
			if(preg_match('/\b(LOAD_FILE|DUMPFILE|AES_DECRYPT|AES_ENCRYPT)\b/i', strtoupper($str)))
			{
				$sql_error = 'no sqli operators';
			}
			elseif(preg_match('/(UNI\*\*ON|1 OR 1=1|1 AND 1=1|1 EXEC XP_)/', strtoupper($str)))
			{
				$sql_error = 'or';
			}
			elseif(preg_match('/(SELECT\s[\w\*\)\(\,\s]+\sFROM\s[\w]+)| (SELECT\s[\w\*\)\(\,\s]+\sFROM\s[\w]+)| (UPDATE\s[\w]+\sSET\s[\w\,\'\=]+)| (INSERT\sINTO\s[\d\w]+[\s\w\d\)\(\,]*\sVALUES\s\([\d\w\'\,\)]+)| (DELETE\sFROM\s[\d\w\'\=]+)/', strtoupper($str)))
			{ 
				$sql_error = 'SELECT';
			} 
			elseif(preg_match('/\b(SCRIPT)|(%3c)|(%3e)|(SELECT)|(UPDATE)|(INSERT)|(DELETE)|(GRANT)|(REVOKE)|(UNION)|(TRUNCATE)|(DROP)|(&amp;lt;)|(&amp;gt;)\b/', strtoupper($str)))
			{ 
				$sql_error = 'script';
			}


			if(isset($check_cond) && $check_cond=='yes')
			{
				if(preg_match('/(<[^>]+)(autofocus|alt|checked|disabled|URL|form|formaction|placeholder|src|img|input|onfocus)=.*/i', $str))
				{ $sql_error = 'error'; }// no quotes
				elseif(preg_match('/[\/\\\\]/', $str))
				{  }// no slashes
				elseif(preg_match('/\b(and|or|null|not|sleep)\b/i', $str))
				{ $sql_error = 'error'; }// no sqli boolean keywords
				elseif(preg_match('/waitfor delay/i', $str))
				{ $sql_error = 'error'; }// no sqli boolean keywords
			}
			return $sql_error;
		}


	   public static function getAllAssessments($compid, $userid){
		   $allAssessmentsArr = Requirement::getallassessmentsbyuser($compid,$userid);
		   $Getdate_formatecomapny = CommonHelper::getdate($compid);
		   if(count($allAssessmentsArr)){
			   foreach($allAssessmentsArr as $assessments){
				   $assessmentsArr = array();
				   $close_time 	= strtotime($assessments['close_date']);
				   $due_time 	= strtotime($assessments['due_date']);
				   $current_time= time();
				   $assessId 	= $assessments['id'];
				 
				if($assessments['hide_assessonrequ']==0){
					$tran = Requirement::getassmentData($compid,$assessId,$userid);
					if(count($tran)==''){
						$token='';
						$fetchtokenid=Requirement::fetchgeneratedurl($compid,$assessId,'systemuser',0);
						if(count($fetchtokenid)>0)
							$token=$fetchtokenid[0]->url_id;
						$encodeduserid=CommonHelper::encode($userid);
						$completionStatus = Requirement::getvalueofsubmiteddata($assessId, $userid, $compid);
						 $assessmentsArr['completion_date']		=  '0000-00-00';
						$assessmentsArr['completionStatus'] 	= (count($completionStatus)>0) ? 'Complete' : 'Incomplete';
						$assessmentsArr['encodeduserid'] 		= $encodeduserid;		
						$assessmentsArr['token'] 				= $token;	
						$assessmentsArr['due_date'] 			= date($Getdate_formatecomapny,strtotime($assessments['due_date']));
						$assessmentsArr['date_sent'] 			= date($Getdate_formatecomapny,strtotime($assessments['date_sent']));
						$assessmentsArr['close_date'] 			= $assessments['close_date'];
						$assessmentsArr['assessment_name'] 		= $assessments['assessment_name'];
						$assessmentsArr['credit_value'] 		= $assessments['credit_value'];
						$assessmentsArr['allow_assessment'] 	= $assessments['allow_assessment'];
						if($assessments['score_type_id']==4)
						{
							if($current_time < $close_time && $current_time < $due_time)
								$assessmentsArr['assesStatus'] = "Open";
							elseif($current_time < $close_time && $current_time > $due_time) 
								$assessmentsArr['assesStatus'] = "Past due";
							elseif($current_time > $close_time)
								$assessmentsArr['assesStatus'] = "Closed";	
						}elseif($assessments['score_type_id']==5){
							$fetchnooftimes = Requirement::getuserassessmentsummary($assessId, $userid, $compid);
							if(count($fetchnooftimes) > 0 and $fetchnooftimes[0]->is_delete == 0) 
							{
								$assessmentarray['nooftimes'] = $fetchnooftimes[0]->no_of_times;
								$totalquestion				  = $fetchnooftimes[0]->total_questions;
								$correctanswers				  = $fetchnooftimes[0]->correct_answers;
								if($totalquestion!=0) {
									$totalrightper			  = ($correctanswers*100)/$totalquestion;
								} else {
									$totalrightper = null;
								}
								$totalrightpercent = number_format($totalrightper,2);
								$pass_percentage = $assessments['pass_percentage']; 
								if($totalrightpercent >= $pass_percentage) {
									$assessmentsArr['assesStatus']='Pass';
								}
								elseif($totalrightpercent < $pass_percentage) {
									$assessmentsArr['assesStatus']='Fail';
								} 
							}
							else
							{
								$assessmentsArr['assesStatus']='Open';
							}
						}
						 $assessmentsMainArr[] = $assessmentsArr; 
					}
				  }else{
					    $token='';
						$fetchtokenid=Requirement::fetchgeneratedurl($compid,$assessId,'systemuser',0);
						
						if(count($fetchtokenid)>0)
						$token=$fetchtokenid[0]->url_id;
					    $encodeduserid=CommonHelper::encode($userid);	
						$completionStatus = Requirement::getvalueofsubmiteddata($assessId, $userid, $compid);
						
						$completion = Requirement::getcompletiondate($assessId, $userid, $compid); 
						$assessmentsArr['completion_date'] 		= isset($completion[0]->completion_date)?$completion[0]->completion_date:'0000-00-00'; 
					$assessmentsArr['completionStatus'] 	= (count($completionStatus)>0) ? 'Complete' : 'Incomplete';
					$assessmentsArr['encodeduserid'] 		= $encodeduserid;		
					$assessmentsArr['token'] 				= $token;	
					$assessmentsArr['due_date'] 			= date($Getdate_formatecomapny,strtotime($assessments['due_date']));
					$assessmentsArr['date_sent'] 			= date($Getdate_formatecomapny,strtotime($assessments['date_sent']));
					$assessmentsArr['close_date'] 			= $assessments['close_date'];
					$assessmentsArr['assessment_name'] 		= $assessments['assessment_name'];
					$assessmentsArr['credit_value'] 		= $assessments['credit_value'];
					$assessmentsArr['allow_assessment'] 	= $assessments['allow_assessment'];
					if($assessments['score_type_id']==4)
					{
						if($current_time < $close_time && $current_time < $due_time)
							$assessmentsArr['assesStatus'] 	=  "Open";
						elseif($current_time < $close_time && $current_time > $due_time) 
							$assessmentsArr['assesStatus'] 	= "Past due";
						elseif($current_time > $close_time)
							$assessmentsArr['assesStatus'] 	= "Closed";	
					}elseif($assessments['score_type_id']==5){
						
						$fetchnooftimes = Requirement::getuserassessmentsummary($assessId, $userid, $compid);
						if (count($fetchnooftimes) > 0 and $fetchnooftimes[0]->is_delete == 0) 
						{
							$assessmentarray['nooftimes'] 	= 	$fetchnooftimes[0]->no_of_times;
							$totalquestion					= 	$fetchnooftimes[0]->total_questions;
							$correctanswers					=	$fetchnooftimes[0]->correct_answers;
							if($totalquestion!=0) {
								$totalrightper				=	($correctanswers*100)/$totalquestion;
							}
							else {
								$totalrightper=null;
							}
							$totalrightpercent = number_format($totalrightper,2);
							$pass_percentage =$assessments['pass_percentage']; 
							if($totalrightpercent >= $pass_percentage) {
								$assessmentsArr['assesStatus']='Pass';
							}
							elseif($totalrightpercent < $pass_percentage) {
								$assessmentsArr['assesStatus']='Fail';
							} 
						}
						else
						{
							$assessmentsArr['assesStatus']='Open';
						}		
					}
						  $assessmentsMainArr[] = $assessmentsArr;
				  }
				  
				 
			   }
		   }
		   return $assessmentsMainArr;
	   }
	   
	   
	   
	    public static function getMyEnrollmentsInProgress($userid, $compid, $flag = 0){
			$myenrollmentsMainArr = array();
			$allMyenrollments = Usertrainingcatalogs::getMyClassScheduleforinprogress($compid, $userid);
			if(count($allMyenrollments) > 0){
			   foreach($allMyenrollments as $myenrollments ){
				   $myenrollmentsArr = array();
				   $classid = $myenrollments->class_id;
				   $itemid = $myenrollments->item_id;
				   $myenrollmentsArr['user_training_id'] 	= 	$myenrollments->user_training_id;
				   $myenrollmentsArr['item_id'] 			= 	$myenrollments->item_id;
				   $myenrollmentsArr['itmname'] 			= 	$myenrollments->schedule_name;
				   $myenrollmentsArr['creditvalue'] 		= 	$myenrollments->credit_value;
				   $myenrollmentsArr['training_status_id']  = 	$myenrollments->training_status_id;
				   $myenrollmentsArr['user_status'] 		= 	$myenrollments->user_status;
				   $myenrollmentsArr['training_type_id'] 	= 	$myenrollments->training_type_id;
				   $myenrollmentsArr['program_id'] 		    = 	$myenrollments->training_program_id;
				   $myenrollmentsArr['class_id'] 			= 	$myenrollments->class_id;
				   $myenrollmentsArr['training_type'] 		= 	$myenrollments->training_type;
				   $myenrollmentsArr['description'] 		= 	$myenrollments->description;
				   $myenrollmentsArr['training_code'] 		= 	$myenrollments->training_code;
				   $myenrollmentsArr['category_name'] 		= 	$myenrollments->category_name;
				   $myenrollmentsArr['supervisor_approval'] = 	$myenrollments->require_supervisor_aproval_status;
				   $myenrollmentsMainArr[] = $myenrollmentsArr;
			   }
			}
			return $myenrollmentsMainArr;
		}   
       
	   
	   public static function getAlleLearningInProgress($userid, $compid){
		   $eLearningInProgressMainArr = array();
		   $alleLearningInProgress = Usertrainingcatalogs::getElearningInProgressofUsers($userid,$compid);
		   foreach($alleLearningInProgress as $eLearningInProgress){
			   $eLearningInProgress = json_decode(json_encode($eLearningInProgress),true);
			   $itemid = $eLearningInProgress['item_id'];
			   $percentage = Assignment::usertraininghistoryDB($userid,$itemid);
			   if(count($percentage) != 0){ 
				$courseper = $percentage[0]->course_completion_percentage;
			   }else{
				 
				$courseper=0;
			   }
			   if($courseper >= 100){
				$courseper = 60;
			  }
			$eLearningInProgressArr['progress_percentage'] 	= 	$courseper;
			$eLearningInProgressArr['user_training_id'] 	= 	$eLearningInProgress['user_training_id'];
			$eLearningInProgressArr['item_name'] 			= 	$eLearningInProgress['item_name'];
			$eLearningInProgressArr['credit_value'] 		= 	$eLearningInProgress['credit_value'];
			$eLearningInProgressArr['created_date'] 		= 	$eLearningInProgress['created_date'];
			$eLearningInProgressArr['user_training_id'] 	= 	$eLearningInProgress['user_training_id'];
			$eLearningInProgressArr['training_program_id'] 	= 	$eLearningInProgress['training_program_id'];
			$eLearningInProgressArr['training_type'] 		= 	$eLearningInProgress['training_type'];
			$eLearningInProgressArr['training_code'] 		= 	$eLearningInProgress['training_code'];
			$eLearningInProgressArr['description'] 			= 	$eLearningInProgress['description'];
			$eLearningInProgressArr['category_name'] 		= 	$eLearningInProgress['category_name'];
			$eLearningInProgressArr['item_id'] 				= 	$eLearningInProgress['item_id'];
			$eLearningInProgressArr['training_type_id'] 	= 	$eLearningInProgress['training_type_id'];
			$eLearningInProgressArr['blended_program_id'] 	= 	$eLearningInProgress['blended_program_id'];
			$eLearningInProgressMainArr[] = $eLearningInProgressArr;
		   }
		 
		   return $eLearningInProgressMainArr;
	   }
   
	
}