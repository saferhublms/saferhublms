<?php
namespace App\Helpers;

use App\Models\ItemMaster;
use App\Models\Assessment;
use App\Models\ClassSchedule;
use App\Models\ScoScoreData;
use App\Models\ItemMasterTrainingcategory;
use App\Models\UserTranscript;
use App\Models\UserTrainingHistory;
use App\Models\TrainingCatalogTranscript;
use App\Models\UserRatingMaster;
use App\Models\UserMaster;
use App\Models\InstructorMaster;
use App\Models\UserSupervisiorApprove;
use App\Http\Traits\Item;
use App\Http\Traits\historicaldata;
use App\Http\Traits\Coursecredits;
use App\Http\Traits\Trainingcatalog;
use DB,Mail,CommonHelper;

class TrainingItemsFunctions 
{  
       use Item,historicaldata,Coursecredits,Trainingcatalog;

         public static function manuallyAddItemFunc($trainingitem)
	    {
			
			 $traningtypeid = $trainingitem['traningtype_id'];
			 if($traningtypeid == 2 || $traningtypeid == 3 || $traningtypeid == 1) {
			  $itemid = TrainingItemsFunctions::getTrainingItemId($trainingitem);
			  $trainingitem['assessment'] = 0;
			  $trainingitem['item_id'] = $itemid;
			}else{
				$assessmentid = TrainingItemsFunctions::getTrainingAssessmentid($trainingitem);
				$trainingitem['assessment'] = $assessmentid;
				$trainingitem['item_id'] = 0;
			}
			
			
			if($traningtypeid == 2 || $traningtypeid == 3)
			{
				$sessionid =TrainingItemsFunctions::addclassToClassMaster($trainingitem);
				$trainingitem['session_id'] = $sessionid;
				$classscheduleid = TrainingItemsFunctions::addClassSchedule($trainingitem);
				
			}else{
				$sessionid = '0';
				$trainingitem['session_id'] = $sessionid;
				$classscheduleid = '0';
			}
			
			$usersids = $trainingitem['usersids'];
			if($traningtypeid != 5){
				$trainingitem['classschedule_id'] =	$classscheduleid;
				$trainingprogramid = TrainingItemsFunctions::addTrainingPrograms($trainingitem);
				$trainingitem['trainingprogram_id']	= $trainingprogramid;
			}else{
				 $trainingitem['classschedule_id'] 		= 0;
				 $trainingitem['trainingprogram_id']	= 0;
			}
			
			foreach($usersids as $key => $value)
			{
				$trainingitem['users_id'] =	$value;
				$trainingitem['lastmodifyuser_id'] = $value;
				
				TrainingItemsFunctions::giveManualCreditToUser($trainingitem);
				
			}
			
			return 1;
		
		}
		
		
		
		public static function getTrainingItemId($trainingitem)
		{
			$title = addslashes(strtolower(trim($trainingitem['training_title'],' ')));
			$compid	= $trainingitem['company_id'];
			$existingitems=ItemMaster::where('item_master.item_name',$title)->where('item_master.company_id','=',$compid)->select('item_id')->distinct()->get();
			if(count($existingitems)>0){
				$itemid = $existingitems[0]['item_id'];
			}else{
				$itemid = TrainingItemsFunctions::addItems($trainingitem);
			}
		
			return $itemid;
			
			
		}
		
	/**
     * addItems() - Method to add item to item master.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 22 December, 2017
     */
	 
		public static function addItems($trainingitem)
		{
			
			$course_id = 0;
			if(isset($trainingitem['course_id']))
			{ $course_id = $trainingitem['course_id'];
        		}
			$compid = $trainingitem['company_id'];
			$data = array();
			$data['title']							= stripslashes($trainingitem['training_title']);
			$data['desc']							= stripslashes($trainingitem['training_desc']);
			$data['item_name']						= $trainingitem['training_title'];
			$data['file_id']						= isset($trainingitem['file_id']) ? $trainingitem['file_id'] : 0;
			$data['course_id']           			= $course_id;
			$data['last_updated_time']				= date('Y-m-d H:i:s');
			$data['credit_value']					= isset($trainingitem['training_credit']) ? $trainingitem['training_credit'] : 0;
			$data['traning_type_id']				= $trainingitem['traningtype_id'];
			$data['certificate_template_id']		= isset($trainingitem['certificate_template_id']) ? $trainingitem['certificate_template_id'] : 0;
			$data['is_active']						= 0;
			$data['require_supervisor_approval']	= $trainingitem['require_supervisor_approval'];
			$data['training_category_id']			= $trainingitem['traininig_category_id'] ;
			$data['cost']							= $trainingitem['training_cost'];
			$data['lastmodifyuser_id']				= $trainingitem['lastmodifyuser_id'];
			$data['completion_date']				= $trainingitem['completion_date'];
			$itemSave=Item::additems($data,$compid);
			return $itemSave;
		}
		
		public static function getTrainingAssessmentid($trainingitem)
		{
			
			$title = addslashes(strtolower(trim($trainingitem['training_title'],' ')));
		    $compid	= $trainingitem['company_id'];
			$existingitems =Assessment::where('assessment.assessment_name',$title)->where('assessment.company_id','=',$compid)->select('id')->distinct()->get();
			if(count($existingitems)>0)
				$id = $existingitems[0]['id'];
			else
				$id = TrainingItemsFunctions::addAssessment($trainingitem);
			return $id;
		}
		
		/**
		 * addAssessment() - Method to add Assessment to Assessment.
		 *
		 * Created By: Rishabh Anand
		 * Created Date: 21 , December 2017
		 */
	 
		public static function addAssessment($trainingitem)
		{ 
			$compid = $trainingitem['company_id'];
			$data = array();
			$data['attempt_taken']		=1;
			$data['assessment_name']	=$trainingitem['training_title'];
			$data['score_type_id']		=5;
			$data['pass_percentage']	=0;
			$data['assess_status_id']	=0;
			$data['due_date']			='0000-00-00';
			$data['allow_assessment']	=1;
			$data['no_of_days']			=0;
			$data['credit_value']		=isset($trainingitem['training_credit']) ? $trainingitem['training_credit'] : 0;
			$data['assessment_desc']	=stripslashes($trainingitem['training_desc']);
			$data['randomize_ques']		=0;
			$data['active_status']		=1;
			$data['is_delete']			=0;
			$data['created_date']		=date('Y-m-d');
			$data['training_code']		=0;
			$data['allow_transcript']	=1;
			$data['hide_assessonrequ']	=1;
			$data['company_id']			= $trainingitem['company_id'];
			
			$assessmentSave = Assessment::create($data);
			return $assessmentSave->id;
		}
		
		
		public static function addclassToClassMaster($trainingitem)
		{
			
			$datas=array();
			$datas['item_id']			= $trainingitem['item_id'];
			$datas['class_name']		= $trainingitem['training_title'];
			$datas['lastmodifyuser_id']	= $trainingitem['lastmodifyuser_id'];
			$datas['maxseat']			= count($trainingitem['usersids']);
			$datas['start_date_class']	= $trainingitem['completion_date'];
			$datas['average_rating']	= $trainingitem['average_rating'];
			$sessionid = historicaldata::addclass($datas, $trainingitem['company_id']);
			return $sessionid;
		}
		
		
		public static function addClassSchedule($trainingitem)
		{
			
			$classscheduleid = 0;
			$classscheduleid = Coursecredits::checkClassExistance($trainingitem['session_id'] , $trainingitem['company_id']);
			
			if($classscheduleid == 0)
			{
				$datas2 = array ();
				$datas2 ['schedule_name'] 		= 	$trainingitem['training_title'];
				$datas2 ['class_id'] 			= 	$trainingitem['session_id'];
				$datas2 ['recursive_date'] 		= 	date ( 'Y-m-d', strtotime ($trainingitem['completion_date']) );
				$datas2 ['max_seat'] 			= 	count($trainingitem['usersids']);
				$datas2 ['is_active'] 			= 	1;
				$datas2 ['no_of_enrolled_seat'] = 	1;
				$datas2 ['no_of_waiting_seat'] 	= 	0;
				$datas2 ['company_id'] 			= 	$trainingitem['company_id'];
				$datas2 ['is_deleted'] 			= 	0;
				$datas2 ['publish_date'] 		= 	date('Y-m-d h:i:s');
				
				$classscheduleSave = ClassSchedule::create($datas2);
				$classscheduleid = $classscheduleSave->class_schedule_id;
			}
			return $classscheduleid;
		}
		
		
		public static function addTrainingPrograms($trainingitem)
		{
			
			$data_TP=array();
			$data_TP['title']							=	stripslashes($trainingitem['training_title']);
			$data_TP['desc']							=	stripslashes($trainingitem['training_desc']);
			$data_TP['traning_type_id']					=	$trainingitem['traningtype_id'];
			$data_TP['require_supervisor_approval']		=	0;
			$data_TP['class_id'] 						= 	$trainingitem['session_id'];
			$trainingprogramid = historicaldata::addtrainingprograms($data_TP, $trainingitem['item_id'], $trainingitem['company_id']);
			return $trainingprogramid;
		}
		
		
		public static function giveManualCreditToUser($trainingitem)
	    {
			
		     $mflag = 0;
             $user_training_id = TrainingItemsFunctions::addDataToUserTrainingCatalog($trainingitem);
			 $trainingitem['user_training_id'] = $user_training_id;
		     $traningtypeid = $trainingitem['traningtype_id'];
			// echo '<pre>';print_r($trainingitem);exit;
			 $user_transcript_id = TrainingItemsFunctions::addDataToUserTranscript($trainingitem);
			
			 if($user_transcript_id > 0)
		     { 
				 $mflag = 1; $trainingitem['instructor_name']='';
				 if($user_training_id!=5) {
					if($traningtypeid == 2 || $traningtypeid == 3) {
                           if(isset($trainingitem['instructor_type'])) {
					if($trainingitem['instructor_type'] == 2) {
						$extUserId=TrainingItemsFunctions::addExternalInstructor($trainingitem);
						$trainingitem['instructor_name'] = $extUserId;
					}
				}
				  $trainingitem['instructor_name'] = $trainingitem['instructor_name'] != '' ? $trainingitem['instructor_name'] : '';		
				  TrainingItemsFunctions::addUserRatingMaster($trainingitem);
			}	
				else{	
				$trainingitem['transcript_id']=$user_transcript_id;
				TrainingItemsFunctions::addSCOdata($trainingitem);
			}					
		}
		
				 TrainingItemsFunctions::insertValueIntoTranscriptHistory($trainingitem);
				 TrainingItemsFunctions::insertValueIntoTrainingCatalogTranscript($trainingitem);
				 if(@$trainingitem['submitTraining_type'] == 'adminside') {
				        Mailnotification::emailforManualAddedItem($trainingitem);//incomplete
			     }elseif(@$trainingitem['submitTraining_type'] == 'userside') {
						TrainingItemsFunctions::emailtoSupervisiorforApproval($trainingitem);
				 }
			 }
			 return $mflag;
			 
	    }
		
		public static function addDataToUserTrainingCatalog($trainingitem) 
		{
			$usersid					=	$trainingitem['users_id'];
			$title						=	$trainingitem['training_title'];
			$traningtypeid				=	$trainingitem['traningtype_id'];
			$trainingprogramid			=	$trainingitem['trainingprogram_id'];
			$itemid						=	$trainingitem['item_id'];
			$assessment					=	$trainingitem['assessment'];
			$sessionid					=	$trainingitem['session_id'];
			$trainingdate				=	$trainingitem['completion_date'];
			$compid						=	$trainingitem['company_id'];
		    $lastmodifyuserid			=	$trainingitem['lastmodifyuser_id'];
			
			
			$mannuallyAddItems=array();
			@$mannuallyAddItems['training_title'] 		= @$title;
			@$mannuallyAddItems['training_type_id'] 	= @$traningtypeid;
			@$mannuallyAddItems['training_program_id'] 	= @$trainingprogramid;
			@$mannuallyAddItems['lastmodifyuser_id'] 	= @$lastmodifyuserid;
			@$mannuallyAddItems['completion_date'] 		= @$trainingitem['completion_date'];
			@$mannuallyAddItems['average_rating'] 		= @$trainingitem['average_rating'];
			$userprogramid = 4;
					$AllCount = 0 ;
					
					$AllCount = historicaldata::ComparefortrainingCatalog($trainingitem);
					 if(count($AllCount) == 0)
                     {
						 if($traningtypeid!=5){
					      if($sessionid != 0)
					       {
							    @$mannuallyAddItems['item_id'] 				= @$itemid;
								@$mannuallyAddItems['session_id']		 	= @$sessionid;
								@$mannuallyAddItems['class_schedule']       = $trainingitem['classschedule_id'];
								@$mannuallyAddItems['start_date'] 			= date('Y-m-d H:i:s',strtotime($trainingdate));
								@$mannuallyAddItems['start_time'] 			= '';
								@$mannuallyAddItems['end_time']				= '';
								
								$user_training_id = historicaldata::creditTrainingprogramByClassroom($usersid,@$mannuallyAddItems,$userprogramid,$compid);
								
						
						   }else{
							$user_training_id =	 historicaldata::creditTrainingprogram($mannuallyAddItems,$compid,$usersid,$itemid,$lastmodifyuserid);
						}
						}else{
						  $user_training_id =	historicaldata::addassessmentinutc($mannuallyAddItems,$compid,$usersid,$assessment,$lastmodifyuserid);
						}
					 }else {
							$user_training_id = $AllCount[0]->user_training_id ;
						}
						return $user_training_id;
		}
		
		
		public static function addDataToUserTranscript($trainingitem)
		{ 
			$datass = array();
			$traningtypeid = $trainingitem['traningtype_id'];
			if($traningtypeid!=5){
			 $datass['item_id'] 				= 	$trainingitem['item_id'];	
			}else{
			 $datass['registration_id']		=	$trainingitem['assessment'];
			}
			$datass['user_id'] 				= 	$trainingitem['users_id'];
			$datass['credit_value'] 		= 	($trainingitem['training_credit']) ? $trainingitem['training_credit'] : 0;
			
			$datass['user_training_id'] 	= 	$trainingitem['user_training_id'];
			$datass['completion_date'] 		= 	date('Y-m-d',strtotime($trainingitem['completion_date']));
			$datass['is_active'] 			= 	1;
			$datass['is_approved'] 			= 	0;
			$datass['company_id'] 			= 	$trainingitem['company_id'];
			$datass['class_id'] 			= 	$trainingitem['session_id'];	
			$datass['is_expired'] 			= 	'0';
			$datass['last_modified_by'] 	= 	$trainingitem ['lastmodifyuser_id'];	
			if($trainingitem['session_id'] == 0)
			{
				$datass['class_schedule_id']=	0;
				$datass['times']			=	0;
			}
			else
			{
				$datass['class_schedule_id'] 	= 	$trainingitem['classschedule_id'];
			}
			
			$submitTraining_type			=	@$trainingitem['submitTraining_type'];
			if($traningtypeid!=5){
				$cat_data['item_id']=$trainingitem['item_id'];
				$cat_data['training_category_id']=$trainingitem['traininig_category_id'];
				$cat_data['company_id']=$trainingitem['company_id'];
				$ItenCatCount = 0;
				$ItenCatCount = historicaldata::checkforexistingentery($cat_data);    
				$ItenCatCount=count($ItenCatCount);
				if($ItenCatCount == 0)
				{
					if($cat_data['training_category_id'] != 0)
					{
						ItemMasterTrainingcategory::create($cat_data);
						
					}
				}
				}
				
				$user_transcript_id = 0;
				if($submitTraining_type != 'userside')
				{                 
					$Count = historicaldata::getcomparedata($datass);                      
					if(count($Count) == 0)    
					$usertranscriptSave = UserTranscript::create($datass);
				
					return @$usertranscriptSave->user_transcript_id;
				}else {
					$require_supervisor_approval	=	$trainingitem['require_supervisor_approval'];
					
			        $auto_approve	=	$trainingitem['auto_approve'];
					if($require_supervisor_approval==0 ) {    
						$usertranscriptSave = UserTranscript::create($datass);
						return $usertranscriptSave->user_transcript_id;
					}elseif($require_supervisor_approval==1 && $auto_approve==1 ) {
						$usertranscriptSave = UserTranscript::create($datass);
						return $usertranscriptSave->user_transcript_id;
					}else {
						$usertrainingid = $trainingitem['user_training_id'];
				        $ssupervisor_id = $trainingitem['ssupervisor_id'];
						$checksuerapprove = Trainingcatalog::checksupervisiorapprove($usertrainingid);
						
						if(count($checksuerapprove)==0)
						   {
							$dataucSuper=array();
							$dataucSuper['user_id']=$trainingitem['users_id'];
							$dataucSuper['training_catalog_id']=$usertrainingid;
							$dataucSuper['supervisior_id']=$ssupervisor_id;
							$dataucSuper['company_id']=$trainingitem['company_id'];
							$dataucSuper['is_approve']=0;
							$UsertrainingcatalogDB=UserSupervisiorApprove::create($dataucSuper);
							$datass['is_active'] = 0;
							$usertranscriptSave = UserTranscript::create($datass);
							$usertranscriptSave->user_transcript_id;
						  }else{
							  $usertranscriptData=UserTranscript::whereuserTrainingId($trainingitem['user_training_id'])->whereuserId($trainingitem['users_id'])->wherecompanyId($trainingitem['company_id'])->whereclassId($trainingitem['session_id'])->first();
							 
							  return  $usertranscriptData->transcript_id;
						  }
					}
					
					
				}
				return $user_transcript_id;
			
			
		}
		
		public static function addExternalInstructor($trainingitem)
		{
			  
			$instructor_name = $trainingitem['instructor_name'];
			$userMasterArr = array();
			$userMasterArr['first_name']	=	$instructor_name;
			$userMasterArr['last_name']	=	$instructor_name;
			$userMasterArr['user_name']	=	$instructor_name;
			$userMasterArr['role_id']		=	3;
			$userMasterArr['company_id']	=	$trainingitem['company_id'];
			$userMasterArr['is_active']	=	1;
			$userMasterArr['is_delete']	=	0;
			$userMasterArr['is_approved']	=	1;
			$userMasterArr['country']		=	229;
			$userMasterArr['created_date']	=	date('Y-m-d H:i:s');
			$userMasterArr['last_updated_by']	=	$trainingitem['lastmodifyuser_id'];
			
			$userMasterSave = UserMaster::create($userMasterArr);
			$userId=$userMasterSave->user_id;
			
			$instructorMaster = array();
			$instructorMaster['instructor_name'] 	= 	$instructor_name;
			$instructorMaster['user_id'] 		 	= 	$userId;
			$instructorMaster['is_internal'] 		= 	1;
			$instructorMaster['company_id'] 		= 	$trainingitem['company_id'];
			$instructorMaster['is_active'] 			= 	1;
			$instructorMaster['last_updated_by']	=	$trainingitem['lastmodifyuser_id'];
			$instructorMaster['created_date']		=	date('Y-m-d H:i:s');		
			$instructorMastersave=InstructorMaster::create($instructorMaster);
			return $userId;
		}
		
		
		public static function addUserRatingMaster($trainingitem)
		{
			
			$data = array(
				'user_training_id'	=> $trainingitem['user_training_id'],
				'user_id'			=> $trainingitem['users_id'],
				'item_id'			=> $trainingitem['item_id'],
				'class_id'			=> ($trainingitem['session_id']) ? $trainingitem['session_id'] : 0,
				'item_type'			=> 2,
				'instructor_id'		=> ($trainingitem['instructor_name']) ? $trainingitem['instructor_name'] : 0,
				'random_number'		=> round(microtime(true) * 1000),
				'class_rate'		=> ($trainingitem['average_rating']) ? $trainingitem['average_rating'] : 0,	
				'instructor_rate'	=> ($trainingitem['ins_average_rating']) ? $trainingitem['ins_average_rating'] : 0,
				'company_id'		=> $trainingitem['company_id'],
				'comment'			=> '',
				'is_active'			=> 1, 
				'last_updated_date'	=> date('Y-m-d H:i:s')
			);
			$userRatingMasterSave= UserRatingMaster::create($data); 
			$trainingprogramid = $userRatingMasterSave->id;
		}
		
		public static function addSCOdata($trainingitem) 
		{
			if($trainingitem['submitTraining_type']=='userside')
			{
				$status =2;
			}else{
				$status =(strtolower($trainingitem['training_status'])=="fail")? 0 : 1;
			}
			
			if(!empty($trainingitem['training_score'])){
			@list($correct_answer , $total_questions)=explode("/",$trainingitem['training_score']);
			}
			
			if(isset($trainingitem['transcript_id']) && $trainingitem['transcript_id']!='' && $trainingitem['transcript_id']!=NULL){
				
				$check_sco=ScoScoreData::wheretranscriptId($trainingitem['transcript_id'])->first();
				if($check_sco!=false){$trainingitem['transcript_id']='';}
			}
			
			$scored=array();
			$scored['user_id']			=	$trainingitem['users_id'];
			$scored['max_score']		=	100;
			$scored['total_questions']	=	isset($total_questions) ? $total_questions : 0;
			$scored['correct_answer']	=	isset($correct_answer) ? $correct_answer : 0;
			$scored['item_id']			=	$trainingitem['item_id'];
			$scored['user_training_id']	=	$trainingitem['user_training_id'];
			$scored['transcript_id']	=	isset($trainingitem['transcript_id']) ? $trainingitem['transcript_id'] : '';
			$scored['status']			=	$status;
			$scored['company_id']			=	$trainingitem['company_id'];
			
			
			$result=ScoScoreData::join('item_master',function($join) use($trainingitem){
				            $join->on('sco_score_data.item_id','=','item_master.item_id')
							     ->where('item_master.company_id',trim($trainingitem['company_id']));
			                   })
							   ->where('sco_score_data.item_id',trim($scored['item_id']))
							   ->where('sco_score_data.user_id',trim($scored['user_id']))
							   ->select('sco_score_data.*')
							   ->get();
			
			if(count($result)>0){
				$userScoreinsert=ScoScoreData::whereitemId($scored['item_id'])->whereuserId($scored['user_id'])->first();
				$user_scoreinsert=$userScoreinsert->scorm_data_id;
			    ScoScoreData::whereitemId($scored['item_id'])->whereuserId($scored['user_id'])->update($scored);
			}else{
				$userScoreinsert =ScoScoreData::create($scored);
				$user_scoreinsert=$userScoreinsert->scorm_data_id;
			}
			
			return $user_scoreinsert;
			
		}
		
		public static function insertValueIntoTranscriptHistory($trainingitem)
		{
			
			$traningtypeid = $trainingitem['traningtype_id'];
			$usertraininghistory			 					= array();
			$usertraininghistory ['user_id'] 					= $trainingitem['users_id'];
			$usertraininghistory ['user_training_id'] 			= $trainingitem['user_training_id'];
			$trainingitem['completion_date'] 					= str_replace('-','/',$trainingitem['completion_date']);
			$usertraininghistory ['training_start_date_time']	= date('Y-m-d H:i:s',strtotime($trainingitem['completion_date']));
			$usertraininghistory ['training_end_date_time'] 	= date('Y-m-d H:i:s',strtotime($trainingitem['completion_date']));
			$usertraininghistory ['php_session_id'] 			= session_id ();
			$usertraininghistory ['course_completion_percentage'] = '100';
			if ($traningtypeid != 4)
				$usertraininghistory ['item_id'] 				= $trainingitem['item_id'];
			$usertraininghistory ['is_active'] 					= '1';
			$userTrainingSave = UserTrainingHistory::create($usertraininghistory);
			$transcriptId = $userTrainingSave->id;
			return $transcriptId;
		}
		
		
		public static function insertValueIntoTrainingCatalogTranscript($trainingitem) 
		{
			
			$traningtypeid			=	$trainingitem['traningtype_id'];
			$trainingCatalogTranscript								= array();
			$trainingCatalogTranscript ['class_schedule_id'] 		= $trainingitem ['classschedule_id'];
			$trainingCatalogTranscript ['class_id'] 				= $trainingitem ['session_id'];
			if ($traningtypeid != 5){
			 if ($traningtypeid != 4)
				$trainingCatalogTranscript ['item_id'] 				= $trainingitem ['item_id'];
			}else{
			 $trainingCatalogTranscript ['registration_id'] 			= $trainingitem['assessment'];
			}
			$trainingCatalogTranscript ['user_id'] 					= $trainingitem ['users_id'];
			$trainingCatalogTranscript ['user_training_id'] 		= $trainingitem ['user_training_id'];
			$trainingCatalogTranscript ['company_id'] 				= $trainingitem ['company_id'];
			$trainingCatalogTranscript ['credit_value'] 			= ($trainingitem['training_credit']) ? $trainingitem['training_credit'] : 0;
			$trainingCatalogTranscript ['completion_date'] 			= date('Y-m-d',strtotime($trainingitem['completion_date']));
			$trainingCatalogTranscript ['expiration_date'] 			= '0000-00-00';
			$trainingCatalogTranscript ['times'] 					= '0';
			$trainingCatalogTranscript ['is_approved'] 				= '1';
			$trainingCatalogTranscript ['is_active'] 				= '1';
			$trainingCatalogSave=TrainingCatalogTranscript::create($trainingCatalogTranscript);
			$transcriptId = $trainingCatalogSave->training_transcript_id;
			return $transcriptId;
		}
		
		public static function emailtoSupervisiorforApproval($trainingitem)
		{
			$compid							=	$trainingitem['company_id'];
			$require_supervisor_approval	=	$trainingitem['require_supervisor_approval'];
			$auto_approve					=	$trainingitem['auto_approve'];
			$usertrainingid					=	$trainingitem['user_training_id'];
			$ssupervisor_id					=	$trainingitem['ssupervisor_id'];
			$complition_date				=	$trainingitem['completion_date'];
			$userid							=	$trainingitem['users_id'];
			$title							=	$trainingitem['training_title'];
			$credit_value					=	($trainingitem['training_credit']) ? $trainingitem['training_credit'] : 0;

			$company = Trainingcatalog::getCompanyname($compid);
			$companyname=$company[0]['company_name'];
			
			$from = CommonFunctions::getFromEmailId($compid);
		    $fromname = CommonFunctions::getFromCompanyName($compid);
			
			if($require_supervisor_approval==1 && $auto_approve==0) {
			     $checksuerapprove = Trainingcatalog::checksupervisiorapprove($usertrainingid);
				 if(count($checksuerapprove)==0)
					{
						$dataucSuper=array();
						$dataucSuper['user_id']=$userid;
						$dataucSuper['training_catalog_id']=$usertrainingid;
						$dataucSuper['supervisior_id']=$ssupervisor_id;
						$dataucSuper['company_id']=$compid;
						$dataucSuper['is_approve']=0;
						UserSupervisiorApprove::create($dataucSuper);
			
						
					}
					$supervisordetails = Trainingcatalog::getuserdetail($ssupervisor_id, $compid);
					
			        $userdetail = Trainingcatalog::getuserdetail($userid, $compid);
			        $username = $userdetail[0]->user_name;
					$supervisorusername = isset($supervisordetails[0]->user_name) ? $supervisordetails[0]->user_name : '';
					$emailid = isset($supervisordetails[0]->email_id) ? $supervisordetails[0]->email_id : '';
					$emailname = $supervisorusername;
					$complition_dates=CommonHelper::simpleDateToPhpDate($complition_date);
					$complition_dates = date('m/d/Y',strtotime($complition_dates));
					$subject1="Approval Required - Submitted Training";
					$content1 = "$supervisorusername,<br><br>
					<p>$username has submitted a training item that requires your approval.<br/>
					</p>
					<p>
					Item : $title<br>
					Completion Date : $complition_dates<br>
					Credits : $credit_value<br>
					</p><br/>
					Sincerely, <br/>
					$companyname ";
					TrainingItemsFunctions::sendmailAction($from, $fromname, $emailname, $subject1, $emailid, $content1);
		   }
		
			
		}
		
		
		
		public static function sendmailAction($from, $fromname, $emailname, $subject, $emailreceiver, $content) {
			try{
				
				Mail::send([], [], function ($message) use($emailreceiver, $emailname,$subject,$from, $fromname,$content){
				 
				    $message->from($from, $fromname);
				    $message->to($emailreceiver, $emailname);
					$message->subject($subject);
					$message->setBody($content, 'text/html'); // for HTML rich messages
				});
				
			}catch(Exception $e){
				echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
		
		}

}