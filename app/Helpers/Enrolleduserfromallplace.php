<?php
namespace App\Helpers;

use App\Models\UserSupervisor;
use App\Models\UserMaster;
use App\Models\UsertrainingCatalog;
use App\Models\TbluserlLink;
use App\Models\UserSupervisiorApprove;
use App\Models\ClassSchedule;
use App\Http\Traits\Enrollclass;
use App\Http\Traits\Userrating;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Trainingprogram;
use CommonHelper;
use Auth; 
use Mailnotification;

class Enrolleduserfromallplace
{

	public static function enrollUserToClass($userid,$roleid, $compid,$classid,$from='',$training_program_code)
	{

		// Enroll User in the class with pending approvals
		// check settings 
		// if required pending approvals
		// check supervisor setting if it is marked as auto approve
		// in case it is not require supervisor approval - approve the training item
		// if marked as auto approve then approve the training item
		// if marked for approval required - send email to supervisor for user enrollment		
		
		//	First we get the all details for this schdule
		$returnval = Enrolleduserfromallplace::ReturnAllTrainingData($compid,$classid,$training_program_code);
		
		$dataenroll =  array();
		$dataenroll['scheduleid'] 			=  $returnval[0]->class_schedule_id;
		$dataenroll['item_name'] 			=  $returnval[0]->item_name;
		$dataenroll['schedule_name'] 		=  $returnval[0]->schedule_name;
		$dataenroll['class_name'] 			=  $returnval[0]->class_name;
		$dataenroll['class_id'] 			=  $returnval[0]->class_id;
		$dataenroll['recursive_date'] 		=  $returnval[0]->recursive_date;
		$dataenroll['max_seat'] 			=  $returnval[0]->max_seat;
		$dataenroll['no_of_waiting_seat'] 	=  $returnval[0]->no_of_waiting_seat;
		$dataenroll['training_type_id'] 	=  $returnval[0]->training_type_id;
		$dataenroll['itemtype'] 			=  $returnval[0]->delivery_type_id;
		$dataenroll['course_id'] 			=  $returnval[0]->course_id;
		
		$scheduleid = $returnval[0]->class_schedule_id;
		$class_id 	= $returnval[0]->class_id;
		$dataenroll['require_supervisor_aproval_status'] =  $returnval[0]->require_supervisor_aproval_status;
	//
		$getTotalenrolled=Enrollclass::getenrolled($compid, $dataenroll['course_id'], $dataenroll['class_id']);
		$dataenroll['no_of_enrolled_seat']=$getTotalenrolled['totalenroll'];
		$leftseat=$dataenroll['max_seat']-$dataenroll['no_of_enrolled_seat'];
		$course_id = $dataenroll['course_id'];
		$dataenroll['leftseat'] =  $leftseat;
		
		
		$Usertrainings = Usertrainingcatalogs::getTrainingCatalogData($course_id,$userid,$class_id,$compid);
		$Usertrainings = json_decode(json_encode($Usertrainings),false);
		
		if(count($Usertrainings))
		{	
			$usertrainingid = @$Usertrainings[0]->user_training_id;
			$trainingstatusid=@$Usertrainings[0]->training_status_id;
		}
		else
		{
			if($dataenroll['require_supervisor_aproval_status']==1)
			{	
				$dataenroll['training_status_id'] =  10;
				$dataenroll['is_enroll'] =  1; 
		    }
			else
			{
				$dataenroll['training_status_id'] =  3;
				$dataenroll['is_enroll'] =  1; 
			}
			 $usertrainingid = Enrollclass::classUserenrolled($dataenroll,$userid,$compid);
		} 
		$dataenroll['user_training_id']=$usertrainingid;
		
		$paramurl=Enrolleduserfromallplace::TbluserlLinkFn($usertrainingid,$userid,$compid,$dataenroll,$roleid);
		
		$class_schedule_details = Enrollclass::getClassSchedules($dataenroll['class_id'], $compid);
	   
		if(isset($dataenroll['course_id']) && isset($dataenroll['class_id'])){
			Enrolleduserfromallplace::updateclass($dataenroll['class_id'], $compid);
		}
		
		if($leftseat < 1)
		{
			Enrollclass::user_class_scedule($scheduleid,$compid,$userid);
			$wherecs=array();
			$wherecs['class_schedule_id']="$scheduleid";
			$wherecs['company_id']       ="$compid";
			$newwaiting=$dataenroll['no_of_enrolled_seat']+1;
			
			Enrollclass::classroomWaitingList($dataenroll, $userid, $compid, $class_schedule_details);
			
			Enrollclass::updateclassschedulewaiting($newwaiting,$wherecs);
			
			Enrolleduserfromallplace::classenrollforwaiting($userid,$compid,$dataenroll,$from,$roleid);
			Mailnotification::employeenrolmentWaitinglist($usertrainingid,$paramurl);	
			
			$check_wt =$dataenroll['class_name'].'#$wt';
		}
		else
		{	
		    $fetchsupervisors = Enrollclass::fetchuserssupervisorss($compid, $userid);
			$fetchsupervisors = json_decode(json_encode($fetchsupervisors),true);
			Enrollclass::classroomEnroll($dataenroll, $userid, $compid, $class_schedule_details);
		
			if (count($fetchsupervisors) || $roleid==1) 
			{
				$noenrolledusers=@$dataenroll['no_of_enrolled_seat']+1;
				if($fetchsupervisors[0]['supervisor_id']==0 && $roleid==1)
					$fetchsupervisors[0]['supervisor_id']=$userid;

				$supervisorid = $fetchsupervisors[0]['supervisor_id'];
			
				if ($supervisorid != '') 
				{	
					$checksetting =       Enrollclass::fetchsupervisorsettting($compid, $supervisorid);
					$fetchupdate		= (count($checksetting) > 0) ? $checksetting[0]->notify_update : 0;
					$fetchautoapprove	= (count($checksetting) > 0) ? $checksetting[0]->auto_approve : 0;
					$class_email_details = array("course_id"=>$dataenroll['course_id'], "class_id"=>$dataenroll['class_id'], "user_id"=>$userid, "company_id"=>$compid);
		
					if($dataenroll['require_supervisor_aproval_status']==1)
					{
					     
						
						  Enrolleduserfromallplace::supervisiorApprovalRequire($userid,$compid,$dataenroll,$fetchautoapprove,$noenrolledusers,$supervisorid,$usertrainingid);
						if ($fetchautoapprove == 1) {	
							//mail to user if enrolled and auto approve is 1
           					 Mailnotification::employeenrolmentRequested($usertrainingid, $paramurl, $class_email_details);
							
						}
						else
						{
							
							  //mail to user if pending approval
							  Mailnotification::employeenrolment($usertrainingid,$paramurl); 
                              //mail to supervisior if pending approval							  
							  Mailnotification::supervisioAprovesendmail($usertrainingid,$paramurl);
							
						}
					}
					else
					{	
						$is_external=0;
						$training_type_id=2;
						$notification_id=1;

						Enrollclass::supervisiorApprovalNotRequire($userid,$usertrainingid);
						if(((int)$training_type_id==2 || (int)$training_type_id==3) && (int)$is_external==0 && $notification_id==1){
							Mailnotification::mailToSupervisorForClassApprove($usertrainingid);
						}
						
						Enrollclass::classroomEmailSendStatus($dataenroll, $userid, $compid, $class_schedule_details);
						//mail to user if enrolled
						Mailnotification::employeenrolmentRequested($usertrainingid, $paramurl, $class_email_details);
					}
				}
			}	
			$check_wt=$dataenroll['class_name'].'#$en';
		}
		
		return $check_wt;
	}
	
	public static function supervisiorApprovalRequire($userid,$compid,$dataenroll,$fetchautoapprove,$noenrolledusers,$supervisorid,$usertrainingid)
	{	
		
		$data=array();
		if ($fetchautoapprove == 1) {
			$data['is_approved'] = 1;
			$data['training_status_id'] = 3;
		}else{
			$data['is_approved'] = 0;
			$data['training_status_id'] = 10;
		}	
		$data['is_enroll'] = 1;

		UsertrainingCatalog::where('user_training_id','=', $usertrainingid)->update($data);

		if($fetchautoapprove != 1) {
			$data2 = array();
			$data2['user_id'] 				= 	$userid;
			$data2['training_catalog_id'] 	= 	$usertrainingid;
			$data2['supervisior_id'] 		= 	$supervisorid;
			$data2['company_id'] 			= 	$compid;
			$data2['is_approve'] 			= 	0;
			UserSupervisiorApprove::insert($data2);
		}
	}
	
	  public static function dropenrolmentAnywhere($userid, $compid, $classid, $mailsend=1)
	  {   
		  $result=UserSupervisor::whereuserId($userid)->whereisActive(1)->wherecompanyId($compid)->get();
		  if(!$result)
			{
				$userMasterData = UserMaster::wherecompanyId($compid)->wheremacId(1)->first()->toArray();
				$data = array();
				$data['user_id'] = $userid;
				$data['company_id'] = $compid;
				$data['is_active'] = 1;
				$data['supervisor_id'] = $userMasterData['user_id'];
				UserSupervisor::create($data);
			}
			$Usertrainings=UsertrainingCatalog::whereuserId($userid)->wherecompanyId($compid)->wheresessionId($classid)->get()->toArray();
			if(count($Usertrainings)>0) {
				$roleid = Auth::user()->role_id;
				$item_id = $Usertrainings[0]['item_id'];
				$user_training_id = $Usertrainings[0]['user_training_id'];
			}
	  }
	  
	  
	  public static function dropenrolfromclass($userid, $compid, $classid,$training_program_code,$mailsend=1){
		  $Usertrainings = Usertrainingcatalogs::fetchAll($userid,$compid,$classid,$course_id = '');
		  	if(count($Usertrainings)>0){
				$roleid = Auth::user()->role_id;
				$course_id = $Usertrainings[0]['course_id'];
				$user_training_id = $Usertrainings[0]['user_training_id'];
				
				Enrolleduserfromallplace::dropUserFromClass($userid, $roleid, $compid, $classid,$training_program_code);
				Enrolleduserfromallplace::deleteFromTblUserLink($user_training_id, $compid);
				$userToEnroll = Usertrainingcatalogs::getUserInWaitinglist($classid, $compid);
				if(count($userToEnroll)>0){
					$classschedule = ClassSchedule::where('class_id','=',$classid)->get()->toArray();
					$maxseat = $classschedule[0]['max_seat'];
					if($maxseat>0){
						$enrolledusers = Usertrainingcatalogs::getenrolledForClass2($classid, $compid);
						$totalenrolled = count($enrolledusers);
					    $leftseat = $maxseat - $totalenrolled;
						if($leftseat>0){
							$coursedetails = Trainingprogram::getClassroomCourseDetails($userid, $compid, $classid, $course_id);
						    $class_start_date = strtotime($coursedetails[0]['start_date_class']);
							if($class_start_date > time()){
							 Enrolleduserfromallplace::enrollUserToClass($userToEnroll[0]['user_id'], $roleid, $compid, $classid);
						    }
							
						}
					}
				}
			}
	  }
	  
	  
	  public static function dropUserFromClass($userid, $roleid, $compid, $class_id,$training_program_code){
	
		$returnval= Enrolleduserfromallplace::ReturnAllTrainingData($compid,$class_id,$training_program_code);
		$dataenroll =  array();
		$dataenroll['scheduleid'] 			=  $returnval[0]->class_schedule_id;
		$dataenroll['mailsend'] 			=  '';//$mailsend;
		$dataenroll['schedule_name'] 		=  $returnval[0]->schedule_name;
		$dataenroll['class_id'] 			=  $returnval[0]->class_id;
		$dataenroll['recursive_date'] 		=  $returnval[0]->recursive_date;
		$dataenroll['max_seat'] 			=  $returnval[0]->max_seat;
		$dataenroll['no_of_waiting_seat'] 	=  $returnval[0]->no_of_waiting_seat;
		$dataenroll['itemtype'] 			=  $returnval[0]->itemtype;
		$dataenroll['course_id'] 				=  $returnval[0]->course_id;
		$dataenroll['require_supervisor_aproval_status'] =  $returnval[0]->require_supervisor_aproval_status;
		
		$class_id							= $returnval[0]->class_id;
		$getTotalenrolled=Enrollclass::getenrolled($compid, $dataenroll['course_id'], $dataenroll['class_id']);
		$dataenroll['no_of_enrolled_seat'] = $getTotalenrolled['totalenroll'];
		
		$leftseat=$dataenroll['max_seat'] - $dataenroll['no_of_enrolled_seat'];
		$dataenroll['leftseat'] = $leftseat;	
		
		$Usertrainings = Usertrainingcatalogs::fetchAll($userid,$compid,$class_id,$dataenroll['course_id']);

		if(count($Usertrainings)>0)
		{
			
			$datau=array();
			$datau['training_status_id']=5;
			$datau['is_enroll']=0;
			foreach($Usertrainings as $key => $value)
			{
				$wherepcat=array();
				$ustid=$value['user_training_id'];
				
				Usertrainingcatalogs::updateanywhere($datau, $userid,$compid,$class_id,$ustid); 
				$getWaitingUser = Enrollclass::getWaitingUser($compid, $class_id);
				Mailnotification::enrolmentDrop($ustid);
				
			}
		}
		return '0';
	}
	  
	  
	  public static function ReturnAllTrainingData($compid,$classid,$training_program_code){
		$finadallresult = Enrollclass::getTrainingDataForenforll($compid,$classid,$training_program_code);
		return $finadallresult;
	}
	
	public static function deleteFromTblUserLink($user_training_id, $compid){
		  TbluserlLink::where('training_catalog_id','=',$user_training_id)->where('company_id','=',$compid)->delete();
	}
	
	public static function updateclass($classid, $company_id, $schedule_id='')
	{
		$updated_schedule_ids = $all_schedule_ids = array();
		$class_master_res = Enrollclass::getSessionSchedules($classid, $company_id);
		$schedule_ids = array();
		if(isset($class_master_res[0]->session_schedule_update))
		{
			$schedule_ids = json_decode($class_master_res[0]->session_schedule_update,true);
			foreach($schedule_ids as $key=>$sschedule_id)
			{
				if($schedule_id!='' && $sschedule_id['schedule_id']==$schedule_id){
					$schedule_ids[$key]['send_mail_status'] = "yes";
				}else{
					$schedule_ids[$key]['send_mail_status'] = "yes";
				}
			}
		}

		 $schedule_ids = json_encode($schedule_ids);
		 Enrollclass::update_class_master($classid, $company_id, $schedule_ids);
 }
 
 public static function classenrollforwaiting($userid,$compid,$dataenroll,$from='',$roleid)
	{	
	
		$user_training_id=$dataenroll['user_training_id'];
		$scheduleid=$dataenroll['scheduleid'];
		$class_id=$dataenroll['class_id'];
		$Usertrainings =UsertrainingCatalog::where('user_training_id','=',$user_training_id)->where('user_id','=',$userid)->where('company_id','=',$compid)->get()->toArray();
			
		if($from=='user'){
			if(count($Usertrainings)==0)
			{ 
			    Enrollclass::user_class_scedule($scheduleid,$compid,$userid);
				$usertrainingid = Enrollclass::classUserenrolledwaiting($dataenroll,$userid,$compid);			
			}
			 else
			{
				$dataut=array();
				$dataut['training_status_id']=4;
				$dataut['is_enroll']=0;		
				UsertrainingCatalog::where('class_id','=',$class_id)->where('user_id','=',$userid)->where('company_id','=',$compid)->where('user_training_id','=',$user_training_id)->update($dataut);
				$usertrainingid=$Usertrainings[0]['user_training_id'];
				Enrollclass::user_class_scedule($scheduleid,$compid,$userid);
			}		 
		}else{
			if(count($Usertrainings)==0)
			{ 
			   
				Enrollclass::user_class_scedule($scheduleid,$compid,$userid);
				$usertrainingid = Enrollclass::classUserenrolledwaiting($dataenroll,$userid,$compid);			
			}
		
		}
		/*Here call mail Send Healper*/
		if($dataenroll['mailsend']==1)
		{
			
			$paramurl=Enrolleduserfromallplace::TbluserlLinkFn($usertrainingid,$userid,$compid,$dataenroll,$roleid);
			Mailnotification::employeenrolmentWaitinglist($usertrainingid,$paramurl);
			/*End Here*/
		}	
	}
	
	 public static function TbluserlLinkFn($usertrainingid,$userid,$compid,$dataenroll,$roleid)
	 {
		 
		 $checkAlreadyValueExit=Userrating::checkUrlexistsornot($usertrainingid,$userid,$compid,'classroomcourse/training');
	
		$snurl=date('Y-m-d h:i:s')."_".$compid."_".$userid;
		$paramurl=md5($snurl);
		$datalink=array();
		$datalink['training_catalog_id']=$usertrainingid;
		$datalink['company_id']=$compid;
		$datalink['user_id']=$userid;
		
		$viewId = ''.$userid.'#'.$roleid.'||training_catalog||'.$dataenroll['course_id'].'||'.$dataenroll['class_id'].'||'.$dataenroll['training_type_id'];		
		$encoded_viewId = CommonHelper::getEncode($viewId);

		$datalink['pagename']="classroomcourse/training";
		$datalink['parameter']="viewId/$encoded_viewId";
		
		if(count($checkAlreadyValueExit)>0)
		{
			$paramurl=$checkAlreadyValueExit[0]->send_URL;
		}
		else
		{					
			$datalink['send_URL']=$paramurl;
			TbluserlLink::insert($datalink);
		}

		 return $paramurl;
	 }
	 
}