<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Http\Traits\Helpcontent;
use App\Http\Traits\Trainingprograms;
use CommonHelper;
use URL;

class HelpController extends Controller
{
    //
	public function helpAction()
	{
		$ses 		=Auth::user();
		$user_id	=$ses->user_id;
		$compid		=$ses->company_id;
		$roleid		=$ses->role_id;

		$topic_name=Helpcontent::getAlltopicsList($compid,$roleid);
		return response()->json($topic_name);
	}
	
	public function topicdetails(Request $request)
	{
		$ses 		=Auth::user();
		$user_id	=$ses->user_id;
		$compid		=$ses->company_id;
		$roleid		=$ses->role_id;
		$topic_id=$request->input('topic_id');
		if($topic_id){
			$allquestions=Helpcontent::getTopicQuestion($topic_id,$compid,$roleid);
			return response()->json($allquestions);
		}
	}
		
		
	public function allreferencesAction()
	{
		$ses 				=Auth::user();
		$userid				=$ses->user_id;
		$company_id			=$ses->company_id;
		$roleid				=$ses->role_id;
		$CssData=CommonHelper::getcompanycss($company_id);
		$buttonclass = 'button_lblue_r4';
		if(count($CssData))
		{
			if(!empty($CssData[0]->button_color))
				$buttonclass = $CssData[0]->button_color;
		}	
		$baseUrl='';
		$allreferences=Helpcontent::getAssignReferences($userid,$company_id,$roleid);			
		$dataArray = array();
		$i=1;
		foreach($allreferences as $val)
		{			
			$button ='';
			$title=$val['title'];
			$category_name=$val['category_name'];
			$viewId = $val['ref_document_id'];
			$getmedia = Helpcontent::getmediatype($viewId, $company_id);
			
			if(isset($getmedia[0]))
			$getmedia = $getmedia[0]['media_id'];
			else $getmedia = 0;

			$encoded_viewId = CommonHelper::getEncode($val['ref_document_id']);
		
			$view="<a class= '".$buttonclass."' href='".$baseUrl."#/index/view-reference/viewid/$encoded_viewId' >View</a>";
			if(!empty($val['file_name']) && $val['allow_download']==1)
			{
				$button = '<div class="'.$buttonclass.' button_lblue_r4 mr07 hand" style="margin-left: 0;" onclick="viewlaunchuser('.$viewId.' , '.$getmedia.');">Launch &#187;</div><a style="padding: 8px 12px;" class="hand   '.$buttonclass.'" target="_blank"  href="'.$baseUrl.'/eliteprodlms/mediafiles/'.$val['company_id'].'/'.$val['file_name'].'" download="'.$val['file_name'].'">Download &#187;</a>';
			}
             $refrence['title']			=   $title;
             $refrence['category_name']	=   $category_name;
             $refrence['view']			=   $view;
             $refrence['button']		=   $button;
			 $dataArray[] =$refrence;
			$i++;
		}
		return response()->json($dataArray);
	}	
	
	public function mediaplayer($file_id) 
	{
			$ses 				 =Auth::user();
			$compid			 =$ses->company_id;
			$protocol            = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http"; 
			$files = Helpcontent::getmediatype($file_id, $compid);
			
            $baseUrl = URL::to('/');
			$path = $protocol.'://'.$baseUrl.'/eliteprodlms/mediafiles/'.$compid.'/';
		
			if(count($files) > 0) 
			{
				$files=$files[0];
				
				$filesname = $files['file_name'];
				
				$namefile=explode('.',$filesname);
				
				$filepath = substr($filesname,-3);
				$revStr = explode('.',strrev($filesname));
				$fileExt = strrev($revStr[0]);
				
				$fileurl=$embedded_code='';
				
				if(isset($files['embedded_code'])){
					$filepath='';
					$embedded_code=stripslashes($files['embedded_code']);
				}else{
					
				    $fileurl=$path.@$filesname;
				}
			}
				 
				
				 return view('user.help.mediaplayer', ['mediafile' => $filesname,'compid'=>$compid,'filepath'=>$filepath,'filesname'=>$fileurl,'namefile'=>$namefile[0],'embedded_code'=>$embedded_code]);
	
	}
	
	public function embeddedplayer($file_id) 
	{
		    $ses 				 =Auth::user();
			$compid				 =$ses->company_id;
			$protocol            = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http"; 
		
			$files = Helpcontent::getmediatype($file_id, $compid);
            $baseUrl = URL::to('/');
			
			$path = $protocol.'://'.$baseUrl.'/eliteprodlms/mediafiles/'.$compid.'/';
		
			if(count($files) > 0) 
			{
				$files=$files[0];
				
				$filesname = $files['file_name'];
				
				$namefile=explode('.',$filesname);
				
				$filepath = substr($filesname,-3);
				$revStr = explode('.',strrev($filesname));
				$fileExt = strrev($revStr[0]);
				
				$fileurl=$embedded_code='';
				
				if(isset($files['embedded_code'])){
					$filepath='';
					$embedded_code=stripslashes($files['embedded_code']);
				}else{
					
				    $fileurl=$path.@$filesname;
				}
			}
				 
				
		 return view('user.help.mediaplayer', ['mediafile' => $filesname,'compid'=>$compid,'filepath'=>$filepath,'filesname'=>$fileurl,'namefile'=>$namefile[0],'embedded_code'=>$embedded_code]);
	}
	
	public function viewReferenceAction(Request $request)
	{
			$ses 				 =Auth::user();
			$company_id				 =$ses->company_id;
			$CssData=CommonHelper::getcompanycss($company_id);
			$buttonclass = 'button_lblue_r4';
			if(count($CssData))
			{
			if(!empty($CssData[0]->button_color))
			    $buttonclass = $CssData[0]->button_color;
			}	
			$viewId =$request->input('viewid');
			$baseUrl='';
			$ref_id =CommonHelper::getDecode($viewId);

			$references =Helpcontent::getreferencetoedit($ref_id,$company_id)->toArray();
			$referencecategory =Helpcontent::getreferencecategory($ref_id, $company_id);
			$getmedia =Helpcontent::getmediatype($ref_id, $company_id);
			$returnarray=array('buttonclass'=>$buttonclass,'ref_id'=>$ref_id,'getmedia'=>$getmedia,'references'=>$references,'referencecategory'=>$referencecategory,'company_id'=>$company_id);
	        return  response()->json($returnarray);
	}
	
}
