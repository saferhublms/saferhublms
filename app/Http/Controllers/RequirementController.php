<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\Requirement;
use CommonHelper,CommonFunctions;
use Auth,DateTime,DateTimeZone;
use App\Models\CourseGroups;
use App\Models\CourseUsers;

class RequirementController extends Controller
{
   
   
   public function requirementDataAction(Request $request){
	   /*  $authtoken = $request->input('authtoken');
		$user_id = CommonHelper::token_decode($authtoken);
		$user = CommonHelper::authtokenuserdtail($user_id); */
		$user = Auth::user();
	    $compid =  $user->company_id;
        $userid =  $user->user_id;
		$roleid =  $user->role_id; 
		$requirement_data = Requirement::commonrequirement($compid,$userid,$roleid);
		return response()->json($requirement_data);
		
   }
   
   public function myrequirements(Request $request)
   {
	    $user = Auth::user();
	    $compid =  $user->company_id;
        $userid =  $user->user_id;
		$roleid =  $user->role_id;
		$returnArray=array();
		$draw = $request->input('draw');
		$userTimeZone=CommonHelper::getUserTimeZone($userid);
		$companyInfo=CommonFunctions::getCompanyInfo();
		
        $courseGroup=CourseGroups::join('user_group','user_group.group_id','=','course_groups.group_id')
		             ->join('course_details','course_details.course_id','=','course_groups.course_id')
		             ->join('courses','course_details.course_id','=','courses.course_id')
		             ->join('training_programs','training_programs.training_program_code','=','courses.training_program_code')
		             ->join('training_type','training_type.training_type_id','=','training_programs.training_type')
					 ->where('user_group.user_id',$userid)
					 ->groupBy('course_groups.course_id')
					 ->select('course_details.course_name','training_type.training_type','courses.credit_value','course_groups.is_mandatory','course_groups.course_groups_id','course_groups.due_date','courses.training_program_code','training_type.training_type_id')
					 ->get();
					 
		$courseUser=CourseUsers::join('course_details','course_details.course_id','=','course_users.course_id')
		             ->join('courses','course_details.course_id','=','courses.course_id')
		             ->join('training_programs','training_programs.training_program_code','=','courses.training_program_code')
		             ->join('training_type','training_type.training_type_id','=','training_programs.training_type')
					 ->where('course_users.user_id',$userid)
					 ->groupBy('course_users.course_id')
					 ->select('course_details.course_name','training_type.training_type','courses.credit_value','course_users.is_mandatory','course_users.course_users_id','course_users.due_date','courses.training_program_code','training_type.training_type_id')
					 ->get();
					 

		for($i=0;$i<count($courseGroup);$i++){		
			
			$newDueDate=CommonHelper::convertDateToSpecificTimezone($courseGroup[$i]->due_date,$userTimeZone);
			$returnArray[]=array('courseName'=>$courseGroup[$i]->course_name,'trainingType'=>$courseGroup[$i]->training_type,'dueDate'=>$newDueDate,'assignType'=>'Group','isMandatory'=>($courseGroup[$i]->is_mandatory==1)?'Mandetory':'Optional','traningProgramId'=>$courseGroup[$i]->training_program_code,'trainingTypeId'=>$courseGroup[$i]->training_type_id,'dateFormat'=>($companyInfo)?CommonHelper::getdatecalenderformateAngular($companyInfo[0]->company_date_formate):'yyyy-MM-dd');
		}	
		
		for($i=0;$i<count($courseUser);$i++){
			
			$newDueDate=CommonHelper::convertDateToSpecificTimezone($courseUser[$i]->due_date,$userTimeZone);
			
			$returnArray[]=array('courseName'=>$courseUser[$i]->course_name,'trainingType'=>$courseUser[$i]->training_type,'dueDate'=>$newDueDate,'assignType'=>'User','isMandatory'=>($courseUser[$i]->is_mandatory==1)?'Mandetory':'Optional','traningProgramId'=>$courseUser[$i]->training_program_code,'trainingTypeId'=>$courseUser[$i]->training_type_id,'dateFormat'=>($companyInfo)?CommonHelper::getdatecalenderformateAngular($companyInfo[0]->company_date_formate):'yyyy-MM-dd');
		}
		
		return response()->json(['data'=>$returnArray,'count'=>count($returnArray),'draw'=>$draw,'companyInfo'=>$companyInfo]);
   }
}
