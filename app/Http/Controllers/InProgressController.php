<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use CommonHelper,Mylearningfunctions,ElearningMedia;
use Auth;
use URL;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Trainingprogram;
use App\Http\Traits\Blendedtrainingitem;
use App\Http\Traits\Assignment;
use App\Http\Traits\Requirement;
use App\Http\Traits\Assessmentcrypt;
use stdClass;

class InProgressController extends Controller
{
	use Usertrainingcatalogs,Trainingprogram,Blendedtrainingitem,Assignment,Requirement;
     public function inprogressDataAction(Request $request){
		$user = Auth::user();
	    $compid =  $user->company_id;
        $userid =  $user->user_id;
		$roleid =  $user->role_id; 
		$baseUrl = URL::to('/');
	    $CssData = CommonHelper::getcompanycss($compid);
		$buttonclass = 'button_lblue_r4';
		if(count($CssData))
			{ 
				if(!empty($CssData[0]->button_color))
					$buttonclass = $CssData[0]->button_color ;
			}
	   $myenrollment_data = array();
	   $soundconceptcmpid = array('14','16', '17');
	   $allMyEnrollments =Mylearningfunctions::getMyEnrollmentsInProgress($userid, $compid, 1);
	   if(count($allMyEnrollments)>0){
		   foreach($allMyEnrollments as $myenrollments){
			   $subgrid_array = array();
			   $status ='';
			   $userenrollment =Usertrainingcatalogs::getTrainingCatalogData($myenrollments['item_id'],$userid,$myenrollments['class_id'],$compid);
			   $program_id = CommonHelper::getEncode($myenrollments['program_id']);
			   $supervisor_approval = CommonHelper::getEncode($myenrollments['supervisor_approval']);
			   $class_id = CommonHelper::getEncode($myenrollments['class_id']);
			   if(count($userenrollment) > 0){
				   if($userenrollment[0]['is_approved']!=2){
					   if($userenrollment[0]['training_status_id']!=5) {
						   $approved = CommonHelper::getEncode(2);
						   $status ='<a ng-click="grid.appScope.openPreview(\''.$approved.'\',\''.$program_id.'\',\''.$supervisor_approval.'\',\''.$class_id.'\',row);" class="button_lblue_r4 hand ">Drop </a>';
					   }
				   }else{
							$approved = CommonHelper::getEncode(1);
							$status ='<a ng-click="grid.appScope.openPreview(\''.$approved.'\',\''.$program_id.'\',\''.$supervisor_approval.'\',\''.$class_id.'\',row);"><div class="new_css_button hand" >Enroll <span>&#187;</span></div></a>';
					}
					
					if(!(($userenrollment[0]['training_status_id']==10) || ($userenrollment[0]['training_status_id']==3) || ($userenrollment[0]['training_status_id']==4))){
						$status.='<div class="button_lblue_r4 hand ml10" ng-click="grid.appScope.hides(\''. CommonHelper::getEncode($myenrollments['user_training_id']).'\',row);">Delete</div>';
					}
					
					$training_code =explode('_#_',$myenrollments['training_code']);
					$viewId = ''.$userid.'#'.$roleid.'||inprogress||'.$myenrollments['item_id'].'||'.$myenrollments['class_id'].'||'.$myenrollments['training_type_id'].'';
					$encoded_viewId = CommonHelper::getEncode($viewId);
					$viewButton='<a class="'.$buttonclass.' hand" href="'.$baseUrl.'/#/classroomcourse/training/viewId/'. $encoded_viewId.'">View<span>&#187;</span></a>';
					
				
						
							/* $myenrollment_data[] = array(
							"id" => $myenrollments['user_training_id'],
							"data" => array(
							'<div class="mb07"><b>Coures Id - </b> ('.$training_code[1].')</div><div class="mb07">  <b>Category - </b>'.$myenrollments['category_name'].'</div><div class="description_blk mb07"><b>Description - </b>'.$myenrollments['description'].'</div>',	stripcslashes($myenrollments['itmname']), $myenrollments['training_type'], $myenrollments['creditvalue'], $myenrollments['user_status'], $status, $viewButton)
							); */
							$subgrid_array[] = array('description'=>'<div class="mb07"><b>Coures Id - </b> ('.$training_code[1].')</div><div class="mb07">  <b>Category - </b>'.$myenrollments['category_name'].'</div><div class="description_blk mb07"><b>Description - </b>'.$myenrollments['description'].'</div>');
							$finalArray=array('item_name'=>stripcslashes($myenrollments['itmname']),'training_type'=> $myenrollments['training_type'],'credits'=>$myenrollments['creditvalue'],'status'=>$myenrollments['user_status'],'action'=>$status,'properties'=>$viewButton,'subgrid'=>$subgrid_array);
				            $myenrollment_data[] = $finalArray;
							
							
						
			   }
		   }
		     
	   }
	   
	   
	     $alleLearningInProgress = Mylearningfunctions::getAlleLearningInProgress($userid,$compid);
		 if(count($alleLearningInProgress)>0){
			 foreach($alleLearningInProgress as $key => $eLearningInProgress){
				 $courseDetailArr =  array();
				 $subgrid_array = array();
				 $training_code =explode('_#_',$eLearningInProgress['training_code']);
				 $pro_percantage=$eLearningInProgress['progress_percentage'];
				 $courseDetailArr['training_type_id']			= 	$eLearningInProgress['training_type_id'];
				 $courseDetailArr['training_program_id']		= 	$eLearningInProgress['training_program_id'];
				 $courseDetailArr['blendedprogid']=$eLearningInProgress['blended_program_id'];
				 $courseDetailArr['usertrainingid']=$eLearningInProgress['user_training_id'];
				 $courseDetailArr = CommonHelper::getMediaFileDetails($userid,$compid,$eLearningInProgress['item_id'],$courseDetailArr);
				 
				 $expiration_status = ElearningMedia::checkTrainingExpiration($eLearningInProgress['item_id'], $userid, $compid, $eLearningInProgress['training_program_id']);
				 
				 $programs = Trainingprogram::getTrainingCatalogsElearning($compid, $eLearningInProgress['training_program_id']);
				
				  
				    $courseDetailArr['user_id'] 					= 	$userid;
					$courseDetailArr['item_id'] 					= 	$programs[0]->item_id;
					$courseDetailArr['company_id'] 					= 	$programs[0]->company_id;
					$courseDetailArr['assignment_id'] 				= 	@$programs[0]->assignment_id;
					$courseDetailArr['training_type'] 				= 	@$programs[0]->training_type;
					$courseDetailArr['credit_value'] 				= 	@$programs[0]->credit_value;
					$courseDetailArr['item_image'] 					= 	$programs[0]->item_image;
					$courseDetailArr['item_name'] 					= 	$programs[0]->item_name;
					$courseDetailArr['learningplanid'] 				= 	@$programs[0]->learningplanid;
					$courseDetailArr['description'] 				= 	$programs[0]->description;
					$courseDetailArr['prerequeisites'] 				= 	$programs[0]->prerequeisites;
					$courseDetailArr['training_type_id']			= 	$programs[0]->training_type_id;
					$courseDetailArr['training_program_id']			= 	$programs[0]->training_program_id;
					$courseDetailArr['average_rating'] 				= 	$programs[0]->average_rating;
					
					
					$fileDetails = $courseDetailArr['fileDetails'];
					$userDetails = $courseDetailArr['userDetails'];
					$username = $userDetails[0]->user_name;
					$file_name = $fileDetails['file_name'];
					$media_id = $fileDetails['media_id'];
					$is_lib = $fileDetails['is_lib'];
					
					if($pro_percantage!='100'){
						$viewId = ''.$userid.'#'.$roleid.'||inprogress||'.$eLearningInProgress['training_program_id'].'';
						$encoded_viewId = CommonHelper::getEncode($viewId);
						$type='';
						$pro_percantage = ($pro_percantage >100) ? 50 : $pro_percantage ;
						$data = array();						
						$data[] = '<div class="mb07"><b>Coures Id - </b> ('.$training_code[1].')</div><div class="mb07">  <b>Category - </b>'.$eLearningInProgress['category_name'].'</div><div class="description_blk mb07"><b>Description - </b>'.$eLearningInProgress['description'].'</div>';
						
						$data[] = stripcslashes($eLearningInProgress['item_name']);
						$data[] = $eLearningInProgress['training_type'];
						if(!in_array($compid, $soundconceptcmpid)) $data[] = $eLearningInProgress['credit_value'];
						if($compid != 71)
						{
							$data[] = '<div class="meter-wrap" style="width: 100%;"><div class="meter-value" style="background-image: url(\''. $baseUrl .'/public/images/prog_bg.png\');background-repeat:repeat-x; height:17px; width:'.$pro_percantage.'%;"><div class="meter-text " style="font-size: 10px;">'.$pro_percantage.'%</div></div></div>';
						}

                        if($type=='view'){
							$scorm_param =CommonHelper::getEncode($userid.'_#_'.$username.'_#_'.$courseDetailArr['item_id'].'_#_'.$courseDetailArr['file_id'].'_#_'+$compid.'_#_1');
						}else{
							$scorm_param =CommonHelper::getEncode($userid.'_#_'.$username.'_#_'.$courseDetailArr['item_id'].'_#_'.$courseDetailArr['file_id'].'_#_'.$compid.'_#_0');
						}						
						
						$courseparam =CommonHelper::getEncode($baseUrl.'/mediafiles/'.$compid.'/');
						if($is_lib == "1"){
							$courseparam = CommonHelper::getEncode($baseUrl.'/mediafiles/');
						}
						
						 $enrollelearning = array(
							'confirm'=>1,
							'media_id'=>$media_id,
							'expiration_status'=>$expiration_status,
							'file_id'=>$courseDetailArr['file_id'],
							'item_id'=>$courseDetailArr['item_id'],
							'trainingprogram_id'=>$courseDetailArr['training_program_id'],
							'userprogramtypeid'=>$courseDetailArr['training_type_id'],
							'blendedprogid'=>$courseDetailArr['blendedprogid'],
							'assignmentsend_id'=>($courseDetailArr['assignment_id']!='') ? $courseDetailArr['assignment_id'] : 0,
							'view'=>$type,
							'file_name'=>$file_name,
							'is_lib'=>$is_lib,
							'username'=>$username,
							"user_id"=>$userid,
							"company_id"=>$compid,
							"fileDetails"=>$courseDetailArr['fileDetails'],
						);
						
						 $enrollelearning_arr = CommonHelper::getEncode(json_encode($enrollelearning));
						 
						 $data[] = '<div class="button_lblue_r4 h6 hand"  ng-click="grid.appScope.launchlearning(\''.$expiration_status.'\', \''.$media_id.'\', \''.$enrollelearning_arr.'\', \''.$scorm_param.'\', \''.$courseparam.'\', \''.$encoded_viewId.'\');">Launch</div><div class="button_lblue_r4 hand h6 ml10" ng-click="grid.appScope.hides(\''. CommonHelper::getEncode($eLearningInProgress['user_training_id']).'\',row);">Delete</div>';						
						$data[] = '<a  href="'.$baseUrl.'/#/elearningmedia/training/viewId/'. $encoded_viewId.'" class="button_lblue_r4 hand">View></a>';
						 /*$myenrollment_data[] = array("id"=>$eLearningInProgress['user_training_id'],"data"=>$data); */
						$subgrid_array[] = array('description'=>$data[0]);
						$finalArray=array('item_name'=>$data[1],'training_type'=>$data[2],'credits'=>$data[3],'status'=>$data[4],'action'=>$data[5],'properties'=>$data[6],'subgrid'=>$subgrid_array);
				        $myenrollment_data[] = $finalArray;
					}
				
			 }
		 }
		 
		        $get_data =Usertrainingcatalogs::getblendedProgressData($userid,$compid);
				
				$get_progress_data =array();
				$total_credit_value='';
				if(count($get_data)>0){
					foreach($get_data as $val){
						$items=Blendedtrainingitem::getallItems($userid,$val->blended_program_id,$compid);
						$blendedTrainingItems = array();
						if(count ( $items ) > 0){
							$i = 0;
							foreach($items as $key => $value){
								$trainingItem 	= new stdClass;
								$trainingTypeId = $value ['training_type_id'];
								$is_sequence 	= $value ['is_sequence'];
								if($trainingTypeId == 1){
									$trainingItem->item_id 			= $value['item_id'];
									$trainingItem->training_item_id = $value['training_item_id'];
									$trainingItem->training_type_id = $value['training_type_id'];
									$trainingItem->sequence 		= $value['sequence'];
									$trainingItem->blened_prog_id	= $value['blendedprogram_id'];
									$trainingItem->blended_title	= $value['title'];
									$trainingItem->credit_value		= $value['credit_value'];
									$trainingItem->item_name		= $value['item_name'];
									$trainingItem->training_status	= $value['status'];
									$trainingItem->assessment_id 	 ='';
									$trainingItem->training_program_id = $value['training_program_id'];
									$trainingItem->schedule_id 		= 0;
									$trainingItem->class_id 		=0;
									$trainingItem->require_supervisor_aproval_status =$value['require_supervisor_aproval_status'];
									$trainingItem->registration_id =$value['registration_id'];	
									$trainingItem->is_blended =$value['is_blended'];
									$blendedTrainingItems[] = $trainingItem;
								}elseif($trainingTypeId == 2 || $trainingTypeId == 3){
									$trainingItem->item_id 			= $value['item_id'];
									$trainingItem->training_item_id = $value['training_item_id'];
									$trainingItem->training_type_id = $value['training_type_id'];
									$trainingItem->sequence 		= $value['sequence'];
									$trainingItem->blened_prog_id	= $value['blendedprogram_id'];
									$trainingItem->blended_title	= $value['title'];
									$trainingItem->credit_value		= $value['credit_value'];
									$trainingItem->item_name		= $value['item_name'];
									$trainingItem->training_status	= $value['status'];
									$trainingItem->assessment_id 	 ='';
									$trainingItem->training_program_id 	= $value['training_program_id'];;
									$trainingItem->class_id 			= $value['class_id'];
									$trainingItem->require_supervisor_aproval_status =$value['require_supervisor_aproval_status'];	
									$trainingItem->registration_id =$value['registration_id'];	
									$trainingItem->is_blended =$value['is_blended'];									
									$blendedTrainingItems[] = $trainingItem;
								}elseif($trainingTypeId == 6){
									$trainingItem->assessment_id 	 = $value ['id'];
									$trainingItem->item_id 	 		= '';
									$trainingItem->training_item_id = $value ['training_item_id'];
									$trainingItem->training_type_id = $value ['training_type_id'];
									$trainingItem->sequence 		= $value ['sequence'];
									$trainingItem->blened_prog_id	= $value ['blendedprogram_id'];
									$trainingItem->blended_title	= $value ['title'];
									$trainingItem->credit_value		= $value['creditvalue'];
									$trainingItem->item_name		= $value['assessment_name'];
									$trainingItem->training_status	= $value['status'];
									$trainingItem->due_date	= $value['due_date'];
									if($value['status'] == 1){
										$totalcredit = $value['credit_value'];
									}
									$trainingItem->training_program_id 	= $value['training_program_id'];
									$trainingItem->class_id 			= @$value['class_id'];
									$trainingItem->require_supervisor_aproval_status =$value['require_supervisor_aproval_status'];	
									$trainingItem->registration_id =$value['registration_id'];	
									$trainingItem->is_blended =$value['is_blended'];										
									$blendedTrainingItems[] = $trainingItem;
								}
							}
							
							$blended_data1 = '';
							$blended_data1 .="<table width='100%' cellspacing='0' cellpadding='0' class='table_style_1'>
							<tr>
								<td><b>Item Name</b></td> 
								<td><b>Training Type</b></td>
								<td><b>Credit</b></td> 
								<td><b>Status</b></td>
								<td><b>Action</b></td> 
								<td><b>Properties</b></td>
							</tr>";	
							if(count($blendedTrainingItems) > 0){
								$i=1;												
							$all_itemid='';$all_assessment_id='';
								foreach($blendedTrainingItems as $key1 =>$program){
									$total_credit_value+=	$program->credit_value;
							        $trainingtypeid 	= 	$program->training_type_id;
							        $itemid				=	$program->item_id;
							        $class_id			=	$program->class_id;
							        $assessment_id      =$program->assessment_id;
							        $training_item_id	=	$program->training_item_id;
							        $sequence			=	$program->sequence;
							        $blened_prog_id		=	$program->blened_prog_id;
							        $blended_title		=	$program->blended_title;
							        $credit_value		=	$program->credit_value;
							        $item_name			=	$program->item_name;
							        $completion			=	$program->training_status;
							        $trainingprogramid	=	$program->training_program_id;
							        $due_date			=	@$program->due_date;
							        $registration_id    =  $program->registration_id;	
							        $is_blended			=  $program->is_blended;
									$require_supervisor_aproval_status	=$program->require_supervisor_aproval_status  ;
							        $k=	0;								
							        if($program->item_id!='')$all_itemid.=$program->item_id.',';			  
							        else $all_assessment_id.=$program->assessment_id.',';				
							        $item_name=stripcslashes($item_name);
                                    if($trainingtypeid==1){
										$itemDetailArr =  array();	
								
								        $itemDetailArr['training_type_id']			= 	$trainingtypeid;
								        $itemDetailArr['training_program_id']			= 	$trainingprogramid;
								        $itemDetailArr['blendedprogid']=$blened_prog_id;
										$itemDetailArr = CommonHelper::getMediaFileDetails($userid,$compid,$itemid,$itemDetailArr);
								        $expiration_status = ElearningMedia::checkTrainingExpiration($itemid, $userid, $compid, $trainingprogramid);
										$programs = Trainingprogram::getTrainingCatalogsElearning($compid, $trainingprogramid);
										
										$itemDetailArr['user_id'] 			= 	$userid;
								        $itemDetailArr['item_id'] 		    = 	$programs[0]->item_id;
								        $itemDetailArr['company_id'] 		= 	$programs[0]->company_id;
								        $itemDetailArr['assignment_id'] 	= 	@$programs[0]->assignment_id;
								        $itemDetailArr['training_type'] 	= 	@$programs[0]->training_type;
								        $itemDetailArr['credit_value'] 		= 	@$programs[0]->credit_value;
								        $itemDetailArr['item_image'] 		= 	$programs[0]->item_image;
								        $itemDetailArr['item_name'] 		= 	$programs[0]->item_name;
								        $itemDetailArr['learningplanid'] 	= 	@$programs[0]->learningplanid;
								        $itemDetailArr['description'] 	    = 	$programs[0]->description;
					
								       $itemDetailArr['prerequeisites'] 	= 	$programs[0]->prerequeisites;
								       $itemDetailArr['training_type_id']	= 	$programs[0]->training_type_id;
								       $itemDetailArr['training_program_id']	= 	$programs[0]->training_program_id;
								       $itemDetailArr['average_rating'] 		= 	$programs[0]->average_rating;
									   $fileDetails = $itemDetailArr['fileDetails'];
								       $userDetails = $itemDetailArr['userDetails'];
								       $username = $userDetails[0]->user_name;
								       $file_name = $fileDetails['file_name'];
								       $is_lib = $fileDetails['is_lib'];
								       $type_elearning='';
									   if($type_elearning=='view'){
									    $scorm_param =CommonHelper::getEncode($userid.'_#_'.$username.'_#_'.$itemDetailArr['item_id'].'_#_'.$itemDetailArr['file_id'].'_#_'+$compid.'_#_1');
								      }else{
									      $scorm_param =CommonHelper::getEncode($userid.'_#_'.$username.'_#_'.$itemDetailArr['item_id'].'_#_'.$itemDetailArr['file_id'].'_#_'.$compid.'_#_0');
								      }
								        $username=preg_replace("/'/", "", $username); 
								        $username=preg_replace('/"/', "", $username);
								        $scormparm = $userid.'_#_'.$username.'_#_'.$courseDetailArr['item_id'].'_#_'.$courseDetailArr['file_id'].'_#_'.$compid.'_#_0';
								
								        $courseparam =CommonHelper::getEncode($baseUrl.'/mediafiles/'.$compid.'/');
								      if($is_lib == "1"){
									    $courseparam = CommonHelper::getEncode($baseUrl.'/mediafiles/');
								      }
								         $type= "eLearning";
										 $percentage = Assignment::usertraininghistoryDB($userid,$itemid);
										 $pro_percantage 	=(count($percentage) > 0) ? $percentage[0]->course_completion_percentage : '0';
										 $status_1='<div class="meter-wrap"><div class="meter-value" style="background-image: url(\''. $baseUrl .'/public/images/prog_bg.png\');background-repeat:repeat-x; height:17px; width:'.$pro_percantage.'%;"><div class="meter-text " style="font-size: 10px;">'.$pro_percantage.'%</div></div></div>';
								         $viewId = $userid."#".$roleid."||inprogress||".$trainingprogramid."||".$blened_prog_id."||";
										 $encoded_viewId = CommonHelper::getEncode($viewId);					
								         $view='<a href="'.$baseUrl.'/#/elearningmedia/training/viewId/'. $encoded_viewId.'" class="'.$buttonclass.'">View<span>&#187;</span></a>';
										 
										 $enrollelearning = array(
									         'confirm'=>1,
									         'media_id'=>$media_id,
									         'expiration_status'=>$expiration_status,
									         'file_id'=>$courseDetailArr['file_id'],
									         'item_id'=>$courseDetailArr['item_id'],
									         'trainingprogram_id'=>$courseDetailArr['training_program_id'],
									         'userprogramtypeid'=>$courseDetailArr['training_type_id'],
									         'blendedprogid'=>$courseDetailArr['blendedprogid'],
									         'assignmentsend_id'=>($courseDetailArr['assignment_id']!='') ? $courseDetailArr['assignment_id'] : 0,
									         'view'=>$type,
									         'file_name'=>$file_name,
									         'is_lib'=>$is_lib,
									         'username'=>$username,
									         "user_id"=>$userid,
									         "company_id"=>$compid,
									         "fileDetails"=>$courseDetailArr['fileDetails'],
								           );
								
								
								          $action = '<div class="button_lblue_r4 h6"  ng-click="grid.appScope.slaunchlearning(\''.$expiration_status.'\', \''.$media_id.'\', \''.$enrollelearning_arr.'\', \''.$scorm_param.'\', \''.$courseparam.'\');">Launch</div>';
                                      							
									}else if($trainingtypeid==2 || $trainingtypeid==3){
										$get_class_data =Usertrainingcatalogs::getMyClassScheduleforBlendedDetails($compid, $userid ,$itemid);
								        $action ='';
										if(!empty($get_class_data)){									
									      if($get_class_data[0]['training_status_id']==1){
										       $status_1 ="Completed";
									       }elseif($get_class_data[0]['training_status_id']==3){
										       $status_1 ="Enrolled";
									       }elseif($get_class_data[0]['training_status_id']==4){
										         $status_1 ="Waiting List";
									       }else{
										         $status_1 ="Dropped";
									       }
								       }else{								
									         $status_1 ="Not Enrolled";
								        }
										if($get_class_data[0]['training_status_id']==1){
									         $status='';
								         }else{
											 $userenrollment =Usertrainingcatalogs::getTrainingCatalogData($itemid,$userid,$class_id,$compid);
											 if(count($userenrollment) > 0){
												 if($userenrollment[0]['is_approved']!=2){
													 if($userenrollment[0]['is_enroll']==1 && $userenrollment[0]['training_status_id']!=5)  {
												         $blended='';
												       $status ='<a ng-click="grid.appScope.openItem(2,'.$trainingprogramid.','.$blended.','.$require_supervisor_aproval_status.','.$class_id.');"  class="'.$buttonclass.' h5" >Drop </a>';
											           }elseif(@$userenrollment[0]['is_enroll']==0 && @$userenrollment[0]['training_status_id']!=4) {
												       $status ='<a ng-click="grid.appScope.openItem(1,'.$trainingprogramid.','.$blened_prog_id.','.$require_supervisor_aproval_status.','.$class_id.');"  class="'.$buttonclass.' h5" >Enroll </a>';
											         }else{
												          $status ='<a ng-click="grid.appScope.openItem(1,'.$trainingprogramid.','.$blened_prog_id.','.$require_supervisor_aproval_status.','.$class_id.');"  class="'.$buttonclass.' h5" >Enroll </a>';
											          }
												    }
											 }else{
										             $status ='<a ng-click="grid.appScope.openItem(1,'.$trainingprogramid.','.$blened_prog_id.','.$require_supervisor_aproval_status.','.$class_id.');"  class="'.$buttonclass.' h5" >Enroll </a>';
									            }
									     }

                                             $action =$status;
								             $type= "Classroom";
								             $viewId = $userid."#".$roleid."||inprogress||".$itemid."||".$class_id."||".$blened_prog_id."||";
								              $encoded_viewId = CommonHelper::getEncode($viewId);					
								              $view='<a href="'.$baseUrl.'/#/classroomcourse/training/viewId/'. $encoded_viewId.'" class="'.$buttonclass.'">View<span>&#187;</span></a>'; 										 
								     }else if($trainingtypeid==6){
										 $token='';
										 $fetchtokenid=Requirement::fetchgeneratedurl($compid,$assessment_id,'systemuser',0);
										 if(count($fetchtokenid)>0)$token=$fetchtokenid[0]->url_id;
										 $encodeduserid=Assessmentcrypt::encode($userid);
										 $url =$baseUrl.'/#/survey/viewassessment/user/'.$encodeduserid.'/token/'.$token.'/red_page/learnassessments';
                                         if($registration_id!=0 &&  $is_blended	==0) {
									         $status_1='Completed';
								         }else{
									            $status_1='Not Completed';
								         }								
								             $action='';							
								             $type= "Assessment";
								             $view='<a href="'.$url.'" class="'.$buttonclass.'" target="_blank" >View<span>&#187;</span></a>';
								             $k++;										 
									 }
									 $blended_data1 .="<tr id=item_$i>
								          <td>$item_name</td>
								            <td>$type</td>
								           <td>$credit_value</td>
								        <td>$status_1</td>
								       <td>$action</td>
								        <td>$view</td>
							         </tr>";
							         $i++;
							}
							$blended_data1 .="</table>";
						}
						 
					} 
			
					$get_all_item = rtrim($all_itemid,",");					
					$get_item_complete = $count_assess_item_complete=0;
					if($get_all_item){
						$get_all_assessment = rtrim($all_assessment_id,",");
						$get_item_complete = Usertrainingcatalogs::getCompletionOfBlended($get_all_item, $compid, $userid);
						
						if($get_all_assessment!=''){
							$get_assess_item_complete =Usertrainingcatalogs::getCompletionOfassessmentBlended($get_all_assessment, $compid, $userid);
							
							if(count($get_assess_item_complete)>0){
								foreach($get_assess_item_complete as $items_complete){
									if($items_complete->registration_id!='' || $items_complete->registration_id!='NULL'){
										$count_assess_item_complete++;
									}
								}
							}
						}
					}
					
					$percent_progress   = 0;					
					$total_percentage = count($get_item_complete)+$count_assess_item_complete;
					$percent_progress = round($total_percentage/count($blendedTrainingItems) * 100 );
					$percent_progress = ($percent_progress > 100) ? 100 : $percent_progress ;
					$subgrid_array = array();
					$subgrid_array[] = array('description'=> $blended_data1);
					$blended_data[] = $blended_data1;
					$blended_data[] = $val->title;
					$blended_data[] = $val->training_type;
					$blended_data[] = $total_credit_value;
					if($compid != 71)
					{
						$blended_data[] = '<div class="meter-wrap"><div class="meter-value" style="background-image: url(\''. $baseUrl .'/public/images/prog_bg.png\');background-repeat:repeat-x; height:17px; width:'.$percent_progress.'%;"><div class="meter-text " style="font-size: 10px;">'.$percent_progress.'%</div></div></div>';
					}
					$blended_data[] = '<div class="'.$buttonclass.' ml10 h6" ng-click="grid.appScope.deleteblended(\''. CommonHelper::getEncode($val->blended_program_id).'\',row);">Delete</div>';
					$viewId = ''.$userid.'#'.$roleid.'||inprogress||'.$val->blended_program_id.'||1';
					$encoded_viewId = CommonHelper::getEncode($viewId);						
					$blended_data[] = '<a class="change_col '.$buttonclass.'" href="' . $baseUrl . '/#/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId.'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
					$finalArray=array('item_name'=>$blended_data[1],'training_type'=>$blended_data[2],'credits'=>$blended_data[3],'status'=>$blended_data[4],'action'=>$blended_data[5],'properties'=>$blended_data[6],'subgrid'=>$subgrid_array);
				     $myenrollment_data[] = $finalArray;
					
					
				
				}
            }
			
			   
			        return response()->json($myenrollment_data);
	 }
	 
	 
	  public function deleteelearningprogressAction(Request $request){
		   $user = Auth::user();
	       $compid =  $user->company_id;
           $userid =  $user->user_id;
		   $roleid =  $user->role_id; 
		   $usertrainingprogramid = $request->input('id');
		   if(!is_numeric($usertrainingprogramid)){
			   $usertrainingprogramid = CommonHelper::getDecode($usertrainingprogramid);
		   }
		   Usertrainingcatalogs::deleteuserenrolledprograms($userid, $usertrainingprogramid);
		  
	  }
	  
	  
	  public function deletemyblendedprogramAction(Request $request){
		   $user = Auth::user();
	       $compid =  $user->company_id;
           $userid =  $user->user_id;
		   $blendedprogram_id = $request->input('id');
		   if(!is_numeric($blendedprogram_id)){ 
		   $blendedprogram_id = CommonHelper::getDecode($blendedprogram_id); 
		   }
		   Usertrainingcatalogs::dropblendeditem($blendedprogram_id,$compid);
		   $result = array("status"=>200,'message'=>'success');
		   return response()->json($result);
    }
	  
}
