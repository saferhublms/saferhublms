<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Traits\Admin;
use App\Http\Traits\Datamanagement;
use App\Models\JobTitleGroup;
use App\Models\SubOrganizationMaster;
use App\Models\DivisionMaster;
use App\Models\User;
use App\Models\SubAdminAssignedGroup;
use App\Models\GroupMaster;
use App\Models\BandLevelMaster;
use App\Models\GroupSetting;
use App\Models\UserGroup;
use CommonFunctions;
use CommonHelper;


class AdmingroupController extends Controller
{
	
    public function managegroupsnew(Request $request)
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$dataArray = array();
			$usersdetails = Admin::getgroupnames($request,$compid,$roleid,$userid);
			//print_r($usersdetails);
			//$usersdetails
			 foreach($usersdetails as $user)
			{
				$group_id =CommonFunctions::getEncode($user->group_id);
				//$group_id = $user->group_id;
				$group_name = stripslashes($user->group_name);
				$description = $user->description;
				$created_by = $user->created_by;
				
				$Edit='';
				$button = "<div class='btn btn-primary' hand ng-click=showCase.openviewpopup('$group_id','viewusers')><i class='fa fa-eye'></i> View </div>";
			
				if($created_by==$userid)
				{
					$Edit = "<div class ='btn btn-success' ng-click=showCase.openviewpopup('$group_id','editgroup') onfocus=if(this.blur)this.blur();><i class='fa fa-pencil'></i> Edit</div> <div class='btn btn-danger fs12' ng-click=showCase.deletefromassessmentmaster('$group_id','delete')>Delete<span>&#187;</span></div>";
				}
				if($roleid==1)
				{
					$Edit = "<div class ='btn btn-success' ng-click=showCase.openviewpopup('$group_id','editgroup') onfocus=if(this.blur)this.blur();><i class='fa fa-pencil'></i> Edit</div> <div class='btn btn-danger fs12' ng-click=showCase.deletefromassessmentmaster('$group_id','deletegroup')><i class='fa fa-trash'></i> Delete</div>";
				}
				$dataArray[] = array("id"=>$user->group_id,'groupName'=>$group_name,'description'=>$description,'viewbutton'=>$button,'actions'=>$Edit);
			} 
			$checkautomatedgroup = Admin::fetchgroupsetting($compid);
			$divisionvalue 		= false;
		    $locationvalue 		= false;
		    $usercompanyvalue 	= false;
		    $departmentvalue 	= false;
		    $firstwordvalue 	= false;
		    $lastwordvalue 		= false;
		    $exactwordvalue 	= false;
		    $areavalue 			= false;
			$bandvalue          = false;
		   if(count($checkautomatedgroup) > 0){
			foreach ($checkautomatedgroup as $value) 
			{
				if($value->division == 1){
					$divisionvalue = true;
				}
				
				if($value->location == 1){
					$locationvalue = true;
				}
				
				if($value->user_company_name == 1){
					$usercompanyvalue = true;
				}
				
				if($value->department == 1){
					$departmentvalue = true;
				}
				
				if($value->first_word == 1){
					$firstwordvalue = true;
				}
				
				if($value->last_word == 1){
					$lastwordvalue = true;
				}
				if($value->exact_word == 1){
					$exactwordvalue = true;
				}
				if($value->area == 1){
					$areavalue = true;
				}
				if($value->band){
					$bandvalue = true;
				}
				
			}
		   }
			$usersdetails['data'] = $dataArray;
			$usersdetails['group_setting'] = array("division"=>$divisionvalue,"location"=>$locationvalue,"user_company_name"=>$usercompanyvalue,"department"=>$departmentvalue,"first_word"=>$firstwordvalue,"last_word"=>$lastwordvalue,"exact_word"=>$exactwordvalue,"area"=>$areavalue,'band'=>$bandvalue);
			$usersdetails->draw = $request->input('draw');
			$usersdetails->total = count($dataArray);
			return response()->json($usersdetails);

	}
	
	
	public function userpop()
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		
		
		/* $layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('elight/popup_layout');
		$ses = $this->rolecomp(); 
		$compid = $ses['compid'];	
		$this->view->compid = $compid;
		$roleid = $ses ['roleid']; */
		/* if($this->getRequest()->isXmlHttpRequest())
		{ */
			/* $model = $this->_getModel(); */
			
		//	$groupid = $this->_request->getParam('groupid');
			$this->view->groupid = $groupid;
			if(!is_numeric($groupid))
			{ $groupid = CommonFunctions::getDecode($groupid); }
			$get_field_name = CommonFunctions::getFieldName($compid);
			$this->view->get_field_name = $get_field_name;			
			$fetchgroupname = $model->editgroupname($groupid, $compid);
			$groupname = $fetchgroupname[0]->group_name;
			$this->view->groupname = $groupname;
			
		/* }
		else
		{
			$layout = Zend_Layout::getMvcInstance();
			$layout->setLayout('elight/error');
			$this->render('error/404',null,true);
		} */
	}
	
	public function getuserpop($groupid)
	{
	//	echo $groupid;exit;
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
        $dataArray = array();
			//$commonfun = $this->_helper->CommonFunctions;
			if(!is_numeric($groupid))
			{ $groupid = CommonHelper::getDecode($groupid); }
		
			$fetchusers = Admin::getUsersForSelectedGroup($compid, $groupid);
			
			$responce = array(); $i=1;
			foreach($fetchusers as $val)
			{
				$temp_arr = array();				
				$temp_arr['user_name'] = stripcslashes($val->user_name);
				$temp_arr['company'] = stripcslashes($val->company);
				$temp_arr['division_name'] = stripcslashes($val->division_name);			
				$temp_arr['area'] = stripcslashes($val->area_name);
				$temp_arr['location'] = stripcslashes($val->location_name);
				$temp_arr['role_name'] = ($val->field_name!='') ? $val->field_name :$val->role_name;
				$temp_arr['delButton'] = "<div class='button_lblue_r4 hand' ng-click=showCase.deletefromusergroup('".CommonFunctions::getEncode($groupid)."','".CommonFunctions::getEncode($val->user_id)."')>Remove <span>&#187;</span></div>";
				//array_push($responce,array_values($temp_arr));
				$i++;
				
			$dataArray[] = array("user_name"=>$temp_arr['user_name'],'company'=>$temp_arr['company'],'division_name'=>$temp_arr['division_name'],'area'=>$temp_arr['area'],'location'=>$temp_arr['location'],'role_name'=>$temp_arr['role_name'],'delButton'=>$temp_arr['delButton']);
			}
			$responce['data'] = $dataArray;
			return response()->json($responce);
			//exit('{rows:'.json_encode($responce).'}');
			
	}
	
	
	public function addgroup(Request $request){
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userRole = array('1','3','5');
		if(in_array($roleid, $userRole)){
			$groupname = trim($request->input('groupname'));
			$vSomeSpecialChars = array("fwckslsh", "quesmark", "permark", "bckslsh", "plusmark"
				, "&quot;", "singleqoute", "&gt;"
				, "&lt;", "&amp;", "Ó", "Ú", "ñ", "Ñ"
				, '“', '”');
				$vReplacementChars = array("/", "?", "%", "\\", '+', '"', "'", ">", "<", "&", "O", "U", "n", "N"
					, '"', '"'); 
				$groupname = str_replace($vSomeSpecialChars, $vReplacementChars, $groupname);
				$description = $request->input('description');
				$addnewgroup = $request->input('addnewgroup');
				$checkgroupname = Admin::checkgroupname($groupname, $compid);
				if($addnewgroup != ''){
					if(count($checkgroupname) > 0){
						$check_groupactive =Admin::checkactivegroup($groupname, $compid);
						if (count($check_groupactive) > 0) {
							$result = array('Status'=>200,"Message"=>'Try with Different Group Name');
							return response()->json($result);
						} else {
							$data['is_active'] =1;
							$data['is_delete'] =0;
							$groupid =Admin::updateanywhere($groupname,$compid,$data);
							$result = array('Status'=>200,"Message"=>'Group has been created successfully.');
							return response()->json($result);
						}
					}
				}
				
				if ($addnewgroup) {
					$data['group_name'] = $groupname;
					$data['description'] = $description;
					$data['company_id'] = $compid;
					$data['created_by']= $userid;
					$data['autogroup'] = 0;
					$data['last_updated_time'] = date('Y-m-d -h-i-s');
					$last_insert_data = GroupMaster::Create($data);
					$groupid = $last_insert_data->group_id;
					if($roleid==5 && count($groupid)>0)
					{
						$data1['user_id']=$userid;
						$data1['group_id']=$groupid;
						$last_insert_data = SubAdminAssignedGroup::Create($data1);
						$groupid = $last_insert_data->group_id;
					}
					$result = array('Status'=>200,"Message"=>'Group has been created successfully.');
				    return response()->json($result);
				}
		}else{
			$result = array('Status'=>408,"Message"=>'Something Went Wrong');
				    return response()->json($result);
		}
	}
	
	
	public function editgroupdata($groupid){
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userRole = array('1','3','5');
		$fetchgroupname = array();
		if(!is_numeric($groupid)){ 
		$groupid = CommonHelper::getDecode($groupid); 
	    }
		if(in_array($roleid, $userRole)){
			$fetchgroupname = Admin::editgroupname($groupid, $compid);
		}
		return response()->json($fetchgroupname);
	}
	
	public function editgrouppop(Request $request){
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userRole = array('1','3','5'); 
		$groupid = $request->input('groupid');
		$editgroupname = $request->input('groupname');
		$description = $request->input('description');
		if(in_array($roleid, $userRole)){
			if($editgroupname != ''){
				$res = trim($editgroupname);
				if(strlen($res)>255) {
					$res = array('Status'=>200,'Message'=>"Group name should not be more than 255 character.");
					return response()->json($res);
				}
				$checkgroupname = Admin::checkGroupExceptThis($editgroupname, $groupid, $compid);
				if(count($checkgroupname) > 0)
				{
					$res = array('Status'=>200,'Message'=>"Try with another group name.");
					return response()->json($res);
				}
				else
				{
					$data['group_name'] = trim(addslashes($editgroupname));
					$data['description'] = trim(stripcslashes($description));
					$data['autogroup'] = 0;
					$data['company_id'] = $compid;
					GroupMaster::where('group_id','=',$groupid)->where('company_id','=',$compid)->update($data);
					$res = array('Status'=>200,'Message'=>"Group updated successfully.");
					return response()->json($res);
				}
				
			}else{
				$res = array('Status'=>200,'Message'=>"Group Name should not be blank");
					return response()->json($res);
			}
		}else{
			$res = array('Status'=>200,'Message'=>"Someting Went Wrong");
					return response()->json($res);
		}
		
	}
	
	public function automatedgroups(Request $request,$opt,$nochecked = ''){
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userRole = array('1','3','5'); 
		if(in_array($roleid, $userRole)){
				$msg 					= $request->input('msg');
				$edituserid 			= $request->input('userid');
				$operation 				= $opt;
				$notchecked 			= $nochecked;
				$allusers 				= Admin::getmyuseronly($compid, $userid, $roleid);
				$this->autoGroupSettingUpdate($userid, $operation, $notchecked, $compid);
			}else{
				
			}
	}
	
	
	
	public function autoGroupSettingUpdate($userid, $operation, $notchecked, $compid)
	{
		$data = array();
		if ($operation == 'fword') {
			if ($notchecked == 1) {
				$data['first_word'] = 0;
				echo "Groups will no longer be automatically created based on the First word.";
			} else {
				$data['first_word'] = 1;
				echo "Groups will be automatically created based on the First word.";
			}
		}
		if ($operation == 'lword') {
			if ($notchecked == 1) {
				$data['last_word'] = 0;
				echo "Groups will no longer be automatically created based on the  Last word.";
			} else {
				$data['last_word'] = 1;
				echo "Groups will be automatically created based on the Last word.";
			}
		}
		if ($operation == 'exact') {
			if ($notchecked == 1) {
				$data['exact_word'] = 0;
				echo "Groups will no longer be automatically created based on the Exact word.";
			} else {
				$data['exact_word'] = 1;
				echo "Groups will be automatically created based on the Exact word.";
			}
		}
		if ($operation == 'companies') {
			if ($notchecked == 1) {
				$data['user_company_name'] = 0;
				echo "Groups will no longer be automatically created based on Companies.";
			} else {
				$data['user_company_name'] = 1;
				echo "Groups will be automatically created based on Companies.";
			}
		}
		if ($operation == 'divisions') {
			if ($notchecked == 1) {
				$data['division'] = 0;
				echo "Groups will no longer be automatically created based on the Division.";
			} else {
				$data['division'] = 1;
				echo "Groups will be automatically created based on the Division.";
			}
		}
		if ($operation == 'location') {
			if ($notchecked == 1) {
				$data['location'] = 0;
				echo "Groups will no longer be automatically created based on the Location.";
			} else {
				$data['location'] = 1;
				echo "Groups will be automatically created based on the Location.";
			}
		}
		if ($operation == 'area') {
			if ($notchecked == 1) {
				$data['area'] = 0;
				echo "Groups will no longer be automatically created based on the Area.";
			} else {
				$data['area'] = 1;
				echo "Groups will be automatically created based on the Area.";
			}
		}
		if ($operation == 'band') {
			if ($notchecked == 1) {
				$data['band'] = 0;
				echo "Groups will no longer be automatically created based on the Band.";
			} else {
				$data['band'] = 1;
				echo "Groups will be automatically created based on the Band.";
			}
		}
		if ($operation == 'department') {
			if ($notchecked == 1) {
				$data['department'] = 0;
				echo "Groups will no longer be automatically created based on the Department.";
			} else {
				$data['department'] = 1;
				echo "Groups will be automatically created based on the Department.";
			}
		}
		$checksetting = Admin::fetchgroupsetting($compid);
		if (count($checksetting) > 0) {	
			$data['last_updated_date'] = date('Y-m-d -h-i-s');
			$data['last_updated_by'] = $userid;
			GroupSetting::where('company_id','=',$compid)->update($data);
		}else{
			$data['company_id'] = $compid;
			date_default_timezone_set('US/Central');
			$data['last_updated_by'] = $userid;
			$data['last_updated_date'] = date('Y-m-d -h-i-s');
			$data['created_date'] = date('Y-m-d');
			GroupSetting::insert($data);
		
		}
	}
	
	
	public function deletegroup(Request $request){
		$mode = $request->input('mode');
		$groupid = $request->input('groupid');
		$type =  $request->input('type');
		$deleteid =  $request->input('deleteid');
		$edituserid =  $request->input('userid');
		if(!is_numeric($edituserid))
		{ $edituserid = CommonHelper::getDecode($edituserid); }
		if(!is_numeric($groupid))
		{ $groupid = CommonHelper::getDecode($groupid); }
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		if($groupid != ''){
			if($mode == 'deletegroup'){
				$userdata['is_active'] = 0;
				$userdata['is_update'] = 2;
				UserGroup::where('group_id','=',$groupid)->update($userdata);
				$data['is_active'] = 0;
				$data['is_delete'] = 1;
				$data['autogroup'] = 0;
				GroupMaster::where('group_id','=',$groupid)->update($data);
				$result = array("Status"=>200,"Message"=>'Group successfully deleted.');
			}
		}else{
			$result = array("Status"=>200,"Message"=>'Please select group');
		}
		
		 return response()->json($result);
	}  
	
	
	public function bulkoptgroup(Request $request)
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userRole = array('1','3','5'); 
		if(in_array($roleid, $userRole))
		{
			$check1 = $request->input('check1');
			$mode = $request->input('mode');
			if ($mode == 'deleteselectedgroups')
				$salute = Admin::deletegroup($check1, $compid);
		   $result = array('Status'=>200,'message'=>'Success');	
		}
		else
		{
		    $result = array('Status'=>408,'message'=>'fail');	
		}
		  
		  return response()->json($result);
	}
	
	public function deleteusergroup(Request $request){
		$groupid = $request->input('groupid');
		if(!is_numeric($groupid))
		{ $groupid = CommonHelper::getDecode($groupid); }
		$user_id = $request->input('userid');
		if(!is_numeric($user_id))
		{ $user_id = CommonHelper::getDecode($user_id); }
	    $userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		if(count($user_id)>0){
			$data['is_active'] = 0;
			UserGroup::where('group_id','=',$groupid)->update($data);
			$result = array('Status'=>200,'Message'=>'User successfully deleted');
		}
		return response()->json($result);
	}
	
	public function addusertoselectedgroups(Request $request){
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userRole = array('1','3','5'); 
		if(in_array($roleid, $userRole)) {
			$checkedgroups = $request->input('check1');
			$sub_org_id 	=  CommonFunctions::fetchUserOranizationIds($userid);
			$subadminPermissionData = Admin::getsubadminpermissiondata($compid);
			if(count($subadminPermissionData)>0){
					$subadminbasedon = $subadminPermissionData[0]['user_based_on'];
					$instructorbasedon      = $subadminPermissionData[0]['user_based_on_instructor'];
				}
				else{
					$subadminbasedon = 0;
					$instructorbasedon  = 0;				
				}
			$allusers = Admin::getmyuseronly($compid,$userid,$roleid,$sub_org_id,$subadminbasedon,$instructorbasedon);	
			
			 $dataArray = array();
			$i=1;
				foreach($allusers as $val) {
				
					$temp_arr = array();
					
					$temp_arr['chbx']		=0;				
					$temp_arr['user_name'] = stripcslashes($val->user_name);
					$temp_arr['company']=stripcslashes($val->sname);
					$temp_arr['division_name'] = stripcslashes($val->division_name);			
					$temp_arr['area'] = stripcslashes($val->area_name);
					$temp_arr['location'] = stripcslashes($val->location_name);
					$temp_arr['role_name'] =($val->field_name!='') ? $val->field_name :$val->role_name;
				
				  $dataArray[] = array("id"=>$val->user_id,'user_name'=>stripcslashes($val->user_name),'company'=>stripcslashes($val->sname),'division_name'=>stripcslashes($val->division_name),'area'=>stripcslashes($val->area_name),'location'=>stripcslashes($val->location_name),'role_name'=>($val->field_name!='') ? $val->field_name :$val->role_name);	
				} 
		}
		
		return response()->json($dataArray);
	}
	
}
