<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TrainingType;
use App\Http\Traits\Traningcategory;
use App\Models\Notification;
use App\Models\SkillMaster;
use CommonHelper,Auth,CommonFunctions;

class CommonController extends Controller
{
    public function gettrainingType()
	{
		$TrainingType=TrainingType::whereisActive(1)->where('training_type_id','<>',3)->get();
		$mediatypes=CommonHelper::getmediatypes();
		return response()->json(['trainingtype'=>$TrainingType,'mediatypes'=>$mediatypes]);
	}
	
	public function getAudioVideoPageData()
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$traningcategoryType=Traningcategory::getTrainingCategory($compid,$userid,$roleid);
		$certificatename=CommonHelper::getcertificates($compid);
		$NotificationType=Notification::get()->toArray();
		$NotificationArray = array();
		if(count($NotificationType)>0)
			{
				foreach ($NotificationType as $row) 
				{
				   $NotificationArray[]=array("notification_id"=>$row['notification_id'],"notification"=>$row['notification'],'notificationType'=>$row['notification_type']);
				}
			}
		$SkillcategoryType=SkillMaster::where('is_active',1)->where('company_id',$compid)->get();	
		$Skillarray = array();
		for($i=0;$i<count($SkillcategoryType);$i++){
			$skilllevel=CommonFunctions::skilllevelmethod($compid,$SkillcategoryType[$i]->skill_id);
			$SkillcategoryType[$i]->levels=$skilllevel;
			$Skillarray[$SkillcategoryType[$i]->skill_id]=$SkillcategoryType[$i];
		}
		return response()->json(['traningcategoryType'=>$traningcategoryType,'certificatename'=>$certificatename,'notification'=>$NotificationArray,'skillarray'=>$Skillarray]);
		
	}
	
	 
	
	
	
	
	
}
