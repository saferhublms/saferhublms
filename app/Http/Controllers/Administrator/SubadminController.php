<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\UserMaster;
use App\Models\JobTitleGroup;
use App\Models\DivisionMaster;
use App\Models\User;
use App\Models\SubAdminAssignedGroup;
use App\Models\SubAdminAssignedCategory;
use App\Models\TrainingCategory;
use App\Models\GroupMaster;
use App\Models\BandLevelMaster;
use App\Models\UserJobtitles;
use CommonFunctions;
use CommonHelper;
use Auth;

class SubadminController extends Controller
{
	public function getSubAdmin($start,$length,$compid,$order,$search)
	{    
	    
		$column=$order[0]['column'];
        $dir=$order[0]['dir'];
		
	    $searchvalue=$search['value'];
		
	    if($column==2)
	    { $ordercolum='user_master.user_name';
	    }elseif($column==3)
	    {
		$ordercolum='gm.group_name';
	    }elseif($column==4)
	    {
		 $ordercolum='tc.category_name';
	    }
		else $ordercolum='user_master.user_name';
	     $limit=$length;
		 $offset=  $start;
		 $results = UserMaster::leftjoin('user_jobtitle as ujt','ujt.user_id','=','user_master.user_id')
		->leftjoin('job_title_group as jtg','jtg.job_id','=','ujt.job_id')
		->leftjoin('sub_admin_assigned_group','sub_admin_assigned_group.user_id','=','user_master.user_id')
		->leftjoin('group_master AS gm','sub_admin_assigned_group.group_id','=','gm.group_id')
		->leftjoin('sub_admin_assigned_category','sub_admin_assigned_category.user_id','=','user_master.user_id')
		->leftjoin('training_category AS tc','sub_admin_assigned_category.category_id','=','tc.training_category_id')
		->where('user_master.company_id','=',$compid)
		->where('user_master.is_active','=',1)
		->where('user_master.is_approved','=',1)
		->where('user_master.is_delete','=',0)
		->where('user_master.role_id','=',5)
		->orderBy($ordercolum, $dir);
	  
		if($searchvalue)
		{
			 $results->where(function($query) use($searchvalue)
					{
						$query->where('user_master.user_name','like',"%{$searchvalue}%")->orwhere('gm.group_name','like',"%{$searchvalue}%")->orwhere('tc.category_name','like',"%{$searchvalue}%");
					});
		}
	    $count=$results->groupBy('user_master.user_id')->select('user_master.user_id','user_master.user_name','jtg.jobtitle_group as job_title')
	     	->get();
		$allusername=$results->groupBy('user_master.user_id')->select('user_master.user_id','user_master.user_name','jtg.jobtitle_group as job_title')
		->skip($offset)->take($limit)->get();
		
		 return array("allusername"=>$allusername,"count"=>count($count)); 
	}
	public function getAssignedGroups($user_id)
	{
		 $results = SubAdminAssignedGroup::leftjoin('group_master AS gm','sub_admin_assigned_group.group_id','=','gm.group_id')
		->where('sub_admin_assigned_group.user_id','=',$user_id)
		->where('gm.is_active','=',1)
		->where('gm.is_delete','=',0)
		->where('sub_admin_assigned_group.is_active','=',1)
		->select('gm.group_name')
		->get();
		
		 return $results; 
	}
	public function getAssignedCategories($user_id)
	{
		$results = SubAdminAssignedCategory::leftjoin('training_category AS tc','sub_admin_assigned_category.category_id','=','tc.training_category_id')
		->where('sub_admin_assigned_category.user_id','=',$user_id)
		->where('tc.is_active','=',1)
		->where('tc.is_delete','=',0)
		->where('sub_admin_assigned_category.is_active','=',1)
		->select('tc.category_name')
		->get();
		 return $results; 
	}
	
   public function getsubadmindataarrAction(Request $request)
	{
		    $search = $request->input('search');
		    $group = $request->input('group');
		    $columns = $request->input('columns');
		    $order = $request->input('order');
		    $draw = $request->input('draw');
		    $name = $request->input('name');
		    $category = $request->input('category');
		    $start = $request->input('start');
		    $length = $request->input('length');
			$userid	= Auth::user()->user_id;
		    $compid	= Auth::user()->company_id;
		    $roleid	= Auth::user()->role_id;
		    $return=array();
			$results = $this->getSubAdmin($start,$length,$compid,$order,$search); 
	
			if(count($results['allusername']))
			{
					foreach($results['allusername'] as $value)
					{   $final=array();
					    $final['id']=$value->user_id;
					    $user_id=CommonHelper::encode_token($final['id']); 
					    $final['buttons']	= '<div style="padding-left:10px;" class="add-group button_lblue_r4" id="gp_'.$final['id'].'"  ng-click="showCase.addgroup(' .$final['id']. ')"  >Group<span>&#187;</span></div>
			            <div class="add-category button_lblue_r4" id="cat_'.$final['id'].'" ng-click="showCase.addcategory(' .$final['id']. ')">Category<span>&#187;</span></div>';
					    $final['user_name'] =$value->user_name;
						$group_name_array = $category_name_array = array();
						$group_name_results = $this->getAssignedGroups($value->user_id);
						if(count($group_name_results))
						{
							foreach($group_name_results as $row)
							{
								if($row->group_name!='')
								{
									$group_name_array[] = $row->group_name;
							    }
						    }
						}
						$final['groups'] = implode(", ",$group_name_array);
						$category_name_array = array();
						$category_name_results = $this->getAssignedCategories($value->user_id);
						
						if(count($category_name_results))
						{
								foreach($category_name_results as $row)
								{
									if($row->category_name != '')
									{
										$category_name_array[] = $row->category_name;
									}
								}
								
						}
						$final['categories'] = implode(", ",$category_name_array);
						
					  $return[]=$final;	
					}
					
					$success=array("status"=>200,"data"=>$return,"count"=>$results['count'],"draw"=>$draw);
					
					  return response()->json($success);
		}
		else
		{  
	            
					  $success=array("status"=>200,"data"=>$return,"count"=>$results['count'],"draw"=>$draw);
					  return response()->json($success);
		} 
	}
	
	 
	public function assignedCategoryAction(Request $request)
	{
		
		$search = $request->input('search');
		$columns = $request->input('columns');
		$order = $request->input('order');
		$draw = $request->input('draw');
		$start = $request->input('start');
		$length = $request->input('length');
		$sub_admin_id = $request->input('id');
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		
		$column=$order[0]['column'];
		$dir=$order[0]['dir'];
		
	    $searchvalue=$search['value'];
		
	    if($column==0)
	    { $ordercolum='training_category.category_name';
	    }elseif($column==1)
	    {
		$ordercolum='training_category.category_name';
	    }elseif($column==2)
	    {
		 $ordercolum='training_category.category_name';
	    }
		else $ordercolum='training_category.is_active';
	     $limit=$length;
		 $offset=  $start;
		$groups = TrainingCategory::where('training_category.is_active','=',1)
		->where('training_category.is_delete','=',0)
		->where('training_category.company_id','=',$compid)
		->where('training_category.category_name','!=','')
		->select('training_category.category_name','training_category.training_category_id', 'training_category.is_active')->orderBy($ordercolum, $dir);
		
		if($searchvalue)
		{
			 $groups->where(function($query) use($searchvalue)
					{
						$query->where('training_category.category_name','like',"%{$searchvalue}%")->orwhere('training_category.is_active','like',"%{$searchvalue}%");
					});
		}
		
		$allcount=$groups->get();
		$Allgroupdata=$groups->skip($offset)->take($limit)->get();
		
		
		$assigned_groups = CommonFunctions::getSubAdminAssignedCategory($sub_admin_id);
		
		
		$responce = array();
		$checked_arr =0;
		if(count($Allgroupdata))
		{
				foreach($Allgroupdata as $val)
				{   $checked_arr =0;
					 if(in_array($val->training_category_id, $assigned_groups))
					{
						$checked_arr= 1;
					}	 			
					$temp_arr = array();
					$temp_arr['id']		= $val->training_category_id;
					$temp_arr['checked']		= $checked_arr;
					$temp_arr['category_name']	=	stripslashes($val->category_name);
					$temp_arr['status']		=	$val->is_active==1 ? "Active" : "In-Active";
					$responce[]=$temp_arr;
					
				
				}
			    $success=array("status"=>200,"data"=>$responce,"count"=>count($allcount),"draw"=>$draw);
			    return response()->json($success);
		}
		else
		{
		 $success=array("status"=>200,"data"=>$responce,"count"=>count($allcount));
		 return response()->json($success);
		}
				
			
	
	}
	
	
	
	
	public function assignedGroupAction(Request $request)
	{
		
		$search = $request->input('search');
		$columns = $request->input('columns');
		$order = $request->input('order');
		$draw = $request->input('draw');
		$start = $request->input('start');
		$length = $request->input('length');
		$sub_admin_id = $request->input('id');
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		
		$column=$order[0]['column'];
		$dir=$order[0]['dir'];
		
	    $searchvalue=$search['value'];
		
	    if($column==0)
	    { $ordercolum='group_master.group_name';
	    }elseif($column==1)
	    {
		$ordercolum='group_master.group_name';
	    }elseif($column==2)
	    {
		 $ordercolum='group_master.group_name';
	    }
		else $ordercolum='group_master.is_active';
	     $limit=$length;
		 $offset=  $start;
		$groups = GroupMaster::where('group_master.is_active','=',1)
		->where('group_master.is_delete','=',0)
		->where('group_master.company_id','=',$compid)
		->where('group_master.group_name','!=','')
		->select('group_master.group_name','group_master.group_id', 'group_master.is_active')->orderBy($ordercolum, $dir);
		
		if($searchvalue)
		{
			 $groups->where(function($query) use($searchvalue)
					{
						$query->where('group_master.group_name','like',"%{$searchvalue}%")->orwhere('group_master.is_active','like',"%{$searchvalue}%");
					});
		}
		
		$allcount=$groups->get();
		$Allgroupdata=$groups->skip($offset)->take($limit)->get();
		
		
		$assigned_groups = CommonFunctions::getSubAdminAssignedGroups($sub_admin_id);
		$responce = array();
		$checked_arr =0;
		if(count($Allgroupdata))
		{
				foreach($Allgroupdata as $val)
				{    $checked_arr =0;
					 if(in_array($val->group_id, $assigned_groups))
					{
						$checked_arr= 1;
					}	 			
					$temp_arr = array();
					$temp_arr['id']		= $val->group_id;
					$temp_arr['checked']		= $checked_arr;
					$temp_arr['group_name']	=	stripslashes($val->group_name);
					$temp_arr['status']		=	$val->is_active==1 ? "Active" : "In-Active";
					$responce[]=$temp_arr;
					
				
				}
			    $success=array("status"=>200,"data"=>$responce,"count"=>count($allcount),"draw"=>$draw);
			    return response()->json($success);
		}
		else
		{
		 $success=array("status"=>200,"data"=>$responce,"count"=>count($allcount));
		 return response()->json($success);
		}
	}
	
	public function saveAssignedGroupAction(Request $request)
	{
			$sub_admin_id_array = $request->input('sub_admin_id');
			$selected_groups_arr = $request->input('selected_groups');
			$flag = $request->input('flag');
			
			if(count($sub_admin_id_array) > 0)
			{
				foreach($sub_admin_id_array as $sub_admin_id_value)
				{
					 if(!is_numeric($sub_admin_id_value))
					{ 
				      // $sub_admin_id_value = CommonFunctions::getDecode($sub_admin_id_value);
				    } 
				   
				     SubAdminAssignedGroup::where('user_id','=',$sub_admin_id_value)->delete();
					if(count($selected_groups_arr))
					{  foreach($selected_groups_arr as $row)
						{
							SubAdminAssignedGroup::insert(['group_id' =>$row, 'user_id' => $sub_admin_id_value,"is_active"=>1]);
						}
					}
				}
				
				$success=array("status"=>200,"data"=>"success");
		        return response()->json($success);
				
			}
			else
			{
				$success=array("status"=>200,"data"=>"success");
		        return response()->json($success);
			}
			
		
	}
	
	 public function saveAssignedCategoryAction(Request $request)
	{
			$sub_admin_id_array = $request->input('sub_admin_id');
			$selected_groups_arr = $request->input('selected_training_category_id');
		    $flag = $request->input('flag');
			if(count($sub_admin_id_array) > 0)
			{
				foreach($sub_admin_id_array as $sub_admin_id_value)
				{
					if(!is_numeric($sub_admin_id_value))
					{ 
				        //$sub_admin_id_value =  CommonFunctions::getDecode($sub_admin_id_value);
				    }
					
					SubAdminAssignedCategory::where('user_id','=',$sub_admin_id_value)->delete();
					if(count($selected_groups_arr))
					{
						foreach($selected_groups_arr as $row)
						{
							SubAdminAssignedCategory::insert(['category_id' =>$row, 'user_id' => $sub_admin_id_value,"is_active"=>1]);
						}
					}
				}
			}
		   $success=array("status"=>200,"data"=>"success");
		   return response()->json($success);
		
	} 
	
	
	
	
	
	
	
	
	
	
	
	
}
