<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\AdminTraits\Traininglibraries;
use CommonHelper;

class TraininglibraryController extends Controller
{
       public function viewLibraryNameAction()
	   {
		   $jsonTrainingLibrary = array();
		   $trainingLibrarys = Traininglibraries::getTrainingLibraryNameAndSize();
		   $counter = 1;
		   foreach($trainingLibrarys as $trainingLibrary)
		   {
				$jsonTrainingLibrary[]=array('category_name'=>isset($trainingLibrary['category_name']) ? $trainingLibrary['category_name'] : "",'trining_library'=>$trainingLibrary['training_library_name'],'course_count'=>$trainingLibrary['course_count'],'training_library_name_id'=>CommonHelper::getEncode($trainingLibrary['training_library_name_id']),'training_library_category_id'=>CommonHelper::getEncode($trainingLibrary['training_library_category_id']));
		   }
		   
		   return response()->json(['gridData'=>$jsonTrainingLibrary]);
	   }
}
