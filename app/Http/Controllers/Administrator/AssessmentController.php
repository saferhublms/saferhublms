<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,CommonHelper;
use App\Models\Course;
use App\Models\UploadedFile;
use App\Models\TrainingPrograms;
use App\Models\CourseDetail;
use App\Models\CourseSkillLevelCredit;
use App\Models\CourseCategory;
use App\Models\CourseNotification;
use ZipArchive,CommonFunctions;
use App\Repositories\ScormRepository;
use App\Repositories\AiccparserRepository;
use App\Repositories\Xml2ArrayRepository;
use App\Models\CoursePermission;
use App\Models\CourseTargetAudience;
use App\Models\CourseTag;
use App\Models\Notification;
use App\Models\GroupMaster;
use App\Models\UserMaster;
use App\Models\TrainingCategory;
use App\Models\HandoutsMaster;
use App\Models\AssessanswertypeMaster;
use App\Models\AssessQuestion;
use App\Http\Traits\Trainingprogram;
use App\Models\Assessment;
use \stdClass,DB;

class AssessmentController extends Controller
{
	
	
    public function saveAssessmentAction(Request $request)
	{
		$compid	= Auth::user()->company_id;
		$userid	= Auth::user()->user_id;
		$course=json_decode($request->input('course'),true);
		$assessment=json_decode($request->input('assessment'),true);
		$courseDetail=json_decode($request->input('courseDetail'),true);
		$trainingPrograms=json_decode($request->input('trainingPrograms'),true);
		$referenceCode=$request->input('referenceCode');
		$isCourseImage=$request->input('isCourseImage');
		$userAllSkills=json_decode($request->input('userAllSkills'));
		$categories=json_decode($request->input('categories'));
		$checkNotification=json_decode($request->input('checkNotification'));
		$questions=json_decode($request->input('question'));
		$targetAudienceGroups=$request->input('targetAudienceGroups');
		$targetAudienceusers=$request->input('targetAudienceusers');
		$permissionGroups=$request->input('permissionGroups');
		$permissionUsers=$request->input('permissionUsers');
		$courseTags=$request->input('courseTags');
		//print_r($questions);exit;
		
		if(!$referenceCode){
			$referenceCode=CommonHelper::trainingCode($request);
		}else{
			$trainingCodeExist=TrainingPrograms::where('training_program_code',$referenceCode)->first();
			if($trainingCodeExist){
					$referenceCode=CommonHelper::trainingCode($request);
			}
		}
		$trainingPrograms['training_program_code']=$referenceCode;
		$trainingPrograms['company_id']=$compid;
		$trainingPrograms['created_by']=$userid;
		$trainingPrograms['updated_by']=$userid;
		TrainingPrograms::create($trainingPrograms);
			
			$courseImage='';
		if($isCourseImage==1){
			$file=$request->file('courseImage');
			$path = 'administrator/mediafiles/'.$compid.'/';
			$pref=$compid.'_'.CommonHelper::random_string(12).'_';
			$filenameOrignal = $file->getClientOriginalName();
			$prefFilename=$pref.$filenameOrignal;
			$filename = $prefFilename;
			$filenamearray =array();
			$filenamearray =explode(".",$filename);			
			$filepos = count($filenamearray);
			
			$fileext = strtolower(end($filenamearray));
			$filname2 = str_replace(".".$fileext,"",$filename); 
			$file->move($path,$filname2.".".$fileext);
			$courseImage="/".$path.$filname2.".".$fileext;
			
		}
		
		$course['training_program_code']=$referenceCode;
		$course['course_image']=$courseImage;
		$course['company_id']=$compid;
		$course['created_by']=$userid;
		$course['updated_by']=$userid;
		$course['published_date']=date('Y-m-d H:i:s');

		$courseSave=Course::create($course);
		$courseId=$courseSave->course_id;
		
		$courseDetail['course_id']=$courseId;
		$courseDetail['lang_id']=1;
		$courseDetail['company_id']=$compid;
		$courseDetail['created_by']=$userid;
		$courseDetail['updated_by']=$userid;
		
		CourseDetail::create($courseDetail);
		
		$courseTag=new CourseTag;
		$courseTag->course_id=$courseId;
		$courseTag->tags=$courseTags;
		$courseTag->save();
		
		$assessment['company_id']=$compid;
		$assessment['course_id']=$courseId;
		$assessment['created_by']=$userid;
		$assessment['last_updated_by']=$userid;
		$assessment['created_date']=date('Y-m-d H:i:s');
		$assessment['last_updated_date']=date('Y-m-d H:i:s');
		
		Assessment::create($assessment);
		
		//Question Answers
		for($i=0;$i<count($questions);$i++){
			if($questions[$i]->answerType==1){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				$questionId=$questionData->ques_id;
				$multiChoce=$questions[$i]->multiChoice;
				for($j=0;$j<count($multiChoce);$j++){
					$answersData=new AssessanswertypeMaster;
					$answersData->question_id=$questionId;
					$answersData->description=$multiChoce[$j]->answerText;
					$answersData->is_correct=$multiChoce[$j]->correctAnswer;
					$answersData->active_status=0;
					$answersData->date_add=date('Y-m-d H:i:s');
					$answersData->date_update=date('Y-m-d H:i:s');
					$answersData->last_update_by=$userid;
					$answersData->save();
					if($multiChoce[$j]->correctAnswer==1){
						$correctAnswerId=$answersData->answer_id;
					}
				}
				$questionData=AssessQuestion::find($questionId);
				$questionData->answer=$correctAnswerId;
				$questionData->save();
				
			}else if($questions[$i]->answerType==2){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				$questionId=$questionData->ques_id;
				$multiSelect=$questions[$i]->multiSelect;
				for($j=0;$j<count($multiSelect);$j++){
					$answersData=new AssessanswertypeMaster;
					$answersData->question_id=$questionId;
					$answersData->description=$multiSelect[$j]->answerText;
					$answersData->is_correct=$multiSelect[$j]->correctAnswer;
					$answersData->active_status=0;
					$answersData->date_add=date('Y-m-d H:i:s');
					$answersData->date_update=date('Y-m-d H:i:s');
					$answersData->last_update_by=$userid;
					$answersData->save();
					if($multiSelect[$j]->correctAnswer==1){
						$correctAnswerId=$answersData->answer_id;
					}
				}
				$questionData=AssessQuestion::find($questionId);
				$questionData->answer=$correctAnswerId;
				$questionData->save();
				
			}else if($questions[$i]->answerType==3){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->answer=($questions[$i]->tf==1)?'truevalue':'falsevalue';
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				
			}else if($questions[$i]->answerType==4){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->answer=($questions[$i]->yn==1)?'yes':'no';
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				
			}else if($questions[$i]->answerType==5){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->descriptive=$questions[$i]->ak;
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
			}
		}
		
		for($i=0;$i<count($userAllSkills);$i++){
			$courseSkillCredit=new CourseSkillLevelCredit;
			$courseSkillCredit->course_id=$courseId;
			$courseSkillCredit->training_type=$trainingPrograms['training_type'];
			$courseSkillCredit->course_credit=$userAllSkills[$i]->creditValue;
			$courseSkillCredit->skill_level_credit=$userAllSkills[$i]->skillLevelCredit;
			$courseSkillCredit->level_id=$userAllSkills[$i]->courseSkillLevels;
			$courseSkillCredit->skill_id=$userAllSkills[$i]->courseSkill;
			$courseSkillCredit->company_id=$compid;
			$courseSkillCredit->created_by=$userid;
			$courseSkillCredit->updated_by=$userid;
			$courseSkillCredit->save();
		}
		
		for($i=0;$i<count($categories);$i++){
			if(is_numeric($categories[$i])){
				$category_id=$categories[$i];
			}else{
				$category_id=$this->saveTrainingCategory($categories[$i]);
			}
			
			$courseCategory=new CourseCategory;
			$courseCategory->course_id=$courseId;
			$courseCategory->training_type=$trainingPrograms['training_type'];
			$courseCategory->training_category_id=$category_id;
			$courseCategory->created_date=date("Y-m-d H:i:s");
			$courseCategory->company_id=$compid;
			$courseCategory->created_by=$userid;
			$courseCategory->updated_by=$userid;
			$courseCategory->save();
		}
        
		for($i=0;$i<count($checkNotification);$i++){
			$courseNotification=new CourseNotification;
			$courseNotification->course_id=$courseId;
			$courseNotification->notification_id=$checkNotification[$i];
			$courseNotification->is_active=1;
			$courseNotification->company_id=$compid;
			$courseNotification->created_by=$userid;
			$courseNotification->updated_by=$userid;
			$courseNotification->save();
		}
		
		
		   $coursePermission=new CoursePermission;
		   $coursePermission->course_id=$courseId;
		   $coursePermission->group_id=$permissionGroups;
		   $coursePermission->user_id=$permissionUsers;
		   $coursePermission->created_by=$userid;
		   $coursePermission->updated_by=$userid;
		   $coursePermission->save();
		   
		   $CourseTargetAudience=new CourseTargetAudience;
		   $CourseTargetAudience->course_id=$courseId;
		   $CourseTargetAudience->group_id=$targetAudienceGroups;
		   $CourseTargetAudience->user_id=$targetAudienceusers;
		   $CourseTargetAudience->created_by=$userid;
		   $CourseTargetAudience->updated_by=$userid;
		   $CourseTargetAudience->training_type=$trainingPrograms['training_type'];
		   $CourseTargetAudience->company_id=$compid;
		   $CourseTargetAudience->save();
		   
		   return response()->json(['status'=>201,'message'=>'success','trainingCode'=>$referenceCode,'courseId'=>$courseId]);
		   
	}
	
	public function getAssessmentDataAction(Request $request)
	{
		$code=$request->input('code');
		$couseTags=$groupAudienceTag=$userAudienceTag=$groupEditTag=$userEditTag=array();
		$assessmentData=Course::join('course_details','course_details.course_id','=','courses.course_id')
								->join('training_programs','training_programs.training_program_code','=','courses.training_program_code')
								->where('training_programs.training_program_code',$code)
								->select('courses.course_image','courses.credit_value','courses.points','course_details.course_name','course_details.course_title','course_details.course_description','training_programs.media_type_id','courses.course_id','courses.media_library_id','courses.certificate_template_id','training_programs.status_id','courses.point_show','courses.credit_show')
								->first();
        $assessmentData->status_id=(string)$assessmentData->status_id;
		$assessmentData->couseCategory=CourseCategory::join('training_category','training_category.training_category_id','=','course_categories.training_category_id')
						             ->where('course_categories.course_id',$assessmentData->course_id)
									 ->select('training_category.training_category_id','training_category.category_name')
									 ->get();
        $courseTag=CourseTag::where('course_id',$assessmentData->course_id)->select('course_tag_id','tags')->first();
		if($courseTag){
			$courseTagDecode=json_decode($courseTag->tags);
		}else{
			$courseTagDecode=array();
		}
		
		for($i=0;$i<count($courseTagDecode);$i++){
			$couseTags[]=array('course_tag_id'=>$assessmentData->course_id+$i,'tags'=>$courseTagDecode[$i]);
		}
		$assessmentData->courseTags=$couseTags;
   
        $notificationSet=CourseNotification::where('course_id',$assessmentData->course_id)->pluck('notification_id')->toArray();
		$NotificationType=Notification::select('notification_id','notification','notification_type as notificationType')->get();
		for($i=0;$i<count($NotificationType);$i++){
			if(in_array($NotificationType[$i]->notification_id,$notificationSet)){
				$NotificationType[$i]->checkNotification=1;
			}else{
				$NotificationType[$i]->checkNotification='';
			}
		}
		
        $courseSkills=CourseSkillLevelCredit::where('course_id',$assessmentData->course_id)->get();
		
		$assessmentData->courseSkill=$courseSkills;
		$assessmentData->notification=$NotificationType;
  
        $targetAudience=CourseTargetAudience::where('course_id',$assessmentData->course_id)->first();
		$CoursePermission=CoursePermission::where('course_id',$assessmentData->course_id)->first();
		if($targetAudience){
			$decodedGroupId=json_decode($targetAudience->group_id);
			$decodedUserId=json_decode($targetAudience->user_id);
		}else{
			$decodedGroupId=array();
			$decodedUserId=array();
		}
		
		for($i=0;$i<count($decodedGroupId);$i++){
			$groupName=GroupMaster::where('group_id',$decodedGroupId[$i])->select('group_name')->first();
			$groupAudienceTag[]=array('group_id'=>$decodedGroupId[$i],'group_name'=>$groupName->group_name);
		}
		for($i=0;$i<count($decodedUserId);$i++){
			$userName=UserMaster::where('user_id',$decodedUserId[$i])->select('user_name')->first();
			$userAudienceTag[]=array('user_id'=>$decodedUserId[$i],'user_name'=>$userName->user_name);
		}
		$assessmentData->groupAudienceTag=$groupAudienceTag;
		$assessmentData->userAudienceTag=$userAudienceTag;
		if($CoursePermission){
			$decodedGroupId=json_decode($CoursePermission->group_id);
			$decodedUserId=json_decode($CoursePermission->user_id);
		}else{
			$decodedGroupId=array();
			$decodedUserId=array();
		}
		
		for($i=0;$i<count($decodedGroupId);$i++){
			$groupName=GroupMaster::where('group_id',$decodedGroupId[$i])->select('group_name')->first();
			$groupEditTag[]=array('group_id'=>$decodedGroupId[$i],'group_name'=>$groupName->group_name);
		}
		for($i=0;$i<count($decodedUserId);$i++){
			$userName=UserMaster::where('user_id',$decodedUserId[$i])->select('user_name')->first();
			$userEditTag[]=array('user_id'=>$decodedUserId[$i],'user_name'=>$userName->user_name);
		}
		$assessmentData->groupEditTag=$groupEditTag;
		$assessmentData->userEditTag=$userEditTag;
		
		$handOuts=HandoutsMaster::where('course_id',$assessmentData->course_id)->where('status',1)->get();
		//print_r($handOuts);exit;
		$assessmentData->handouts=$handOuts;
		
		
		$assessment=Assessment::where('course_id',$assessmentData->course_id)->first();
		$timeLimitExplod=explode(":",$assessment->time_limit);
		$assessment->hours=$timeLimitExplod[0];
		$assessment->minutes=$timeLimitExplod[1];
		$assessmentData->assessmentData=$assessment;
		$assessment->allow_assessment=($assessment->allow_assessment==1)?true:false;
		$assessment->allow_transcript=($assessment->allow_transcript==1)?true:false;
		$assessment->hide_assessonrequ=($assessment->hide_assessonrequ==1)?true:false;
		
		$questions=AssessQuestion::where('course_id',$assessmentData->course_id)->get();
		$assesmentQuestionAnswer=array();
		for($i=0;$i<count($questions);$i++){
			if($questions[$i]->question_type_id==1){
				$assesmentQuestionAnswer[$i]['questionId']=$questions[$i]->ques_id;
				$assesmentQuestionAnswer[$i]['id']=$i+1;
				$assesmentQuestionAnswer[$i]['answerType']=(string)$questions[$i]->question_type_id;
				$assesmentQuestionAnswer[$i]['question']=$questions[$i]->question;
				$assesmentQuestionAnswer[$i]['randomAnswer']=($questions[$i]->random_answers==1)?true:false;
				$assesmentQuestionAnswer[$i]['multiSelect']=array();
				$answers=AssessanswertypeMaster::where('question_id',$questions[$i]->ques_id)->select('answer_id as answerId','description as answerText','is_correct as correctAnswer')->get();
				$assesmentQuestionAnswer[$i]['multiChoice']=$answers;
				
			}else if($questions[$i]->question_type_id==2){
				$assesmentQuestionAnswer[$i]['questionId']=$questions[$i]->ques_id;
				$assesmentQuestionAnswer[$i]['id']=$i+1;
				$assesmentQuestionAnswer[$i]['answerType']=(string)$questions[$i]->question_type_id;
				$assesmentQuestionAnswer[$i]['question']=$questions[$i]->question;
				$assesmentQuestionAnswer[$i]['randomAnswer']=($questions[$i]->random_answers==1)?true:false;
				$assesmentQuestionAnswer[$i]['multiChoice']=array();
				$answers=AssessanswertypeMaster::where('question_id',$questions[$i]->ques_id)->select('answer_id as answerId','description as answerText','is_correct as correctAnswer')->get();
				for($k=0;$k<count($answers);$k++){
					$answers[$k]->correctAnswer=($answers[$k]->correctAnswer==1)?true:false;
				}
				$assesmentQuestionAnswer[$i]['multiSelect']=$answers;
			}else if($questions[$i]->question_type_id==3){
				$assesmentQuestionAnswer[$i]['questionId']=$questions[$i]->ques_id;
				$assesmentQuestionAnswer[$i]['id']=$i+1;
				$assesmentQuestionAnswer[$i]['answerType']=(string)$questions[$i]->question_type_id;
				$assesmentQuestionAnswer[$i]['question']=$questions[$i]->question;
				$assesmentQuestionAnswer[$i]['randomAnswer']=($questions[$i]->random_answers==1)?true:false;
				$assesmentQuestionAnswer[$i]['tf']=($questions[$i]->answer=='falsevalue')?0:1;
				$assesmentQuestionAnswer[$i]['multiChoice']=array();
				$assesmentQuestionAnswer[$i]['multiSelect']=array();
			}else if($questions[$i]->question_type_id==4){
				$assesmentQuestionAnswer[$i]['questionId']=$questions[$i]->ques_id;
				$assesmentQuestionAnswer[$i]['id']=$i+1;
				$assesmentQuestionAnswer[$i]['answerType']=(string)$questions[$i]->question_type_id;
				$assesmentQuestionAnswer[$i]['question']=$questions[$i]->question;
				$assesmentQuestionAnswer[$i]['randomAnswer']=($questions[$i]->random_answers==1)?true:false;
				$assesmentQuestionAnswer[$i]['yn']=($questions[$i]->answer=='no')?0:1;
				$assesmentQuestionAnswer[$i]['multiChoice']=array();
				$assesmentQuestionAnswer[$i]['multiSelect']=array();
			}else if($questions[$i]->question_type_id==5){
				$assesmentQuestionAnswer[$i]['questionId']=$questions[$i]->ques_id;
				$assesmentQuestionAnswer[$i]['id']=$i+1;
				$assesmentQuestionAnswer[$i]['answerType']=(string)$questions[$i]->question_type_id;
				$assesmentQuestionAnswer[$i]['question']=$questions[$i]->question;
				$assesmentQuestionAnswer[$i]['randomAnswer']=($questions[$i]->random_answers==1)?true:false;
				$assesmentQuestionAnswer[$i]['ak']=$questions[$i]->descriptive;
				$assesmentQuestionAnswer[$i]['multiChoice']=array();
				$assesmentQuestionAnswer[$i]['multiSelect']=array();
			}
		}
		$assessmentData->questionAnswer=$assesmentQuestionAnswer;

        return response()->json(['status'=>201,'data'=>$assessmentData]);		
		
	 
	}
	
	public function editAssessmentAction(Request $request)
	{
		$newCourseTage=$audienceGroupArray=$audienceUserArray=$preminssionGroup=$permissionUser=array();
		$compid	= Auth::user()->company_id;
		$userid	= Auth::user()->user_id;
		$code=$request->input('code');
		
		$course=json_decode($request->input('course'),true);
		$courseDetail=json_decode($request->input('courseDetail'),true);
		$trainingPrograms=json_decode($request->input('trainingPrograms'),true);
		$isCourseImage=$request->input('isCourseImage');
		$userAllSkills=json_decode($request->input('userAllSkills'));
		$categories=json_decode($request->input('categories'));
		$checkNotification=json_decode($request->input('checkNotification'));
		$targetAudienceGroups=json_decode($request->input('targetAudienceGroups'));
		$targetAudienceusers=json_decode($request->input('targetAudienceusers'));
		$permissionGroups=json_decode($request->input('permissionGroups'));
		$permissionUsers=json_decode($request->input('permissionUsers'));
		$courseTags=json_decode($request->input('courseTags'),true);
		$assessment=json_decode($request->input('assessment'),true);
		$questions=json_decode($request->input('question'));
		
		$courseDetails=Course::where('training_program_code',$code)->first();
		$courseImage=$courseDetails->course_image;
		if($isCourseImage==1){
			$file=$request->file('courseImage');
			$path = 'administrator/mediafiles/'.$compid.'/';
			$pref=$compid.'_'.CommonHelper::random_string(12).'_';
			$filenameOrignal = $file->getClientOriginalName();
			$prefFilename=$pref.$filenameOrignal;
			$filename = $prefFilename;
			$filenamearray =array();
			$filenamearray =explode(".",$filename);			
			$filepos = count($filenamearray);
			$fileext = strtolower(end($filenamearray));
			$filname2 = str_replace(".".$fileext,"",$filename); 
			$file->move($path,$filname2.".".$fileext);
			$courseImage="/".$path.$filname2.".".$fileext;
			
		}
		$course['course_image']=$courseImage;
		$course['updated_by']=$userid;
		$courseDetail['updated_by']=$userid;
		$trainingPrograms['updated_by']=$userid;
		$courseId=$courseDetails->course_id;
		Course::where('training_program_code',$code)->update($course);
		CourseDetail::where('course_id',$courseId)->update($courseDetail);
		TrainingPrograms::where('training_program_code',$code)->update($trainingPrograms);
		
		CourseSkillLevelCredit::where('course_id',$courseId)->delete();
		for($i=0;$i<count($userAllSkills);$i++){
			$courseSkillCredit=new CourseSkillLevelCredit;
			$courseSkillCredit->course_id=$courseId;
			$courseSkillCredit->training_type=1;
			$courseSkillCredit->course_credit=$userAllSkills[$i]->course_credit;
			$courseSkillCredit->skill_level_credit=$userAllSkills[$i]->skill_level_credit;
			$courseSkillCredit->level_id=$userAllSkills[$i]->level_id;
			$courseSkillCredit->skill_id=$userAllSkills[$i]->skill_id;
			$courseSkillCredit->company_id=$compid;
			$courseSkillCredit->created_by=$userid;
			$courseSkillCredit->updated_by=$userid;
			$courseSkillCredit->save();
		}
		CourseCategory::where('course_id',$courseId)->delete();
		for($i=0;$i<count($categories);$i++){
			$categorySelectId=isset($categories[$i]->training_category_id)?$categories[$i]->training_category_id:$categories[$i];
			$courseCategory=new CourseCategory;
			$courseCategory->course_id=$courseId;
			$courseCategory->training_type=1;
			$courseCategory->training_category_id=$categorySelectId;
			$courseCategory->created_date=date("Y-m-d H:i:s");
			$courseCategory->company_id=$compid;
			$courseCategory->created_by=$userid;
			$courseCategory->updated_by=$userid;
			$courseCategory->save();
			
		}
        CourseNotification::where('course_id',$courseId)->delete();
		for($i=0;$i<count($checkNotification);$i++){
			$courseNotification=new CourseNotification;
			$courseNotification->course_id=$courseId;
			$courseNotification->notification_id=$checkNotification[$i];
			$courseNotification->is_active=1;
			$courseNotification->company_id=$compid;
			$courseNotification->created_by=$userid;
			$courseNotification->updated_by=$userid;
			$courseNotification->save();
		}
		    $courseExistingTags=CourseTag::where('course_id',$courseId)->select('tags')->first();
			 if($courseExistingTags){
				$courseExistingTagsDecode=json_decode($courseExistingTags->tags); 
			 }else{
				 $courseExistingTagsDecode=array();
			 }
			  CourseTag::where('course_id',$courseId)->delete();
			
			   for($i=0;$i<count($courseTags);$i++){
				   if(isset($courseTags[$i]['course_tag_id'])){
					   $tag=$courseTags[$i]['tags'];
				   }
				   
				   else{
					   if(is_numeric($courseTags[$i])){
						  $tag= $courseExistingTagsDecode[$courseTags[$i]-$courseId];
					   }else{
						  $tag= $courseTags[$i];
					   }
				   }
				   $newCourseTage[]=$tag;
				   
				   
			   }
		   $courseTag=new CourseTag;
		   $courseTag->course_id=$courseId;
		   $courseTag->tags=json_encode($newCourseTage);
		   $courseTag->save();
		 
		 
		   CourseTargetAudience::where('course_id',$courseId)->delete();
		   
		   for($j=0;$j<count($targetAudienceGroups);$j++){
			$groupSelectId=isset($targetAudienceGroups[$j]->group_id)?$targetAudienceGroups[$j]->group_id:$targetAudienceGroups[$j];
				$audienceGroupArray[]=$groupSelectId;
			}
			for($j=0;$j<count($targetAudienceusers);$j++){
				$userSelectId=isset($targetAudienceusers[$j]->user_id)?$targetAudienceusers[$j]->user_id:$targetAudienceusers[$j];
				$audienceUserArray[]=$userSelectId;
			}
		    
		   $CourseTargetAudience=new CourseTargetAudience;
		   $CourseTargetAudience->course_id=$courseId;
		   $CourseTargetAudience->group_id=json_encode($audienceGroupArray);
		   $CourseTargetAudience->user_id=json_encode($audienceUserArray);
		   $CourseTargetAudience->created_by=$userid;
		   $CourseTargetAudience->updated_by=$userid;
		   $CourseTargetAudience->training_type=1;
		   $CourseTargetAudience->company_id=$compid;
		   $CourseTargetAudience->save();
		   
		   CoursePermission::where('course_id',$courseId)->delete();
		   
		   for($j=0;$j<count($permissionGroups);$j++){
			$groupSelectId=isset($permissionGroups[$j]->group_id)?$permissionGroups[$j]->group_id:$permissionGroups[$j];
				$preminssionGroup[]=$groupSelectId;
			}
			for($j=0;$j<count($permissionUsers);$j++){
				$userSelectId=isset($permissionUsers[$j]->user_id)?$permissionUsers[$j]->user_id:$permissionUsers[$j];
				$permissionUser[]=$userSelectId;
			}
			
		
		   $coursePermission=new CoursePermission;
		   $coursePermission->course_id=$courseId;
		   $coursePermission->group_id=json_encode($preminssionGroup);
		   $coursePermission->user_id=json_encode($permissionUser);
		   $coursePermission->created_by=$userid;
		   $coursePermission->updated_by=$userid;
		   $coursePermission->save();
		   
		   
		   Assessment::where('course_id',$courseId)->update($assessment);
		  AssessQuestion::where('course_id',$courseId)->delete();
			//Question Answers
		  for($i=0;$i<count($questions);$i++){
			if($questions[$i]->answerType==1){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				$questionId=$questionData->ques_id;
				$multiChoce=$questions[$i]->multiChoice;
				for($j=0;$j<count($multiChoce);$j++){
					$answersData=new AssessanswertypeMaster;
					$answersData->question_id=$questionId;
					$answersData->description=$multiChoce[$j]->answerText;
					$answersData->is_correct=$multiChoce[$j]->correctAnswer;
					$answersData->active_status=0;
					$answersData->date_add=date('Y-m-d H:i:s');
					$answersData->date_update=date('Y-m-d H:i:s');
					$answersData->last_update_by=$userid;
					$answersData->save();
					if($multiChoce[$j]->correctAnswer==1){
						$correctAnswerId=$answersData->answer_id;
					}
				}
				$questionData=AssessQuestion::find($questionId);
				$questionData->answer=$correctAnswerId;
				$questionData->save();
				
			}else if($questions[$i]->answerType==2){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				$questionId=$questionData->ques_id;
				$multiSelect=$questions[$i]->multiSelect;
				for($j=0;$j<count($multiSelect);$j++){
					$answersData=new AssessanswertypeMaster;
					$answersData->question_id=$questionId;
					$answersData->description=$multiSelect[$j]->answerText;
					$answersData->is_correct=$multiSelect[$j]->correctAnswer;
					$answersData->active_status=0;
					$answersData->date_add=date('Y-m-d H:i:s');
					$answersData->date_update=date('Y-m-d H:i:s');
					$answersData->last_update_by=$userid;
					$answersData->save();
					if($multiSelect[$j]->correctAnswer==1){
						$correctAnswerId=$answersData->answer_id;
					}
				}
				$questionData=AssessQuestion::find($questionId);
				$questionData->answer=$correctAnswerId;
				$questionData->save();
				
			}else if($questions[$i]->answerType==3){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->answer=($questions[$i]->tf==1)?'truevalue':'falsevalue';
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				
			}else if($questions[$i]->answerType==4){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->answer=($questions[$i]->yn==1)?'yes':'no';
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
				
			}else if($questions[$i]->answerType==5){
				$questionData=new AssessQuestion;
				$questionData->course_id=$courseId;
				$questionData->question_type_id=$questions[$i]->answerType;
				$questionData->ques_no=$i+1;
				$questionData->descriptive=$questions[$i]->ak;
				$questionData->question=$questions[$i]->question;
				$questionData->random_answers=$questions[$i]->randomAnswer;
				$questionData->last_updated_by=$userid;
				$questionData->created_date=date('Y-m-d H:i:s');
				$questionData->last_updated_date=date('Y-m-d H:i:s');
				$questionData->save();
			}
		}
		   
		   return response()->json(['status'=>201,'message'=>'success','courseId'=>$courseId]);
		   
		   
		   
		   
		
	}
}
