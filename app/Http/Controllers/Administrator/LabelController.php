<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use CommonHelper;
use App\Models\PageLabel;

class LabelController extends Controller
{
    public function getlabels()
	{
		 $language=array('hn','en','ar');
		 $returnArray=array();
	     $pageList[]=array('pagename'=>'training catalog');
	     $pageList[]=array('pagename'=>'my requirement');
	     $pageList[]=array('pagename'=>'transcript');
		
		 /* for($i=0;$i<count($pageList);$i++){
			 if($pageList[$i]['pagename']=='training catalog'){
				 $lableArray=array('trainingcatalogtitle','category','allcategory','itemtype','startdate','creditvalue','view','itemname','advancesearch','itemtype');
				 $pageList[$i]['lables']=$lableArray;
			 }else if($pageList[$i]['pagename']=='my requirement'){
				 $lableArray=array('myrequirementtitle','coursename','trainingtype','duedate','assign','assigntype','action');
				 $pageList[$i]['lables']=$lableArray;
			 }else if($pageList[$i]['pagename']=='transcript'){
				 $lableArray=array('mytranscripttitle','enddate','status','datecompleted');
				 $pageList[$i]['lables']=$lableArray;
			 }
		 } */
		 
		 $allLabels[]=array('name'=>'trainingcatalogtitle');
		 $allLabels[]=array('name'=>'category');
		 $allLabels[]=array('name'=>'allcategory');
		 $allLabels[]=array('name'=>'itemtype');
		 $allLabels[]=array('name'=>'startdate');
		 $allLabels[]=array('name'=>'creditvalue');
		 $allLabels[]=array('name'=>'itemname');
		 $allLabels[]=array('name'=>'view');
		 $allLabels[]=array('name'=>'advancesearch');
		 $allLabels[]=array('name'=>'itemtype');
		 $allLabels[]=array('name'=>'myrequirementtitle');
		 $allLabels[]=array('name'=>'coursename');
		 $allLabels[]=array('name'=>'trainingtype');
		 $allLabels[]=array('name'=>'duedate');
		 $allLabels[]=array('name'=>'assign');
		 $allLabels[]=array('name'=>'assigntype');
		 $allLabels[]=array('name'=>'action');
		 $allLabels[]=array('name'=>'mytranscripttitle');
		 $allLabels[]=array('name'=>'enddate');
		 $allLabels[]=array('name'=>'status');
		 $allLabels[]=array('name'=>'datecompleted');
		 
		  
		  
		  for($i=0;$i<count($language);$i++){
			  for($j=0;$j<count($allLabels);$j++){
				  $pageLabelExist=PageLabel::where('language_id',$language[$i])->where('label',$allLabels[$j]['name'])->first();
				  if( $pageLabelExist){
					  $allLabels[$j]['value']=$pageLabelExist->label_value;
				  }else{
					  $allLabels[$j]['value']='';
				  }
			  }
			  
			  $returnArray[$language[$i]]=$allLabels;
		  }
		  return ( $returnArray);
	}
	
	public function saveLabel(Request $request)
	{
		$langId=$request->input('langId');
		$label=$request->input('label');
		$label=$label[$langId];
		
		for($i=0;$i<count($label);$i++){
			if(isset($label[$i]['value'])){
			$pageLabelExist=PageLabel::where('language_id',$langId)->where('label',$label[$i]['name'])->first();
			if($pageLabelExist){
				$pageLabel=PageLabel::find($pageLabelExist->page_label_id);
				$pageLabel->label_value=$label[$i]['value'];
				$pageLabel->save();
			}else{
				$pageLabel=new PageLabel;
				$pageLabel->language_id=$langId;
				$pageLabel->label=$label[$i]['name'];
				$pageLabel->label_value=$label[$i]['value'];
				$pageLabel->is_active=1;
				$pageLabel->save();
			}
		}
	 }
		return 'success';
	}
	
	
	public function getLabel(Request $request){
		//$lang=App::getLocale();
		
		$language=$request->input('language');
		if($language=='en'){
			$pageLabelExist=PageLabel::where('language_id','en')->pluck('label_value','label');
			
			$returnArray=array('trainingcatalogtitle'=>(isset($pageLabelExist['trainingcatalogtitle']))?$pageLabelExist['trainingcatalogtitle']:'Training Catalog','category'=>(isset($pageLabelExist['category']))?$pageLabelExist['category']:'Category','allcategory'=>(isset($pageLabelExist['allcategory']))?$pageLabelExist['allcategory']:'All Category','itemtype'=>(isset($pageLabelExist['itemtype']))?$pageLabelExist['itemtype']:'Item Type',"startdate"=>(isset($pageLabelExist['startdate']))?$pageLabelExist['startdate']:"Start Date","creditvalue"=>(isset($pageLabelExist['creditvalue']))?$pageLabelExist['creditvalue']:"Credit Value","view"=>(isset($pageLabelExist['view']))?$pageLabelExist['view']:"View","itemname"=>(isset($pageLabelExist['itemname']))?$pageLabelExist['itemname']:"Item Name","advancesearch"=>(isset($pageLabelExist['advancesearch']))?$pageLabelExist['advancesearch']:"Advance Search","myrequirementtitle"=>(isset($pageLabelExist['myrequirementtitle']))?$pageLabelExist['myrequirementtitle']:"My Requirement","coursename"=>(isset($pageLabelExist['coursename']))?$pageLabelExist['coursename']:"Course Name","trainingtype"=>(isset($pageLabelExist['trainingtype']))?$pageLabelExist['trainingtype']:"Training Type","duedate"=>(isset($pageLabelExist['duedate']))?$pageLabelExist['duedate']:"Due Date","assign"=>(isset($pageLabelExist['assign']))?$pageLabelExist['assign']:"Assigned Group/User","assigntype"=>(isset($pageLabelExist['assigntype']))?$pageLabelExist['assigntype']:"Assigned Type","action"=>(isset($pageLabelExist['action']))?$pageLabelExist['action']:"Actions",'mytranscripttitle'=>(isset($pageLabelExist['mytranscripttitle']))?$pageLabelExist['mytranscripttitle']:'My Transcript','enddate'=>(isset($pageLabelExist['enddate']))?$pageLabelExist['enddate']:'End Date','status'=>(isset($pageLabelExist['status']))?$pageLabelExist['status']:'Status','datecompleted'=>(isset($pageLabelExist['datecompleted']))?$pageLabelExist['datecompleted']:'Completed Date');
		}else if($language=='ar'){
			$pageLabelExist=PageLabel::where('language_id','ar')->pluck('label_value','label');
			
			$returnArray=array('trainingcatalogtitle'=>(isset($pageLabelExist['trainingcatalogtitle']))?$pageLabelExist['trainingcatalogtitle']:'كتالوج التدريب','category'=>(isset($pageLabelExist['category']))?$pageLabelExist['category']:'طبقة','allcategory'=>(isset($pageLabelExist['allcategory']))?$pageLabelExist['allcategory']:'كل فئة','itemtype'=>(isset($pageLabelExist['itemtype']))?$pageLabelExist['itemtype']:'كل فئة',"startdate"=>(isset($pageLabelExist['startdate']))?$pageLabelExist['startdate']:"كل فئة","creditvalue"=>(isset($pageLabelExist['creditvalue']))?$pageLabelExist['creditvalue']:"كل فئة","view"=>(isset($pageLabelExist['view']))?$pageLabelExist['view']:"لتدريب","itemname"=>(isset($pageLabelExist['itemname']))?$pageLabelExist['itemname']:"لتدريب","advancesearch"=>(isset($pageLabelExist['advancesearch']))?$pageLabelExist['advancesearch']:"البحث المتقدم","myrequirementtitle"=>(isset($pageLabelExist['myrequirementtitle']))?$pageLabelExist['myrequirementtitle']:"متطلباتي","coursename"=>(isset($pageLabelExist['coursename']))?$pageLabelExist['coursename']:"كتالوج","trainingtype"=>(isset($pageLabelExist['trainingtype']))?$pageLabelExist['trainingtype']:"كتالوج","duedate"=>(isset($pageLabelExist['duedate']))?$pageLabelExist['duedate']:"كتالوج","assign"=>(isset($pageLabelExist['assign']))?$pageLabelExist['assign']:"كتالوج","assigntype"=>(isset($pageLabelExist['assigntype']))?$pageLabelExist['assigntype']:"كتالوج","action"=>(isset($pageLabelExist['action']))?$pageLabelExist['action']:"كتالوج",'mytranscripttitle'=>(isset($pageLabelExist['mytranscripttitle']))?$pageLabelExist['mytranscripttitle']:'كتالوج فئة','enddate'=>(isset($pageLabelExist['enddate']))?$pageLabelExist['enddate']:'فئة','status'=>(isset($pageLabelExist['status']))?$pageLabelExist['status']:'كل','datecompleted'=>(isset($pageLabelExist['datecompleted']))?$pageLabelExist['datecompleted']:'كتالوج');
		}else{
			$pageLabelExist=PageLabel::where('language_id','hn')->pluck('label_value','label');
			
			$returnArray=array('trainingcatalogtitle'=>(isset($pageLabelExist['trainingcatalogtitle']))?$pageLabelExist['trainingcatalogtitle']:'प्रशिक्षण कैटलॉग','category'=>(isset($pageLabelExist['category']))?$pageLabelExist['category']:'श्रेणी','allcategory'=>(isset($pageLabelExist['allcategory']))?$pageLabelExist['allcategory']:'सभी श्रेणी','itemtype'=>(isset($pageLabelExist['itemtype']))?$pageLabelExist['itemtype']:'वस्तु प्रकार',"startdate"=>(isset($pageLabelExist['startdate']))?$pageLabelExist['startdate']:"आरंभ करने की तिथि","creditvalue"=>(isset($pageLabelExist['creditvalue']))?$pageLabelExist['creditvalue']:"क्रेडिट मूल्य","view"=>(isset($pageLabelExist['view']))?$pageLabelExist['view']:" दर्शन","itemname"=>(isset($pageLabelExist['itemname']))?$pageLabelExist['itemname']:"वस्तु का नाम","advancesearch"=>(isset($pageLabelExist['advancesearch']))?$pageLabelExist['advancesearch']:"उन्नत खोज","myrequirementtitle"=>(isset($pageLabelExist['myrequirementtitle']))?$pageLabelExist['myrequirementtitle']:"मेरी आवश्यकता","coursename"=>(isset($pageLabelExist['coursename']))?$pageLabelExist['coursename']:"कोर्स का नाम","trainingtype"=>(isset($pageLabelExist['trainingtype']))?$pageLabelExist['trainingtype']:"प्रशिक्षण का प्रकार","duedate"=>(isset($pageLabelExist['duedate']))?$pageLabelExist['duedate']:"नियत तारीख","assign"=>(isset($pageLabelExist['assign']))?$pageLabelExist['assign']:"समूह / उपयोगकर्ता असाइन ","assigntype"=>(isset($pageLabelExist['assigntype']))?$pageLabelExist['assigntype']:"असाइन किया गया प्रकार","action"=>(isset($pageLabelExist['action']))?$pageLabelExist['action']:"कार्य",'mytranscripttitle'=>(isset($pageLabelExist['mytranscripttitle']))?$pageLabelExist['mytranscripttitle']:'मेरी प्रतिलिपि','enddate'=>(isset($pageLabelExist['enddate']))?$pageLabelExist['enddate']:'अंतिम तिथि','status'=>(isset($pageLabelExist['status']))?$pageLabelExist['status']:'स्थिति','datecompleted'=>(isset($pageLabelExist['datecompleted']))?$pageLabelExist['datecompleted']:'संपन्न तिथि');
		}
		return response()->json(['data'=>$returnArray]);
		
	}
}
