<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,CommonHelper;
use App\Models\Course;
use App\Models\UploadedFile;
use App\Models\TrainingPrograms;
use App\Models\CourseDetail;
use App\Models\CourseSkillLevelCredit;
use App\Models\CourseCategory;
use App\Models\CourseNotification;
use ZipArchive,CommonFunctions;
use App\Models\CoursePermission;
use App\Models\CourseTargetAudience;
use App\Models\CourseTag;
use App\Models\Notification;
use App\Models\GroupMaster;
use App\Models\UserMaster;
use App\Models\TrainingCategory;
use App\Models\HandoutsMaster;
use App\Http\Traits\Trainingprogram;
use App\Models\CourseSetting;
use App\Models\ClassTimezones;
use App\Models\SessionScedule;
use App\Models\ClassMaster;
use App\Models\ClassSchedule;
use \stdClass,DB;

class ClassroomController extends Controller
{
	
	
	public function getClassRoomCourseData()
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$allgroup = CommonFunctions::getallgroup($compid,$userid,$roleid);	
		$allusers = CommonFunctions::getallusers($compid);	
        $returnArray=array();
		
		return response()->json(['allgroup'=>$allgroup,'allusers'=>$allusers]);
	}
	
	public function saveClassroomCourseAction(Request $request)
	{
		$compid	= Auth::user()->company_id;
		$userid	= Auth::user()->user_id;
		$course=json_decode($request->input('course'),true);
		$courseDetail=json_decode($request->input('courseDetail'),true);
		$referenceCode=$request->input('referenceCode');
		$isCourseImage=$request->input('isCourseImage');
		$userAllSkills=json_decode($request->input('userAllSkills'));
		$categories=json_decode($request->input('categories'));
		$checkNotification=json_decode($request->input('checkNotification'));
		$courseSetting=json_decode($request->input('courseSetting'),true);
		$courseTags=$request->input('courseTags');
		$targetAudienceGroups=$request->input('targetAudienceGroups');
		$targetAudienceusers=$request->input('targetAudienceusers');
		$permissionGroups=$request->input('permissionGroups');
		$permissionUsers=$request->input('permissionUsers');
		$trainingPrograms=json_decode($request->input('trainingPrograms'),true);
		//print_r($categories);exit;
		if(!$referenceCode){
			$referenceCode=CommonHelper::trainingCode($request);
		}else{
			$trainingCodeExist=TrainingPrograms::where('training_program_code',$referenceCode)->first();
			if($trainingCodeExist){
					$referenceCode=CommonHelper::trainingCode($request);
			}
		}
		
		$trainingPrograms['training_program_code']=$referenceCode;
		$trainingPrograms['company_id']=$compid;
		$trainingPrograms['created_by']=$userid;
		$trainingPrograms['updated_by']=$userid;
		TrainingPrograms::create($trainingPrograms);
		
		$courseImage='';
		if($isCourseImage==1){
			$file=$request->file('courseImage');
			$path = 'administrator/mediafiles/'.$compid.'/';
			$pref=$compid.'_'.CommonHelper::random_string(12).'_';
			$filenameOrignal = $file->getClientOriginalName();
			$prefFilename=$pref.$filenameOrignal;
			$filename = $prefFilename;
			$filenamearray =array();
			$filenamearray =explode(".",$filename);			
			$filepos = count($filenamearray);	
			$fileext = strtolower(end($filenamearray));
			$filname2 = str_replace(".".$fileext,"",$filename); 
			$file->move($path,$filname2.".".$fileext);
			$courseImage="/".$path.$filname2.".".$fileext;
		}
		
			$course['training_program_code']=$referenceCode;
			$course['course_image']=$courseImage;
			$course['company_id']=$compid;
			$course['created_by']=$userid;
			$course['updated_by']=$userid;
			$course['published_date']=date('Y-m-d H:i:s');

			$courseSave=Course::create($course);
			$courseId=$courseSave->course_id;
			
			$courseDetail['course_id']=$courseId;
		    $courseDetail['lang_id']=1;
		    $courseDetail['company_id']=$compid;
		    $courseDetail['created_by']=$userid;
		    $courseDetail['updated_by']=$userid;
		    CourseDetail::create($courseDetail);
			
			$courseSetting['course_id']=$courseId;
			$courseSetting['company_id']=$compid;
			CourseSetting::create($courseSetting);
			
			for($i=0;$i<count($userAllSkills);$i++){
			if($userAllSkills[$i]->courseSkill){
			$courseSkillCredit=new CourseSkillLevelCredit;
			$courseSkillCredit->course_id=$courseId;
			$courseSkillCredit->training_type=$trainingPrograms['training_type'];
			$courseSkillCredit->course_credit=$userAllSkills[$i]->creditValue;
			$courseSkillCredit->skill_level_credit=$userAllSkills[$i]->skillLevelCredit;
			$courseSkillCredit->level_id=$userAllSkills[$i]->courseSkillLevels;
			$courseSkillCredit->skill_id=$userAllSkills[$i]->courseSkill;
			$courseSkillCredit->company_id=$compid;
			$courseSkillCredit->created_by=$userid;
			$courseSkillCredit->updated_by=$userid;
			$courseSkillCredit->save();
			}
		}
			
			for($i=0;$i<count($categories);$i++){
			
			if(is_numeric($categories[$i])){
				$category_id=$categories[$i];
			}else{
				$category_id=$this->saveTrainingCategory($categories[$i]);
				
			}
			
			$courseCategory=new CourseCategory;
			$courseCategory->course_id=$courseId;
			$courseCategory->training_type=$trainingPrograms['training_type'];
			$courseCategory->training_category_id=$category_id;
			$courseCategory->created_date=date("Y-m-d H:i:s");
			$courseCategory->company_id=$compid;
			$courseCategory->created_by=$userid;
			$courseCategory->updated_by=$userid;
			$courseCategory->save();
			
		}
        
		for($i=0;$i<count($checkNotification);$i++){
			$courseNotification=new CourseNotification;
			$courseNotification->course_id=$courseId;
			$courseNotification->notification_id=$checkNotification[$i];
			$courseNotification->is_active=1;
			$courseNotification->company_id=$compid;
			$courseNotification->created_by=$userid;
			$courseNotification->updated_by=$userid;
			$courseNotification->save();
		}
		
		
		   $coursePermission=new CoursePermission;
		   $coursePermission->course_id=$courseId;
		   $coursePermission->group_id=$permissionGroups;
		   $coursePermission->user_id=$permissionUsers;
		   $coursePermission->created_by=$userid;
		   $coursePermission->updated_by=$userid;
		   $coursePermission->save();
		   
		   $CourseTargetAudience=new CourseTargetAudience;
		   $CourseTargetAudience->course_id=$courseId;
		   $CourseTargetAudience->group_id=$targetAudienceGroups;
		   $CourseTargetAudience->user_id=$targetAudienceusers;
		   $CourseTargetAudience->created_by=$userid;
		   $CourseTargetAudience->updated_by=$userid;
		   $CourseTargetAudience->training_type=$trainingPrograms['training_type'];
		   $CourseTargetAudience->company_id=$compid;
		   $CourseTargetAudience->save();
		   
		   return response()->json(['status'=>201,'message'=>'success','trainingCode'=>$referenceCode,'courseId'=>$courseId]);
	}
	
	
	
    public function getClassroomdataAction(Request $request)
	{
		$couseTags=$groupAudienceTag=$userAudienceTag=$groupEditTag=$userEditTag=array();
		$code=$request->input('code');
		
		$elearningData=Course::join('course_details','course_details.course_id','=','courses.course_id')
								->join('training_programs','training_programs.training_program_code','=','courses.training_program_code')
								->where('training_programs.training_program_code',$code)
								->select('courses.course_image','courses.credit_value','courses.points','course_details.course_name','course_details.course_title','course_details.course_description','training_programs.media_type_id','courses.course_id','courses.media_library_id','courses.certificate_template_id','training_programs.status_id','courses.point_show','courses.credit_show')
								->first();
								
		$couseSetting=CourseSetting::where('course_id',$elearningData->course_id)->first();	
		$couseSetting->required_approval=(string)$couseSetting->required_approval;
		$couseSetting->enrollment_type=(string)$couseSetting->enrollment_type;
		$couseSetting->is_unenrollment=($couseSetting->is_unenrollment==1)?true:false;
        $elearningData->CourseSetting=$couseSetting;		
		$elearningData->status_id=(string)$elearningData->status_id;
        $elearningData->couseCategory=CourseCategory::join('training_category','training_category.training_category_id','=','course_categories.training_category_id')
						             ->where('course_categories.course_id',$elearningData->course_id)
									 ->select('training_category.training_category_id','training_category.category_name')
									 ->get();
		                          
        $courseTag=CourseTag::where('course_id',$elearningData->course_id)->select('course_tag_id','tags')->first();
		if($courseTag){
			$courseTagDecode=json_decode($courseTag->tags);
		}else{
			$courseTagDecode=array();
		}
		
		for($i=0;$i<count($courseTagDecode);$i++){
			$couseTags[]=array('course_tag_id'=>$elearningData->course_id+$i,'tags'=>$courseTagDecode[$i]);
		}
		$elearningData->courseTags=$couseTags;
		
		$elearningData->media=UploadedFile::where('files_id',$elearningData->media_library_id)->first();
		
		$notificationSet=CourseNotification::where('course_id',$elearningData->course_id)->pluck('notification_id')->toArray();
		$NotificationType=Notification::select('notification_id','notification','notification_type as notificationType')->get();
		for($i=0;$i<count($NotificationType);$i++){
			if(in_array($NotificationType[$i]->notification_id,$notificationSet)){
				$NotificationType[$i]->checkNotification=1;
			}else{
				$NotificationType[$i]->checkNotification='';
			}
		}
		
		$courseSkills=CourseSkillLevelCredit::where('course_id',$elearningData->course_id)->get();
		
		$elearningData->courseSkill=$courseSkills;
		$elearningData->notification=$NotificationType;
		
		$targetAudience=CourseTargetAudience::where('course_id',$elearningData->course_id)->first();
		$CoursePermission=CoursePermission::where('course_id',$elearningData->course_id)->first();
		if($targetAudience){
			$decodedGroupId=json_decode($targetAudience->group_id);
			$decodedUserId=json_decode($targetAudience->user_id);
		}else{
			$decodedGroupId=array();
			$decodedUserId=array();
		}
		
		
		for($i=0;$i<count($decodedGroupId);$i++){
			$groupName=GroupMaster::where('group_id',$decodedGroupId[$i])->select('group_name')->first();
			$groupAudienceTag[]=array('group_id'=>$decodedGroupId[$i],'group_name'=>$groupName->group_name);
		}
		for($i=0;$i<count($decodedUserId);$i++){
			$userName=UserMaster::where('user_id',$decodedUserId[$i])->select('user_name')->first();
			$userAudienceTag[]=array('user_id'=>$decodedUserId[$i],'user_name'=>$userName->user_name);
		}
		$elearningData->groupAudienceTag=$groupAudienceTag;
		$elearningData->userAudienceTag=$userAudienceTag;
		if($CoursePermission){
			$decodedGroupId=json_decode($CoursePermission->group_id);
			$decodedUserId=json_decode($CoursePermission->user_id);
		}else{
			$decodedGroupId=array();
			$decodedUserId=array();
		}
		
		for($i=0;$i<count($decodedGroupId);$i++){
			$groupName=GroupMaster::where('group_id',$decodedGroupId[$i])->select('group_name')->first();
			$groupEditTag[]=array('group_id'=>$decodedGroupId[$i],'group_name'=>$groupName->group_name);
		}
		for($i=0;$i<count($decodedUserId);$i++){
			$userName=UserMaster::where('user_id',$decodedUserId[$i])->select('user_name')->first();
			$userEditTag[]=array('user_id'=>$decodedUserId[$i],'user_name'=>$userName->user_name);
		}
		$elearningData->groupEditTag=$groupEditTag;
		$elearningData->userEditTag=$userEditTag;
		
		$handOuts=HandoutsMaster::where('course_id',$elearningData->course_id)->where('status',1)->get();
		//print_r($handOuts);exit;
		$elearningData->handouts=$handOuts;
		
		return response()->json(['status'=>201,'data'=>$elearningData]);
	}
	
	
	public function editClassroomCourseAction(Request $request)
	{
		$newCourseTage=$audienceGroupArray=$audienceUserArray=$preminssionGroup=$permissionUser=array();
		$compid	= Auth::user()->company_id;
		$userid	= Auth::user()->user_id;
		$code=$request->input('code');
		
		$course=json_decode($request->input('course'),true);
		$courseDetail=json_decode($request->input('courseDetail'),true);
		$trainingPrograms=json_decode($request->input('trainingPrograms'),true);
		$isCourseImage=$request->input('isCourseImage');
		$userAllSkills=json_decode($request->input('userAllSkills'));
		$categories=json_decode($request->input('categories'));
		$checkNotification=json_decode($request->input('checkNotification'));
		$targetAudienceGroups=json_decode($request->input('targetAudienceGroups'));
		$targetAudienceusers=json_decode($request->input('targetAudienceusers'));
		$permissionGroups=json_decode($request->input('permissionGroups'));
		$permissionUsers=json_decode($request->input('permissionUsers'));
		$courseTags=json_decode($request->input('courseTags'),true);
		$courseSetting=json_decode($request->input('courseSetting'),true);
		
		$courseDetails=Course::where('training_program_code',$code)->first();
		$courseImage=$courseDetails->course_image;
		if($isCourseImage==1){
			$file=$request->file('courseImage');
			$path = 'administrator/mediafiles/'.$compid.'/';
			$pref=$compid.'_'.CommonHelper::random_string(12).'_';
			$filenameOrignal = $file->getClientOriginalName();
			$prefFilename=$pref.$filenameOrignal;
			$filename = $prefFilename;
			$filenamearray =array();
			$filenamearray =explode(".",$filename);			
			$filepos = count($filenamearray);
			$fileext = strtolower(end($filenamearray));
			$filname2 = str_replace(".".$fileext,"",$filename); 
			$file->move($path,$filname2.".".$fileext);
			$courseImage="/".$path.$filname2.".".$fileext;	
		}
		$course['course_image']=$courseImage;
		$course['updated_by']=$userid;
		$courseDetail['updated_by']=$userid;
		$trainingPrograms['updated_by']=$userid;
		$courseSetting['updated_by']=$userid;
		$courseId=$courseDetails->course_id;
		Course::where('training_program_code',$code)->update($course);
		CourseDetail::where('course_id',$courseId)->update($courseDetail);
		CourseSetting::where('course_id',$courseId)->update($courseSetting);
		TrainingPrograms::where('training_program_code',$code)->update($trainingPrograms);
		
		
		
		CourseSkillLevelCredit::where('course_id',$courseId)->delete();
		for($i=0;$i<count($userAllSkills);$i++){
			$courseSkillCredit=new CourseSkillLevelCredit;
			$courseSkillCredit->course_id=$courseId;
			$courseSkillCredit->training_type=1;
			$courseSkillCredit->course_credit=$userAllSkills[$i]->course_credit;
			$courseSkillCredit->skill_level_credit=$userAllSkills[$i]->skill_level_credit;
			$courseSkillCredit->level_id=$userAllSkills[$i]->level_id;
			$courseSkillCredit->skill_id=$userAllSkills[$i]->skill_id;
			$courseSkillCredit->company_id=$compid;
			$courseSkillCredit->created_by=$userid;
			$courseSkillCredit->updated_by=$userid;
			$courseSkillCredit->save();
		}
		
		CourseCategory::where('course_id',$courseId)->delete();
		for($i=0;$i<count($categories);$i++){
			$categorySelectId=isset($categories[$i]->training_category_id)?$categories[$i]->training_category_id:$categories[$i];
			$courseCategory=new CourseCategory;
			$courseCategory->course_id=$courseId;
			$courseCategory->training_type=1;
			$courseCategory->training_category_id=$categorySelectId;
			$courseCategory->created_date=date("Y-m-d H:i:s");
			$courseCategory->company_id=$compid;
			$courseCategory->created_by=$userid;
			$courseCategory->updated_by=$userid;
			$courseCategory->save();
			
		}
        CourseNotification::where('course_id',$courseId)->delete();
		for($i=0;$i<count($checkNotification);$i++){
			$courseNotification=new CourseNotification;
			$courseNotification->course_id=$courseId;
			$courseNotification->notification_id=$checkNotification[$i];
			$courseNotification->is_active=1;
			$courseNotification->company_id=$compid;
			$courseNotification->created_by=$userid;
			$courseNotification->updated_by=$userid;
			$courseNotification->save();
		}
		    $courseExistingTags=CourseTag::where('course_id',$courseId)->select('tags')->first();
			 if($courseExistingTags){
				$courseExistingTagsDecode=json_decode($courseExistingTags->tags); 
			 }else{
				 $courseExistingTagsDecode=array();
			 }
			  CourseTag::where('course_id',$courseId)->delete();
			
			   for($i=0;$i<count($courseTags);$i++){
				   if(isset($courseTags[$i]['course_tag_id'])){
					   $tag=$courseTags[$i]['tags'];
				   }
				   
				   else{
					   if(is_numeric($courseTags[$i])){
						  $tag= $courseExistingTagsDecode[$courseTags[$i]-$courseId];
					   }else{
						  $tag= $courseTags[$i];
					   }
				   }
				   $newCourseTage[]=$tag;  
			   }
		   $courseTag=new CourseTag;
		   $courseTag->course_id=$courseId;
		   $courseTag->tags=json_encode($newCourseTage);
		   $courseTag->save();	   
		   CourseTargetAudience::where('course_id',$courseId)->delete();
		   for($j=0;$j<count($targetAudienceGroups);$j++){
			$groupSelectId=isset($targetAudienceGroups[$j]->group_id)?$targetAudienceGroups[$j]->group_id:$targetAudienceGroups[$j];
				$audienceGroupArray[]=$groupSelectId;
			}
			for($j=0;$j<count($targetAudienceusers);$j++){
				$userSelectId=isset($targetAudienceusers[$j]->user_id)?$targetAudienceusers[$j]->user_id:$targetAudienceusers[$j];
				$audienceUserArray[]=$userSelectId;
			}
		   $CourseTargetAudience=new CourseTargetAudience;
		   $CourseTargetAudience->course_id=$courseId;
		   $CourseTargetAudience->group_id=json_encode($audienceGroupArray);
		   $CourseTargetAudience->user_id=json_encode($audienceUserArray);
		   $CourseTargetAudience->created_by=$userid;
		   $CourseTargetAudience->updated_by=$userid;
		   $CourseTargetAudience->training_type=1;
		   $CourseTargetAudience->company_id=$compid;
		   $CourseTargetAudience->save();
		   CoursePermission::where('course_id',$courseId)->delete();
		   for($j=0;$j<count($permissionGroups);$j++){
			$groupSelectId=isset($permissionGroups[$j]->group_id)?$permissionGroups[$j]->group_id:$permissionGroups[$j];
				$preminssionGroup[]=$groupSelectId;
			}
			for($j=0;$j<count($permissionUsers);$j++){
				$userSelectId=isset($permissionUsers[$j]->user_id)?$permissionUsers[$j]->user_id:$permissionUsers[$j];
				$permissionUser[]=$userSelectId;
			}
			
		   $coursePermission=new CoursePermission;
		   $coursePermission->course_id=$courseId;
		   $coursePermission->group_id=json_encode($preminssionGroup);
		   $coursePermission->user_id=json_encode($permissionUser);
		   $coursePermission->created_by=$userid;
		   $coursePermission->updated_by=$userid;
		   $coursePermission->save();
		   
		    return response()->json(['status'=>201,'message'=>'success','courseId'=>$courseId]);
	}
	
	public function getAddClassdataAction()
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$instruction=CommonFunctions::getInstructorList($compid,$roleid,$userid);
		$timezones=ClassTimezones::get();
		return response()->json(['instruction'=>$instruction,'timezones'=>$timezones]);
		
	}
	
	public function addCourseClassAction(Request $request)
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$code=$request->input('code');
		$classData=json_decode($request->input('classData'),true);
		$sessionData=$request->input('sessionData');
		$sceduleArray=array();
		
		$course=Course::join('course_details','course_details.course_id','=','courses.course_id')
		              ->where('courses.training_program_code',$code)
					  ->first();
		if(!$classData['class_name'])			  {
			$classCount=ClassMaster::where('course_id',$course->course_id)->count();
			$classData['class_name']=$course->course_name." Class ".($classCount+1);
		}else{
			$classData['class_name']=$course->course_name." ".$classData['class_name'];
		}
		
       	$classData['item_type_id']=2;
       	$classData['course_id']=$course->course_id;
       	$classData['company_id']=$compid;
       	$classData['created_by']=$userid;
       	$classData['updated_by']=$userid;
       	$classData['last_modified']=date("Y-m-d H:i:s");
       	$classData['published_date']=date("Y-m-d H:i:s");
		$classDataSave=ClassMaster::create($classData);
		$classId=$classDataSave->class_id;
		
		$classScedule=new ClassSchedule;
		$classScedule->schedule_name=$course->course_name;
		$classScedule->class_id=$classId;
		$classScedule->max_seat=$classData['maximum_seat'];
		$classScedule->company_id=$compid;
		$classScedule->is_active=1;
		$classScedule->is_deleted=0;
		$classScedule->save();
		
		for($i=0;$i<count($sessionData);$i++){
			$starttime=date('H:i:s',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
			$endTime=date('H:i:s',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
			$sessionSchdule=new SessionScedule;
			$sessionSchdule->status_id=1;
			$sessionSchdule->class_id=$classId;
			$sessionSchdule->time_zone=$sessionData[$i]['timezone'];
			$sessionSchdule->dayid=date('Y-m-d',strtotime($sessionData[$i]['expdate']));
			$sessionSchdule->start_time_hr=date('H',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
			$sessionSchdule->start_time_min=date('i',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
			$sessionSchdule->end_time_hr=date('H',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
			$sessionSchdule->end_time_min=date('i',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
			$sessionSchdule->start_date=date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['expdate'])));
			$sessionSchdule->end_date=date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['eexpdate'])));
			$sessionSchdule->instructor_id=$sessionData[$i]['instructor'];
			$sessionSchdule->location=$sessionData[$i]['location'];
			$sessionSchdule->company_id=$compid;
			$sessionSchdule->created_by=$userid;
			$sessionSchdule->last_updated_by=$userid;
			$sessionSchdule->ses_length_hr=$sessionSchdule->end_time_hr-$sessionSchdule->start_time_hr;
			$sessionSchdule->ses_length_min=$sessionSchdule->end_time_min-$sessionSchdule->start_time_min;
			$sessionSchdule->save();
			$sceduleArray[]=array('schedule_id'=>$sessionSchdule->schedule_id,'send_mail_status'=>'');
			
		}
		$clsssSessions=SessionScedule::where('class_id',$classId)->orderBy('start_date')->first();
		
		$classDataSave=ClassMaster::find($classId);
		$classDataSave->session_schedule_update=json_encode($sceduleArray);
		$classDataSave->start_date_class=$clsssSessions->start_date;
		$classDataSave->save();
		return response()->json(['status'=>201,'message'=>'success','code'=>$code]);
	}
	
	
	public function getcourseClassesAction(Request $request)
	{
		$todayData=date('Y-m-d');
		$code=$request->input('code');
		$courseClassesRemain=Course::join('class_master','class_master.course_id','=','courses.course_id')
		                      ->where('class_master.status_id','<>',3)
		                      ->where('courses.training_program_code',$code)
		                      ->where('class_master.start_date_class','>=',$todayData)
							  ->get();
							  
		$courseClassesPast=Course::join('class_master','class_master.course_id','=','courses.course_id')
		                     ->where('class_master.status_id','<>',3)
		                      ->where('courses.training_program_code',$code)
		                      ->where('class_master.start_date_class','<',$todayData)
							  ->get();
							  
		return response()->json(['courseClassesRemain'=>$courseClassesRemain,'courseClassesPast'=>$courseClassesPast]);					  
	}
	
	public function getClassDataAction(Request $request)
	{
		$classId=$request->input('classId');
		$classMater=ClassMaster::where('class_id',$classId)->first();
		$classMater->delivery_type_id=(string)$classMater->delivery_type_id;
		$classMater->status_id=(string)$classMater->status_id;
		$sessionSchedule=SessionScedule::where('class_id',$classId)->get();
		$sessionScedule=array();
		for($j=0;$j<count($sessionSchedule);$j++)
		{
			$sessionScedule[]=array('sessionId'=>$j+1,'expdate'=>$sessionSchedule[$j]->start_date,'eexpdate'=>$sessionSchedule[$j]->end_date,'startTime'=>date("Y-m-d H:i:s",strtotime($sessionSchedule[$j]->start_time_hr.":".$sessionSchedule[$j]->start_time_min)),'endTime'=>date("Y-m-d H:i:s",strtotime($sessionSchedule[$j]->end_time_hr.":".$sessionSchedule[$j]->end_time_min)),'location'=>$sessionSchedule[$j]->location,'instructor_id'=>$sessionSchedule[$j]->instructor_id,'timezone'=>(int)$sessionSchedule[$j]->time_zone,'scheduleId'=>$sessionSchedule[$j]->schedule_id);
		}	
		return response()->json(['classData'=>$classMater,'sessionData'=>$sessionScedule]);
	}
	
	public function deleteScheduleAction(Request $request)
	{
		$scheduleId=$request->input('scheduleId');
		SessionScedule::where('schedule_id',$scheduleId)->delete();
		return response()->json(['status'=>201]);
	}
	
	public function editCourseClassAction(Request $request)
	{
		$userid=Auth::user()->user_id;
		$compid=Auth::user()->company_id;
		$classId=$request->input('classId');
		$classData=json_decode($request->input('classData'),true);
		$sessionData=$request->input('sessionData');
		ClassMaster::where('class_id',$classId)->update($classData);
		for($i=0;$i<count($sessionData);$i++){
			if($sessionData[$i]['scheduleId']){
				$starttime=date('H:i:s',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
				$endTime=date('H:i:s',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
				$sessionSchdule=SessionScedule::find($sessionData[$i]['scheduleId']);
				$sessionSchdule->status_id=1;
				$sessionSchdule->class_id=$classId;
				$sessionSchdule->time_zone=$sessionData[$i]['timezone'];
				$sessionSchdule->dayid=date('Y-m-d',strtotime($sessionData[$i]['expdate']));
				$sessionSchdule->start_time_hr=date('H',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
				$sessionSchdule->start_time_min=date('i',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
				$sessionSchdule->end_time_hr=date('H',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
				$sessionSchdule->end_time_min=date('i',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
				$sessionSchdule->start_date=date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['expdate'])));
				$sessionSchdule->end_date=date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['eexpdate'])));
				$sessionSchdule->instructor_id=$sessionData[$i]['instructor_id'];
				$sessionSchdule->location=$sessionData[$i]['location'];
				$sessionSchdule->company_id=$compid;
				$sessionSchdule->created_by=$userid;
				$sessionSchdule->last_updated_by=$userid;
				$sessionSchdule->save();
				$sceduleArray[]=array('schedule_id'=>$sessionData[$i]['scheduleId'],'send_mail_status'=>'');
			}else{
				$starttime=date('H:i:s',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
				$endTime=date('H:i:s',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
				$sessionSchdule=new SessionScedule;
				$sessionSchdule->status_id=1;
				$sessionSchdule->class_id=$classId;
				$sessionSchdule->time_zone=$sessionData[$i]['timezone'];
				$sessionSchdule->dayid=date('Y-m-d',strtotime($sessionData[$i]['expdate']));
				$sessionSchdule->start_time_hr=date('H',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
				$sessionSchdule->start_time_min=date('i',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['startTime'])));
				$sessionSchdule->end_time_hr=date('H',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
				$sessionSchdule->end_time_min=date('i',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['endTime'])));
				$sessionSchdule->start_date=date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['expdate'])));
				$sessionSchdule->end_date=date('Y-m-d',strtotime('+5 hour +30 minutes',strtotime($sessionData[$i]['eexpdate'])));
				$sessionSchdule->instructor_id=$sessionData[$i]['instructor_id'];
				$sessionSchdule->location=$sessionData[$i]['location'];
				$sessionSchdule->company_id=$compid;
				$sessionSchdule->created_by=$userid;
				$sessionSchdule->last_updated_by=$userid;
				$sessionSchdule->save();
				$sceduleArray[]=array('schedule_id'=>$sessionSchdule->schedule_id,'send_mail_status'=>'');
			}
			
				
			
		}
		
		$clsssSessions=SessionScedule::where('class_id',$classId)->orderBy('start_date')->first();
		$classDataSave=ClassMaster::find($classId);
		$classDataSave->session_schedule_update=json_encode($sceduleArray);
		$classDataSave->start_date_class=$clsssSessions->start_date;
		$classDataSave->save();
		return response()->json(['status'=>201,'message'=>'success']);
		
	}
}
