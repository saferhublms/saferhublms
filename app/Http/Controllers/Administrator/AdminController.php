<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Traits\Admin;
use App\Http\Traits\Datamanagement;
use App\Http\Traits\Traningcategory;
use App\Http\Traits\Loginfunction;
use App\Models\JobTitleGroup;
use App\Models\SubOrganizationMaster;
use App\Models\DivisionMaster;
use App\Models\User;
use App\Models\UserMaster;
use App\Models\SubAdminAssignedGroup;
use App\Models\GroupMaster;
use App\Models\BandLevelMaster;
use App\Models\TrainingType;
use App\Models\Notification;
use App\Models\SkillMaster;
use App\Models\InstructorMaster;
use App\Models\ClassTimezones;
use CommonFunctions,CommonHelper;
use DateTime;
use Mailnotification;

class AdminController extends Controller
{
   
	 public function adduserview(Request $request){
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		
		//field locked status..
		$LockedStatus = Datamanagement::fetchLockedstatusData($compid);
		//get job tittle;
		$alljobtitle = $this->getalljobtitle($compid);
		//get Country;
		$allcountry =Admin::getCountry();
		//get company
		$allcompany =$this->getallcompany($compid, $roleid, $userid);
		//get Division
		$alldivision = $this->getalldivision($compid);
		//get area 
		$allarea=$this->getallarea($compid);
		//get Location 
		$alllocation=$this->getalllocation($compid);
		//get supervisior 
		$supervisior = $this->getallsupervisior($compid,$userid,$roleid);	
		//get group
		$allgroup = $this->getallgroup($compid,$roleid,$userid);
		//get user fields
		$get_field_name=CommonFunctions::getFieldName($compid);
		//timezones
		$timeZones=ClassTimezones::get();
		
		
		//check field
		 $val18=$val15=$val19=$val7=$val8=$val9=$val10=$val17=0;
		 $field_18 	=Admin::checkfield(18,$compid);
		 if($field_18){
			 $val18=1;
		 }
		 $field_15= Admin::checkfield(15,$compid);
		 if($field_15){
			 $val15=1;
		 }
		 $field_19= Admin::checkfield(19,$compid);
		 if($field_19){
			 $val19=1;
		 }
		 $field_7= Admin::checkfield(7,$compid);
		 if($field_7){
			 $val7=1;
		 }
		 $field_8= Admin::checkfield(8,$compid);
		  if($field_8){
			 $val8=1;
		 }
		 $field_9= Admin::checkfield(9,$compid);
		 if($field_9){
			 $val9=1;
		 }
		 
		 $field_10= Admin::checkfield(10,$compid);
		 if($field_10){
			 $val10=1;
		 }
		 $field_17= Admin::checkfield(17,$compid);
		 if($field_17){
			 $val17=1;
		 }
		 $checkFieldArray=array('field_18'=>$val18,'field_15'=>$val15,'field_19'=>$val19,'field_7'=>$val7,'field_8'=>$val8,'field_9'=>$val9,'field_10'=>$val10,'field_17'=>$val17);
		 
		 //get  bandlevel 
		$allband= $this->getallband($compid);
		
		//getcustom field;
		$getcustomfieldnameandidbycompnyname=Admin::getcustomfieldnameandidbycompnyname($compid);
		if(count($getcustomfieldnameandidbycompnyname)>0)
		{
			$customfieldIdArr=array();
			$customfieldnameArr=array();
			$customfieldvalueArr=array();
			$k=0;
			foreach($getcustomfieldnameandidbycompnyname as $customfieldnameandid)
			{
				$getcustomfieldvalue=Admin::getcustomfieldvalue($customfieldnameandid->custom_field_id,'',$compid);
				if(count($getcustomfieldvalue)>0)
				{
					$customfieldvalueArr[$k]=$getcustomfieldvalue[0]->field_value;
				}
				$k++;
			}
		}
		$customfieldArr = $getcustomfieldnameandidbycompnyname;
		$customfieldnameArr = $customfieldnameArr;
		$customfieldvalueArr = $customfieldvalueArr;
		$strhml='';
		$strhml=$this->customfieldhtml($customfieldArr,$customfieldnameArr,$customfieldvalueArr);
		
		//get company password setting
		$PasswordValidateSetting = CommonFunctions::fetchCompPasswordsettingData($compid);
		// method for making  PasswordValidateSetting div.
		$tag_str = CommonFunctions::customlitag($PasswordValidateSetting);
		
		$alldata=array('PasswordValidateSetting'=>$PasswordValidateSetting,'tag_str'=>$tag_str,'lockedStatus'=>$LockedStatus,'jobtitle'=>$alljobtitle,'Country'=>$allcountry,'allcompany'=>$allcompany,'alldivision'=>$alldivision,'allarea'=>$allarea,'alllocation'=>$alllocation,'supervisior'=>$supervisior,'allgroup'=>$allgroup,'checkFieldArray'=>$checkFieldArray,'allband'=>$allband,'customfieldArr'=>$customfieldArr,'customfieldvalueArr'=>$customfieldvalueArr,'customfieldnameArr'=>$customfieldnameArr,'strhml'=>$strhml,'timeZones'=>$timeZones);
		return response()->json($alldata);
		
	 }
	 
	 public function customfieldhtml($customfieldArr,$customfieldnameArr,$customfieldvalueArr){
		 
		 $strhtml="";
		 $strhtml.='<input  type="hidden" name="countcustomfield" id="countcustomfield" value="'.count($customfieldnameArr).'">';
		 $strhtml.='<input type="hidden" name="customfieldcount" id="customfieldcount" value="'.count($customfieldArr).'">';
											
		$dateArr = array();
		for($i=0;$i<count($customfieldArr);$i++)
		{									
			if($customfieldArr[$i]->is_active == 1)
			{
				if($customfieldArr[$i]->column_type == 'Date') {
					$dateArr[$i] =	$i;
				}								
				    $strhtml.='<input type="hidden" name="customfieldId_'.$i.'" id="customfieldId_'.$i.'" value="'.$customfieldArr[$i]->custom_field_id.'"><div class="fl wp100" >';						 
					$colunmtype=$customfieldArr[$i]->column_type;
					if($colunmtype=='Text Area') {
							$strhtml.='<div class="manage-name fs14 fta wp100 fc03 b fl">';
							echo $customfieldArr[$i]->column_name.' :';
							if($customfieldArr[$i]->validation_required == 1) { 
								$strhtml.='<span style="color: #ff0000;font-size:18px;">*</span>';
							}
							$strhtml.='</div>';
							$strhtml.='<div class="manage-name wp100 fl mt05 ">
							<textarea class="form-control" name="customfieldvalue_'.$i.'" id="customfieldvalue_'.$i.'" value="">';
							if(!empty($customfieldvalueArr[$i]))
							    $strhtml.=$customfieldvalueArr[$i];
							$strhtml.='</textarea></div>';				
						}elseif($colunmtype=='Radio Button'){
							//

							$col_values=$customfieldArr[$i]->col_values;
							$explodevalues=explode(",",$col_values);	
							$strhtml.='<div class="manage-name fs14  mt30 fta fc03 b fl">'
							.$customfieldArr[$i]->column_name.' :';
								if($customfieldArr[$i]->validation_required == 1) 
								{ 
									$strhtml.='<span style="color: #ff0000;font-size:18px;">*</span>';
								} 
								$strhtml.='</div>';
								$strhtml.='<div class="manage-name wp100 fl mt05 bs01" style="padding:8px 0 8px 0;">
								<input type="hidden" name="customfieldvalue_'.$i.'" id="customfieldvalue_'.$i.'" data-valid_req="'.$customfieldArr[$i]->validation_required.'" data-name="'.$customfieldArr[$i]->column_name.'" value="radiobutton">';
								$checked0=$checked1='';
								if(isset($customfieldvalueArr[$i])){
									if($customfieldvalueArr[$i]==$explodevalues[0]){
										$checked0= "checked";
									}
									if($customfieldvalueArr[$i]==$explodevalues[1]){
										 $checked1="checked";
									}
								}
								
								$strhtml.='&nbsp;&nbsp;<input type="radio" name="radiobuttonvalue_'.$i.'" id="radiobuttonvalue_'.$i.'_1" value="'.$explodevalues[0].'"  '.$checked0.'>'
								.$explodevalues[0].'<input type="radio" name="radiobuttonvalue_'.$i.'" id="radiobuttonvalue_'.$i.'_2" value="'.$explodevalues[1].'"  style="margin-left:10px;"	 '.$checked1.'>'.$explodevalues[1].'</div>';	
								
							}elseif($colunmtype=='Check Box'){
									$col_values=$customfieldArr[$i]->col_values;
									$explodevalues=explode(",",$col_values);
									$strhtml.='<input type="hidden" name="customfieldvalue_'.$i.'" id="customfieldvalue_'.$i.'" data-valid_req="'.$customfieldArr[$i]->validation_required.'" data-name="'.$customfieldArr[$i]->column_name.'" value="checkbox">
									<input type="hidden" name="totalcheckbox_'.$i.'" id="totalcheckbox_'.$i.'" value="'.count($explodevalues).'">
									<div class="manage-name fs14 mt30 fta fc03 b fl">'. 
									$customfieldArr[$i]->column_name.':';
									if($customfieldArr[$i]->validation_required == 1) { 
									$strhtml.='<span style="color: #ff0000;font-size:18px;">*</span>';
									}
									$strhtml.='</div>
									<div class="manage-name wp100 fl bs01 mt05 " style="padding:8px 0 8px 0;">';		 
                                     $checked = '';
									$checkkedarray=array();
									if(@$customfieldvalueArr[$i])
									$checkkedarray=explode(",",$customfieldvalueArr[$i]);
								
									for($k=0;$k<count($explodevalues);$k++)
									{
									if(@$customfieldvalueArr[$i]){
										if(in_array($explodevalues[$k],$checkkedarray)){
											$strhtml.='<input type="checkbox" name="checkboxvalue_'.$i.'_'.$k.'" id="checkboxvalue_'.$i.'_'.$k.'"  value="'.$explodevalues[$k].'" style="margin-left:10px;" checked>'.$explodevalues[$k];
										}
										else
									    {
											$strhtml.='<input type="checkbox" name="checkboxvalue_'.$i.'_'.$k.'" id="checkboxvalue_'.$i.'_'.$k.'"  value="'.$explodevalues[$k].'"  style="margin-left:10px;" >'.$explodevalues[$k];
									    }	
									}
									else
									{
											$strhtml.='<input type="checkbox" name="checkboxvalue_'.$i.'_'.$k.'" id="checkboxvalue_'.$i.'_'.$k.'"  value="'.$explodevalues[$k].'"  style="margin-left:10px;" >'.$explodevalues[$k];
									 }			
								  
								}
								  $strhtml.='</div>';						
								}elseif($colunmtype=='Drop down'){
									$col_values=$customfieldArr[$i]->col_values;
									$explodevalues=explode(",",$col_values);
									$strhtml.='<div class="manage-name fs14 mt30 fta fc03 b fl">'.$customfieldArr[$i]->column_name.':';
									if($customfieldArr[$i]->validation_required == 1) { 
									$strhtml.='<span style="color: #ff0000;font-size:18px;">*</span>';
									} 
									$strhtml.='</div>';
									$strhtml.='<div class="manage-name wp100 mt05 fl">
											<select name="customfieldvalue_'.$i.'" id="customfieldvalue_'.$i.'" data-valid_req="'.$customfieldArr[$i]->validation_required.'" data-name="'.$customfieldArr[$i]->column_name.'" class="form-control">
											<option value="">Select</option>';
														 
											for($k=0;$k<count($explodevalues);$k++)
											{
											$selected='';
											if(@$customfieldvalueArr[$i]){ 
											if($customfieldvalueArr[$i]==$explodevalues[$k]){
											  $selected= "selected";
											  $strhtml.='<option  value="'.$explodevalues[$k].'" selected>'.$explodevalues[$k].'</option>';
											}
											else
											{
											   $strhtml.='<option   value="'.$explodevalues[$k].'">'.$explodevalues[$k].'</option>';
											}
											}
											else
											{
											   $strhtml.='<option   value="'.$explodevalues[$k].'">'.$explodevalues[$k].'</option>';
											}
											}
											$strhtml.='</select></div>';
										 }elseif($colunmtype=='Date') { 
										        $custom_field_id= $customfieldArr[$i]->custom_field_id;
												$strhtml.='<input class="form-control" type="hidden" value="" size="30" name="customfieldvalue_'.$i.'" id="customfieldvalue_'.$i.'" data-valid_req="'.$customfieldArr[$i]->validation_required.'" data-name="'.$customfieldArr[$i]->column_name.'" data-type="'.$colunmtype.'" /><div class="manage-name fs14 wp100  mt30 fta fc03 b fl">'.$customfieldArr[$i]->column_name.':';
												if($customfieldArr[$i]->validation_required == 1) {
												$strhtml.='<span style="color: #ff0000;font-size:18px;">*</span>';
												} 
												$strhtml.='</div>';
												$strhtml.='<div class="user-name wp100 fl fs14 fta fc03 b mt05">';
												$valuehtml='';
												if(!empty($customfieldvalueArr[$i]))
												{ 
												$valuehtml= $customfieldvalueArr[$i];
												}
												$strhtml.='<md-datepicker ng-model="myDate['.$custom_field_id.']" md-placeholder="Enter date"></md-datepicker>
												</div>';
									     }else{ 
											$strhtml.='<div class="manage-name wp100 fs14  mt30 fta fc03 b fl">'.$customfieldArr[$i]->column_name.':';
											if($customfieldArr[$i]->validation_required == 1) { 
											$strhtml.='<span style="color: #ff0000;font-size:18px;">*</span>';
											} 
											$strcustom='';
											if(!empty($customfieldvalueArr[$i])){
											$strcustom=$customfieldvalueArr[$i];
											}
											$strhtml.='</div>
											<div class="manage-name wp100 fl mt05">
											<input class="form-control" type="text" value="'.$strcustom.'" size="30" name="customfieldvalue_'.$i.'" id="customfieldvalue_'.$i.'" data-valid_req="'.$customfieldArr[$i]->validation_required.'" data-name="'.$customfieldArr[$i]->column_name.'" />
											</div>';
										}
												
									}
									 $strhtml.='<div ng-init="calldatefunction();">';
						}
											return $strhtml;
		 
		 
	 }
	 
	  public function getalljobtitle($compid){
		  $alljob=JobTitleGroup::where('jobtitle_group','!=','')->where('company_id','=',$compid)->where('is_active','=',1)->select('job_id','jobtitle_group')->orderby('jobtitle_group','asc')->get();
		  return $alljob;
		
	 }
	 
	public function getallcompany($compid, $roleid = 1, $userid)
	{
		
		 if($roleid==5){
			$comapnydata= SubOrganizationMaster::join('user_organization','user_organization.sub_organization_id','=','sub_organization_master.sub_organization_id')->where('sub_organization_master.sub_organization_name','!=','')->where('sub_organization_master.is_active','=',1)->where('sub_organization_master.is_delete','=',0)
			->where('sub_organization_master.company_id','=',$compid)->where('user_organization.is_active','=',1)
			->where('user_organization.user_id','=',$userid)->distinct()
			->select('sub_organization_master.sub_organization_id','sub_organization_master.sub_organization_name')->orderby('sub_organization_master.sub_organization_name','asc')->get();
		
		 }else{
			 $comapnydata=SubOrganizationMaster::where('sub_organization_name','!=','')->where('is_active','=',1)->where('is_delete','=',0)
			 ->where('company_id','=',$compid)->
			 select('sub_organization_id','sub_organization_name')->orderby('sub_organization_name','asc')->get();
			 
		 }
		 return $comapnydata;
	}
	
	public function getalldivision($compid)
	{
		$division_arr=DivisionMaster::where('division_name','!=','')->where('company_id','=', $compid)
		->where('is_active', '=', 1)
		->where('is_delete','=',0)
		->orderby('division_name','asc')
		->select('division_id','division_name')->get();
		return $division_arr;
	}
	 
	public function getallarea($compid)
	{
		$allarea = Admin::getallareadata($compid);
		return $allarea;
	}
	
	public function getalllocation($compid)
	{
		
		$alllocation =  Admin::getalllocationdata($compid);
		return $alllocation;
	}
	
	public function getallsupervisior($compid,$userid,$roleid)
	{
		
		if($roleid==5 || $roleid==3)
		{
			$subadmingrp=SubAdminAssignedGroup::where('user_id','=',$userid)->where('is_active','=',1)->pluck('group_id');
			$role_id = array(2,3,5); 
			$supervisor_arr=User::join('user_group','user_group.user_id','=','user_master.user_id')
			->join('group_master','group_master.group_id','=','group_master.group_id')
			->where('user_master.company_id','=',$compid)->where('user_master.is_active','=',1)->where('user_master.is_delete','=',0)->whereIn('user_master.role_id',$role_id)->where('group_master.is_active','=',1)->where('group_master.is_delete','=',0)->where('group_master.company_id','=',$compid)->where('user_group.is_active','=',1)->whereIn('user_group.group_id',$subadmingrp)->groupby('user_master.user_id')
			->orderby('user_master.user_name','asc')->select('user_master.user_id','user_master.user_name')->get();
	
		}
		else
		{
			$role_id = array(1,2,3,5); 
			$supervisor_arr=User::where('company_id','=',$compid)->where('is_active','=',1)->where('is_delete','=',0)->whereIn('role_id',$role_id)->orderby('user_master.user_name','asc')->select('user_id','user_name')->get();
		
		}
		
		return $supervisor_arr;
	}
	
	public function getallgroup($compid,$roleid,$userid)
	{
		if($roleid == 5)
		{ 
	       $group_arr=GroupMaster::join('sub_admin_assigned_group','sub_admin_assigned_group.group_id','=','group_master.group_id')
		   ->where('group_master.company_id','=',$compid)->where('group_master.is_active','=',1)->where('group_master.is_delete','=',0)->where('sub_admin_assigned_group.user_id','=', $userid)
		   ->where('sub_admin_assigned_group.is_active','=',1)->orderby('group_master.group_name','asc')->select('group_master.group_id','group_master.group_name')->get();
		}
		else
		{
			$group_arr= GroupMaster::where('company_id','=',$compid)->where('is_active','=',1)->where('is_delete','=',0)->select('group_id','group_name')->get();
		}
				
		return $group_arr;
	}
	public function getallband($compid)
	{	
		$band_level_arr = BandLevelMaster::where('company_id','=',$compid)->where('band_name','!=','')->where('is_active','=',1)->orderby('band_name','asc')->select('band_id','band_name')->get();
		return $band_level_arr;	
	}

	
	public function addnewuser(Request $request){
	    $userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$userLoggedIn = $userid;
		$userRole = array('1','3','5'); 
		$Getdate_formatecomapny=CommonHelper::getdate($compid);
		if(in_array($roleid, $userRole))
		{
				$supervisior = $request->input('supervisior');
				$first_name =  $request->input('first_name');
				$middle_name = $request->input('middle_name');
				$last_name =   $request->input('last_name');
				$band_name =   $request->input('band_name');
				$job_title =   $request->input('jobtitle');
				$company_name = $request->input('company_name');	
				$divison_name = $request->input('divison_name');
				$group_name =   $request->input('group_name');
				$get_area_id = 		$request->input('area');
				$get_location_id = 	$request->input('location');
				$phone_code = 	$request->input('phone_code');
				$timezonesSelect = 	$request->input('timezonesSelect');
				if(!is_numeric($get_area_id)){
						$error=array('status'=>'408','message'=>"use only single area");
						return response()->json($error);
				}
				if(!is_numeric($get_location_id)){
					    $error=array('status'=>'408','message'=>"use only single location");
						return response()->json($error);
				}
				//$get_location_id  = $model->check_locationname($location,$compid);
				//$get_area_id=$model->check_areaname($area,$compid);
				$phone_no = $request->input('phone_no');
				$position = strpos($phone_no, "-");
				if(strstr($phone_no, '-'))
				{
					$subcount = substr_count($phone_no, '-');
					if($subcount == 1)
					{
						if($position == 3) {
							$lastchars = substr($phone_no, strpos($phone_no, "-") + 1);
							$initialchars = substr($phone_no, 0, strpos($phone_no, "-"));
							$phone_no1 = "-" . $initialchars . "-" . $lastchars;
						}
					}
					if($position == 0) {
						$lastchars = substr($phone_no, strpos($phone_no, "-") + 1);
						$phone_no1 = "-" . $lastchars;
					}
				}

				$email_id = $request->input('emailid');
				$emp_id = $request->input('emp_id');
				$expdate = 	$request->input('expdate');
				
				$password = $request->input('password');
				$country = 	$request->input('country');
				$rolename = $request->input('rolename');
				$loginname =$request->input('loginname');
				$imagename = '';
				//$numcategory = $request->input('totalcategory');
				//$imagename = $request->input('imagename');			
				$status = $request->input('status_id');
							
				$checkusername = Loginfunction::checkuserforlogin($loginname, $compid);
				$checkuser_identity = Admin::checkUserLoginIdentifier($loginname, $compid);
				if(($checkusername) || ($checkuser_identity))
				{  
					 $error=array('status'=>'408','message'=>"Try with Different User Name.");
					 return response()->json($error);
					
				}
				$checkmail = Admin::checkemail($email_id, $compid);
				if($checkmail)
				{		
					 $error=array('status'=>'408','message'=>"Try with Different Email Id.");
					 return response()->json($error);
				}
				
				
				if($loginname != '')
				{
					$data['user_name'] 		= $first_name.' '.$last_name;
					$data['first_name'] 	= $first_name;
					$data['middle_name'] 	= isset($middle_name)?$middle_name:'';				
					$data['last_name'] 		= $last_name;
					$data['login_id'] 		= $loginname;
					$data['login_name'] 	= $compid.'_#_'.$loginname;				
					$salt_key 				= CommonHelper::create_salt();
					$data['salt_key'] 		= $salt_key;				
					$data['password'] 		= CommonHelper::getEncode($password, 'no').$salt_key;
					$data['role_id'] 		= $rolename;
					$CheckReset = Admin::checksettings($compid);
					if($CheckReset->change_password == 1){
						$data['is_reset'] 	= 1;	
					}else{
						$data['is_reset'] 	= 0;	
					} 
					$data['is_reset'] 	    = 0;
					$data['user_pic_path'] 	= $imagename;
					$data['country'] 		= $country;
					$data['email_id'] 		= $email_id;
					if($expdate!='')
					{
						$date = DateTime::createFromFormat($Getdate_formatecomapny, $expdate);
						$data['expiration_date'] = $date->format('Y-m-d');
					}
					$data['location'] 		   = $get_location_id;
					$data['phone_no'] 		   = $phone_code . $phone_no;
					$data['area'] 			   = $get_area_id;
					$data['is_active'] 		   = $status;
					$data['is_approved'] 	   = 1;
					$data['last_updated_by']   = $userid;
					$data['last_updated_time'] = date('Y-m-d -h-i-s');
					$data['created_date'] 	   = date('Y-m-d -h-i-s');
					$data['change_pass_date']  = date('Y-m-d H:i:s');	
					$data['company_id'] 	   = $compid;		
					$data['timezone_id'] 	   = $timezonesSelect;		
					$getuserid =UserMaster::create($data);
					$lastinid =$getuserid->user_id; 
					
					$user_identity['user_id']         = $lastinid;
					$user_identity['company_id']      = $compid;
					$user_identity['user_identity']   = $emp_id!='' ? $compid.'_#_'.$emp_id : $compid.'_#_'.$loginname;
					Admin::adduseridentifier($user_identity); 
					
					 if($lastinid)
					 {
						
						$checkjobtitle 			= Admin::checkjob_titlename($job_title,$compid,$lastinid);	

						$checkdivision 			= Admin::check_divisionname($divison_name,$compid,$lastinid);	

						$checkcompany 	    	= Admin::check_companyname($company_name,$compid,$lastinid);

						$checkgroup 				= Admin::checkgroupnames($group_name,$compid,$lastinid,$roleid,$userLoggedIn);  

						$checksupervisor 	= Admin::check_supervisor($supervisior,$compid,$lastinid);	
						if($band_name){						 
						$checkbandlevel		= Admin::check_bandlevel($band_name,$compid,$lastinid);
						}
					 } 
					$edituserid=$lastinid;
					
					$customfield=json_decode($request->input('customfield'),true);
				
					foreach($customfield as $customs){
						$customfieldId=$customs['id'];
						
						if($customs['is_date']=='yes'){
						  $data_time = new \DateTime($customs['val'], new \DateTimeZone('UTC'));
						  $customfieldvalue = $data_time->format('Y-m-d');
						}else{
							if(is_array($customs['val'])){
						       $customfieldvalue=implode(',',$customs['val']);	
                            }else{
                               $customfieldvalue=$customs['val'];	
                            }							
													  
						}
						$where=array();
						$where['custom_field_id'] 	= $customfieldId;
						$where['user_id'] 			= $edituserid;
						$where['company_id'] 		= $compid;
						
						Admin::updatecustomvalue($customfieldvalue, $where, $customfieldId, $edituserid, $compid);
					}
					
                    $fetchuserdetail =User::where('user_id','=',$lastinid)->where('company_id','=',$compid)->first();
					
					$fetchphoneno = $fetchuserdetail->phone_no;
					$updateduserid = $fetchuserdetail->user_id;
					if (preg_match('/\(?\d{3}\)?[-\s.]?\d{3}[-\s.]\d{4}/x', $fetchphoneno, $matches))
					{
						$position = strpos($fetchphoneno, "-");
						$phonecode = substr($fetchphoneno, 0, 3);
						if ($position == 6)
						{
							$phonelast = substr($fetchphoneno, strpos($fetchphoneno, "-") + 1);
							$phoneinitial = substr($fetchphoneno, 3, 4);
							$phone_no = $phonecode . "-" . $phoneinitial . $phonelast;
							$upphone['phone_no'] = $phone_no;
							user::where('user_id','=',$lastinid)->update($upphone);
						}
					} else {
						$phone = preg_replace("/[^0-9]/", "", $fetchphoneno);
						$fetchphoneno = $phone;
						if (strlen($fetchphoneno) == 9 || strlen($fetchphoneno) == 10) {
							$phonecode = substr($fetchphoneno, 0, 3);
							$phoneinitial = substr($fetchphoneno, 3, 3);
							$phonelast = substr($fetchphoneno, 6);
							$phone_no = $phonecode . "-" . $phoneinitial . "-" . $phonelast;
							$whereuser=array();
							$upphone['phone_no'] = $phone_no;
							user::where('user_id','=',$lastinid)->update($upphone);

						}
					}

					// $this->_helper->Organizegroup->groupcreation($compid,$userLoggedIn,2,$lastinid,$roleid);
					 if ($rolename == 3) {
						$inst['instructor_name'] = $first_name.' '.$last_name;
						$inst['user_id'] = $lastinid;
						$inst['company_id'] = $compid;
						$inst['is_active'] = 1;
						$inst['created_date'] = date('Y-m-d -h-i-s');
						$inst['last_updated_by'] = $userid;
						$inst['last_updated_date'] = date('Y-m-d -h-i-s');
						$inst['title'] = 'Instructor';
						InstructorMaster::create($inst);
					} 
				
					
					 Admin::checkforwelcomemail($lastinid, $compid, $data['email_id'], $data['user_name'], $password);
					//$this->_redirect('/admin/manageuserlist');
					$response=array('status'=>'200','message'=>"User Added Successfully.");
					return response()->json($response);
					
				}
		}else{
			$this->_redirect('/index/home');
	   }
		
	}

	public function manageuserlist(Request $request){
		$mode=$request->input('mode');
		$start=$request->input('start');
		$length=$request->input('length');
		$search = $request->input('search');
		$draw = $request->input('draw');
		$order = $request->input('order');
		$orderColumn=$order[0]['column'];
		$orderType=$order[0]['dir'];
		$searchtext=$search['value'];
		$user_id=Auth::user()->user_id;
		$compid=Auth::user()->company_id;
		$roleid=Auth::user()->role_id;
		
		
		$subadminPermissionData = Admin::getsubadminpermissiondata($compid);
		$sub_org_id = CommonFunctions::fetchUserOranizationIds($user_id);
		if(count($subadminPermissionData)>0)
		{
			$subadminbasedon = $subadminPermissionData[0]['user_based_on'];
			$instructorbasedon = $subadminPermissionData[0]['user_based_on_instructor'];
		}
		else
		{
			$subadminbasedon = 0;
			$instructorbasedon = 0;
		}
		if($mode=='selectedviewuser'){
			$usertoken=$request->input('usertoken');
			$selecteduserid=CommonHelper::getDecode($usertoken);
		
			 $dataallinfo= CommonFunctions::getusersall($compid,$user_id ,$selecteduserid,$roleid,'0',$sub_org_id,$subadminbasedon,$instructorbasedon,'','','','','');
			
		     $success=array("status"=>200,"data"=>$dataallinfo[0],'selecteduserid'=>$dataallinfo[2]);
		}else{
			 $dataallinfo= CommonFunctions::getUserInfo($compid,$user_id ,'',$roleid,'0',$sub_org_id,$subadminbasedon,$instructorbasedon,$start,$length,$orderType,$orderColumn,$searchtext);
		    $success=array("status"=>200,"data"=>$dataallinfo[0],"count"=>count($dataallinfo[1]),"draw"=>$draw);
		}
		
		 return $success;

	}
	
	public function getuserfieldname()
	{
		$compid=Auth::user()->company_id;
		$get_field_name=CommonFunctions::getFieldName($compid);
		return $get_field_name;
		
	}
	
	public function getedituserdetail(Request $request)
	{
		$selectedusertoken=$request->input('selectedusertoken');
		$edituserid=CommonHelper::getDecode($selectedusertoken);

		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		
		//field locked status..
		$LockedStatus = Datamanagement::fetchLockedstatusData($compid);
		//get all job tittle;
		$alljobtitle = $this->getalljobtitle($compid);
		//get all Country;
		$allcountry =Admin::getCountry();
		//get all  company
		$allcompany =$this->getallcompany($compid, $roleid, $userid);
		//get all Division
		$alldivision = $this->getalldivision($compid);
		//get all area 
		$allarea=$this->getallarea($compid);
		//get all Location 
		$alllocation=$this->getalllocation($compid);
		//get all supervisior 
		$supervisior = $this->getallsupervisior($compid,$userid,$roleid);	
		//get all group
		$allgroup = $this->getallgroup($compid,$roleid,$userid);
		
		$timeZones=ClassTimezones::get();
		
		
		//check field
		 $val18=$val15=$val19=$val7=$val8=$val9=$val10=$val17=0;
		 $field_18 	=Admin::checkfield(18,$compid);
		 if($field_18){
			 $val18=1;
		 }
		 $field_15= Admin::checkfield(15,$compid);
		 if($field_15){
			 $val15=1;
		 }
		 $field_19= Admin::checkfield(19,$compid);
		 if($field_19){
			 $val19=1;
		 }
		 $field_7= Admin::checkfield(7,$compid);
		 if($field_7){
			 $val7=1;
		 }
		 $field_8= Admin::checkfield(8,$compid);
		  if($field_8){
			 $val8=1;
		 }
		 $field_9= Admin::checkfield(9,$compid);
		 if($field_9){
			 $val9=1;
		 }
		 
		 $field_10= Admin::checkfield(10,$compid);
		 if($field_10){
			 $val10=1;
		 }
		 $field_17= Admin::checkfield(17,$compid);
		 if($field_17){
			 $val17=1;
		 }
		 $checkFieldArray=array('field_18'=>$val18,'field_15'=>$val15,'field_19'=>$val19,'field_7'=>$val7,'field_8'=>$val8,'field_9'=>$val9,'field_10'=>$val10,'field_17'=>$val17);
		 
		 //get all bandlevel 
		$allband= $this->getallband($compid);
		
		//getcustom all field;
		$getcustomfieldnameandidbycompnyname=Admin::getcustomfieldnameandidbycompnyname($compid);
		if(count($getcustomfieldnameandidbycompnyname)>0)
		{
			$customfieldIdArr=array();
			$customfieldnameArr=array();
			$customfieldDateArr=array();
			$customfieldvalueArr=array();
			$k=0;
			foreach($getcustomfieldnameandidbycompnyname as $customfieldnameandid)
			{
				$getcustomfieldvalue=Admin::getcustomfieldvalue($customfieldnameandid->custom_field_id,$edituserid,$compid);
				if(count($getcustomfieldvalue)>0)
				{
					 if($customfieldnameandid->column_type == 'Date'){
					    $customfieldDateArr[$customfieldnameandid->custom_field_id]=$getcustomfieldvalue[0]->field_value;
					 }
					 
					 $customfieldvalueArr[$k]=$getcustomfieldvalue[0]->field_value;
				}
				$k++;
			}
		}
		$customfieldArr = $getcustomfieldnameandidbycompnyname;
		$customfieldnameArr = $customfieldnameArr;
		$customfieldvalueArr = $customfieldvalueArr;
		$strhml='';
		$strhml=$this->customfieldhtml($customfieldArr,$customfieldnameArr,$customfieldvalueArr);
		
		//get company password setting
		$PasswordValidateSetting = CommonFunctions::fetchCompPasswordsettingData($compid);
		// method for making  PasswordValidateSetting div.
		$tag_str = CommonFunctions::customlitag($PasswordValidateSetting);
		
		//get user all user detail.
		
		//get user groups
		$usergroups = Admin::getUserGroup($edituserid, $compid);
	
		//get user jobs
		$userjobs = Admin::getuserjob($edituserid, $compid);
		
		//get user division
		$userdivision = Admin::getuserdivision($edituserid, $compid);	
		
		//get user company
		$usercompany = Admin::getusercompany($edituserid, $compid);
		
		//get user area
		$user_area = Admin::getuserareadata($edituserid,$compid);	
		//get user location
		$user_location =Admin::getuserlocationdata($edituserid,$compid);
		
		//get user supervisor
		$usersupervisor = Admin::getusersupervisor($edituserid, $compid);
		
		$subadminPermissionData = Admin::getsubadminpermissiondata($compid);
		$sub_org_id = CommonFunctions::fetchUserOranizationIds($edituserid);
		if(count($subadminPermissionData)>0)
		{
			$subadminbasedon = $subadminPermissionData[0]['user_based_on'];
			$instructorbasedon = $subadminPermissionData[0]['user_based_on_instructor'];
		}
		else
		{
			$subadminbasedon = 0;
			$instructorbasedon = 0;
		}
		$getuserbasicdetail=CommonFunctions::getusertoedit($compid,$edituserid,'',$roleid,$sub_org_id,$subadminbasedon);
		
		$alldata=array('PasswordValidateSetting'=>$PasswordValidateSetting,'tag_str'=>$tag_str,'lockedStatus'=>$LockedStatus,'jobtitle'=>$alljobtitle,'Country'=>$allcountry,'allcompany'=>$allcompany,'alldivision'=>$alldivision,'allarea'=>$allarea,'alllocation'=>$alllocation,'supervisior'=>$supervisior,'allgroup'=>$allgroup,'checkFieldArray'=>$checkFieldArray,'allband'=>$allband,'customfieldArr'=>$customfieldArr,'customfieldvalueArr'=>$customfieldvalueArr,'customfieldnameArr'=>$customfieldnameArr,'strhml'=>$strhml,'usergroups'=>$usergroups,'userjobs'=>$userjobs,'userdivision'=>$userdivision,'usercompany'=>$usercompany,'user_area'=>$user_area,'user_location'=>$user_location,'usersupervisor'=>$usersupervisor,'getuserbasicdetail'=>$getuserbasicdetail,'customfieldDateArr'=>$customfieldDateArr,'timeZones'=>$timeZones);
		return response()->json($alldata);
		
	}
	
	public function updateuserdata(Request $request)
	{
		//echo '<pre>'; print_r($request->input()); exit;
			$userid =Auth::user()->user_id;
			$compid =Auth::user()->company_id;
			$roleid =Auth::user()->role_id;
			$userLoggedIn = $userid;
			$userRole = array('1','3','5');
			if(in_array($roleid, $userRole))
			{
				$first_name=$request->input('first_name');
				$last_name=$request->input('last_name');
				$user_name=$first_name.' '.$last_name;
				$loginname=$request->input('loginname');
				$password=$request->input('password');
				$emailid=$request->input('emailid');
				$fetchphoneno=$request->input('phone_no');
				$rolename=$request->input('rolename');
				$country=$request->input('country');
				$middle_name=$request->input('middle_name');
				$area=$request->input('area');
				$divisionname=$request->input('divison_name');
				$customfield=$request->input('customfield');
				$supervisior=$request->input('supervisior');
				$company=$request->input('compname');
				$edituserid=$request->input('usertoken');
				$location=$request->input('location');
				$select_status=$request->input('status');
				$expdate=$request->input('expdate');
				$group_name=$request->input('group_name');
				$jobtitle=$request->input('jobtitle');
				$band_name=$request->input('band_name');
				$timeZone=$request->input('timeZone');
				if(!is_numeric($edituserid)){
				 $edituserid=CommonHelper::getDecode($edituserid);
				}
				
				$get_area_id= isset($area['area_id'])? $area['area_id'] : $area ;
                $get_location_id= isset($location['location_id']) ? $location['location_id'] : $location;
	
				if(!is_numeric($get_area_id)){
						$error=array('status'=>'408','message'=>"use only single area");
						return response()->json($error);
				}
				if(!is_numeric($get_location_id)){
					    $error=array('status'=>'408','message'=>"use only single location");
						return response()->json($error);
				}
				
				$customfield=json_decode($request->input('customfield'),true);

				foreach($customfield as $customs){
					$customfieldId=$customs['id'];
					
					if($customs['is_date']=='yes'){
					  $data_time = new \DateTime($customs['val'], new \DateTimeZone('UTC'));
					  $customfieldvalue = $data_time->format('Y-m-d');
					}else{
						if(is_array($customs['val'])){
						   $customfieldvalue=implode(',',$customs['val']);	
						}else{
						   $customfieldvalue=$customs['val'];	
						}							
												  
					}
					$where=array();
					$where['custom_field_id'] 	= $customfieldId;
					$where['user_id'] 			= $edituserid;
					$where['company_id'] 		= $compid;
					
					Admin::updatecustomvalue($customfieldvalue, $where, $customfieldId, $edituserid, $compid);
				}
				
				$checkusername = Admin::checkuserexceptthis($loginname, $edituserid, $compid);
				if($checkusername)
				{	
					$error=array('status'=>'408','message'=>"Try with Different User Name.");
					return response()->json($error);
				}
				
				if(preg_match('/\(?\d{3}\)?[-\s.]?\d{3}[-\s.]\d{4}/x', $fetchphoneno, $matches))
				{
					$position = strpos($fetchphoneno, "-");
					$phonecode = substr($fetchphoneno, 0, 3);
					$phone_no = $fetchphoneno;
				}
				else
				{
					$phone_no = preg_replace("/[^0-9]/", "", $fetchphoneno);
				}
				
				$data['user_name'] = $user_name;
				$data['first_name'] = $first_name;	
				$data['last_name'] = $last_name;
				$data['login_id'] = $loginname;
				$data['login_name'] = $compid.'_#_'.$loginname;
				if($password!="" && $password!="********")
				{
					$salt_key = CommonHelper::create_salt();
					$data['salt_key'] = $salt_key;
					$data['password'] = CommonHelper::getEncode($password, 'no').$salt_key;
				}
				
				if($expdate!='')
				{
					$expdate = date('Y-m-d',strtotime($expdate));
					if(strtotime($expdate)<time())
					{
						$data['is_active'] = 0;
					}
					if($select_status == 1 && strtotime($expdate)>time()) 
					{
						$data['is_active'] = 1;
					}
				}
				else
				{
					$data['is_active']=$select_status;
					
					$data['expiration_date'] = "0000-00-00";
				}

				$data['role_id']  = $rolename;
				$data['country']  = $country;
				$data['email_id'] = $emailid;
				$data['location'] = $get_location_id;
				$data['phone_no'] = ($phone_no) ? $phone_no : '0';
				$data['area']     = $get_area_id;	
				$data['middle_name'] = $middle_name;	
				$data['is_approved'] = 1;
				$data['last_updated_time'] = date('Y-m-d -h-i-s');
				$data['company_id'] = $compid;
				$data['timezone_id'] = $timeZone;
		//	echo '<pre>'; print_r($data); exit;
				 $checkdivision = Admin::update_divisionname($divisionname, $compid, $edituserid);
				 $checkcompany  = Admin::update_companyname($company,$compid,$edituserid);
				 Admin::updatejob_titlenamenew($jobtitle, $compid, $edituserid);
				 $group_ids = Admin::update_groupnames($group_name,$compid,$edituserid,$roleid,$userLoggedIn);
				 Admin::updatebandlevel($band_name, $compid, $edituserid);	
				 $checksupervisior = Admin::update_supervisior($supervisior,$compid,$edituserid);
				
				$is_approve =UserMaster::where('user_id','=',$edituserid)->where('company_id','=',$compid)->first();
				$approve_status = $is_approve->is_approved;
				if($approve_status==0){
					UserMaster::where('user_id','=',$edituserid)->where('company_id','=',$compid)->update($data);
					Mailnotification::welcomeemail($lastinid,$compid,$email,$username,$password,'');
					Mailnotification::UserAprovalbyAdmin($edituserid, $compid);	
				}
				else{
					UserMaster::where('user_id','=',$edituserid)->where('company_id','=',$compid)->update($data);
				}
				
				$user_identity['user_identity'] = $compid.'_#_'.$loginname;
                Admin::updateUserIdentifier($edituserid,$compid,$user_identity);
				
				
				/* $fetchuserdetail = $model->getuserdetail($edituserid, $compid);
				$updateduserid = $fetchuserdetail[0]->user_id; */
				
					//user_group functionality starts here
				/* if(count($fetchuserdetail) > 0)
				{
					$stausdata = array();
					$stausdata['is_active'] = 0 ;
					$groupwhere = "user_id = $edituserid" ;
					$model->updateanywhere('user_group' , $stausdata , $groupwhere);
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->setFetchMode(Zend_Db::FETCH_OBJ);
					$groupdata = array();
					if(count($group_ids)>0)
					{
						foreach($group_ids as $val)
						{
							$userwithgroupid = "SELECT * from user_group AS U where group_id=? and user_id=? and auto_group=1";
							$result = $db->fetchAll($userwithgroupid,array($val,$edituserid));
							if(count($result)>0) $groupdata[] = $result[0]->group_id;
						}
					}

					$model->disableIsUpdate($edituserid,$compid);
					$checkautomatedgroup = $model->fetchgroupsetting($compid);
					$divisionvalue = $locationvalue = $usercompanyvalue = $departmentvalue = $firstwordvalue = $lastwordvalue = $exactwordvalue = $areavalue = $bandvalue = 0;
					if(count($checkautomatedgroup) > 0)
					{
						foreach($checkautomatedgroup as $value)
						{
							$divisionvalue = $value->division;
							$locationvalue = $value->location;
							$usercompanyvalue = $value->user_company_name;
							$departmentvalue = $value->department;
							$firstwordvalue = $value->first_word;
							$lastwordvalue = $value->last_word;
							$exactwordvalue = $value->exact_word;
							$areavalue = $value->area;
							$bandvalue = $value->band;
						}
					}					
					
					$this->_helper->LearningplanFunctions->addDeleteUserInLP('get_group', $edituserid, $compid, 'check');
					$manual_group = array_values(array_diff($group_ids,$groupdata));
					
					if(count($manual_group>0)){
						$model->update_manualgroupnames($manual_group,$edituserid,$userLoggedIn,$compid);						
					}
					$this->getDefaultGroup($compid, $edituserid, $userLoggedIn);
					$this->_helper->Organizegroup->groupcreation($compid, $userLoggedIn, 2, $edituserid, $roleid);
					$where=array();
					$data=array('is_update'=>2);
					$where['is_active = ?'] = 0;
					$where['user_id = ?'] = $edituserid;
					$results = $db->update('user_group', $data, $where);
				} */
				
			
				if ($rolename != 3) {
					$checkinstructor = Admin::fetchinstructordata($compid, $edituserid);
					if ($checkinstructor) {
						$instructordata['is_active'] = 0;
						InstructorMaster::where('user_id','=',$edituserid)->where('company_id','=',$compid)->update($instructordata);
					}
				} 
				
				 if ($rolename == 3) {
					$instructordata['instructor_name'] = $first_name.' '.$last_name;;
					$instructordata['company_id'] = $compid;
					$instructordata['last_updated_by'] = $userid;
					$instructordata['last_updated_date'] = date('Y-m-d -h-i-s');
					$instructordata['user_id'] = $edituserid;
					$instructordata['is_active'] = 1;
					$checkinstructor = Admin::fetchinstructordata($compid, $edituserid);
					if ($checkinstructor){
						InstructorMaster::where('user_id','=',$edituserid)->where('company_id','=',$compid)->update($instructordata);
						
					}else{
						$instructordata['created_date'] = date('Y-m-d -h-i-s');
						$instructordata['is_internal'] = 0;
						InstructorMaster::create($instructordata);
					}
				} 
				
				$response=array('status'=>'200','message'=>"User Updated Successfully.");
				return response()->json($response);
			}
		}
		

}
