<?php

namespace App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GroupMaster;
use App\Models\UserMaster;
use App\Models\Course;
use App\Models\CourseGroups;
use App\Models\CourseUsers;
use App\Models\CourseDetail;
use DB,Auth,CommonHelper;

class CourseDistributionController extends Controller
{
    //
	public function coursedistributionData(Request $request)
	{
		$company_id=Auth::user()->company_id;
		$trainingCode=$request->input('trainingCode');
		$course_data=Course::where('training_program_code','=',$trainingCode)->where('status_id','=',1)->select('course_id')->first();
		if($course_data){
			$course_id=$course_data->course_id;
	        $courseGrp=CourseGroups::where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('status_id','=',1)->pluck('group_id');
			
			$groupName=GroupMaster::where('company_id',$company_id)->where('is_active','=',1)->where('is_delete','=',0)->whereNotIn('group_id',$courseGrp)->select('group_id','group_name')->get();
			
			$courseUser=CourseUsers::where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('status_id','=',1)->pluck('user_id');
			
			$userName=UserMaster::where('company_id',$company_id)->where('is_active','=',1)->where('is_delete','=',0)->whereNotIn('user_id',$courseUser)->select('user_id','user_name')->get();
			
			$courseDetail=CourseDetail::where('course_id',$course_id)->select('course_name','course_title')->first();
			if($courseDetail->course_title){
				$courseName=$courseDetail->course_name." ".$courseDetail->course_title;
			}else{
				$courseName=$courseDetail->course_name;
			}
			
			
			$allData=array('allusers'=>$userName,'allgroup'=>$groupName,'courseName'=>$courseName);
			return response()->json($allData);
		}else{
		   $allData=array('status'=>408,'message'=>'Invalid Training');
	       return response()->json($allData);
		}
	}
	
	
	
	public function coursedistribution(Request $request)
	{   
	   
	    $company_id=Auth::user()->company_id;
	    $user_id=Auth::user()->user_id;
		
		$trainingCode=$request->input('trainingCode');
		$groups=$request->input('groups');
		$optionalGroup=$request->input('optionalGroup');
		$users=$request->input('users');
		$optionalUser=$request->input('optionalUser');
		$due_date=$request->input('due_date');
		
		$course_data=Course::where('training_program_code','=',$trainingCode)->where('status_id','=',1)->select('course_id')->first();
		if($course_data){
			
			$course_id=$course_data->course_id;
			foreach($groups as $grpid){
				$courseGrp=CourseGroups::where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('status_id','=',1)->where('group_id','=',$grpid)->first();
				if(!$courseGrp){
					
					$dataCreate=array();
					$dataCreate['course_id']=$course_id;
					$dataCreate['group_id']=$grpid;
					$dataCreate['company_id']=$company_id;
					$dataCreate['due_date']= date('Y-m-d',strtotime($due_date.'+1day'));
					$dataCreate['status_id']=1;
					$dataCreate['is_mandatory']=1;
					$dataCreate['last_updated_by']=$user_id;
					$dataCreate['created_by']=$user_id;
					$dataCreate['created_date']=date('Y-m-d');
                   CourseGroups::create($dataCreate);
				 } 
			}
			foreach($optionalGroup as $grpid){
				$courseGrp=CourseGroups::where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('status_id','=',1)->where('group_id','=',$grpid)->first();
				if(!$courseGrp){
					$dataCreate=array();
					$dataCreate['course_id']=$course_id;
					$dataCreate['group_id']=$grpid;
					$dataCreate['company_id']=$company_id;
					$dataCreate['due_date']= date('Y-m-d',strtotime($due_date.'+1day'));
					$dataCreate['status_id']=1;
					$dataCreate['is_mandatory']=0;
					$dataCreate['last_updated_by']=$user_id;
					$dataCreate['created_by']=$user_id;
					$dataCreate['created_date']=date('Y-m-d');
                   CourseGroups::create($dataCreate);
				 } 
			}
			
			foreach($users as $userid){
				$courseUser=CourseUsers::where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('status_id','=',1)->where('user_id','=',$userid)->first();
				if(!$courseUser){
					$dataCreate=array();
					$dataCreate['course_id']=$course_id;
					$dataCreate['user_id']=$userid;
					$dataCreate['company_id']=$company_id;
					$dataCreate['due_date']=date('Y-m-d',strtotime($due_date.'+1day'));
					$dataCreate['status_id']=1;
					$dataCreate['is_mandatory']=1;
					$dataCreate['last_updated_by']=$user_id;
					$dataCreate['created_by']=$user_id;
					$dataCreate['created_date']=date('Y-m-d');
					CourseUsers::create($dataCreate);
				} 
			}
			foreach($optionalUser as $userid){
				$courseUser=CourseUsers::where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('status_id','=',1)->where('user_id','=',$userid)->first();
				if(!$courseUser){
					$dataCreate=array();
					$dataCreate['course_id']=$course_id;
					$dataCreate['user_id']=$userid;
					$dataCreate['company_id']=$company_id;
					$dataCreate['due_date']=date('Y-m-d',strtotime($due_date.'+1day'));
					$dataCreate['status_id']=1;
					$dataCreate['is_mandatory']=0;
					$dataCreate['last_updated_by']=$user_id;
					$dataCreate['created_by']=$user_id;
					$dataCreate['created_date']=date('Y-m-d');
					CourseUsers::create($dataCreate);
				} 
			}
			
			$allData=array('status'=>200,'message'=>'Assign Successfully');
			return response()->json($allData);
			
		}else{
		   $allData=array('status'=>408,'message'=>'Invalid Training');
	       return response()->json($allData);
		}
	}
	
	public function getAssignedUsers(Request $request){
		$company_id=Auth::user()->company_id;
		$start=$request->input('start');
		$length=$request->input('length');
		$search = $request->input('search');
		$draw = $request->input('draw');
		$order = $request->input('order');
		$trainingCode = $request->input('trainingCode');
		$orderColumn=$order[0]['column'];
		$orderType=$order[0]['dir'];
		$searchtext=$search['value'];
		$Getdate_formatecomapny = CommonHelper::getdate($company_id);
		
		$course_data=Course::where('training_program_code','=',$trainingCode)->where('status_id','=',1)->select('course_id')->first();
		if($course_data){
				$course_id=$course_data->course_id;
				$querydata=CourseUsers::join('user_master',function($join) use($company_id){
				$join->on('user_master.user_id','=','course_users.user_id')
				->where('user_master.is_active','=',1)
				->where('user_master.is_delete','=',0)
				->where('user_master.company_id','=',$company_id);
				})
				->where('course_users.company_id','=',$company_id)->where('course_users.status_id','=',1)->where('course_users.course_id','=',$course_id);

				/* if($searchtext){
					$querydata=$querydata->where(function($query) use($searchtext){
					$query->where('course_details.course_name','like','%'.$searchtext.'%')
					->orWhere('courses.credit_value','like','%'.$searchtext.'%')
					->orWhere('training_type.training_type','like','%'.$searchtext.'%');
					});
				} */

				$column='';
				if($orderColumn==0){
				$column='user_master.user_name';
				}
				
				$totalcount=$querydata->groupby('user_master.user_id','course_users.course_id')->get();
				if($column){
				
				$Getdate_formatecomapny=str_replace('y','%Y',str_replace('Y','%Y',str_replace('d','%d',str_replace('m','%m',$Getdate_formatecomapny))));
				
				$allrecords=$querydata->orderBy($column,$orderType)->groupby('user_master.user_id','course_users.course_id')->skip($start)->take($length)->select(DB::raw("course_users.course_users_id,user_master.user_name,DATE_FORMAT(course_users.due_date,'$Getdate_formatecomapny') as due_date,DATE_FORMAT(course_users.created_date, '$Getdate_formatecomapny') as created_date"),'is_mandatory')->get();
				}else{
				$allrecords=$querydata->groupby('user_master.user_id','course_users.course_id')->skip($start)->take($length)->select("course_users.course_users_id,user_master.user_name,DATE_FORMAT(course_users.due_date,'$Getdate_formatecomapny') as due_date",'is_mandatory')->get();
				}
				return  $success=array("status"=>200,"data"=>$allrecords,"count"=>count($totalcount),"draw"=>$draw);
		}
	}
	
	public function getAssignedGroups(Request $request){
		$company_id=Auth::user()->company_id;
		$start=$request->input('start');
		$length=$request->input('length');
		$search = $request->input('search');
		$draw = $request->input('draw');
		$order = $request->input('order');
		$trainingCode = $request->input('trainingCode');
		$orderColumn=$order[0]['column'];
		$orderType=$order[0]['dir'];
		$searchtext=$search['value'];
		$Getdate_formatecomapny = CommonHelper::getdate($company_id);
		
		$course_data=Course::where('training_program_code','=',$trainingCode)->where('status_id','=',1)->select('course_id')->first();
		if($course_data){
				$course_id=$course_data->course_id;
				$querydata=CourseGroups::join('group_master',function($join) use($company_id){
				$join->on('group_master.group_id','=','course_groups.group_id')
				->where('group_master.is_active','=',1)
				->where('group_master.is_delete','=',0)
				->where('group_master.company_id','=',$company_id);
				})
				->where('course_groups.company_id','=',$company_id)->where('course_groups.status_id','=',1)->where('course_groups.course_id','=',$course_id);

				/* if($searchtext){
					$querydata=$querydata->where(function($query) use($searchtext){
					$query->where('course_details.course_name','like','%'.$searchtext.'%')
					->orWhere('courses.credit_value','like','%'.$searchtext.'%')
					->orWhere('training_type.training_type','like','%'.$searchtext.'%');
					});
				} */

				$column='';
				if($orderColumn==0){
				$column='group_master.group_name';
				}
				
				$totalcount=$querydata->groupby('group_master.group_id','course_groups.course_id')->get();
				if($column){
				
				$Getdate_formatecomapny=str_replace('y','%Y',str_replace('Y','%Y',str_replace('d','%d',str_replace('m','%m',$Getdate_formatecomapny))));
				
				$allrecords=$querydata->orderBy($column,$orderType)->groupby('group_master.group_id','course_groups.course_id')->skip($start)->take($length)->select(DB::raw("course_groups.course_groups_id,group_master.group_name,DATE_FORMAT(course_groups.due_date,'$Getdate_formatecomapny') as due_date,DATE_FORMAT(course_groups.created_date, '$Getdate_formatecomapny') as created_date"),'is_mandatory')->get();
				}else{
				$allrecords=$querydata->groupby('group_master.group_id','course_groups.course_id')->skip($start)->take($length)->select("course_groups.course_groups_id,group_master.group_name,DATE_FORMAT(course_groups.due_date,'$Getdate_formatecomapny') as due_date",'is_mandatory')->get();
				}
				return  $success=array("status"=>200,"data"=>$allrecords,"count"=>count($totalcount),"draw"=>$draw);
		}
	}
	
	
	public function unassignuserAction(Request $request)
	{
		$assignedUserId=$request->input('assignedUserId');
		CourseUsers::where('course_users_id',$assignedUserId)->delete();
		return response()->json(['status'=>201,'message'=>'success']);
	}
	
	public function unassigngroupAction(Request $request)
	{
		$assignedGroupId=$request->input('assignedGroupId');
		CourseGroups::where('course_groups_id',$assignedGroupId)->delete();
		return response()->json(['status'=>201,'message'=>'success']);
	}
	
}
