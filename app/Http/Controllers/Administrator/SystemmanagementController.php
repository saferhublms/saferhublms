<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,CommonFunctions;
use App\Http\Traits\Admin;
use App\Models\CompanySettings;
use App\Models\SubadminPermissions;
use App\Models\CompanyPasswordSetting;

class SystemmanagementController extends Controller
{
    public function getSystemsetting()
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$permission=CommonFunctions::getCompanySettingGroups($compid);
		$tabShowPermission=array();
		if(!in_array($roleid, @$permission['Security_System_Settings'])){
			$tabShowPermission['Security_System_Settings']=1;
		}else{
		    $tabShowPermission['Security_System_Settings']=0;
		}
		if(!in_array($roleid, @$permission['My_Requirement_Settings'])){
			$tabShowPermission['My_Requirement_Settings']=1;
		}else{
		    $tabShowPermission['My_Requirement_Settings']=0;
		}
		if(!in_array($roleid, @$permission['SubAdmin_Permission_Settings'])){
			$tabShowPermission['SubAdmin_Permission_Settings']=1;
		}else{
		    $tabShowPermission['SubAdmin_Permission_Settings']=0;
		}
		if(!in_array($roleid, @$permission['SystemSetting_DisableVolunteerReq'])){
			$tabShowPermission['SystemSetting_DisableVolunteerReq']=1;
		}else{
		    $tabShowPermission['SystemSetting_DisableVolunteerReq']=0;
		}
		
		$fetchallvalues=Admin::checkduplicatedataforcompsetting($compid);
		if(count($fetchallvalues)>0){
		$fetchallvalues[0]->approval_mailid=($fetchallvalues[0]->approval_mailid !='') ? $fetchallvalues[0]->approval_mailid : '';
		$fetchallvalues[0]->input_training_records=($fetchallvalues[0]->input_training_records==1)?true:false;
		$fetchallvalues[0]->require_supervisor_approval=($fetchallvalues[0]->require_supervisor_approval==1)?true:false;
		$fetchallvalues[0]->update_user_info=($fetchallvalues[0]->update_user_info==0)?true:false;
		$fetchallvalues[0]->can_edit_skills=($fetchallvalues[0]->can_edit_skills==1)?true:false;
		$fetchallvalues[0]->automated_registration=($fetchallvalues[0]->automated_registration==1)?true:false;
		$fetchallvalues[0]->training_feedback=($fetchallvalues[0]->training_feedback==0)?true:false;
		$fetchallvalues[0]->classroom_feedback=($fetchallvalues[0]->classroom_feedback==0)?true:false;
		$fetchallvalues[0]->change_username_password=($fetchallvalues[0]->change_username_password==1)?true:false;
		$fetchallvalues[0]->is_fail_transcript=($fetchallvalues[0]->is_fail_transcript==1)?true:false;
		$fetchallvalues[0]->supervisor_training_view=($fetchallvalues[0]->supervisor_training_view==1)?true:false;
		$fetchallvalues[0]->downgrade_role=($fetchallvalues[0]->downgrade_role==1)?true:false;
		$fetchallvalues[0]->promote_user=($fetchallvalues[0]->promote_user==1)?true:false;
		$fetchallvalues[0]->welcome_user_mail=($fetchallvalues[0]->welcome_user_mail==1)?true:false;
		$fetchallvalues[0]->welcome_user_mail=($fetchallvalues[0]->welcome_user_mail==1)?true:false;
		$fetchallvalues[0]->change_password=($fetchallvalues[0]->change_password==1)?true:false;
		$fetchallvalues[0]->lp_complete=($fetchallvalues[0]->lp_complete==1)?true:false;
		$fetchallvalues[0]->assessment_complete=($fetchallvalues[0]->assessment_complete==1)?true:false;
		$fetchallvalues[0]->assignment_complete=($fetchallvalues[0]->assignment_complete==1)?true:false;
		}
		
		$fetchPasswordSetting=CommonFunctions::fetchCompPasswordsettingData($compid);
		if($fetchPasswordSetting){
			$fetchPasswordSetting[0]->password_expiration_time=(string)$fetchPasswordSetting[0]->password_expiration_time;
			$fetchPasswordSetting[0]->password_length=(string)$fetchPasswordSetting[0]->password_length;
			$fetchPasswordSettingData=($fetchPasswordSetting[0]->custom_pass_setting==1)?true:false;
		}else{
			$fetchPasswordSettingData=0;
		}
		
		
		$fetchSubadminPermission = Admin::getsubadminpermissiondata($compid);
		if($fetchSubadminPermission){
			$fetchSubadminPermission[0]['user_based_on']=(string)$fetchSubadminPermission[0]['user_based_on'];
			$fetchSubadminPermission[0]['user_edit_permission']=(string)$fetchSubadminPermission[0]['user_edit_permission'];
			$fetchSubadminPermission[0]['user_based_on_instructor']=(string)$fetchSubadminPermission[0]['user_based_on_instructor'];
			$fetchSubadminPermission[0]['user_edit_permission_instructor']=(string)$fetchSubadminPermission[0]['user_edit_permission_instructor'];
		}
		
		
		return response()->json(['tabShowPermission'=>$tabShowPermission,'fetchallvalues'=>$fetchallvalues,'compId'=>$compid,'fetchPasswordSetting'=>$fetchPasswordSettingData,'passwordSetting'=>$fetchPasswordSetting,'submitPermission'=>$fetchSubadminPermission]);
				
				
		
	}
	
	public function systemmgtAction(Request $request){
		
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$fetchValue=$request->input('fetchValue');
		$compsetting=$request->input('compsetting');
		$newsettings=$request->input('newsetting');
		$updateuserinfo=$request->input('updateuserinfo');
		$training_feedback=$request->input('training_feedback');
		$classroom_feedback=$request->input('classroom_feedback');
		$autoregister=$request->input('autoregister');
		$trainingrecords=$request->input('trainingrecords');
		$items=$request->input('items');
		$supervisorapproval=$request->input('supervisorapproval');
		$caneditskills=$request->input('caneditskills');
		$admin_email=$request->input('admin_email');
		$change_username_password=$request->input('change_username_password');
		$supervisor_training_view=$request->input('supervisor_training_view');
		$company_name=$request->input('company_name');
		$promote_user=$request->input('promote_user');
		$downgrade_role=$request->input('downgrade_role');
		$approval_emailid=$request->input('approval_emailid');
		$welcome_user_mail=$request->input('welcome_user_mail');
		$is_pass_transcript=$request->input('is_pass_transcript');
		
		if($compsetting != '') {
			if($newsettings == '1'){
				$checkupdate=false;
			}else{
				$checkupdate=true;
			}
			$data['input_training_records'] = $trainingrecords;
			$data['require_supervisor_approval']=$supervisorapproval;
			$data['automated_registration']=$autoregister;
			$data['update_user_info']=$updateuserinfo;
			$data['training_feedback']=$training_feedback;
			$data['classroom_feedback']=$classroom_feedback;
			$data['can_edit_skills']=$caneditskills;
			$data['only_instructor_create_blog']=$fetchValue['only_instructor_create_blog'];
			$data['change_username_password']=$change_username_password;
			$data['supervisor_training_view']=$supervisor_training_view;
			$data['user_layout']=$fetchValue['user_layout'];
			$data['admin_email']=$admin_email;
			$data['company_name']=$company_name;
			$data['promote_user']=$promote_user;
			$data['downgrade_role']=$downgrade_role;
			$data['rating']=$items;
			$data['approval_mailid']=$approval_emailid;
			$data['welcome_user_mail']=$welcome_user_mail;
			$data['is_fail_transcript']=$is_pass_transcript;
			$data['last_updated_date']=date('Y-m-d -h-i-s');	
			$data['last_updated_by']=$userid;
			$data['company_id']=$compid;
			if(!$checkupdate)
			{
				$data['created_date'] = date('Y-m-d -h-i-s');
				CompanySettings::create($data);
				return "success";
			}else{			
				if(Admin::checkduplicatedataforcompsetting($compid))
				{
					CompanySettings::where('company_id',$compid)->update($data);
					return "update";
				}
			}
				
			
		}
	}
	
	public function passwordsettingAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3','5');
		$change_password = $request->input('change_password');
		if(in_array($roleid, $userRole))
		{
			$fetchPasswordSetting=CommonFunctions::fetchCompPasswordsettingData($compid);
			$fetchallvalues=Admin::checkduplicatedataforcompsetting($compid);
            $password_expiration = $request->input('password_expiration');
            $password_length = $request->input('password_length');
			$password_complexity = $request->input('password_complexity');
			$invalidpashit = $request->input('invalidpashit');
            $lockeffecPass = $request->input('lockeffecPass');
			$pass_setting = $request->input('pass_setting');
            $data = array();
			$data['change_password'] = $change_password;	
            $passData = array();
			$passData['custom_pass_setting']=$pass_setting;
			if($pass_setting == 1) {
				$passData['password_expiration_time']=$password_expiration;
				$passData['password_length']=$password_length;
				$passData['password_complexity']=$password_complexity;
				$passData['maximum_invalid_hit']=$invalidpashit;
				if($invalidpashit != 0)
					$passData['locked_password_time']=$lockeffecPass;
				else
					$passData['locked_password_time']=0;
			}else {
				$passData['password_expiration_time']= 0;
				$passData['password_length']= 0 ;
				$passData['password_complexity']= 1 ;
				$passData['maximum_invalid_hit']= 0;
				$passData['locked_password_time']= 0;
			}
			$passData['company_id'] = $compid;
			$passData['created_by'] = $userid;
			if(count($fetchPasswordSetting) > 0){
				CompanyPasswordSetting::where('company_id',$compid)->update($passData);
				CompanySettings::where('company_id',$compid)->update($data);
			}else{
				$passData['created_date'] = date('Y-m-d H:i:s');
				CompanyPasswordSetting::create($passData);
				if(count($fetchallvalues) > 0)
				{
					CompanySettings::where('company_id',$compid)->update($data);
				}
			}
			
			
			
		}
		
	}
	
		public function myrequirementsettingAction(Request $request)
		{
		    $user = Auth::user();
			$compid = $user->company_id;
			$userid = $user->user_id;
			$roleid = $user->role_id;
			$userRole = array('1','3','5');
			if(in_array($roleid , $userRole))
			{
				$data = array();
				$lp_req_value = $request->input('lp_req_value');
				$assess_req_value = $request->input('assess_req_value');
				$assign_req_value = $request->input('assign_req_value');
				$data['lp_complete'] 		 = $lp_req_value; 
				$data['assessment_complete'] = $assess_req_value; 
				$data['assignment_complete'] = $assign_req_value; 
				CompanySettings::where('company_id',$compid)->update($data);
				
			}
		
		}
		
		public function subadminpermissionsettingAction(Request $request)
		{
			$user = Auth::user();
			$compid = $user->company_id;
			$userid = $user->user_id;
			$roleid = $user->role_id;
			$userRole = array('1','3','5');
			if(in_array($roleid , $userRole))
			{
				$data = array();
				$user_based_on = $request->input('user_based_on');
				$user_edit_permission = $request->input('user_edit_permission');
				$user_based_on_instructor = $request->input('user_based_on_instructor');
				$user_edit_permission_instructor = $request->input('user_edit_permission_instructor');
				$data['user_based_on'] 		 = $user_based_on; 
				$data['user_edit_permission'] = $user_edit_permission; 
				$data['user_based_on_instructor'] = $user_based_on_instructor; 	
				$data['user_edit_permission_instructor'] = $user_edit_permission_instructor;
				$checkdata = Admin::chkSubAdminPerSetting($compid);
				if(count($checkdata) > 0){			
					SubadminPermissions::where('company_id',$compid)->update($data);
				}
				else{
					$data['company_id'] = $compid ;
					SubadminPermissions::create($data);
				}	
			}
			
		}
		
		
		
}
