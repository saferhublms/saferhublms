<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,CommonHelper;
use App\Http\AdminTraits\Notificationcontent;
use App\Models\NotificationMaster;

class NotificationmanagementsController extends Controller
{
    public function allnotificationmgtsAction() 
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3','5');
		if(in_array($roleid, $userRole)) 
		{
			$fetchallvalues=Notificationcontent::getSUallNotificationlist();
			
			$dataArray=array();
			$i=1;
			if(count($fetchallvalues)>0)
			{
				foreach($fetchallvalues as $row)
				{
					$check='';
					$notification_id = CommonHelper::getEncode($row->notification_id);
					$notification_name = $row->notification_name;
					$is_show = $row->activenotification;
					$hiderow = true;
					if($is_show == 0){
						if($compid == 1){
							$hiderow = true;
						}else{
							$hiderow = false;
						}
					}
					if($hiderow)
					{
						$name = $notification_name;
						$dataArray[]=array('notification_name'=>$name,'is_show'=>$is_show,'notification_id'=>$notification_id);
					}
				}
			}
			
			return response()->json(['gridData'=>$dataArray]);
			
		}
	}
	
	public function shownotificationAction(Request $request) 
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3');
		if(in_array($roleid, $userRole)) 
		{
			$notification_id  = $request->input('notification_id');
			$hideshow  = $request->input('hideshow');
			if(!is_numeric($notification_id))
			{ $notification_id = CommonHelper::getDecode($notification_id); }
		     Notificationcontent::updateNotificationShow($hideshow, $notification_id, $compid);
		}
	}
	
	public function editAction(Request $request) 
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3');
		if(in_array($roleid, $userRole)) 
		{
			$notification_id = $request->input('notification_id');
			$defaltid = $request->input('is_default');
			if(!is_numeric($notification_id))
			{ $notification_id = CommonHelper::getDecode($notification_id); }
			$notification =Notificationcontent::getNotificationContent($notification_id,$defaltid, $compid);
			$notification_custom_field = Notificationcontent::getNotificationCustomField($notification_id);
			$result =array();
			if(count($notification_custom_field)>0)
			{
				foreach($notification_custom_field as $val)
				{
					$company_ids =explode(',',$val['company_ids']);	
					$company_arr=array();
					foreach($company_ids as $res){
						$company_arr[]=$res;
					}					
					if($val['enable_disable']==1 && (in_array($compid,$company_arr))){
						$result[] =array('fi'=>$val['custom_field_name'],'se'=>$val['custom_field_code']);
					}
					if(!in_array($compid,$company_arr) && $val['enable_disable']==2){
						$result[] =array('fi'=>$val['custom_field_name'],'se'=>$val['custom_field_code']);
					}
					if($val['enable_disable']==0){
					  $result[] =array('fi'=>$val['custom_field_name'],'se'=>$val['custom_field_code']);
					}					
				}	
			}
			return response()->json(['result'=>$result,'notification'=>$notification]);
		}
		
	}
	
	public function updatenotificationAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3');
		$notificationData=$request->input('notificationData');
		$notification_id=$request->input('notification_id');
		if($notificationData){
			$data = array();
			$data['subject']     		= addslashes($notificationData[0]['subject']);
			$data['notification_name'] = addslashes($notificationData[0]['notification_name']);
			$data['notification_content'] 			= addslashes($notificationData[0]['notification_content']);
			NotificationMaster::where('notification_id',$notificationData[0]['notification_id'])->update($data);
			
		}
	}
	
	public function getnotifications(Request $request)
		{
			
			$user = Auth::user();
			$compid = $user->company_id;
			$userid = $user->user_id;
			
			$start=$request->input('start');
			$length=$request->input('length');
			$search = $request->input('search');
			$draw = $request->input('draw');
			
			$notifications=NotificationMaster::leftjoin('notification_company as nc',function($query) use($compid){
				                   $query->on('notification_master.notification_id','=','nc.notification_id')
								         ->where('nc.company_id','=',$compid);
			                        })
									->leftjoin('notification_content as ncc',function($query) use($compid){
				                   $query->on('notification_master.notification_id','=','ncc.notification_id')
								         ->where('ncc.company_id','=',$compid);
			                        })
									->select('notification_master.notification_id','notification_master.notification_name','notification_master.is_active as activenotification','nc.is_active')
			                       ->get();
			
	
			return response()->json(['data'=>$notifications]);
									
		}
		
		public function updatestatusAction(Request $request)
		{
			$user = Auth::user();
			$compid = $user->company_id;
			$notificationId=$request->input('notificationId');
			$status=$request->input('status');
			Notificationcontent::updateNotificationStatus($status, $notificationId, $compid);
			return response()->json(['status'=>201,'message'=>'success']);
		}
		
		public function resoredefaultAction(Request $request)
		{
			$user = Auth::user();
			$compid = $user->company_id;
			$notificationId=$request->input('notificationId');
			Notificationcontent::updateNotificationDefault('1', $notificationId, $compid);
			return response()->json(['status'=>201,'message'=>'success']);
		}
		
		
}
