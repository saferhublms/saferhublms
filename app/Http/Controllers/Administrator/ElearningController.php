<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,CommonHelper;
use App\Models\Course;
use App\Models\UploadedFile;
use App\Models\TrainingPrograms;
use App\Models\CourseDetail;
use App\Models\CourseSkillLevelCredit;
use App\Models\CourseCategory;
use App\Models\CourseNotification;
use ZipArchive,CommonFunctions;
use App\Repositories\ScormRepository;
use App\Repositories\AiccparserRepository;
use App\Repositories\Xml2ArrayRepository;
use App\Models\CoursePermission;
use App\Models\CourseTargetAudience;
use App\Models\CourseTag;
use App\Models\Notification;
use App\Models\GroupMaster;
use App\Models\UserMaster;
use App\Models\TrainingCategory;
use App\Models\TrainingType;
use App\Models\HandoutsMaster;
use App\Http\Traits\Trainingprogram;
use \stdClass,DB,DateTime,DateTimeZone;
class ElearningController extends Controller
{
	
	public function __construct(AiccparserRepository $AiccparserRepository,ScormRepository $ScormRepository,Xml2ArrayRepository $Xml2ArrayRepository){
		$this->AiccparserRepository     = $AiccparserRepository;
		$this->ScormRepository 		    = $ScormRepository;
		$this->Xml2ArrayRepository 		= $Xml2ArrayRepository;
	}
	
    public function getReferenceCode(Request $request)
	{
		$userid	= Auth::user()->user_id;
		$compid	= Auth::user()->company_id;
		$roleid	= Auth::user()->role_id;
		$genratedCode=CommonHelper::trainingCode($request);
		return $genratedCode;
		
	}
	public function updateTrainingStatusAction(Request $request)
	{

		$items=$request->input('items');
		$tid=$request->input('tid');
		if($items[$tid]=="1")
		{
			TrainingPrograms::where('training_program_code', $tid)
          ->update(['status_id' => 1]);
		}
		elseif($items[$tid]=="0")
		{
			TrainingPrograms::where('training_program_code', $tid)
          ->update(['status_id' => 0]);
		}

	}
	public function updateMultipleTrainingStatusAction(Request $request)
	{
		$data=$request->input('data');
		$value=$request->input('value');
		for($i=0;$i<count($data); $i++) 
		{
			if ($value=="1")
			 {
				TrainingPrograms::where('training_program_code',$data[$i]['key'])
          ->update(['status_id' => 1]);
			}
			elseif($value=="0")
			{
				TrainingPrograms::where('training_program_code',$data[$i]['key'])
          ->update(['status_id' => 0]);
			}
			
		}
		return response()->json(['status'=>200,'message'=>'Status Successfully Updated']);

	}
	
	public function saveFirstCourseData(Request $request)
	{
		$compid	= Auth::user()->company_id;
		$courseName=$request->input('courseName');
		$courseTitle=$request->input('courseTitle');
		if($courseTitle){
			$courseExist=CourseDetail::where('course_name',$courseName)->where('course_title',$courseTitle)->where('company_id',$compid)->first();
			$responseText='Course name and course title already exists.';
		}else{
			$courseExist=CourseDetail::where('course_name',$courseName)->where('company_id',$compid)->first();
			$responseText='Course name already exists.';
		}
		if($courseExist){
			return response()->json(['status'=>401,'message'=>$responseText]);
		}else{
			return response()->json(['status'=>200,'message'=>'success.']);
		}
	}
	
	public function uploadfileAction(Request $request) {
		$passing_score=$request->input('passing_score');
	    $compid	= Auth::user()->company_id;
		$media_id = $request->input('media_type');
		
		if($media_id!=10){
		$support_media = explode("|",CommonHelper::getmediatypes($media_id));
		$path = 'administrator/mediafiles/'.$compid.'/';
		
		$status = "success";
		
		$file=$request->file('courseFile');
		
		$pref=$compid.'_'.CommonHelper::random_string(12).'_';
		
		$filenameOrignal = $file->getClientOriginalName();
		
		$file_size = $file->getClientSize();
		$file_type = $file->getClientOriginalExtension();
		$prefFilename=$pref.$filenameOrignal;
		$filename = $prefFilename;
		$file_url = '/administrator/mediafiles/'.$compid.'/'.$prefFilename;
		$media_title = $filenameOrignal;
		
		
		$masteryscore = ($passing_score)?$passing_score:'';
		$filenamearray =array();
		$filenamearray =explode(".",$filename);			
		$filepos = count($filenamearray);
		
		$fileext = strtolower(end($filenamearray));
		$filname2 = str_replace(".".$fileext,"",$filename); 

		$data1 = $xmltext = $ver = $manifest = $parent_root = $organization = $title = $identifier = $launch = $scorm_type = $packageType = $manifestfile = '';
		
			//3 for SCORM/AICC and ziped documents
		if(in_array($media_id,array(3,6,11)))
			{
				if($fileext == 'zip'){
					$zip = new ZipArchive;
					$res = $zip->open($file);
					if ($res === TRUE) {
					$zip->extractTo($path.$filname2);
					$zip->close();
				}else {
					var_dump($res);
				}
				}
				
				if($fileext == 'rar'){
					$rar_file = rar_open($path.$filname);
					$entries = rar_list($rar_file);
					foreach ($entries as $entry) {
						$entry->extract($path.$filname2);
					}
					rar_close($rar_file);
				}
				
				//3 for SCORM/AICC
				if(in_array($media_id,array(3,11))){
					
					  $manifestfile = $path.$filname2.'/imsmanifest.xml';
					  if(file_exists($manifestfile)){
							$packageType = 'SCORM';
						}else{
							$packageType = 'AICC';
						}
						
						if($packageType == 'SCORM'){
							//require_once 'xml2Array.php';
							//$objXML = new xml2Array();
							$scmfunction = $this->ScormRepository;
							
							$xmltext = file_get_contents($manifestfile);
							$pattern = '/&(?!\w{2,6};)/';
							$replacement = '&amp;'; 
							$manifests = $this->Xml2ArrayRepository->parse($xmltext);
							
							
							$scoes = new \stdClass();
							$scoes->version = '';  
							$scoes=$scmfunction->scorm_get_manifest($manifests, $scoes);
							
							$newitem = new \stdClass();
							$newitem->version=$scoes->version;
							$newitem->defaultorg=$scoes->defaultorg;
							$newitem->parent_root="/";
							$scorm_parsed_data=new \stdClass();
							$data1 = count($scoes->elements);
							if(count($scoes->elements) > 0)
							{
								foreach ($scoes->elements as $manifest => $organizations) {
									foreach ($organizations as $organization => $items) {
										foreach ($items as $identifier => $item) {                     
											@$newitem->scorm = $scormid;
											@$newtem->manifest = $manifest;
											$newitem->organization = $organization;
											$standarddatas = array('parent', 'identifier', 'launch', 'scormtype', 'title','masteryscore');
											foreach ($standarddatas as $standarddata) {
												if (isset($item->$standarddata)) {
													$newitem->$standarddata = $scmfunction->addslashes_js($item->$standarddata);   //values got should be insertable..         
													$data1 = $data1.$newitem->$standarddata;
												}
											} 
										}
									}
								}
							}
							$manifestfile=$path.$filname2;
							$threshold = $scmfunction->get_scorm_threshold_value($manifestfile.'/');
							$ver = $newitem->version;
							$identifier = $newitem->identifier;
							$title = $newitem->title;
							$launch = $newitem->launch;
							$scorm_type = $newitem->scormtype;
							$parent_root = $newitem->parent_root;
							//$masteryscore = (isset($newitem->masteryscore)? $newitem->masteryscore: $threshold);							
							$masteryscore = $passing_score;
						}else{
								$reg_exp_aicc_au_course = "/\.au/";
								$reg_exp_aicc_crs_course = "/\.crs/";
								$files = $this->AiccparserRepository->read_all_files($path.$filname2);
								$aicc_au_file_path = $this->AiccparserRepository->check_aicc_file($files, $reg_exp_aicc_au_course);
								if(isset($aicc_au_file_path)){
								$key_value_array = array();
								$threshold = 100;
								$key_value_array = $this->AiccparserRepository->read_aicc_au_file($aicc_au_file_path, $key_value_array);
								$aicc_crs_file_path = $this->AiccparserRepository->check_aicc_file($files, $reg_exp_aicc_crs_course);
								$key_value_array = $this->AiccparserRepository->read_aicc_crs_file($aicc_crs_file_path,$key_value_array);
								
								$organization = $key_value_array['course_creator'];
								$ver = "AICC";
								$title = $key_value_array['course_title'];
								$identifier = $key_value_array['course_id'];
							
								$launch = (isset($key_value_array['file_name']) ? $key_value_array['file_name'] : "");
								$scorm_type = $key_value_array['type'];
								$parent_root = "/";
								//$masteryscore = (isset($key_value_array['mastery_score']) && $key_value_array['mastery_score'] != ''? $key_value_array['mastery_score']: $threshold);
								$masteryscore = $passing_score;

								}
						}
				}
			}
			
			
			$file_title_arr = explode("_",$filname2);
			unset($file_title_arr[0]);
			unset($file_title_arr[1]);
			$file_title = implode("_",$file_title_arr);
			$is_lib = 0;
				// for Audio/Video/Document/SCORM/Aicc/Zip
			//if(in_array($media_id,array(2,8,1,3,6,11))){
			$file->move($path,$filname2.".".$fileext);
			//}
			
			$data=array();
			$data['file_name'] = $filname2.".".$fileext;
			$data['file_path'] = "/".$path."".$filname2.".".$fileext;
			$data['format_id'] = 1;
			$data['file_size'] = $file_size;
			$data['is_active'] = 1;
			$data['package_type'] = $packageType;
			$data['data1'] = "";
			$data['data2'] = "";
			$data['manifest'] = ($manifest!='') ? $manifest : 0 ;
			$data['version'] = $ver;
			$data['organization'] = $organization;
			$data['title'] = $title;
			$data['identifier'] = $identifier;
			$data['launch'] = $launch;
			$data['scorm_type'] = $scorm_type;
			$data['parent_root'] = $parent_root;
			$data['media_id'] = $media_id;
			$data['media_title'] = (($media_title!="")?$media_title:$file_title);
			$data['file_title'] = $file_title;
			$data['masteryscore'] = $masteryscore ;
			$data['company_id'] = $compid;
			$data['is_lib'] = $is_lib;
		
			
			$lastinsertedid = UploadedFile::create($data);
			
			if($media_id==7){
				CommonFunctions::convert2pdf($filename,$path,$lastinsertedid->files_id,'yes');
			}
			
			if($media_id==4){
				CommonFunctions::convert2pdf($filename,$path,$lastinsertedid->files_id,'no');
			}
			
			
			return response()->json(['uploadedId'=>$lastinsertedid->files_id]);
			
		}else{
			//for link
			$data['file_name'] = '';
				$data['file_path'] = '';
				$data['format_id'] = 1;
				$data['file_size'] = 0;
				$data['is_active'] = 1;
				$data['package_type'] = '';
				$data['data1'] = "";
				$data['data2'] = "";
				$data['version'] = '';
				$data['manifest'] = 0;
				$data['version'] = '';
				$data['organization'] = '';
				$data['title'] = '';
				$data['identifier'] = '';
				$data['launch'] = '';
				$data['scorm_type'] = '';
				$data['parent_root'] = '';
				$data['url'] =$request->input('embededcode');
				$data['media_id'] = $request->input('media_type');
				$data['media_title'] = '';
				$data['added_status']='yes';
				$data['company_id'] = $compid;
				$lastinsertedid = UploadedFile::create($data);
			    return response()->json(['uploadedId'=>$lastinsertedid->files_id]);
		}
			
			
			
	
	}
	
	public function deletefileAction(Request $request)
	{
		$files_id = $request->input('fileId');
		UploadedFile::where('files_id',$files_id)->delete();
		return response()->json(['status'=>201,'message'=>'success']);
	}
	
	public function saveCourseAction(Request $request)
	{
		$compid	= Auth::user()->company_id;
		$userid	= Auth::user()->user_id;
		$course=json_decode($request->input('course'),true);
		$courseDetail=json_decode($request->input('courseDetail'),true);
		$trainingPrograms=json_decode($request->input('trainingPrograms'),true);
		$referenceCode=$request->input('referenceCode');
		$isCourseImage=$request->input('isCourseImage');
		$userAllSkills=json_decode($request->input('userAllSkills'));
		$categories=json_decode($request->input('categories'));
		$checkNotification=json_decode($request->input('checkNotification'));
		$targetAudienceGroups=$request->input('targetAudienceGroups');
		$targetAudienceusers=$request->input('targetAudienceusers');
		$permissionGroups=$request->input('permissionGroups');
		$permissionUsers=$request->input('permissionUsers');
		$courseTags=$request->input('courseTags');

		//print_r($userAllSkills);exit;
		if(!$referenceCode){
			$referenceCode=CommonHelper::trainingCode($request);
		}else{
			$trainingCodeExist=TrainingPrograms::where('training_program_code',$referenceCode)->first();
			if($trainingCodeExist){
					$referenceCode=CommonHelper::trainingCode($request);
			}
		}
		$trainingPrograms['training_program_code']=$referenceCode;
		$trainingPrograms['company_id']=$compid;
		$trainingPrograms['created_by']=$userid;
		$trainingPrograms['updated_by']=$userid;
		TrainingPrograms::create($trainingPrograms);
			
			$courseImage='';
		if($isCourseImage==1){
			$file=$request->file('courseImage');
			$path = 'administrator/mediafiles/'.$compid.'/';
			$pref=$compid.'_'.CommonHelper::random_string(12).'_';
			$filenameOrignal = $file->getClientOriginalName();
			$prefFilename=$pref.$filenameOrignal;
			$filename = $prefFilename;
			$filenamearray =array();
			$filenamearray =explode(".",$filename);			
			$filepos = count($filenamearray);
			
			$fileext = strtolower(end($filenamearray));
			$filname2 = str_replace(".".$fileext,"",$filename); 
			$file->move($path,$filname2.".".$fileext);
			$courseImage="/".$path.$filname2.".".$fileext;
			
		}
		
		$course['training_program_code']=$referenceCode;
		$course['course_image']=$courseImage;
		$course['company_id']=$compid;
		$course['created_by']=$userid;
		$course['updated_by']=$userid;
		$course['published_date']=date('Y-m-d H:i:s');

		$courseSave=Course::create($course);
		$courseId=$courseSave->course_id;
		
		   $courseDetail['course_id']=$courseId;
		   $courseDetail['lang_id']=1;
		   $courseDetail['company_id']=$compid;
		   $courseDetail['created_by']=$userid;
		   $courseDetail['updated_by']=$userid;
		   CourseDetail::create($courseDetail);
		
		   $courseTag=new CourseTag;
		   $courseTag->course_id=$courseId;
		   $courseTag->tags=$courseTags;
		   $courseTag->save();
		
		

		for($i=0;$i<count($userAllSkills);$i++){
			if($userAllSkills[$i]->courseSkill){
			$courseSkillCredit=new CourseSkillLevelCredit;
			$courseSkillCredit->course_id=$courseId;
			$courseSkillCredit->training_type=$trainingPrograms['training_type'];
			$courseSkillCredit->course_credit=$userAllSkills[$i]->creditValue;
			$courseSkillCredit->skill_level_credit=$userAllSkills[$i]->skillLevelCredit;
			$courseSkillCredit->level_id=$userAllSkills[$i]->courseSkillLevels;
			$courseSkillCredit->skill_id=$userAllSkills[$i]->courseSkill;
			$courseSkillCredit->company_id=$compid;
			$courseSkillCredit->created_by=$userid;
			$courseSkillCredit->updated_by=$userid;
			$courseSkillCredit->save();
			}
		}
		
		for($i=0;$i<count($categories);$i++){
			
			if(is_numeric($categories[$i])){
				$category_id=$categories[$i];
			}else{
				$category_id=$this->saveTrainingCategory($categories[$i]);
				
			}
			
			$courseCategory=new CourseCategory;
			$courseCategory->course_id=$courseId;
			$courseCategory->training_type=$trainingPrograms['training_type'];
			$courseCategory->training_category_id=$category_id;
			$courseCategory->created_date=date("Y-m-d H:i:s");
			$courseCategory->company_id=$compid;
			$courseCategory->created_by=$userid;
			$courseCategory->updated_by=$userid;
			$courseCategory->save();
			
		}
        
		for($i=0;$i<count($checkNotification);$i++){
			$courseNotification=new CourseNotification;
			$courseNotification->course_id=$courseId;
			$courseNotification->notification_id=$checkNotification[$i];
			$courseNotification->is_active=1;
			$courseNotification->company_id=$compid;
			$courseNotification->created_by=$userid;
			$courseNotification->updated_by=$userid;
			$courseNotification->save();
		}
		
		
		   $coursePermission=new CoursePermission;
		   $coursePermission->course_id=$courseId;
		   $coursePermission->group_id=$permissionGroups;
		   $coursePermission->user_id=$permissionUsers;
		   $coursePermission->created_by=$userid;
		   $coursePermission->updated_by=$userid;
		   $coursePermission->save();
		   
		   $CourseTargetAudience=new CourseTargetAudience;
		   $CourseTargetAudience->course_id=$courseId;
		   $CourseTargetAudience->group_id=$targetAudienceGroups;
		   $CourseTargetAudience->user_id=$targetAudienceusers;
		   $CourseTargetAudience->created_by=$userid;
		   $CourseTargetAudience->updated_by=$userid;
		   $CourseTargetAudience->training_type=$trainingPrograms['training_type'];
		   $CourseTargetAudience->company_id=$compid;
		   $CourseTargetAudience->save();
		   
		
		return response()->json(['status'=>201,'message'=>'success','trainingCode'=>$referenceCode,'courseId'=>$courseId]);
        
		
		
		
	}
	
	public function saveembededcode(Request $request)
	{
		$compid	= Auth::user()->company_id;
		$embededTitle=$request->input('embededTitle');
		$embededCode=$request->input('embededCode');
		$embededCodefileId=$request->input('embededCodefileId');
		$media_id=$request->input('mediaType');
		if($embededCodefileId){
			$data['media_id'] = $media_id;
			$data['embedded_code'] = $embededCode;
			$data['media_title'] = $embededTitle;
			UploadedFile::where('files_id',$embededCodefileId)->update($data);
			return response()->json(['uploadedId'=>$embededCodefileId]);
				
		}else{
			$data['file_name'] = '';
			$data['file_path'] = '';
			$data['format_id'] = 1;
			$data['file_size'] = 0;
			$data['is_active'] = 1;
			$data['package_type'] = '';
			$data['data1'] = "";
			$data['data2'] = "";
			$data['version'] = '';
			$data['manifest'] = 0;
			$data['version'] = '';
			$data['organization'] = '';
			$data['title'] = '';
			$data['identifier'] = '';
			$data['launch'] = '';
			$data['scorm_type'] = '';
			$data['parent_root'] = '';
			$data['embedded_code'] = $embededCode;
			$data['media_id'] = $media_id;
			$data['media_title'] = $embededTitle;
			$data['company_id'] = $compid;
			$lastinsertedid = UploadedFile::create($data);
			return response()->json(['uploadedId'=>$lastinsertedid->files_id]);
		}
		
	}
	
	public function elearningListAction(Request $request)
	{
		$category=$request->input('category');
		$training=$request->input('training');
		date_default_timezone_set('America/Toronto');	
		$compid	= Auth::user()->company_id;
		$start=$request->input('start');
		$length=$request->input('length');
		$search = $request->input('search');
		 $draw = $request->input('draw');
		 $order = $request->input('order');
		 $orderColumn=$order[0]['column'];
		 $orderType=$order[0]['dir'];
		 if($orderColumn==0){
			 $column='course_details.course_title';
		 }
		
		$elerningCourseList=Course::join('course_details','course_details.course_id','=','courses.course_id')
		                          ->join('training_programs','training_programs.training_program_code','=','courses.training_program_code')
								  ->leftjoin('media_types','training_programs.media_type_id','=','media_types.media_id')
								  ->join('course_categories','course_categories.course_id','=','courses.course_id')
								  ->join('training_category','training_category.training_category_id','=','course_categories.training_category_id')
								  ->leftjoin('training_type','training_type.training_type_id','=','training_programs.training_type')
								  ->groupBy('courses.course_id')
								  ->where('courses.company_id',$compid)
								  ->where('courses.status_id',1)
								  ->orderBy($column,$orderType)
								  ->select('course_details.course_title','training_programs.training_program_code','training_programs.status_id','media_types.media_name','courses.course_id',DB::raw('group_concat(training_category.category_name) as categoryName'),'courses.course_image','training_type.training_type','course_details.course_name','training_programs.training_type as trainingTypeId','courses.created_at');
								  
		
		if( $search['value']){
			$elerningCourseList=$elerningCourseList->where(function($query) use($search){
				    $query->where('course_details.course_name','like','%'.$search['value'].'%')
					      ->orWhere('course_details.course_title','like','%'.$search['value'].'%')
					      ->orWhere('media_types.media_name','like','%'.$search['value'].'%');
			});	
		}
			if($category){
			$elerningCourseList=$elerningCourseList->where(function($query) use($category){
				    $query->where('training_category.category_name',$category);

			});	
		}
			if($training){
			$elerningCourseList=$elerningCourseList->where(function($query) use($training){
				    $query->where('training_type.training_type',$training);

			});	
		}

		$eleningCourseCount=count($elerningCourseList->get());	
		$elerningCourseList=$elerningCourseList->skip($start)->take($length)->get();
		//echo '<pre>';print_r($elerningCourseList);exit; 
		/* for($i=0;$i<count($elerningCourseList);$i++){
			if(!$elerningCourseList[$i]->media_name){
				$elerningCourseList[$i]->media_name='';
			}
		} */
	/* 	echo $elerningCourseList[0]->created_at;echo '<br>';
		$date = new DateTime($elerningCourseList[0]->created_at, new DateTimeZone('UTC'));
echo $date->format('Y-m-d H:i:s');echo '<br>';
$date->setTimezone(new DateTimeZone('America/Toronto'));
echo $date->format('Y-m-d H:i:s');echo '<br>';
exit;
		echo $elerningCourseList[0]->created_at;exit;
		echo '<pre>';print_r($elerningCourseList);exit; */
		//this is for droupdown listing of Training_type And Training_category
$categoryName =TrainingCategory::whereIn('is_active', [0,1])
               ->pluck('category_name');

$trainingType =TrainingType::whereIn('is_active', [0,1])
               ->pluck('training_type');

		return response()->json(['data'=>$elerningCourseList,'count'=>$eleningCourseCount,'draw'=>$draw,'CategoryName'=>$categoryName,'TrainingType'=>$trainingType]);
	}
	
	
	public function getelearningdataAction(Request $request)
	{
		$couseTags=$groupAudienceTag=$userAudienceTag=$groupEditTag=$userEditTag=array();
		$code=$request->input('code');
		
		$elearningData=Course::join('course_details','course_details.course_id','=','courses.course_id')
								->join('training_programs','training_programs.training_program_code','=','courses.training_program_code')
								->where('training_programs.training_program_code',$code)
								->select('courses.course_image','courses.credit_value','courses.points','course_details.course_name','course_details.course_title','course_details.course_description','training_programs.media_type_id','courses.course_id','courses.media_library_id','courses.certificate_template_id','training_programs.status_id')
								->first();
		$elearningData->status_id=(string)$elearningData->status_id;
        $elearningData->couseCategory=CourseCategory::join('training_category','training_category.training_category_id','=','course_categories.training_category_id')
						             ->where('course_categories.course_id',$elearningData->course_id)
									 ->select('training_category.training_category_id','training_category.category_name')
									 ->get();
		                          
        $courseTag=CourseTag::where('course_id',$elearningData->course_id)->select('course_tag_id','tags')->first();
		if($courseTag){
			$alltags =$courseTag->tags!='undefined'?$courseTag->tags:json_encode(array());
			
			$courseTagDecode=json_decode($alltags);
		}else{
			$courseTagDecode=array();
		}
		
		for($i=0;$i<count($courseTagDecode);$i++){
			$couseTags[]=array('course_tag_id'=>$elearningData->course_id+$i,'tags'=>$courseTagDecode[$i]);
		}
		$elearningData->courseTags=$couseTags;
		
		$elearningData->media=UploadedFile::where('files_id',$elearningData->media_library_id)->first();
		
		$notificationSet=CourseNotification::where('course_id',$elearningData->course_id)->pluck('notification_id')->toArray();
		$NotificationType=Notification::select('notification_id','notification','notification_type as notificationType')->get();
		for($i=0;$i<count($NotificationType);$i++){
			if(in_array($NotificationType[$i]->notification_id,$notificationSet)){
				$NotificationType[$i]->checkNotification=1;
			}else{
				$NotificationType[$i]->checkNotification='';
			}
		}
		
		$courseSkills=CourseSkillLevelCredit::where('course_id',$elearningData->course_id)->get();
		
		$elearningData->courseSkill=$courseSkills;
		$elearningData->notification=$NotificationType;
		
		$targetAudience=CourseTargetAudience::where('course_id',$elearningData->course_id)->first();
		$CoursePermission=CoursePermission::where('course_id',$elearningData->course_id)->first();
		if($targetAudience){
			$usertarget=$targetAudience->user_id!='undefined'?$targetAudience->user_id:json_encode(array());
			$decodedGroupId=json_decode($targetAudience->group_id);
			$decodedUserId=json_decode($usertarget);
		}else{
			$decodedGroupId=array();
			$decodedUserId=array();
		}
		
		
		for($i=0;$i<count($decodedGroupId);$i++){
			$groupName=GroupMaster::where('group_id',$decodedGroupId[$i])->select('group_name')->first();
			$groupAudienceTag[]=array('group_id'=>$decodedGroupId[$i],'group_name'=>$groupName->group_name);
		}
		for($i=0;$i<count($decodedUserId);$i++){
			$userName=UserMaster::where('user_id',$decodedUserId[$i])->select('user_name')->first();
			$userAudienceTag[]=array('user_id'=>$decodedUserId[$i],'user_name'=>$userName->user_name);
		}
		$elearningData->groupAudienceTag=$groupAudienceTag;
		$elearningData->userAudienceTag=$userAudienceTag;
		if($CoursePermission){
			$decodedGroupId=json_decode($CoursePermission->group_id);
			$decodedUserId=json_decode($CoursePermission->user_id);
		}else{
			$decodedGroupId=array();
			$decodedUserId=array();
		}
		
		for($i=0;$i<count($decodedGroupId);$i++){
			$groupName=GroupMaster::where('group_id',$decodedGroupId[$i])->select('group_name')->first();
			$groupEditTag[]=array('group_id'=>$decodedGroupId[$i],'group_name'=>$groupName->group_name);
		}
		for($i=0;$i<count($decodedUserId);$i++){
			$userName=UserMaster::where('user_id',$decodedUserId[$i])->select('user_name')->first();
			$userEditTag[]=array('user_id'=>$decodedUserId[$i],'user_name'=>$userName->user_name);
		}
		$elearningData->groupEditTag=$groupEditTag;
		$elearningData->userEditTag=$userEditTag;
		
		$handOuts=HandoutsMaster::where('course_id',$elearningData->course_id)->where('status',1)->get();
		//print_r($handOuts);exit;
		$elearningData->handouts=$handOuts;
		
		return response()->json(['status'=>201,'data'=>$elearningData]);
 								
								
		
	}
	
	public function editCourseAction(Request $request)
	{
		$newCourseTage=$audienceGroupArray=$audienceUserArray=$preminssionGroup=$permissionUser=array();
		$compid	= Auth::user()->company_id;
		$userid	= Auth::user()->user_id;
		$code=$request->input('code');
		
		$course=json_decode($request->input('course'),true);
		$courseDetail=json_decode($request->input('courseDetail'),true);
		$trainingPrograms=json_decode($request->input('trainingPrograms'),true);
		$isCourseImage=$request->input('isCourseImage');
		$userAllSkills=json_decode($request->input('userAllSkills'));
		$categories=json_decode($request->input('categories'));
		$checkNotification=json_decode($request->input('checkNotification'));
		$targetAudienceGroups=json_decode($request->input('targetAudienceGroups'));
		$targetAudienceusers=json_decode($request->input('targetAudienceusers'));
		$permissionGroups=json_decode($request->input('permissionGroups'));
		$permissionUsers=json_decode($request->input('permissionUsers'));
		$courseTags=json_decode($request->input('courseTags'),true);
		
		
		
		$courseDetails=Course::where('training_program_code',$code)->first();
		$courseImage=$courseDetails->course_image;
		if($isCourseImage==1){
			$file=$request->file('courseImage');
			$path = 'administrator/mediafiles/'.$compid.'/';
			$pref=$compid.'_'.CommonHelper::random_string(12).'_';
			$filenameOrignal = $file->getClientOriginalName();
			$prefFilename=$pref.$filenameOrignal;
			$filename = $prefFilename;
			$filenamearray =array();
			$filenamearray =explode(".",$filename);			
			$filepos = count($filenamearray);
			
			$fileext = strtolower(end($filenamearray));
			$filname2 = str_replace(".".$fileext,"",$filename); 
			$file->move($path,$filname2.".".$fileext);
			$courseImage="/".$path.$filname2.".".$fileext;
			
		}
		
		$course['course_image']=$courseImage;
		$course['updated_by']=$userid;
		$courseDetail['updated_by']=$userid;
		$trainingPrograms['updated_by']=$userid;
		$courseId=$courseDetails->course_id;
		Course::where('training_program_code',$code)->update($course);
		CourseDetail::where('course_id',$courseId)->update($courseDetail);
		TrainingPrograms::where('training_program_code',$code)->update($trainingPrograms);
		
		CourseSkillLevelCredit::where('course_id',$courseId)->delete();
		for($i=0;$i<count($userAllSkills);$i++){
			$courseSkillCredit=new CourseSkillLevelCredit;
			$courseSkillCredit->course_id=$courseId;
			$courseSkillCredit->training_type=1;
			$courseSkillCredit->course_credit=$userAllSkills[$i]->course_credit;
			$courseSkillCredit->skill_level_credit=$userAllSkills[$i]->skill_level_credit;
			$courseSkillCredit->level_id=$userAllSkills[$i]->level_id;
			$courseSkillCredit->skill_id=$userAllSkills[$i]->skill_id;
			$courseSkillCredit->company_id=$compid;
			$courseSkillCredit->created_by=$userid;
			$courseSkillCredit->updated_by=$userid;
			$courseSkillCredit->save();
		}
		
		CourseCategory::where('course_id',$courseId)->delete();
		for($i=0;$i<count($categories);$i++){
			$categorySelectId=isset($categories[$i]->training_category_id)?$categories[$i]->training_category_id:$categories[$i];
			$courseCategory=new CourseCategory;
			$courseCategory->course_id=$courseId;
			$courseCategory->training_type=1;
			$courseCategory->training_category_id=$categorySelectId;
			$courseCategory->created_date=date("Y-m-d H:i:s");
			$courseCategory->company_id=$compid;
			$courseCategory->created_by=$userid;
			$courseCategory->updated_by=$userid;
			$courseCategory->save();
			
		}
        CourseNotification::where('course_id',$courseId)->delete();
		for($i=0;$i<count($checkNotification);$i++){
			$courseNotification=new CourseNotification;
			$courseNotification->course_id=$courseId;
			$courseNotification->notification_id=$checkNotification[$i];
			$courseNotification->is_active=1;
			$courseNotification->company_id=$compid;
			$courseNotification->created_by=$userid;
			$courseNotification->updated_by=$userid;
			$courseNotification->save();
		}
		    $courseExistingTags=CourseTag::where('course_id',$courseId)->select('tags')->first();
			 if($courseExistingTags){
				$courseExistingTagsDecode=json_decode($courseExistingTags->tags); 
			 }else{
				 $courseExistingTagsDecode=array();
			 }
			  CourseTag::where('course_id',$courseId)->delete();
			
			   for($i=0;$i<count($courseTags);$i++){
				   if(isset($courseTags[$i]['course_tag_id'])){
					   $tag=$courseTags[$i]['tags'];
				   }
				   
				   else{
					   if(is_numeric($courseTags[$i])){
						  $tag= $courseExistingTagsDecode[$courseTags[$i]-$courseId];
					   }else{
						  $tag= $courseTags[$i];
					   }
				   }
				   $newCourseTage[]=$tag;
				   
				   
			   }
		   $courseTag=new CourseTag;
		   $courseTag->course_id=$courseId;
		   $courseTag->tags=json_encode($newCourseTage);
		   $courseTag->save();
		   
		   
		   CourseTargetAudience::where('course_id',$courseId)->delete();
		   
		   for($j=0;$j<count($targetAudienceGroups);$j++){
			$groupSelectId=isset($targetAudienceGroups[$j]->group_id)?$targetAudienceGroups[$j]->group_id:$targetAudienceGroups[$j];
				$audienceGroupArray[]=$groupSelectId;
			}
			for($j=0;$j<count($targetAudienceusers);$j++){
				$userSelectId=isset($targetAudienceusers[$j]->user_id)?$targetAudienceusers[$j]->user_id:$targetAudienceusers[$j];
				$audienceUserArray[]=$userSelectId;
			}
		    
		   $CourseTargetAudience=new CourseTargetAudience;
		   $CourseTargetAudience->course_id=$courseId;
		   $CourseTargetAudience->group_id=json_encode($audienceGroupArray);
		   $CourseTargetAudience->user_id=json_encode($audienceUserArray);
		   $CourseTargetAudience->created_by=$userid;
		   $CourseTargetAudience->updated_by=$userid;
		   $CourseTargetAudience->training_type=1;
		   $CourseTargetAudience->company_id=$compid;
		   $CourseTargetAudience->save();
		   
		   CoursePermission::where('course_id',$courseId)->delete();
		   
		   for($j=0;$j<count($permissionGroups);$j++){
			$groupSelectId=isset($permissionGroups[$j]->group_id)?$permissionGroups[$j]->group_id:$permissionGroups[$j];
				$preminssionGroup[]=$groupSelectId;
			}
			for($j=0;$j<count($permissionUsers);$j++){
				$userSelectId=isset($permissionUsers[$j]->user_id)?$permissionUsers[$j]->user_id:$permissionUsers[$j];
				$permissionUser[]=$userSelectId;
			}
			
		
		   $coursePermission=new CoursePermission;
		   $coursePermission->course_id=$courseId;
		   $coursePermission->group_id=json_encode($preminssionGroup);
		   $coursePermission->user_id=json_encode($permissionUser);
		   $coursePermission->created_by=$userid;
		   $coursePermission->updated_by=$userid;
		   $coursePermission->save();
		   
		    return response()->json(['status'=>201,'message'=>'success','courseId'=>$courseId]);
		   
		  
		
	}
	
	public function saveTrainingCategory($categoryName)
	{
		$compId=Auth::user()->company_id;
		$category=TrainingCategory::where('category_name',$categoryName)->first();
		if($category){
			return $category->training_category_id;
		}else{
			$traininCatSave=new TrainingCategory;
			$traininCatSave->category_name=$categoryName;
			$traininCatSave->company_id=$compId;
			$traininCatSave->is_active=1;
			$traininCatSave->is_delete=0;
			$traininCatSave->save();
			return $traininCatSave->training_category_id;
		}
	}
	
	public function uploadHandoutAction(Request $request)
	{
		$compid	= Auth::user()->company_id;
		$courseId=$request->input('courseId');
		$path = 'administrator/mediafiles/'.$compid.'/handouts/';
		
		$status = "success";
		
		$file=$request->file('handoutFile');
		
		$pref=$compid.'_'.CommonHelper::random_string(12).'_';
		
		$filenameOrignal = $file->getClientOriginalName();
		
		$file_size = $file->getClientSize();
		$file_type = $file->getClientOriginalExtension();
		$prefFilename=$pref.$filenameOrignal;
		$filename = $prefFilename;
		$file_url = '/administrator/mediafiles/'.$compid.'/handouts/'.$prefFilename;
		$media_title = $filenameOrignal;
		
		$masteryscore = '';
		$filenamearray =array();
		$filenamearray =explode(".",$filename);			
		$filepos = count($filenamearray);
		
		$fileext = strtolower(end($filenamearray));
		$filname2 = str_replace(".".$fileext,"",$filename);
		$file->move($path,$filname2.".".$fileext);
		
		$handOuts=new HandoutsMaster;
		$handOuts->handout_title=$media_title;
		$handOuts->file_name=$filename;
		$handOuts->handout_path=$file_url;
		$handOuts->course_id=$courseId;
		$handOuts->company_id=$compid;
		$handOuts->status=1;
		$handOuts->save();
		
		
	}
	
	public function getpreviewdataAction(Request $request)
	{
		$trainingprogram_code=$request->input('code');
        $sessiondata=Auth::user();
		$company_id=$sessiondata->company_id;
		$roleid=$sessiondata->role_id;
		$userid=$sessiondata->user_id;
	
		$courseDetailArr = $prerequeisites = $support_formats = array();
		$filenames = $usertrainingid = '';
		$media_id = $averageRating = $userAverageRating = 0;	
		$courseDetailArr['trainingprogram_code'] = $trainingprogram_code;
		$programs = $this->getElearningTrainingCatalogsDetail($company_id, $trainingprogram_code);
		
		if ($programs) 
		{
			$course_id = $programs->course_id;	
			$courseDetailArr['user_id'] 					= 	$userid;
			$courseDetailArr['course_id'] 					= 	$programs->course_id;
			$courseDetailArr['company_id'] 					= 	$company_id;
			$courseDetailArr['training_type'] 				= 	$programs->training_type;
			$courseDetailArr['credit_value'] 				= 	$programs->credit_value;
			$courseDetailArr['course_image'] 				= 	$programs->course_image;
			$courseDetailArr['training_title'] 				= 	$programs->training_title;
			$courseDetailArr['description'] 				= 	$programs->course_description;
			$courseDetailArr['prerequeisites'] 				= 	$programs->prerequeisites;
			$courseDetailArr['training_type_id']			= 	$programs->training_type_id;
			$courseDetailArr['training_program_code']		= 	$programs->training_program_code;
			$courseDetailArr['average_rating'] 				= 	$programs->average_rating;
		    $courseDetailArr['TrainingCategoryselected'] 	= 	$programs->category_name;	
			$courseDetailArr['type'] 					    = 	'';
			$courseDetailArr['userprogramtypeid'] 			=    1;
		    $alltargetaud 									= 	Trainingprogram::targetaudiences($company_id, $course_id);
			$targetaudthtml									=   $this->targetaudiencehtml($alltargetaud);
			$courseDetailArr['alltargetaud'] 				= 	$targetaudthtml;
			$courseDetailArr 								=   Trainingprogram::getMediaFileDetails($userid,$company_id,$course_id,$courseDetailArr); 
		}
		
		 $view='view';
		 $enrollelearning = array(
			"confirm"=>1 , 
			"training_program_code"=>$courseDetailArr['training_program_code'], 
			"userprogramtypeid"=>$courseDetailArr['userprogramtypeid'], 
			"course_id"=>$courseDetailArr['course_id'], 
			"fileDetails"=>$courseDetailArr['fileDetails'], 
			"media_id"=>$courseDetailArr['media_id'], 
			"file_id"=>$courseDetailArr['file_id'],
			"expiration_status"=>'', 
			"company_id"=>$company_id,
			"user_id"=>$userid,
			"view"=>$view
		);
		$courseDetailArr['checkhandout'] ='';
		$courseDetailArr['transcript_id'] = 0;			
		$courseDetailArr['userprogramtypeid'] =  1;		
		$response =array(
			  "status"=>200,
			  "courseData"=>$courseDetailArr,
			  "enrollelearning"=>CommonHelper::getEncode(json_encode($enrollelearning)),
		);
		return response()->json($response); 		
		
	}
	
	public function targetaudiencehtml($targetAudience){
		
			$targetstr='';
			
			if(count(@$targetAudience)>0)
			{
				$targetstr.= '<div style="height: 160px; overflow:auto; scrollbar-base-color:#9f9f9f; scrollbar-arrow-color:#9f9f9f;">';

				$job = 0;
				$grp = 0;
				foreach ($targetAudience as $key => $val) {
					if($val['type_name'] == 'user')
					{
						if($job == 0){
							$job++;

							$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Users</b></div>';
						 } 
							$targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
						   
					}else{
						if($grp == 0){
										$grp++;
							
									$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Groups</b></div>';
									 }
								  $targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
							
					  } 
				 }
				   $targetstr.='</div>';
				} 
			
			 return $targetstr;
	}
	
	public function getElearningTrainingCatalogsDetail($compid, $programcode) 
		{  
				$lang_id=1;
				$querydata=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid);
				})
				->join('training_type','training_type.training_type_id','=','training_programs.training_type')
				->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
				})
				->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
				})
				->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
				})
				->where('courses.company_id','=',$compid)->where('training_programs.training_program_code','=',$programcode)
				->select(DB::raw('training_programs.training_type as training_type_id,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type,if(training_programs.training_type=1,"ON Demand",CURDATE()) as recursive_date,courses.course_image,course_details.course_description,course_details.prerequeisites,courses.average_rating,training_programs.training_program_code,GROUP_CONCAT(distinct training_category.category_name) as category_name'))->first();
				return $querydata;
		}
	public function removeHandoutAction(Request $request)
	{
		$handoutId=$request->input('handoutId');
		HandoutsMaster::where('handout_id',$handoutId)->update(['status'=>0]);
		return response()->json(['message'=>'success','status'=>201]);						  
	}
	
	public function deletecourseAction(Request $request)
	{
		$courseCode=$request->input('courseCode');
		Course::where('training_program_code',$courseCode)->update(['status_id'=>2]);
		return response()->json(['status'=>201,'message'=>'success']);
	}
	
}
