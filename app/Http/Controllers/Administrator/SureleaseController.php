<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\AdminTraits\Surelease;
use Auth,CommonHelper;
use App\Models\SupreAdminReleasenote;

class SureleaseController extends Controller
{
    public function allreleasesAction()
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3','5');
		if(in_array($roleid, $userRole))
		{ 
	            $allreleasenotes = Surelease::fetchAllReleasenotes();
				$dataArray =array();
			    $i=1;
				if(count($allreleasenotes)>0)
				{
					foreach ($allreleasenotes as $val) 
					{	
					    $release_date = date('m/d/Y', strtotime($val->release_date));
						$title = $val->title;
						$version = $val->version;
						$id = CommonHelper::getEncode($val->id);
						
						$dataArray[]=array('date'=>$release_date,'title'=>$title,'release'=>$version,'is_active'=>$val->is_active,'id'=>$id);
					}
				}
				return response()->json(['gridData'=>$dataArray]);
		}
	}
	
	public function addeditreleasenotesAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3','5');
		if(in_array($roleid, $userRole))
		{
			$vSomeSpecialChars = array("quesmark", "permark", "bckslsh", "plusmark"
			, "nquot;", "singleqoute", "ngt;"
			, "nlt;", "namp;", "Ó", "Ú", "ñ", "Ñ", '“', '”'
			, "dbl_s", "and", "smlBracOpn", "smlBracCls", "bigBracOpn", "bigBracCls", 'hash', 'dollar', 'star');
			$vReplacementChars = array("?", "%", "/", '+', '"', "'", ">", "<", "&", "O", "U", "n", "N"
			, '"', '"',    "§", "&", "(", ")", "[", "]", '#', '$', '*');
			
			$operation 	= 	$request->input('type');
			$date 	= 	$request->input('date');
			$tittle 	= 	$request->input('title');
			$title 		= 	str_replace($vSomeSpecialChars, $vReplacementChars, $tittle);
			$data = array();
			if(!empty($date))
				$data['release_date'] = date('Y-m-d', strtotime($date));
			
			$data['title'] 			= $title;
			$data['description'] 	= $request->input('description');
			$data['short_desc'] 	= $request->input('short_desc');
			$data['maintenance_msg'] 	= $request->input('maintenance_msg');
			$data['version'] 	= $request->input('version');
			$data['start_hrtime'] 	= $request->input('start_hrtime');
			$data['start_amorpm'] 	= $request->input('start_amorpm');
			$data['end_hrtime'] 	= $request->input('end_hrtime');
			$data['end_amorpm'] 	= $request->input('end_amorpm');
			$data['timezone'] 	= $request->input('timezone');
			
			if($operation == 'addann')
			{
				$lastid = SupreAdminReleasenote::insert($data);
				if($lastid > 0)
				return "Record added successfully";
			}elseif($operation == 'updateann') {
				$id=$request->input('id');
				if(!is_numeric($id))
				{ $id = CommonHelper::getDecode($id); }
			    SupreAdminReleasenote::whereid($id)->update($data);
				return "Record updated successfully";
			}
			
			
			
		}
		
	}
	
	public function releasenotevalbyidAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3','5');
		$perticularRelease=array();
		if(in_array($roleid, $userRole))
		{
			$id=CommonHelper::getDecode($request->input('id'));
			$perticularRelease = SupreAdminReleasenote::find($id);
		}
		return $perticularRelease;
		
	}
	
	public function publishAction(Request $request) 
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3');
		if(in_array($roleid, $userRole)) 
		{
			$id=$request->input('id');
			$isActive=$request->input('isActive');
			if(!is_numeric($id))
		   { $id = CommonHelper::getDecode($id); }
	       Surelease::updatepublish($isActive, $id); 
		}
	}
	
	public function deletereleaseAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$userRole = array('1','3');
		if(in_array($roleid, $userRole)) 
		{
			$id=$request->input('id');
			if(!is_numeric($id))
		   { $id = CommonHelper::getDecode($id); }
	        Surelease::updatedelete('1', $id); exit;
		}
	}
}
