<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use CommonHelper,DateTime,DateTimeZone;
use App\Http\Traits\Transcript;
use App\Http\Traits\Trainingcatalog;

class TranscriptController extends Controller
{
  
	/**
	 * transcriptData() - Method to retrive transcript data for grid on my elearning section.
     *
	 * Created By: Siddharth Kushwah
	 * Created Date: 27 DEC, 2017
     */
	public function transcriptData(Request $request)
	{      
			$compid = Auth::user()->company_id;
			$userid = Auth::user()->user_id;
			$roleid = Auth::user()->role_id;
			$trainingType  =CommonHelper::getdecode($request->input('trainingType',''));
			$startDate     =$request->input('startDate');
			$endDate       =$request->input('endDate');
	        
			$userTimeZone=CommonHelper::getUserTimeZone($userid);
			$start=$request->input('start');
			$length=$request->input('length');
			$search = $request->input('search');
			$draw = $request->input('draw');
			$order = $request->input('order');
			$orderColumn=$order[0]['column'];
			$orderType=$order[0]['dir'];
			$searchtext=$search['value'];
	   
			$baseUrl='';
			$buttonclass='button_lblue_r4';
			$transcript_data=array();
			
	    
			if($startDate){
			  $startDate	=date('Y-m-d',strtotime($startDate));
			}
			
			if($endDate){
			  $endDate	=date('Y-m-d',strtotime($endDate));
			}
			
            $allTranscript = Transcript::getAllTranscript($userid, $compid, $startDate, $endDate, $trainingType,$start,$length,$search,$draw,$order,$orderColumn,$orderType,$searchtext);
			
			$viewhref='';
			 if(count($allTranscript[0])>0)
			{
				foreach($allTranscript[0] as $key => $transcript)
				{
					$data = array();
					$item_name     					= stripcslashes($transcript['item_name']);	
                    $credit_value  					= $transcript['credit_value'];					
					$type        					= $transcript['training_type'];
					$training_program_code      	= $transcript['training_program_code'];
					$ccstatus      					= $transcript['ccstatus'];
					$score         					= $transcript['score'];
					$status 						= $transcript['status'];
					$course_id 						= $transcript['course_id'];
					$completion_date 				= $transcript['completion_date'];
					$view_mode 						= $transcript['view_mode'];
					$transcript_id 					= $transcript['transcript_id'];
						$view_btn = $certificate_btn = '';
						if($transcript['training_type_id']==5)
						{
							
							$res_arr = array(
								"confirm"=>1,
								"userprogramtypeid"=>5,
								"training_program_code"=>$training_program_code,
								"course_id"=>$course_id, 
								"launchtime"=>date('Y-m-d h:i:s'),
								"view"=>0,
								"transcript_id"=>$transcript_id,
								"user_assess_resp_id"=>$transcript['user_assess_resp_id']
							);
							
							if($view_mode==1){
							   $action_url = 'mylearning/viewlistquizandsurvey/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
							}else{
							   $action_url = 'mylearning/viewquizandsurvey/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
							}
							
							$view_btn = '<a class="'.$buttonclass.' hand" ng-click=launch_player(\''.$action_url.'\')>View<span>&#187;</span></a>';
							
						}
						/* else if(@$transcript['training_type_id']==4)
						{
							$view_btn = '<a class="change_col" href="'.$baseUrl.'#/trainingcatalog/submittrainings/type/transcript/submittraining/1/trainingprogram_id/'.CommonHelper::getEncode($transcript['training_program_id']).'/red_page/transcript"><div class="'.$buttonclass.' hand">View<span >&#187;</span></div></a>&nbsp<div class ="button_lblue_r4 hand" ng-click=grid.appScope.openviewpopup('.$userid.','.$transcript['item_id'].') onfocus=if(this.blur)this.blur();>History<span>&#187;</span></div>';
						} 
						else if($transcript['training_type_id'] !=0)
						{
							$viewId = ''.$userid.'#'.$roleid.'||transcript||'.$transcript['blended_program_id'].'||1';
							$encoded_viewId = CommonHelper::getEncode($viewId);
							$view_btn = '<a class="change_col" href="'.$baseUrl.'#/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId.'"><div class="'.$buttonclass.' hand">View<span >&#187;</span></div></a>';
						} */
						else if($transcript['training_type_id'] ==1)
						{
							//$viewId = ''.$userid.'#'.$roleid.'||transcript||';
							//$encoded_viewId = CommonHelper::getEncode($viewId);
							$view_btn = '<a class="change_col" href="'.$baseUrl.'#/elearningmedia/training/'.$training_program_code.'"><div class="'.$buttonclass.' hand">View<span >&#187;</span></div></a>&nbsp<div class ="button_lblue_r4 hand" ng-click=openviewpopup('.$userid.','.$transcript['course_id'].') onfocus=if(this.blur)this.blur();>History<span>&#187;</span></div>';
						}
						else 
						{
							$viewId = ''.$userid.'#'.$roleid.'||transcript||'.$transcript['item_id'].'||'.$transcript['class_id'].'';
							$encoded_viewId = CommonHelper::getEncode($viewId);				
							$view_btn = '<a href="'.$baseUrl.'#/classroomcourse/training/viewId/'.$encoded_viewId.'"><div class="'.$buttonclass.' hand">View<span>&#187;</span></div></a>&nbsp<div class ="button_lblue_r4 hand" ng-click=grid.appScope.openviewpopup('.$userid.','.$transcript['course_id'].') onfocus=if(this.blur)this.blur();>History<span>&#187;</span></div>';
						}
						
					
						$certificate_template_id = (!empty($transcript['certificate_template_id'])) ? (int)$transcript['certificate_template_id'] : 0;
						if($transcript['training_type'] == "eLearning" && $certificate_template_id!=0 && $ccstatus==1 && $transcript['status'] != 'Fail'  )
						{
							
							$certificate_btn = '<a class="change_col certificate_button" href="'.$baseUrl.'/mylearning/certificatedownload/id/'.CommonHelper::getEncode($transcript['transcript_id']).'"></a>';
						}
						
						if($transcript['training_type']=="Classroom" && $certificate_template_id!=0 && $ccstatus==1 )
						{
							 $certificate_btn = '<a class="change_col certificate_button" href="'.$baseUrl.'/mylearning/certificatedownload/id/'.CommonHelper::getEncode($transcript['transcript_id']).'"></a>'; 
							
						}
						
						if($transcript['training_type'] ==  "Assessment"  &&  $ccstatus==1 && $transcript['status']!='Fail')
						{
							/*  $certificatepath=$baseUrl.'/mylearning/assessmentcertificatedownload/id/'.CommonHelper::getEncode($transcript['transcript_id']);
							$certificate_btn = '<div class ="change_col certificate_button" ng-click=grid.appScope.downloadcertificate(\''.$certificatepath.'\') onfocus=if(this.blur)this.blur();></div>'; */
							$certificate_btn = '<a class="change_col certificate_button" href="'.$baseUrl.'/mylearning/certificatedownload/id/'.CommonHelper::getEncode($transcript['transcript_id']).'"></a>';
						}
						$viewhref = $view_btn." ".$certificate_btn;
						
						$newCompletionData=CommonHelper::convertDateToSpecificTimezone($completion_date,$userTimeZone);
						
						
							
					$transcript_data[]=array('transcript_id'=>$transcript['transcript_id'],'item_name'=>$item_name,'type'=>$type,'score'=>$score,'status'=>$status,'completion_date'=>$newCompletionData,'credit_value'=>$credit_value,'button'=>$viewhref,'dateFormat'=>($transcript['dateformat'])?CommonHelper::getdatecalenderformateAngular($transcript['dateformat']):'yyyy-MM-dd');

					
				}
			}
		//	echo '<pre>'; print_r($transcript_data); exit;
			return  $success=array("status"=>200,"data"=>$transcript_data,"count"=>$allTranscript[1],"draw"=>$draw);
			//echo '<pre>'; print_r($transcript_data); exit;
			/* $response =array(
			  "status"=>200,
			  "data"=>$transcript_data,
			 );
			 return response()->json($response); */

    }
	
	public function gettranscripthistory(Request $request)
	{
		$identity	=Auth::user();
		$user_id	=$identity->user_id;
		$compid	=$identity->company_id;
		$item_id=$request->input('item_id');
		$get_data =Transcript::getTranscriptHistory($item_id,$user_id,$compid);
			$i=1;$transcript_data = array();
			if(count($get_data)>0){
				foreach($get_data as $val){ 
				    $data =array();
					$data['item_name']=$val->item_name;
					$data['credit_value']=$val->credit_value;
					$data['status']='Completed';
					$data['completion_date']=$val->completion_date;
					$data['type']=$val->itemtype;
					$transcript_data[] = $data;
					$i++;
				}
			}
			$response =array(
			"status"=>200,
			"data"=>$transcript_data,
			);
			return response()->json($response);	
	}
	
	public function certificatedownload($id){

		$certificate_row = Trainingcatalog::getcertificate(CommonHelper::getdecode($id));
	
		if($certificate_row->SCORE!= '' )
		{
			$certificate_content = str_replace('%USER_NAME%', $certificate_row->USER_NAME, $certificate_row->certificate_content);
			$certificate_content = str_replace('%COURSE_NAME%', $certificate_row->TRAINING_TITLE, $certificate_content);
			$certificate_content = str_replace('%SCORE%', $certificate_row->SCORE, $certificate_content);
			$certificate_content = str_replace('%CREDIT%', $certificate_row->CREDIT, $certificate_content);
			$certificate_content = str_replace('%DATE_OF_COMPLETION%', $certificate_row->DATE_OF_COMPLETION, $certificate_content);
		}
		else
		{
			$certificate_content = str_replace('%USER_NAME%', $certificate_row->USER_NAME, $certificate_row->certificate_content);
			$certificate_content = str_replace('%COURSE_NAME%', $certificate_row->TRAINING_TITLE, $certificate_content);
			$certificate_content = str_replace('%SCORE%', $certificate_row->SCORE, $certificate_content);
			$certificate_content = str_replace('%CREDIT%', $certificate_row->CREDIT, $certificate_content);		
			$certificate_content = str_replace('%DATE_OF_COMPLETION%', $certificate_row->DATE_OF_COMPLETION, $certificate_content);
		}
		
		$compid = $certificate_row->company_id;
		$template_id = $certificate_row->id;
		$certificate_title = $certificate_row->title;
		$certificate_content = $certificate_content;
		$template_bg_image = $certificate_row->template_bg_image;
		$bg_img_mode = $certificate_row->bg_img_mode;
		$dowload_option = 'Yes';		
		view()->share('dowload_option',$dowload_option);
		view()->share('template_id',$template_id);
		view()->share('certificate_title',$certificate_title);
		view()->share('bg_img_mode',$bg_img_mode);
		view()->share('template_bg_image',$template_bg_image);
		view()->share('certificate_content',$certificate_content);
		view()->share('baseUrl','');
		view()->share('compid',$compid);
		//$html = view::make('user.certificate.previewcertificate');
		$html= view('user.certificate.previewcertificate', ['dowload_option'=>$dowload_option,'template_id'=>$template_id,'certificate_title'=>$certificate_title,'bg_img_mode'=>$bg_img_mode,'template_bg_image'=>$template_bg_image,'certificate_content'=>$certificate_content,'baseUrl'=>'','compid'=>$compid]);
	    echo $html; exit;

	}
	
}
