<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use CommonHelper;
use Session;
use Auth,Mylearningfunctions;
use App\Http\Traits\Admin;
use App\Models\CompanyAboutUs;
use App\Models\CompanyAnnouncement;
use App\Http\Traits\Contentmanagement;
use App\Models\UserMaster;
use CommonFunctions,App;

class MenuController extends Controller
{
	use Admin,Contentmanagement;
    //
	
	/**
     * getAppMenus() - Method to get App Menus 
     *
     */
	/* public function getAppMenus(Request $request)
	{
		
	    $all=CommonHelper::getMenuList(60,1,2,'no');
		echo '<pre>'; print_r($all); exit;
	} */
	
	public function getAppMenus(Request $request)
	{
		$boxData=array();
		$identity=Auth::user();
		$authToken=$request->input('authToken');
		if(!$authToken){
				$error =array(
					"status"=>408,
					"message"=>"authToken should not be empty"
					);
				return response()->json($error);
		 }
		$auth_status=CommonHelper::auth_status($authToken);
		if($auth_status){
			$user_id =CommonHelper::token_decode($authToken);
		}else{
			$error =array(
				"status"=>408,
				"message"=>'user auth invalid'
				);
			return response()->json($error);
			
		}
			
			$sessiondata =CommonHelper::authtokenuserdtail($user_id);
			$roleid=$sessiondata['role_id'];
			$compid=$sessiondata['company_id'];
		
			
		$baseUrl = '#';	
		$navigation_str = '';	
		
		$sub_cats = CommonHelper::getMenuList(60,1,2,'no');

		$user_action_role = CommonHelper::getCompanySettingGroups($compid);
		
		$checksubmit=1;
		$countdiv = 0;
		if($sessiondata['roleid']==4)
		{
		 $class= 'col-md-4';
		}else{
		 $class='col-md-3';
		}
		foreach($sub_cats as $SubCatValue)
		{
			$MenuName = $SubCatValue->menu_name;
			$MenuUrl  = $SubCatValue->menu_url;
			$MenuIcon = isset($SubCatValue->menu_icon) ? $SubCatValue->menu_icon : '';
			if($checksubmit==0 && $SubCatValue->menu_item_id=='67') continue;
			$boxData[]=array('linkUrl'=>'/#/'.$MenuUrl,'imageUrl'=>'/public/app-images/'.$MenuIcon.'.png','menuName'=>$MenuName);
			
			$countdiv+= 1; 
			if($countdiv % 4 == 0 && $sessiondata['roleid']!=4)
			{
			$navigation_str.= '';
			}
			if($countdiv % 3 == 0 && $sessiondata['roleid']==4)
			{
			$navigation_str.= '';
			}		
		}
		$headerMenu = CommonHelper::getMenuList(0,1,2,'no');
		foreach($headerMenu as $headerMenuValue)
		{
			if(!($headerMenuValue->menu_item_id == 47 || $headerMenuValue->menu_item_id == 104 || $headerMenuValue->menu_item_id == 105 || $headerMenuValue->menu_item_id == 60))
			{ 
				if($headerMenuValue->menu_item_id == 61)
				{
					$boxData[]=array('linkUrl'=>'/#/discussions/index','imageUrl'=>'/eliteprodlms/public/assets/images/comments.png','menuName'=>'Discussions');
                    				
					
					$countdiv+= 1;
					if($countdiv % 4 == 0 && $sessiondata['roleid']!=4)
					{
						$navigation_str.= '';
					}
					if($countdiv % 3 == 0 && $sessiondata['roleid']==4)
					{
						$navigation_str.= '';
					}
					if(!(in_array($sessiondata['roleid'], @$user_action_role['Collaboration_Hide_Blog'])))
					{
						$boxData[]=array('linkUrl'=>'/#/blog/index','imageUrl'=>'/public/app-images/blogs.png','menuName'=>'Blogs');
						
						
						$countdiv+= 1;
					}
					if($countdiv % 4 == 0 && $sessiondata['roleid']!=4)
					{
					$navigation_str.= '';
					}
					if($countdiv % 3 == 0 && $sessiondata['roleid']==4)
					{
					$navigation_str.= '';
					}
					$boxData[]=array('linkUrl'=>'/#/expert/index','imageUrl'=>'/public/app-images/expertdirectory.png','menuName'=>'Expert Directory');
					    
					
					$countdiv+= 1;
					if($countdiv % 4 == 0 && $sessiondata['roleid']!=4) {
					     $navigation_str.= '';
					}
					if($countdiv % 3 == 0 && $sessiondata['roleid']==4) {
					    $navigation_str.= '';
					}
				}
				else
				{ 
				    $MenuName = $headerMenuValue->menu_name;
					$MenuUrl  = $headerMenuValue->menu_url;
					$MenuIcon = isset($headerMenuValue->menu_icon) ? $headerMenuValue->menu_icon : '';
				    $company_id = array(43,56);
				    if(($headerMenuValue->menu_item_id != 44) && in_array($compid, $company_id)){
					
					if($countdiv % 4 == 0 && $sessiondata['roleid']!=4){
						$navigation_str.= '';
					}
					if($countdiv % 3 == 0 && $sessiondata['roleid']==4) {
						$navigation_str.= '';
					}
					$boxData[]=array('linkUrl'=>'/#/'.$MenuUrl,'imageUrl'=>'/public/app-images/'.$MenuIcon.'.png','menuName'=>$MenuName);
					$countdiv+= 1;	
                   }elseif(!in_array($compid, $company_id)) {
						if($countdiv % 4 == 0 && $sessiondata['roleid']!=4) {
						$navigation_str.= '';
						}
						if($countdiv % 3 == 0 && $sessiondata['roleid']==4) {
							$navigation_str.= '';
						}
						$boxData[]=array('linkUrl'=>'/#/'.$MenuUrl,'imageUrl'=>'/public/app-images/'.$MenuIcon.'.png','menuName'=>$MenuName);
						$countdiv+=1;	
				   }
				}
			}		
		}
		foreach($headerMenu as $headerMenuValue)
		    {
				{ 
				    $MenuName = $headerMenuValue->menu_name;
					$MenuUrl  = $headerMenuValue->menu_url;
					$MenuIcon = isset($headerMenuValue->menu_icon) ? $headerMenuValue->menu_icon:'';
				    $company_id = array(43,56);
				    if(($headerMenuValue->menu_item_id == 44) && in_array($compid, $company_id)) {
						if($countdiv % 4 == 0 && $sessiondata['roleid']!=4) {
							$navigation_str.= '';
						}
						if($countdiv % 3 == 0 && $sessiondata['roleid']==4) {
							$navigation_str.= '';
						}
					    $boxData[]=array('linkUrl'=>'/#/'.$MenuUrl,'imageUrl'=>'/public/app-images/'.$MenuIcon.'.png','menuName'=>$MenuName);
						$countdiv+= 1;	
                   }
				}
			}
			
		if($countdiv % 4 == 0 && $sessiondata['roleid']!=4)
		{
			$navigation_str.= '';
		}
        if($countdiv % 3 == 0 && $sessiondata['roleid']==4)
        {
	     $navigation_str.= '';
	    }
	   
		/* if(!in_array($sessiondata['roleid'], @$user_action_role['Home_HideAnnouncements']))
		{
			$boxData[]=array('linkUrl'=>'/#/index/announcement','imageUrl'=>'/eliteprodlms/public/assets/images/announcements.png','menuName'=>'Announcements');
			            
			
			$countdiv+= 1;
		} */
		
		if($countdiv % 4 == 0 && $sessiondata['roleid']!=4)
		{
			$navigation_str.= '';
		}
        if($countdiv % 3 == 0 && $sessiondata['roleid']==4)
       {
	     $navigation_str.= '';
	   }
		
	
		$returnArray=array('navigation_str'=>$navigation_str,'boxData'=>$boxData);
		return $returnArray;
		
	}
	
	/**
     * appsheadernav() - Method used to get app header.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 04 January, 2018
     */
	
	public function appsheadernav()
	{
		$compid=1;
		$navigation_str = '';
		$allowforsubmittraining=Mylearningfunctions::checkForSubmitTraining($compid);
		$checksubmit=$allowforsubmittraining[0]->input_training_records;
		$cats = CommonHelper::getMenuList(0,1,2,'no');
		$cats = json_decode(json_encode($cats),TRUE);
		
		$CatReplica = array();
		$CatReplica = $cats;
		
		$sub_cats = CommonHelper::getMenuList(60,1,2,'no');
		$sub_cats = json_decode(json_encode($sub_cats),TRUE);
		
		$header = $sub_header = array();
		
		foreach($cats as $cat)
		{
			$menu_name = $cat['menu_name'];
			$menu_url = $cat['menu_url'];
			
			if($cat['menu_item_id'] == 104)
			{
				$header[] = '<li><a class="border-gray" href="/#/'.$menu_url.'" onfocus="if(this.blur)this.blur();"><i class="fa fa-home mr07"></i>'.$menu_name.'</a></li>';
			}else if($cat['menu_item_id'] == 105) {
				$submenu = '<ul class="dropdown-menu app_dropdownmenu">';
				foreach($CatReplica as $CatReplicaValue)
				{
					if(!($CatReplicaValue['menu_item_id'] == 47 || $CatReplicaValue['menu_item_id'] == 104 || $CatReplicaValue['menu_item_id'] == 105 || $CatReplicaValue['menu_item_id'] == 60))
					{
						if(!($CatReplicaValue['menu_item_id'] == 86 || $CatReplicaValue['menu_item_id'] == 62 || $CatReplicaValue['menu_item_id'] == 89 || $CatReplicaValue['menu_item_id'] == 61 || $CatReplicaValue['menu_item_id'] == 44))
						{
							$CatReplica_name = $CatReplicaValue['menu_name'];
							$CatReplica_url = $CatReplicaValue['menu_url'];
							$sub_menu_icon = isset($CatReplicaValue['menu_icon']) ? $CatReplicaValue['menu_icon'] : '';
							$submenu.= '<li>
										  <a class="border-gray"  href="/#/'.$CatReplica_url.'" title="'.$CatReplica_name.'" onfocus="if(this.blur)this.blur();"><i class="fa fa-'.$sub_menu_icon.'"></i>' .$CatReplica_name. '</a>
										</li>';
						}
					}
				}
				foreach($CatReplica as $CatReplicaValue)
				{ 
						if($CatReplicaValue['menu_item_id'] == 44)
						{
							$CatReplica_name = $CatReplicaValue['menu_name'];
							$CatReplica_url = $CatReplicaValue['menu_url'];
							$sub_menu_icon = isset($CatReplicaValue['menu_icon']) ? $CatReplicaValue['menu_icon'] : '';
							$submenu.= '<li>
										  <a class="admin" href="/#/'.$CatReplica_url.'" title="'.$CatReplica_name.'" onfocus="if(this.blur)this.blur();"><i class="fa fa-'.$sub_menu_icon.'"></i>' .$CatReplica_name. '</a>
										</li>';
						}
				}
				foreach($sub_cats as $subCatValue)
				{
					$sub_menu_name = $subCatValue['menu_name'];
					$sub_menu_url = $subCatValue['menu_url'];
					$sub_menu_icon = isset($subCatValue['menu_icon']) ? $subCatValue['menu_icon'] : '';
					if($checksubmit==0 && $subCatValue['menu_item_id']=='67') continue;
					$submenu.= '<li><a class="admin" href="/#/'.$sub_menu_url.'" title="'.$sub_menu_name.'" onfocus="if(this.blur)this.blur();"><i class="fa fa-'.$sub_menu_icon.'"></i>' .$sub_menu_name. '</a>
								</li>';
				}
				/* $submenu.= '<li><a class="admin" href="/#/index/announcement" title="announcement" onfocus="if(this.blur)this.blur();"><i class="fa fa-bullhorn"></i>Announcements</a>
								</li>'; */
								
				foreach($CatReplica as $CatReplicaValue)
				{
					if(!($CatReplicaValue['menu_item_id'] == 47 || $CatReplicaValue['menu_item_id'] == 104 || $CatReplicaValue['menu_item_id'] == 105 || $CatReplicaValue['menu_item_id'] == 60 || $CatReplicaValue['menu_item_id'] == 44))
					{
						if($CatReplicaValue['menu_item_id'] == 86 || $CatReplicaValue['menu_item_id'] == 62 || $CatReplicaValue['menu_item_id'] == 89 || $CatReplicaValue['menu_item_id'] == 61)
						{
							$CatReplica_name = $CatReplicaValue['menu_name'];
							$CatReplica_url = $CatReplicaValue['menu_url'];
							$sub_menu_icon = isset($CatReplicaValue['menu_icon']) ? $CatReplicaValue['menu_icon'] : '';
							$submenu.= '<li>
										  <a class="admin" href="/#/'.$CatReplica_url.'" title="'.$CatReplica_name.'" onfocus="if(this.blur)this.blur();"><i class="fa fa-'.$sub_menu_icon.'"></i>' .$CatReplica_name. '</a>
										</li>';
						}
					}
				}
				$submenu.= '</ul>';
				
				
				
				$header[] = '<li class="dropdown hand user"><a class="dropdown-toggle" data-toggle="dropdown"  style="vertical-align:middle; top:1px" onfocus="if(this.blur)this.blur();"><i class="fa fa-th mr07"></i>'.$menu_name.'</a>'. $submenu .
				'</li>';		
			}else
			{
				continue;
			}	
			
		}
		
		$seperator_str = '';	
		$navigation_str = implode($seperator_str,$header);	
		
		return $navigation_str;	
		
		
	}
	
	/**
     * footernav() - Method used to get app footer.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 04 January, 2018
     */
	public function footernav()
	{
		$company_id = Auth::user()->company_id;
		$navigation_str = '';
		$cats = CommonHelper::getMenuList(0,1,3,'no');
		$cats = json_decode(json_encode($cats),TRUE);
		$companylinkinfo = CommonFunctions::fetchlinkinfo($company_id);
		
		foreach($cats as $cat)
		{
			$menu_name = $cat['menu_name'];
			if(strtolower($menu_name)=='links'){
				$navigation_str .= '<div style="padding: 0 0; text-align: center;">';
				if(count($companylinkinfo)>0){
				foreach($companylinkinfo as $key=>$value){
					$navigation_str .= '<a style="padding: 10px 0; display: inline-block;" class="admin_foo white" target="_blank" href="'.@$value->link_url.'">'.@$value->link_title.'</a>';
				 }
				}
				$navigation_str .= '</div>';
			}
		}
		return $navigation_str;
	}
	
	/**
     * companyCmsInfo() - Method used to get company cmsinfo.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 04 January, 2018
     */
	public function companyCmsInfo()
	{
		$compid=Auth::user()->company_id;
		$userid=Auth::user()->user_id;
		$roleid = Auth::user()->role_id;
		$userskills = Admin::getUserInformation($userid, $compid);
		$companycmsinfo=Contentmanagement::fetchcompanyinfo($compid);
		$companyInfo=CommonFunctions::getCompanyInfo($compid);
		$returnArray=array('userskills'=>$userskills,'companycmsinfo'=>$companycmsinfo,'companyInfo'=>$companyInfo);
		return $returnArray;
		
	}
	
	public function aboutAction()
	{
		$compid=Auth::user()->company_id;
		$companyaboutus 	= CompanyAboutUs::where('company_id','=',$compid)->where('is_active','=',1)->get();
	    return response()->json(array('aboutus'=>$companyaboutus));
	}
	
	public function getannouncement()
	{
		
		$compid=Auth::user()->company_id;
		$user_id=Auth::user()->user_id;
		
		$systemuser=UserMaster::where('user_id','=',$user_id)->first();
		$loginusername=$systemuser->user_name ?? '';
		
		$Companyanouncement =CompanyAnnouncement::where('company_id','=',$compid)->orderby('last_updated_date','desc')->get();
		$monthArray=array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'AUG','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
		$storeannouncement=array();
		if(count($Companyanouncement)>0){
		foreach($Companyanouncement as $valuedata){
			$user_name=UserMaster::where('user_id','=',$valuedata->last_updated_by)->first();
			$valueannouncement['longdesc']=$valuedata->announcement_longdescription;
			$valueannouncement['shortdesc']=$valuedata->announcement_description;
			$a=explode(' ',$valuedata->created_date);
			$b=explode('-',$a[0]);
			$valueannouncement['month']=$monthArray[$b[1]];
			$valueannouncement['date']=$b[2];
			$valueannouncement['announcement_title']=$valuedata->announcement_title;
			$valueannouncement['user_name']=$user_name->user_name ?? '';
			$valueannouncement['counter']=count($Companyanouncement);
			$storeannouncement[]=$valueannouncement;
		} 
		}
			$newdate = date("F j, Y - G:i A");
		 return response()->json(array('announcement'=>$storeannouncement,'newdate'=>$newdate,'systemuser'=>$loginusername));
	}
	
	public function leftnav()
	{
	   $cats = CommonHelper::getMenuList(0,1,1,'no');
	   
	   foreach($cats as $cat){
		   $main_menu_url = ((trim($cat->menu_url)!="") ? '/administrators/#/'.$cat->menu_url : '1');
		   $cat->main_menu_url=$main_menu_url;
		   $parent_id = $cat->menu_item_id;
		   $sub_cats = CommonHelper::getMenuList($parent_id,1,1,'no');
		   if($sub_cats) {
			   foreach($sub_cats as $sub_cat){
			   $sub_menu_url = ((trim($sub_cat->menu_url)!="") ?'/administrators/#/'.$sub_cat->menu_url : 'javascript:void(0);');
			   $sub_cat->sub_menu_url=$sub_menu_url;
			   $sub_cat->menu_name=str_replace('Beta','<font color="red">Beta</font>',$sub_cat->menu_name);
			   }
		   }
		   $cat->sub_cats=$sub_cats;
	   }
	   
	   return response()->json(['status'=>201,'leftMenu'=>$cats]);
	   //return $cats;
	}
	
	public function changeLocale(Request $request, $lang)
	{
		//print_r($request);exit();
		//$this->validate($request, ['locale' => 'required|in:fr,en']);

		\Session::put('locale', $lang);
		$translationScript = "'use strict'; (function() {var translationInfoModule = angular.module('eLightApp', []);translationInfoModule.service('TranslationRepository', function() {var translations = ssss; return {getTranslation: function (code) {return translations[code];},getDefaultLocale: function () {return ssss},getPossibleLocales: function () {return ssss;}}});})();";

		return response($translationScript)
            ->withHeaders([
                'Content-Type' => 'text/javascript;charset=UTF-8'
            ]);
		//return redirect()->back();
	}
	
	public function getLabel(Request $request){
		//$lang=App::getLocale();
		
		$language=$request->input('language');
		if($language=='en'){
			$returnArray=array('trainingcatalogtitle'=>'Training Catalog','category'=>'Category','allcategory'=>'All Category','itemtype'=>'Item Type',"startdate"=>"Start Date","creditvalue"=>"Credit Value","view"=>"View","itemname"=>"Item Name","advancesearch"=>"Advance Search","myrequirementtitle"=>"My Requirement","coursename"=>"Course Name","trainingtype"=>"Training Type","duedate"=>"Due Date","assign"=>"Assigned Group/User","assigntype"=>"Assigned Type","action"=>"Actions",'mytranscripttitle'=>'My Transcript','enddate'=>'End Date','status'=>'Status','datecompleted'=>'Completed Date');
		}else if($language=='ar'){
			$returnArray=array('trainingcatalogtitle'=>'كتالوج التدريب','category'=>'طبقة','allcategory'=>'كل فئة','itemtype'=>'كل فئة',"startdate"=>"كل فئة","creditvalue"=>"كل فئة","view"=>"لتدريب","itemname"=>"لتدريب","advancesearch"=>"البحث المتقدم","myrequirementtitle"=>"متطلباتي","coursename"=>"كتالوج","trainingtype"=>"كتالوج","duedate"=>"كتالوج","assign"=>"كتالوج","assigntype"=>"كتالوج","action"=>"كتالوج",'mytranscripttitle'=>'كتالوج فئة','enddate'=>'فئة','status'=>'كل','datecompleted'=>'كتالوج');
		}else{
			$returnArray=array('trainingcatalogtitle'=>'प्रशिक्षण कैटलॉग','category'=>'श्रेणी','allcategory'=>'सभी श्रेणी','itemtype'=>'वस्तु प्रकार',"startdate"=>"आरंभ करने की तिथि","creditvalue"=>"क्रेडिट मूल्य","view"=>" दर्शन","itemname"=>"वस्तु का नाम","advancesearch"=>"उन्नत खोज","myrequirementtitle"=>"मेरी आवश्यकता","coursename"=>"कोर्स का नाम","trainingtype"=>"प्रशिक्षण का प्रकार","duedate"=>"नियत तारीख
             ","assign"=>"समूह / उपयोगकर्ता असाइन ","assigntype"=>"असाइन किया गया प्रकार","action"=>"कार्य",'mytranscripttitle'=>'मेरी प्रतिलिपि','enddate'=>'अंतिम तिथि','status'=>'स्थिति','datecompleted'=>'संपन्न तिथि');
		}
		return response()->json(['data'=>$returnArray]);
		
	}
	
	
}
