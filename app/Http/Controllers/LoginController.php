<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use CommonHelper;
use DateTime;
use DateTimeZone;
use App\Models\User;
use App\Http\Traits\Loginfunction;
use Exception;
use Session;
use App\Models\UserInfo;
use App\Models\UmsLogin;
use CommonFunctions;
//use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends Controller
{
	

   
	public function login(Request $request) 
	{ 
	     /*  Session::flush();
		  echo 'fff'; exit; */
		  $password = $request->input('password');
		  $username = $request->input('user_name');
		  $error =$request->input('errorcode');
		 // $loginform =$request->input('loginform');
		  $loginform =1;
        	
		//$model = $this->_getModel();
		/* $error = $this->_getParam('error');
		$this->view->error=$error; */
		//$locktime = $this->_getParam('locktime');
		/* $session = new Zend_Session_Namespace('returnurl'); 
		$returnurl = @$session->returnurl; */
		
		$msgArr=Loginfunction::getaMainitenanceMsg();
	
		if(count($msgArr) > 0)
		{
			$currentDateTimeSec = time();
			$userTimezone = 'America/Chicago';
			
			$start_hrtime = isset($msgArr[0]->start_hrtime) ? $msgArr[0]->start_hrtime : '00';
			$end_hrtime = isset($msgArr[0]->end_hrtime)?$msgArr[0]->end_hrtime:'00';
			if($msgArr[0]->start_amorpm == 'PM')
				$start_hrtime = $msgArr[0]->start_hrtime +12;
			if($msgArr[0]->end_amorpm == 'PM')
				$end_hrtime = $msgArr[0]->end_hrtime +12;
				
			$start_time = $msgArr[0]->release_date.' '.$start_hrtime.':00:00';
			$end_time = $msgArr[0]->release_date.' '.$end_hrtime.':00:00';
			
			
			
			$date = new \DateTime($start_time, new \DateTimeZone('Asia/Calcutta'));
			$date->setTimezone(new \DateTimeZone($userTimezone));
			$start_time_changed = $date->format('Y-m-d h:i:s');

			
			$startDateTimeSec = strtotime($start_time_changed);
		
		    $date2 = new \DateTime($end_time, new \DateTimeZone('Asia/Calcutta'));
			$date2->setTimezone(new \DateTimeZone($userTimezone));
			$end_time_changed = $date2->format('Y-m-d h:i:s');

			$endDateTimeSec = strtotime($end_time_changed);
			
			if($startDateTimeSec < $currentDateTimeSec && $endDateTimeSec > $currentDateTimeSec )
			{
			 $errorrResponse =array(
			  "status"=>200,
			  "message"=>"success",
			   "mode"=>'loginmaintenace'
			 );
			 return response()->json($errorrResponse);
			}
		}
		
		
	
		$fetchvalues =Loginfunction::getaccessurl();
	
		$compid = $fetchvalues['company_id'];
	
		/*  $company_layout = $this->_helper->CommonFunctions->getlayoutforoptimum($fetchvalues->company_id);
		$this->view->user_layout = $company_layout[0]['user_layout'] ; */
		
		if (isset($error) && ($error == 0)) {
			 $errorrResponse =array(
			  "status"=>408,
			  "message"=>"Incorrect username or password.",
			 );
			 return response()->json($errorrResponse);
		}elseif($error == 1) {
				$errorrResponse =array(
				"status"=>408,
				"message"=>"You are currently not authorized to access training on this site.  Please contact to your Administrator if you have questions.",
				);
				return response()->json($errorrResponse);
			
		}
		elseif($error == 2)
		{
			$errorrResponse =array(
			  "status"=>408,
			  "message"=>"Invalid username or password. Try again.",
			 );
			 return response()->json($errorrResponse);
		}
		elseif($error == 3)
		{ 
			
			$Comp_Id = $fetchvalues['company_id'] ;
			$PassSetData = Loginfunction::fetchCompPasswordsettingData($Comp_Id);
			$locked_Time = $PassSetData->locked_password_time ;
			$MaximumInvalidHit = $PassSetData->maximum_invalid_hit ;
			if(!(($MaximumInvalidHit != 0) && ($locked_Time == 0)))
			{
				$msg = "Maximum number of login attempts exceeded. Your account has been locked for $locked_Time minutes.";
			}
			else
			{
				$msg = "Maximum number of login attempts exceeded. Your account has been locked permanetly , Please contact to admin.";
			}
			
			$errorrResponse =array(
			  "status"=>408,
			  "message"=>$msg,
			 );
			 return response()->json($errorrResponse);
			
		}
		elseif($error == 4)
		{
			
			$msg = "Account has expired.<br> 
			        Please contact the system administrator.";
			$errorrResponse =array(
			"status"=>408,
			"message"=>$msg,
			);
			return response()->json($errorrResponse);
			
		}
		else
		{
		    $msg = "";
		}
		
	   $fetchsetting=array();
		if ($fetchvalues) 
		{
			if ($fetchvalues['company_id'] != 0) 
			{
				
				$fetchlogo 				= Loginfunction::getmylogo($compid);
				$fetchsetting 			= Loginfunction::getcompanysetting($compid);
			
			}
			if ($fetchvalues['company_id'] == 0) 
			{
			   $msg =$fetchvalues['message'];
			   $status='408';
			   
				$Response =array(
				"status"=>$status,
				"message"=>$msg
				);
				return response()->json($Response);
				
			}
		}
		
		if($fetchsetting)
		{
			$status='200';
            $registerinfo				=	$fetchsetting[0]->automated_registration;
			
        }
		
		  
		
	
		/* if(!isset($_POST['loginform'])){
			$result = Loginfunction::getCompanyCss($fetchvalues['company_id']);	
			$Response =array(
			"status"=>$status,
			"result"=>$result,
			"fetchlogo"=>$fetchlogo,
			"registerinfo"=>$registerinfo,
			"message"=>$msg,
			);
			return response()->json($Response);
		} */
		
		if ($loginform) 
		{
			$username	=	addslashes($username);
	        if ($username == ''){
				$errorrResponse =array(
				"status"=>408,
				"message"=>"Please Enter User Name.",
				);
				return response()->json($errorrResponse);
			}
			if($password==''){
				$errorrResponse =array(
				"status"=>408,
				"message"=>"Please Enter Password.",
				);
				return response()->json($errorrResponse);
			}
	            $result=array('status'=>200); 
		      if(!Auth::check()){// echo 'fff'; exit;
			      $password_encode = CommonHelper::getEncode($password, 'no');
				 
		    	  $result = $this->authlogin($username, $password_encode, $compid);
			  } 

			
			$roleid ='';
			if((Auth::check()) && ($result['status'] ==200))
			{	
				$url	=	$_SERVER['HTTP_HOST'];
				$url	=	str_replace(".","",$url);
				setcookie("$url", "$compid", time()+2*60*60,"/");
				
				
				$identity =Auth::user();
				$user_id = $identity->user_id;
				$company_id=$identity->company_id;
				$roleid=$identity->role_id;
				
				/*---please work for commented---*/
				/* $userTimeZone = $request->getParam('userTimeZone');
				$userTimeZoneSession 	= new Zend_Session_Namespace('userTimeZone');
				$userTimeZoneSession->userTimeZone 	= $userTimeZone;				
				$indexmodel = new Model_index_Index();
				$logindetail =$indexmodel->checkIsSet($user_id,$company_id);
				$resset = $logindetail[0]['is_reset'];
				if($resset==1)
				{
					$viewid=$this->_helper->CommonFunctions->getEncode('id/'.$user_id.'/compid/'.$company_id);
					$this->_redirect('/index/createnewpassword/viewid/'.$viewid);
				} */
				
				$ums['login_time']= date('Y-m-d -h-i-s');
				$ums['ip_address']=$_SERVER['REMOTE_ADDR'];
				$ums['user_id']=$user_id;
				$ums['login_datetime']=date('Y-m-d -h-i-s');
				$ums['last_updated'] =date('Y-m-d -h-i-s');
				$ums['company_id']=$company_id;
				$fetchumsinfo=Loginfunction::fetchumsinfoforlogout($company_id,$user_id);
				
				if($fetchumsinfo)
				{
					
					$logintime=$fetchumsinfo->login_time;
					
					$extendedtime = date('Y-m-d -h-i-s', strtotime($logintime)+7200);
					$updatelogout['logout_time']=$extendedtime;
					
					UmsLogin::where('company_id','=',$company_id)->where('user_id','=',$user_id)->update($updatelogout);
				}else{
					
					UmsLogin::insert($ums);
					
				}
				
			    $result	= array(
				"status"=>200,
				"message"=>"success",
				"authToken"=>CommonHelper::encode_token($user_id)
				);
				
				
			}
			/* $session = new Zend_Session_Namespace('samlreturnurl');
			if(isset($session->returnurl) && $session->returnurl!="") $this->_redirect($session->returnurl);
			$base_url = Zend_Controller_Front::getInstance()->getBaseUrl();
			$redirect_url = array("", "$base_url/index/login", "$base_url/", "$base_url/index/login/error/0" , "$base_url/index/login/error/1");
			
			if(!in_array($returnurl,$redirect_url) )
			{
				$returnurlsend = new Zend_Session_Namespace('returnurl');
				$returnurlsend->returnurl = ''; 
				$returnurl = str_replace($base_url,"",$returnurl);
				$this->_redirect($returnurl);
			}else{
				$this->_helper->CommonFunctions->redirect($roleid);
			} */
			
			return response()->json($result);
		}
		
	}
	
		private function authlogin($username, $password, $compid)
		{
     
			try{
              	 $login_name 	= $compid.'_#_'.$username;
			     $user1=User::where('login_name','=',$login_name)->where('company_id','=',$compid)->first();
				 $salt_key=$user1->salt_key;
				 $passwordnew=$password.$salt_key;
			     
                
				if(($user1))
				{  
		              	$login_name 	= $compid.'_#_'.$username;
						$user2=User::where('login_name','=',$login_name)->where('password','=',$passwordnew)->where('company_id','=',$compid)->first();
					     if ($user2){
						    Auth::login($user2);
						    $user = Auth::User();
							$reurnresponse=$this->loginUserInfo($compid,$username);
						   
					      }else {
							        $reurnresponse=$this->loginUserInfo($compid,$username);
							
						} 
				 
					   return $reurnresponse;
				}
				else
				{
					return array('status'=>408,'message'=>'Invalid Credentials');
					
				}
			}
			catch(Exception $exception)
			{
				
			    	return array('status'=>408,'message'=>$exception->getMessage());
					
			}  

		} 
		
		public function loginUserInfo($compid,$username){
			
		$PassSetData = Loginfunction::fetchCompPasswordsettingData($compid);

			if($PassSetData)
			{
				
				$Userdata = Loginfunction::checkuserforlogin($username, $compid);
				
				if($Userdata)
				{
					$UserID = $Userdata->user_id;
					
					if($UserID > 0)
					{
						 $checkUserInfoExistanceData = Loginfunction::UserInfoPassSetExistance($UserID , $compid);
						
							if($checkUserInfoExistanceData)
							{				
								$blocktimeinSec = ($PassSetData->locked_password_time) * 60 ;
								$infoUpdateddate = strtotime($checkUserInfoExistanceData->last_updated_date);
								$infoUpdatedplusBlocktime = $infoUpdateddate + $blocktimeinSec ;
								$currDate = strtotime(date('Y-m-d H:i:s')) ;
								
								if(Auth::check())
								{
									$validatedate = 0;
									$validateLoginAttempts = 0;
									$LockedPasswordTime = $PassSetData->locked_password_time ;
									$MaximumInvalidHit = $PassSetData->maximum_invalid_hit ;
									if(!(($MaximumInvalidHit != 0) && ($LockedPasswordTime == 0)))
									{
										if($currDate > $infoUpdatedplusBlocktime)
										{
											$validatedate = 1 ;								
										}
									}
									if(($checkUserInfoExistanceData->login_attempts) < ($PassSetData->maximum_invalid_hit))
									{
										$validateLoginAttempts = 1 ; 	
									}
									if($validatedate || $validateLoginAttempts)
									{
										$validatedate = 1 ;
										$validateLoginAttempts = 1 ; 	
										$datainfor['login_attempts']  = 0 ;
										$datainfor['last_updated_date'] = date('Y-m-d H:i:s') ;
										
										UserInfo::where('user_id','=',$UserID)->where('company_id','=',$compid)->update($datainfor);
										
									}
								}
							else
							{ 
								$LockedPasswordTime = $PassSetData->locked_password_time ;
								$MaximumInvalidHit = $PassSetData->maximum_invalid_hit ;
								if(!(($MaximumInvalidHit != 0) && ($LockedPasswordTime == 0)))
								{
									if(($currDate > $infoUpdatedplusBlocktime) && (($checkUserInfoExistanceData->login_attempts) == ($PassSetData->maximum_invalid_hit)))
									{
										$datainfor['login_attempts']  = 0 ;
										$datainfor['last_updated_date'] = date('Y-m-d H:i:s') ;
										
										UserInfo::where('user_id','=',$UserID)->where('company_id','=',$compid)->update($datainfor);
										
									}
								}
							}
						}
					 }
					}
				}
								
				if ((Auth::check()) && (($validatedate !== 0) || ($validateLoginAttempts !== 0)))
				{
				
					 $identity=Auth::user();
				     //Loginfunction::getCompanySettingGroups($identity->company_id);
	
					if($identity->is_active != 1 || $identity->is_delete == 1  || $identity->user_id == ''  || $identity->company_id == '' ){
						Auth::logout();
					    $error = 1 ;
						$errorrResponse =array(
						"status"=>408,
						"message"=>"You are currently not authorized to access training on this site.  Please contact to your Administrator if you have questions.",
						"errorcode"=>$error
						);
		             	return $errorrResponse;
					    
					}else{
						$errorrResponse =array(
						"status"=>200,
						"message"=>"success",
						);
		             	return $errorrResponse ;
					}
					
				} 
				else 
				{
					
					$PassSetData = Loginfunction::fetchCompPasswordsettingData($compid);
					
					$locktime = 0 ;
					if(($PassSetData) && ($PassSetData->maximum_invalid_hit > 0))
					{
						$locktime = $PassSetData->locked_password_time ;
						$Userdata = Loginfunction::checkuserforlogin($username, $compid);
						if($Userdata)
						{			
                   			
							$UserID = $Userdata->user_id; 
							
							$checkUserInfoExistanceData = Loginfunction::UserInfoPassSetExistance($UserID, $compid);
 	
							$datainfo['user_id']  = $UserID ;
							$datainfo['company_id']  = $compid ;

							if(count($checkUserInfoExistanceData) > 0)
							{
									$error = 2 ;
								if((($checkUserInfoExistanceData->login_attempts)+1) >=  ($PassSetData->maximum_invalid_hit))
								{
									$error = 3 ;
								}
								if(($checkUserInfoExistanceData->login_attempts) <  ($PassSetData->maximum_invalid_hit))
								{
									$datainfo['login_attempts']  = $checkUserInfoExistanceData->login_attempts + 1 ;
									$datainfo['last_updated_date'] = date('Y-m-d H:i:s') ;
									
									UserInfo::where('user_id','=',$UserID)->where('company_id','=',$compid)->update($datainfo);
									
								}			
							}
							else
							{
								$error = 2 ;
								$datainfo['login_attempts']  = 1 ;
								$datainfo['last_updated_date'] = date('Y-m-d H:i:s') ;
								UserInfo::insert($datainfo);
								
							}
						}
					}
					  Auth::logout();
					$errorrResponse =array(
							"status"=>408,
							"message"=>"fail",
							"errorcode"=>$error,
						);
					return $errorrResponse ;
				}
							
	}
	public function logout(){ 
	       
	         Auth::logout();
			 Session::flush();
		    
			echo 'dfgdf'; exit;
			//Session::flush();
	}
	
	public function getdateformate(){
		$identity =Auth::user();
		$company_id=$identity->company_id;
		return $dateformate =CommonHelper::getdate($company_id);
	}
	
}
