<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use CommonHelper,Enrolleduserfromallplace,Mailnotification;
use App\Http\Traits\Classroomtraining;
use App\Http\Traits\Trainingcatalog;
use App\Http\Traits\Trainingprograms;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Trainingprogram;
use DateTime;
use App\Models\UsertraningStatus;
use App\Models\ItemMaster;
use App\Models\SubmitTrainingMaster;
use URL;
class ClassroomcourseController extends Controller
{
   
	public function classroomtrainingview(Request $request)
	{ 
	
		$user_data =Auth::user();
		$compid = $user_data->company_id;
		$userid = $user_data->user_id;
		$roleid = $user_data->role_id;
		$viewId = $request->input('viewId');
		$class_id = $request->input('classid');

		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$supervisior_approval = 0;
		$allocateSeat		  = 0;
		
		$sql_injection_status = CommonHelper::prevent_from_sql_injection($viewId);
		if($sql_injection_status!='no error')
		{
			$response =array(
			  "status"=>408,
			  "message"=>"error",
			);
			return response()->json($response);
		}
		
		$scheduleArr = $Usertrainingcatalog =$userCourseInfo= array();
		$classRating = $insRating = '';
		
		$courseId=0;
		if($class_id!=''){
			 $check_class_id=Classroomtraining::checkClassMaster($compid,$class_id,$viewId);	
             $courseId=$check_class_id->course_id;			
		}
		
		//itemId
		if($class_id != '' && $class_id > 0 && !empty($check_class_id))
		{
			$coursedetails = Trainingprogram::getClassroomCourseDetails($userid, $compid, $class_id, $courseId);
			
			$enrolledusers = Usertrainingcatalogs::getenrolledForClass2($class_id, $compid);
			
			
			$coursedetails[0]['current_enroll'] = count($enrolledusers);
			if($coursedetails[0]['start_date_class'] == NULL || $coursedetails[0]['start_date_class'] == '')
			{
				$start_date_class = '';
			}
			else
			{
				$start_date_class = date($Getdate_formatecomapny,strtotime($coursedetails[0]['start_date_class']));
			}
			

			$courseDetailArr = array();
			$courseDetailArr['course_id'] 				= $coursedetails[0]['course_id'];
			$course_id									= $coursedetails[0]['course_id'];
			$classid									= $coursedetails[0]['class_id'];
			$courseDetailArr['class_id'] 				= $coursedetails[0]['class_id'];
			$courseDetailArr['class_name'] 			 	= $coursedetails[0]['class_name'];
			$courseDetailArr['training_program_code'] 	= $coursedetails[0]['training_program_code'];
			$courseDetailArr['credit_value'] 			= $coursedetails[0]['credit_value'];
			$courseDetailArr['training_type_id'] 	    =2;
			$courseDetailArr['delivery_type_id'] 	    =$coursedetails[0]['delivery_type_id'];
			
			$courseDetailArr['available_seats'] 	= $coursedetails[0]['max_seat'] - $coursedetails[0]['current_enroll'];
			
			$courseDetailArr['training_category'] 	= $coursedetails[0]['category_name'];
			
			$courseDetailArr['description'] 		= $coursedetails[0]['course_description'];
			
			//$courseDetailArr['item_cost'] 			= $coursedetails[0]['item_cost'];
			//$courseDetailArr['item_image'] 			= $coursedetails[0]['item_image'];
			$courseDetailArr['prerequeisites'] 		= $coursedetails[0]['prerequeisites'];
			$courseDetailArr['virtual_information'] = $coursedetails[0]['virtual_session_information'];
			
			$courseDetailArr['last_schedule_date']  = $coursedetails[0]['last_schedule_date'];
			$courseDetailArr['class_start_date'] 	= $start_date_class;
			
			$courseDetailArr['supervisor_approval'] = $coursedetails[0]['require_supervisor_aproval_status'];
			$supervisior_approval 					= $coursedetails[0]['require_supervisor_aproval_status'];
			
			$courseDetailArr['certificate_template_id'] = $coursedetails[0]['certificate_template_id'];
			
		//$classid=$class_id;
		//echo $classid; exit;
			if($classid)
			{
				$programsschedule = Classroomtraining::allSessionsForClass($classid,$compid)->toArray();
				
				foreach($programsschedule as $schedule)
				{
					if(isset($schedule['timezone_name'])) {
						$timezone='('.$schedule['timezone_name'].')';
					} else {
						$timezone = '';
					}
					
					if($schedule['timeformat']=='PM' && $schedule['start_time_hr']!=12) {
						$starthour=$schedule['start_time_hr']+12;
					}
					elseif($schedule['timeformat']=='AM' && $schedule['start_time_hr']==12) {
						$starthour=$schedule['start_time_hr']+12;
					}
					else {
						$starthour=$schedule['start_time_hr'];
					}
					$starttimeval=$starthour.":".$schedule['start_time_min'];
					$endtimehr=$starthour+$schedule['ses_length_hr'];
					$endtimemin=$schedule['start_time_min']+$schedule['ses_length_min'];
					$nextEndtime=date('h:i A',mktime($endtimehr, $endtimemin, 0, 0, 0, 0000));
					$starttimeclass=date('h:i ',strtotime($schedule['start_time_hr'].":".$schedule['start_time_min']));
					$starttimeclass=$starttimeclass." ".$schedule['timeformat'];
					
					$tempArr = array();
					$tempArr['class_name'] 	= 	$coursedetails[0]['class_name'];
					$tempArr['start_date'] 	= 	date($Getdate_formatecomapny, strtotime($schedule['dayid']));
					
					$tempArr['start_time'] 	=	$starttimeclass.' '.$timezone;
					$tempArr['end_time']	=	$nextEndtime.' '.$timezone;
					$tempArr['instructor_name'] = ucfirst($schedule['user_name']);
					$tempArr['location'] 	= ($schedule['location'] != '0') ? $schedule['location'] : "";
					$scheduleArr[] = $tempArr;
				}
				
				$Usertrainingcatalog =Classroomtraining::getUtcClassDetail($course_id,$userid,$compid,$classid);
				
				$userCourseInfo      =   Trainingcatalog::getUserCourseInfo($userid, $course_id, $classid, $compid);
			
				//$trainingprogramid=$courseDetailArr['training_program_id'];
				// $coursedetails = Classroomtraining::getClassroomRequiewApproval($userid, $compid, $class_id, $trainingprogramid);	
				 //$supervisior_approval = $coursedetails[0]->require_supervisor_aproval_status;
		
				
				$available_seats = $coursedetails[0]['max_seat'] - count($enrolledusers);

				if($available_seats == 0){
					$allocateSeat = 1;
				}

				$allocateSeat			=	(isset($allocateSeat)) ? $allocateSeat : '';
			}
		
			//$targetAudience = Classroomtraining::getTragetAudienceforCourse($course_id,$compid);
			$targetAudience 				= 	Trainingprogram::targetaudiences($compid, $course_id);
			//$coursematerialsettings = Classroomtraining::getCourseMaterialSetting($itemId,$compid);

			$transcript_id=Trainingprogram::getTranscriptId($compid, $userid, $course_id); 
			$handouts = Classroomtraining::getClassHandouts($course_id,$compid);
			
			//$curriculams = Classroomtraining::getClassCurriculum($itemId,$compid);
			$avgInstRating=$avgClassRating=0;
			
			$scheduleIs=0;
			$scheduleDate='';
			if(!empty($scheduleArr)){
			$date = DateTime::createFromFormat($Getdate_formatecomapny, $scheduleArr[0]['start_date']);
			$scheduleDate 	= 		$date->format('Y-m-d');
			}
			if((strtotime($scheduleDate)) >= (strtotime(date('Y-m-d')))) 
			{
				$scheduleIs=1;
			}
			$buttonclass = 'button_lblue_r4';
			
			//
			//$viewMaterialSettngs = $coursematerialsettings;
		
			/*    $isHandoutAvailable = 0;
			$msgid				= '000';
			$canviewmartial = $viewMaterialSettngs->can_view_handout;
			$canviewmartialafterenrollment = $viewMaterialSettngs->can_view_material;
			if($canviewmartialafterenrollment == '1'){
				$isHandoutAvailable = 1;
			}
			else{
				$msgid 		= '555';
			} */
			//
			
			//
			/* $isCurriculumAvailableforins = 0;
			$isCurriculumAvailableforall = 0;
			$isCurriculumAvailableforenroll = 0;
			$allusercanview = $viewMaterialSettngs->all_userr_can_view_curriculum;
			$onlyinstructorcanview = $viewMaterialSettngs->instructor_can_view_curriculum;
			$enrollusercanview = $viewMaterialSettngs->user_can_view_curriculum;
			if($allusercanview == '1'){
				$isCurriculumAvailableforall = 1;
			}else if($onlyinstructorcanview == 1 ){
				$isCurriculumAvailableforins = 1;
			}else if($enrollusercanview == '1'){
				$isCurriculumAvailableforenroll = 1;
			}else if($onlyinstructorcanview == '1' && $role_id != '3'){
				$msgid 		= '666';
			}else{
				$msgid 		= '777';
			} */
			//

			//
			$enroll=2;
			$datahtmlarr=$this->classcourseDivButton($scheduleArr,$Getdate_formatecomapny,$Usertrainingcatalog,$Usertrainingcatalog,$supervisior_approval,$enroll,$buttonclass,$allocateSeat,$userCourseInfo,$courseDetailArr,$targetAudience);
			
			  $IslastScheduleDiv=1;
			if(strtotime($courseDetailArr['last_schedule_date']) >=strtotime(date('Y-m-d')))
			{
				$IslastScheduleDiv=1;
			}
			
				$isinstructor=0;
			if($roleid==3){
				
				$isinstructor=1;
			}
			
			//
				$response =array(
				  "status"=>200,
				  "message"=>"success",
				  //"curriculams"=>$curriculams,
				  "handouts"=>$handouts,
				 //"coursematerialsettings"=>$coursematerialsettings,
				  "transcript_id"=>$transcript_id,
				  "item_type_id"=>2,
				  "avgInstRating"=>$avgInstRating,
				  "avgClassRating"=>$avgClassRating,
				  "Usertrainingcatalog"=>$Usertrainingcatalog,
				  "scheduleArr"=>$scheduleArr,
				  /* "type"=>$type,
				  "viewId"=>$type, */
				  "compid"=>$compid,
				  "roleid"=>$roleid,
				  "courseDetailArr"=>$courseDetailArr,
				  "userCourseInfo"=>$userCourseInfo,
				  "scheduleIs"=>$scheduleIs,
				  "buttonclass"=>$buttonclass,
				  "allocateSeat"=>$allocateSeat,
				  "supervisior_approval"=>$supervisior_approval,
				  "strhtml"=>$datahtmlarr['strhtml'],
				  "targetstr"=>$datahtmlarr['targetstr'],
				//  "IslastScheduleDiv"=>$IslastScheduleDiv,
				  //"isHandoutAvailable"=>$isHandoutAvailable,
				  //"isCurriculumAvailableforall"=>$isCurriculumAvailableforall,
				 // "isCurriculumAvailableforins"=>$isCurriculumAvailableforins,
				  //"isCurriculumAvailableforenroll"=>$isCurriculumAvailableforenroll,
				  "isinstructor"=>$isinstructor,
				  
				);
			  return response()->json($response);
		}
	}
	
	public function classcourseDivButton($scheduleArr,$Getdate_formatecomapny,$Usertrainingcatalog,$userenrollment,$supervisior_approval,$enroll,$buttonclass,$allocateSeat,$userCourseInfo,$courseDetailArr,$targetAudience){
	
		            $str='';
		            $scheduleArrDate='0000-00-00';
					$baseUrl='';
					if(!empty($scheduleArr)){
					$date = DateTime::createFromFormat($Getdate_formatecomapny, $scheduleArr[0]['start_date']);
					$scheduleArrDate 	= 		$date->format('Y-m-d');
					}
				
					if((strtotime($scheduleArrDate)) >= (strtotime(date('Y-m-d')))) 
					{
                         	$str .='<div id="loadingiddis" style="display:none;margin-left: 142px;position: absolute;top: 87%;z-index: 1000;"><img class="wp55"src="/public/images/animatedEllipse2.gif" border="0"></div>'; 
							 $enroll_id =('enroll/1/id/'.$courseDetailArr['training_program_code'].'/su_status/'.$courseDetailArr['supervisor_approval'].'/class_id/'.$courseDetailArr['class_id']);
							  $enroll_ID =('enroll/2/id/'.$courseDetailArr['training_program_code'].'/su_status/'.$courseDetailArr['supervisor_approval'].'/class_id/'.$courseDetailArr['class_id']);
								
							if(count($userenrollment) > 0){
							if($userenrollment[0]['is_approved']!=2){
							if((@$userenrollment[0]['is_enroll']==1 && @$userenrollment[0]['training_status_id']!=5) || ($Usertrainingcatalog[0]['training_status_id']==4)) {
								
								
								$str.='<div id="enrollbutton1" class="wp100 fl"><div style="display: inline-block;" ng-click="enroll_class_drop(2, \''.CommonHelper::getEncode($courseDetailArr['training_program_code']).'\', \''.CommonHelper::getEncode($courseDetailArr['supervisor_approval']).'\', \''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');"><div class="'.$buttonclass.'" >Drop<span>&#187;</span></div></div></div>';
							
								
							}
							elseif($supervisior_approval==1 && $enroll!=2) {
								$str.='<div class="fl wp100 mt10"><div class="'.$buttonclass.'" border="0" ng-click="pending_apprv(1, \''.CommonHelper::getEncode($courseDetailArr['training_program_code']).'\', \''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');">Enroll <span>&#187;</span></div>';
							}elseif($allocateSeat==1  ){
								 //done//
							   $str.='<div class="fl wp100 mt10"><div class="'.$buttonclass.'" border="0" ng-click="checkwt(1,\''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');">Enroll<span>&#187;</span></div></div>';
							   
							}elseif(@$userenrollment[0]['is_enroll']==0 && @$userenrollment[0]['training_status_id']!=4) {
								
								//$blendedprogram_id=!empty($blendedprogram_id)? CommonHelper::getEncode($blendedprogram_id) : CommonHelper::getEncode("0");
                               $str.='<div id="enrollbutton1" class="wp100 fl"><div ng-click="re_enroll_drop_class_drop(1 ,\''.CommonHelper::getEncode($courseDetailArr['training_program_code']).'\', \''.CommonHelper::getEncode($courseDetailArr['supervisor_approval']).'\', \''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');"><div class="'.$buttonclass.'" >Enroll <span>&#187;</span></div></div></div>';
							   
							}else{
								
							//$blendedprogram_id=!empty($blendedprogram_id)? CommonHelper::getEncode($blendedprogram_id) : CommonHelper::getEncode("0");
							$str.='<div id="enrollbutton1" class="wp100 fl"><div style="display: inline-block;" ng-click="enroll_class(1, \''.CommonHelper::getEncode($courseDetailArr['training_program_code']).'\', \''.CommonHelper::getEncode($courseDetailArr['supervisor_approval']).'\', \''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');"><div class="'.$buttonclass.'" >Enroll<span>&#187;</span></div></div></div>';
								
							}
					}
					}elseif($supervisior_approval==1 && $enroll!=2) {
				
							$str.='<div class="fl wp100 mt10"><div class="'.$buttonclass.'" border="0" ng-click="pending_apprv(1, \''.CommonHelper::getEncode($courseDetailArr['training_program_code']).'\', \''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');">Enroll <span>&#187;</span></div>';
						
					}elseif($allocateSeat==1  ){
							//done//	
						$str.='<div class="fl wp100 mt10"><div class="'.$buttonclass.'" border="0" ng-click="checkwt(1,\''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');">Enroll<span>&#187;</span></div></div>';
					}else{
						
							//done//	
						//$blendedprogram_id=!empty($blendedprogram_id)? CommonHelper::getEncode($blendedprogram_id) : CommonHelper::getEncode("0");
						$str.='<div id="enrollbutton1" class="wp100 fl"><div style="display: inline-block;" ng-click="enroll_class(1, \''.CommonHelper::getEncode($courseDetailArr['training_program_code']).'\', \''.CommonHelper::getEncode($courseDetailArr['supervisor_approval']).'\', \''.CommonHelper::getEncode($courseDetailArr['class_id']).'\');"><div class="'.$buttonclass.'" >Enroll<span>&#187;</span></div></div></div>';
					}
					$str.='<div style="display:block" id="msge" class="edit-user-update fs12 b  fc03 w600 fta  mt10 pl01 mb10 ml18 fl">';
								if($userCourseInfo[0]['user_status'] !=''){
									$str.='You already have status "'.$userCourseInfo[0]['user_status'].'" for a class of same course.</br>';
								}
								if( @$Usertrainingcatalog[0]['is_approved']==2){
									$str.="Approval denied.";
								}elseif(count($Usertrainingcatalog) == 0 ){
									$str.= "(Note: enrolling for this class will enroll you for all sessions of the class)<br>You are currently not enrolled.";
								}elseif($Usertrainingcatalog[0]['is_enroll']==0 && $Usertrainingcatalog[0]['training_status_id']!=4){
									$str.="(Note: enrolling for this class will enroll you for all sessions of the class)<br>You are currently not enrolled.";
								}else{
									if( @$Usertrainingcatalog[0]['training_status_id']==10)
									{
										$str.= "Pending Approval";
									}elseif( @$Usertrainingcatalog[0]['training_status_id']==4)
									{
										$str.="You are currently in waiting list.";
									}
									else{
										if(@$Usertrainingcatalog[0]['training_status_id']==1)
										{
											$str.="You have completed this course.";
										}
										elseif(@$Usertrainingcatalog[0]['training_status_id']==3){
											$str.= "You are currently enrolled.";
										}
									}	
								}
							
							$str.='</div>';
					} 
					elseif(($courseDetailArr['class_id']=='') || (strtotime($scheduleArrDate)) < (strtotime(date('Y-m-d'))))
					{	
					   //done//
						$str.='<div id="enrollbutton1" class="wp100 fl"><div ng-click=submitraining("'.CommonHelper::getEncode($courseDetailArr['course_id']).'")><div class="'.$buttonclass.'" >Enroll <span>&#187;</span></div></div></div>';
					} 
						
				
					
			    $targetstr='';
				$targetstr.=	'<div class="wp100 mt10 fl">';
					if(count(@$targetAudience)>0)
					{
						$targetstr.='<div class="widget box fl wp100">';
						$targetstr.='<div class="widget-header"><h4><i class="fa fa-users o blue_1"></i>
						Target Audience</h4> </div>';

						$targetstr.= '<div style="height: 160px; overflow:auto; scrollbar-base-color:#9f9f9f; scrollbar-arrow-color:#9f9f9f;">';

						$user = 0;
						$grp = 0;
						foreach ($targetAudience as $key => $val) {
						if($val['type_name'] == 'user')
						{
							if($user == 0){
								$user++;

							$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>User Name</b></div>';
							} 
								$targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
                               
									}else{
										if($grp == 0){
											$grp++;
								
										$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Groups</b></div>';
										 }
                                      $targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
								
										} 
									}
								 $targetstr.='</div></div>';
						} 
					 $targetstr.='</div>';
					
               return array('strhtml'=>$str,'targetstr'=>$targetstr);
	}
	
	
	 public function confirmenrollAction(Request $request){
		
		$allocateSeat = $request->input('allocateSeat',0);
		if($allocateSeat == ''){
			$allocateSeat=0;
		}
		$userprogramtypeid = $request->input('userprogramtypeid',2);
		if($userprogramtypeid == ''){
			$userprogramtypeid = 2;
		}
		
		$token 					= $request->input('token');
		$training_program_code 	= CommonHelper::getDecode($token);
		
		$classid = $request->input('scheduleid','');
		if($classid == ''){
			$classid = '';
		}
		$enroll = $request->input('enroll',0);
		if($enroll == ''){
			$enroll = 0;
		}
		/* if(!is_numeric($allocateSeat))
		{
			$allocateSeat = CommonHelper::getDecode($allocateSeat);
		} */
		/* if(!is_numeric($userprogramtypeid))
		{
			$userprogramtypeid = CommonHelper::getDecode($userprogramtypeid);
		} */
		
		if(!is_numeric($classid))
		{
			$classid = CommonHelper::getDecode($classid);
		}
		
		//echo "$enroll ===  $classid === $learningplanid == $userprogramtypeid === $allocateSeat == $blendedprogid"; exit;
		     // 7938      ===       0        ==      2             ===       0       ==     0
		$user_data =Auth::user();
		$compid = $user_data->company_id;
		$userid = $user_data->user_id;
		$roleid = $user_data->role_id;
		if($enroll==1)
		{   
				//CommonHelper::blendedutcentry($blendedprogid,$userid,$compid);
				$from='user';
				$strclass=Enrolleduserfromallplace::enrollUserToClass($userid,$roleid, $compid,$classid,$from,$training_program_code);
				 $result =array('status'=>200,'message'=>'success');
		
		}
		else
		{
		
			Enrolleduserfromallplace::dropenrolfromclass($userid,$compid,$classid,$training_program_code);
			 $result =array('status'=>200,'message'=>'success');
		}
		 return response()->json($result);
	}
	
	public function classroaster(Request $request){
		$classid=$request->input('classid');
		$itemid=$request->input('itemid');
		$ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$roleid = $ses->role_id;
		$classid=7008;
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$Users = Usertrainingcatalogs::getenrolleduserssforroster($compid, $itemid, $classid);

		$dataArray=array();
		$i=1;
		foreach($Users as $value)
		{ 
			$user_name	= $value->user_name;
			$first_name = $value->first_name;
			$last_name = $value->last_name;
			$jobTitles= $value->jobTitles;
			if($value->createdDate!='0000-00-00 00:00:00'){$dates= date($Getdate_formatecomapny,strtotime($value->createdDate));}else{$dates= "--";}
			$area=$value->area;
			$location=$value->location;
			if($value->training_status_id==3)
				$valststus=3;
			else
				$valststus=$value->training_status_id;
			$Userstatus = UsertraningStatus::where('user_status_id','!=',8)->get()->toArray();
		
			$Userstatusarray = array();
			foreach ($Userstatus as $rows) {
				  $Userstatusarray[$rows['user_status_id']] = $rows['user_status'];
			}
			$status= $Userstatusarray[$valststus];
			
			$dataArray[]=array('first_name'=>$first_name,'last_name'=>$last_name,'jobTitles'=>$jobTitles,'dates'=>$dates,'status'=>$status);
		}
	    return response()->json($dataArray);
	}
	
	public function classhandouts(Request $request){
		$ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$roleid = $ses->role_id;
		$itemId	= $request->input('id');
		$handouts = Classroomtraining::getClassHandouts($itemId,$compid);
		$strhandout=$this->handoutorcaricullamhtml($handouts,1);
		return response()->json(array('strhandout'=>$strhandout));
	}
	
	public function handoutorcaricullamhtml($dataarra,$type){
       if($type==2)
	   {
         $name= 'Class Materials - Curriculum';
         $message='There are no curriculum for this class.';

	   }else{
         $name= 'Class Materials - Handouts';
         $message='There are no current handouts for this class';
	   }	   
	   
		$baseUrl = URL::to('/');
		$str='';
		 if(count($dataarra)>0){
		$str.='<div class="widget box fl wp100 mt15">';
		$str.='<div class="widget-header">';
		$str.='<h4>';
		$str.=''.$name.'';
		$str.='</h4>';                                       
		$str.='</div>';	
		$str.='<div class="widget-content" style="padding: 0px; overflow-y: auto; height: 235px;">';
		$str.='<div class="currently-assigned-users-box wp100 fl">';
		$str.='<div class="assigned-user wp100 fs13 fta fc03 b fl">';
		$str.='<div class="scrool-box wp100 fl">';
		$str.='<div>';
		$i=0;if(count($dataarra)>0){   
		 
		foreach($dataarra as $key =>$value){
		$style=($i++ % 2 == 0) ? 'Name wp100 p05 bgc07 t-b-12 fl bob01': 'Name bob01 wp100 p05 bgc05 t-b-12 fl';
		if($type==1)
		{
			$a = explode('/',strrchr($value['handout_path'],'/'));
			$title=$a[1];
		}else{
			$title =$value['file_name'];
		}
		
		$str.='<div class="'.$style.'">';
		
		$str.='<div class="sales-team wp94 fl" title="'.$title.'">'.$value['file_name'].'</div>';
		$str.='<div class="fl" id="filenameid"></div>';
		$str.='<div class="cross-icon fl" id="downloadid"><a target="_blank" href="'.$baseUrl.'/eliteprodlms/uploadfiles/'.$title.'" download="'.$title.'"  title="'.$title.'"><img src="'.$baseUrl.'/eliteprodlms/public/images/drop_arrow_pop.png" alt=""/></a></div>';
		'</div>';
	    }
		}
		$str.='</div>';
		$str.='</div>';
		$str.='</div>';
		$str.='<div class="fl wp50"></div>';
		$str.='</div>';
		}else{
		$str.='<div class=" fc02 fta fs13 b tac mt30"><center>'.$message.'</div>';
		}
		$str.='</div style="clear: both;"></div></div>';
		
	    return $str;
	}
	
	public function curriculams(Request $request)
	{		
	   $ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$roleid = $ses->role_id;
		$itemId	= $request->input('id');
		$curriculams = Classroomtraining::getClassCurriculum($itemId,$compid);
		$carriculahtml=$this->handoutorcaricullamhtml($curriculams,2);
		return response()->json(array('carriculahtml'=>$carriculahtml));
	}
	
	public function trainingrequestpopupAction(Request $request) 
	{	
		$ses = Auth::user();
		$compid = $ses->company_id;
		$roleid = $ses->role_id;
		$userid = $ses->user_id;
		$itemId = CommonHelper::getDecode($request->input('itemId'));
		$request=$request->input('request');
        $itemArr = ItemMaster::where('item_id','=',$itemId)->where('company_id','=',$compid)->where('is_active','=',1)->select('item_name')->first();
		$itemName = $itemArr->item_name;
		if($request == 'submit')
		{
			$data['title'] 			= $itemName;
			$data['description'] 	= '';
			$data['user_id'] 		= $userid;
			$data['company_id'] 	= $compid;
			$data['last_updated_date'] 	= date('Y-m-d H:i:s');
			SubmitTrainingMaster::insert($data);
			Mailnotification::courseRequestmail($itemArr,$userid,$compid);
		    return response()->json(array('status'=>200,'message'=>'Success'));
		}
	}
	
	
}
