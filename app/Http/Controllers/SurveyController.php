<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,CommonHelper,CommonFunctions;
use App\Http\Traits\Assessments;
use App\Http\Traits\Trainingprogram;
use App\Http\Traits\Assessmentcrypt;
use App\Models\UserassessmentMaster;
use App\Models\UsersAssessmentResponse;
use App\Models\UserassessmentProp;
use App\Models\UsertrainingCatalog;
//use App\Models\UserAssessment;
use App\Models\UserTranscript;
use App\Models\UserTranscriptHistory;
use App\Models\AssessmentResponseTranscript;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Trainingcatalog;
use App\Models\UserTrainingHistory;
use App\Models\Course;
use App\Models\UserTrainingCatalogHistory;
use Session;
class SurveyController extends Controller
{
	use Assessments;
   
	
	public function viewassessmentAction(Request $request) 
	{
		$sessiondata=Auth::user();
		$company_id=$sessiondata->company_id;
		$roleid=$sessiondata->role_id;
		$userid=$sessiondata->user_id;
		
		$viewId=$request->input('viewId');
		
		$courseDetailArr = $prerequeisites = $support_formats = array();
		$filenames = $usertrainingid = '';
		$media_id = $averageRating = $userAverageRating = 0;	
		
		$trainingprogram_code = $viewId;
		$courseDetailArr['trainingprogram_code'] = $trainingprogram_code;
		
		$programs = Trainingprogram::getCourseTrainingDetail($company_id, $trainingprogram_code);
		
		if ($programs) 
		{
			$course_id = $programs->course_id;	
			$courseDetailArr['user_id'] 					= 	$userid;
			$courseDetailArr['course_id'] 					= 	$programs->course_id;
			$courseDetailArr['company_id'] 					= 	$company_id;
			$courseDetailArr['training_type'] 				= 	$programs->training_type;
			$courseDetailArr['credit_value'] 				= 	$programs->credit_value;
			$courseDetailArr['course_image'] 				= 	$programs->course_image;
			$courseDetailArr['training_title'] 				= 	$programs->training_title;
			$courseDetailArr['description'] 				= 	$programs->course_description;
			$courseDetailArr['prerequeisites'] 				= 	$programs->prerequeisites;
			$courseDetailArr['training_type_id']			= 	$programs->training_type_id;
			$courseDetailArr['training_program_code']		= 	$programs->training_program_code;
			$courseDetailArr['average_rating'] 				= 	$programs->average_rating;
			$courseDetailArr['TrainingCategoryselected'] 	= 	$programs->category_name;	
			$courseDetailArr['type'] 					    = 	'';
			$courseDetailArr['userprogramtypeid'] 			=    5;
			$alltargetaud 									= 	Trainingprogram::targetaudiences($company_id, $course_id);
			$targetaudthtml									=   $this->targetaudiencehtml($alltargetaud);
			$courseDetailArr['alltargetaud'] 				= 	$targetaudthtml;
		}

			$courseDetailArr['completion_status'] 				= Trainingprogram::checkTrainingExpiration($course_id, $userid, $company_id);
			$view=0;
            $detailAssessmnt= Assessments::getassessmentname($company_id, $course_id);
			$view_mode=$detailAssessmnt->view_mode;
			$enrollAssessment = array(
			"confirm"=>1 , 
			"training_program_code"=>$courseDetailArr['training_program_code'], 
			"userprogramtypeid"=>$courseDetailArr['userprogramtypeid'], 
			"course_id"=>$courseDetailArr['course_id'], 
			"view_mode"=>$view_mode, 
			"completion_status"=>$courseDetailArr['completion_status'], 
			"company_id"=>$company_id,
			"user_id"=>$userid,
			"view"=>$view
			);
		
			$courseDetailArr['checkhandout'] ='';
			$courseDetailArr['transcript_id'] = Trainingprogram::getTranscriptId($company_id, $userid, $course_id);		
		
			$response =array(
			  "status"=>200,
			  "courseDetailArr"=>$courseDetailArr,
			  "enrollelearning"=>CommonHelper::getEncode(json_encode($enrollAssessment)),
			);
			return response()->json($response); 
	}
	
	public function targetaudiencehtml($targetAudience){
		
			$targetstr='';
			$targetstr.=	'<div class="wp100 mt10 fl">';
			if(count(@$targetAudience)>0)
			{
				$targetstr.='<div class="widget box fl wp100">';
				$targetstr.='<div class="widget-header"><h4><i class="fa fa-users o blue_1"></i>
				Target Audience</h4> </div>';

				$targetstr.= '<div style="height: 160px; overflow:auto; scrollbar-base-color:#9f9f9f; scrollbar-arrow-color:#9f9f9f;">';

				$job = 0;
				$grp = 0;
				foreach ($targetAudience as $key => $val) {
					if($val['type_name'] == 'user')
					{
						if($job == 0){
							$job++;

							$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Users</b></div>';
						 } 
							$targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
						   
					}else{
						if($grp == 0){
										$grp++;
							
									$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Groups</b></div>';
									 }
								  $targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
							
					  } 
				 }
				  $targetstr.='</div></div>';
				} 
			 $targetstr.='</div>';
			 return $targetstr;
	}
	
	
	public function enrollelearning(Request $request){
		
		    $enrollelearning_arr=json_decode(CommonHelper::getDecode($request->input('enrollelearning_data')));
            $ses=Auth::user();
			$roleID = $ses->role_id;
			$compid = $ses->company_id;
			$userid = $ses->user_id;

			$training_program_code = $userprogramtypeid = $course_id =0;
			
			foreach($enrollelearning_arr as $key=>$value) $$key=$value;
			if(!$view) $view = "0";
			
			if(is_int((int)$training_program_code) && is_int((int)$userprogramtypeid) && is_int((int)$course_id))
			{
                 
				
				if($view=="0")
				{
					$utcdataid = Trainingprogram::getUserTrainingId($course_id, $userid, $compid);
					if(!$utcdataid)
					{
						$usertrainingid = Trainingprogram::takeTraining($course_id, $userid, $userprogramtypeid, $compid);
					}
					else
					{				
						if($completion_status=="retake")
						{
							$utc_res = Trainingprogram::getRetakeUserTrainingId($course_id,$userid,$compid,$training_program_code);
							if($utc_res){
								$usertrainingid = $utc_res->user_training_id;
							}else{
								$usertrainingid =  Trainingprogram::takeTraining($training_program_code, $userid,$userprogramtypeid, $compid);
							}
						}
						else{
							$usertrainingid = $utcdataid->user_training_id;
						}
					}
				}else{
					$usertrainingid = '';
				}

				$mode = $action_url = '';
				
				$res_arr = array(
					"confirm"=>isset($confirm)?$confirm:'',
					"expiration_status"=>isset($expiration_status)?$expiration_status:'',
					"username"=>isset($username)?$username:'',
					"userprogramtypeid"=>$userprogramtypeid,
					"training_program_code"=>$training_program_code,
					"course_id"=>$course_id, 
					"usertrainingid"=>$usertrainingid,
					"launchtime"=>date('Y-m-d h:i:s'),
					"view"=>$view
				);
				//$view_mode== 0 slide
				//$view_mode== 1 list
				if($view_mode==1){
				  $action_url = 'mylearning/viewlistquizandsurvey/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
				}else{
					$action_url = 'mylearning/viewquizandsurvey/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
				}
			 
				exit('{ "param":"'. $action_url .'",  "mode":"tb_show"}');
			}
			else{
				exit('Wrong URL Called');
			}
	}
	
	public function viewListAssessment($viewId)
	{
		$view_arr=json_decode(CommonHelper::getDecode($viewId));
		foreach($view_arr as $key=>$value) $$key=$value;
		
		$userid = Auth::user()->user_id;
		$compid = Auth::user()->company_id;
		
		$Getdate_formatecomapny =  CommonHelper::getdate($compid);
		$fetchvalues =CommonFunctions::getaccessurl();
		$useranswer="";
		$istimer=0;
		if($fetchvalues) 
		{
			if ($fetchvalues['company_id'] != 0) 
			{
				$compid = $fetchvalues['company_id'];
				$realcompid = $compid;
			}
			if ($fetchvalues['company_id'] == 0) 
			{
				return response()->json(['status'=>408,'message'=>$fetchvalues['msg']]); exit;
			}
		}
		  $totalsecond='';
		  $assessid=$course_id;
		if(isset($user_assess_resp_id) && isset($transcript_id) && ($user_assess_resp_id!=0) && ($transcript_id!=0)){
			$assementdetails = Assessments::fetchUserAssessment($userid, $training_program_code, $compid,$user_assess_resp_id);
			
		}else{
			$userAssessProp=UserassessmentProp::whereuserId($userid)->wherecourseId($assessid)->wherecompanyId($compid)->orderby('user_assessment_prop_id','desc')->first();
			$prop_id=($userAssessProp)?$userAssessProp->user_assessment_prop_id:0;
			$userselect=array();
			$from_transcript=2;
			$assementdetails = Assessments::getAssessmentQuizdetails($userid,$training_program_code, $compid,$prop_id);
			if($assementdetails['time_limit']!='00:00:00'){
				$time=explode(':',$assementdetails['time_limit']);
				$h=$time[0]*3600;
				$m=$time[1]*60;
				$s=$time[2];
				$totalsecond=$h+$m+$s;
				$istimer=1;
			}
			
		}
		
		$fetchuploadedfiles=array();
		//$fetchuploadedfiles=Assessments::getmyuploadedassessment($compid, $training_program_code);
		
		if(!$assementdetails['user_attemt_taken'])
		{
			return view('user.assessment.viewlistassessment',['user'=>$userid,'token'=>$viewId,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'nooftimes'=>$assementdetails['no_of_times'],'attempttaken'=>$assementdetails['attempt_taken'],'userattemttaken'=>$assementdetails['user_attemt_taken'],'allowassessment'=>$assementdetails['allow_assessment'],'assementdetails'=>$assementdetails,'totalrightper'=>100,'fetchuploadedfiles'=>$fetchuploadedfiles,'mode'=>'','totalsecond'=>$totalsecond,'istimer'=>$istimer]);
		}
        else
		{
		
			return view('user.assessment.successfullsubmitlist',['user'=>$userid,'token'=>$viewId,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'nooftimes'=>$assementdetails['no_of_times'],'attempttaken'=>$assementdetails['attempt_taken'],'userattemttaken'=>$assementdetails['user_attemt_taken'],'allowassessment'=>$assementdetails['allow_assessment'],'assementdetails'=>$assementdetails,'totalrightper'=>$assementdetails['totalrightper'],'msg'=>'y']);
		} 
	}
	
	public function viewQuizSurvey($viewId) 
	{
		$view_arr=json_decode(CommonHelper::getDecode($viewId));
		
		foreach($view_arr as $key=>$value) $$key=$value;
		
		$userid = Auth::user()->user_id;
		$compid = Auth::user()->company_id;
		
		$Getdate_formatecomapny =  CommonHelper::getdate($compid);
		$fetchvalues =CommonFunctions::getaccessurl();
		$useranswer="";
		if($fetchvalues) 
		{
			if ($fetchvalues['company_id'] != 0) 
			{
				$compid = $fetchvalues['company_id'];
				$realcompid = $compid;
			}
			if ($fetchvalues['company_id'] == 0) 
			{
				return response()->json(['status'=>408,'message'=>$fetchvalues['msg']]); exit;
			}
		}
		$assessid=$course_id;
	
		$from_transcript=2;
		if(isset($user_assess_resp_id) && isset($transcript_id) && ($user_assess_resp_id!=0) && ($transcript_id!=0)){
			  $assementdetails = Assessments::fetchUserAssessment($userid, $training_program_code, $compid,$user_assess_resp_id);
	          $assess_data =$this->finduserpassAndFail('','', '',$assementdetails,1,$userid);
			
		}else{
			$userAssessProp=UserassessmentProp::whereuserId($userid)->wherecourseId($assessid)->wherecompanyId($compid)->orderby('user_assessment_prop_id','desc')->first();
			$prop_id=($userAssessProp)?$userAssessProp->user_assessment_prop_id:0;
			
			$assess_data =$this->finduserpassAndFail($userAssessProp,$compid, $assessid,array(),2,$userid);
			$assementdetails = Assessments::getAssessmentQuizdetails($userid,$training_program_code, $compid,$prop_id);
		}
		
		$fetchuploadedfiles=array();
		$totalsecond=0;
		$istimer=0;
		if(!$assementdetails['user_attemt_taken']){
			if($assementdetails['time_limit']!='00:00:00'){
			$time=explode(':',$assementdetails['time_limit']);
			$h=$time[0]*3600;
			$m=$time[1]*60;
			$s=$time[2];
			$totalsecond=$h+$m+$s;
			$istimer=1;
			}
			return view('user.assessment.viewassessment',['assementdetails'=>$assementdetails,'fetchuploadedfiles'=>$fetchuploadedfiles,'user'=>$userid,'token'=>$viewId,'from_transcript'=>$from_transcript,'istimer'=>$istimer,'totalsecond'=>$totalsecond]);
		}
        else
		{
			
			return view('user.assessment.successfullsubmitasse',['user'=>$userid,'token'=>$viewId,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'nooftimes'=>$assementdetails['no_of_times'],'attempttaken'=>$assementdetails['attempt_taken'],'userattemttaken'=>$assementdetails['user_attemt_taken'],'allowassessment'=>$assementdetails['allow_assessment'],'assementdetails'=>$assementdetails,'totalrightper'=>100,'from_transcript'=>$from_transcript,'assess_data'=>$assess_data,'istimer'=>$istimer,'totalsecond'=>$totalsecond]);
		}
		
	}
	
	public function finduserpassAndFail($userAssessProp,$compid, $assessid,$arrayData,$type,$userid)
	{
		 if($type==2){
			$flag=($userAssessProp)?$userAssessProp->correct_answers:0;
			$assess_data = Assessments::getAssessmentDetailForPopup($compid, $assessid, 0);//fetch data from assessment
			$count_response = count(Assessments::usersassessmentresponse($assessid, $userid, $compid));//fetch uar.id from users_assessment_response
			if(count($assess_data)>0){
				$assess_data[0]['attempt_taken'] = $assess_data[0]['attempt_taken'] - $count_response;
				$assess_data[0]['user_score'] = '0';
				$assess_data[0]['user_status'] = 'Fail';
			}

			if($flag > 0 && count($assess_data)>0)
			{
				$qs_data = Assessments::getAssessQuesPopupExceptDescType($assessid);//fetch data from assess_question
				$assess_data[0]['user_score'] = round(($flag*100)/$qs_data[0]['total_ques']);
				$assess_data[0]['user_score'] = round(($flag*100)/$qs_data[0]['total_ques']);
				if($assess_data[0]['user_score'] >= $assess_data[0]['pass_percentage'])
					$assess_data[0]['user_status'] = 'Pass';
			}

			$assess_data['pp']=isset($assess_data[0])?$assess_data[0]['pass_percentage']:100;
			$assess_data['usr']=isset($assess_data[0])?$assess_data[0]['user_score']:0;
			$assess_data['uss']=isset($assess_data[0])?$assess_data[0]['user_status']:'Fail';
		 }else{
	
			$user_status = 'Fail';
			
			if($arrayData['totalrightper'] >= $arrayData['pass_percentage'])
			{
				$user_status = 'Pass';
			}
			 
			$assess_data['pp']= $arrayData['pass_percentage'];
			$assess_data['usr']=$arrayData['totalrightper'];
			$assess_data['uss']=$user_status;
		 }
		return $assess_data;
	}
	
	public function submitassessmentAction(Request $request) 
	{
		
		    $userid = $request->input('user');
			$token = $request->input('token');
			$skipids = $request->input('skipids');
			$view_arr=json_decode(CommonHelper::getDecode($token));
		   
			foreach($view_arr as $key=>$value) $$key=$value;
			$skipids=explode(',',$skipids);
			
			$assessid = $course_id ;
			if($assessid && $userid) {
				$compid = Auth::user()->company_id;

				$duedate= date('Y-m-d');
			
				$flag=0;
				
				$fetchassessments = Assessments::attemptedquestions($assessid, $userid, $compid);
				
				if(count($fetchassessments) > 0) {
					UserassessmentMaster::whereuserId($userid)->wherecourseId($assessid)->delete();
				}
				$i=0;
				while($i<=6)
				{
					unset($_REQUEST['questype'.$i]);
					$i++;
				}
				$responce1 = $responce = $stockList = array();
				foreach($_REQUEST as $stockKey => $stock)
				{
					if($stock) {
						$data = array();
						if(sscanf($stockKey, "multichoiceoption_%d", $stockId)) {
							if(in_array($stockId,$skipids)) continue;//user has skipped this question so don't track it.
							$question_type = 'multichoice';
							if($stockId && !empty($stock))  {
								$a1 = explode('||', $stock);
								$questionid = $a1[0];
								$answerid = $a1[1];
								$data['question_id'] = $questionid;
								$data['course_id'] = $assessid;
								$data['answer'] = $answerid;
								$data['descriptive'] = '';
								$data['user_id'] = $userid;
								$data['last_updated_date'] = date('Y-m-d -h-i-s');
								$data['created_date'] = date('Y-m-d -h-i-s');
								
								UserassessmentMaster::create($data);
								$student_response = 'multichoiceoption_'."$questionid".'_'."$answerid";
								$answer_id = Assessments::fetchassessmenanswerid($assessid, $questionid);
								$correct_response = $answer_id[0]->answer;
								$result = 'Incorrect';
								if($correct_response) {
									$correct_response = 'multichoiceoption_'."$questionid".'_'."$correct_response";
								}
								
								if($correct_response == $student_response) { 
								       $flag++;
									   $result = 'Correct';
							    }
								$userans = ltrim(Assessments::fetchAnswerForResp($assessid, $questionid, $answerid), '<p>');
								$userans = rtrim($userans, '</p>');
								$crctans = ltrim(Assessments::fetchAnswerForResp($assessid, $questionid, $answer_id[0]->answer), '<p>');
								$crctans = rtrim($crctans, '</p>');
								array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$userans,"correct_response"=> $crctans,"result"=>$result,"weighting"=>0,));	
							 }
						}
							if(sscanf($stockKey, "trueval_%d", $stockId)) {
                                    if(in_array($stockId,$skipids)) continue;//user has skipped this question so don't track it.
                                    $question_type = 'truefalse';									
									if($stockId && !empty($stock)) {
										$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
										$data['answer'] = $stock;
										$data['descriptive'] = '';
										$data['question_id'] = $questionid;
										$data['course_id'] = $assessid;
										$data['user_id'] = $userid;
										$data['last_updated_date'] = date('Y-m-d -h-i-s');
										$data['created_date'] = date('Y-m-d -h-i-s');
										UserassessmentMaster::create($data);
										$student_response = 'trueval_'."$questionid";
										$answer_id = Assessments::fetchassessmenanswerid($assessid, $questionid);
										$correct_response = $answer_id[0]->answer;
										$result = 'Incorrect';
										
										if($stock == $correct_response) { $correct_response = 'trueval_'."$questionid"; }
								        if($correct_response == $student_response) { $flag++; $result = 'Correct'; }
										array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$stock,"correct_response"=> $answer_id[0]->answer,"result"=>$result,"weighting"=>0,));
							        }
							}
							
							if(sscanf($stockKey, "descriptive_%d", $stockId)) {
							         if(in_array($stockId,$skipids)) continue;//user has skipped this question so don't track it.
									 $question_type = 'textans';
									 if($stockId && !empty($stock)) {
										$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
                                        $data['question_id'] = $questionid;
										$data['answer'] = '';
										$data['course_id'] = $assessid;
										$data['descriptive'] = $stock;
										$data['user_id'] = $userid;
										$data['last_updated_date'] = date('Y-m-d -h-i-s');
										$data['created_date'] = date('Y-m-d -h-i-s');	
                                        UserassessmentMaster::create($data);
                                        $student_response = 'descriptive_'."$questionid"; 
										
                                        $answer_id = Assessments::fetchassessmenanswerid($assessid, $questionid);
										$correct_response = $answer_id[0]->answer;										
										$result = '[TextBase Question]';
										if($stock == $correct_response) { $correct_response = 'descriptive_'."$questionid"; }
										else { $correct_response = 'descriptive_'."$questionid"; }
										array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$stock,"correct_response"=> $answer_id[0]->descriptive,"result"=>$result,"weighting"=>0,));
							         }
						    }
							
							if(sscanf($stockKey, "mulseloption_%d", $stockId)) {
							     if(in_array($stockId,$skipids)) continue;//user has skipped this question so don't track it.
								 $question_type = 'multiselect';
								 if($stockId && !empty($stock))  {
									    $a1 = explode('||', $stock);
									    $questionid = $a1[0];
									    $answerid = $a1[1];	
										$data['question_id'] = $questionid;
										$data['course_id'] = $assessid;
										$data['answer'] = $answerid;
										$data['descriptive'] = '';
										$data['user_id'] = $userid;
										$data['last_updated_date'] = date('Y-m-d -h-i-s');
										$data['created_date'] = date('Y-m-d -h-i-s');	
                                        UserassessmentMaster::create($data);
                                        $student_response = 'mulseloption_'."$questionid".'_'."$answerid";										
                                        $answer_id = Assessments::fetchassessmenanswerid($assessid, $questionid);
										$correct_response = $answer_id[0]->answer;
                                        $result = 'Incorrect';
										
                                        if($correct_response) { $correct_response = 'mulseloption_'."$questionid".'_'."$correct_response"; }
								        else { $correct_response = 'mulseloption_'."$questionid".'_'."$correct_response"; }										
										if($correct_response == $student_response){
											$result = 'Correct';
										}

								        array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$answerid,"correct_response"=> ($answer_id) ? $answer_id[0]->answer : '',"result"=>$result,"weighting"=>0,));
							     }
						    }
							
							if(sscanf($stockKey, "linkertvalueinedit_%d", $stockId)) {
							     if(in_array($stockId,$skipids)) continue;//user has skipped this question so don't track it.
								 $question_type = 'link';
								 if($stockId && !empty($stock)){
									$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
									$data['question_id'] = $questionid;
									$data['course_id'] = $assessid;
									$data['answer'] = $stock;
									$data['descriptive'] = '';
									$data['user_id'] = $userid;
									$data['last_updated_date'] = date('Y-m-d -h-i-s');
									$data['created_date'] = date('Y-m-d -h-i-s');
									 UserassessmentMaster::create($data);
									$student_response = 'linkertvalueinedit_'."$questionid"; 
									 $answer_id = Assessments::fetchassessmenanswerid($assessid, $questionid);
									$correct_response = $answer_id[0]->answer;
									$result = 'Incorrect';
									if($correct_response) { $correct_response = 'linkertvalueinedit_'."$correct_response"; }
									if($correct_response == $student_response) { $flag++; $result = 'Correct'; }
									
									array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$stock,"correct_response"=> $answer_id[0]->answer,"result"=>$result,"weighting"=>0,));
								}
						    }
							
							if(sscanf($stockKey, "yesradio_%d", $stockId)) {
								if(in_array($stockId,$skipids)) continue;//user has skipped this question so don't track it.
								$question_type = 'yesno';
								if($stockId && !empty($stock))
								{
									$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
									$data['question_id'] = $questionid;
									$data['course_id'] = $assessid;
									$data['answer'] = $stock;
									$data['descriptive'] = '';
									$data['user_id'] = $userid;
									$data['last_updated_date'] = date('Y-m-d -h-i-s');
									$data['created_date'] = date('Y-m-d -h-i-s');
									UserassessmentMaster::create($data);
									$student_response = 'yesradio_'."$questionid"; 
									$answer_id = Assessments::fetchassessmenanswerid($assessid, $questionid);
									$correct_response = $answer_id[0]->answer;
									$result = 'Incorrect';
									if($stock == $correct_response) { $correct_response = 'yesradio_'."$questionid"; }
									if($correct_response == $student_response) { $flag++; $result = 'Correct'; }
									
									array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$stock,"correct_response"=> $answer_id[0]->answer,"result"=>$result,"weighting"=>0,));
								}
							}

					}
				}
				
				$i=0; $respid = array();
				foreach($responce as $resp)
				{
					$qsid = $resp['qid'];
					if($resp['question_type'] == 'multiselect' && !in_array($qsid, $respid)) {
						$userans = $crctans = array();
						$respid[0] = $resp['qid'];
						$uans = $resp['student_response'];
						$cans = $resp['correct_response'];
						$userCorrectAnswer=array(); //b
						$correctAnswer=array();    //a
						if($uans) {
							$userans = Assessments::fetchUserAnswerForMultiple($assessid, $qsid, $userid, 2);//fetch data from user_assessment_master
							
							$corrres1=explode(',',$userans);
								foreach($corrres1 as $userCorrectAnswerData)
								{
									$userCorrectAnswer[]= $userCorrectAnswerData;
								}
						}
						
						
						if($cans) {
							$crctans = Assessments::fetchCorrectAnswerForMultiple($assessid, $qsid, 2);//fetch data from assess_answer_type_master
							
							$correct_response1 =$crctans;
							$corrres=explode(',',$correct_response1);
							
							foreach($corrres as $correctAnswerData)
							{
							$correctAnswer[]=$correctAnswerData;
							}
							
						}
						$responce1[$i] = $resp;
						$correctCount=0; //kk
							foreach($userCorrectAnswer as $userCorrect)
							{
								if(in_array($userCorrect,$correctAnswer))
								{
								    $correctCount++;
								}
							}
							
							if(count($correctAnswer)==$correctCount){
								$result1='Correct';
							}else{
								$result1='Incorrect';
							}
							$responce1[$i]['student_response'] = $userans;
							$responce1[$i]['correct_response'] = $crctans;
							$responce1[$i]['result'] = $result1;
							$i++;
						
						
					}else if($resp['question_type'] == 'multiselect' && in_array($qsid, $respid)) {
						$i--;
						if($resp['result'] == 'Incorrect')
								{ $responce1[$i]['result'] = 'Incorrect'; }
						$i++;
					}else if($resp['question_type'] != 'multiselect'){
						$responce1[$i] = $resp;
						$i++;
					}
				}
				
				foreach($responce1 as $resp)
				{
					if($resp['question_type'] == 'multiselect' && $resp['result'] == 'Correct')
					{ $flag++; }
				}
			
				$user_responce=json_encode($responce1);
				
				//
				if($_REQUEST['confirmedradio'] == 'yes') {
			        $this->updateAssessmentcompletion($view_arr,$user_responce); 
				}
				 //
				$assess_data = Assessments::getAssessmentDetailForPopup($compid, $assessid, 0);//fetch data from assessment
			
				$count_response = count(Assessments::usersassessmentresponse($assessid, $userid, $compid));//fetch uar.id from users_assessment_response
				if(count($assess_data)>0){
				$assess_data[0]['attempt_taken'] = $assess_data[0]['attempts'] - $count_response;
				$assess_data[0]['user_score'] = '0';
				$assess_data[0]['user_status'] = 'Fail';
				$assess_data[0]['pass_percentage']=$assess_data[0]['pass_percentage'];
				}
				
				if($flag > 0 && count($assess_data)>0)
				{
					
					$qs_data = Assessments::getAssessQuesPopupExceptDescType($assessid);//fetch data from assess_question
					$assess_data[0]['pass_percentage']=$assess_data[0]['pass_percentage'];
					$assess_data[0]['user_score'] = round(($flag*100)/$qs_data[0]['total_ques']);
					$assess_data[0]['user_score'] = round(($flag*100)/$qs_data[0]['total_ques']);
					if($assess_data[0]['user_score'] >= $assess_data[0]['pass_percentage'])
						$assess_data[0]['user_status'] = 'Pass';
				}
				
				return response()->json(['status'=>201,'details'=>$assess_data]);
			
			}
	}
	

	public function updateAssessmentcompletion($view_data_arr,$user_responce) 
	{
		
           foreach($view_data_arr as $key=>$value) $$key=$value;
		
			$ses			= Auth::user();
			$compid		 	= $ses->company_id;
			$userid 		= $ses->user_id;
			
			$dataall=UsertrainingCatalog::where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->where('training_status_id','=',1)->where('status_id','=',1)->first();
			if($dataall){
				UsertrainingCatalog::where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->where('training_status_id','=',1)->where('status_id','=',1)->delete();
			}
			
			$userlearningplan = Usertrainingcatalogs::getTrainingCatalogbyUser($userid,$compid,$course_id);
			if(count($userlearningplan) == 0)
			{	
				$programsschedule= array();
				$lastId = Trainingcatalog::EnrolledForElearningLearningplan($course_id, $userid, $userprogramtypeid, $compid);
				$usertrainingid = $lastId;
			}
			else
			{
				$usertrainingid = $userlearningplan[0]['user_training_id'];
			}
        
		
		if($confirm==1 && $course_id!='')
		{
			 
			 //update and insert data into user_training_history table
			$this->updateInsertUserTrainingHistory($usertrainingid, $course_id, $userid,$launchtime);
			
			//get course credit;
			 $creditdata=Course::where('course_id','=',$course_id)->where('status_id','=',1)->where('company_id','=',$compid)->first();
			 $credit_value= 0;
			 if($creditdata){
				$credit_value= $creditdata->credit_value;
			 }
			
			 $duedate=date('Y-m-d');
			 //insert update data into User Assessment Prop table
			$responseArr= $this->AddUpdateUserassessmentProp($course_id,$compid,$userid,$duedate,$user_responce,$usertrainingid);
			 
			//update data into user_trascript table
			$this->updateDataIntoTrascript($credit_value,$course_id,$userid,$usertrainingid,$compid,$responseArr);
			
			//update and insert data into user_training_catalog table
			$this->updateInsertUserTrainingCatalog($course_id, $compid, $userid,$usertrainingid);

			CommonFunctions::getCourseSkillLevel($course_id, $compid,$userid,5);
		}
		
	}
	
	public function AddUpdateUserassessmentProp($assessid,$compid,$userid,$duedate,$user_responce,$usertrainingid)
	{
		$totalquestionsinassess = Assessments::fetchtotalquestionsofassesment($compid, $assessid);
		$attempteduserques =Assessments::countattemptedquestions($assessid, $userid, $compid);
		$countcorrectanswers = Assessments::countcorrectattemptedquestions($assessid, $userid, $compid);
		$countcorrectmutipleanswers = Assessments::countcorrectattemptedmultiplequestions($assessid, $userid, $compid);
		
		$fetchnooftimes = Assessments::getvalueofsubmiteddata($assessid, $userid, $compid,$usertrainingid);
		
		$userAttemptCount=isset($fetchnooftimes[0]->attempt_taken)? $fetchnooftimes[0]->attempt_taken: '0';
		$no_of_times=isset($fetchnooftimes[0]->no_of_times)? $fetchnooftimes[0]->no_of_times: '0';
		$totalCorrectAnswer = $countcorrectanswers[0]->correctanswers + $countcorrectmutipleanswers;
		//
		$confirmuser['course_id'] = $assessid;
		$confirmuser['company_id'] = $compid;
		$confirmuser['user_id'] = $userid;
		$confirmuser['total_questions'] = $totalquestionsinassess[0]->totalquestions;
		$confirmuser['attempted_questions'] = count($attempteduserques);
		$confirmuser['is_completed'] = 1;
		$confirmuser['correct_answers'] = $totalCorrectAnswer;
		$confirmuser['last_updated_date'] = date('Y-m-d -h-i-s');
		$confirmuser['status_id'] = 1;
		$confirmuser['user_training_id'] = $usertrainingid;
		if (count($fetchnooftimes) == 0)  {	
			$confirmuser['no_of_times'] = 1;
			$confirmuser['attempt_taken'] = 1;
			$userassessmentpropsave = UserassessmentProp::create($confirmuser);
			$userassessmentpropid=$userassessmentpropsave->user_assessment_prop_id;
			$no_of_time1=1;
		}else {	
			$no_of_time1=$no_of_times+1;
			$confirmuser['no_of_times']=$no_of_time1;
			$userAttemptCount1=$userAttemptCount+1;
			$confirmuser['attempt_taken'] = $userAttemptCount1;
			UserassessmentProp::whereuserId($userid)->wherecourseId($assessid)->wherecompanyId($compid)->where('user_training_id','=',$usertrainingid)->update($confirmuser);
			$userassessmentpropid=$fetchnooftimes[0]->user_assessment_prop_id;
		}
		
		$userAssessmasterupdt = array();
		$userAssessmasterupdt['user_assessment_prop_id'] = $userassessmentpropid ;	
		UserassessmentMaster::whereuserId($userid)->wherecourseId($assessid)->update($userAssessmasterupdt);
		
		
	    $userAssessResponseArr=UsersAssessmentResponse::where('course_id','=',$assessid)->where('user_id','=',$userid)->where('user_assessment_prop_id','=',$userassessmentpropid)->where('company_id','=',$compid)->where('status_id','=',1)->first();
		
		if(!$userAssessResponseArr){
				$userresponse['course_id'] = $assessid;
				$userresponse['user_assessment_prop_id'] = $userassessmentpropid;
				$userresponse['user_id'] = $userid;
				$userresponse['company_id'] = $compid;
				$userresponse['user_responce'] = $user_responce;
				$userresponse['status_id'] = 1;
				$responseSave = UsersAssessmentResponse::create($userresponse);
				$responseid=$responseSave->id; 
		}else{
		        $responseid=$userAssessResponseArr->id;
		}
		
		return array('responseid'=>$responseid,'userassessmentpropid'=>$userassessmentpropid);
	}
	
	public function updateInsertUserTrainingCatalog($course_id, $compid, $userid,$usertrainingid){
		  
		$data['training_status_id']	=	1;	
		UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->update($data);
		
	    $dataall=UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->first()->toArray();
		
		UserTrainingCatalogHistory::insert($dataall);
		
		UsertrainingCatalog::where('user_training_id','!=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->where('training_status_id','=',1)->delete();
	}
	
	public function updateInsertUserTrainingHistory($usertrainingid, $course_id, $userid,$launchtime)
	{
		$recordss=UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('user_training_id','=',$usertrainingid)->where('status_id','=',1)->first();
		
		if($recordss)
		{

			$datatrans['user_training_id']=$usertrainingid;
			$datatrans['course_id']=$course_id;
			$datatrans['user_id']=$userid;
			$datatrans['status_id']=0;
			UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->update($datatrans);
		}
				
			$data1['user_training_id']=$usertrainingid;
			$data1['course_id']=$course_id;
			$data1['user_id']=$userid;
			$data1['course_completion_percentage']=100;
			$data1['training_end_date_time']=date('Y-m-d H:i:s');
			$data1['training_start_date_time']=$launchtime;
			$data1['php_session_id']=Session::getId();
			UserTrainingHistory::insert($data1);
	}
	
	public function updateDataIntoTrascript($credit_value,$course_id,$userid,$usertrainingid,$compid,$responseArr)
	{
		
		$data['class_id']=0;
		$data['training_type_id']=1;
		$data['user_id']=$userid;
		$data['course_id']=$course_id;
		$data['user_training_id']=$usertrainingid;
		$data['company_id']=$compid;
		$data['credit_value']=$credit_value;
		$data['completion_date']=date('Y-m-d');
		$data['times']=1;
		$data['credit_from']=0;
		$data['is_approved']=1;
		$data['status_id']=1;
		$data['last_modified_by']=$userid;
		$data['expiration_date'] = '0000-00-00';
		
		$currentDate = date('Y-m-d');
		$transcriptsdata = UserTranscript::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->first();
		
		$utid = 0;
		if($transcriptsdata)
		{   
	        $transcriptsdata=$transcriptsdata->toArray();
			$utid 	= $transcriptsdata['transcript_id'];
			$times 	= $transcriptsdata['times'];
			$datat['completion_date']= date('Y-m-d');
			if(!$data['expiration_date'])
			{ 
			 $data['expiration_date'] = '0000-00-00'; 
		    }else{
			   $datat['expiration_date'] = $data['expiration_date'];
			}
			$datat['status_id']=1;
			$datat['times']=$times+1;
			$datat['user_training_id']=$usertrainingid;
			
			UserTranscript::where('course_id','=',$course_id)->where('user_id','=',$userid)->where('company_id','=',$compid)->update($datat);
			
			UserTranscriptHistory::insert($transcriptsdata);

		}
		else
		{
			if(!$data['expiration_date'])
			{
		     $data['expiration_date'] = '0000-00-00'; 
		    }
		    $utid = UserTranscript::insertGetId($data);
			
			
		}
		
		$this->InsertUpdateUserAssessmentTranscript($course_id, $userid, $compid,$utid,$responseArr);
		
	}
	
	public function InsertUpdateUserAssessmentTranscript($assessid, $userid, $compid, $transcriptid,$responseArr){
		
		        $userassessmentpropid=$responseArr['userassessmentpropid'];
		        
				$responseid=$responseArr['responseid'];
				
				$assessmentdetail= Assessments::getassessmentname($compid, $assessid);
				
				$pass_percentage=isset($assessmentdetail->pass_percentage)?$assessmentdetail->pass_percentage : 0;
				
				$allow_transcript=isset($assessmentdetail->allow_transcript)?$assessmentdetail->allow_transcript : 0;
				
				$fetchnooftimes = Assessments::getuserassessmentsummary($assessid, $userid, $compid, $userassessmentpropid);
				$totalquestion	    = 	$fetchnooftimes[0]->total_questions;
				$correctanswers		=	$fetchnooftimes[0]->correct_answers;
				if($totalquestion!=0) {
					$totalrightper	=	($correctanswers*100)/$totalquestion;
				} else {
					$totalrightper=null;
				}
				$totalrightpercent	= 	number_format($totalrightper,2);
				
				if($allow_transcript==1) {
					  if($totalrightpercent >= $pass_percentage) {
						
							 $assesstrans['user_assess_resp_id'] = $responseid;
							 $assesstrans['course_id'] = $assessid;
							 $assesstrans['transcript_id'] = $transcriptid;
							 $assesstrans['score'] =$totalrightpercent;
							 $assesstrans['status'] ='Pass';
							 $assesstrans['created_date'] =date('Y-m-d H:i:s');
							 AssessmentResponseTranscript::create($assesstrans);
							
					  }
				}else {
						
							 $assesstrans['user_assess_resp_id'] = $responseid;
							 $assesstrans['course_id'] = $assessid;
							 $assesstrans['transcript_id'] = $transcriptid;
							 $assesstrans['score'] =$totalrightpercent;
							 $assesstrans['created_date'] =date('Y-m-d H:i:s');
							 if($totalrightpercent >= $pass_percentage) {
								$assesstrans['status'] ='Pass';
							 } else {
								$assesstrans['status'] ='Fail';
							 }
							 AssessmentResponseTranscript::create($assesstrans);
				}
		
	}
	
	public function reviewassessmentAction(Request $request)
	{
		$userid = $request->input('user_key');
	    $token  = $request->input('token');
		$compid = Auth::user()->company_id;
		$userid = Auth::user()->user_id;
		$view_arr=json_decode(CommonHelper::getDecode($token));
		foreach($view_arr as $key=>$value) $$key=$value;
		
		$assessid=$course_id;
		
		if($assessid && $userid && $token) {
			 
				$user_responce = array();
				$quesids = explode(",", $_REQUEST['quesids']);
				if($quesids){
					if(isset($user_assess_resp_id) && isset($transcript_id) && ($user_assess_resp_id!=0) && ($transcript_id!=0)){
				   	$user_responce = Assessments::fetchTranscriptDataForReviewAllQues($compid, $assessid, $userid, $quesids,$user_assess_resp_id);
					// echo '<pre>'; print_r($user_responce); exit;
						//$user_responce = Assessments::fetchDataForReviewAllQues($compid, $assessid, $userid, $quesids);
						
					}else{
						$user_responce = Assessments::fetchDataForReviewAllQues($compid, $assessid, $userid, $quesids);
					}
					return response()->json(['status'=>201,'data'=>$user_responce]);
				} 
		}
	}
	
	public function successfullsubmitasseAction($user,$token){
		$userid = Auth::user()->user_id;
		$compid = Auth::user()->company_id;
		$Getdate_formatecomapny =  CommonHelper::getdate($compid);
		$assementdetails = Assessments::getUserAssessment($user,$token, $compid);
		 $from_transcript=2;
		// echo '<pre>';print_r($assementdetails);exit;
		 $assessid = $assementdetails['id'];
		 $userAssessProp=UserassessmentProp::whereuserId($userid)->whereassessId($assessid)->wherecompanyId($compid)->first();
			//echo '<pre>';print_r($userAssessProp);exit;
			$flag=($userAssessProp)?$userAssessProp->correct_answers:0;
			$assess_data = Assessments::getAssessmentDetailForPopup($compid, $assessid, 0);//fetch data from assessment
			//echo '<pre>';print_r($assess_data);exit;
				$count_response = count(Assessments::usersassessmentresponse($assessid, $userid, $compid));//fetch uar.id from users_assessment_response
				if(count($assess_data)>0){
					$assess_data[0]['attempt_taken'] = $assess_data[0]['attempt_taken'] - $count_response;
				$assess_data[0]['user_score'] = '0';
				$assess_data[0]['user_status'] = 'Fail';
				}
				
				if($flag > 0 && count($assess_data)>0)
				{
					
					$qs_data = Assessments::getAssessQuesPopupExceptDescType($assessid);//fetch data from assess_question
					$assess_data[0]['user_score'] = round(($flag*100)/$qs_data[0]['total_ques']);
					$assess_data[0]['user_score'] = round(($flag*100)/$qs_data[0]['total_ques']);
					if($assess_data[0]['user_score'] >= $assess_data[0]['pass_percentage'])
						$assess_data[0]['user_status'] = 'Pass';
				}
				$assess_data['pp']=isset($assess_data[0])?$assess_data[0]['pass_percentage']:100;
				$assess_data['usr']=isset($assess_data[0])?$assess_data[0]['user_score']:0;
				$assess_data['uss']=isset($assess_data[0])?$assess_data[0]['user_status']:'Fail';
				//echo '<pre>';print_r($assess_data);exit;
		return view('user.assessment.successfullsubmitasse',['user'=>$user,'token'=>$token,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'nooftimes'=>$assementdetails['no_of_times'],'attempttaken'=>$assementdetails['attempt_taken'],'userattemttaken'=>$assementdetails['user_attemt_taken'],'allowassessment'=>$assementdetails['allow_assessment'],'assementdetails'=>$assementdetails,'totalrightper'=>100,'from_transcript'=>$from_transcript,'assess_data'=>$assess_data]);
	}
	
	
	public function submitListAssessmentAction(Request $request) 
	{
		$userid = $request->input('user');
		$token = $request->input('token');
		$skipids = $request->input('skipids');
		$view_arr=json_decode(CommonHelper::getDecode($token));
	   
		foreach($view_arr as $key=>$value) $$key=$value;
		$skipids=explode(',',$skipids);
		
		$assessid = $course_id ;
		if($assessid && $userid) {
			$compid = Auth::user()->company_id;
			$duedate= date('Y-m-d');
			$responce = array();
			$multiselct = array();
			$storemulti=array();
			$count = 0;
			$correct_response='';
			$question_type='';
			$student_response='';
			$result='';
			$questionid='';
			$fetchassessments = Assessments::attemptedquestions($assessid, $userid, $compid);
			if(count($fetchassessments) > 0) {
				UserassessmentMaster::whereuserId($userid)->wherecourseId($assessid)->delete();
			}
			unset($_REQUEST['x']);
			unset($_REQUEST['y']);
			$stockList = array();  //Your list of "stocks" indexed by the number found at the end of "stock"
			$x = 0;
			
			foreach ($_REQUEST as $stockKey => $stock) 
			{
				if(sscanf($stockKey, "descriptive_%d", $stockId)) 
				{  
					$question_type='textans';
					if($stockId)
					{
						if($stock != '') 
						{
							$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
							$desc['question_id'] = $questionid;
							$desc['answer'] = '';
							$desc['course_id'] = $assessid;
							$desc['descriptive'] = $stock;
							$desc['user_id'] = $userid;
							$desc['last_updated_date'] = date('Y-m-d -h-i-s');
							$desc['created_date'] = date('Y-m-d -h-i-s');
							$student_response=$_REQUEST['descriptive'.'_'."$questionid"]; 
							$answer_id =Assessments::fetchassessmenanswerid($assessid,$questionid);
							$answerid1=$answer_id[0]->descriptive;
							$correct_response=$answerid1;
							$result='[TextBase Question]';
							if ($_REQUEST['descriptive_' . $questionid] != ''){
								UserassessmentMaster::create($desc);
							}
						}
					}
				}
				if(sscanf($stockKey, "trueval_%d", $stockId) != '') 
				{
					$question_type='truefalse';
					if($stockId) 
					{
						$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
						$editques['answer'] = $stock;
						$editques['descriptive'] = ' ';
						$editques['question_id'] = $questionid;
						$editques['course_id'] = $assessid;
						$editques['user_id'] = $userid;
						$editques['last_updated_date'] = date('Y-m-d -h-i-s');
						$editques['created_date'] = date('Y-m-d -h-i-s');
						$student_response=$_REQUEST['trueval'.'_'."$questionid"];
						$answer_id =Assessments::fetchassessmenanswerid($assessid,$questionid);
						$answerid1=$answer_id[0]->answer;
						$correct_response=$answerid1;
						if(trim($student_response)==trim($correct_response)) {
							$result='Correct';
						} else {
							$result='Incorrect';
						}
						UserassessmentMaster::create($editques);
					}
				}
				if(sscanf($stockKey, "yesradio1_%d", $stockId) != '') 
				{
					$question_type='yesno';
					if($stockId) 
					{ 
						$descriptive = $stockKey;
						$questionid = substr($stockKey, strpos($stockKey, "_") + 1);
						$yesradio1_['question_id'] = $questionid;
						$yesradio1_['course_id'] = $assessid;
						$yesradio1_['answer'] = $stock;
						$yesradio1_['descriptive'] = ' ';
						$yesradio1_['user_id'] = $userid;
						$yesradio1_['last_updated_date'] = date('Y-m-d -h-i-s');
						$yesradio1_['created_date'] = date('Y-m-d -h-i-s');
						$student_response=$_REQUEST['yesradio1'.'_'."$questionid"]; 
						$answer_id =Assessments::fetchassessmenanswerid($assessid,$questionid);
						$answerid1=$answer_id[0]->answer;
						$correct_response=$answerid1;
						if(trim($student_response)==trim($correct_response)) {
							$result='Correct';
						} else {
							$result='Incorrect';
						}
						UserassessmentMaster::create($yesradio1_);
						
					}
				}
				if(sscanf($stockKey, "multichoicecorrectineditradio_%d", $textvalue) != '') 
				{
					$question_type='multichoice';
					if($textvalue) 
					{
						$a1 = explode('||', $stock); 
						$questionid = $a1[0];
						$answerid = $a1[1];
						$multi['question_id'] = $questionid;
						$multi['course_id'] = $assessid;
						$multi['answer'] = $answerid;
						$multi['descriptive'] = ' ';
						$multi['user_id'] = $userid;
						$multi['last_updated_date'] = date('Y-m-d -h-i-s');
						$multi['created_date'] = date('Y-m-d -h-i-s');
						UserassessmentMaster::create($multi);
						
						$student_response=$_REQUEST['multichoicecorrectinedittext'.'_'."$questionid".'_'."$answerid"]; 
						$answer_id =Assessments::fetchassessmenanswerid($assessid,$questionid);
						$answerid1=$answer_id[0]->answer;
						$correct_response = $_REQUEST['multichoicecorrectinedittext'.'_'."$questionid".'_'."$answerid1"];
						if($correct_response == $student_response) {
							$result='Correct';
						} else {
							$result='Incorrect';
						}
					}
				}
				
				if(sscanf($stockKey, "mseloptinedit_%d", $textvalue)) 
				{    
				   $question_type='multiselect';
					if ($textvalue) 
					{
						$a1 = explode('||', $stock);
						$questionid = $a1[0];
						$answerid = $a1[1];
						$multi['question_id'] = $questionid;
						$multi['course_id'] = $assessid;
						$multi['answer'] = $answerid;
						$multi['descriptive'] = ' ';
						$multi['user_id'] = $userid;
						$multi['last_updated_date'] = date('Y-m-d -h-i-s');
						$multi['created_date'] = date('Y-m-d -h-i-s');
						UserassessmentMaster::create($multi);
					}
				}
				
				if($questionid!='')
				{
					if($question_type!='multiselect')
					{   
						array_push($responce, array("qid"=>$questionid,"question_type"=>$question_type,"student_response"=>$student_response,"correct_response"=> $correct_response,"result"=>$result,"weighting"=>0,));
						$questionid='';
						$question_type='';
						$student_response='';
						$correct_response='';
						$result='';
					}
				}
			}
			
			$responce1 = array_unique($responce, SORT_REGULAR);	
			if(isset($_REQUEST['multiSelJsonString'])) 
			{
				$question_type1='multiselect';
				$multiSelAnsArr = json_decode($_REQUEST['multiSelJsonString'],true);
				
				$quesIdArr = array();
				$userResponseArr = array();
				foreach($multiSelAnsArr as $multiSelAns)
				{
					$multiSelArr = explode('||',$multiSelAns);
					$quesId  = $multiSelArr[0];
					$answ_id = $multiSelArr[1];
					$descript =Assessments::fetchmultiselectansbyuser($answ_id,$quesId);	
					$user_ans= $descript[0]->description;
					if(in_array($quesId,$quesIdArr)) {
						$userResponseArr[$quesId.'ans']  = trim($userResponseArr[$quesId.'ans']).'#'.$user_ans;
					} else {
						$userResponseArr[$quesId.'ans']  = $quesId.'||'.$user_ans;
					}
					array_push($quesIdArr,$quesId);
				}
				foreach($userResponseArr as $responseArr)
				{
					$res=explode('||',$responseArr);
					$student_response1=$res[1];
					$questionid1=$res[0];
					$answer_id =Assessments::fetchmultiselectans($questionid1);
					$res1=array();
					$main='';
					foreach($answer_id as $ans)
					{
						if($ans->option_value!='')
						{
							$res1['description']=$ans->description;
							$main.=$ans->description.'#';
						}
					}
					$correct_response1 =rtrim($main,'#');
					$corrres=explode('#',$correct_response1);
					$a=array();
					foreach($corrres as $cr){
					 $a[]=$cr;
					}
					
					$b=array();
					$corrres1=explode('#',$student_response1);
					foreach($corrres1 as $cr1){
					$b[]= $cr1;
					}
					
					$i=0;
					$markwrong=0;
					foreach($b as $c){
					if(in_array($c,$a)){
					  $i++;
					}else{
					 $markwrong=1;
					}
					}
					if($markwrong==0){
					if(count($a)==$i){
					$result1='Correct';
					}
					}else{
					$result1='Incorrect';
					}
					
					array_push($multiselct, array("qid"=>$questionid1,"question_type"=>$question_type1,"student_response"=>$student_response1,"correct_response"=> $correct_response1,"result"=>$result1,"weighting"=>0,));
				}
			}
			$quesAndAnsDes=array_merge($multiselct,$responce1);
			$user_responce = json_encode($quesAndAnsDes);
			
          if($_REQUEST['confirmedradio'] == 'yes') {
			        $this->updateAssessmentcompletion($view_arr,$user_responce); 
			}
			
			return redirect('mylearning/viewlistquizandsurvey/viewId/'.$token);
		}else{
			exit;
		}
	}
	
	public function viewListResultAssessment($viewId)
	{
		$view_arr=json_decode(CommonHelper::getDecode($viewId));
		foreach($view_arr as $key=>$value) $$key=$value;
		
		$userid = Auth::user()->user_id;
		$compid = Auth::user()->company_id;
		
		$Getdate_formatecomapny =  CommonHelper::getdate($compid);
		$fetchvalues =CommonFunctions::getaccessurl();
		$useranswer="";
		if($fetchvalues) 
		{
			if ($fetchvalues['company_id'] != 0) 
			{
				$compid = $fetchvalues['company_id'];
				$realcompid = $compid;
			}
			if ($fetchvalues['company_id'] == 0) 
			{
				return response()->json(['status'=>408,'message'=>$fetchvalues['msg']]); exit;
			}
		}
		$assessid=$course_id;
		if(isset($user_assess_resp_id) && isset($transcript_id) && ($user_assess_resp_id!=0) && ($transcript_id!=0)){
			$assementdetails = Assessments::fetchUserAssessment($userid, $training_program_code, $compid,$user_assess_resp_id);	
		}else{
			$userAssessProp=UserassessmentProp::whereuserId($userid)->wherecourseId($assessid)->wherecompanyId($compid)->orderby('user_assessment_prop_id','desc')->first();
			$prop_id=($userAssessProp)?$userAssessProp->user_assessment_prop_id:0;
			
			$assementdetails = Assessments::getAssessmentQuizdetails($userid,$training_program_code, $compid,$prop_id);
			
		}
	
		$fetchuploadedfiles=array();
		return view('user.assessment.viewlistassessment',['user'=>$userid,'token'=>$viewId,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'nooftimes'=>$assementdetails['no_of_times'],'attempttaken'=>$assementdetails['attempt_taken'],'userattemttaken'=>$assementdetails['user_attemt_taken'],'allowassessment'=>$assementdetails['allow_assessment'],'assementdetails'=>$assementdetails,'totalrightper'=>100,'fetchuploadedfiles'=>$fetchuploadedfiles,'mode'=>'','istimer'=>'','totalsecond'=>'']);
		
	}
	
	
	public function viewListAssessmentRetake($viewId)
	{
		$view_arr=json_decode(CommonHelper::getDecode($viewId));
		foreach($view_arr as $key=>$value) $$key=$value;
		
		$userid = Auth::user()->user_id;
		$compid = Auth::user()->company_id;
		
		$Getdate_formatecomapny =  CommonHelper::getdate($compid);
		$fetchvalues =CommonFunctions::getaccessurl();
		$useranswer="";
		if($fetchvalues) 
		{
			if ($fetchvalues['company_id'] != 0) 
			{
				$compid = $fetchvalues['company_id'];
				$realcompid = $compid;
			}
			if ($fetchvalues['company_id'] == 0) 
			{
				return response()->json(['status'=>408,'message'=>$fetchvalues['msg']]); exit;
			}
		}
		$assessid=$course_id;
		$totalsecond='';
		$istimer='';
		if(isset($user_assess_resp_id) && isset($transcript_id) && ($user_assess_resp_id!=0) && ($transcript_id!=0)){
			$assementdetails = Assessments::fetchUserAssessment($userid, $training_program_code, $compid,$user_assess_resp_id);	
			if($assementdetails['time_limit']!='00:00:00'){
				$time=explode(':',$assementdetails['time_limit']);
				$h=$time[0]*3600;
				$m=$time[1]*60;
				$s=$time[2];
				$totalsecond=$h+$m+$s;
				$istimer=1;
			}
		}else{
			$userAssessProp=UserassessmentProp::whereuserId($userid)->wherecourseId($assessid)->wherecompanyId($compid)->orderby('user_assessment_prop_id','desc')->first();
			$prop_id=($userAssessProp)?$userAssessProp->user_assessment_prop_id:0;
			
			$assementdetails = Assessments::getAssessmentQuizdetails($userid,$training_program_code, $compid,$prop_id);
			if($assementdetails['time_limit']!='00:00:00'){
				$time=explode(':',$assementdetails['time_limit']);
				$h=$time[0]*3600;
				$m=$time[1]*60;
				$s=$time[2];
				$totalsecond=$h+$m+$s;
				$istimer=1;
			}
		}
		
		$fetchuploadedfiles=array();
		return view('user.assessment.viewlistretakeassessment',['user'=>$userid,'token'=>$viewId,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'nooftimes'=>$assementdetails['no_of_times'],'attempttaken'=>$assementdetails['attempt_taken'],'userattemttaken'=>$assementdetails['user_attemt_taken'],'allowassessment'=>$assementdetails['allow_assessment'],'assementdetails'=>$assementdetails,'totalrightper'=>100,'fetchuploadedfiles'=>$fetchuploadedfiles,'mode'=>'','istimer'=>$istimer,'totalsecond'=>$totalsecond]);
		
	}
	
}
