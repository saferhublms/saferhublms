<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth,URL;
use CommonFunctions,CommonHelper,Trainingcatelogallplace,Assignmentfunctions,Mailnotification;
use App\Http\Traits\Admin;
use App\Http\Traits\Supervisior;
use App\Http\Traits\Learningplan;
use App\Http\Traits\Trainingprogram;
use App\Http\Traits\Assignment;
use App\Http\Traits\Coursecredits;
use App\Models\ClassSchedule;
use App\Models\UsertrainingCatalog;
use App\Models\UserTranscript;
use App\Models\SupervisorSetting;
use App\Http\Traits\Datamanagement;
use DateTime,Enrolleduserfromallplace;
use TrainingItemsFunctions;

class SupervisiorController extends Controller
{
    /**
     * supervisorReports() - Method to get supervisor reports and used on the Supervisor Tools page.
     * url: api/supervisor-reports
	 * Created By: Akshay Kumar Tyagi
	 * Created Date: 10 January, 2018
     */
	 
	 public function supervisorReportsAction(Request $request){
           $user = Auth::user();
           $compid = $user->company_id;
		   $user_id = $user->user_id;
		   $roleid = $user->role_id;
		   $baseUrl = URL::to('/');
           $sub_org_id = CommonFunctions::fetchUserOranizationIds($user_id);
		   $level = 1;
		   $flag = 0;
		   $supervisor_data = $dataArray = $unique_supervisor_data = array();
		   $CssData = CommonHelper::getcompanycss($compid);
		   $buttonclass = 'new_css_button';
		   if(count($CssData)){
			if(!empty($CssData[0]->button_color))
				$buttonclass = $CssData[0]->button_color;
		   }	
          $subadminPermissionData = Admin::getsubadminpermissiondata($compid);
          if(count($subadminPermissionData)> 0) {
				$subadminbasedon = $subadminPermissionData[0]['user_based_on'];
				$instructorbasedon      = $subadminPermissionData[0]['user_based_on_instructor'];
		  }else{
				$subadminbasedon = 0;
				$instructorbasedon = 0;
		  }
          $unique_supervisor_data[] = $user_id;		  
		  $supervisor_data = $this->getreports($user_id, $level, 1, $user_id, $roleid ,$compid,$supervisor_data, $unique_supervisor_data,$sub_org_id, $subadminbasedon, $instructorbasedon);
          if(count($supervisor_data)){
			  foreach($supervisor_data AS $supervisor_key=>$supervisor_row){
				  $userid = $supervisor_row['user_id'];
				  $user_name = $supervisor_row['user_name'];
				  $totalpercentages = $complete_status = 0;
				  $Percentage = Learningplan::getTotalPercentageForadmin($compid, $userid);
				  $percentagevalue = 0;
				  if(count($Percentage)>0){
					  foreach($Percentage AS $Percentagedata){
						  $totalItems=$Percentagedata->total_items;
						  $completedItems=$Percentagedata->item_completed;
						  if($completedItems > 0 && $totalItems >0){
								$totalpercentage=floor(($completedItems/$totalItems)*100);
								$percentagevalue = $percentagevalue + $totalpercentage;
						 }
					  }
					  $totalpercentages = floor($percentagevalue/count($Percentage));
				  }
				  if($totalpercentages>=100) $complete_status = 1;
				  $jobtitle =	$supervisor_row['jobTitles'];
				  $returnstringval='<div class="meter-wrap"> <div class="meter-value" style="background-image: url('.$baseUrl.'/public/images/prog_bg.png); background-repeat:repeat-x; height:17px; width:'.$totalpercentages.'%" > <div class="meter-text " style="font-size: 10px;">'.$totalpercentages.' %</div>
					</div> </div>';
				  $view =	'<div class="hand '.$buttonclass.'" ng-click="grid.appScope.openviewpopup(\'unassignedtraining\',\''.CommonHelper::getEncode($userid).'\')">Assign<span>&#187;</span></div>';	
				  $properties = '<input type="hidden" name="edituserid" id="edituserid" value="'.CommonHelper::getEncode($userid).'"/><div class="hand '.$buttonclass.' lblue1" ng-click="grid.appScope.openviewpopup(\'viewassignment\',\''.CommonHelper::getEncode($userid).'\')" >Learning Plan <span>&#187;</span></div>&nbsp;<div class=" hand '.$buttonclass.' lblue2" style="margin-left:0;" ng-click="grid.appScope.openviewpopup(\'viewprofile\',\''.CommonHelper::getEncode($userid).'\')">Profile<span>&#187;</span></div>';
				  
				  $standrd = array('83','84'); 
					
					if(($flag == 1) || (in_array($compid, $standrd)))
					{					
						$dataArray[] = array("id"=>CommonHelper::getEncode($userid),"data"=>array(0,'<input name="complete_status[]" type="hidden" value="'.$complete_status.'"/>'.$user_name, @$jobtitle, $returnstringval, $properties,'normalId'=>$userid));
					}
					else					
					{
						$dataArray[] = array("id"=>CommonHelper::getEncode($userid),'name'=>'<input name="complete_status[]" type="hidden" value="'.$complete_status.'"/>'.$user_name,'title'=>@$jobtitle,'learning_plan'=>$returnstringval,'traning'=>$view,'properties'=>$properties,'normalId'=>$userid);
					}
			  }
			  
			  
		  }
                      return response()->json($dataArray);		  
	 }  
	 
	 /**
     * getreports() - Method to get supervisor rows in recursive
     * 
	 * Created By: Akshay Kumar Tyagi
	 * Created Date: 10 January, 2018
     */
	 public function getreports($user_id, $level, $cnt_level, $parent_id, $roleid, $compid,$supervisor_data, $unique_supervisor_data,$suborgId,$subadminbasedon=0,$instructorbasedon=0){
		 if($user_id==$parent_id){
			$cnt_level=1;
		 }
		 $supervisorids = array();
		 $supervisorids[] = $user_id;
		 $user_res = Supervisior::getSupervisorRows($level, $user_id, $roleid, $compid, $suborgId, $subadminbasedon, $instructorbasedon);
         if(count($user_res)){
			 foreach($user_res as $user_row){
				$role_id = $user_row['role_id'];
				 if($role_id == 4){
					$supervisor_data[] = $user_row;
				 }else{
					$supervisor_data[] = $user_row;
					$sub_user_id = $user_row['user_id'];
					if($user_id!=$sub_user_id){
						if(!in_array($sub_user_id, $unique_supervisor_data)){
							$unique_supervisor_data[] = $sub_user_id;
						}
					}
				 }
			 }
		 }
         if($level > 1){
			 for($i=1; $i<=$level; $i++){
				 foreach($unique_supervisor_data as $uniquesupervisorid){
					 if(!in_array($uniquesupervisorid, $supervisorids)){
						 $supervisorids[] = $uniquesupervisorid;
						 $sub_user_id 		= $uniquesupervisorid;
						 $dataarray = $this->getSupervisorlevel($sub_user_id, $level, $cnt_level, $parent_id, $roleid, $compid,$supervisor_data, $unique_supervisor_data);	
						 $supervisor_data = $dataarray[0];
						 $unique_supervisor_data = $dataarray[1];
					 }
				 }
			 }
		 }		 
		 return $supervisor_data;
	 }
	 
	 
	 
	 public function getSupervisorlevel($user_id, $level, $cnt_level, $parent_id, $roleid, $compid,$supervisor_data, $unique_supervisor_data){
		 $user_res = Supervisior::getSupervisorRows($level, $user_id, $roleid, $compid);
		 if($user_res){
			 foreach($user_res as $user_row){
				 $role_id = $user_row['role_id'];
				 if($role_id == 4){
					$supervisor_data[] = $user_row;
				 }else{
					$supervisor_data[] = $user_row;
					$sub_user_id = $user_row['user_id'];
					if($user_id!=$sub_user_id){
						if(!in_array($sub_user_id, $unique_supervisor_data)){
							$unique_supervisor_data[] = $sub_user_id;
						}
					}
				}
			 }
		 }
		 return array($supervisor_data,$unique_supervisor_data);
	 }
	 
	 
	 
	 public function trainingcatolgdataarrAction(Request $request){
		   $user = Auth::user();
           $compid = $user->company_id;
		   $user_id = $user->user_id;
		   $roleid = $user->role_id;
		   $baseUrl = URL::to('/');
		   $edituseriddata = explode('/',CommonHelper::getDecode($request->input('edituserid')));
		   $edituserid = (!empty($edituseriddata)) ? $edituseriddata[0] : '';
		   $categoryid = count($edituseriddata) == 3 ? $edituseriddata[2] : '';
		   $getElearniingarray=Trainingprogram::getElearningTrainingPrograms($compid,$user_id,$roleid,'','assignment');
	   
		   $getclassarray=Trainingprogram::getClassroomTrainingPrograms($compid,$user_id,$roleid,'','','assignment');
		   
		   $getblendedarray=Trainingprogram::getblendedTrainingPrograms($compid,$user_id,$roleid,'','assignment');
		   	
		   $programs0=array_merge($getElearniingarray,$getclassarray);
			$programs1=array_merge($programs0,$getblendedarray);
		  	$programsresult=Trainingcatelogallplace::supervisor_return_html($programs1,$roleid,$compid,$request->input('edituserid'));
			$final=array();
			foreach($programsresult as $k=>$v){
					  $id=$v['id'];
					  $final[$id]   =$v['data'];	
				}
				
			$Arr = $this->array_msort($final, array(1=>SORT_ASC));
			     $dataArray=array();
				
				foreach($Arr as $k=>$v){
					$dataArray[] = array("id"=>$k,"checkbox"=>$v[0], "item_name"=>$v[1], "course_id"=>$v[2], "start_date"=>$v[3], "credits"=>$v[4],"item_type"=>$v[5]);
				}

              $trainingcategory =CommonFunctions::getTraningCategoryforsupervisortool($compid,$user_id,$roleid);			
				 $finalarray = array("data"=>$dataArray,"category"=>$trainingcategory);
				return response()->json($finalarray);
		  
	 }


     public function array_msort($array, $cols){
			$colarr = array();
			foreach ($cols as $col => $order) {
				$colarr[$col] = array();
				foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
			}
			$eval = 'array_multisort(';
			foreach ($cols as $col => $order) {
				$eval .= '$colarr[\''.$col.'\'],'.$order.',';
			}
			$eval = substr($eval,0,-1).');';
			eval($eval);
			$ret = array();
			foreach ($colarr as $col => $arr) {
				foreach ($arr as $k => $v) {
					$k = substr($k,1);
					if (!isset($ret[$k])) $ret[$k] = $array[$k];
					$ret[$k][$col] = $array[$k][$col];
				}
			}
			return $ret;

    }


    public function categoryfilterAction(Request $request){
		   $user = Auth::user();
           $compid = $user->company_id;
		   $userid = $user->user_id;
		   $roleid = $user->role_id;
		   $baseUrl = URL::to('/');
		   $edituserid = $request->input('edituserid');
		   $edituserid=explode(',',$edituserid);
		   $categoryID=CommonHelper::getDecode($request->input('categoryType'));
		   $traingTypeId=CommonHelper::getDecode($request->input('itemType'));
		   $catid=$traingTypeId;
		   $cattype = $categoryID;
		   $getclassarray = $getElearniingarray = $getblendedarray = $getvirtualclassarray = array();
		   if($traingTypeId == '1'){
				$getElearniingarray = Trainingprogram::getElearningTrainingPrograms($compid,$userid,$roleid,$categoryID,'assignment');
			}else if($traingTypeId == '2' ){
				$getclassarray = Trainingprogram::getClassroomTrainingPrograms($compid,$userid,$roleid,$categoryID, $traingTypeId ,'assignment');
			}else if($traingTypeId == '3'){
				$getvirtualclassarray = Trainingprogram::getClassroomTrainingPrograms($compid,$userid,$roleid,$categoryID, $traingTypeId,'assignment');
			}else if($traingTypeId == '4'){
				$getblendedarray = Trainingprogram::getblendedTrainingPrograms($compid,$userid,$roleid,$categoryID,'assignment');
			}else{
				$getElearniingarray=Trainingprogram::getElearningTrainingPrograms($compid,$userid,$roleid,$categoryID,'assignment');
				// get CLassroom 
				$getclassarray=$getvirtualclassarray = Trainingprogram::getClassroomTrainingPrograms($compid,$userid,$roleid,$categoryID,'','assignment');
				// get Blended program 
				$getblendedarray=Trainingprogram::getblendedTrainingPrograms($compid,$userid,$roleid,$categoryID,'assignment');
			}
			$programs0=array_merge($getElearniingarray,$getclassarray);
			$programs1=array_merge($getvirtualclassarray,$getblendedarray);
			$programs2=array_merge($programs0,$programs1);
			$programsresult=array();
			$programsresult=Trainingcatelogallplace::supervisor_return_html($programs2,$roleid,$compid,$edituserid[0]);
			 $final=array();
			foreach($programsresult as $k=>$v){
					  $id=$v['id'];
					  $final[$id]   =$v['data'];	
				}
				
			$Arr = $this->array_msort($final, array(1=>SORT_ASC));
			     $dataArray=array();
				
				foreach($Arr as $k=>$v){
					$dataArray[] = array("checkbox"=>$v[0], "item_name"=>$v[1], "course_id"=>$v[2], "start_date"=>$v[3], "credits"=>$v[4],"item_type"=>$v[5]);
				}		
				$finalarray = array("data"=>$dataArray);
				return response()->json($finalarray);
	}
	
	
	public function assigntrainingtousersAction(Request $request){
		   $user = Auth::user();
           $compid = $user->company_id;
		   $userid = $user->user_id;
		   $roleid = $user->role_id;
		   $baseUrl = URL::to('/');
		   $mode = $request->input('mode');
		   $Getdate_formatecomapny =  CommonHelper::getdate($compid);
		   $assignmentarray = array();
		   if($mode == 'assignselectedcourses'){
			   $users=CommonHelper::getDecode($request->input('selectedusers'));
			   $allcheckeduserswithcourses = explode(',', $users);
			   $training_program = $request->input('training_program');
			   $alltraining_program = explode(',',$training_program);
			  
			   $duedate = $request->input('duedate');
			 
			  if($duedate!=''){
				  $duedate  = date('d-m-Y', strtotime($duedate));
				  $date = DateTime::createFromFormat($Getdate_formatecomapny, $duedate);
			     $duedate 	= 		$date->format('Y-m-d');
				   
			    }
				
			$assign_startdate 	= $request->input('assign_startdate');
			if($assign_startdate!=''){
			$date = DateTime::createFromFormat($Getdate_formatecomapny, $assign_startdate);
			$assign_startdate 	= 		$date->format('Y-m-d');
			 }
			 $created_by = $userid;
			 if(count($alltraining_program)){
				 foreach($alltraining_program as $training_program_id){
				     $training_program_id=CommonHelper::getDecode($training_program_id);
					 $assignment_id 	= Assignmentfunctions::addNewAssignment($training_program_id,$duedate,$compid,$created_by,$assign_startdate);
					 $userassignids 	= Assignment::usersAssignmentWithTrainingId($training_program_id, $compid);
					 foreach ($allcheckeduserswithcourses as $user_id){
						 if(!in_array($user_id, $userassignids)){
							 $allAssignmentArr 	= Assignmentfunctions::assignUser($user_id, $training_program_id, $assignment_id, $compid);
							 $assignmentarray[]	= $allAssignmentArr;
						 }else{
							$allAssignmentArr 	= Assignmentfunctions::updateUser($user_id, $training_program_id, $assignment_id, $compid);	
						}
					 }
				 }
			 }
		   }
		   Mailnotification::AssignmentMailsendtoAllUser($assignmentarray,$roleid);
		     $response = array('status'=>200,'mesaage'=>'success');
			 return response()->json($response);
	}
	
	
	public function supervisorlearningplanAction(Request $request){
		   $user = Auth::user();
		   $baseUrl = URL::to('/');
           $compid = $user->company_id;
		   $userid = CommonHelper::getDecode($request->input('edituserid'));
		   $learnPlanArr1=Learningplan::learnPlanArr($compid,$userid);
		   $learningplans = $this->array_msort($learnPlanArr1, array('due_date'=>SORT_ASC));
		   $learningplans = array_unique($learningplans, SORT_REGULAR);
		   $Getdate_formatecomapny = CommonHelper::getdate($compid);
		   $dataArray=array();
		   $unique_items = array();
		   if(count($learningplans)>0){
			   foreach($learningplans as $learnPlandata){
				   $credit_value='';
				   $learning_plan_name='';
				   $end_date =($learnPlandata['end_date']!='0000-00-00') ? $learnPlandata['end_date'] : '0000-00-00'; 
				   if($end_date>=date('Y-m-d') || $end_date == '0000-00-00'){
					   if(!in_array($learnPlandata['item_id'], $unique_items)){
						   $unique_items[] = $learnPlandata['item_id'];
						   $learning_plan_name= $learnPlandata['learning_plan_name'];
						   $credit_value= $learnPlandata['credit_value'];
						   $item_name= stripcslashes($learnPlandata['item_name']);
						   $training_type= $learnPlandata['training_type'];
						   $title ='';
						   if($learnPlandata['imagname']=='flag_red.png'){
							    $title ='Item Incomplete';
						   }elseif($learnPlandata['imagname']=='u184.png'){
							    $title ='Item Complete';
						   }elseif($learnPlandata['imagname']=='flag_yellow.png'){
							    $title ='Item Expired';
						   }elseif($learnPlandata['imagname']=='fail_icon2.png'){
							    $title ='Item Failed';
						   }
						   
							$imagname='<img title="'.$title.'" style="width:17px;height:22px;" src='.$baseUrl.'/user/assets/images/'.$learnPlandata['imagname'].'>';
							$due_date = ($learnPlandata['due_date'] != '' && $learnPlandata['due_date']!='0000-00-00'&& $learnPlandata['due_date']!='1969-12-31' ) ? (($learnPlandata['due_date']) == 'Never'? 'Never': date($Getdate_formatecomapny,strtotime($learnPlandata['due_date']))) : '-';
							$expiration_date = ($learnPlandata['exp_date'] != '' && $learnPlandata['exp_date']!='0000-00-00' && $learnPlandata['exp_date']!='1969-12-31') ? (($learnPlandata['exp_date']) == 'Never'? 'Never': date($Getdate_formatecomapny,strtotime($learnPlandata['exp_date']))): '-';
							if($expiration_date == '12/31/1969' || $expiration_date == '12-31-1969'){
							$expiration_date = ' -';
							}
							
							$percent = '<div class="meter-wrap"><div class="meter-value" style="background-image: url('.$baseUrl.'/user/assets/images/prog_bg.png);background-repeat:repeat-x; width: '. ((isset($learnPlandata['percent']) && $learnPlandata['percent'] > 100) ? '100' : intval($learnPlandata['percent'])).'%;"><div class="meter-text " style="font-size: 10px;">'. ((isset($learnPlandata['percent']) && $learnPlandata['percent'] > 100) ? '100' : $learnPlandata['percent']).'%</div></div></div>';
						
						    $idrow = $learnPlandata['learning_plan_id'].'-'.$learnPlandata['item_id'].'-'.$learnPlandata ['blended_program_id'];
							$dataArray[] = array("id"=>$idrow, "learning_plan"=>$learning_plan_name, "learning_plan_items"=>$item_name, "traning_type"=>$training_type, "credits"=>$credit_value,"flag"=>$imagname,"expiration"=>$expiration_date,"due_date"=>$due_date,"progress"=>$percent);
					   }
				   }
			   }
		   }
		        
		       return response()->json($dataArray);
	}
	
	public function getSupervisorSetting()
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$fetchsupersetting = Supervisior::supervisorsetttings($compid, $userid);
		if ($roleid != 1) 
		{
			$fetchusersofelearning = Supervisior::classroomSupervisorPendingApprovals($compid, $userid);
			$fetchpendingapprovalsbysupervisor = Supervisior::submittedTrainingForSupervisorApprovals($compid, $userid);
			$total_pending_approval = count($fetchusersofelearning)+count($fetchpendingapprovalsbysupervisor);
		}
		else if ($roleid == 1)
		{
			$fetchusersofelearning = Supervisior::countclassroomSupervisorPendingApprovals($compid, -1);
			$fetchpendingapprovalsbysupervisor =Supervisior::SubmitTrainingSupervisorApprovals($compid, -1);
			$total_pending_approval = count($fetchusersofelearning)+count($fetchpendingapprovalsbysupervisor);
		}
		$notify = false;
		$autoapprove = false;
		if (count($fetchsupersetting) > 0) 
		{
			$notify = ($fetchsupersetting[0]->notify_update==1)?true:false;
			$autoapprove = ($fetchsupersetting[0]->auto_approve==1)?true:false;
		}
		return response()->json(['notify'=>$notify,'autoapprove'=>$autoapprove,'status'=>201,'total_pending_approval'=>$total_pending_approval]);
	}
	
	public function supervisortoolsAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$mode = $request->input('mode');
		$notify = $request->input('notify');
		$courseapp = $request->input('courseapp');
		$fetchsupersetting = Supervisior::supervisorsetttings($compid, $userid);
		$exists = 0;
		if ($mode != '') {
			if (count($fetchsupersetting) > 0)  {
				$exists = 1;
				$id = $fetchsupersetting[0]->supervisor_id;
				if ($mode == 'tonotifyupdate') {
					$data['notify_update'] = $notify;
					echo $mode . "$notify";
				}
				if ($mode == 'toapprovecourse') {
					$data['auto_approve'] = $courseapp;
					if ($roleid != 1) {
						$fetchusersofelearning = Supervisior::getunaapproveduserstosupervisorforelearning($compid, $userid, '1');
						$collectcoursesidsarr = $fetchusersofelearning;
						$collectcourses = array();
						if (count(@$collectcoursesidsarr) > 0) 
						{
							foreach ($collectcoursesidsarr as $key => $val) 
							{
								if ($val != Array()) 
								{
									$collectcourses[$key] = $val;
								}
							}
						}
						
						if (count($collectcourses) > 0) 
						{
							foreach ($collectcourses as $values) 
							{
								foreach ($values as $itemvals) 
								{
									$scheduleId = $itemvals->session_id;
									$edituserid = $itemvals->user_id;
									$wheretoupdateinutc = 'session_id=' . $scheduleId . ' and user_id=' . $edituserid;
									$classScheduleData = Supervisior::getClassScheduleData($scheduleId, $compid);
									if (count($classScheduleData) > 0) 
									{
										$noenrollseats = $classScheduleData[0]->no_of_enrolled_seat;
										$maxseats = $classScheduleData[0]->max_seat;
										if ($noenrollseats < $maxseats) 
										{
											$numOfenroll = $classScheduleData[0]->no_of_enrolled_seat + 1;
											$updateEnrolled['no_of_enrolled_seat'] = $numOfenroll;
											$whereSchedule = array();
											$whereSchedule []= "class_id= $scheduleId";
											$whereSchedule[] = "company_id = $compid";
											ClassSchedule::whereclassId($scheduleId)->wherecompanyId($compid)->update($updateEnrolled);
										}
											$utcupdata['is_approved'] = 1;
											$utcupdata['is_enroll'] = 1;
											$utcupdata['training_status_id'] = 3;
											$utcupdata['last_modfied_by'] = $userid;
											UsertrainingCatalog::wheresessionId($scheduleId)->whereuserId($edituserid)->update($utcupdata);
											$utransdata['is_active'] = 1;

											UserTranscript::whereclassId($scheduleId)->wherecompanyId($compid)->update($utransdata);
									}
								}
							}
						}
						$fetchelearningplusclassinsubmittraining = Supervisior::fetchsubmittraininginsupervisorfor($compid, $userid);
						if (count($fetchelearningplusclassinsubmittraining) > 0) 
						{
							foreach ($fetchelearningplusclassinsubmittraining as $submitvalues) 
							{
								foreach ($submitvalues as $subvals) 
								{
									$wheretoupdateinutc = 'user_training_id=' . $subvals->user_training_id;
									$utcupdata['is_approved'] = 1;
									$utcupdata['is_enroll'] = 1;
									$utcupdata['training_status_id'] = 1;
									$utcupdata['training_date'] = date('Y-m-d');
									UsertrainingCatalog::whereuserTrainingId($subvals->user_training_id)->update($utcupdata);
									$utransdata['is_active'] = 1;
									UserTranscript::whereuserTrainingId($subvals->user_training_id)->update($utransdata);
								}
							}
						}
						
					}
					$collectcoursesidsarr = array();
					if ($roleid == 1) 
					{
						$fetchelearningplusclass =Supervisior::getunaapprovedusersofelearningclassroom($compid, '1');

						@$collectcoursesidsarr = $fetchelearningplusclass;
						$collectcourses = array();
						if (count(@$collectcoursesidsarr) > 0) 
						{
							foreach ($collectcoursesidsarr as $key => $val) 
							{
								if ($val != Array()) 
								{
									$collectcourses[$key] = $val;
								}
							}
						}
						
						if (count($collectcourses) > 0) 
						{
							foreach ($collectcourses as $values) 
							{
								foreach ($values as $itemvals) 
								{
									$scheduleId = $itemvals->session_id;
									$edituserid = $itemvals->user_id;
									$wheretoupdateinutc = 'session_id=' . $scheduleId . ' and user_id=' . $edituserid;
									$classScheduleData = Supervisior::getClassScheduleData($scheduleId, $compid);
									if (count($classScheduleData) > 0) 
									{
										$noenrollseats = $classScheduleData[0]->no_of_enrolled_seat;
										$maxseats = $classScheduleData[0]->max_seat;
										if ($noenrollseats < $maxseats) 
										{
											$numOfenroll = $classScheduleData[0]->no_of_enrolled_seat + 1;
											$updateEnrolled['no_of_enrolled_seat'] = $numOfenroll;
											$whereSchedule = array();
											$whereSchedule[] = "class_id= $scheduleId";
											$whereSchedule[] = "company_id = $compid";
											ClassSchedule::whereclassId($scheduleId)->wherecompanyId($compid)->update($updateEnrolled);
										
										}
										$utcupdata['is_approved'] = 1;
										$utcupdata['is_enroll'] = 1;
										$utcupdata['training_status_id'] = 3;
										$utcupdata['last_modfied_by'] = $userid;
										UsertrainingCatalog::wheresessionId($scheduleId)->whereuserId($edituserid)->update($utcupdata);
										$utransdata['is_active'] = 1;
										UserTranscript::whereclassId($scheduleId)->wherecompanyId($compid)->update($utransdata);

									}
								}
							}
						}
						$fetchelearningplusclassinsubmittraining = Supervisior::fetchelearningplusclassinsubmittraining($compid, '4');
						if (count($fetchelearningplusclassinsubmittraining) > 0) 
						{
							foreach ($fetchelearningplusclassinsubmittraining as $submitvalues) 
							{
								foreach ($submitvalues as $subvals) 
								{
									$utcupdata['is_approved'] = 1;
									$utcupdata['is_enroll'] = 1;
									$utcupdata['training_status_id'] = 1;
									$utcupdata['training_date'] = date('Y-m-d');
									UsertrainingCatalog::whereuserTrainingId($subvals->user_training_id)->update($utcupdata);
									$utransdata['is_active'] = 1;
									UserTranscript::whereuserTrainingId($subvals->user_training_id)->update($utransdata);
									
								}
							}
							
						}
					}
					echo $mode . "$courseapp";
				}
				$data['last_updated_date'] = date('Y-m-d -h-i-s');
				SupervisorSetting::wheresupervisorId($userid)->wherecompanyId($compid)->update($data);
				exit;
				
			}
			
			if ($exists == 0) 
			{
				if ($mode == 'tonotifyupdate') 
				{
					$data['notify_update'] = $notify;
					echo $mode . "$notify";
				}
				if ($mode == 'toapprovecourse') 
				{
					$data['auto_approve'] = $courseapp;
					echo $mode . "$courseapp";
					if ($data['auto_approve'] == 1) 
					{
						$data1['is_approved'] = 1;
						$data1['last_modified_time'] = date('Y-m-d -h-i-s');
						UsertrainingCatalog::wherelastModfiedBy($userid)->update($data1);
					}
				}
				$data['company_id'] = $compid;
				$data['supervisor_id'] = $userid;
				$data['created_date'] = date('Y-m-d -h-i-s');
				SupervisorSetting::create($data);
			}
			
		}
	}
	
	public function pendingpop1Action(Request $request) 
	{
		$operation = $request->input('operation');
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		if($roleid == 1){
			$supervisor_id = -1;
		}else{
			$supervisor_id = $userid;
		}
		if($operation==0){
			$responce = array();
			$responce = $this->classroomapprovals($responce,$supervisor_id, $compid);
			$responce = $this->submittedtrainingApprovals($responce,$supervisor_id, $compid);
			return response()->json(['status'=>201,'gridData'=>$responce]);
			
		}elseif($operation==1){
			$responce = array();
			$responce = $this->classroomapprovals($responce,$supervisor_id, $compid);
			return response()->json(['status'=>201,'gridData'=>$responce]);
		}elseif($operation==2){
			$responce = array();
			$responce = $this->submittedtrainingApprovals($responce, $supervisor_id, $compid);
			return response()->json(['status'=>201,'gridData'=>$responce]);
		}
		
	}
	public function classroomapprovals($responce, $supervisor_id, $compid)
	{
		$fetchusersofelearning = Supervisior::classroomSupervisorPendingApprovals($compid, $supervisor_id);  
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		foreach($fetchusersofelearning as $all)
			{			
				$temp_arr = array();
				$tpi = CommonFunctions::getEncode($all->training_program_id);
				$uti = CommonFunctions::getEncode($all->user_training_id);
				$temp_arr['chbx'] = 0;						
				$temp_arr['user_name'] = ucfirst($all->user_name);
				if($all->training_type_id!=4) $temp_arr['title'] = stripslashes(stripslashes($all->training_title));
				if($all->training_type_id==4) $temp_arr['title'] = $all->title;
				$temp_arr['recursive_date']= date($Getdate_formatecomapny, strtotime(@$all->recursive_date));		
				$temp_arr['itemtype']=$all->itemtype;
				$temp_arr['View']="<span class=view fl hand><div class=button_lblue_r4 onclick=viewclassroom('$tpi')>View<span>&#187;</span> </div></span>";
				
				array_push($responce, array("name"=>$temp_arr['user_name'],"item"=>$temp_arr['title'],"training_date"=>$temp_arr['recursive_date'],"item_type"=>$temp_arr['itemtype'],'chbx'=>0,'tpi'=>$tpi,'uti'=>$uti,'view'=>$temp_arr['View'],'functionName'=>'viewclassroom','functionPara'=>$tpi,'functionCall'=>'classroom','id'=>$all->user_id."||".$all->user_training_id));
			}
		return $responce;		
	}
	
	public function submittedtrainingApprovals($responce,$supervisor_id,$compid)
	{
		$fetchelearningplusclassinsubmittraining = Supervisior::submittedTrainingForSupervisorApprovals($compid, $supervisor_id);
		$Getdate_formatecomapny = CommonHelper::getdate($compid);	
		foreach($fetchelearningplusclassinsubmittraining as $all)
		{			
			$temp_arr = array();
			$uti = CommonHelper::getEncode($all->user_training_id);
			$temp_arr['chbx'] = 0;
			$temp_arr['user_name'] = ucfirst($all->user_name);
			$temp_arr['title'] = $all->training_title;
			if($all->completion_date!='0000-00-00')
			$temp_arr['completion_date']= date($Getdate_formatecomapny, strtotime(@$all->completion_date));				
			$temp_arr['itemtype']=$all->itemtype;
			$temp_arr['View']="<span class=view fl hand><div class=button_lblue_r4 onclick=viewuser('$uti')>View<span>&#187;</span></div></span>";
			
			array_push($responce, array("name"=>$temp_arr['user_name'],"item"=>$temp_arr['title'],"training_date"=>$temp_arr['completion_date'],"item_type"=>$temp_arr['itemtype'],'chbx'=>0,'uti'=>$uti,'view'=>$temp_arr['View'],'functionName'=>'viewuser','functionPara'=>$uti,'functionCall'=>'submittraining','id'=>$all->user_id."||".$all->user_training_id));
		}
		return $responce;
	}
	
	public function pendingpopAction(Request $request) 
	{
		$mode=$request->input('mode');
		$usertrainingid=$request->input('usertrainingid');
		
		$user = Auth::user();
		$compid = $user->company_id;
		$userid = $user->user_id;
		$roleid = $user->role_id;
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		
		$utransdata = array();
		
		if($mode != '') 
		{
			$edituserid = $request->input('usertrainingid');
			if(!is_numeric($edituserid))
			{ $usertrainingid = $edituserid = CommonHelper::getDecode($edituserid); }

			if($mode == 'selectedviewuser')
				{
					$userdetail =Supervisior::submittedTrainingForSelectedSupervisor($compid, $edituserid);
					if($userdetail) {
						$uti = CommonHelper::getEncode($userdetail[0]->user_training_id);
						$completionDate=stripslashes(date($Getdate_formatecomapny,strtotime($userdetail[0]->completion_date)));
						return response()->json(['status'=>201,'userdetail'=>$userdetail,'mode'=>'selectedviewuser','completionDate'=>$completionDate,'uti'=>$uti]);
					}
				}
			if($mode == 'selectedclassroom')
			{
				$userdetail = Supervisior::classroomSupervisorforselected($compid, $edituserid);
				if($userdetail) {
					$uti = CommonHelper::getEncode($userdetail[0]->user_training_id);
						$completionDate=stripslashes(date($Getdate_formatecomapny,strtotime($userdetail[0]->completion_date)));
						return response()->json(['status'=>201,'userdetail'=>$userdetail,'mode'=>'selectedclassroom','completionDate'=>$completionDate,'uti'=>$uti]);
				}
			}
			
			if($mode == 'submitted' || $mode == 'denysubmitted') 
			{ 
		       if ($mode == 'denysubmitted') 
				{
					$itemtype='';
					$submitdata['is_approved'] = 2;
					$utransdata['is_active'] = 0;
					$updateapprove_record=Supervisior::UpadateApproverecord($compid,$usertrainingid);
					Mailnotification::CourseEnrollmentDenySubmittraing($usertrainingid,$itemtype);
				}
				
		       if($mode == 'submitted') 
				{
					$submitdata['is_approved'] = 1;
					$utransdata['is_active'] = 1;
					$itemtype = 0;
					$deleteapprove_record=Supervisior::deleteApproverecord($compid,$usertrainingid);	
					Mailnotification::CourseEnrollmentApprovalSubmittraing($usertrainingid,$itemtype);	
				}
			}
			
			if($mode == 'approved' || $mode == 'deny') 
			{
				if($usertrainingid != '') 
				{
					$userTrainingData = Supervisior::getUserTrainingData($usertrainingid);
					$scheduleId = $userTrainingData[0]['session_id'];
					$companyID = $userTrainingData[0]['company_id'];
					$UserID = $userTrainingData[0]['user_id'];
					$Trainingdate = $userTrainingData[0]['training_date'];
					$whereSchedule = array();
					$whereSchedule[] = "class_id= $scheduleId";
					$whereSchedule[] = "company_id = $companyID";
					$whereCatalog = array();
					$whereCatalog []= "session_id= $scheduleId";
					$whereCatalog[] = "user_id = $UserID";
					$whereCatalog[] = "company_id = $companyID";
				}
				
				if($mode == 'approved') 
				{			
					$data=array();
					$data['is_approved'] = 1;
					$data['is_enroll'] = 1;
					$data['training_status_id'] = 3;
					$data['last_modfied_by'] = $userid;
					UsertrainingCatalog::where('session_id',$scheduleId)->where('user_id',$UserID)->where('company_id',$companyID)->update($data);
					Supervisior::deleteApproverecord($compid,$usertrainingid);
					Mailnotification::CourseEnrollmentApproval($usertrainingid);
				}
				if ($mode == 'deny') 
				{
					$updateapprove_record=Supervisior::UpadateApproverecord($compid,$usertrainingid);
					$data['is_approved'] = 2;
					$data['is_enroll'] = 0;
					$data['training_status_id'] = 5;
					$data['last_modfied_by'] = $userid;
					UsertrainingCatalog::where('session_id',$scheduleId)->where('user_id',$UserID)->where('company_id',$companyID)->update($data);
					Supervisior::deleteApproverecord($compid,$usertrainingid);
					$classScheduleData = Supervisior::getClassScheduleData($scheduleId, $companyID);
					$classScheduleData = json_decode(json_encode($classScheduleData),false);
					if(count($classScheduleData) > 0) 
					{
						$noenrollseats = $classScheduleData[0]->no_of_enrolled_seat;
						$maxseats = $classScheduleData[0]->max_seat;
						$numOfenroll = $classScheduleData[0]->no_of_enrolled_seat - 1;
						$updateEnrolled=array();
						$updateEnrolled['no_of_enrolled_seat'] = $numOfenroll;
						ClassSchedule::where('class_id',$scheduleId)->where('company_id',$companyID)->update($updateEnrolled);
					}
					Mailnotification::CourseEnrollmentDeny($usertrainingid);
				}
				
			}
			
			
			if($mode == 'bothapprove' || $mode == 'bothdeny')
			{
				$selectedusers=$request->input('selectedusers');
				$usertoupdate = explode(',', $selectedusers);
				if(count($selectedusers) > 0) 
				{
					foreach($usertoupdate as $useridwithtrainingid) 
					{
						$usertrainingid_arr = explode('||',$useridwithtrainingid);
						if(isset($usertrainingid_arr))
						{
							$userval = $usertrainingid_arr[0];
							$usertrainingid = $usertrainingid_arr[1];
							if($usertrainingid != '') 
							{
								$userTrainingData = Supervisior::getUserTrainingData($usertrainingid);
								if(is_object($userTrainingData))
								{
									$scheduleId = $userTrainingData[0]->session_id;
									$companyID = $userTrainingData[0]->company_id;
									$UserID = $userTrainingData[0]->user_id;
									$userProgramTypeId = $userTrainingData[0]->user_program_type_id;
								}
								else
								{
									$scheduleId = $userTrainingData[0]['session_id'];
									$companyID = $userTrainingData[0]['company_id'];
									$UserID = $userTrainingData[0]['user_id'];
									$userProgramTypeId = $userTrainingData[0]['user_program_type_id'];
								}	
							}
							/* userProgramTypeId  1 for Classroom Type and 4 for SubmitTraining Type */
							if($mode == 'bothapprove') {
								if($userProgramTypeId == 1) {
									$userTrainingData = Supervisior::getUserTrainingData($usertrainingid);
									$userTrainingData = json_decode(json_encode($userTrainingData),false);
									$scheduleId = $userTrainingData[0]->session_id;
									$companyID = $userTrainingData[0]->company_id;			
									try
									{
										$deleteapprove_record=Supervisior::deleteApproverecord($companyID,$usertrainingid);	
										
										$data=array();
										$data['is_approved'] = 1;
										$data['is_enroll'] = 1;
										$data['training_status_id'] = 3;
										$data['last_modfied_by'] = $userid;
										
										$whereCatalog = array();
										$whereCatalog[] = "session_id= $scheduleId";
										$whereCatalog[] = "user_id = $userval";
										$whereCatalog[] = "company_id = $companyID";
										
										UsertrainingCatalog::where('session_id',$scheduleId)->where('user_id',$userval)->where('company_id',$companyID)->update($data);
											
										Mailnotification::CourseEnrollmentApproval($usertrainingid);
										
										
									}catch (Exception $e) {}
								}else if($userProgramTypeId == 4) {
									$data['is_approved'] = 1;
									$utransdata['is_active'] = 1;
									$data['training_status_id'] = 3;
									$data['is_enroll'] = 1;
									$data['last_modfied_by'] = $userid;
									$deleteapprove_record=Supervisior::deleteApproverecord($compid,$usertrainingid);
									$itemtype = 0;
									Mailnotification::CourseEnrollmentApprovalSubmittraing($usertrainingid , $itemtype);
									
									UsertrainingCatalog::whereuserTrainingId($usertrainingid)->whereuserId($userval)->update($data);
									UserTranscript::whereuserTrainingId($usertrainingid)->whereuserId($userval)->update($utransdata);
									
								}
							}else if($mode == 'bothdeny')
							{
								if($userProgramTypeId == 1)  
								{
									try
									{
										$updateapprove_record=Supervisior::UpadateApproverecord($compid,$usertrainingid);
										$statusapporved['is_approved'] = 2;
										$statusapporved['is_enroll'] = 0;
										$statusapporved['training_status_id'] = 5;
										$statusapporved['last_modfied_by'] = $userid;
										$where = 'user_training_id=' . $usertrainingid;	
										$utransdata['is_active'] = 0;
										UsertrainingCatalog::whereuserTrainingId($usertrainingid)->update($statusapporved);
										UserTranscript::whereuserId($userval)->whereclassId($scheduleId)->update($utransdata);
										Mailnotification::CourseEnrollmentDeny($usertrainingid);	
									} 
									catch (Exception $e) {}
									
								}else if($userProgramTypeId == 4)
								{
									$data['is_approved'] = 2;
									$utransdata['is_active'] = 0;
									$data['training_status_id'] = 3;
									$data['is_enroll'] = 0;
									$updateapprove_record=Supervisior::UpadateApproverecord($compid,$usertrainingid);
									Mailnotification::CourseEnrollmentDenySubmittraing($usertrainingid);
									$data['last_modfied_by'] = $userid;
									
									UsertrainingCatalog::whereuserTrainingId($usertrainingid)->whereuserId($userval)->update($data);
									UserTranscript::whereuserTrainingId($usertrainingid)->whereuserId($userval)->update($utransdata);
								}
							}
						}
					}
				}
				
			}
			
			
			if($mode == 'approveselected') 
			{
				$selectedusers=$request->input('selectedusers');
				$usertoupdate = explode(',', $selectedusers);
				$a1 = array();
				if(count($selectedusers) > 0) 
				{
					foreach($usertoupdate as $useridwithtrainingid) 
					{
						$usertrainingid_arr = explode('||',$useridwithtrainingid);
						if(isset($usertrainingid_arr))
						{
							$userval = $usertrainingid_arr[0];
							$usertrainingid = $usertrainingid_arr[1];
							if ($usertrainingid != '') 
							{
								$userTrainingData = Supervisior::getUserTrainingData($usertrainingid);
								$userTrainingData = json_decode(json_encode($userTrainingData),false);
								$scheduleId = $userTrainingData[0]->session_id;
								$companyID = $userTrainingData[0]->company_id;
								$Trainingdate = $userTrainingData[0]->training_date;
								try
								{
									$deleteapprove_record=Supervisior::deleteApproverecord($companyID,$usertrainingid);
									$data=array();
									$data['is_approved'] = 1;
									$data['is_enroll'] = 1;
									$data['training_status_id'] = 3;
									$data['last_modfied_by'] = $userid;
									UsertrainingCatalog::where('session_id',$scheduleId)->where('user_id',$userval)->where('company_id',$companyID)->update($data);
									Mailnotification::CourseEnrollmentApproval($usertrainingid);
									
								}catch (Exception $e) {
								}
							}
						}
					}
				}
			}
			
			if($mode == 'denyselected') 
			{
				$selectedusers=$request->input('selectedusers');
				$usertoupdate = explode(',', $selectedusers);
				$a1 = array();
				if (count($selectedusers) > 0) 
				{
					foreach ($usertoupdate as $useridwithtrainingid) 
					{
						$usertrainingid_arr = explode('||',$useridwithtrainingid);
						if(isset($usertrainingid_arr))
						{
							$userval = $usertrainingid_arr[0];
							$usertrainingid = $usertrainingid_arr[1];
							if ($usertrainingid != '') 
							{
								$userTrainingData = Supervisior::getUserTrainingData($usertrainingid);
								if(is_object($userTrainingData))
								{
									$scheduleId = $userTrainingData[0]->session_id;
									$companyID = $userTrainingData[0]->company_id;
									$UserID = $userTrainingData[0]->user_id;
									$Trainingdate = $userTrainingData[0]->training_date;
								}
								else
								{
									$scheduleId = $userTrainingData[0]['session_id'];
									$companyID = $userTrainingData[0]['company_id'];
									$UserID = $userTrainingData[0]['user_id'];
									$Trainingdate = $userTrainingData[0]['training_date'];
								}
							}
							try{
								$updateapprove_record=Supervisior::UpadateApproverecord($compid,$usertrainingid);
							}
							catch (Exception $e) {}
							$statusapporved['is_approved'] = 2;
							$statusapporved['is_enroll'] = 0;
							$statusapporved['training_status_id'] = 5;
							$statusapporved['last_modfied_by'] = $userid;
							$where = 'user_training_id=' . $usertrainingid;
							UsertrainingCatalog::whereuserTrainingId($usertrainingid)->update($statusapporved);
							$classScheduleData = Supervisior::getClassScheduleData($scheduleId, $companyID);
							if (count($classScheduleData) > 0) 
							{
								$noenrollseats = $classScheduleData[0]->no_of_enrolled_seat;
								$maxseats = $classScheduleData[0]->max_seat;
								$numOfenroll = $classScheduleData[0]->no_of_enrolled_seat - 1;
								$updateEnrolled=array();
								$updateEnrolled['no_of_enrolled_seat'] = $numOfenroll;
								ClassSchedule::where('class_id',$scheduleId)->where('company_id',$companyID)->update($updateEnrolled);
								
							}
							$whereinutrans = 'user_id=' . $userval . ' and class_id=' . $scheduleId;
							$utransdata['is_active'] = 0;
							UserTranscript::whereuserId($userval)->whereclassId($scheduleId)->update($utransdata);
							Mailnotification::CourseEnrollmentDeny($usertrainingid);
							
						}
					}
				}
			}
			
			if($mode == 'approvesubmit' || $mode == 'denysubmit') 
			{
				$selectedusers=$request->input('selectedusers');
				$usertoupdate = explode(',', $selectedusers);
				if(count($selectedusers) > 0) 
				{
					foreach ($usertoupdate as $useridwithtrainingid) 
					{
						$textvalue = $useridwithtrainingid;
						$needle = '||';
						$userval = substr($textvalue, 0, strpos($textvalue, $needle));
						$usertrainingid = substr($textvalue, strpos($textvalue, "||") + 2);
						if($mode == 'approvesubmit') 
						{
							$data['is_approved'] = 1;
							$utransdata['is_active'] = 1;
							$data['training_status_id'] = 3;
							$data['is_enroll'] = 1;
							$deleteapprove_record=Supervisior::deleteApproverecord($compid,$usertrainingid);
							$itemtype = 0;
							Mailnotification::CourseEnrollmentApprovalSubmittraing($usertrainingid , $itemtype);
						}
						if($mode == 'denysubmit') 
						{
							$data['is_approved'] = 2;
							$utransdata['is_active'] = 0;
							$data['training_status_id'] = 3;
							$data['is_enroll'] = 0;
							$updateapprove_record=Supervisior::UpadateApproverecord($compid,$usertrainingid);
							Mailnotification::CourseEnrollmentDenySubmittraing($usertrainingid);
						}
						$data['last_modfied_by'] = $userid;
						UsertrainingCatalog::whereuserTrainingId($usertrainingid)->whereuserId($userval)->update($data);
						UserTranscript::whereuserTrainingId($usertrainingid)->whereuserId($userval)->update($utransdata);
						
					}
				}
			}
		}
	}
	
	
	public function getAllClassAction(Request $request)
	{
		$edituserid=$request->input('edituserid');
		
		$user = Auth::user();
		$compid = $user->company_id;
		$user_id = $user->user_id;
		$roleid = $user->role_id;
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$classarray = Coursecredits::getclassroomdata ($compid,$user_id,$roleid);
		$response = array();
		$i=1;
		if (count($classarray)>0)
		{
			foreach ($classarray as $key=>$val2)
			{
				$getenrolledapp = Coursecredits::getenrolled ($compid,$val2['item_id'], $val2['class_id']);
				if (count ( $getenrolledapp ) > 0) {
					$totalapp = $getenrolledapp ['totalapp'];
					$totalenroll = $getenrolledapp ['totalenroll'];
				} else {
					$totalapp = 0;
					$totalenroll = 0;
				}
				
				$class_id = $val2['class_id'];
				$itemid = $val2['item_id'];
				$class_name = stripcslashes(stripcslashes($val2['class_name']));
				$instructor_name = stripcslashes($val2['instructor_name']);				
				$location = stripcslashes($val2['location']);				
				$recursive_date = date($Getdate_formatecomapny,strtotime($val2['recursive_date']));
				$max_seat = stripcslashes($val2['max_seat']);
				$leftseat = ($max_seat-$totalenroll);
				$temp_arr = array('chbx'=>0,'class_name'=>$class_name,'instructor_name'=>$instructor_name,'location'=>$location,'recursive_date'=>$recursive_date,'max_seat'=>$max_seat,'leftseat'=>$leftseat,'id'=>$class_id);
				array_push($response, ($temp_arr));
				$i++;
			}
		}
		return response()->json(['status'=>201,'gridData'=>$response]);
	}
	
	public function enrollUserToClassAction(Request $request)
	{
		$user = Auth::user();
		$compid = $user->company_id;
		$user_id = $user->user_id;
		$roleid = $user->role_id;
		$class_ids = $request->input('class_id');
		$user_ids = $request->input('user_id');
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$user_id = explode(',',$user_ids);
	
		if($class_ids != '')
		{
			$program = explode (',', $class_ids);
			foreach($program as $programvalue)
			{
				$class_id = $programvalue;
				$get_content = Supervisior::getStartDateOfClass($compid,$class_id);
				$item_id = $get_content['item_id'];
				$schedule_date = $get_content['start_date_class'];
				$tariningProg = Supervisior::getTrainingProgramDetail ( $item_id, $class_id, $compid );
				$tariningProg = json_decode(json_encode($tariningProg),true);
				$get_id=array();$j=0;	
				
				if(count($tariningProg) > 0)
				{
					foreach($user_id as $key1 => $userid)
					{
						for($x = 0; $x < count ( $tariningProg ); $x ++)
						{
							if(strtotime($schedule_date) >= strtotime(date('Y-m-d')))
							{ 
						        $user_role_id = CommonFunctions::getUserRole($userid);
								$get_id[]=Enrolleduserfromallplace::enrollUserToClass($userid, $user_role_id, $compid, $class_id);
							}
						}
					}
					
					if(count($get_id)>0)
					{
						foreach($get_id as $val)
						{
							$class_name =substr($val, 0, -4); 
							if(substr($val,-2)=='wt'){
								$j++;
							}
						}
						return $class_name." , Successfully Enrolled = ".(count($get_id)-$j)." , Waiting List = ".$j."\n";						
					}
				}
			}
		}
	}
	
	
	
public function givecreditforselectedAction(Request $request)
	{
		    $ses = Auth::user();
			$compid = $ses->company_id;
			$userid = $ses->user_id;
			$roleid = $ses->role_id;
			
			$mode = $request->input('mode', 1);
			$checkeduser = $request->input('checkeduser');
			$creditarray=$this->givecreditAction($checkeduser);
			
			$callfrom = 'supervisortool' ;
			$programs = Coursecredits::getAllTrainingProgramswithClassessofcourse($compid, $userid, $roleid, $mode, $callfrom);	
			$Getdate_formatecomapny = CommonHelper::getdate($compid);
			$flag='Y';
			$dataArray=''; 
			
			foreach($programs as $value) 
			{
				if($value->item_type_id!=4)
				{
					if($flag=='Y')
					{
						if($value->training_type_id==2||$value->training_type_id==3)
						{
							$itemID = $value->item_id;
							$AllClassofCourse = array();
							$AllClassofCourse = Coursecredits::FindAllClassessofcourse($value->item_id , $compid);
							$training_title1 = '';
							if(count($AllClassofCourse) > 0)
							{
								$training_title1 ="<table width='80%' cellspacing='0' cellpadding='0' class='table_style_1 '>
								<tr>
									<td>&nbsp;</td>
									<td><b>Class Name</b></td> 					
									<td><b>Start Date</b></td>				
									<td><b>End Date</b></td> 											
								</tr>";
							}
							else
							{
								$training_title1 = "There is No Class in this Course";
							}
							foreach($AllClassofCourse as $CourseforClass)
							{
								$ClassID = $CourseforClass->class_id;
								$ClassName = $CourseforClass->class_name;
								$StartDate = date($Getdate_formatecomapny,strtotime($CourseforClass->start_date_class));
								$EndDate = date($Getdate_formatecomapny,strtotime($CourseforClass->endate_date));
								$itemTypeId = $CourseforClass->item_type_id;
								$checkIDforclass = $itemID.'-'.$itemTypeId.'-'.$ClassID;
								$name = "Classcheckbox[$itemID][]";
								$classoncheck = 'checkme_'.$itemID;
								$training_title1.= "<tr style='padding-bottom: 20px;'>
								<td><input  type='radio' name='".$name."' value='".$checkIDforclass."' id='".$checkIDforclass."' class='".$classoncheck."' ></td>
								<td>$ClassName</td> 					
								<td>$StartDate</td>				
								<td>$EndDate</td> 											
								</tr>";
							}
							$training_title1.= "</table>";
							$id = $value->item_id.'-'.$value->training_type_id.'-'.$value->class_id;
						}else if($value->training_type_id==1){
							$training_title1 = '';
							$id = $value->item_id.'-'.$value->training_type_id;
						}
						else
						{
							$training_title1 = '';
							$id = $value->assess_id.'-'.$value->training_type_id;
						}	
						$training_title = stripslashes($value->training_title);									
						if($value->training_type_id==4)
						{
							$itemtype = "Blended program";
						}
						else
						{
							$itemtype = @$value->itemtype;
						}
					}
					$subgrid_array = array();
					$subgrid_array[] = array('description'=> $training_title1);
					$dataArray[]   = array('id'=>$id,'training_title'=>$training_title,'itemtype'=>$itemtype,'subgrid'=>$subgrid_array);
					
				}
			}
			
			return response()->json(array('dataArray'=>$dataArray,'creditarray'=>$creditarray));
			
	}
	
	public function givecreditAction($checkeduser)
	{
		$ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$roleid = $ses->role_id;
		
		$checkeduser = str_replace(',', '-', $checkeduser);
		$allusers = explode('-', $checkeduser);
	    $instructorlistarray =Admin::getInstructor($compid,$roleid,$userid);
	
		$instructorArray =array();
		foreach($instructorlistarray as $row){
			$instructor['user_name'] = $row->user_name; 
			$instructor['user_id'] = $row->user_id; 
			$instructorArray[]=$instructor;
		}
		$LockedStatus = Datamanagement::fetchLockedstatusData($compid);
		$certificatename = Supervisior::getcertificates($compid);
		$trainingtypessarray = Supervisior::getTtrainingType();
		$Getdate_formatecomapny =  CommonHelper::getdate($compid);
		$calenderdateformate =  CommonHelper::getdatecalenderformate($Getdate_formatecomapny);
		$traningcategoryType = Supervisior::getTrainingCategory($compid,$userid,$roleid);
		
		return array('userid'=>$userid,'compid'=>$compid,'checkeduser'=>$checkeduser,'allusers'=>$allusers,'instructorlist'=>$instructorArray,'LockedStatus'=>$LockedStatus,'certificatename'=>$certificatename,'Trainingtypessarray'=>$trainingtypessarray,'Getdate_formatecomapny'=>$Getdate_formatecomapny,'calenderdateformate'=>$calenderdateformate,'skillcategory'=>$traningcategoryType);
	}
	
	
	
		public function manuallyadditemAction(Request $request) {
		
			$ses=Auth::user();
			$compid = $ses->company_id;
			$userid = $ses->user_id;
			$roleid = $ses->role_id;		
			$users =$request->input('users');
			
			if ($users != '') {
				$user = explode ( ',', $users );
				
				$i = 0;
				
				foreach ( $user as $uservalue ) {
						
					
					if(is_numeric($uservalue)){
						$usersid [$i] = $uservalue;
					}
					else{
						$uservalue=CommonHelper::getDecode($uservalue);
						$usersid [$i] = $uservalue;
					}
					$i ++;
				}
			}	
			$Getdate_formatecomapny = CommonHelper::getdate($compid);
			
			$tittle 					= 		$request->input( 'title' );
			$title 						= 		str_replace ( "singlequote", "'", $tittle );
			$credit_value 				= 		$request->input ( 'credit_value' );
			$desc 						= 		$request->input( 'desc' );
			$cost 						= 		$request->input( 'cost' );
			$score 						= 		$request->input( 'score', 0);
			$status 					= 		$request->input( 'status' );
			$star1 						= 		$request->input( 'star1', 0 );
			$star2 						= 		$request->input( 'star2', 0 );
			$certificate_template_id 	= 		$request->input('certificate_template_id',0);
			$skillcategoryid 			= 		$request->input ( 'skillcategoryid', 0 );
			$traningtypeid 				= 		$request->input( 'traningtypeid' );
			$compledate 				= 		$request->input ( 'compledate' );
			$instructor_name 			= 		$request->input( 'instructor_name',0 );
			
			$date = DateTime::createFromFormat($Getdate_formatecomapny, $compledate );
			$newdate 	= 		$date->format('Y-m-d');
			$compledate 				= 		$newdate;
			
			$titlee 					= 		str_replace ( "'", "''", $title );
			$dataarray =  array();
			$dataarray['company_id'] 					= $compid;
			$dataarray['lastmodifyuser_id'] 			= $userid;
			$dataarray['training_title'] 				= $title;
			$dataarray['instructor_name'] 				= $instructor_name;
			$dataarray['instructor_type'] 				= $request->input('instructor_type' );
			$dataarray['training_credit'] 				= $credit_value;
			$dataarray['training_desc'] 				= $desc;			
			$dataarray['training_cost'] 				= $cost;
			$dataarray['certificate_template_id']  		= ($certificate_template_id!='') ? $certificate_template_id:0;
			$dataarray['training_score'] 				= $score;
			$dataarray['training_status'] 				= $status;			
			if ($star1 > 0) {	
				$dataarray['average_rating'] 			= $star1;
			}else{	
				$dataarray['average_rating'] 			= 0;
			}	
			if ($star2 > 0) {	
				@$dataarray['ins_average_rating'] 		= $star2;
			}else{	
				$dataarray['ins_average_rating'] 		= 0;
			}	
			$dataarray['traininig_category_id'] 		= $skillcategoryid;
			$dataarray['traningtype_id'] 				= $traningtypeid;
			$dataarray['completion_date'] 				= $compledate;
			$dataarray['usersids'] 						= $usersid;			
			$dataarray['require_supervisor_approval'] 	= 0;			
			$dataarray['submitTraining_type'] 			= 'adminside'; 
			TrainingItemsFunctions::manuallyAddItemFunc($dataarray);
	      exit ();
	}
	public function addexistingitemsAction(Request $request)
	{
		
		$ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$roleid = $ses->role_id;
		$users =  $request->input('users');
		$programsid =$request->input('items');
		$Classid = $request->input('ClassIds');

			if($Classid != '' && $programsid != '')
			{
				$AllPrograms = $Classid.$programsid;
			}
			else if($Classid != '')
			{
				$AllPrograms = rtrim($Classid,',');
			}
			else
			{
				$AllPrograms = $programsid;
			}
			
			$usersid = array ();
			if ($users != '') {
				$user = explode ( ',', $users );
				
				$i = 0;
				
				foreach ( $user as $uservalue ) {
						
					
					if(is_numeric($uservalue)){
						$usersid [$i] = $uservalue;
					}
					else{
						$uservalue=CommonHelper::getDecode($uservalue);
						$usersid [$i] = $uservalue;
					}
					$i ++;
				}
			}
			
			if (count ( $usersid ) > 0)
			{
				if ($AllPrograms != '')
				{
					$program = explode ( ',', $AllPrograms );
					
					foreach ( $program as $programvalue )
					{
						$classID = 0;
						$prog = explode ( '-', $programvalue );					
						$ItemID   = $prog [0];
						$progtype = $prog [1];
						$classID  = isset($prog[2]) ? $prog[2] : 0;
						
						$credit_from = 1;
						if ($progtype == 1) { 
							Coursecredits::giveCreditElearningToSelectedUsers ( $userid, $compid, $usersid, $ItemID, $credit_from );
						} else if ($progtype == 2 || $progtype == 3) {
							Coursecredits::giveCreditClassroomToSelectedUsers ( $userid, $compid, $usersid, $ItemID, $classID, $credit_from );
						} else if ($progtype == 4) {
							Coursecredits::giveCreditBlendedProgToSelectedUsers ( $userid, $compid, $usersid, $ItemID, $credit_from );
						}
						else if($progtype == 6)
						{
							Coursecredits::giveCreditAssessmentToSelectedUsers( $userid, $compid, $usersid, $ItemID, $credit_from );
						}
					}
				}
			}
		
	}
	
	public function userpopupAction(Request $request)
	{
		$edituserid=$request->input('edituserid');
		$ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$roleid = $ses->role_id;
		$edituserid =CommonHelper::getDecode($edituserid); 
		$userskills = Admin::getuserdetail($edituserid, $compid);
        $getcustomfieldnameandidbycompnyname=Admin::getcustomfieldnameandidbycompnyname($compid);
		if(count($getcustomfieldnameandidbycompnyname)>0)
		{
			$customfieldIdArr=array();
			$customfieldnameArr=array();
			$customfieldvalueArr=array();
			$k=0;
			foreach($getcustomfieldnameandidbycompnyname as $customfieldnameandid)
			{
				$getcustomfieldvalue=Admin::getcustomfieldvalue($customfieldnameandid->custom_field_id,$edituserid,$compid);
				if(count($getcustomfieldvalue)>0)
				{
					$customfieldvalueArr[$k]=$getcustomfieldvalue[0]->field_value;
				}
				$k++;
			}
		}
		$fetchrolename = Admin::getrolenamesbyroleid($userskills[0]->role_id);
		$skillnames = Admin::getuserskills($edituserid, $compid);
		return response()->json(['status'=>201,'customfieldArr'=>$getcustomfieldnameandidbycompnyname,'customfieldvalueArr'=>$customfieldvalueArr,'userskills'=>$userskills,'customfieldnameArr'=>$customfieldnameArr,'fetchrolename'=>$fetchrolename,'skillnames'=>$skillnames]);
	}
	
	public function getassignedgroupsAction(Request $request)
	{
	     $userid=$request->input('userid');
		 $ses=Auth::user();
		 $compid = $ses->company_id;
		 $userid = $ses->user_id;
		 $roleid = $ses->role_id;
		 $selecteduserid=$edituserid=$encodeuid;		
	}
}
