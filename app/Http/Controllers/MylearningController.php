<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mylearningfunctions,CommonHelper;
use App\Models\UploadedFile;
use App\Models\UsertrainingCatalog;
use App\Http\Traits\Supervisior;
use App\Http\Traits\Assessments;
use App\Http\Traits\Learningplan;
use DateTime,TrainingItemsFunctions,Auth,Enrolleduserfromallplace;

class MylearningController extends Controller
{
	use Supervisior,Assessments,Learningplan;
	
	
	/**
	 * assignmentData() - Method to get assignment elearning data for grid on my elearning section.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 22 , December 2017
     */
	 
	 
    public function assignmentDataAction()
	{
		    $user = Auth::user();
			$compid = $user->company_id;
			$userid = $user->user_id;
			$roleid = $user->role_id;
			
			$viewData=Mylearningfunctions::getViewData($compid);
			
			$pageDescription = (!empty($viewData[0]->description)) ? $viewData[0]->description : '';
			
			
			$soundconceptcmpid = array('14','16', '17'); 
			$assignment_data = array();
			$buttonclass = 'button_lblue_r4';
			$Getdate_formatecomapny = CommonHelper::getdate($compid);
			$allAssignmentArr = Mylearningfunctions::getAllAssignmentArr($userid, $compid);	
			if(count($allAssignmentArr)>0) {
				 foreach($allAssignmentArr as $key => $assignment) { 
				           $assignment_str = array();
						   
						   $assignment_str[] = stripcslashes($assignment['assignmentName']);
						   
						   $assignment_str[] = $assignment['training_type'];
							if(!in_array($compid, $soundconceptcmpid))  
							$assignment_str[] = (isset($assignment['credit_value'])) ? $assignment['credit_value'] : 0;
							
							$assignment_str[] = (($assignment['assigned_date']!='') ? date($Getdate_formatecomapny,strtotime($assignment['assigned_date'])) : "");
							$assignment_str[] = (($assignment['due_date']!='' && $assignment['due_date']!='12/31/1969') ? date($Getdate_formatecomapny,strtotime($assignment['due_date'])) : "");
							$assignment_str[] =  (isset($assignment['assignmentstatus'])) ? $assignment['assignmentstatus'] : '';
												
							$assignment['imagname'] = (isset($assignment['imagname'])) ? $assignment['imagname'] : '';
							$title ='';	
							  if($assignment['imagname']=='flag_red.png'){
								$title ='Item Incomplete';
							}elseif($assignment['imagname']=='u184.png'){
								$title ='Item Complete';
							}elseif($assignment['imagname']=='flag_yellow.png'){
								$title ='Item Expired';
							}elseif($assignment['imagname']=='fail_icon2.png'){
								$title ='Item Failed';
							}


                           $assignment_str[] =  '<img title="'.$title.'" src="/user/assets/images/'.$assignment['imagname'].'" />';
					
						if($assignment['training_type_id']==4){
							
							$viewId = ''.$userid.'#'.$roleid.'||assignment||'.$assignment['blended_item_id'].'||2';
							$encoded_viewId =CommonHelper::getEncode($viewId);
							
							$assignment_str[] =  '<a class="change_col '.$buttonclass.'" href="/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId.'">View<span>&#187;</span></a>';
							$url="/trainingcatalog/blendedprogram/viewId/".$encoded_viewId;
							
						}elseif($assignment['training_type_id']==1){
						
							$viewId = ''.$userid.'#'.$roleid.'||assignment||'.$assignment['training_program_id'].'';
							$encoded_viewId = CommonHelper::getEncode($viewId);
							
							$assignment_str[] =  '<a class="change_col '.$buttonclass.'" href="/elearningmedia/training/viewId/'.$encoded_viewId.'">View<span>&#187;</span></a>';
							$url="/elearningmedia/training/viewId/".$encoded_viewId;
						
						}else{
							$viewId = ''.$userid.'#'.$roleid.'||assignment||'.$assignment['item_id'].'||'.$assignment['class_id'].'';
							$encoded_viewId = CommonHelper::getEncode($viewId);
							
							$assignment_str[] =  '<a class="change_col '.$buttonclass.'" href="/classroomcourse/training/viewId/'.$encoded_viewId.'">View<span>&#187;</span></a>';
							$url="/classroomcourse/training/viewId/".$encoded_viewId;
						}

						/* $assignment_data[] = array(
						"id"=>$assignment['assignment_id'],
						"data"=>$assignment_str
					);	 */	 
					
					$finalArray=array('assigments'=>$assignment_str[0],'training_type'=>$assignment_str[1],'credits'=>$assignment_str[2],'assigned'=>$assignment_str[3],'due_date'=>$assignment_str[4],'status'=>$assignment_str[5],'flag'=>$assignment_str[6],'flagTitle'=>$title,'flagUrl'=>'/user/assets/images/'.$assignment['imagname'],'buttonclass'=>$buttonclass,'url'=>$url,'view'=>$assignment_str[7]);
					$assignment_data[] = $finalArray;
						 			
				 }
			}
			  
			$viewDataArray=array('pageDescription'=>$pageDescription);
			return response()->json(['gridData'=>$assignment_data,'viewData'=>$viewDataArray]);
			
	}
	
	/**
	 * assignmentData() - Method to get enrollement data for grid on my elearning section.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 19 , December 2017
     */
	public function myenrollmentsDataAction()
	{
		    $user = Auth::user();
			$compid = $user->company_id;
			$userid = $user->user_id;
			$roleid = $user->role_id;
			
			$myenrollment_data = array();
			$soundconceptcmpid = array('14','16', '17');
			$Getdate_formatecomapny = CommonHelper::getdate($compid);
			$buttonclass = 'button_lblue_r4';
			
			$allMyEnrollments =Mylearningfunctions::getAllMyEnrollments($userid, $compid);
			if(count($allMyEnrollments)>0)
				{			
			        foreach($allMyEnrollments as $myenrollments)
						{
							$training_code =explode('_#_',$myenrollments['training_code']);
							$viewId = ''.$userid.'#'.$roleid.'||myenrollments||'.$myenrollments['item_id'].'||'.$myenrollments['class_id'].'||'.$myenrollments['training_type_id'].'';
							$encoded_viewId = CommonHelper::getEncode($viewId);
							
							$finalArray=array('item_name'=>stripcslashes($myenrollments['itmname']),'start_date'=>date($Getdate_formatecomapny,strtotime($myenrollments['start_date'])),'training_type'=>(string)$myenrollments['training_type'],'credits'=>$myenrollments['creditvalue'],'location'=>$myenrollments['traininglocation'],'status'=>$myenrollments['user_status'],'drop'=>'<img title="button to delete course" class="highlightit hand" src="/public/images/cross-icon.png" onclick="hides(\''.CommonHelper::getEncode($myenrollments['user_training_id']).'\');"/>','view'=>'<a class="'.$buttonclass.'" href="/#/classroomcourse/training/viewId/'. $encoded_viewId.'">View<span>&#187;</span></a>','viewHref'=>'/#/classroomcourse/training/viewId/'. $encoded_viewId,'enrollmentEncodedId'=>CommonHelper::getEncode($myenrollments['user_training_id']));
							
							$myenrollment_data[] = $finalArray;
						}
				}
           return response()->json($myenrollment_data);
			
	}
	
	/**
	 * submittrainingAction() - Method used to submit user training.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 21 , December 2017
     */
	 
	public function submittrainingAction(Request $request)
	{
		    $user = Auth::user();
			$compid = $user->company_id;
			$userid = $user->user_id;
			$roleid = $user->role_id;
			
			$title=$request->input('title');
			$description=$request->input('description');
			$completionDate=date('Y-m-d',strtotime(CommonHelper::simpleDateToPhpDate($request->input('completionDate'))));
			
			$credit_value=$request->input('credit');
			$traningtypeid=$request->input('traningtype');
			$isAttch=$request->input('isAttch');
			
			$allowforsubmittraining=Mylearningfunctions::checkForSubmitTraining($compid);
			$auto_approve= 0;
			$supervisor_arr = Supervisior::supervisorsetttings($compid, $userid);
			
			$supervisor_id=''; $error='';
			if(count($supervisor_arr)>0) {			
				$supervisor_id=$supervisor_arr[0]->supervisor_id;
				$auto_approve=$supervisor_arr[0]->auto_approve;
			}else {
				$supervisor_arr = Supervisior::getsupervisorfu($compid, $userid);
				if($supervisor_arr[0]['supervisor_id']!=0){
					$supervisor_id=$supervisor_arr[0]['supervisor_id'];
				}
				$auto_approve=0;
			}
			
			$sql_injection_status = Mylearningfunctions::prevent_from_sql_injection($title,"yes");
			if($sql_injection_status!='no error') {
				$msg = 'Invalid input for title.';
				$error=1; 
				return response()->json(['status'=>408,'message'=>$msg]);
			}
			$sql_injection_status = Mylearningfunctions::prevent_from_sql_injection($description,"yes");
			if($sql_injection_status!='no error') {
				$msg = 'Invalid input for description.';
				$error=1; 
				return response()->json(['status'=>408,'message'=>$msg]);
			}
			
			
			
			
			$sql_injection_status = Mylearningfunctions::prevent_from_sql_injection($credit_value,"yes");
			if($sql_injection_status!='no error') {
				$error=1; 
				return response()->json(['status'=>408,'message'=>'access-denied']);
			}
			
			$Getdate_formatecomapny = CommonHelper::getdate($compid);
			$date = DateTime::createFromFormat($Getdate_formatecomapny, $completionDate);
			if(!$error && $title && $traningtypeid) {
				/* if($completionDate!='') {
					$completionDate  =	$date->format('Y-m-d');
				}
				 */
				if($allowforsubmittraining[0]['require_supervisor_approval']==1) {
					$is_approved=1;
					$approved=0;
				}else {
					$is_approved=0;
					$approved=1;
				}
				
				if($isAttch==1){
					$lastfileid=CommonHelper::uploadAction($request->file('attachFile'));
				}else{
					$lastfileid='';
				}
				
				$dataarray['company_id'] 					= $compid;
				$dataarray['lastmodifyuser_id'] 			= $userid;
				$dataarray['training_title'] 				= stripslashes($title);
				$dataarray['file_id'] 						= $lastfileid;
				$dataarray['training_credit'] 				= $credit_value;
				$dataarray['training_desc'] 				= stripslashes($description);			
				$dataarray['training_cost'] 				= 0;
				$dataarray['certificate_template_id']  		= 0;
				$dataarray['training_score'] 				= 0;
				$dataarray['training_status'] 				= 0;		
				$dataarray['average_rating'] 				= 0;
				$dataarray['ins_average_rating'] 			= 0;
				$dataarray['traininig_category_id'] 		= 0;
				$dataarray['traningtype_id'] 				= $traningtypeid;
				$dataarray['completion_date'] 				= $completionDate;
				$dataarray['training_title'] 				= stripslashes($title);
				$dataarray['usersids'] 						= array('0' => $userid);
				$dataarray['require_supervisor_approval'] 	= $is_approved; 
				$dataarray['submitTraining_type'] 			= 'userside'; 
				$dataarray['auto_approve'] 					= $auto_approve; 
				$dataarray['ssupervisor_id'] 				= $supervisor_id;
				TrainingItemsFunctions::manuallyAddItemFunc($dataarray);
				$msg = 'Your request for submit training has been successfully submitted.';
				return response()->json(['status'=>201,'message'=>$msg]);
				
				 
			} 
			
	}
	
	
	/**
	 * deletemyenrollprogramAction() - Method used to delete enrollement.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 05, January 2017
     */
	public function deletemyenrollprogramAction($usertrainingprogramid)
	{
		$compid = Auth::user()->company_id;
        $userid = Auth::user()->user_id;
		if(!is_numeric($usertrainingprogramid)) {
			$usertrainingprogramid = CommonHelper::getDecode($usertrainingprogramid);
		}
		$trainingData=UsertrainingCatalog::whereuserTrainingId($usertrainingprogramid)->get()->toArray();
		$classID=$trainingData[0]['session_id'];
		Enrolleduserfromallplace::dropenrolmentAnywhere($userid,$compid,$classID);
		
	}
	
	/**
	 * learnassessmentsDataAction() - Method used to get assessment data.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 05, January 2017
     */
	public function learnassessmentsDataAction()
	{
		$compid = Auth::user()->company_id;
        $userid = Auth::user()->user_id;
        $roleid = Auth::user()->role_id;
		$elearninginprogress_data =$returnArray= array();
		$buttonclass = 'button_lblue_r4';
		$allAssessments = Mylearningfunctions::getAllAssessments($compid,$userid);
		$viewData=Mylearningfunctions::getViewData($compid);
		$pageDescription = (!empty($viewData[0]->description)) ? $viewData[0]->description : '';
		if(count($allAssessments)>0) {
				$i=1;
				foreach($allAssessments as $key => $assessments) {
					$encodeduserid 		= $assessments['encodeduserid'];
					$token 				= $assessments['token'];
					$close_date_time 	= strtotime($assessments['close_date']);
					$due_date_time 		= strtotime($assessments['due_date']);
					$allow_assessment 	= $assessments['allow_assessment'];
					$assementdetails = Assessments::getUserAssessment($encodeduserid, $token, $compid);
					$nooftimes = $assementdetails['no_of_times'];
					$data = array();
					$data[] = stripcslashes($assessments['assessment_name']);	
					$data[] = $assessments['credit_value'];
                    $data[] = $assessments['due_date'];	
					$data[] = $assessments['assesStatus'];
					$data[] = $assessments['completionStatus'];
					$url ='/survey/viewassessment/user/'.$encodeduserid.'/token/'.$token.'/red_page/learnassessments';
					if($allow_assessment==1)
						{
							if(empty($nooftimes))
							{
								$data[] = '<a href="'.$url.'" target="_blank"><div class="hand '.$buttonclass.'"  >View<span>&#187;</span></div></a>';
							}
							else
							{
								$data[] = '<div class="hand '.$buttonclass.'" ng-click="grid.appScope.redirect1(\''.$encodeduserid.'\',\''.$token.'\');">View<span>&#187;</span></div>';
							}
					    }
						if($allow_assessment==0)
						{
							if(empty($nooftimes))
							{
								$data[] = '<a href="'.$url.'"  target="_blank"><div class="hand button_lblue_r4" style="'.$buttonclass.'" >View<span>&#187;</span></div></a>';
							}
							else
							{
								$data[] = '<div class="hand '.$buttonclass.'" ng-click="grid.appScope.redirect1(\''.$encodeduserid.'\',\''.$token.'\');">View<span>&#187;</span></div>';
							}
						}
						$elearninginprogress_data[] = array("id"=>$i,"data"=>$data);
						$i++;
						$returnArray[]=array('assessment_title'=>stripcslashes($assessments['assessment_name']),'credits'=>$assessments['credit_value'],'due_date'=>$assessments['due_date'],'status'=>$assessments['assesStatus'],'progress'=>$assessments['completionStatus'],'view'=>end($data));
					
				}
		}
		$viewDataArray=array('pageDescription'=>$pageDescription);
		return response()->json(['gridData'=>$returnArray,'viewData'=>$viewDataArray]);
	}	
	
	
	/**
	 * learningDataAction() - Method to get learning plan data for grid on my elearning section.
     *
	 * Created By: Rishabh Anand
	 * Created Date: 08 January, 2017
     */
	public function learningDataAction()
	{
		$compid = Auth::user()->company_id;
		$userid = Auth::user()->user_id;
		$roleid = Auth::user()->role_id;
		$learing_plan_data = array();
		$viewData=Mylearningfunctions::getViewData($compid);
		$pageDescription = (!empty($viewData[0]->description)) ? $viewData[0]->description : '';
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$buttonclass = 'button_lblue_r4';
		$learnPlanArr = Learningplan::learnPlanArr($compid,$userid);
		if(count($learnPlanArr)>0) {
			foreach($learnPlanArr as $learnPlandata)
				{
					$due_date = ($learnPlandata['due_date'] != '' && $learnPlandata['due_date']!='0000-00-00' && $learnPlandata['due_date']!='1969-12-31') ? (($learnPlandata['due_date']) == 'Never'? 'Never': date($Getdate_formatecomapny,strtotime($learnPlandata['due_date']))) : '-';
					$expiration_date = ($learnPlandata['exp_date'] != '' && $learnPlandata['exp_date']!='0000-00-00' && $learnPlandata['exp_date']!='1969-12-31') ? (($learnPlandata['exp_date']) == 'Never'? 'Never': date($Getdate_formatecomapny,strtotime($learnPlandata['exp_date']))): '-';
					if($expiration_date == '12/31/1969' || $expiration_date == '12-31-1969'){
						$expiration_date = ' -';
					}
					$percent =((isset($learnPlandata['percent']) && $learnPlandata['percent'] > 100) ? '100' : intval($learnPlandata['percent']));
					
					if($learnPlandata['training_type_id']==4){
						$viewId = ''.$userid.'#'.$roleid.'||learningplan||'.$learnPlandata['blended_program_id'].'||3';
						$encoded_viewId =CommonHelper::getEncode($viewId);
						$url='/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId;
					}else if($learnPlandata['training_type_id']==2 || $learnPlandata['training_type_id']==3){
						$viewId = ''.$userid.'#'.$roleid.'||learningplan||'.$learnPlandata['item_id'].'||'.$learnPlandata['class_id'].'||'.$learnPlandata['training_type_id'].'';
						$encoded_viewId =CommonHelper::getEncode($viewId);
						$url='/classroomcourse/training/viewId/'.$encoded_viewId;
					}else{ 
						$viewId = ''.$userid.'#'.$roleid.'||learningplan||'.$learnPlandata['trainingProgramId'].'';
						$encoded_viewId =CommonHelper::getEncode($viewId);
						$url='/elearningmedia/training/viewId/'.$encoded_viewId;
					}
					$title ='';
					if($learnPlandata['imagname']=='flag_red.png'){
						$title ='Item Incomplete';
					}elseif($learnPlandata['imagname']=='u184.png'){
						$title ='Item Complete';
					}elseif($learnPlandata['imagname']=='flag_yellow.png'){
						$title ='Item Expired';
					}elseif($learnPlandata['imagname']=='fail_icon2.png'){
						$title ='Item Failed';
					}
					
					$learing_plan_data[]=array('learnin_plan_name'=>$learnPlandata['learning_plan_name'],'item_name'=>$learnPlandata['item_name'],'training_type'=>$learnPlandata['training_type'],'credit_value'=>$learnPlandata['credit_value'],'due_date'=>$due_date,'expiration_date'=>$expiration_date,'percent'=>$percent,'imageUrl'=>'/eliteprodlms/public/images/'. $learnPlandata['imagname'],'imageTitle'=>$title,'viewUrl'=>$url);
					
				}
				
		}
		$viewDataArray=array('pageDescription'=>$pageDescription);
		return response()->json(['gridData'=>$learing_plan_data,'viewData'=>$viewDataArray]);
	}
	
	
}
