<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use CommonHelper;
use App\Http\Traits\Loginfunction;
use App\Http\Traits\Trainingprogram;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Trainingcatalog;
use App\Http\Traits\Admin;
use App\Http\Traits\Blendedcommonclass;
use App\Models\UsertrainingCatalog;
use App\Models\UserTrainingHistory;
use App\Models\User;
use App\Models\BlendedItemCompletion;
use App\Models\TrainingCatalogTranscript;
use App\Models\UserTranscriptHistory;
use App\Models\UserTranscript;
use App\Models\UserRatingMaster;
use App\Models\Course;
use App\Models\UserTrainingCatalogHistory;
use App\Models\BlendedprogramTrainingItem;
use App\Models\BlendedUserComplation;
use App\Repositories\ScormRepository;
use URL;
use DB;
use Session;
use CommonFunctions;
use \stdClass;
//use Exception;

class ElearningmediaController extends Controller
{
    public function __construct(ScormRepository $ScormRepository){
		$this->ScormRepository = $ScormRepository;
	}
	
	public function viewtrainingdetail(Request $request)
	{ 
	    $sessiondata=Auth::user();
		$company_id=$sessiondata->company_id;
		$roleid=$sessiondata->role_id;
		$userid=$sessiondata->user_id;
		$viewId=$request->input('viewId');
		/* $decoded_param = CommonHelper::getDecode($viewId);
		$param_arr = explode('||',$decoded_param);
		
		$authIdArr = explode('#',$param_arr[0]);
		$authUserId = $authIdArr[0];
		$authRoleId = $authIdArr[1]; */
		$courseDetailArr = $prerequeisites = $support_formats = array();
		$filenames = $usertrainingid = '';
		$media_id = $averageRating = $userAverageRating = 0;	
		
		//$courseDetailArr['red_page'] = $type = $param_arr[1];
		
	
		
		//$trainingprogram_code = $param_arr[2];
		$trainingprogram_code = $viewId;
		$courseDetailArr['trainingprogram_code'] = $trainingprogram_code;
		
		
		$submittraining = 0;
		if($submittraining==1)
		{
			$programs = $model->getSubmitraining($company_id, $trainingprogram_code);
		}else{
			$programs = Trainingprogram::getCourseTrainingDetail($company_id, $trainingprogram_code);
		}
		//$courseDetailArr['averageRating'] = $this->getCourseRating($programs);
		if ($programs) 
		{
			$course_id = $programs->course_id;	
			$courseDetailArr['user_id'] 					= 	$userid;
			$courseDetailArr['course_id'] 					= 	$programs->course_id;
			$courseDetailArr['company_id'] 					= 	$company_id;
			$courseDetailArr['training_type'] 				= 	$programs->training_type;
			$courseDetailArr['credit_value'] 				= 	$programs->credit_value;
			$courseDetailArr['course_image'] 				= 	$programs->course_image;
			$courseDetailArr['training_title'] 				= 	$programs->training_title;
			$courseDetailArr['description'] 				= 	$programs->course_description;
			$courseDetailArr['prerequeisites'] 				= 	$programs->prerequeisites;
			$courseDetailArr['training_type_id']			= 	$programs->training_type_id;
			$courseDetailArr['training_program_code']		= 	$programs->training_program_code;
			$courseDetailArr['average_rating'] 				= 	$programs->average_rating;
		    $courseDetailArr['TrainingCategoryselected'] 	= 	$programs->category_name;	
			$courseDetailArr['type'] 					    = 	'';
			$courseDetailArr['userprogramtypeid'] 			=    1;
		    $alltargetaud 									= 	Trainingprogram::targetaudiences($company_id, $course_id);
			$targetaudthtml									=   $this->targetaudiencehtml($alltargetaud);
			$courseDetailArr['alltargetaud'] 				= 	$targetaudthtml;
			$courseDetailArr 								=   Trainingprogram::getMediaFileDetails($userid,$company_id,$course_id,$courseDetailArr); 
		}
	   
	     $courseDetailArr['expiration_status'] 				= Trainingprogram::checkTrainingExpiration($course_id, $userid, $company_id);
		 $view=0;
		
		$enrollelearning = array(
			"confirm"=>1 , 
			"training_program_code"=>$courseDetailArr['training_program_code'], 
			"userprogramtypeid"=>$courseDetailArr['userprogramtypeid'], 
			"course_id"=>$courseDetailArr['course_id'], 
			"fileDetails"=>$courseDetailArr['fileDetails'], 
			"media_id"=>$courseDetailArr['media_id'], 
			"file_id"=>$courseDetailArr['file_id'],
			"expiration_status"=>$courseDetailArr['expiration_status'], 
			"company_id"=>$company_id,
			"user_id"=>$userid,
			"view"=>$view
		);
	    /*echo '<pre>'; print_r($enrollelearning); exit; */
	    //$courseDetailArr['checkhandout'] = Trainingprograms::checkhandout($item_id, $company_id);
		$courseDetailArr['checkhandout'] ='';
		$courseDetailArr['transcript_id'] = Trainingprogram::getTranscriptId($company_id, $userid, $course_id);		
		$courseDetailArr['userprogramtypeid'] =  1;		
		$response =array(
			  "status"=>200,
			  "courseDetailArr"=>$courseDetailArr,
			  "enrollelearning"=>CommonHelper::getEncode(json_encode($enrollelearning)),
		);
		return response()->json($response); 
	}
	
	public function targetaudiencehtml($targetAudience){
		
			$targetstr='';
			$targetstr.=	'<div class="wp100 mt10 fl">';
			if(count(@$targetAudience)>0)
			{
				$targetstr.='<div class="widget box fl wp100">';
				$targetstr.='<div class="widget-header"><h4><i class="fa fa-users o blue_1"></i>
				Target Audience</h4> </div>';

				$targetstr.= '<div style="height: 160px; overflow:auto; scrollbar-base-color:#9f9f9f; scrollbar-arrow-color:#9f9f9f;">';

				$job = 0;
				$grp = 0;
				foreach ($targetAudience as $key => $val) {
					if($val['type_name'] == 'user')
					{
						if($job == 0){
							$job++;

							$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Users</b></div>';
						 } 
							$targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
						   
					}else{
						if($grp == 0){
										$grp++;
							
									$targetstr.='<div class="fl sup-scroll wp100 pl05 mt05"><b>Groups</b></div>';
									 }
								  $targetstr.='<div class="fl sup-scroll w190"><div class="sales-team w10 ml05 fl">&#8226;</div><div class="sales-team w150  fs13 fta fc03 ml10 fl">'.stripslashes(@$val['name']).'</div></div>';
							
					  } 
				 }
				  $targetstr.='</div></div>';
				} 
			 $targetstr.='</div>';
			 return $targetstr;
	}
	
	
	
	public function getCourseRating($programs)
	{
		$adminAverageRating = 0;
		if(isset($programs->average_rating)){
			$adminAverageRating = $programs->average_rating;
		}		
		return $adminAverageRating;
	}
	
	public function enrollelearning(Request $request){
		
		    $enrollelearning_arr=json_decode(CommonHelper::getDecode($request->input('enrollelearning_data')));
            $ses=Auth::user();
			$roleID = $ses->role_id;
			$compid = $ses->company_id;
			$userid = $ses->user_id;

			$training_program_code = $userprogramtypeid = $course_id  = $media_id = $file_id =0;
			
			
			foreach($enrollelearning_arr as $key=>$value) $$key=$value;
			if(!$view) $view = "0";
			
			if(is_int((int)$training_program_code) && is_int((int)$userprogramtypeid) && is_int((int)$course_id)  && is_int((int)$media_id) && is_int((int)$file_id))
			{
                 
				
				if($view=="0")
				{
					$utcdataid = Trainingprogram::getUserTrainingId($course_id, $userid, $compid);
					if(!$utcdataid)
					{
						$usertrainingid = Trainingprogram::takeTraining($course_id, $userid, $userprogramtypeid, $compid);
					}
					else
					{				
						if($expiration_status=="retake")
						{
							$utc_res = Trainingprogram::getRetakeUserTrainingId($course_id,$userid,$compid,$training_program_code);
							if($utc_res){
								$usertrainingid = $utc_res->user_training_id;
							}else{
								$usertrainingid =  Trainingprogram::takeTraining($training_program_code, $userid,$userprogramtypeid, $compid);
							}
						}
						else{
							$usertrainingid = $utcdataid->user_training_id;
						}
					}
				}else{
					$usertrainingid = '';
				}

				$mode = $action_url = '';
				
				$res_arr = array(
					"confirm"=>isset($confirm)?$confirm:'',
					"expiration_status"=>isset($expiration_status)?$expiration_status:'',
					"file_id"=>isset($fileDetails->files_id)?$fileDetails->files_id:'',
					"file_name"=>isset($fileDetails->file_name)?$fileDetails->file_name:'',
					"is_lib"=>isset($fileDetails->is_lib)?$fileDetails->is_lib:'',
					"username"=>isset($username)?$username:'',
					"media_id"=>$media_id,
					"userprogramtypeid"=>$userprogramtypeid,
					"training_program_code"=>$training_program_code,
					"course_id"=>$course_id, 
					"usertrainingid"=>$usertrainingid,
					"launchtime"=>date('Y-m-d h:i:s'),
					"view"=>$view
				);
				
				//print_r($res_arr);
				if(in_array($media_id,array(1,2,4,5,8,10))){
					if($media_id==10)
					{
							$action_url = 'elearningmedia/iframeplayer/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
					}
					elseif($media_id==5){
						$action_url = 'elearningmedia/embeddedplayer/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
					}else{
						$action_url = 'elearningmedia/mediaplayer/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
					}
					
					exit('{ "param":"'. $action_url .'",  "mode":"tb_show"}');
				}else if(in_array($media_id,array(7))){
					$res_arr['userprogramtypeid']=1;
					$action_url = 'elearningmedia/user-audio-player/viewId/'.CommonHelper::getEncode(json_encode($res_arr));
					
					exit('{ "param":"'. $action_url .'",  "mode":"player", "usertrainingid":"'.$usertrainingid.'", "viewId":"'.CommonHelper::getEncode(json_encode($res_arr)).'"}');
				}else{
					/* $scorm_param = $this->_getParam('scorm_param', 0);
					$courseparam = $this->_getParam('courseparam', 0);
					$redirecturl = $this->_getParam('redirecturl', 0); */
					$redirecturl =0;
					$status="";
					$allow_attempt=30;
					if(isset($fileDetails)){
                          $baseUrl = URL::to('/');
						if($media_id==11){
							$action_url = 'elearningmedia/aiccplayer';
						}elseif($media_id==3){
							$action_url = 'elearningmedia/scormplayer';
						}
						
						$url = '/administrator/mediafiles/'.$company_id.'/';
						$files_id = $fileDetails->files_id;
	
						
						$username_arr = Admin::getUserName($userid, $company_id);
						
						$username='';
						if($username_arr[0]){
							$username = $username_arr[0]->user_name;
							$username=preg_replace("/'/", "", $username); 
							$username=preg_replace('/"/', "", $username);
						}
						
						$courseparam = $url;
						$param_arr=array();
						$sql = "SELECT count(ssd.id) as next_attempt FROM scorm_score_data as ssd WHERE ssd.user_id=:userid and ssd.item_id=:item_id and ssd.company_id=:company_id AND (ssd.value='completed' OR ssd.value='passed' OR ssd.value='failed')";
						$param_arr['userid']=$userid;
						$param_arr['item_id']=$course_id;
						$param_arr['company_id']=$company_id;
		                $result2=DB::select(DB::raw($sql) ,$param_arr);
						$next_attempt=0;
                        $next_attempt=$result2[0]->next_attempt;						

						if($next_attempt){
							$next_attempt = $result2[0]->next_attempt;	
						}
						if($next_attempt==$allow_attempt){
							$status="expired";
						}
						$next_attempt = $next_attempt+1;
						$res_arr['next_attempt']=$next_attempt;
						if($view=='view'){
							$scorm_param = $userid.'_#_'.$username.'_#_'.$course_id.'_#_'.$files_id.'_#_'.$company_id.'_#_1_#_'.$usertrainingid.'_#_'.$next_attempt;
						}else{
							$scorm_param = $userid.'_#_'.$username.'_#_'.$course_id.'_#_'.$files_id.'_#_'.$company_id.'_#_0_#_'.$usertrainingid.'_#_'.$next_attempt;
						}
						
					}

					$res_arr['view']=$view;
					$res_arr['scorm_param']=$scorm_param;
					$res_arr['courseparam']=$courseparam;
					
					exit('{ "param":"'. $action_url.'/param/'.CommonHelper::getEncode(json_encode($res_arr)) .'",  "mode":"scorm", "view":"'.$view.'", "status":"'.$status.'", "redirecturl":"'.$redirecturl.'"}');
				}
			}
			else{
				exit('Wrong URL Called');
			}
	}
	
	public function mediaplayer(Request $request,$viewid) 
	{
		 
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http"; 
		$sessiondata=Auth::user();
		$compid=$sessiondata->company_id;

		$view_data_arr = json_decode(CommonHelper::getDecode($viewid));

		foreach($view_data_arr as $key=>$value) $$key=$value;

		$baseUrl = URL::to('/');
		$fileid = Trainingprogram::getfileid($course_id,$compid);
		
		$filesid = $fileid->media_library_id;
		if ($filesid) {
			
			$files = Trainingprogram::getfilename($filesid,$compid);
            
			
			$path =$baseUrl.'/administrator/mediafiles/'.$compid.'/';
			//$path = 'user/mediafiles/'.$compid.'/';

			if($files) 
			{
				$filesname = $files->file_name;
				$namefile=explode('.',$filesname);
				
				$filepath = substr($filesname,-3);
				$revStr = explode('.',strrev($filesname));
				$fileExt = strrev($revStr[0]);
				
				$fileurl=$embedded_code='';
				
				if($media_id==5){
					$filepath='';
					$embedded_code=stripslashes($files->embedded_code);
				}else{
					
				    $fileurl=$path.@$filesname;
				}
				

				 return view('user.elearningmedia.mediaplayer', ['mediafile' => $filesname,'compid'=>$compid,'filepath'=>$filepath,'filesname'=>$fileurl,'view'=>$view,'viewId'=>$viewid,'namefile'=>$namefile[0],'embedded_code'=>$embedded_code,'media_id'=>$media_id]);
				 
				
				 
			}
		}
	}
	
	public function iframemedia($compid,$filesname,$ext) 
	{
		$baseUrl = URL::to('/');
		$file=$filesname.'.'.$ext;
		$filesname =$baseUrl.'/administrator/mediafiles/'.$compid.'/'.$file;
		//$filesname='http://localhost:8001/eliteprodlms/mediafiles/'.$compid.'/'.$file;
		 return view('user.elearningmedia.iframe-media', ['filesname' => $filesname,'compid'=>$compid]);
	}
	
	public function linkplayer($viewId) 
	{
		$sessiondata=Auth::user();
		$compid=$sessiondata->company_id;
		$view_data_arr = json_decode(CommonHelper::getDecode($viewId));
		foreach($view_data_arr as $key=>$value) $$key=$value;
		
		$fileid = Trainingprogram::getfileid($course_id,$compid);
		$filesid = $fileid->media_library_id;
		if ($filesid) {
			
			$files = Trainingprogram::getfilename($filesid,$compid);
            
			if(count($files) > 0) 
			{
				if($media_id==10){
					$filepath='';
					$url=$files->url;
				}
				
				return view('user.elearningmedia.iframe-player', ['url'=>$url,'url_desc'=>'','media_id'=>$media_id,'filesname'=>'','embedded_code'=>'','view'=>$view,'viewId'=>$viewId]);
				 
			}
		}
  
	}
	
	public function scormplayer($param,$viewId)
	{ 
	
	  if(!Auth::check()){
		 $this->checkloginstatus($param);
	  }
	  
		$sessiondata=Auth::user();
		$company_id=$sessiondata->company_id;
		$roleid=$sessiondata->role_id;
		$userid=$sessiondata->user_id;
		
		/* $company_id = $this->ses['compid'];
		$userid = $this->ses['userid'];
		$this->view->company_id = $company_id; */
		
		/* $param = $this->_getParam('param', 0);
		$redirecturl = $this->_getParam('redirecturl', '');
		$this->view->redirecturl = $redirecturl;
		$this->view->scormparam = $param; */
		$param_arr = json_decode(CommonHelper::getDecode($param));

		foreach($param_arr as $key=>$value) $$key=$value;

		$user_training_id = $usertrainingid;
		$url = $courseparam;
		$var = 'view=&param='.CommonHelper::getEncode($scorm_param).'&courseparam='.CommonHelper::getEncode($courseparam);
		$variablearray  = explode('_#_', $scorm_param); 
	
		$user_id		= $variablearray[0];
		$user_name 		= $variablearray[1];
		$course_id 		= $variablearray[2];
		$file_id 	 	= $variablearray[3];
		$company_id 	= $variablearray[4];
		$preview	 	= $variablearray[5];
		$sco 			= 	$this->ScormRepository->getScorm($course_id, $user_id, $file_id);
		$scorm_title	=	$sco->title;
		$scorm_org		= 	$sco->organization;
		$scorm_file		=	$sco->launch;
		$scorm_version	=	$sco->version;
		$package_type	=	$sco->package_type;
		$file 			= 	$sco->name;
		$file_name 		= 	$sco->file_name;
		$masteryscore	=	$sco->masteryscore;
		$sco->id = $file_id;  
		
		$mode = 'normal'; // navigation mode
		$attempt = $next_attempt;
		$seekey = $this->ScormRepository->random_string(10);
		//IE 6 Bug workaround
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') !== false && ini_get('zlib.output_compression') == 'On') {
			ini_set('zlib.output_compression', 'Off');
		}

		$userdata = new \stdClass();
		if ($usertrack =$this->ScormRepository->scorm_get_tracks($course_id, $file_id, $user_id, $attempt, $usertrainingid)) {
			if ((isset($usertrack->{'cmi.exit'}) && ($usertrack->{'cmi.exit'} != 'suspend')) || ($scorm_version != "SCORM_1.3")) {
				foreach ($usertrack as $key => $value) {
					$userdata->$key = ($value!='') ? $this->ScormRepository->addslashes_js($value) : '';
				}
			} else {
				$userdata->status = '';
				$userdata->score_raw = '';
			}
		} else {
			$userdata->status = '';
			$userdata->score_raw = '';
		}
		
		$userdata->student_id = $this->ScormRepository->addslashes_js($user_id);
		$userdata->student_name = $this->ScormRepository->addslashes_js($user_name);
		$userdata->mode = 'normal';
		if (isset($mode)) {
			$userdata->mode = $mode;
		}
		if ($userdata->mode == 'normal') {
			$userdata->credit = 'credit';
		} else {
			$userdata->credit = 'no-credit';
		}
		$userdata->masteryscore = $masteryscore;
		$userdata->parameters = '';
		$userdata->isvisible = 'TRUE';
		
		$attempt = $attempt;
		$file_id = $file_id;
		$preview = $preview;
		$course_id = $course_id;
		$user_id = $user_id;
		$seekey = $seekey;
		
		$scorm_title	=	$sco->title;
		$scorm_org		= 	$sco->organization;
		$scorm_file		=	$sco->launch;
		$scorm_version	=	$sco->version;
		$package_type	=	$sco->package_type;
		$file 			= 	$sco->name;
		$file_name 		= 	$sco->file_name;
		$masteryscore	=	$sco->masteryscore;
		$scorm	=	$sco;
		
		//$this->view->userdata = $userdata;
		$scorm_version = strtolower(preg_replace('/[^a-zA-Z0-9_-]/i', '', $scorm_version));  
		//$this->view->scorm_version = $scorm_version;		
		
		$LMS_api = ($scorm_version == 'scorm_12' || $scorm_version == 'SCORM_1.2' || empty($scorm_version) || $scorm_version == '') ? 'API' : 'API_1484_11';
		$domainname = $_SERVER['HTTP_HOST'];

		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
		//$this->view->aicc_sid = $scorm_param;
       
		//exit();
		$loadsco = array(
			"scorm_title" 		=> $sco->title,
			"LMS_api" 			=> $LMS_api,
			"scorm_file" 		=> $sco->launch,
			"scorm_version" 	=> $scorm_version,
			"urlindex" 			=> ($is_lib==2) ? "http://" : "https://",
			"aicc_sid" 			=> $scorm_param,
			"protocol" 			=> ($is_lib==2) ? "http" : "https",
			"domainname" 		=> $domainname,
			"url" 				=> $courseparam,
			"file_name" 		=> $sco->file_name,
			"preview" 			=> $preview,
			"company_id" 		=> $company_id,
			"user_id" 			=> $userid,
			"user_training_id" 	=> $usertrainingid,
			"course_id" 		=> $course_id,
			"file_id" 			=> $file_id,
			//"varparam" 		=> $this->view->scormparam,
		);
	
		$loadsco = CommonHelper::getEncode(json_encode($loadsco));
		
		$redirecturl	='';
		$baseUrl		=URL::to('/');
		
		 return view('user.elearningmedia.scormplayer', ['user_training_id' => $usertrainingid,'url'=>$courseparam,'var'=>$var,'aicc_sid'=>$scorm_param,'protocol'=>$protocol,'domainname'=>$domainname,'LMS_api'=>$LMS_api,'scorm_version'=>$scorm_version,'userdata'=>$userdata,'scorm'=>$sco,'masteryscore'=>$masteryscore,'file_name'=>$file_name,'file'=>$file,'package_type'=>$package_type,'scorm_file'=>$scorm_file,'scorm_org'=>$scorm_org
		,'scorm_title'=>$scorm_title,'seekey'=>$seekey,'user_id'=>$user_id,'course_id'=>$course_id,'preview'=>$preview,'file_id'=>$file_id,'attempt'=>$attempt,'loadsco'=>$loadsco,'redirecturl'=>$redirecturl,'baseUrl'=>$baseUrl,'viewId'=>$viewId,'company_id'=>$company_id,'ScormRepository'=>$this->ScormRepository]);
		 
	  
	}
	
	public function loadscorm($param)
	 {
		$scormparam =json_decode(CommonHelper::getDecode($param));
		 return view('user.elearningmedia.loadscorm', ['scormparam'=>$scormparam]);
	 }
	
	public function checkloginstatus($reqdata){	
		
		$data_arr = json_decode(CommonHelper::getDecode($reqdata));
		 if(isset($data_arr->user_id) && isset($data_arr->company_id)){
			if($this->doLogin($data_arr)){
				exit('{"status":"success"}');
			}else{
				exit('{"status":"fail"}');
			}
		} 
	}
	
	public function dologin($data_arr){
		if(!Auth::check()){
		    if(isset($data_arr->company_id) && isset($data_arr->user_id)){
			$user2=User::where('company_id','=',$data_arr->company_id)->where('user_id','=',$data_arr->user_id)->first();
			if ($user2)
			{
				 Auth::login($user2);
				 $user = Auth::User();
				 return true;
			}else{
				 return false;
			}
		}else{
			 return false;
		}
	  }else{
		  return true;
	  }
	}
	
	
	public function datamodel(Request $request){

		$reqdata=$request->input('reqdata');
		$data_arr = json_decode(CommonHelper::getDecode($reqdata));
		if(!isset($data_arr->id) && !isset($data_arr->file_id) && !isset($data_arr->attempt) && !isset($data_arr->user_id) && !isset($data_arr->user_training_id) && !isset($data_arr->company_id)){
			exit("false\n101");
		}
		
		$item_id = $data_arr->id;
		$file_id = $data_arr->file_id;
		$attempt = $data_arr->attempt;
		$user_id = $data_arr->user_id;
		$user_training_id = $data_arr->user_training_id;
		$company_id = $data_arr->company_id;
		//echo "$item_id, $file_id, $attempt, $user_id, $user_training_id, $company_id";
		$result = true;
		$request_value = null;
		$questionAnsArray = $questionArray = array();
		$postParams = $_REQUEST;//$this->getRequest()->getPost();
		//echo "<pre>";
		//print_r($postParams);
		//usertrainingid
		
		//Log Request
		
		$text = "\n------------Request Log Time ".date('d-m-Y H:i:s')."------------\n";
		$text .= "Requested Data: ".print_r($_REQUEST,true)."\n";
		$text .= "Decode reqdata: ".print_r($data_arr,true)."\n";
		$this->writelog($text);
		try{
			$incorrectAnsCount = $correctAnsCount = $total_questions = 0;
			foreach ($postParams as $element => $value) {
				$element = str_replace('__','.',$element);
				
				if (substr($element,0,3) == 'cmi') {
					$netelement = preg_replace('/\.N(\d+)\./',"\.\$1\.",$element);
					$result = $this->ScormRepository->scorm_insert_track($user_id, $item_id, $file_id, $attempt, $netelement, $value, $user_training_id, $company_id);// && $result;
				}
				
				$netelement1 = preg_replace('/\.N(\d+)\./',"\.\$1\.",$element);

				if (substr($element,0,15) == 'adl.nav.request') {
					$search = array('@continue@', '@previous@', '@\{target=(\S+)\}choice@', '@exit@', '@exitAll@', '@abandon@', '@abandonAll@');
					$replace = array('continue_', 'previous_', '\1', 'exit_', 'exitall_', 'abandon_', 'abandonall');
					$action = preg_replace($search, $replace, $value);
					if ($action != $value) {
						// Evaluating navigation request
						$valid = $this->ScormRepository->scorm_seq_overall($item_id,$file_id,$user_id,$action,$attempt, $user_training_id);
						$valid = 'true';

						// Set valid request
						$search = array('@continue@', '@previous@', '@\{target=(\S+)\}choice@');
						$replace = array('true', 'true', 'true');
						$matched = preg_replace($search, $replace, $value);
						if ($matched == 'true') {
							$request_value = 'adl.nav.request_valid["'.$action.'"] = "'.$valid.'";';
						}
					}
				}

				if(strstr($netelement1,'cmi.interactions')){
					
					preg_match( '/[\._]([0-9]*)\./', $netelement1, $intMatch );
					if($intMatch && $intMatch[1]!=''){
						$questionAnsIndex = $intMatch[1];
						/* if(isset($questionAnsArray[$questionAnsIndex])){
							$questionAnsArray[$questionAnsIndex] = array();
						} */
						if(strstr($element,'.id')){
							$questionArray[$this->ScormRepository->addslashes_js($element)] = "id#".$value;
						}
						preg_match( '/[\._][0-9]*\.(.*)$/', $element, $lastIndexMatch );
						if($lastIndexMatch[1] != ''){
							$column = $lastIndexMatch[1];
							$questionAnsArray[$questionAnsIndex]["$column"] = $this->ScormRepository->addslashes_js($value);
						}
						if($lastIndexMatch[1] == 'result'){
							$total_questions++;
							if($value == 'correct'){
								$correctAnsCount++;
							}else{
								$incorrectAnsCount++;
							}
						}
					}
				}
			}
			
			//print_r($questionAnsArray);
			$this->ScormRepository->updateCMIInteractionsQuesAns($user_id, $item_id, $file_id, $questionAnsArray, $correctAnsCount, $incorrectAnsCount, $total_questions, $user_training_id, $company_id);
			//echo "total_questions: $total_questions<br>correctAnsCount: $correctAnsCount<br>incorrectAnsCount: $incorrectAnsCount</pre>";
			if ($result) {
				echo "true\n0";
			} else {
				echo "false\n101";
			}
			if ($request_value != null) {
				echo "\n".$request_value;
			}
		} catch (Exception $e) {
			$this->writelog('Caught exception: ',  $e->getMessage(), "\n");
		}
	}
	
	public function writelog($text){
		$baseUrl = URL::to('/');
		$filename = $_SERVER['DOCUMENT_ROOT']."/public/temp/".date("Ym")."_scorm_logs.log";
		
		if(file_exists($filename)){
			$myfile = fopen($filename, 'a');
		}else{
			$myfile = fopen($filename, 'w');
		}
        if (!$myfile) {
            return false;
        } else {
            $bytes = fwrite($myfile, $text);
            fclose($myfile);
            return $bytes;
        }
	}
	
	public function aiccplayer($param,$viewId)
	{ 
	    $ses= Auth::user();
		$company_id = $ses->company_id;
		$userid     = $ses->user_id;
		
		//$this->view->company_id = $company_id;
		
		/* $useridsuperid = $this->_getParam('edituserid', '');
		$sql_injection_status = $this->_helper->CommonFunctions->prevent_from_sql_injection($useridsuperid); */
		
		/* $itemTypeId = $this->_getParam('itemTypeId','');
		$sql_injection_status = $this->_helper->CommonFunctions->prevent_from_sql_injection($this->_helper->CommonFunctions->getDecode($itemTypeId));
		$this->view->itemTypeId = $itemTypeId; */
		
		/* $categoryTypeID = $this->_getParam('categoryTypeID','');
		$sql_injection_status = $this->_helper->CommonFunctions->prevent_from_sql_injection($this->_helper->CommonFunctions->getDecode($categoryTypeID));
		$this->view->categoryTypeID = $categoryTypeID; */
		
	//	$viewId = $this->_getParam('viewId', 0);
		//below line neccssory for check Cross-Site Scripting
		/* json_decode($this->_helper->CommonFunctions->getDecode($viewId));
		$sql_injection_status = $this->_helper->CommonFunctions->prevent_from_sql_injection($this->_helper->CommonFunctions->getDecode($categoryTypeID)); */
		//$this->view->viewId = $viewId ; 
		
		/* if($sql_injection_status!='no error')
		{
		    $layout = Zend_Layout::getMvcInstance();
            $layout->setLayout('elight/error');
            $this->render('error/404',null,true);
		} */
		
		/* $param = $this->_getParam('param', 0);
		$redirecturl = $this->_getParam('redirecturl', ''); */
		
	/* 	$this->view->redirecturl = $redirecturl;
		$this->view->scormparam = $param; */
		$param_arr = json_decode(CommonHelper::getDecode($param));

		foreach($param_arr as $key=>$value) $$key=$value;

		$variablearray  = explode('_#_', $scorm_param); 
		$user_id		= $variablearray[0];
		$user_name 		= $variablearray[1];
		$item_id 		= $variablearray[2];
		$file_id 	 	= $variablearray[3];
		$company_id 	= $variablearray[4];
		$preview	 	= $variablearray[5]; 
		
        $sco 			= 	$this->ScormRepository->getScorm($item_id, $user_id, $file_id);
		$scorm_title	=	$sco->title;
		$scorm_org		= 	$sco->organization;
		$scorm_file		=	$sco->launch;
		$scorm_version	=	$sco->version;
		$package_type	=	$sco->package_type;
		$file 			= 	$sco->name;
		$file_name 		= 	$sco->file_name;
		$masteryscore	=	$sco->masteryscore;
		$sco->id = $file_id; 
		
		$mode = 'normal'; // navigation mode
		$attempt = $next_attempt;
		$seekey = $this->ScormRepository->random_string(10);
		//IE 6 Bug workaround
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') !== false && ini_get('zlib.output_compression') == 'On') {
			ini_set('zlib.output_compression', 'Off');
		}
		
		$scorm_version = strtolower(preg_replace('/[^a-zA-Z0-9_-]/i', '', $scorm_version));
		$LMS_api = ($scorm_version == 'scorm_12' || $scorm_version == 'SCORM_1.2' || empty($scorm_version) || $scorm_version == '') ? 'API' : 'API_1484_11';
		$domainname = $_SERVER['HTTP_HOST'];
		$baseUrl = URL::to('/');
		$loadsco = array(
			"scorm_title" 		=> $sco->title,
			"LMS_api" 			=> $LMS_api,
			"scorm_file" 		=> $sco->launch,
			"scorm_version" 	=> $scorm_version,
			"urlindex" 			=> ($is_lib==2) ? "http://" : "https://",
			"aicc_sid" 			=> $scorm_param,
			"protocol" 			=> ($is_lib==2) ? "http" : "https",
			"domainname" 		=> $domainname,
			"url" 				=> $courseparam,
			"file_name" 		=> $sco->file_name,
			"preview" 			=> $preview,
			"company_id" 		=> $company_id,
			"user_id" 			=> $userid,
			"user_training_id" 	=> $usertrainingid,
			"item_id" 			=> $item_id,
			"file_id" 			=> $file_id,
		);
		
		 $redirecturl='';
		 $loadsco=CommonHelper::getEncode(json_encode($loadsco));
		 return view('user.elearningmedia.aiccplayer', ['loadsco'=>$loadsco,'baseUrl'=>$baseUrl,'preview'=>$preview,'attempt'=>$attempt,'user_training_id' => $usertrainingid,'user_id'=>$user_id,'item_id'=>$item_id,'company_id'=>$company_id,'file_id'=>$file_id,'redirecturl'=>$redirecturl,'viewId'=>$viewId,'scormparam'=>$param]);
	}
	
	public function loadaicc($param){
		 $scormparam = json_decode(CommonHelper::getDecode($param));
		 return view('user.elearningmedia.loadaicc',['scormparam'=>$scormparam]);
	}
	
	public function confirmation($param){
		  $identity=Auth::user();
		  $company_id =$identity->company_id;
		  $feedback='';
		  //$param = json_decode(CommonHelper::getDecode($param));
		 return view('user.elearningmedia.confirmation',['viewId'=>$param,'compid'=>$company_id,'feedback'=>$feedback]);
		
	}
	
	public function programinpercentageAction(Request $request)
	{
		$percentage				=$request->input('percentage');
		$enrollelearning_data	=$request->input('enrollelearning_data');
		$ses=Auth::user();
		$compid = $ses->company_id;
		$userid = $ses->user_id;
		$trainingprogram_id = $userprogramtypeid = $course_id = $media_id = $file_id = 0;
		$enrollelearning_arr = json_decode(CommonHelper::getDecode($enrollelearning_data));
		
		foreach($enrollelearning_arr as $key=>$value) $$key=$value;
	
		$data['training_status_id']=3;
		$data['is_enroll']=1; 
		UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->update($data);
        $records=UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('status_id','=',1)->first();
           
		$data1['user_training_id']=$usertrainingid;
		$data1['user_id']=$userid;
		$data1['course_id']=$course_id;
		$data1['training_start_date_time']=$launchtime;
		$data1['course_completion_percentage']=$percentage;
		$data1['training_end_date_time']=date('Y-m-d H:i:s');
		$data1['php_session_id']=Session::getId();
		$data1['status_id']=1;
		if($records){
			UserTrainingHistory::insert($data1);
		
		}else{
			$datah['status_id']=0;
			$datah['php_session_id']=Session::getId();
			UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->update($datah);
			UserTrainingHistory::insert($data1);
		}
	}
	
	public function updatelearningcompletion(Request $request) 
	{

		$ses			=Auth::user();
		$compid		 	= $ses->company_id;
		$userid 		= $ses->user_id;
		$classrate		=$request->input('classrate');
		$comment		=$request->input('comment');
		$confirm		=$request->input('confirm');
	
		$viewId			=$request->input('viewId');
		$view_data_arr 	= json_decode(CommonHelper::getDecode($viewId));
	 
		foreach($view_data_arr as $key=>$value) $$key=$value;
		
		$userlearningplan = Usertrainingcatalogs::getTrainingCatalogId($usertrainingid,$userid,$compid);
	

		if(count($userlearningplan) == 0)
		{	
			$programsschedule= array();
			$lastId = Trainingcatalog::EnrolledForElearningLearningplan($course_id, $userid, $userprogramtypeid, $compid);
			$usertrainingid = $lastId;
		}
		else
		{
			$usertrainingid = $userlearningplan[0]['user_training_id'];
		}
           
		
		if($confirm==1 && $course_id!='')
		{
			
			 //update and insert data into user_training_history table
			$this->updateInsertUserTrainingHistory($usertrainingid, $course_id, $userid,$launchtime);
			
			
			
			 $creditdata=Course::where('course_id','=',$course_id)->where('status_id','=',1)->where('company_id','=',$compid)->first();
			 $credit_value= 0;
			 if($creditdata){
				$credit_value= $creditdata->credit_value;
			 }
			//update data into user_trascript table
			$this->updateDataIntoTrascript($credit_value,$course_id,$userid,$usertrainingid,$compid);
			
			//update and insert data into user_training_catalog table
			$this->updateInsertUserTrainingCatalog($course_id, $compid, $userid,$usertrainingid);
			
			$feedback=CommonFunctions::getELearnigFeedback($compid);
			
			if($feedback==0)
			{
				if((int)$classrate!=0){
					//update and insert data into user_rating_master table
					$this->updateInsertUserRatingMaster($compid, $course_id, $userid, $classrate, $usertrainingid, $comment);
				}
			} 
			
			
			//update data into item_master table
			$this->updateCourseRating($course_id, $compid);
			
			CommonFunctions::getCourseSkillLevel($course_id, $compid,$userid,1);
		}
		
	}
	
	
	public function updateCourseRating($course_id, $compid)
	{				
		$averageclassrating = Trainingcatalog::averageClassRating($course_id,$compid);
		if($averageclassrating){
			$datai['average_rating']=$averageclassrating;
			Course::where('course_id','=',$course_id)->where('company_id','=',$compid)->update($datai);
		}	
	}
	
	public function updateInsertUserTrainingCatalog($course_id, $compid, $userid,$usertrainingid){
		$data['training_status_id']	=	1;	
		UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->update($data);
		
	    $dataall=UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->first()->toArray();
		UserTrainingCatalogHistory::insert($dataall);
		
		UsertrainingCatalog::where('user_training_id','!=',$usertrainingid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->where('user_id','=',$userid)->where('training_status_id','=',1)->delete();
		
	} 
	
	
	public function updateInsertUserTrainingHistory($usertrainingid, $course_id, $userid,$launchtime)
	{
		$recordss=UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('user_training_id','=',$usertrainingid)->where('status_id','=',1)->first();
		
		if($recordss)
		{

			$datatrans['user_training_id']=$usertrainingid;
			$datatrans['course_id']=$course_id;
			$datatrans['user_id']=$userid;
			$datatrans['status_id']=0;
			UserTrainingHistory::where('user_id','=',$userid)->where('course_id','=',$course_id)->update($datatrans);
		}
				
			$data1['user_training_id']=$usertrainingid;
			$data1['course_id']=$course_id;
			$data1['user_id']=$userid;
			$data1['course_completion_percentage']=100;
			$data1['training_end_date_time']=date('Y-m-d H:i:s');
			$data1['training_start_date_time']=$launchtime;
			$data1['php_session_id']=Session::getId();
			UserTrainingHistory::insert($data1);
	} 
	
	public function updateDataIntoTrascript($credit_value,$course_id,$userid,$usertrainingid,$compid)
	{
		
		$data['class_id']=0;
		$data['training_type_id']=1;
		$data['user_id']=$userid;
		$data['course_id']=$course_id;
		$data['user_training_id']=$usertrainingid;
		$data['company_id']=$compid;
		$data['credit_value']=$credit_value;
		$data['completion_date']=date('Y-m-d');
		$data['times']=1;
		$data['credit_from']=0;
		$data['is_approved']=1;
		$data['status_id']=1;
		$data['last_modified_by']=$userid;
		$data['expiration_date'] = '0000-00-00';
		
		$currentDate = date('Y-m-d');
		$transcriptsdata = UserTranscript::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('company_id','=',$compid)->first();
		
		$utid = 0;
		if($transcriptsdata)
		{   
	        $transcriptsdata=$transcriptsdata->toArray();
			$utid = $transcriptsdata['transcript_id'];
			$times = $transcriptsdata['times'];
			$datat['completion_date']= date('Y-m-d');
			if(!$data['expiration_date'])
			{ 
			 $data['expiration_date'] = '0000-00-00'; 
		    }else{
			   $datat['expiration_date'] = $data['expiration_date'];
			}
			$datat['status_id']=1;
			$datat['times']=$times+1;
			$datat['user_training_id']=$usertrainingid;
			
			UserTranscript::where('course_id','=',$course_id)->where('user_id','=',$userid)->where('company_id','=',$compid)->update($datat);
			
			UserTranscriptHistory::insert($transcriptsdata);

		}
		else
		{
			if(!$data['expiration_date'])
			{
		     $data['expiration_date'] = '0000-00-00'; 
		    }
		    $transcript_id = UserTranscript::insertGetId($data);
			/* $data['transcript_id']=$transcript_id;
		    UserTranscriptHistory::insert($data); */
			
		}
		
	}

	public function updateInsertUserRatingMaster($company_id, $course_id, $user_id, $classrate, $user_training_id, $comment)
	{
		
		$utc_rows =UsertrainingCatalog::where('user_training_id','=',$user_training_id)->where('course_id','=',$course_id)->where('company_id','=',$company_id)->where('user_id','=',$user_id)->where('training_status_id','=',1)->first();

		$ut_rows =UserTranscript::where('company_id','=',$company_id)->where('course_id','=',$course_id)->where('user_id','=',$user_id)->where('user_training_id','=',$user_training_id)->first();
	
		if(($utc_rows) && ($ut_rows)){
			   $urm_rows  =UserRatingMaster::where('company_id','=',$company_id)->where('course_id','=',$course_id)->where('user_id','=',$user_id)->where('user_training_id','=',$user_training_id)->first();

			if(!$urm_rows){
				$randomassessid = intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
				$data3 = array();
				$data3['user_training_id']	= $user_training_id;
				$data3['user_id']			= $user_id;
				$data3['class_rate']		= isset($classrate) ? $classrate : 0;
				$data3['comment']			= isset($comment) ? $comment : '';
				$data3['course_id']			= $course_id;
				$data3['random_number']		= $randomassessid;
				$data3['status_id']			= 1;
				$data3['item_type']			= 1;
				$data3['company_id']		= $company_id;
				$data3['class_id']			= 0;
				$data3['instructor_id']		= 0;
				$data3['instructor_rate']	=0;
				UserRatingMaster::insert($data3);
			}
			
		}
	}
	
}
