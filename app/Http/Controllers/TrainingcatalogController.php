<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,CommonHelper,Trainingcatelogallplace;
use stdClass;
use App\Http\Traits\Blendedtrainingitem;
use App\Http\Traits\Trainingprogram;
use App\Models\UserTranscript;
use App\Http\Traits\Usertrainingcatalogs;
use App\Http\Traits\Assessments;
use App\Http\Traits\Assessmentcrypt;
use App\Http\Traits\Userrating;
use App\Http\Traits\Contentmanagement;
use App\Http\Traits\Trainingprograms;
use App\Models\UsertrainingCatalog;
use URL,CommonFunctions;


class TrainingcatalogController extends Controller
{
	use Contentmanagement,Trainingprograms;
	
    public function mygridBlendedAction(Request $request){
		$user = Auth::user();
		$baseUrl = URL::to('/');
	    $compid =  $user->company_id;
        $userid =  $user->user_id;
		$roleid =  $user->role_id;
        $viewid = $request->input('viewid');
        $decoded_param = CommonHelper::getDecode($viewid);
        $param_arr = explode('||',$decoded_param);
		$authIdArr = explode('#',$param_arr[0]);
		$authUserId = $authIdArr[0];
		$authRoleId = $authIdArr[1];
		$type = $param_arr[1];
		$blendedprogid = $param_arr[2];
		$userprogramtypeid = $param_arr[3];
        $blendedDetail = Blendedtrainingitem::getblendeddetail($blendedprogid, $compid);
		$programs = Blendedtrainingitem::getitemsUnderBlendedProgram($userid, $blendedprogid, $compid);
		$backto = 'blended';
		$itemTypeId = 0;
		$categoryTypeID = 0;
		$detail = array('programs'=>@$programs,'titles'=>$blendedDetail[0]['title'],'description'=>$blendedDetail[0]['description'],'blendedimage'=>$blendedDetail[0]['blendedimage']);
		
		$items=Blendedtrainingitem::getallItems($userid,$blendedprogid,$compid);
		$dataArray =array();
		$titles = '';
		$blendedTrainingItems = array();
		$statuscount = 0;
		$totalcredit = 0;
		if(count ( $items ) > 0){
				$i = 0;
		   foreach ( $items as $key => $value ){
			   $trainingItem = new stdClass;
			   $trainingTypeId = $value ['training_type_id'];
			   $is_sequence = $value ['is_sequence'];
			   if($trainingTypeId == 1){
				        $trainingItem->item_id 			= $value['item_id'];
						$trainingItem->training_item_id = $value['training_item_id'];
						$trainingItem->training_type_id = $value['training_type_id'];
						$trainingItem->sequence 		= $value['sequence'];
						$trainingItem->blened_prog_id	= $value['blendedprogram_id'];
						$trainingItem->blended_title	= $value['title'];
						$trainingItem->credit_value		= $value['credit_value'];
						$trainingItem->item_name		= $value['item_name'];
						$trainingItem->training_status	= $value['status'];
						if($value['status'] == 1)
						{
							$statuscount++;
							$totalcredit = $totalcredit+$value['credit_value'];
						}
						$trainingItem->training_program_id = $value['training_program_id'];
						$trainingItem->schedule_id 		= 0;
						$trainingItem->class_id 		=0;
						$blendedTrainingItems[] = $trainingItem;
			   }elseif($trainingTypeId == 2 || $trainingTypeId == 3){
				        $trainingItem->item_id 			= $value ['item_id'];
						$trainingItem->training_item_id = $value ['training_item_id'];
						$trainingItem->training_type_id = $value ['training_type_id'];
						$trainingItem->sequence 		= $value ['sequence'];
						$trainingItem->blened_prog_id	= $value ['blendedprogram_id'];
						$trainingItem->blended_title	= $value ['title'];
						$trainingItem->credit_value		= $value['credit_value'];
						$trainingItem->item_name		= $value['item_name'];
						$trainingItem->training_status	= $value['status'];
						if($value['status'] == 1)
						{
							$statuscount++;
							$totalcredit = $value['credit_value'];
						}
						$trainingItem->training_program_id 	= '';
						$class_id = Trainingprogram::getLatestClass($compid,$value['item_id']);
						$trainingItem->class_id = ($class_id!='') ? $class_id['class_id']:'';
						$blendedTrainingItems[] = $trainingItem;
						
			   }elseif($trainingTypeId == 6 && $value['id'] != 0){
						$trainingItem->item_id 			= $value ['id'];
						$trainingItem->training_item_id = $value ['training_item_id'];
						$trainingItem->training_type_id = $value ['training_type_id'];
						$trainingItem->sequence 		= $value ['sequence'];
						$trainingItem->blened_prog_id	= $value ['blendedprogram_id'];
						$trainingItem->blended_title	= $value ['title'];
						$trainingItem->credit_value		= $value['creditvalue'];
						$trainingItem->item_name		= $value['assessment_name'];
						$trainingItem->training_status	= $value['status'];
						$trainingItem->due_date	= $value['due_date'];
						if($value['status'] == 1)
						{
							$statuscount++;
							$totalcredit = $value['credit_value'];
						}
						$trainingItem->training_program_id 	= '';
						$trainingItem->class_id 			= @$value['class_id'];
						$blendedTrainingItems[] = $trainingItem;
					}
		   }
		   
		     if($statuscount == count ( $items )){ 
					$this->updateTranscriptStatus($userid,$blendedprogid,$statuscount,$totalcredit,$compid); 
				 }
				 
				if(count($blendedTrainingItems) > 0){
					$i=0; $j=1;
					$statusComplete = '0';
					$showuiseq = 0;
					$compid=$compid;
					foreach($blendedTrainingItems as $key1 =>$program){
						if($i%2==0){
							$class="bgc17";
						}else{
							$class="bgc08";
						}
						
						$trainingtypeid 	= 	$program->training_type_id;
						$itemid				=	$program->item_id;
						$class_id			=	$program->class_id;
						$training_item_id	=	$program->training_item_id;
						$sequence			=	$program->sequence;
						$blened_prog_id		=	$program->blened_prog_id;
						$blended_title		=	$program->blended_title;
						$credit_value		=	$program->credit_value;
						$item_name			=	$program->item_name;
						$completion			=	$program->training_status;
						$trainingprogramid	=	$program->training_program_id;
						$due_date			=	@$program->due_date;
						$showuiseq++;
						if($completion == 1){
						   $statusComplete = $showuiseq;
						}
                        $item_name=stripcslashes($item_name);
						if($trainingtypeid==1){
							$type= "eLearning";
						}else if($trainingtypeid==2 || $trainingtypeid==3){ 
							$type= "Classroom";
						}else if($trainingtypeid==6){ 
							$type= "Assessment";
						}
                        if($completion == 1){
							$status= "Completed";
						}else{ 
							$status= "Incomplete";
						}
                        $ak= $showuiseq;
						$button = '';
						if($trainingtypeid==1){
							$viewId = $userid."#".$roleid."||".$backto."||".$trainingprogramid."||".$blened_prog_id."||";
							$encoded_viewId = CommonHelper::getEncode($viewId);
							$encodedurl = $baseUrl."/#/elearningmedia/training/viewId/". $encoded_viewId."/itemTypeId/".$itemTypeId."/categoryTypeID/".$categoryTypeID;
							$button.='<input type="hidden"  id="record_'.$j.'" value="'.$completion.'">';
							if($completion==0){  
								$button .= "<div id=hide_'".$j."'><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."')>View<span>&#187;</span></a></div><input type='hidden' id='programid_".$j."' value='".$completion."'>";
							}else{
								$button .= "<div id=hide_'".$j."'><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."')>View<span>&#187;</span></a></div>";
							} 
						}elseif($trainingtypeid==2 || $trainingtypeid==3){
							$viewId = $userid."#".$roleid."||".$backto."||".$itemid."||".$class_id."||".$blened_prog_id."||";
							$encoded_viewId = CommonHelper::getEncode($viewId);
							$encodedurl = $baseUrl."#/classroomcourse/training/viewId/". $encoded_viewId."/itemTypeId/".$itemTypeId."/categoryTypeID/".$categoryTypeID;
							$button.='<input type="hidden"  id="record_'.$j.'" value="'.$completion.'">';
							if($completion==0)
							{
								$button .= "<div id=hide_'".$j."'><a class= button_lblue_r4 style=outline:none;border-radius:4px; ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."')>View<span>&#187;</span></a></div><input type='hidden'  id='programid_".$j."' value='".$completion."'>";
							}else{
								$button .= "<div id=hide_'".$j."' ><a class= button_lblue_r4  ng-click= grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."')>View<span>&#187;</span></a></div>";
							}
						}else{
							$this->distributeUserAssessment($blened_prog_id,$itemid,$due_date); 
							$encodeduserid=Assessmentcrypt::encode($userid);
							$token='';
							$fetchtokenid=Assessments::fetchgeneratedurl($compid,$itemid,'systemuser',0);
							if(count($fetchtokenid)>0)
								$token=$fetchtokenid[0]->url_id;
							$assementdetails = Assessments::getUserAssessment($encodeduserid,$token, $compid);
							$nooftimes = $assementdetails['no_of_times'];
							$allow_assessment = $assementdetails['allow_assessment'];
							$encodedurl =$baseUrl.'/survey/viewassessment/user/'.$encodeduserid.'/token/'.$token;
							$encodedurl1 =$baseUrl.'/mylearning/successfullsubmitasse/userid/'.$encodeduserid.'/token/'.$token;					
							$button.='<input type="hidden"  id="record_'.$j.'" value="'.$completion.'">'; 
							if($completion==0){
								 if($allow_assessment==1){
									  if(empty($nooftimes))
									 {
										$button .= "<div id=hide_'".$j."' ><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div><input type='hidden' id='programid_".$j."' value='".$completion."'>";
									 }
									 else
									 {
										$button .= "<div id=hide_'".$j."' ><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl1."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div><input type='hidden' id='programid_".$j."' value='".$completion."'>";
									 }
								 }
								  if($allow_assessment==0)
								 {
									 if(empty($nooftimes))
									 {
										$button .= "<div id=hide_'".$j."' ><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div><input type='hidden' id='programid_".$j."' value='".$completion."'>";
									 }
									 else
									 {
										$button .= "<div id=hide_'".$j."' ><a class=button_lblue_r4 ng-click = grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl1."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div><input type='hidden' id='programid_".$j."' value='".$completion."'>";
									 }
								 }
							}else{
								if($allow_assessment==1)
								{
									 if(empty($nooftimes))
									 {
										$button .= "<div id=hide_'".$j."'><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div>";
									 }
									 else
									 {
										$button .= "<div id=hide_'".$j."'><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl1."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div>";
									 }
								 }
								  if($allow_assessment==0)
								 {
									 if(empty($nooftimes))
									 {
									 $button .= "<div id=hide_'".$j."'><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div>";
									 }
									 else
									 {
									  $button .= "<div id=hide_'".$j."'><a class=button_lblue_r4 ng-click=grid.appScope.completioncheck('".$j."','".$completion."','".$showuiseq."','".$encodedurl1."','".$due_date."','".$itemid."','".$is_sequence."','".$blened_prog_id."')>View<span>&#187;</span></a></div>";
									 }
								 }
							}	
						}

                        /*  $dataArray[] = array("id"=>$j,"data"=>array($item_name,$credit_value,$type,$status,$sequence,$button));		
						  */
                         $finalarray[] = array('item_name'=>$item_name,'credit'=>$credit_value,'itme_type'=>$type,'traning_status'=>$status,'sequence'=>$sequence,'button'=>$button);	
                         $j++;						 
					}
				}	 


              				 
		}
		    return response()->json($finalarray);
          	/* 	 echo '<pre>'; print_r($dataArray); exit;
			$json = $this->_helper->json->encodeJson($dataArray);
			exit('{rows:'.$json.'}'); */
		
	}



     public function updateTranscriptStatus($userid,$blendedprogid,$statuscount,$totalcredit,$compid){
		$databld['company_id']		=	$compid;
		$databld['user_id']			=	$userid;
		$databld['credit_value']	=	$totalcredit;
		$databld['completion_date']	=	date('Y-m-d');
		$databld['is_active']		=	1;
		$databld['is_approved']		=	1;
		$databld['last_modified_by']=	$userid;
		$databld['registration_id']	=	$blendedprogid;
		$databld['is_blended']		=	1;
		$tranciptdatablend  = UserTranscript::where('registration_id','=',$blendedprogid)->where('user_id','=',$userid)->where('is_blended','=',1)->WhereNull('item_id')->where('is_active','=',1)->where('company_id','=',$compid)->get();
		if(count($tranciptdatablend)==0){ 
			Usertrainingcatalogs::insertusertrascript($databld);
	 	} 
		$result = UsertrainingCatalog::where('user_id','=',$userid)->where('blended_program_id','=',$blendedprogid)->where('training_type_id','=',4)->where('company_id','=',$compid)->select('user_id')->get();
		if(count($result)){
		  UsertrainingCatalog::where('user_id','=',$userid)->where('blended_program_id','=',$blendedprogid)->where('training_type_id','=',4)->where('company_id','=',$compid)->update(['training_status_id'=>1]);
		}else{
			$data = array(
                'user_id'               => $userid,
				'program_id'            => 0,
				'is_enroll'             => 1,
				'is_approved'           => 1,
				'training_type_id'      => 4,
				'user_program_type_id'  => 0,
				'item_id'               => 0,
				'company_id'            => $compid,
				'training_status_id'    => 1,
				'blended_program_id'    => $blendedprogid,
				'learning_plan_id'      => 0,
                'session_id'       		=> '',
                'last_modfied_by'     	=> $userid,
                'created_date'    		=> date('Y-m-d H-i-s'),
			);
			
			Usertrainingcatalogs::insertusertrainigcatalog($databld);
		}
	}	
	
	
	
	function distributeUserAssessment($blenedprogid,$assess_id,$due_date){
		$user = Auth::user();
		$baseUrl = URL::to('/');
	    $compid =  $user->company_id;
        $userid =  $user->user_id;
		$roleid =  $user->role_id;
		$allGroupIdArr = array();
		$usersFromAllGroup = array();
		if($userid != '')
		$allUserIdArr = explode(',',$userid);
	    if(count($allUserIdArr) > 0){
			foreach($allUserIdArr as $user_id){
				$tempArr = array();
				$tempArr['assess_id'] 		= $assess_id;
				$tempArr['user_id'] 		= $user_id;
				$tempArr['blended_prog_id'] = $blenedprogid;
				$tempArr['company_id'] 		= $compid;
				$tempArr['due_date'] 		= '0000-00-00';
				$tempArr['is_delete'] 		= 0;
				$tempArr['attempt_taken'] = 1;
				$tempArr['assigned_date'] = date('Y-m-d');
				$tempArr['date_sent'] = '0000-00-00';
				$checkUserAssign = Assessments::checkUserGroupAssign('user_assessment',$assess_id,$user_id,$compid);
				if(count($checkUserAssign)==0){
					Assessments::insertuserassessment('user_assessment', $tempArr);
				}
			}
		}
		
		foreach($allUserIdArr as $user_id){
		$encodeduserid = Assessmentcrypt::encode($user_id);
		$checkAlreadyValueExit=Userrating::checkUrlexistsornot($assess_id,$user_id,$compid,'survey/viewassessment');
		if(count($checkAlreadyValueExit) == 0){
			$urlid = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
			$fetchgeneratedurl = Assessments::fetchgeneratedurl($compid, $assess_id, 'systemuser', 0);
			if (count($fetchgeneratedurl) > 0)
				{
					$urlid = $fetchgeneratedurl[0]->url_id;
				}
			if(count($fetchgeneratedurl) == 0)
				{
					$inserturl = array();
					$inserturl['url_id'] = $urlid;
					$inserturl['assess_schedule_id'] = 0;
					$inserturl['assess_id'] = $assess_id;
					$inserturl['url_type'] = 'systemuser';
					$inserturl['last_updated_by'] = $user_id;
					$inserturl['last_updated_date'] = date('Y-m-d -h-i-s');
					$assessurlid = Assessments::lastinsertid('assessment_url_master', $inserturl);
				}

             $snurl=date('Y-m-d h:i:s')."_".$compid."_".$user_id;
				$paramurl=md5($snurl);
				$datalink=array();
				$datalink['training_catalog_id']=$assess_id;
				$datalink['company_id']=$compid;
				$datalink['user_id']=$user_id;
				$datalink['training_catalog_id']=0;
				$datalink['parameter']='user/' . $encodeduserid . '/token/' . $urlid;
				$datalink['send_URL']=$paramurl;
				$datalink['pagename']="survey/viewassessment";
				Usertrainingcatalogs::inserttbluserlink('tbl_user_link',$datalink);				
		}
			
		}
	}
	
	/**
     * trainingCatolgDataArr() - Method to get training catalog data 
     * 
	 * Created By: Siddharth Kushwah
	 * Created Date: 25 Feb, 2018
     */
	public function trainingcatolgdataarrAction(Request $request)
	{	
	        $compid = Auth::user()->company_id;
			$userid = Auth::user()->user_id;
			$roleid = Auth::user()->role_id;
			
			$mode=$request->input('mode');
			$start=$request->input('start');
			$length=$request->input('length');
			$search = $request->input('search');
			$draw = $request->input('draw');
			$order = $request->input('order');
			$orderColumn=$order[0]['column'];
			$orderType=$order[0]['dir'];
			$searchtext=$search['value'];
			
			
			$programs1=Trainingcatelogallplace::getElearniingCatelog($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext);
	//echo '<pre>'; print_r($programs1); exit;
			$getclassarray=array();
			$getblendedarray=array();
		
			$programsresult=Trainingcatelogallplace::return_html($userid,$programs1[0],$roleid,$compid,$itemTypeId='',$categoryTypeID='');	return  $success=array("status"=>200,"data"=>$programsresult,"count"=>$programs1[1],"draw"=>$draw);

	}
	
	public function trainingcatalogFilterlist()
	{
		$compid = Auth::user()->company_id;
		$userid = Auth::user()->user_id;
		$roleid = Auth::user()->role_id;
		$trainingcategory = CommonFunctions::getTraningCategory($compid);
		$trainingtype = CommonFunctions::get_training_type();
		$instructorarray = CommonFunctions::getInstructorList($compid,$roleid,$userid);
		return response()->json(['trainingcategory'=>$trainingcategory,'trainingtype'=>$trainingtype,'instructorarray'=>$instructorarray]);
	}
	
	public function categoryfilterAction(Request $request)
	{
		
		//$viewData=Contentmanagement::fetchcmstrainingcataloginfo($compid);
		$categoryID = $request->input('categoryType');
		$traingTypeId = $request->input('itemType');
		$catid=$traingTypeId;
		$cattype = ($categoryID=='null') ? '' :$categoryID;
		    $compid = Auth::user()->company_id;
			$userid = Auth::user()->user_id;
			$roleid = Auth::user()->role_id;
			
			$mode=$request->input('mode');
			$start=$request->input('start');
			$length=$request->input('length');
			$search = $request->input('search');
			$draw = $request->input('draw');
			$order = $request->input('order');
			$orderColumn=$order[0]['column'];
			$orderType=$order[0]['dir'];
			$searchtext=$search['value'];
	
			
			$programs1=Trainingcatelogallplace::getTrainingCatalogFilter($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$traingTypeId,$cattype);
	
			$getclassarray=array();
			$getblendedarray=array();
			
			$programsresult=Trainingcatelogallplace::return_html($userid,$programs1[0],$roleid,$compid,$catid='',$cattype='');	 
			return  $success=array("status"=>200,"data"=>$programsresult,"count"=>$programs1[1],"draw"=>$draw);
		
		
	
	}
	
	public function trainingCatalogAdvanceFilter(Request $request)
	{
				$compid = Auth::user()->company_id;
				$userid = Auth::user()->user_id;
				$roleid = Auth::user()->role_id;
				$searchResult=array();
				
				$filterdata=$request->input('filterdata');
				$compid = Auth::user()->company_id;
				$userid = Auth::user()->user_id;
				$roleid = Auth::user()->role_id;
				$mode=$request->input('mode');
				$start=$request->input('start');
				$length=$request->input('length');
				$search = $request->input('search');
				$draw = $request->input('draw');
				$order = $request->input('order');
				$orderColumn=$order[0]['column'];
				$orderType=$order[0]['dir'];
				$searchtext=$search['value'];
		   

				$programs1=Trainingcatelogallplace::getTrainingCatalogAdvanceFilter($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$filterdata);

				
				$programsresult=Trainingcatelogallplace::return_html($userid,$programs1[0],$roleid,$compid,$catid='',$cattype='');	 
				
				return  $success=array("status"=>200,"data"=>$programsresult,"count"=>$programs1[1],"draw"=>$draw);
				
			
	}

	
}
