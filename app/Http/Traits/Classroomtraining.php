<?php
namespace App\Http\Traits;
use App\Models\ClassMaster;
use App\Models\ItemMaster;
use App\Models\UsertrainingCatalog;
use App\Models\TargetAudience;
use App\Models\ClassMaterialSettings;
use App\Models\HandoutsMaster;
use App\Models\CurriculamMaster;
use App\Models\Course;
use Mail,DB;

trait Classroomtraining{
	 
	 
		public static function checkClassMaster($compid,$class_id,$viewId)
		{
			
			$results=Course::join('class_master',function($join) use($class_id,$compid){
				$join->on('class_master.course_id','=','courses.course_id')
				->where('class_master.class_id','=',$class_id)
				->where('class_master.is_active','=',1)
				->where('class_master.status_id','=',1)
				->where('class_master.company_id','=',$compid);
			})
			->join('session_scedule',function($join){
				$join->on('session_scedule.class_id','=','class_master.class_id')
				  ->where('session_scedule.status_id',1);
				
			})
			->where('courses.training_program_code','=',$viewId)
			->havingRaw('min(session_scedule.dayid)>=curdate()')->first();
			return $results;
			
		}
		
	public static function getClassroomCourseForCourseDetails($userid, $compid,$class_id, $itemId)
	{
		$results=ItemMaster::join('item_master_trainingcategory',function($join) use($compid,$itemId){
				$join->on('item_master_trainingcategory.item_id','=','item_master.item_id')
				  ->where('item_master_trainingcategory.company_id',$compid)
				  ->where('item_master_trainingcategory.item_id',$itemId);
				
			})
			->join('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','item_master_trainingcategory.training_category_id')
				->where('training_category.company_id',$compid);
			})
			->join('class_master',function($join) use($compid,$itemId,$class_id){
				$join->on('class_master.item_id','=','item_master.item_id')
				  ->where('class_master.is_active',1)
				  ->where('class_master.class_id',$class_id)
				  ->where('class_master.company_id',$compid)
				  ->where('class_master.item_id',$itemId);
				
			})
			->where('item_master.item_id','=',$itemId)
			->where('item_master.is_active','=',1)
			->where('item_master.company_id','=',$compid)
			->select(DB::raw('class_master.class_id,class_master.class_name,item_master.item_id, item_master.item_name, item_master.item_type_id, item_master.training_type_id, item_master.delivery_type_id, item_master.description, item_master.prerequeisites, item_master.credit_value, item_master.item_cost, item_master.item_image, group_concat(training_category.category_name) as category_name'))
			->get();	
			
			return $results;
	}
	
	public static function allSessionsForClass($classid, $compid)
	{
		
		$result=ClassMaster::join('session_scedule',function($join) use($classid,$compid){
				$join->on('session_scedule.class_id','=','class_master.class_id')
				  ->where('session_scedule.status_id',1)
				  ->where('session_scedule.company_id',$compid)
				  ->where('session_scedule.class_id',$classid);
				
		})
		->leftjoin('user_master',function($join) use($compid){
				$join->on('user_master.user_id','=','session_scedule.instructor_id')
				  ->where('user_master.is_active',1)
				  ->where('user_master.company_id',$compid);
				
		})
		->leftjoin('class_timezones','class_timezones.class_timezone_id','=','session_scedule.time_zone')
			->where('class_master.class_id','=',$classid)
			->where('class_master.is_active','=',1)
			->where('class_master.status_id','=',1)
			->where('class_master.company_id','=',$compid)
			->select(DB::raw('dayid as enddate,dayid as startdate, session_scedule.location, session_scedule.start_time_hr, session_scedule.start_time_min, session_scedule.timeformat, session_scedule.time_zone, session_scedule.dayid, session_scedule.ses_length_min, session_scedule.ses_length_hr, class_timezones.timezone_name, user_master.user_name, class_master.maximum_seat, class_master.class_name, session_scedule.schedule_id'))->get();
			return $result;
	}
	
	public static function getUtcClassDetail($course_id,$userid,$compid,$classid){
		$res =UsertrainingCatalog::where('course_id', '=', $course_id)
			->where('user_id', '=', $userid)
			->where('company_id', '=' , $compid)
			->where('status_id','=',1)
			->where('class_id', '=',$classid)
			->get()->toArray();

		return $res;
	}
	
	public static function getTragetAudienceforCourse($item_id,$compid)
	{
		$itemresult=TargetAudience::leftjoin('job_title_group',function($join) use($compid){
				$join->on('job_title_group.job_id','=','target_audience.job_group_id')
				  ->where('job_title_group.is_active',1)
				  ->where('job_title_group.company_id',$compid);
				
		})
		->leftjoin('group_master',function($join) use($compid){
				$join->on('group_master.group_id','=','target_audience.job_group_id')
				  ->where('group_master.is_active',1)
				  ->where('group_master.is_delete',0)
				  ->where('group_master.company_id',$compid);
				
		})
		->where('target_audience.item_id','=',$item_id)
		->where('target_audience.job_group_id','!=',0)
		->where('target_audience.company_id','=',$compid)
		->where('target_audience.is_active','=',1)->select('target_audience.item_id','target_audience.type_name','group_master.group_name','job_title_group.jobtitle_group')->get();

		return $itemresult;
	}
	
	public static function getCourseMaterialSetting($item_id,$compid)
	{
		$itemresult =ClassMaterialSettings::where('item_id','=',$item_id)
		->where('company_id','=',$compid)->select('can_view_material',
		'can_view_handout','instructor_can_view_curriculum','user_can_view_curriculum','all_userr_can_view_curriculum')->first();
		return $itemresult;
	}
	
	public static function getClassHandouts($item_id,$compid)
	{
		$handouts =HandoutsMaster::where('course_id','=',$item_id)
		->where('company_id','=',$compid)->where('status','=',1)->get();
		return $handouts;
	}
	public static function getClassCurriculum($item_id,$compid)
	{
		$curriculum =CurriculamMaster::where('item_id','=',$item_id)
		->where('company_id','=',$compid)->where('status','=',1)->get();
		return $curriculum;
	}
	
		
		
	public static function getClassroomRequiewApproval($userid, $compid, $class_id)
	{
		$itemsql = "SELECT cm.maximum_seat, im.require_supervisor_aproval_status from training_program tp 
		join class_master cm on cm.class_id=:class_id1 and tp.item_id=cm.item_id and cm.company_id=:company_id1
		join item_master im on tp.item_id=im.item_id and im.company_id=:company_id2 and im.item_type_id!=4 
		where tp.company_id=:company_id3 and tp.class_id=:class_id2 and im.training_type_id in (2,3) and tp.is_active=1";
		$item_arr=array();
		$item_arr['class_id1']=$class_id;
		$item_arr['class_id2']=$class_id;
		$item_arr['company_id1']=$compid;
		$item_arr['company_id2']=$compid;
		$item_arr['company_id3']=$compid;
		$result = DB::select(DB::raw($itemsql) ,$item_arr);
    	return $result;
	}
	
	
	public static function getenrolledForClass2($classid, $compid) 
	{
		$select = "SELECT `user_training_catalog`.*, `U`.* FROM `user_training_catalog` INNER JOIN `user_master` AS `U` ON user_training_catalog.user_id = U.user_id and U.is_delete=0 and U.is_approved=1 and U.is_active=1 WHERE (user_training_catalog.company_id = :company_id1 ) AND (user_training_catalog.session_id = :class_id1 ) AND (user_training_catalog.is_enroll = 1) AND (user_training_catalog.training_status_id =3) GROUP BY `user_training_catalog`.`user_id`";
		$item_arr=array();
		$item_arr['class_id1']=$classid;
		$item_arr['company_id1']=$compid;
		$results = DB::select(DB::raw($select) ,$item_arr);
    	return $results;
	}
}
 