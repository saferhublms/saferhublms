<?php
namespace App\Http\Traits;
use Mail,DB,URL,CommonHelper;
use App\Models\UsertrainingCatalog;
use App\Models\UserTranscript;
use App\Models\TbluserlLink;

trait Usertrainingcatalogs{
	
	public static function getMyClassScheduleforinprogress($compId, $user_id){
    	$curdate = date('Y-m-d');
		$item_arr=array();
    	$sql ="SELECT cm.class_name as schedule_name,group_concat(tc.category_name) as category_name,im.training_code,im.description, cm.class_id,im.item_id, im.item_name,ty.training_type_id, im.delivery_type_id, im.published_date,im.credit_value,ty.training_type,im.description,
    	cm.virtual_session_information, tp.training_program_id,tp.training_title, utc.training_status_id, uts.user_status,
    	utc.user_training_id,cm.item_type_id,im.require_supervisor_aproval_status FROM user_training_catalog utc
		join item_master im on im.item_id = utc.item_id and im.company_id = :compId1
		join item_master_trainingcategory imt on imt.item_id =im.item_id and imt.is_active=1 and imt.company_id = :compId2
		join training_category tc on tc.training_category_id =imt.training_category_id and tc.is_active=1 and tc.company_id=:compId3 
		join class_master cm on cm.class_id = utc.session_id and cm.item_id = im.item_id and cm.company_id = :compId4 
		join user_training_status uts on uts.user_status_id = utc.training_status_id
		INNER JOIN training_type ty ON ty.training_type_id = cm.item_type_id
		join training_program tp on tp.item_id = im.item_id and tp.class_id = cm.class_id and tp.company_id = :compId5
		WHERE cm.is_active = 1 and (cm.item_type_id =2 or cm.item_type_id =3) and cm.start_date_class >= :curdate and utc.user_id = :user_id and utc.company_id = :compId6 and utc.training_status_id not in ('0', '1', '5') GROUP BY im.item_id,cm.class_id";
    	$item_arr['compId1'] =$compId;
    	$item_arr['compId2'] =$compId;
    	$item_arr['compId3'] =$compId;
    	$item_arr['compId4'] =$compId;
    	$item_arr['compId5'] =$compId;
    	$item_arr['compId6'] =$compId;
    	$item_arr['user_id'] =$user_id;
    	$item_arr['curdate'] =$curdate;
		$result=DB::select(DB::raw($sql) ,$item_arr);
    	return $result;
    }
	
	
	public static function getTrainingCatalogData($course_id,$userid,$class_id,$company_id)
	{
		$result = UsertrainingCatalog::where('course_id','=',$course_id)->where('user_id','=',$userid)->where('company_id','=',$company_id)->where('class_id','=',$class_id)->where('status_id','=',1)->get();
		return $result;
	}
	
	
	 public static function getElearningInProgressofUsers($userid, $compid)
	{
    	$item_arr=array();
    	$sql = "SELECT DISTINCT utg.user_training_id, group_concat(tc.category_name) as category_name, im.training_code, ty.training_type, utg.user_program_type_id, utg.program_id, tp.training_program_id, utg.item_id, utg.learning_plan_id, utg.blended_program_id, utg.assignment_id, utg.is_approved, utg.is_enroll, utg.user_id, utg.company_id, utg.session_id, utg.training_status_id, utg.is_active, utg.created_date, tp.training_type_id, tp.class_id, tp.training_title, tp.description training_desc, tp.published_date, im.item_name, im.item_type_id, imt.training_category_id, im.title, im.description, im.credit_value, im.item_cost, im.require_supervisor_aproval_status
		FROM user_training_catalog utg
		INNER JOIN training_program tp ON utg.program_id = tp.training_program_id and tp.company_id=:compId1
		INNER JOIN item_master im ON utg.item_id = im.item_id and im.company_id=:compId2
		INNER JOIN training_type ty ON ty.training_type_id = im.training_type_id
		LEFT JOIN item_master_trainingcategory imt ON im.item_id = imt.item_id and imt.company_id=:compId5
		join training_category tc on tc.training_category_id =imt.training_category_id and tc.is_active=1 and tc.company_id=:compId3
		WHERE (utg.is_enroll = 1) AND (im.is_active = 1) AND (utg.training_status_id!= 1) AND (tp.training_type_id = 1) AND (utg.company_id = :compId4) AND (utg.user_id = :userid)
		GROUP BY im.item_id";
		$item_arr['compId1'] =$compid;
    	$item_arr['compId2'] =$compid;
    	$item_arr['compId3'] =$compid;
    	$item_arr['compId4'] =$compid;
    	$item_arr['compId5'] =$compid;
    	$item_arr['userid']  =$userid;
    	$result = DB::select(DB::raw($sql) ,$item_arr);
    	return $result;
    }
	
	
	public static function getblendedProgressData($userid,$company_id)
	{
	    $item_arr=array();
		$sql = "select utc.*,bpi.title,tpp.training_type,group_concat(tc.category_name) as category_name from user_training_catalog utc
		join blended_program_items bpi on bpi.blended_item_id =utc.blended_program_id and bpi.is_active=1 
		join blended_program_training_category btc on btc.blended_program_id = bpi.blended_item_id
		join training_category tc on  btc.training_category_id = tc.training_category_id and tc.is_active=1 and tc.is_delete=0 and tc.company_id='$company_id'
		join training_type tpp on tpp.training_type_id=utc.training_type_id
		where utc.company_id='$company_id' and bpi.company_id='$company_id' and utc.is_active=1 and utc.user_id='$userid' and utc.training_type_id=4 and utc.training_status_id=3 group by  btc.blended_program_id";
		$item_arr['company_id1'] =$company_id;
		$item_arr['company_id2'] =$company_id;
		$item_arr['company_id3'] =$company_id;
		$item_arr['userid'] =$userid;
		$result = DB::select(DB::raw($sql) ,$item_arr);
    	return $result;
	}
	
	public static function getCompletionOfBlended($item_ids, $compid, $userid) 
	{
		$item = array();
		$sql = "select item_id from user_transcript where item_id in ('".$item_ids."') and user_id=:userid and company_id=:compid and is_active=1 group by item_id"; 
		$item['compid'] = $compid;
		
		$item['userid'] = $userid;
		$result = DB::select(DB::raw($sql) ,$item);
		
		return $result;
		
	}
	
	
	public static function getCompletionOfassessmentBlended($item_ids, $compid, $userid) 
	{
		$result = UserTranscript::whereIn('registration_id',$item_ids)->where('user_id','=',$userid)->where('company_id','=',$compid)->where('is_blended','=',0)->get();
		return $result;

	}
	
	
	
	public static function getMyClassScheduleforBlendedDetails($compId, $user_id ,$item_id)
	{
        $item_array = array();
        $result = array();
    	$itemsql ="SELECT utc.training_status_id,cm.class_name as schedule_name,group_concat(tc.category_name) as category_name,im.training_code,ty.training_type,im.description, cs.class_schedule_id, cs.class_id,im.item_id, im.item_name,
		im.training_type_id, im.delivery_type_id, im.published_date,im.credit_value,ty.training_type as itemtype,
		ss.dayid as recursive_date, ss.ses_length_hr, ss.ses_length_min, ss.timeformat, ss.location, ss.start_time_hr, ss.start_time_min,im.description,
    	cm.virtual_session_information, tp.training_program_id,tp.training_title, utc.training_status_id, uts.user_status,
    	utc.user_training_id,cm.item_type_id,im.require_supervisor_aproval_status FROM class_schedule cs
		join class_master cm on cm.class_id = cs.class_id and cm.company_id = :compId1
		join item_master im on im.item_id = cm.item_id 
		join item_master_trainingcategory imt on imt.item_id =im.item_id and imt.is_active=1 and imt.company_id = :compId2
		join training_category tc on tc.training_category_id =imt.training_category_id and tc.is_active=1 and tc.company_id=:compId3 
		join user_training_catalog utc on utc.item_id = cm.item_id and utc.session_id = cm.class_id and utc.company_id = :compId4
		join user_training_status uts on uts.user_status_id = utc.training_status_id
		INNER JOIN training_type ty ON ty.training_type_id = im.training_type_id
		join training_program tp on tp.item_id = im.item_id and tp.class_id = cm.class_id and tp.company_id = :compId5
		JOIN (SELECT min(ssd.dayid) as dayid, ssd.class_id, ssd.ses_length_hr, ssd.ses_length_min, ssd.timeformat, ssd.location, ssd.start_time_hr, ssd.start_time_min from session_scedule ssd where ssd.company_id = :compId6 and ssd.is_active = 1 
		group by ssd.class_id) ss ON cs.class_id = ss.class_id
		WHERE im.company_id = :compId7 and im.item_id=:item_id AND cm.is_active = 1 AND cs.is_deleted = 0 and cs.is_active = 1 and cs.company_id = :compId8 and 
		(cm.item_type_id =2 or cm.item_type_id =3) and utc.user_id = :user_id  GROUP BY im.item_id,cm.class_id";
		$item_array['compId1'] = $compId;
		$item_array['compId2'] = $compId;
		$item_array['compId3'] = $compId;
		$item_array['compId4'] = $compId;
		$item_array['compId5'] = $compId;
		$item_array['compId6'] = $compId;
		$item_array['compId7'] = $compId;
		$item_array['compId8'] = $compId;
		$item_array['user_id'] = $user_id;
		$item_array['item_id'] = $item_id;
		
    	$itemresult= DB::select(DB::raw($itemsql) ,$item_array);
		foreach($itemresult as $getdata){
			 $result[] = (array)$getdata; 
		}
		
		 return $result;
    	
    }
	
	
	public static function deleteuserenrolledprograms($userid, $usertrainingprogramid)
	{
		 $data = array('is_enroll' => 0,	'is_active' => 0, 'last_modfied_by' => $userid, 'last_modified_time' => date('Y-m-d H:i:s'));
		 UsertrainingCatalog::where('user_training_id','=',$usertrainingprogramid)->update($data);
		
	}
	
	
	public static function insertusertrascript($data){
		$catalog = UserTranscript::create($data);
		return $catalog->transcript_id;
	}
	
	
	public static function insertusertrainigcatalog($data){
		UsertrainingCatalog::create($data);
	}
	
	public static function inserttbluserlink($data){
		TbluserlLink::create($data);
	}
	
	public static function dropblendeditem($blendedprogram_id,$company_id)
	{
		$result = UsertrainingCatalog::where('blended_program_id','=',$blendedprogram_id)->where('company_id','=',$company_id)->delete();
		
	
	}
	
	public static function checkBlendedEntry($blendedprogid,$compid) {
		
		 $results = UsertrainingCatalog::where('blended_program_id','=',$blendedprogid)->where('company_id','=',$compid)->select('user_training_id')->get();
    	return $results;
    }
	
	
	public static function fetchall($userid,$compid,$classid,$course_id){
		
		$data = UsertrainingCatalog::where('user_id','=',$userid)->where('company_id','=',$compid)->where('class_id','=',$classid);
		 if($course_id == ''){
			$result = $data->get()->toArray();  
		 }else{
			 $result = $data->where('course_id','=',$course_id)->get()->toArray();  
		 }
		
		return $result;
	}
	
	public static function updateanywhere($datau,$userid,$compid,$class_id,$ustid){
		UsertrainingCatalog::where('user_id','=',$userid)->where('company_id','=',$compid)->where('class_id','=',$class_id)->where('user_training_id','=',$ustid)->update($datau);
	}
	
	public static function getUserInWaitinglist($classid,$compid){
		$results = UsertrainingCatalog::where('class_id','=',$classid)->where('training_status_id','=',4)->where('company_id','=',$compid)->orderBy('user_training_id')->get()->toArray();
		return $results;
	
    }
	
	 public static function getenrolledForClass2($classid, $compid) {
		$item_array = array();
		$select = "SELECT `user_training_catalog`.*, `U`.* FROM `user_training_catalog` INNER JOIN `user_master` AS `U` ON user_training_catalog.user_id = U.user_id and U.is_delete=0 and U.is_approved=1 and U.is_active=1 WHERE (user_training_catalog.company_id = :compid) AND (user_training_catalog.class_id = :classid) AND (user_training_catalog.is_enroll = 1) AND (user_training_catalog.training_status_id =3) GROUP BY `user_training_catalog`.`user_id`";
		$itme_array['compid'] = $compid; 
		$itme_array['classid'] = $classid; 
		$results = DB::select(DB::raw($select),$itme_array);
		return $results;
	}
	
	public static function getenrolleduserssforroster($compid, $itemid, $classid)
	{
		    $item_array = array();
		    $sqlcatalog = "select um.user_id,um.login_id, um.first_name, um.last_name, um.user_name, um.email_id,um.area,um.location, um.division_name, group_concat(jtg.jobtitle_group) as jobTitles ,
			utc.created_date as createdDate ,cm.class_name as schedule_name, uts.user_status, utc.training_status_id,utc.is_enroll,utc.is_approved,utc.user_training_id,
			cm.class_id as session_id,im.item_id from user_training_catalog utc
			join user_training_status uts on uts.user_status_id=utc.training_status_id
			join item_master im on im.item_id = utc.item_id and im.is_active=1
			join class_master cm on cm.class_id = utc.session_id and cm.is_active = 1
			join user_master um on um.user_id = utc.user_id
			left join user_jobtitle ujt on ujt.user_id=um.user_id and ujt.is_active = 1 
			left join job_title_group jtg on jtg.job_id=ujt.job_id and jtg.is_active = 1
			where utc.session_id = :classid and utc.item_id = :itemid and utc.company_id = :compid 
			 and um.is_active = 1 and um.is_delete = 0 and utc.is_active = 1 
			group by utc.user_id,utc.item_id,utc.session_id order by um.user_name";

			$item_array['classid'] = $classid; 
			$item_array['compid'] = $compid; 
			$item_array['itemid'] = $itemid; 
			$results = DB::select(DB::raw($sqlcatalog),$item_array);
		  return $results; 
    }
	
	public static function getTrainingCatalogId($usertrainingid,$userid,$compid){
		$userlearningplan_query=UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->where('user_id','=',$userid)->where('company_id','=',$compid)->get();
		$userlearningplan_res=array();
		if(count($userlearningplan_query)!=0){

				$userlearningplan_res=$userlearningplan_query->toArray();
		}
		return $userlearningplan_res;
	}
	
	public static function getTrainingCatalogbyUser($userid,$compid,$course_id){
		$userlearningplan_query=UsertrainingCatalog::where('user_id','=',$userid)->where('course_id','=',$course_id)->where('status_id','=',1)->where('company_id','=',$compid)->get();
		$userlearningplan_res=array();
		if(count($userlearningplan_query)!=0){

				$userlearningplan_res=$userlearningplan_query->toArray();
		}
		return $userlearningplan_res;
	}
	
}
