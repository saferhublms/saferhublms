<?php
namespace App\Http\Traits;

use App\Models\CompanyCmsLearningPlan;
use App\Models\CompanyCmsInfo;
use App\Models\CompanyCmsTrainingcatalog;

trait Contentmanagement{
 
	   public static function fetchcmslearningplaninfo($compid)
	   {
		   $result=CompanyCmsLearningPlan::wherecompanyId($compid)->whereisActive(1)->get();
		   return $result;
	   }
	   
	   public static function fetchcompanyinfo($compid) 
	   {
		   $result=CompanyCmsInfo::wherecompanyId($compid)->whereisActive(1)->get();
		   if(count($result)==0){
			$result[0]->company_name='';
			$result[0]->company_logo='';
			$result[0]->lms_content='';
			$result[0]->company_address2='';
			$result[0]->company_address1='';
			$result[0]->map_direction='';
			$result[0]->last_updated_by='';
			$result[0]->created_date='';
			$result[0]->is_active='';
			$result[0]->header_option='';
			$result[0]->allow_expand_width='';
			$result[0]->home_page_html='';
			$result[0]->company_date_formate='';
			$result[0]->logo_setting='';
		}
		
		return $result;
	   }
	   
	   public static function fetchcmstrainingcataloginfo($compid) 
	   {
		   $result=CompanyCmsTrainingcatalog::wherecompanyId($compid)->whereisActive(1)->get();
			
			return $result;
	   }


}