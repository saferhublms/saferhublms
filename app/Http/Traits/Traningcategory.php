<?php
namespace App\Http\Traits;
use App\Http\Traits\Trainingprogram;
use App\Models\TrainingCategory;
use DB;

trait Traningcategory{
	
	public static function trainingcategoryforuser($compid,$subAdminId,$roleid)
	{
		$ta_arr=array();
		$sql = "select tc.* from training_category tc";
		if($roleid == 2)
		{
		
			$userrolearray = Trainingprogram::getSupervisorSupervisorId($compid,$subAdminId);
			$subAdminId = $userrolearray[0];
			$roleid = $userrolearray[1];
		}
		if($roleid != 1)
		{
			$sql .= " join sub_admin_assigned_category saac on saac.`category_id` = tc.`training_category_id`
			where saac.`user_id` =:user_id ";
			$whr = ' and ';
			$ta_arr['user_id']=$subAdminId;
		}
		else
		{
			$whr = ' where ';
			$sql .= " $whr tc.is_delete=0 and tc.is_active=1 and tc.company_id =:company_id order by tc.category_name asc";
			$ta_arr['company_id']=$compid;
		}
		$traningcategoryType = DB::select(DB::raw($sql),$ta_arr);
		$traningcategoryType = json_decode(json_encode($traningcategoryType),true);
		return $traningcategoryType;
	}
	
	public static function getTrainingCategory($compid,$subAdminId,$roleid)
	{
		$traningcategory_arr = Traningcategory::trainingcategorylist($compid,$subAdminId,$roleid);
		
		/* foreach($traningcategoryArray as $row)
		{
	        $id = $row->training_category_id;
			$category_name = $row->category_name;
			array_push($traningcategory_arr, array("id"=>$id,"text"=>$category_name));
		} */
		return $traningcategory_arr;
		
	}
	
	public static function trainingcategorylist($compid,$subAdminId,$roleid)
	{
		if($roleid != 1)
		{
			$result=TrainingCategory::join('sub_admin_assigned_category as saac','saac.category_id','=','training_category.training_category_id')
			->where('saac.user_id',$subAdminId)
			->get();
		}else{
			$result=TrainingCategory::where('is_delete','=',0)->where('is_active','=',1)->where('company_id','=',$compid)->select('training_category_id','category_name')->orderBy('category_name')->get();
		}
		return $result;
	}
	

	
	
	
	
}
