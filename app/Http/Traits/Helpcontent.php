<?php
namespace App\Http\Traits;
use DB;
use App\Models\RefDocuments;

trait Helpcontent{
 
	public static function getAlltopicsList($company_id,$roleid)
	{
		$sql="SELECT distinct ht.topic_name,ht.topic_id FROM help_topics AS ht 
		join help_cat hc on hc.topic_id = ht.topic_id and hc.is_active = 1 AND hc.company_id=:company_id1
		join help_faq hf on hf.faq_id = hc.faq_id and hf.is_active = 1  AND hf.company_id=:company_id2
		join help_role as hr on hf.faq_id=hr.faq_id and hr.is_active=1 and hr.company_id=:company_id3
		WHERE ht.is_active=1 AND ht.company_id=:company_id4 and hr.role_id=:role_id ORDER BY ht.manage_order";
		$param=array();
		$param['company_id1']	=$company_id;
		$param['company_id2']	=$company_id;
		$param['company_id3']	=$company_id;
		$param['company_id4']	=$company_id;
		$param['role_id']		=$roleid;
		$result=DB::select(DB::raw($sql),$param);
		return $result;
	}
	
	public static function getTopicQuestion($topic_id,$company_id,$roleid)
	{
		
		$sql="select hf.faq_question,hf.faq_answer,hf.faq_id,hr.role_id,ht.topic_name,ht.topic_id from help_faq as hf
		join help_role as hr on hf.faq_id=hr.faq_id and hr.is_active=1 and hr.company_id=:company_id1
		join help_cat as hc on hf.faq_id=hc.faq_id and hc.is_active=1 and hc.company_id=:company_id2
		join help_topics as ht on ht.topic_id=hc.topic_id and ht.company_id=:company_id3
		where hr.is_active=1 and hr.role_id=:role_id and ht.topic_id=:topic_id and hf.is_active =1
		group by hf.faq_question order by hc.manage_order asc";
		$param=array();
		$param['company_id1']	=$company_id;
		$param['company_id2']	=$company_id;
		$param['company_id3']	=$company_id;
		$param['role_id']		=$roleid;
		$param['topic_id']		=$topic_id;
		$result=DB::select(DB::raw($sql),$param);

		$topic_arr = array();		
		foreach($result as $row)
		{
			$id = $row->faq_id;
			$faq_question = $row->faq_question;
			$faq_answer = $row->faq_answer;
			array_push($topic_arr, array("qid"=>$id,"question"=>$faq_question,"answer"=>$faq_answer));
		}
		return $topic_arr;
	}
    
	public static function getAssignReferences($userid,$company_id,$roleid)
    {
					
		if($roleid!=1)
		{
			$result=RefDocuments::leftjoin('ref_target_audience',function($join) use($company_id){
			$join->on('ref_target_audience.ref_document_id','=','ref_documents.ref_document_id')
			->where('ref_target_audience.company_id','=',$company_id)
			->where('ref_target_audience.is_active',1);
			})
			->where('ref_documents.company_id','=',$company_id)->where('ref_documents.is_active',1)->where('ref_documents.status',1)->orderby('ref_documents.manage_order','asc')
			->select('ref_documents.ref_document_id','ref_documents.title','ref_target_audience.job_id','ref_target_audience.type')->get()->toArray();
		
		}
		else
		{
			
			
		  $result=RefDocuments::where('company_id','=',$company_id)->where('is_active',1)->where('status',1)->orderby('ref_documents.manage_order','asc')->select(DB::raw(' ref_document_id,title,0 as job_id,"" as type'))->get()->toArray();;
			

		}	
		
		$ref_result=array();
		$ref_Arr=array();
		if(count($result))
		{
		    foreach($result as $key => $value)
			{
			    $type= $value['type'];
				$document_id= $value['ref_document_id'];
			    if($type=='job')
				{
					
					$res=RefDocuments::leftjoin('ref_doc_category',function($join) use($company_id){
					$join->on('ref_doc_category.ref_document_id','=','ref_documents.ref_document_id')
					->where('ref_doc_category.company_id','=',$company_id)
					->where('ref_doc_category.is_active',1);
					})
					->join('training_category' ,function($join) use($company_id){
						$join->on('training_category.training_category_id','=','ref_doc_category.category_id')
								->where('training_category.company_id','=',$company_id)
								->where('training_category.is_active',1)
								->where('training_category.is_delete',0);
					})
					->join('ref_target_audience',function($join) use($company_id){
					$join->on('ref_target_audience.ref_document_id','=','ref_documents.ref_document_id')
					->where('ref_target_audience.company_id','=',$company_id)
					->where('ref_target_audience.is_active',1);
					})
					->join('user_jobtitle',function($join) use($userid){
						$join->on('user_jobtitle.job_id','=','ref_target_audience.job_id')
						->where('user_jobtitle.user_id','=',$userid)
						->where('user_jobtitle.is_active','=',1);
					})
					->join('job_title_group',function($join) use($company_id){
						$join->on('job_title_group.job_id','=','user_jobtitle.job_id')
						->where('job_title_group.is_active',1)
						->where('job_title_group.company_id','=',$company_id);
					})
					->leftjoin('ref_uploaded_file',function($join) use($company_id){
						$join->on('ref_uploaded_file.refdoc_file_id','=','ref_documents.ref_doc_id')
						->where('ref_uploaded_file.company_id','=',$company_id);
					})
					->where('ref_documents.company_id','=',$company_id)->where('ref_documents.is_active',1)->where('ref_documents.status',1)->where('ref_documents.ref_document_id','=',$document_id)->groupby('ref_documents.title')->orderby('ref_documents.manage_order','asc')
					->select(DB::raw('ref_documents.title,group_concat(training_category.category_name) as  category_name,
					ref_uploaded_file.file_name,ref_uploaded_file.file_path,ref_uploaded_file.media_id,ref_documents.allow_download,ref_documents.company_id,ref_documents.ref_document_id'))->get()->toArray();
				
					if(count($res)>0)
					{
						 if(!in_array($res[0]['ref_document_id'],$ref_Arr))
						 {
							$ref_Arr[]    = $res[0]['ref_document_id'];
							$ref_result[] = $res[0];
						 }
					}
				}
				else if($type=='group')
				{
					$res=RefDocuments::leftjoin('ref_doc_category',function($join) use($company_id){
					$join->on('ref_doc_category.ref_document_id','=','ref_documents.ref_document_id')
					->where('ref_doc_category.company_id','=',$company_id)
					->where('ref_doc_category.is_active',1);
					})
					->join('training_category' ,function($join) use($company_id){
						$join->on('training_category.training_category_id','=','ref_doc_category.category_id')
								->where('training_category.company_id','=',$company_id)
								->where('training_category.is_active',1)
								->where('training_category.is_delete',0);
					})
					->join('ref_target_audience',function($join) use($company_id){
					$join->on('ref_target_audience.ref_document_id','=','ref_documents.ref_document_id')
					->where('ref_target_audience.company_id','=',$company_id)
					->where('ref_target_audience.is_active',1);
					})
					->join('user_group',function($join) use($userid){
						$join->on('user_group.group_id','=','ref_target_audience.job_id')
						->where('user_group.user_id','=',$userid)
						->where('user_group.is_active','=',1);
					})
					->join('group_master',function($join) use($company_id){
						$join->on('group_master.group_id','=','user_group.group_id')
						->where('group_master.is_active',1)
						->where('group_master.company_id','=',$company_id);
					})
					->leftjoin('ref_uploaded_file',function($join) use($company_id){
						$join->on('ref_uploaded_file.refdoc_file_id','=','ref_documents.ref_doc_id')
						->where('ref_uploaded_file.company_id','=',$company_id);
					})
					->where('ref_documents.company_id','=',$company_id)->where('ref_documents.is_active',1)->where('ref_documents.status',1)->where('ref_documents.ref_document_id','=',$document_id)->groupby('ref_documents.title')->orderby('ref_documents.manage_order','asc')
					->select(DB::raw('ref_documents.title,group_concat(training_category.category_name) as  category_name,
					ref_uploaded_file.file_name,ref_uploaded_file.file_path,ref_uploaded_file.media_id,ref_documents.allow_download,ref_documents.company_id,ref_documents.ref_document_id'))->get()->toArray();
					
					
					if(count($res)>0)
					{
						 if(!in_array($res[0]['ref_document_id'],$ref_Arr))
						 {
							$ref_Arr[]    = $res[0]['ref_document_id'];
							$ref_result[] = $res[0];
						 }
					}
				}
				else
				{
					
					$res=RefDocuments::leftjoin('ref_doc_category',function($join) use($company_id){
					$join->on('ref_doc_category.ref_document_id','=','ref_documents.ref_document_id')
					->where('ref_doc_category.company_id','=',$company_id)
					->where('ref_doc_category.is_active',1);
					})
					->join('training_category' ,function($join) use($company_id){
						$join->on('training_category.training_category_id','=','ref_doc_category.category_id')
								->where('training_category.company_id','=',$company_id)
								->where('training_category.is_active',1)
								->where('training_category.is_delete',0);
					})
					
					->leftjoin('ref_uploaded_file',function($join) use($company_id){
						$join->on('ref_uploaded_file.refdoc_file_id','=','ref_documents.ref_doc_id')
						->where('ref_uploaded_file.company_id','=',$company_id);
					})
					->where('ref_documents.company_id','=',$company_id)->where('ref_documents.is_active',1)->where('ref_documents.status',1)->where('ref_documents.ref_document_id','=',$document_id)->groupby('ref_documents.title')->orderby('ref_documents.manage_order','asc')
					->select(DB::raw('ref_documents.title,group_concat(training_category.category_name) as  category_name,ref_uploaded_file.file_name,ref_uploaded_file.file_path,ref_uploaded_file.media_id,ref_documents.allow_download,ref_documents.company_id,ref_documents.ref_document_id'))->get()->toArray();
					
					if(count($res)>0)
					{
						if(!in_array($res[0]['ref_document_id'],$ref_Arr))
						{
							$ref_Arr[]    = $res[0]['ref_document_id'];
							$ref_result[] = $res[0];
						}
					}
				}		
			}		
		}
		
		return $ref_result;
    }
	
	public static function getmediatype($editreferenceid, $compid)
	{
		$result=RefDocuments::leftjoin('ref_uploaded_file',function($join) use($compid){
			$join->on('ref_uploaded_file.refdoc_file_id','=','ref_documents.ref_doc_id')
			->where('ref_uploaded_file.company_id','=',$compid);
		})
		->where('ref_documents.ref_document_id','=',$editreferenceid)
		->where('ref_documents.company_id','=',$compid)
		->select('ref_documents.ref_doc_id','ref_uploaded_file.file_name','ref_uploaded_file.media_id','ref_uploaded_file.embedded_code')->get()->toArray();
		return $result;
	}
		  
   public static function getreferencetoedit($editreferenceid,$company_id)
    {
	     $result=RefDocuments::where('ref_document_id','=',$editreferenceid)->where('company_id','=',$company_id)->first();
		 return $result;
    }
	
	public static function getreferencecategory($editreferenceid, $company_id)
	{
		$result=RefDocuments::join('ref_doc_category',function($join) use($company_id){
		$join->on('ref_doc_category.ref_document_id','=','ref_documents.ref_document_id')
		->where('ref_doc_category.company_id','=',$company_id)
		->where('ref_doc_category.is_active',1);
		})
		->join('training_category' ,function($join) use($company_id){
			$join->on('training_category.training_category_id','=','ref_doc_category.category_id')
					->where('training_category.company_id','=',$company_id)
					->where('training_category.is_active',1)
					->where('training_category.is_delete',0);
		})
		->where('ref_doc_category.ref_document_id','=',$editreferenceid)->where('ref_doc_category.company_id','=',$company_id)->where('ref_doc_category.is_active',1)->select(DB::raw('GROUP_CONCAT(category_name) as category_name'))->first();
		return $result;
	}
}