<?php
namespace App\Http\Traits;
use DB;
use App\Models\ClassMaster;
use App\Models\UserMaster;
use App\Http\Traits\Admin;
use App\Models\TrainingPrograms;
use App\Models\Course;
use App\Models\GroupMaster;
use App\Models\CourseTargetAudience;
use App\Models\UploadedFile;
use App\Models\UserTranscript;
use App\Models\UsertrainingCatalog;
use App\Models\UserGroup;
use Carbon;
trait Trainingprogram{
	
	public static function getTrainingCatalogsElearning($compid, $progid) 
	{
		$item_arr = array();
		$sql="SELECT tp.*,tt.training_type,im.* FROM training_program AS tp
		JOIN item_master AS im ON tp.item_id=im.item_id AND im.is_active=1 AND im.company_id=:compId AND im.training_type_id=1 
		JOIN training_type AS tt ON im.training_type_id =tt.training_type_id AND tt.is_active=1 
		WHERE tp.company_id=:compId2 AND tp.training_program_id=:progid and tp.is_active=1";
		$item_arr['compId'] =$compid;
    	$item_arr['compId2'] =$compid;
    	$item_arr['progid'] =$progid;
		$result = DB::select(DB::raw($sql) ,$item_arr);
    	return $result;
	}
	
	public static function getLatestClass($compid,$item_id)
	{
		$result = ClassMaster::where('item_id','=',$item_id)->where('is_active','=',1)->where('company_id','=',$compid)->where('start_date_class','>=', date('Y-m-d H:i:s'))->groupby('class_id')->select('start_date_class','class_id')->first();
		return $result;
	}
	
	
	public static function getClassroomCourseDetails($userid, $compid, $class_id, $courseId)
	{
			$lang_id=1;
			$querydata=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
			})
			->join('training_type','training_type.training_type_id','=','training_programs.training_type')
			->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
			})
			->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
			})
			->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
			})
			->join('class_master',function($join) use($compid){
				$join->on('class_master.course_id','=','courses.course_id')
				->where('class_master.company_id','=',$compid)
				->where('class_master.status_id','=',1);
			})
			->join('class_schedule',function($join) use($compid){
				$join->on('class_schedule.class_id','=','class_master.class_id')
				->where('class_schedule.company_id','=',$compid);
			})
			->leftjoin('course_settings',function($join) {
				$join->on('course_settings.course_id','=','courses.course_id');
			})
			->join(DB::raw('(SELECT min(ssd.dayid) as dayid,max(ssd.dayid) as last_schedule_date, ssd.class_id from session_scedule ssd  where ssd.company_id = '.$compid.' and ssd.status_id=1  group by ssd.class_id)
			   ss'), 
			function($join)
			{
			$join->on('class_master.class_id', '=', 'ss.class_id');
			})
			->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)->where('class_master.class_id','=',$class_id)
			->where('courses.course_id','=',$courseId)
			->select(DB::raw('training_programs.training_type as training_type_id,course_details.course_name,course_details.course_title,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type, ss.dayid as class_date, ss.last_schedule_date,class_master.class_name,class_master.class_id,course_settings.enrollment_type as require_supervisor_aproval_status,course_settings.is_unenrollment,class_master.maximum_seat as max_seat,class_master.delivery_type_id,class_master.start_date_class,GROUP_CONCAT(distinct training_category.category_name) as category_name,course_details.course_description,course_details.prerequeisites,class_master.virtual_session_information,courses.certificate_template_id'))->get();
			$itemresult = json_decode(json_encode($querydata),true);
			return $itemresult;
			//echo '<pre>'; print_r(); exit;
			//$res = DB::select(DB::raw($itemsql),$item_array);
			/* if(!count($res))
			{
				$coursedetails = Trainingprogram::getClassroomCourseForCourseDetails($userid, $compid, $class_id, $itemId);
				$start_date_class = '';
				$coursedetails[0]['class_id'] 						= '';
				$coursedetails[0]['class_name'] 					= $coursedetails[0]['item_name'];
				$coursedetails[0]['item_id'] 						= $itemId;
				$coursedetails[0]['max_seat']  						= '';
				$coursedetails[0]['current_enroll'] 				= '';
				$coursedetails[0]['virtual_session_information'] 	= '';			
				$coursedetails[0]['last_schedule_date'] 			= '';	
				$coursedetails[0]['require_supervisor_aproval_status'] = '';		
				$coursedetails[0]['certificate_template_id'] 		= '';			
				$coursedetails[0]['training_program_id']			= '';
				return $coursedetails;
			}
			else
			{
				$itemresult = json_decode(json_encode($res),true);
				return $itemresult;
			} */
		/* $item_array = array();
		$itemsql = "SELECT cs.publish_date, cs.schedule_name, cm.class_name, cm.start_date_class, cm.endate_date, cm.maximum_seat, cm.current_enroll,	cm.virtual_session_information, cm.is_email_checked, cm.is_automatic_require_post_class_evaluation, cm.is_require_response_for_credit, cm.evaluation_pref_send_same_day, cs.class_schedule_id, cm.class_id, cs.max_seat, cs.expiry_date, im.item_id, im.item_name, im.item_type_id, im.training_type_id, im.delivery_type_id, im.description, im.prerequeisites, im.credit_value, im.item_cost, im.item_image, tp.company_id, tp.training_program_id, im.published_date, cs.is_deleted, ty.training_type as itemtype, ss.dayid as class_date, ss.last_schedule_date, ss.time_zone, ss.start_time_hr, ss.start_time_min, ss.ses_length_hr, ss.ses_length_min, ss.timeformat, ss.instructor_id, ss.location, group_concat(distinct tc.category_name) as category_name, im.certificate_template_id, im.require_supervisor_aproval_status FROM item_master im
		join class_master cm on cm.item_id=:item_id1 and cm.is_active=1 and cm.class_id=:class_id1 and cm.status_id!=0 and cm.company_id=:compid1
		join class_schedule cs on cs.class_id=:class_id2 and cs.is_deleted=0 and cs.company_id=:compid2 and cs.is_active=1
		left join training_program tp on tp.item_id=:item_id2 and tp.class_id=:class_id3 and tp.company_id=:compid3 and tp.is_active=1
		join training_type ty ON ty.training_type_id=im.training_type_id and ty.is_active=1
		join item_master_trainingcategory imtc on imtc.item_id=:item_id3 and imtc.company_id=:compid4 and imtc.is_active=1
		join training_category tc on tc.training_category_id=imtc.training_category_id and tc.company_id=:compid5 and tc.is_active=1 and tc.is_delete=0
		join (SELECT min(ssd.dayid) as dayid,max(ssd.dayid) as last_schedule_date, ssd.time_zone, ssd.start_time_hr, ssd.start_time_min, ssd.ses_length_hr, ssd.ses_length_min, ssd.timeformat, ssd.instructor_id, ssd.location, ssd.class_id from session_scedule ssd where ssd.company_id=:compid6 group by ssd.class_id) ss ON cs.class_id=ss.class_id
		where im.item_id=:item_id4 and im.company_id=:compid7 and im.item_type_id!=4 and im.training_type_id in (2,3)";
		$item_array['item_id1'] = $itemId;
		$item_array['item_id2'] = $itemId;
		$item_array['item_id3'] = $itemId;
		$item_array['item_id4'] = $itemId;
		$item_array['class_id1'] = $class_id;
		$item_array['class_id2'] = $class_id;
		$item_array['class_id3'] = $class_id;
		$item_array['compid1'] = $compid;
		$item_array['compid2'] = $compid;
		$item_array['compid3'] = $compid;
		$item_array['compid4'] = $compid;
		$item_array['compid5'] = $compid;
		$item_array['compid6'] = $compid;
		$item_array['compid7'] = $compid;
		
		$res = DB::select(DB::raw($itemsql),$item_array);
		if(!count($res))
		{
			$coursedetails = Trainingprogram::getClassroomCourseForCourseDetails($userid, $compid, $class_id, $itemId);
			$start_date_class = '';
			$coursedetails[0]['class_id'] 						= '';
			$coursedetails[0]['class_name'] 					= $coursedetails[0]['item_name'];
			$coursedetails[0]['item_id'] 						= $itemId;
			$coursedetails[0]['max_seat']  						= '';
			$coursedetails[0]['current_enroll'] 				= '';
			$coursedetails[0]['virtual_session_information'] 	= '';			
			$coursedetails[0]['last_schedule_date'] 			= '';	
			$coursedetails[0]['require_supervisor_aproval_status'] = '';		
 			$coursedetails[0]['certificate_template_id'] 		= '';			
			$coursedetails[0]['training_program_id']			= '';
			return $coursedetails;
		}
		else
		{
			$itemresult = json_decode(json_encode($res),true);
			return $itemresult;
		} */
	}
	
	
	public static function getClassroomCourseForCourseDetails($userid, $compid, $class_id, $itemId)
	{
		$item_array = array();
		$itemsql = "select im.item_id, im.item_name, im.item_type_id, im.training_type_id, im.delivery_type_id, im.description, im.prerequeisites, im.credit_value, im.item_cost, im.item_image, group_concat(tc.category_name) as category_name from item_master im 
		join item_master_trainingcategory imtc on imtc.item_id='$itemId' and imtc.company_id='$compid'
		join training_category tc on tc.training_category_id=imtc.training_category_id and tc.company_id='$compid'
		where im.item_id='$itemId' and im.is_active=1 and im.company_id='$compid'";
		$item_array['userid'] = $userid;
		$item_array['itemId1'] = $itemId;
		$item_array['itemId12'] = $itemId;
		$item_array['compid1'] = $compid;
		$item_array['compid2'] = $compid;
		$item_array['compid3'] = $compid;
		$res = DB::select(DB::raw($itemsql),$item_array);
		/* $res= $db->fetchAll($itemsql); */
		 $itemresult = json_decode(json_encode($res),true); 
		return $itemresult;
	}
	
	
	/* public static function getElearningTrainingPrograms($compid,$userid,$roleid,$cattype,$callfrom){
		$strQuery2 = "";
		$su_roleid = $roleid;
		if($cattype != '')
			$strQuery2 = " and imtc.training_category_id = '$cattype'";		
		$strQuery4 = $strQuery3 =  $statusId = "";
		if($callfrom == 'assignment'){
			if($roleid==2){
				$userrolearray =Trainingprogram::getSupervisorSupervisorId($compid,$userid);	
				$su_userid = $userrolearray[0];
				$su_roleid = $userrolearray[1];
			}else{
				$su_userid = $userid ;
				}
				
		   $compSettings = Admin::checkduplicatedataforcompsetting($compid);
            if(($compSettings[0]->supervisor_training_view == 1) && ($roleid==2)) { 
				$roleid	= 1;
				$statusId = " im.status_id = 1 ";
			}else{	
                if($su_roleid != 1){
					$strQuery3 = "join sub_admin_assigned_category subc on (imtc.training_category_id = subc.category_id)";
					$strQuery4 = "and user_id = $su_userid";
					$statusId = " im.status_id != 0 ";
				}else{ 
					$roleid	= 1;
					$statusId = " im.status_id = 1 ";
				}			
			}   
		}else
			$statusId = " im.status_id = 1 ";
		$item = array();
		if($roleid != 1){
			$checkTargetAudience="select ta.job_group_id,im.item_id ,ta.type_name,im.item_type_id 
			from item_master as im  
			left join target_audience as ta on ta.item_id=im.item_id and ta.is_active=1 and ta.company_id=:compid1 and ta.job_group_id!=0 
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id=:compid2
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) ".$strQuery3." and tct.company_id=:compid3
			where im.is_active=1 and im.training_type_id=1 and im.company_id=:compid4 and im.item_type_id !=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 $strQuery4";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
			$item['compid4'] = $compid;
		}else{
			$checkTargetAudience="select im.item_id , 0 as job_group_id ,'' as type_name,im.item_type_id from item_master as im  
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id=:compid1
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id=:compid2
			where im.is_active=1 and im.training_type_id=1 and im.company_id=:compid3 and item_type_id!=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 group by im.item_id";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
		}
		  $target = DB::select(DB::raw($checkTargetAudience),$item);
		  $itemresult=array();
		  $uniuqeItemIdArray=array();
		  if(count($target)>0){
			  foreach($target as $key => $value){
				  
				  if(is_object($value)){ 
					$item_type_id	=	$value->item_type_id;
					$item_id		=	$value->item_id;
					$type_name		=	$value->type_name;
				  }
				 else{ 
					$item_type_id	=	$value['item_type_id'];
					$item_id		=	$value['item_id'];
					$type_name		=	$value['type_name'];
				} 
				
				 $itemid=$item_id;
				 $param = array();
				 if($type_name=='job'){
					 $jobTitleBasedItem="select im.training_type_id,im.item_name as training_title,im.item_id,im.training_code,im.item_name,im.average_rating,im.credit_value,
					im.item_type_id,im.delivery_type_id,tp.blended_program_id,tp.class_id,tp.published_date,tp.training_program_id,	im.file_id,tc.training_type as itemtype from item_master as im 
					join training_type as tc on tc.training_type_id=im.training_type_id
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid1 and ta.is_active=1
					join user_jobtitle as uj on uj.job_id = ta.job_group_id and uj.user_id=:userid and uj.is_active=1  
					join job_title_group as jtg on jtg.job_id=uj.job_id and jtg.is_active = 1  and jtg.company_id=:compid2
					left join training_program as tp on im.item_id= tp.item_id and tp.is_active=1 and (tp.blended_program_id = '' or  tp.blended_program_id is  null)
					where im.company_id = :compid3 and ".$statusId." and im.item_id=:itemid 
					and im.training_type_id=1 and im.is_active='1' and im.item_type_id!=4  group by im.item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['userid'] = $userid;
					$param['itemid'] = $itemid;
					$res  = DB::select(DB::raw($jobTitleBasedItem),$param);
					if(count(@$res)>0)
					{
						if(!in_array($itemid, $uniuqeItemIdArray)){
							$uniuqeItemIdArray[] = $itemid;
							$itemresult[] = $res;
						}
					}
					 
				 }else if($type_name=='group'){
					 $groupBasedItem="select im.training_type_id,im.item_name as training_title,im.item_id,im.training_code,im.item_name,im.average_rating,im.credit_value,
					im.item_type_id,im.delivery_type_id,tp.blended_program_id,tp.class_id,tp.published_date,tp.training_program_id,	im.file_id,tc.training_type as itemtype from item_master as im 
					join training_type as tc on tc.training_type_id=im.training_type_id
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid1 and ta.is_active=1
					join user_group as ugp on ugp.group_id = ta.job_group_id and ugp.user_id=:userid and ugp.is_active=1   
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0 and gm.company_id=:compid2
					left join training_program as tp on im.item_id= tp.item_id and tp.is_active=1 and (tp.blended_program_id = '' or  tp.blended_program_id is  null)
					where im.company_id = :compid3 and ".$statusId." and im.item_id=:itemid 
					and im.training_type_id=1 and im.item_type_id!=4 group by im.item_id";
					 $param['compid1'] = $compid;
					 $param['compid2'] = $compid;
					 $param['compid3'] = $compid;
					 $param['userid'] = $userid;
					 $param['itemid'] = $itemid;
					 $res  = DB::select(DB::raw($groupBasedItem),$param);
					 if(count(@$res)>0){
						if(!in_array($itemid, $uniuqeItemIdArray)){
							$uniuqeItemIdArray[] = $itemid;
							$itemresult[] = $res;
						}
						
					}
				 }else {
					$itemid=$item_id;
					$itemsql = "select im.training_type_id,im.item_name as training_title,im.item_id,im.training_code,im.item_name,im.average_rating,im.credit_value,
					im.item_type_id,im.delivery_type_id,tp.blended_program_id,tp.class_id,tp.published_date,tp.training_program_id,	im.file_id,tc.training_type as itemtype from item_master as im 
					join training_type as tc on tc.training_type_id=im.training_type_id
					left join training_program as tp on im.item_id= tp.item_id and tp.is_active=1 and (tp.blended_program_id = '' or  tp.blended_program_id is  null)
					where im.company_id = '$compid' and ".$statusId." and im.item_id=$itemid and im.training_type_id=1 and im.item_type_id!=4 group by im.item_id";
					 $param['compid1'] = $compid;
					 $param['itemid'] = $itemid;
					 $res  = DB::select(DB::raw($itemsql),$param);
					 if(count(@$res)>0){
						if(!in_array($itemid, $uniuqeItemIdArray)){
							$uniuqeItemIdArray[] = $itemid;
							$itemresult[]	=	$res;
						}
					}
				 }
			  }
		  } 
		 $itemresult = json_decode(json_encode($itemresult),true);
		 return $itemresult;
		
	} */
	
	
	
	public static function  getSupervisorSupervisorId($compid,$userid){
		$check_role_array =array(1,3,5);
		for($i=0; $i<10; $i++)
		{
			  $result = UserMaster::join('user_supervisor',function($join) use($compid){
				  $join->on('user_supervisor.user_id','=','user_master.user_id')
				       ->where('user_supervisor.is_active','=',1)
					   ->where('user_supervisor.company_id','=',$compid);
			  })->where('user_master.user_id','=',$userid)
			    ->where('user_master.company_id','=',$compid)
				->select('user_master.user_id','user_supervisor.user_id','user_supervisor.supervisor_id','user_master.role_id')
				->get()->toArray();
		
			if(count($result)>0)
			{				
				if(!in_array($result[0]['role_id'],$check_role_array)){										
					$userid =$result[0]['supervisor_id'];
					$role_id =$result[0]['role_id'];
				}else{
					$userid =$result[0]['user_id'];
					$role_id =$result[0]['role_id'];
				}				
			}				
		}	
		return array($userid,$role_id);
	}
	
	
	/* public static function getblendedTrainingPrograms($compid,$userid,$roleid,$cattype,$callfrom){
		$strQuery2 = "";
		$suroleid = $roleid;
		if($cattype != '')
			$strQuery2 = " and bptc.training_category_id = '$cattype' ";
		$strQuery4 = $strQuery3 =  $statusId = "";
		if($callfrom == 'assignment'){
			if($roleid==2){
				$userrolearray =Trainingprogram::getSupervisorSupervisorId($compid,$userid);	
				$userid = $userrolearray[0];
				$suroleid = $userrolearray[1];
			}
			$compSettings = Admin::checkduplicatedataforcompsetting($compid);
			if(($compSettings[0]->supervisor_training_view == 1) && ($roleid==2)) {
				$roleid	= 1;
				$statusId = " bpi.status_id = 1 ";
			}else{	
				if($suroleid != 1){
					$strQuery3 = "join sub_admin_assigned_category subc on (bptc.training_category_id = subc.category_id)";
					$strQuery4 = "and subc.user_id = $userid";
					$statusId = " bpi.status_id != 0 ";
				}else{
					$roleid	= 1;
					$statusId = " bpi.status_id = 1 ";
				}
			}
		}else
			$statusId = " bpi.status_id = 1 ";
		   $blendedIdArr=array();
		   $item = array();
		   if($roleid != 1){
			   $checkTargetAudience="select distinct bta.job_group_id,bpi.blended_item_id ,bta.type_name 
			from blended_program_items as bpi  
			left join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compid1 and bta.job_group_id!=0 
			left join blended_program_training_category bptc on bpi.blended_item_id = bptc.blended_program_id
			left join training_category tct ON (bptc.training_category_id = tct.training_category_id) ".$strQuery3." and tct.company_id=:compid2
			where bpi.is_active=1 and bpi.company_id=:compid3 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 $strQuery4";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
		   }else{
			$checkTargetAudience="select bpi.blended_item_id , 0 as job_group_id ,'' as type_name 
			from blended_program_items as bpi  
			left join blended_program_training_category bptc on bpi.blended_item_id = bptc.blended_program_id
			left join training_category tct ON (bptc.training_category_id = tct.training_category_id) and tct.company_id=:compid1
			where bpi.is_active=1 and bpi.company_id=:compid2 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 group by bpi.blended_item_id";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			
		}
		
		$target = DB::select(DB::raw($checkTargetAudience),$item);
		$itemresult=array();
		$param = array();
		if(count($target)>0){
			foreach($target as $key => $value){
				$blended_item_id	=	$value->blended_item_id;
				$type_name			=	$value->type_name;
				
				if($type_name=='job'){
					$jobTitleBasedItem="select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and $statusId and bpi.company_id=:compid1
					join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compid2 and bta.job_group_id!=0 
					join user_jobtitle as uj on uj.job_id = bta.job_group_id and uj.user_id=:userid and uj.is_active=1  
					join job_title_group as jtg on jtg.job_id=uj.job_id and jtg.is_active = 1 and jtg.company_id=:compid3
					where tp.company_id = :compid4 and tp.blended_program_id=:blended_item_id1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blended_item_id2 and bpi.is_active='1' group by bpi.blended_item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['userid'] = $userid;
					$param['blended_item_id1'] = $blended_item_id;
					$param['blended_item_id2'] = $blended_item_id;
					$res  = DB::select(DB::raw($jobTitleBasedItem),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['blended_item_id'],$blendedIdArr)){
							$blendedIdArr[] = $res[0]['blended_item_id'];
							$itemresult[] = $res;
						}
					}
				}else if($type_name=='group'){
					$groupBasedItem="select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and $statusId and bpi.company_id=:compid1
					join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compid2 and bta.job_group_id!=0 
					join user_group as ugp on ugp.group_id = bta.job_group_id and ugp.user_id=:userid and ugp.is_active=1   
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0 and gm.company_id=:compid3
					where tp.company_id = :compid4 and tp.blended_program_id=:blended_item_id1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blended_item_id2 and bpi.is_active='1' group by bpi.blended_item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['userid'] = $userid;
					$param['blended_item_id1'] = $blended_item_id;
					$param['blended_item_id2'] = $blended_item_id;
					$res  = DB::select(DB::raw($groupBasedItem),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['blended_item_id'],$blendedIdArr)){
							$blendedIdArr[] = $res[0]['blended_item_id'];
							$itemresult[] = $res;
						}
					}
				}else {
					$itemsql = "select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and $statusId and bpi.company_id=:compid1
					where tp.company_id = :compid2 and tp.blended_program_id=:blended_item_id1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blended_item_id2 and bpi.is_active='1' group by bpi.blended_item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['blended_item_id1'] = $blended_item_id;
					$param['blended_item_id2'] = $blended_item_id;
					$res  = DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['blended_item_id'],$blendedIdArr)){
							$blendedIdArr[] = $res[0]['blended_item_id'];
							$itemresult[] = $res;
						}
					}
				}
			}
		}
		$itemresult = json_decode(json_encode($itemresult),true);
		return $itemresult;
	} */
	
	
	/* public static function getClassroomTrainingPrograms($compid, $userid, $roleid,$cattype,$traingTypeId='',$callfrom){
		$traingTypeId =($traingTypeId == '') ? '2,3' : $traingTypeId;
		$strQuery2 = "";
		$suroleid = $roleid;
		if($cattype != '')
			$strQuery2 = "and imtc.training_category_id = '$cattype'";
		
		$strQuery5 = $strQuery4 = $strQuery3 = $strQuery3 = "";
		if($traingTypeId == 3){
			
			$strQuery5	= "and cm.item_type_id in ($traingTypeId)";
			}else {
			$strQuery5	= "and im.training_type_id in ($traingTypeId)";
		}
		if($callfrom == 'assignment'){
			if($roleid==2){
				$userrolearray =Trainingprogram::getSupervisorSupervisorId($compid,$userid);	
				$userid = $userrolearray[0];
				$suroleid = $userrolearray[1];
			}
			
			$compSettings = Admin::checkduplicatedataforcompsetting($compid);
			
			if(($compSettings[0]->supervisor_training_view == 1) && ($roleid==2)) {
				$roleid	= 1;
				$statusId = " cm.status_id = 1";
			}else{
				if($suroleid != 1){
					$strQuery3 = "join sub_admin_assigned_category subc on (imtc.training_category_id = subc.category_id)";
					$strQuery4 = "and user_id = $userid";
					$statusId = " cm.status_id != 0 ";
				}else{
					$roleid	= 1;
					$statusId = " cm.status_id = 1";
				}
			}
		}else
			$statusId = " cm.status_id = 1";
		$item = array();
		if($roleid != 1){
			$checkTargetAudience="select distinct ta.job_group_id,im.item_id ,ta.type_name,im.item_type_id 
			from item_master as im  
			left join target_audience as ta on ta.item_id=im.item_id and ta.is_active=1 and ta.company_id=:compid1 and ta.job_group_id != 0 
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id  and imtc.company_id=:compid2
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) ".$strQuery3." and tct.company_id=:compid3
			join class_master cm on cm.item_id=im.item_id and cm.company_id=:compid4
			where im.is_active=1 and im.company_id=:compid5 and im.item_type_id !=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 $strQuery4 $strQuery5";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
			$item['compid4'] = $compid;
			$item['compid5'] = $compid;
		}else{
		 $checkTargetAudience="select im.item_id , 0 as job_group_id ,'' as type_name,im.item_type_id from item_master as im  
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id=:compid1
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id=:compid2
			join class_master cm on cm.item_id=im.item_id and cm.company_id=:compid3
			where im.is_active=1 and im.company_id=:compid4 and im.item_type_id !=4 and tct.is_delete=0 and tct.is_active=1 ".$strQuery2." $strQuery5";	
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
			$item['compid4'] = $compid;
		}
		
              $target = DB::select(DB::raw($checkTargetAudience),$item);
			 
			  $itemresult=array();
		      $itemArr=array();
		      $uniqueClassId = array();
			  $param = array();
			  if(count($target)>0){
				  foreach($target as $key => $value){
					  $item_type_id=$value->item_type_id;
				      $item_id=$value->item_id;
				      $type_name=$value->type_name;
				      $itemid = $item_id;
				      if($type_name=='job'){
						  $itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
					cs.expiry_date, im.item_id, im.item_name,im.training_code, im.item_type_id, cm.item_type_id as training_type_id, im.delivery_type_id, im.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM item_master im 
					join class_master cm on im.item_id = cm.item_id and $statusId and cm.company_id=:compid1
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid2 and ta.is_active = 1
					join user_jobtitle as uj on uj.job_id = ta.job_group_id and uj.user_id=:userid and uj.is_active=1
					join job_title_group as jtg on jtg.job_id=uj.job_id	and jtg.is_active = 1 and jtg.company_id=:compid3	
					join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id=:compid4	
					join training_type ty ON ty.training_type_id  = cm.item_type_id
					join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = :compid5 and ssd.is_active=1  group by ssd.class_id) ss ON cs.class_id = ss.class_id 
					left join training_program tp on tp.item_id = im.item_id and cm.class_id = tp.class_id and tp.company_id=:compid6 and tp.is_active = 1
					where im.company_id = :compid7 and im.item_type_id!=4 and cm.item_type_id in ($traingTypeId) and im.item_id=:itemid and cs.is_deleted = 0  group by im.item_id, cm.class_id";
					
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['compid5'] = $compid;
					$param['compid6'] = $compid;
					$param['compid7'] = $compid;
					$param['userid'] = $userid;
					$param['itemid'] = $itemid;
					$res=  DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['class_id'],$uniqueClassId)){
							$uniqueClassId[] = $res[0]['class_id'];
							$itemresult[] = $res;
						}
					}
						  
				   }else if($type_name=='group'){
					   $itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
					cs.expiry_date, im.item_id, im.item_name,im.training_code, im.item_type_id, cm.item_type_id as training_type_id ,im.delivery_type_id, im.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM item_master im 
					join class_master cm on im.item_id = cm.item_id and $statusId and cm.company_id=:compid1
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid2 and ta.is_active = 1
					join user_group as ugp on ugp.group_id = ta.job_group_id and ugp.user_id=:userid and ugp.is_active=1 	
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0  and gm.company_id=:compid3
					join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id=:compid4
					join training_type ty ON ty.training_type_id  = cm.item_type_id
					join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = :compid5 and ssd.is_active=1  group by ssd.class_id) ss ON cs.class_id = ss.class_id 
					left join training_program tp on tp.item_id = im.item_id and cm.class_id = tp.class_id and tp.company_id=:compid6 and tp.is_active = 1
					where im.company_id = :compid7 and im.item_id=:itemid and cm.item_type_id in ($traingTypeId) and im.item_type_id != 4 and im.training_type_id in (2,3) and  cs.is_deleted = 0 group by im.item_id, cm.class_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['compid5'] = $compid;
					$param['compid6'] = $compid;
					$param['compid7'] = $compid;
					$param['userid'] = $userid;
					$param['itemid'] = $itemid;
					$res=  DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['class_id'],$uniqueClassId)){
							$uniqueClassId[] = $res[0]['class_id'];
							$itemresult[] = $res;
						}
					}
				   }else{
					    $itemsql ="SELECT cm.class_name, cm.class_id,cm.maximum_seat as max_seat,im.item_id, im.item_name,im.training_code, 
					im.item_type_id, cm.item_type_id as training_type_id, im.delivery_type_id, im.company_id,tp.training_program_id, im.published_date,im.credit_value, ty.training_type as itemtype,cm.start_date_class as recursive_date
					FROM item_master im 
					join class_master cm on im.item_id = cm.item_id and $statusId and cm.company_id=:compid1
					join training_type ty ON ty.training_type_id = cm.item_type_id
					left join training_program tp on tp.item_id = im.item_id and cm.class_id = tp.class_id and tp.company_id=:compid2 and tp.is_active = 1
					where im.company_id = :compid3 and im.item_id=:itemid and cm.item_type_id in ($traingTypeId) and im.item_type_id != 4 and  cm.is_active = 1 and im.training_type_id in (2,3) and DATE_FORMAT(cm.start_date_class,'%Y-%m-%d') >= CURRENT_DATE() 
					group by im.item_id, cm.class_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['itemid'] = $itemid;
					$res=  DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['class_id'],$uniqueClassId)){
							$uniqueClassId[] = $res[0]['class_id'];
							$itemresult[] = $res;
						}
					}
				   }
				  }
			  }
			  $itemresult = json_decode(json_encode($itemresult),true);
		

		return $itemresult;
	} */
	public static function getelearningdata($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$traingTypeId='',$categoryId='',$filterdata=array())
	{
		
		return TrainingProgram::getElearningTrainingPrograms($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$traingTypeId,$categoryId,$filterdata);
	}
	
	public static function getElearningTrainingPrograms($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$traingTypeId,$categoryId,$filterdata){
	     
		if($roleid != 1){
			     $targetaudienceitems=TrainingProgram::targetAudiencesCourses($compid,$userid);
			     $itemresult=TrainingProgram::allCoursesTraining($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$targetaudienceitems,$traingTypeId,$categoryId,$filterdata);
		}else{
			
				$itemresult=TrainingProgram::allCoursesTraining($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,array(),$traingTypeId,$categoryId,$filterdata);
		}
		
		 return $itemresult;
	}
	
	public static function allCoursesTraining($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$targetaudienceitems,$traingTypeId,$categoryId,$filterdata){
	
					if($orderColumn==0){
						$column='course_details.course_name';
					}
					elseif($orderColumn==1){
						$column='course_details.course_name';//work here again
					}elseif($orderColumn==2){
						$column='courses.credit_value';
					}elseif($orderColumn==3){
						$column='training_type.training_type';
					}
					
					$lang_id=1;
					$querydata=Course::join('training_programs',function($join) use($compid){
						$join->on('training_programs.training_program_code','=','courses.training_program_code')
						 ->where('training_programs.is_internal','=',1)
						 ->where('training_programs.company_id','=',$compid)
						 ->where('training_programs.status_id','=',1);
					})
					->join('training_type','training_type.training_type_id','=','training_programs.training_type')
					->leftjoin('course_categories',function($join) use($compid){
						$join->on('course_categories.course_id','=','courses.course_id')
						->where('course_categories.is_active','=',1)
						->where('course_categories.company_id','=',$compid);
					})
					->leftjoin('training_category',function($join) use($compid){
						$join->on('training_category.training_category_id','=','course_categories.training_category_id')
						->where('training_category.is_active','=',1)
						->where('training_category.is_delete','=',0)
						->where('training_category.company_id','=',$compid);
					})
					->join('course_details',function($join) use($compid,$lang_id){
						$join->on('course_details.course_id','=','courses.course_id')
						->where('course_details.company_id','=',$compid)
						->where('course_details.lang_id','=',$lang_id);
					})
					->leftjoin('class_master',function($join) use($compid){
						$join->on('class_master.course_id','=','courses.course_id')
						->where('class_master.company_id','=',$compid)
						->where('class_master.status_id','=',1);
					})
					->leftjoin('class_schedule',function($join) use($compid){
						$join->on('class_schedule.class_id','=','class_master.class_id')
						->where('class_schedule.company_id','=',$compid);
					})
					
					->leftjoin(DB::raw('(SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = '.$compid.' and ssd.status_id=1  group by ssd.class_id)
					   ss'), 
				function($join)
				{
				   $join->on('class_master.class_id', '=', 'ss.class_id');
				})
					->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)
					
					->select(DB::raw('training_programs.training_type as training_type_id,course_details.course_name,course_details.course_title,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type,if(training_programs.training_type=1,"ON Demand",ss.dayid) as recursive_date,class_master.class_name,class_master.class_id'));
					
			
			 //for Target audience
			if(($roleid!=1) &&  (count($targetaudienceitems)!=0))
			{
				$querydata=$querydata->whereIn('courses.course_id',$targetaudienceitems);
				
			}
			
			//for advance filter
			if($filterdata){
				
				$trainingtypeid =$filterdata['trainingtypeid']!='' ? $filterdata['trainingtypeid']:0;
				$trainingtitle  =$filterdata['trainingtitle']!=''  ? $filterdata['trainingtitle'] :'';
				$category  		=$filterdata['category']!=''  	   ? $filterdata['category'] :0;
				$instructorname =$filterdata['instructorname']!='' ? $filterdata['instructorname']:'';
				$creditvalue    =$filterdata['creditvalue']!=''    ? $filterdata['creditvalue']   :0; 
				$costvalue      =$filterdata['costvalue']!=''      ? $filterdata['costvalue']     :0;
				$startdate      =$filterdata['startdate']!=''      ? $filterdata['startdate']     :'';
				$endDate     	=$filterdata['endDate'] !=''       ? $filterdata['endDate']       :'';
				if($trainingtypeid){
					$querydata=$querydata->where('training_type.training_type_id','=',$trainingtypeid);
				}
				if($trainingtitle){
				$querydata=$querydata->where('course_details.course_name','like','%'.$trainingtitle.'%');
				}
				
				if($creditvalue){
				$querydata=$querydata->where('courses.credit_value','=',$creditvalue);
				}
				
				if($category){
				$querydata=$querydata->where('training_category.training_category_id','=',$category);
				}
				
				if($costvalue){
				$querydata=$querydata->where('courses.item_cost','=',$costvalue);
				}
				
				
			}
			
			//for simple filter
			if($traingTypeId){
				$querydata=$querydata->where('training_type.training_type_id','=',$traingTypeId);
			} 
			//for simple filter
			if($categoryId){
				$querydata=$querydata->where('training_category.training_category_id','=',$categoryId);
			}
			
			//for grid filter
			if($searchtext){
			$querydata=$querydata->where(function($query) use($searchtext){
			$query->where('course_details.course_name','like','%'.$searchtext.'%')
			->orWhere('courses.credit_value','like','%'.$searchtext.'%')
			->orWhere('training_type.training_type','like','%'.$searchtext.'%');
			});
			}
			
			$totalcount=$querydata->groupby('training_programs.training_program_code')->get();
			
			$totalclass=TrainingProgram::countallClasscountlessthancurrdate($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$targetaudienceitems,$traingTypeId,$categoryId,$filterdata);
			
			//echo $totalclass; exit;
			if($column){
				$allrecords=$querydata->orderBy($column,$orderType)->groupby('training_programs.training_program_code')->skip($start)->take($length)->get();
			}else{
				$allrecords=$querydata->groupby('training_programs.training_program_code')->skip($start)->take($length)->get();
			}
		    $totalrecord=count($totalcount)-$totalclass;
			return array($allrecords,$totalrecord);
	 }
	 
	 public static function countallClasscountlessthancurrdate($compid,$userid,$roleid,$start,$length,$orderType,$orderColumn,$searchtext,$targetaudienceitems,$traingTypeId,$categoryId,$filterdata){
		 if($orderColumn==0){
				$column='course_details.course_name';
			}
			elseif($orderColumn==1){
				$column='course_details.course_name';//work here again
			}elseif($orderColumn==2){
				$column='courses.credit_value';
			}elseif($orderColumn==3){
				$column='training_type.training_type';
			}
			
		    $lang_id=1;
		    $querydata=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
			})
			->join('training_type','training_type.training_type_id','=','training_programs.training_type')
		
			->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
			})
			->join('class_master',function($join) use($compid){
				$join->on('class_master.course_id','=','courses.course_id')
				->where('class_master.company_id','=',$compid)
				->where('class_master.status_id','=',1);
			})
			->join('class_schedule',function($join) use($compid){
				$join->on('class_schedule.class_id','=','class_master.class_id')
				->where('class_schedule.company_id','=',$compid);
			})
			
			->join(DB::raw('(SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = '.$compid.' and ssd.status_id=1  group by ssd.class_id)
               ss'), 
        function($join)
        {
           $join->on('class_master.class_id', '=', 'ss.class_id');
        })
			->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)->whereRaw('ss.dayid < CURRENT_DATE()')
			->select(DB::raw('ss.dayid'));
			
			
			 //for Target audience
			if(($roleid!=1) &&  (count($targetaudienceitems)!=0))
			{
				$querydata=$querydata->whereIn('courses.course_id',$targetaudienceitems);
				
			}
			
			//for advance filter
			if($filterdata){
				
				$trainingtypeid =$filterdata['trainingtypeid']!='' ? $filterdata['trainingtypeid']:0;
				$trainingtitle  =$filterdata['trainingtitle']!=''  ? $filterdata['trainingtitle'] :'';
				$category  		=$filterdata['category']!=''  	   ? $filterdata['category'] :0;
				$instructorname =$filterdata['instructorname']!='' ? $filterdata['instructorname']:'';
				$creditvalue    =$filterdata['creditvalue']!=''    ? $filterdata['creditvalue']   :0; 
				$costvalue      =$filterdata['costvalue']!=''      ? $filterdata['costvalue']     :0;
				$startdate      =$filterdata['startdate']!=''      ? $filterdata['startdate']     :'';
				$endDate     	=$filterdata['endDate'] !=''       ? $filterdata['endDate']       :'';
				if($trainingtypeid){
					$querydata=$querydata->where('training_type.training_type_id','=',$trainingtypeid);
				}
				if($trainingtitle){
				$querydata=$querydata->where('course_details.course_name','like','%'.$trainingtitle.'%');
				}
				
				if($creditvalue){
				$querydata=$querydata->where('courses.credit_value','=',$creditvalue);
				}
				
				if($category){
				$querydata=$querydata->where('training_category.training_category_id','=',$category);
				}
				
				if($costvalue){
				$querydata=$querydata->where('courses.item_cost','=',$costvalue);
				}
				
				
			}
			
			//for simple filter
			if($traingTypeId){
				$querydata=$querydata->where('training_type.training_type_id','=',$traingTypeId);
			} 
			//for simple filter
			if($categoryId){
				$querydata=$querydata->where('training_category.training_category_id','=',$categoryId);
			}
			
			//for grid filter
			if($searchtext){
			$querydata=$querydata->where(function($query) use($searchtext){
			$query->where('course_details.course_name','like','%'.$searchtext.'%')
			->orWhere('courses.credit_value','like','%'.$searchtext.'%')
			->orWhere('training_type.training_type','like','%'.$searchtext.'%');
			});
			}
			
			$totalcount=$querydata->groupby('training_programs.training_program_code')->get();
		 return count($totalcount);
	 }
		public static function getCourseTrainingDetail($compid, $programcode) 
		{  
				$lang_id=1;
				$querydata=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
				})
				->join('training_type','training_type.training_type_id','=','training_programs.training_type')
				->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
				})
				->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
				})
				->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
				})
				->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)->where('training_programs.training_program_code','=',$programcode)
				->select(DB::raw('training_programs.training_type as training_type_id,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type,if(training_programs.training_type=1,"ON Demand",CURDATE()) as recursive_date,courses.course_image,course_details.course_description,course_details.prerequeisites,courses.average_rating,training_programs.training_program_code,GROUP_CONCAT(distinct training_category.category_name) as category_name'))->first();
				return $querydata;
		}
		
		
			public static function targetaudiences($compid, $course_id) 
			{
				$gropus_data=CourseTargetAudience::where('course_id','=',$course_id)->where('group_id','!=','[]')->where('company_id','=',$compid)->first();
				$groupids= isset($gropus_data->group_id) ? json_decode($gropus_data->group_id) : array();
				
				$result1=GroupMaster::whereIn('group_id',$groupids)->where('company_id','=',$compid)->where('is_active','=',1)->select(DB::raw('"group" as type_name,group_name as name'))->get();
				
				$store=array();
				if($result1)
				{
					foreach($result1 as $key=>$value)
					{
						if($value!=Array())
						{   
					        $result=array();
							$result['name']=$value->name;
							$result['type_name']=$value->type_name;
							$store[]=$result;
						}
					}
				}
			
				$users_data=CourseTargetAudience::where('course_id','=',$course_id)->where('user_id','!=','[]')->where('user_id','!=','undefined')->where('company_id','=',$compid)->first();
				$userids=isset($users_data->user_id) ? json_decode($users_data->user_id) : array();
				
				$result2=UserMaster::whereIn('user_id',$userids)->where('company_id','=',$compid)->where('is_active','=',1)->select(DB::raw('"user" as type_name,user_name as name'))->get();
				
				if($result2)
				{
					
					foreach($result2 as $key=>$value)
					{
						if($value!=Array())
						{       $result=array();
								$result['name']=$value->name;
							    $result['type_name']=$value->type_name;
								$store[]=$result;
							
						}
					}
				}
				
				return $store;
			}
			
	public static function getTranscriptId($compid, $userid, $course_id)
	{
		$result=UserTranscript::where('company_id','=',$compid)->where('course_id','=',$course_id)->where('status_id','=',1)->where('user_id','=',$userid)->select('transcript_id')->first();	
		return isset($result->transcript_id) ? $result->transcript_id : 0;
	}

	public static function getMediaFileDetails($userid,$company_id,$course_id,$courseDetailArr)
	{  
		$filedir = $filepath = $filesname = $fileDetails = $scormDetails = $userDetails = $files = $usersDetails = $media_id = $masteryscore='';
		$usertrainingid=$support_formats='';	

		$fileid = Trainingprogram::getfileid($course_id,$company_id);		
		$media_library_id = $fileid->media_library_id;
		
		$is_lib='';
		$courseDetailArr['file_id'] = $media_library_id;
		if($media_library_id!=0) 
		{  
			$fileDetails = Trainingprogram::getfilename($media_library_id,$company_id);
			if($fileDetails)
			{
				$filesname = $fileDetails->file_name;
				$is_lib = $fileDetails->is_lib;
				$media_id = $fileDetails->media_id;
				$masteryscore = ($fileDetails->masteryscore > 0) ? $fileDetails->masteryscore : ' '  ;
				$filedir = substr($filesname, 0, -4);
				$filepath = explode('.', $filesname);
				$support_formats = explode("|",$fileDetails->support_formats);
			}
		}
		$courseDetailArr['media_id'] 				= 	$media_id;
		$courseDetailArr['filedir'] 				= 	$filedir;
		$courseDetailArr['filepath'] 				= 	$filepath;
		$courseDetailArr['filesname'] 				= 	$filesname;
		$courseDetailArr['fileDetails'] 			= 	$fileDetails;
		$courseDetailArr['passingscore'] 			= 	$masteryscore;
		$courseDetailArr['support_formats'] 		= 	$support_formats;
		$courseDetailArr['is_lib'] 					= 	$is_lib;
		return $courseDetailArr;
	}

	public static function getfileid($course_id,$company_id)
	{
		$result =Course::where('company_id','=',$company_id)->where('course_id','=',$course_id)->where('status_id','=',1)->select('media_library_id')->first();
		return $result;
	}

	public static function getfilename($file_id,$company_id)
	{
		$result =UploadedFile::join('media_types','media_types.media_id','=','uploaded_files.media_id')->where('uploaded_files.files_id','=',$file_id)
		->where('uploaded_files.company_id','=',$company_id)->first();
		    $returnresponse='';
		if($result){
			$returnresponse=$result;
		}
		return $returnresponse;
	}

	public static function checkTrainingExpiration($course_id, $userid, $compid){
		$expiration_status = 'new';
		$ut_res = Trainingprogram::getTranscriptId($compid, $userid, $course_id);

		if($ut_res){
				$expiration_status = 'retake';					
		  }
		return $expiration_status;
	}
	

	public static function getUserTrainingId($course_id, $userid, $compid){
		$res =UsertrainingCatalog::where('course_id', '=', $course_id)
		->where('user_id', '=', $userid)
		->where('company_id', '=' , $compid)
		->where('status_id','=',1)
		->where('training_status_id', '=', 3)->orderBy('user_training_id', 'desc')
		->first();
		if(isset($res->user_training_id)){
			UsertrainingCatalog::where('course_id', '=', $course_id)
			->where('user_id', '=', $userid)
			->where('company_id', '=' , $compid)
			->where('training_status_id', '=', 3)
			->where('user_training_id', '!=', $res->user_training_id)
			->delete();
		}

		return $res;
	}

	public static function takeTraining($course_id, $userid, $userprogramtypeid, $compid)
	{		
		
		$lastId = Trainingcatalog::EnrolledForElearningLearningplan($course_id, $userid, $userprogramtypeid, $compid);		
		return $lastId;
		
	}
	
	
	
	
	public static function getRetakeUserTrainingId($course_id,$userid,$compid,$training_program_code){
	    $utc_res =UsertrainingCatalog::where('course_id', '=', $course_id)
		->where('user_id', '=', $userid)
		->where('company_id', '=' , $compid)
		->where('status_id','=',1)
		->orderBy('user_training_id', 'desc')
		->first();
		return $utc_res;
	}
	
	public static function targetAudiencesCourses($compid,$userid) 
			{
				$course_data=CourseTargetAudience::where('user_id','!=','[]')->orWhere('user_id','!=','undefined')->orWhere('group_id','!=','[]')->orWhere('company_id','=',$compid)->get();
				$store1=array();
				$store=array();
				foreach($course_data as $dataAll){
					$course_id= isset($dataAll->course_id) ? $dataAll->course_id : '';
					
					$groupids= isset($dataAll->group_id) ? json_decode($dataAll->group_id) : array();
					if($groupids){
						$allusergroup=UserGroup::whereIn('group_id',$groupids)->where('is_active','=',1)->where('user_id','=',$userid)->get();
						if(count($allusergroup)!=0){
							 if(!in_array($course_id,$store1))
							  {
								  $store1[]=$course_id;
							  }
						}
					}
					
					$userids= isset($dataAll->user_id) ? json_decode($dataAll->user_id) : array();
					if($userids){
						if(in_array($userid,$userids)){
							  if(!in_array($course_id,$store))
							  {
								  $store[]=$course_id;
							  }
						}
					}
				}
				
				$allcourses=array_merge($store,$store1);
				return $allcourses;
			}
}
