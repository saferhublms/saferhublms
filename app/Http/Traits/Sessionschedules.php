<?php
namespace App\Http\Traits;

use App\Models\SupervisorSetting;
use App\Models\UserSupervisor;
use App\Models\UserMaster;
use App\Models\UsertrainingCatalog;
use App\Models\ClassSchedule;
use App\Models\SessionScedule;
use Mail,DB;

trait Sessionschedules{
	
	     public static function getLateststartdate($classid, $compid)
		 {
			$result =SessionScedule::whereclassId($classid)->wherecompanyId($compid)->whereisActive(1)->select(DB::raw('min(dayid) as dayid'))->get();
			return $result;
		 }
		 
		 public static function getScheduleStartDate($classid, $compid)
		{
			$result=SessionScedule::leftjoin('user_master as um','session_scedule.instructor_id','=','um.user_id')
			                      ->leftjoin('class_timezones as ctz','session_scedule.time_zone','=','ctz.class_timezone_id')
								  ->where('session_scedule.class_id',$classid)
								  ->where('session_scedule.company_id',$compid)
								  ->where('session_scedule.is_active',1)
								  ->select('session_scedule.schedule_id','session_scedule.dayid','session_scedule.ses_length_hr','session_scedule.ses_length_min','session_scedule.start_time_hr','session_scedule.start_time_min','session_scedule.timeformat','session_scedule.location','um.user_name','ctz.timezone_name','ctz.timezone_regions','ctz.timezone_dst','ctz.utc_offset','ctz.utc_offset_dst')
								  ->get();
			return $result;
		}
	
}