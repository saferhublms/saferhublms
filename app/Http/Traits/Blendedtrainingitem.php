<?php
namespace App\Http\Traits;
use Mail,DB,URL,CommonHelper;
use App\Models\BlendedProgramItem;


trait Blendedtrainingitem{
	
	
	public static function getallItems($userid,$id,$compid)
	{
		 $result = array();
		$getElearniingarray =Blendedtrainingitem::getitemsUnderBlendedProgram($userid,$id,$compid);
		foreach($getElearniingarray as $getdata){
			 $result[] = (array)$getdata; 
		}
		 return $result;
		
	}
	
	
	public static function getitemsUnderBlendedProgram($userid,$id,$compid)
	{
		$item_array = array();
		$sql="select im.item_id,im.require_supervisor_aproval_status,ass.id,bpti.training_type_id,bpti.training_item_id,bpti.due_date,bpti.sequence,bpti.blendedprogram_id,
		bpi.title,bpi.blendedimage,bpi.description,im.credit_value,im.item_name,ass.assessment_name,ass.credit_value as creditvalue,bpi.is_sequence,
		tp.training_program_id,uta.registration_id,uta.is_blended,tp.class_id,if(ut.transcript_id || uta.transcript_id> 0, '1','0') status 
		from blendedprogram_training_items bpti 
		join blended_program_items bpi on bpti.blendedprogram_id=bpi.blended_item_id
		left join item_master im on im.item_id=bpti.item_id and im.company_id = :compid1 
		left join assessment ass on ass.id=bpti.assessment_id and ass.is_delete = 0 and ass.company_id = :compid2 
		left join training_program tp on tp.item_id=im.item_id and tp.is_active=1 and tp.company_id = :compid3 
		left join user_transcript ut on ut.item_id=im.item_id and ut.user_id=:userid1 and ut.company_id = :compid4 and ut.is_active=1 
		left join user_transcript uta on uta.registration_id=ass.id and uta.user_id=:userid2 and uta.company_id = :compid5  and uta.is_active=1
		where bpti.blendedprogram_id=:id and bpti.is_active=1 and bpti.is_delete=1 and bpi.company_id=:compid6 
		group by im.item_id,ass.id order by bpti.sequence";
		
		$item_array['compid1'] = $compid; 
		$item_array['compid2'] = $compid; 
		$item_array['compid3'] = $compid; 
		$item_array['compid4'] = $compid; 
		$item_array['compid5'] = $compid; 
		$item_array['compid6'] = $compid; 
		$item_array['userid1'] = $userid; 
		$item_array['userid2'] = $userid; 
		$item_array['id'] = $id; 
		$result = DB::select(DB::raw($sql),$item_array);
		return $result;
	}
	
	
	public static function getblendeddetail($blendedId , $compid)
	{
		$result = BlendedProgramItem::where('blended_item_id','=',$blendedId)->where('company_id','=',$compid)->get(); 
		return $result; 
	}
	
	
	
	
}
