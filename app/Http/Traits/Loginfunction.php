<?php
namespace App\Http\Traits;
use Mail,DB;
use App\Models\CompanyPasswordSetting;
use App\Models\CompanyCmsInfo;
use App\Models\CompanySettings;
use App\Models\CompanyConfiguration;
use App\Models\CompanyCss;
use App\Models\User;
use App\Models\UserInfo;
use App\Models\UmsLogin;
use Auth;
use Session;
use stdClass;

trait Loginfunction{
	
	/**
     * fetchCompPasswordsettingData() - function to get Company Password setting.
     *
	 * Created By: Siddharth Kushwah
	 *
	 * Created Date: 21 Dec, 2017
     */
	public static function fetchCompPasswordsettingData($compid) 
	{
		$result =CompanyPasswordSetting::where('company_id','=',$compid)->first();
		return $result;
	}
	
	/**
     * getmylogo() - function to get my logo.
     *
	 * Created By: Siddharth Kushwah
	 *
	 * Created Date: 21 Dec, 2017
     */
	public static function getmylogo($compid) {
		$result =CompanyCmsInfo::where('company_id','=',$compid)->where('is_active' ,'=', 1)->first();
		return $result;
	}
	/**
     * getcompanysetting() - function to get company settings.
     *
	 * Created By: Siddharth Kushwah
	 *
	 * Created Date: 21 Dec, 2017
     */
	public static function getcompanysetting($compid) {
        $result = CompanySettings::join('company_configuration','company_configuration.company_config_id','=','company_settings.company_id')->where('company_settings.company_id','=',$compid)->where('company_configuration.is_active','=',1)->get();
		return $result;
	}
	
	
	/**
     * getaMainitenanceMsg() - function to get scheduled maintenance msg.
     *
	 * Created By: Siddharth Kushwah
	 *
	 * Created Date: 21 Dec, 2017
     */
	public static function getaMainitenanceMsg()
	{

		$sql = "SELECT * FROM super_admin_releasenotes WHERE is_maintenance=1 and is_active=0 and is_delete=0 and curdate() between DATE_SUB(release_date,INTERVAL 2 DAY) and release_date";
        $result  =DB::select(DB::raw($sql));
        return $result;
	}
	
	/**
     * getaccessurl() - function to get access url.
     *
	 * Created By: Siddharth Kushwah
	 *
	 * Created Date: 21 Dec, 2017
     */
	public static function getaccessurl()
	{
		
        $url	=	$_SERVER['HTTP_HOST'];
        $pattern=	'/^www/';
		preg_match($pattern, substr($url,0), $matches, PREG_OFFSET_CAPTURE);
                
		if(count($matches)==0){
		    $accessurl = $_SERVER['HTTP_HOST'];
		}
		if(count($matches)>0)
		{
			$accessurl = substr($url, strpos($url, "www.") + 4);
		}
             
		$checkaccessurl = Loginfunction::checkaccessurl($accessurl);
		if (count($checkaccessurl) > 0) {
			$company_id = $checkaccessurl[0]->company_id;
			return array('company_id' => $company_id, 'is_remote_authenticated' => $checkaccessurl[0]->is_remote_authenticated, 'is_maintainance' => $checkaccessurl[0]->is_maintainance);
		}if (count($checkaccessurl) == 0) {
			$msg = "Company is not authorised yet!";
			return array('company_id' => 0, 'message' => $msg);
		}
		$msg = "Company is not authorised yet!";
		return array('company_id' => 0, 'message' => $msg);
	}
	
		/**
		* checkaccessurl() - function to check access url.
		*
		* Created By: Siddharth Kushwah
		*
		* Created Date: 21 Dec, 2017
		*/
		public static function checkaccessurl($accessurl)
		{
			$sql = "select comp.company_config_id as company_id, app.is_remote_authenticated as is_remote_authenticated,app.is_maintainance as is_maintainance from app_configuration as app join company_configuration as comp on comp.company_config_id=app.company_id and app.app_url='$accessurl' and app.is_active='1'";
			$result  =DB::select(DB::raw($sql));
			return $result;
		}

		/**
		* getcompanycss() - function to  get companycss.
		*
		* Created By: Siddharth Kushwah
		*
		* Created Date: 21 Dec, 2017
		*/
		public static function getcompanycss($compid)
		{ 
	          $result = CompanyCss::where('company_id','=',$compid)->select('button_color','footer_bg','footer_link_bg','header_bg','header_text','header_title','template_id')->get();
			   
			if(count($result)==0){
				$result[0] = new stdClass();
				$result[0]->header_bg='';
				$result[0]->header_text='';
				$result[0]->header_title='';
				$result[0]->button_color='';
				$result[0]->footer_link_bg='';
				$result[0]->template_id='';
				$result[0]->footer_bg='';
			}
			return $result;
		}
		
		/**
		* getEncode($param, $add_salt_key='yes', $salt_key='') - function to  generate encoded token.
		*
		* Created By: Siddharth Kushwah
		*
		* Created Date: 22 Dec, 2017
		*/
		public static function getEncode($param, $add_salt_key='yes', $salt_key='')
		{
		if($add_salt_key=='yes'){
			$result = str_replace(array('+','/','='),array('-','_',''),base64_encode($param));
			if($salt_key!=''){

				$result = $salt_key.$salt_key.$result.$salt_key;
			}else{
				$result = Loginfunction::randLetter().Loginfunction::randLetter().$result.Loginfunction::randLetter();
			}
		}else{
			$result = base64_encode($param);
		}
		return $result;
		}
		
        /**
		* randLetter() - function to  generate rand string.
		*
		* Created By: Siddharth Kushwah
		*
		* Created Date: 22 Dec, 2017
		*/
		public static function randLetter()
		{
			$int = rand(0,51);
			$a_z = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$rand_letter = $a_z[$int];
			return $rand_letter;
		}
		
		 public static function checkuserforlogin($username, $compid)
		{ 
			$login_name = $compid.'_#_'.$username;
			$result =User::where('login_name','=',trim(strtolower($login_name)))->where('company_id', '=' , $compid)->first();
			
			return $result;
			
		}
		 
		public static function UserInfoPassSetExistance($userId, $compid)
		{

			$result =UserInfo::where('user_id','=',$userId)->where('company_id', '=' , $compid)->first();
			
			return $result;
					
		}
		
    public static function fetchumsinfoforlogout($company_id, $merchant_id) {
		
		$result =UmsLogin::where('user_id','=',$merchant_id)->where('company_id', '=' , $company_id)->whereNull('logout_time')->first();
		
		return $result;
	}

	
	public function getCompanySettingGroups($company_id)
	{
		echo $company_id; exit;
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		$sql = "SELECT ca.action_code, if(car.role_ids!='',car.role_ids,'0')  AS role_ids FROM company_action ca 
			left join company_action_role car on car.action_code = ca.action_code and car.company_id='$company_id' AND car.is_active=1";
		$session = new Zend_Session_Namespace('csg');
		$res = $db->fetchAll($sql);
		$permission = array();
		if($res)
		{
			foreach($res as $rep){
				$permission[$rep['action_code']] = explode(",",$rep['role_ids']);
			}
			$session->permission = $permission;
		}
	}
	
	
}
