<?php
namespace App\Http\Traits;

use App\Models\SupervisorSetting;
use App\Models\ClassMaster;
use App\Models\TrainingProgram;
use App\Models\UsertrainingCatalog;
use App\Models\UserTranscript;
use App\Models\ItemMasterTrainingcategory;
use Mail,DB;

trait historicaldata{
	
	public static  function addclass($datas, $compid)
	{
		$classname = $datas['class_name'];		
		$item_id = $datas['item_id'];
		$result=ClassMaster::where('class_master.item_id' ,'=',$item_id)->where('class_master.class_name','=',$classname)
			->where('class_master.company_id','=',$compid)->select('class_id')->get();
		$classid = 0;
		if(count($result) > 0) {
				if(is_object($result[0])) {
					$classid=$result[0]->class_id;
				}
				else {
					$classid=$result[0]['class_id'];
				}	
			}else {
				$data = array(
					'item_id'      									=> $datas['item_id'],
					'class_name'      								=> $datas['class_name'],
					'item_type_id'  								=> 4,
					'delivery_type_id'                              => 1,
					'status_id'       								=> 1,
					'maximum_seat'                                  => $datas['maxseat'],
					'current_enroll'                                => $datas['maxseat'],
					'is_allowed_waiting'							=> 0,
					'published_date'                                => '0000-00-00',
					'virtual_session_information'                   => '',
					'is_active'     								=> 1,
					'is_email_checked'                              => 0,
					'is_automatic_require_post_class_evaluation'    => 0,
					'mail_send'     								=> 0,
					'is_require_response_for_credit'                => 0,
					'avg_rating'       								=> ($datas['average_rating']) ? $datas['average_rating'] : 0,
					'evaluation_pref_send_same_day'                 => 0,
					'no_of_days'									=> 0,
					'publish_days_prior_to_sessions'                => 0,
					'hard-copy_rating'                              => 0,
					'evaluation_file_name'                          => 0,
					'company_id'									=> $compid,
					'created_by'                                    => $datas['lastmodifyuser_id'],
					'updated_by'       								=> $datas['lastmodifyuser_id'],
					'last_modified'                                 => date("Y-m-d H:i:s"),
					'start_date_class'								=> date('Y-m-d',strtotime($datas['start_date_class'])),
					'endate_date'									=> date('Y-m-d',strtotime($datas['start_date_class'])),
					'end_type'       								=> 1,
					'publish_all_reccurence'						=> 1,
					'recurrence_enable'								=> 0
				);
				$classMasterSave=ClassMaster::create($data);
				$classid = $classMasterSave->class_id; 
			}
         return $classid;			
	}
	
	
	public static function addtrainingprograms($mannuallyAddItem, $itemid, $compid) 
	{
		$classid = $mannuallyAddItem['class_id'];
		$result=TrainingProgram::where('training_program.item_id','=',$itemid)
			->where('training_program.class_id','=', $classid)
			->where('training_program.company_id','=', $compid)
			->get();
		$trainingId = 0;
		
		if(count($result) > 0) {
			if(is_object($result[0])) {
				$trainingId=$result[0]->training_program_id;
			}
			else{
				$trainingId=$result[0]['training_program_id'];
			}
		}else{
			$data = array(
				'item_id' 					=> $itemid,
				'training_type_id' 			=> $mannuallyAddItem['traning_type_id'],
				'class_id' 					=> $mannuallyAddItem['class_id'],
				'company_id' 				=> $compid,
				'blended_program_id'       	=> 0,
				'training_title' 			=> $mannuallyAddItem['title'],
				'description' 				=> $mannuallyAddItem['desc'],
				'is_active'     			=> 1,
				'require_supervisor_approval' => $mannuallyAddItem['require_supervisor_approval'],
				'avg_rating' 				=> 1,
			);
			$trainingSave=TrainingProgram::create($data);
			$trainingId=$trainingSave->training_program_id;
		}
		return $trainingId;	
			
	}
	
	
	public static function ComparefortrainingCatalog($alldata)
	{
		$itemid = $alldata['item_id'];
		$user_id = $alldata['users_id'];
		$companyid = $alldata['company_id'];
		$traningtype_id=$alldata['traningtype_id'];
		$assessmentid=isset($alldata['assessment']) ? $alldata['assessment'] : '';
		$result=UsertrainingCatalog::where('user_training_catalog.user_id','=',$user_id)->where('user_training_catalog.company_id','=',$companyid);
		if($itemid!=0 && $traningtype_id!=5){
			$result=$result->where('user_training_catalog.item_id','=',$itemid);
		}else{
			$result=$result->where('user_training_catalog.program_id','=',$assessmentid);
			
		}
		$result=$result->get();
		return $result;
	}
	
	
	public static function creditTrainingprogramByClassroom($userEnrolledForLearningplansid,$trainingprograms,$userprogramid,$compid)
	{
		
		$data = array(
			'user_id'              	=>	$userEnrolledForLearningplansid,
			'program_id'     		=> 	$trainingprograms['training_program_id'],
			'is_enroll'            	=> 	1,
			'is_approved'          	=> 	0,
			'company_id'           	=>	$compid,
			'item_id'     			=> 	$trainingprograms['item_id'],
			'training_type_id'      => 	2,
			'user_program_type_id'  =>	$userprogramid,
			'training_status_id'	=>	1,
			'session_id'          	=> 	$trainingprograms['session_id'],
			'last_modfied_by' 		=> 	$trainingprograms['lastmodifyuser_id'],
			'last_modified_time' 	=> 	date ( 'Y-m-d H:i:s' ),
			'created_date' 			=> 	date ( 'Y-m-d H:i:s' ),
			'learning_plan_id' 		=> 	0,
			'blended_program_id' 	=> 	0,
			'assignment_id' 		=> 	0,
			'is_active' 			=> 	0,
		);
		$userTrainingSave=UsertrainingCatalog::create($data);
		
		return $userTrainingSave->user_training_id;
	}
	
	public static function creditTrainingprogram($trainingprograms,$compid,$usersid,$itemid,$sessionuserid)
	{
		
		$progress =(isset($trainingprograms['progress'])) ? $trainingprograms['progress'] : 1;
		if($progress!=1) { $progress = 3; }
		$data = array(
			'user_id'               => $usersid,
			'program_id'      		=> ($trainingprograms['training_program_id']) ? $trainingprograms['training_program_id'] : 0,
			'is_enroll'             => 1,
			'is_approved'           => 0,
			'company_id'            => $compid,
			'training_type_id'      => 1,
			'user_program_type_id'  => 4,
			'training_status_id'	=> $progress,
			'last_modfied_by'       => $sessionuserid,
			'item_id'             	=> $itemid,
			'last_modfied_by' 		=> $trainingprograms['lastmodifyuser_id'],
			'last_modified_time' 	=> date ( 'Y-m-d H:i:s' ),
			'created_date' 			=> date ( 'Y-m-d H:i:s' ),
			'class_schedule_id'     => 0,
			'learning_plan_id'      => 0,
			'blended_program_id'    => 0,
			'assignment_id'         => 0,
		);
		$userTrainingSave=UsertrainingCatalog::create($data);
		
		return $userTrainingSave->user_training_id;
	}
	
	
	 public static function addassessmentinutc($trainingprograms,$compid,$usersid,$assessid,$sessionuserid)
	{
		
		$progress =(isset($trainingprograms['progress'])) ? $trainingprograms['progress'] : 1;
		if($progress!=1) { $progress = 3; }
		$data = array(
			'user_id'               => $usersid,
			'program_id'      		=> $assessid,
			'is_enroll'             => 1,
			'is_approved'           => 0,
			'company_id'            => $compid,
			'training_type_id'      => 5,
			'user_program_type_id'  => 4,
			'training_status_id'	=> $progress,
			'last_modfied_by'       => $sessionuserid,
			'item_id'             	=> 0,
			'last_modfied_by' 		=> $trainingprograms['lastmodifyuser_id'],
			'last_modified_time' 	=> date ( 'Y-m-d H:i:s' ),
			'created_date' 			=> date ( 'Y-m-d H:i:s' ),
			'class_schedule_id'     => 0,
			'learning_plan_id'      => 0,
			'blended_program_id'    => 0,
			'assignment_id'         => 0,
		);
		$userTrainingSave=UsertrainingCatalog::create($data);
		
		return $userTrainingSave->user_training_id;
	}
	
	public static function checkforexistingentery($alldata)
	{
		$itemid = $alldata['item_id'];
		$training_cat_id = $alldata['training_category_id'];
		$companyid = $alldata['company_id'];
		$result=ItemMasterTrainingcategory::whereitemId($itemid)->wheretrainingCategoryId($training_cat_id)->wherecompanyId($companyid)->whereisActive(1)->get();
		if(isset($result[0])){
			$result = $result[0]->item_master_trainingcategory_id;
		} 
		return $result;
	}
	
	
	public static function  getcomparedata($alldata)
	{
		
		$itemid = $alldata['item_id'];
		$user_id = $alldata['user_id'];
		$companyid = $alldata['company_id'];
		$completion_date = $alldata['completion_date'];
		
		$result = UserTranscript::whereitemId($itemid)->whereuserId($user_id)->wherecompletionDate($completion_date)->wherecompanyId($companyid)->get();
		return $result;
	}
	
	
}