<?php
namespace App\Http\Traits;
use Mail,DB,URL,CommonHelper,Mylearningfunctions;
use App\Models\CompanySettings;
use App\Models\AssessmentGroup;
use App\Models\AssessmentUser;
use App\Models\AssessmenturlMaster;
use App\Models\UserassessmentProp;
use App\Models\UserTranscript;
use App\Models\UsertrainingCatalog;
use App\Models\AssessQuestion;
use App\Models\AssessanswertypeMaster;
use App\Models\UserassessmentMaster;


trait Requirement{
	
	public static function commonrequirement($compid,$userid,$roleid){
		$requirement_data = array();
		$baseUrl = URL::to('/');
		$Getdate_formatecomapny = CommonHelper::getdate($compid);
		$buttonclass = 'button_lblue_r4';
		$allAssignmentArr = Mylearningfunctions::getAllAssignmentArr($userid, $compid);	
		$requirmentSettings = CompanySettings::where('company_id','=',$compid)->select('lp_complete', 'assessment_complete', 'assignment_complete')->first();	
		 if(count($allAssignmentArr)>0){
		   foreach($allAssignmentArr as $assignment){
			   if($requirmentSettings->assignment_complete == 1 && $assignment['imagname']=='u184.png') continue;
			   $assignment_str = array();
			   $assignment_str[] = stripcslashes($assignment['assignmentName']);
			   $assignment_str[] = $assignment['training_type'];
			   $assignment_str[] = (isset($assignment['credit_value'])) ? $assignment['credit_value'] : 0;
			   $assigned_date=(($assignment['assigned_date']!='') ? date($Getdate_formatecomapny,strtotime($assignment['assigned_date'])) : "");	
			   $assignment_str[] =$assigned_date;
				
			 
			   if($assignment['imagname'] == 'u184.png'){
				 $assignment_str[]=(($assignment['due_date']!='') ? date($Getdate_formatecomapny,strtotime($assignment['due_date'])) : "-");
			   }else{
				 $assignment_str[] = (($assignment['due_date']!='') ? date($Getdate_formatecomapny,strtotime($assignment['due_date'])) : "-");
			   }
			   $arrhidestatus=array(71,83,84);
				if(!in_array($compid,$arrhidestatus))
				{
					$assignment_str[] =  (isset($assignment['assignmentstatus'])) ? $assignment['assignmentstatus'] : '';
				}
			   $assignment['imagname'] = (isset($assignment['imagname'])) ? $assignment['imagname'] : '';
			   $title ='';
			   if($assignment['imagname']=='flag_red.png'){
					$title ='Item Incomplete';
			   }elseif($assignment['imagname']=='u184.png'){
					$title ='Item Complete';
					$assignment['imagname'] = 'flag_green.png';
			   }elseif($assignment['imagname'] == 'flag_orange.png'){
					$title ='Due Date Passed';
			   }elseif($assignment['imagname'] == 'fail_icon2.png'){
					$title ='Item Failed';
			   }
			   $assignment_str[] =  '<img title="'.$title.'" src="'.$baseUrl.'/user/assets/images/'.$assignment['imagname'].'" />';
			   $redpage='assignment';
			   if($assignment['training_type_id'] == 4){
					$viewId = ''.$userid.'#'.$roleid.'||'.$redpage.'||'.$assignment['blended_item_id'].'||2'.'||requirement';
					$encoded_viewId =CommonHelper::getEncode($viewId);
					$url = $baseUrl.'/#/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId;
					$assignment_str[] =  '<a class="change_col '.$buttonclass.'" href="'.$baseUrl.'/#/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId.'">View<span>&#187;</span></a>';
				}elseif($assignment['training_type_id'] == 1){
					$viewId = ''.$userid.'#'.$roleid.'||'.$redpage.'||'.$assignment['training_program_id'].'';
					$encoded_viewId = CommonHelper::getEncode($viewId);
					$url = $baseUrl.'/#/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId;
					$assignment_str[] =  '<a class="change_col '.$buttonclass.'" href="'.$baseUrl.'/#/elearningmedia/training/viewId/'.$encoded_viewId.'">View<span>&#187;</span></a>';
				}else{
					$viewId = ''.$userid.'#'.$roleid.'||'.$redpage.'||'.$assignment['item_id'].'||'.$assignment['class_id'].'';
					$encoded_viewId = CommonHelper::getEncode($viewId);
					$url = $baseUrl.'/#/trainingcatalog/blendedprogram/viewId/'.$encoded_viewId;
					$assignment_str[] =  '<a class="change_col '.$buttonclass.'" href="'.$baseUrl.'/#/classroomcourse/training/viewId/'.$encoded_viewId.'">View<span>&#187;</span></a>';
				}
			   
				/* $requirement_data[] = array(
					"id"=>$assignment['item_id'].'||'.'assignment'.'||'.$assignment_str[4],
					"data"=>$assignment_str
				); */
				
				$finalArray=array('assigments'=>$assignment_str[0],'training_type'=>$assignment_str[1],'credits'=>$assignment_str[2],'assigned'=>$assignment_str[3],'due_date'=>$assignment_str[4],'status'=>$assignment_str[5],'flag'=>$assignment_str[6],'view'=>$assignment_str[7]);
				$requirement_data[] = $finalArray;
		   }
	   } 
	      
		  $allAssessments = Mylearningfunctions::getAllAssessments($compid,$userid);
		  if(count($allAssessments)>0){
			  $i=1;
			foreach($allAssessments as $key => $assessments){
				if($requirmentSettings->assessment_complete == 1 && $assessments['completionStatus'] == 'Complete') continue ;
				$encodeduserid 	= $assessments['encodeduserid'];
				$token = $assessments['token'];
				$close_date_time = strtotime($assessments['close_date']);
				$due_date_time = strtotime($assessments['due_date']);
				$allow_assessment = $assessments['allow_assessment'];
				$assementdetails = Requirement::getUserAssessment($encodeduserid, $token, $compid);
				$nooftimes = $assementdetails['no_of_times'];					
				$data = array();					
				$data[] = stripcslashes($assessments['assessment_name']);
				$data[] = stripcslashes('Assessment');	
                $data[] = $assessments['credit_value'];		
                $data[] = $assessments['date_sent'];				
				if($assessments['completionStatus'] == 'Incomplete') {
				$data[] = $assessments['due_date'];
				}else{
				$data[] =  $assessments['due_date'];
				}
				
				$arrhidestatus=array(71,83,84,34);
				if(!in_array($compid,$arrhidestatus))
				{
				 $data[] = $assessments['assesStatus'];
				}
				
				if($assessments['completionStatus'] == 'Incomplete') {
				  if(strtotime(str_replace('-', '/', $assessments['due_date']))>=strtotime(date('m/d/Y'))){
					
					$data[] = '<img title = "Item Incomplete" src="'. $baseUrl.'/user/assets/images/flag_red.png"/>';
                    }else{
						
					$data[] = '<img title = "Due Date Passed" src="'. $baseUrl.'/user/assets/images/flag_orange.png"/>';
                    }					
				}else{
				    
					$data[] = '<img title="Item Complete" src="'. $baseUrl.'/user/assets/images/flag_green.png"/>';
				}					
				$url =$baseUrl.'/#/survey/viewassessment/user/'.$encodeduserid.'/token/'.$token.'/red_page/learnassessments';
				if($allow_assessment==1)
				{
					if(empty($nooftimes))
					{
						$data[] = '<a class="change_col '.$buttonclass.'" href="'.$url.'">View<span>&#187;</span></a>';/* '<a href="'.$url.'" target="_blank" ><div class="hand '.$buttonclass.'" >View<span>&#187;</span></div></a>'; */
						
					}
					else
					{
						$data[] = '<div class="hand '.$buttonclass.'" onclick="redirect1(\''.$encodeduserid.'\',\''.$token.'\');">View<span>&#187;</span></div>';
					}
				}
				if($allow_assessment==0)
				{
					if(empty($nooftimes))
					{
						$data[] = '<a class="change_col '.$buttonclass.'" href="'.$url.'">View<span>&#187;</span></a>';/* '<a href="'.$url.'" target="_blank" ><div class="hand '.$buttonclass.'" >View<span>&#187;</span></div></a>' */
						
						
						 
					}
					else
					{
						$data[] = '<div class="hand '.$buttonclass.'" onclick="redirect1(\''.$encodeduserid.'\',\''.$token.'\');">View<span>&#187;</span></div>';
					}
				}
				/* $requirement_data[] = array(
					"id"=>$i,
					"data"=>$data
				); */
				$i++;
				$finalArray=array('assigments'=>$data[0],'training_type'=>$data[1],'credits'=>$assignment_str[2],'assigned'=>$data[3],'due_date'=>$data[4],'status'=>$data[5],'flag'=>$data[6],'view'=>$data[7]);
				$requirement_data[] = $finalArray;
				
			}
			  
		  }
		 
	   return $requirement_data;
		   
	}
	
	 public static function getallassessmentsbyuser($compid, $userid){
		$result = $assessmentArr = array();
		$assessmentAssigned = Requirement::assessmentAssignedtoUser($compid, $userid);
		if(count($assessmentAssigned))
		{
			$assessStr = implode(',',$assessmentAssigned);
			$sql1 = "select if(a.due_date >= au.due_date,a.due_date,au.due_date) AS max_due_date, a.id, a.allow_assessment, a.hide_assessonrequ, a.score_type_id, a.pass_percentage, au.due_date, au.date_sent, a.assessment_name, a.assess_status_id, a.credit_value, a.assessment_desc, asp.close_date,a.user_ques_view  from assessment a
			join assessment_survey_prop asp on a.id=asp.assess_id
			join assessment_users au on a.id=au.assess_id and au.is_active=1 and au.is_delete=0 and au.company_id='$compid' and au.user_id='$userid'
			where a.is_delete=0 and a.company_id='$compid' and a.id in ($assessStr) group by a.id";
			
			$sql2 = "select if(a.due_date >= ag.due_date, a.due_date,ag.due_date) AS max_due_date, a.id, a.allow_assessment, a.hide_assessonrequ, a.assess_status_id, a.score_type_id, ag.due_date, ag.date_sent, a.assessment_name, a.pass_percentage,a.user_ques_view,a.credit_value, a.assessment_desc, asp.close_date from assessment a
			join assessment_survey_prop asp on a.id=asp.assess_id
			join assessment_groups ag on a.id=ag.assess_id and ag.is_active=1 and ag.is_delete=0 and ag.company_id='$compid'
			join user_group ug on ag.group_id=ug.group_id and ug.user_id='$userid' and ug.is_active=1
			where a.is_delete=0 and a.company_id='$compid' and a.id in ($assessStr) and asp.close_date>=ug.created_date group by a.id";
			
			$result1 = DB::select($sql1);
			$result2 = DB::select($sql2);
			$result3 = array_merge($result1,$result2);	
			
			$assessIdArr = array();
			
			foreach($result3 as $rest)
			{
				$res  = (array)$rest;
				if(in_array($res['id'],$assessIdArr))
				{
					$date1 = strtotime($res['max_due_date']);
					$date2 = strtotime($assessmentArr[$res['id']]['due_date']);
					if($date1 > $date2)
						$assessmentArr[$res['id']]['due_date'] = $res['due_date'];
				}
				else
				{
					$tempArr = array();
					$tempArr['id'] 				= $res['id'];
					$tempArr['assessment_name'] = $res['assessment_name'];
					$tempArr['due_date'] 		= $res['due_date'];
					$tempArr['date_sent'] 		= $res['date_sent'];
					$tempArr['credit_value'] 	= $res['credit_value'];
					$tempArr['assessment_desc'] = $res['assessment_desc'];
					$tempArr['close_date'] 		= $res['close_date'];
					$tempArr['allow_assessment']	= $res['allow_assessment'];
					$tempArr['hide_assessonrequ']	= $res['hide_assessonrequ'];
					$tempArr['score_type_id'] 		= $res['score_type_id'];
					$tempArr['user_ques_view']		= $res['user_ques_view'];
					$tempArr['pass_percentage']		= $res['pass_percentage'];
					$assessmentArr[$res['id']] 		= $tempArr;
					array_push($assessIdArr,$res['id']);
				}
			}   
		}
           return $assessmentArr;
		 
	}
	
	
	public static function assessmentAssignedtoUser($compid, $userid)
	{
		$result = AssessmentUser::where('user_id','=',$userid)->where('is_active','=',1)->where('is_delete','=',0)->where('company_id','=',$compid)->get();
		
		$result2 = AssessmentGroup::join('user_group','user_group.group_id','=','assessment_groups.group_id')->where('assessment_groups.is_active','=',1)->where('assessment_groups.is_delete','=',0)->where('assessment_groups.company_id','=',$compid)->where('user_group.user_id','=',$userid)->where('user_group.is_active','=',1)->distinct('assessment_groups.assess_id')->get();
		
		$result3 = array_merge($result->toArray(),$result2->toArray());
		$assArr = array();
		foreach($result3 as $res)
			array_push($assArr,$res['assess_id']);
		$assArr = array_unique($assArr);
		return $assArr;
	}
	
	
	public static function getassmentData($userid, $compid, $assessId)
	{
		$sql = "SELECT distinct art.score,uap.total_questions,art.`status` ,art.created_date ,assmnt.assessment_name,assmnt.score_type_id,assmnt.pass_percentage ,cc.status as ccstatus ,utc.program_id,utc.user_program_type_id,upt.type_name as user_program_type,assmnt.certificate_template_id
		FROM user_transcript ut 
		JOIN  user_training_catalog utc on utc.user_training_id= ut.user_training_id and utc.company_id=$compid
		JOIN assessment assmnt ON (ut.registration_id = assmnt.id) and assmnt.company_id=$compid
		join user_assessment_prop as uap  on uap.assess_id=assmnt.id and uap.user_id=ut.user_id
		left JOIN user_program_type upt ON (upt.user_program_type_id = utc.user_program_type_id)
		left join assessment_response_transcript as art on art.assess_id=assmnt.id
		LEFT JOIN certificate_content as cc on assmnt.certificate_template_id = cc.id and cc.status = 1 and cc.company_id=$compid
		WHERE ut.registration_id =$assessId AND ut.is_active = 1 AND assmnt.is_delete = 0 AND assmnt.company_id =$compid AND ut.company_id = $compid AND ut.user_id = $userid AND ut.is_blended = 0 ";
		$result = DB::select($sql);
		return $result;
	}
	
	
	
	public static function fetchgeneratedurl($compid, $assessid, $urltype, $scheid)
	{
		$sql = AssessmenturlMaster::join('assessment','assessment.id','=','assessment_url_master.assess_id')->where('assessment.company_id','=',$compid)->where('assessment_url_master.assess_id','=',$assessid)->where('assessment_url_master.url_type','=',$urltype)-> where('assessment_url_master.assess_schedule_id','=',$scheid)->select('assessment_url_master.*')->get();
		return $sql;
	}
	
	
	public static function getvalueofsubmiteddata($assessid, $userid, $compid) 
	{
	     $sql = UserassessmentProp::join('user_transcript as ut',function($join) use ($userid,  $compid) {
			  $join->on('user_assessment_prop.assess_id','=','ut.registration_id')->where('ut.user_id','=',$userid)->where('ut.is_blended','=',0)->where('ut.company_id','=',$compid);	  
		})->where('user_assessment_prop.user_id','=',$userid)->where('user_assessment_prop.company_id','=',$compid)->where('user_assessment_prop.assess_id','=',$assessid)->where('user_assessment_prop.is_completed','=',1)->where('ut.is_active','=',1)->groupBy('ut.user_id','ut.registration_id')->select('user_assessment_prop.*')->get();		
		return $sql;
	}
	
	
	public static function getuserassessmentsummary($assessid, $userid, $compid,$prop_id = 0) 
	{
		$sql = UserassessmentProp::where('user_id','=',$userid)->where('company_id','=',$compid)->where('is_completed','=',1)->where('assess_id','=',$assessid);
		if($prop_id > 0){
		 $sql->where('id','=',$prop_id);
		}
		 $result = $sql->select('assess_id','due_date', 'total_questions', 'attempted_questions', 'correct_answers', 'is_completed', 'no_of_times','is_delete','last_updated_date')->get();
		return $result;
	}
	
	
	public static function getcompletiondate($assessid, $userid, $compid) 
	{
		$sql = UserTranscript::where('user_id','=',$userid)->where('registration_id','=',$assessid)->where('is_blended','=',0)->where('company_id','=',$compid)->where('is_active','=',1)->groupBy('registration_id','completion_date','user_id')->select('completion_date')->get();
		return $sql;
	}
	
	
	
	public static function getUserAssessment($userenc, $randomurlvalue, $compid){
		if($userenc != '')
			$userid = CommonHelper::decode($userenc);
		    $resauth=array();
			$sqlcheckauth = "select ua.attempt_taken as user_attemt_taken,uap.no_of_times, au.due_date AS max_due_date,au.date_sent,asp.close_date,aum.assess_schedule_id,
		a.* from assessment a
		join assessment_url_master aum on a.id=aum.assess_id and aum.url_id='$randomurlvalue'
		join assessment_survey_prop asp on a.id=asp.assess_id
		join assessment_users au on a.id=au.assess_id and au.is_active=1 and au.is_delete=0 and au.user_id='$userid' and au.company_id='$compid'
		left join user_assessment ua on ua.assess_id=a.id and ua.user_id='$userid' and ua.is_delete=0 and ua.company_id='$compid'
		left join user_assessment_prop uap on uap.assess_id =a.id and uap.is_delete=0 and uap.user_id='$userid' and uap.company_id='$compid'
		where a.is_delete=0 and a.company_id='$compid'"; 		
		$result1=DB::select($sqlcheckauth);
		
	    $sqlcheckauth1 = "select ua.attempt_taken as user_attemt_taken,uap.no_of_times, ag.due_date AS max_due_date,ag.date_sent,asp.close_date,aum.assess_schedule_id,
		a.* from assessment a
		join assessment_url_master aum on a.id = aum.assess_id and aum.url_id='$randomurlvalue'
		join assessment_survey_prop asp on a.id=asp.assess_id
		join assessment_groups ag on a.id=ag.assess_id and ag.company_id='$compid' and ag.is_active=1 and ag.is_delete=0 
		join user_group ug on ug.group_id = ag.group_id and ug.user_id='$userid' and ug.is_active=1
		left join user_assessment ua on ua.assess_id=a.id and ua.user_id='$userid' and ua.is_delete=0 and ua.company_id='$compid'
		left join user_assessment_prop uap on uap.assess_id =a.id and uap.is_delete=0 and uap.user_id='$userid' and uap.company_id='$compid'
		where a.is_delete=0 and a.company_id='$compid'";
		$result2=DB::select($sqlcheckauth1);
		
		$sqlcheckauth = "select  ua.attempt_taken as user_attemt_taken,uap.no_of_times, ua.due_date AS max_due_date,ua.date_sent,asp.close_date,aum.assess_schedule_id,
		a.* from assessment a
		join assessment_url_master aum on a.id = aum.assess_id and aum.url_id='$randomurlvalue'
		join assessment_survey_prop asp on a.id=asp.assess_id
		join user_assessment ua on ua.assess_id=a.id and ua.is_delete=0 and ua.company_id='$compid' and ua.user_id='$userid'
		join user_master as um on um.user_id='$userid' and um.is_active=1 and um.is_delete=0 and um.company_id='$compid'
		left join user_assessment_prop uap on uap.assess_id =a.id and uap.is_delete=0 and uap.user_id='$userid' and uap.company_id='$compid'
		where a.is_delete=0 and a.company_id='$compid'";
		$result3=DB::select($sqlcheckauth);
		
		if(count($result1))
			$resauth = $result1;
		elseif(count($result2))
			$resauth = $result2;
		else
			$resauth = $result3;
		
		$assessmentarray = array();
		
		if(count($resauth)>0){
			foreach($resauth as $values){
				$assessid=$values->id;
				$assessmentarray['itemtypeid'] 			= "6";
				$assessmentarray['id'] 					= $values->id;
				$assessmentarray['assess_schedule_id']  = $values->assess_schedule_id;
				$assessmentarray['assessment_name']     = $values->assessment_name;
				$assessmentarray['assessment_desc']     = $values->assessment_desc;
				$assessmentarray['pass_percentage']     = $values->pass_percentage;
				$assessmentarray['score_type_id']       = $values->score_type_id;
				$assessmentarray['due_date']  			= $values->max_due_date;
				$assessmentarray['attempt_taken']  		= $values->attempt_taken;
				$assessmentarray['date_sent']  			= $values->date_sent;
				$assessmentarray['no_of_times']  		= $values->no_of_times;
				$assessmentarray['user_attemt_taken']  	= $values->user_attemt_taken;
				$assessmentarray['allow_transcript']  	= $values->allow_transcript;
				$assessmentarray['close_date'] 			= $values->close_date;
				$assessmentarray['allow_assessment'] 	= $values->allow_assessment;
				$userassessment = Requirement::fetchuserassessfromtrainingcatalog($userid, $assessid, $compid);
				
				if(count($userassessment) == 0) 
				{
					$usertrainingdata = array();
					$usertrainingdata['program_id'] 			= $assessid;
					$usertrainingdata['item_id'] 				= 0;
					$usertrainingdata['learning_plan_id'] 		= 0;
					$usertrainingdata['blended_program_id'] 	= 0;
					$usertrainingdata['assignment_id'] 			= 0;
					$usertrainingdata['training_type_id'] 		= 0;
					$usertrainingdata['user_program_type_id'] 	= 5;
					$usertrainingdata['user_id'] 				= $userid;
					$usertrainingdata['company_id'] 			= $compid;
					$usertrainingdata['training_status_id'] 	= 6;
					$usertrainingdata['created_date'] 			= date('Y-m-d -h-i-s');
				    Requirement::insertanywhere('UsertrainingCatalog', $usertrainingdata);
				}
				
				$prop_id = 0 ;
				$questionarray = array();
				$fetchtotalquestionsofassesment =Requirement::fetchassessmentquestions($compid, $assessid);
				if($resauth[0]->randomize_ques == 1)
				{
					$fetchtotalquestionsofassesment = Requirement::shuffle_with_keys($fetchtotalquestionsofassesment);
				}
				
				if(count($fetchtotalquestionsofassesment)>0){
					foreach($fetchtotalquestionsofassesment as $value){
						$question = array();
						$questid 						= $value->ques_id;
						$question['question_id']  		= $questid;
						$question['ques_no']			= $value->ques_no;
						$question['question']			= $value->question;
						$question['answer']				= $value->answer;
						$question['descriptive']		= $value->descriptive;
						$question['question_type_id']	= $value->question_type_id;
						$question['random_answers']		= $value->random_answers;
						$question['question']			= $value->question;
						$fetchansweroptions = Requirement::fetchansweroptions($compid, $assessid, $questid);
						
						$answeroptions 	= array();
						if(count($fetchansweroptions)>0)
						{
							foreach($fetchansweroptions as $optionsval)
							{
								$options = array();
								$options['answer_id']		=	$optionsval->answer_id;
								$options['answer']			=	$optionsval->answer;
								$options['random_answers']	=	$optionsval->random_answers;
								$options['description']		=	$optionsval->description;
								$options['is_correct']		=	$optionsval->is_correct;
								$answeroptions[]			=	$options;
							}
						}
						
						$question['ansoptions']		=	$answeroptions;
						$fetchuserans	= Requirement::usersquestionsans($assessid, $userid, $compid, $questid);
						$prop_id = 0 ;
						if(count($fetchuserans))
							$prop_id = $fetchuserans[0]->user_assessment_prop_id;
						    $useranswer 	= array();
							
							if(count($fetchuserans)>0)
						    {
							foreach($fetchuserans as $useroptionsval)
							{
								$ansoptions = array();
								$ansoptions['question_id']			=	$useroptionsval->question_id;
								$ansoptions['answer']				=	$useroptionsval->answer;
								$ansoptions['descriptive']			=	$useroptionsval->descriptive;
								$useranswer[]		=	$ansoptions;
							}
						   }
						$question['userans']		=	$useranswer;
						$questionarray[]=$question;
						
						
					}	
				}
			     $assessmentarray['questionarray']  			= $questionarray;
				$fetchnooftimes = Requirement::getuserassessmentsummary($assessid, $userid, $compid,$prop_id);
				if(count($fetchnooftimes) > 0 and $fetchnooftimes[0]->is_delete == 0) 
				{
					$assessmentarray['nooftimes'] 		= $fetchnooftimes[0]->no_of_times;
					$totalquestion						= $fetchnooftimes[0]->total_questions;
					$correctanswers						= $fetchnooftimes[0]->correct_answers;
					if($totalquestion!=0)
					{
						$totalrightper					= ($correctanswers*100)/$totalquestion;
					} else {
						$totalrightper = null;
					}
					$totalrightpercent					= number_format($totalrightper,2);
					$assessmentarray['totalquestion'] 	= $totalquestion;
					$assessmentarray['correctanswers'] 	= $correctanswers;
					$assessmentarray['iscompleted'] 	= $fetchnooftimes[0]->is_completed;
					$assessmentarray['lastmodifydate']	= $fetchnooftimes[0]->last_updated_date;
					$assessmentarray['totalrightper']	= $totalrightpercent;
				}
				else
				{
					$assessmentarray['iscompleted']   = 0;
				}     
		   }
		}
		
		return $assessmentarray;
	}
	
	
	public static function fetchuserassessfromtrainingcatalog($edituserid, $assessid, $compid)
	{
	    $sql = UsertrainingCatalog::where('user_id','=',$edituserid)->where('program_id',$assessid)->where('user_program_type_id','=',5)->where('company_id','=',$compid)->get();
		return $sql;
	}
	
	public static function insertanywhere($mytable, array $data){
	     $mytable::create($data);
	}
	
	public static function fetchassessmentquestions($compid, $assessid)
	{ 
		$sql = AssessQuestion::join('assessment as a','a.id','=','assess_question.assess_id')->where('A.company_id','=',$compid)->where('A.is_delete','=',0)->where('assess_question.assess_id','=',$assessid)->where('assess_question.is_delete','=',0)->orderBy('assess_question.manage_order')->select('assess_question.ques_id', 'assess_question.ques_no', 'assess_question.question', 'assess_question.question_type_id', 'assess_question.answer', 'assess_question.descriptive', 'assess_question.random_answers')->get(); 
	   
		return $sql;
	}
	
	
	public static function shuffle_with_keys(&$array)
	{
		/* Auxiliary array to hold the new order */
		$aux = array();
		/* We work with an array of the keys */
		$keys = array_keys($array->toArray());
		/* We shuffle the keys *///enter code here`
		shuffle($keys);
		/* We iterate thru' the new order of the keys */
		foreach ($keys as $key) {
			/* We insert the key, value pair in its new order */
			$aux[$key] = $array[$key];
			/* We remove the element from the old array to save memory */
			unset($array[$key]);
		}
		/* The auxiliary array with the new order overwrites the old variable */
		$array = $aux;
		return $array;
	}
	
	public static function fetchansweroptions($compid, $assessid, $questid)
	{
        $result = AssessanswertypeMaster::join('assess_question as aq',function($join) use($assessid, $questid){
			    $join->on('aq.ques_id','=','assess_answer_type_master.question_id')->where('aq.assess_id','=',$assessid)->where('aq.ques_id','=',$questid);
			
		})->select('aq.random_answers','aq.answer','assess_answer_type_master.answer_id', 'assess_answer_type_master.description', 'assess_answer_type_master.is_correct')->get();
		
		if (count($result) > 0)
		{
			if ($result[0]->random_answers == 1)
			{
				$res = Requirement::shuffle_with_keys($result);
				return $res;
			}
			else
				return $result;
		}
		  
	}
	
	
	public static function usersquestionsans($assessid, $user_id, $compid, $questionid)
	{
		$sql = UserassessmentMaster::where('assess_id','=',$assessid)->where('user_id','=',$user_id)->where('question_id','=',$questionid)->where('is_active','=',1)->select('user_assessment_master.user_assessment_prop_id','user_assessment_master.assess_id', 'user_assessment_master.question_id', 'user_assessment_master.descriptive', 'user_assessment_master.answer')->get();
		return $sql;
	}
	
	
	
}
