<?php
namespace App\Http\Traits;

use App\Models\ItemMaster;

trait Item{
	
	  
	   public static function additems($mannuallyAddItem,$compid)
		{
			
			if($mannuallyAddItem['course_id']=='')
			{
				$first_word = $mannuallyAddItem['traning_type_id']==1 ? 'E' :'C';
				$get_item_id = Item::getLastIdOfItem();
				$item_id = (@$get_item_id[0]['item_id'])+1;
				$mannuallyAddItem['course_id'] = $first_word.$compid.rand(10,100).$item_id;
			}		
			$data = array(
				'item_name'      		=> @$mannuallyAddItem['title'],
				'item_type_id'      	=>  4,
				'training_type_id'  	=> @$mannuallyAddItem['traning_type_id'],
				'title'       			=> @$mannuallyAddItem['title'],
				'description'       	=> @$mannuallyAddItem['desc'],
				'file_id'       		=> @$mannuallyAddItem['file_id'],
				'is_active'       		=> 1,
				'status_id'				=> 2,
				'item_cost'     		=> @$mannuallyAddItem['cost'],
				'company_id'     		=> $compid,
				'training_code'			=> $compid.'_#_'.@$mannuallyAddItem['course_id'] ,
				'credit_value'     		=> @$mannuallyAddItem['credit_value'],
				'average_rating'     	=> @$mannuallyAddItem['average_rating'],
				'certificate_template_id' => $mannuallyAddItem['certificate_template_id'],
				'created_by'            => $mannuallyAddItem['lastmodifyuser_id'],
				'created_date'          => date('Y-m-d',strtotime($mannuallyAddItem['completion_date'])),
			);
			
			$itemMasterSave=ItemMaster::create($data);
			$last_id = $itemMasterSave->item_id;
			return $last_id;
		 }
		 
		 public static function getLastIdOfItem()
		{
			$result = ItemMaster::orderBy('item_id','desc')->skip(0)->take(1)->select('item_id')->get();
			return $result;
		}
		
		public static function getfileid($itemid,$company_id){
	      $result =ItemMaster::where('item_id','=',$itemid)->where('company_id','=',$company_id)->where('is_active','=',1)->select('file_id')->first();
		   return $result->file_id;
        }
	
}