<?php
namespace App\Http\Traits;

use App\Models\CompanyCmsInfo;
use App\Models\UserMaster;
use App\Models\Assessment;
use App\Models\Course;
use App\Models\UsertrainingCatalog;
use App\Models\AssessanswertypeMaster;
use App\Models\AssessQuestion;
use App\Models\UserassessmentMaster;
use App\Models\UserassessmentProp;
use App\Models\AssessAttachment;
use App\Models\user_assessment;
use App\Models\AssessmenturlMaster;
use App\Models\UsersAssessmentResponse;
use App\Models\UserAssessment;
use App\Http\Traits\Assessmentcrypt;
use DB;

trait Assessments{
	
		 public static function getAssessmentQuizdetails($userid, $programcode, $compid,$prop_id)
		 {
			 $resauth=array();
				
				$lang_id=1;
				$result1=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
				})
				->join('training_type','training_type.training_type_id','=','training_programs.training_type')
				->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
				})
				->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
				})
				->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
				})
				->join('assessment',function($join) use($compid){
					   $join->on('assessment.course_id','=','courses.course_id')
					   ->where('assessment.company_id',$compid);
				})
				->leftjoin('user_assessment_prop',function($join) use($compid,$userid){
					$join->on('user_assessment_prop.course_id','=','assessment.course_id')
					->where('user_assessment_prop.status_id',1)
					->where('user_assessment_prop.user_id',$userid)
					->where('user_assessment_prop.company_id',$compid);
				})
				->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)->where('training_programs.training_program_code','=',$programcode)
				->select(DB::raw('training_programs.training_type as training_type_id,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type,if(training_programs.training_type=1,"ON Demand",CURDATE()) as recursive_date,courses.course_image,course_details.course_description,course_details.prerequeisites,courses.average_rating,training_programs.training_program_code,GROUP_CONCAT(distinct training_category.category_name) as category_name,user_assessment_prop.attempt_taken as user_attemt_taken,user_assessment_prop.no_of_times,assessment.attempts,assessment.view_mode,assessment.score_type_id,assessment.pass_percentage,assessment.allow_transcript,assessment.hide_assessonrequ,assessment.randomize_ques,assessment.time_limit'))->get();
								 
				if(count($result1)) {
					$resauth = $result1;
				}
				
				$assessmentarray = array();
				if(count($resauth)>0) {
				  foreach($resauth as $values)
				   {
						    $assessid=$values->course_id;
							$assessmentarray['itemtypeid'] 			= $values->training_type_id;
							$assessmentarray['id'] 					= $values->course_id;
							$assessmentarray['assessment_name']     = $values->training_title;
							$assessmentarray['assessment_desc']     = $values->course_description;
							$assessmentarray['pass_percentage']     = $values->pass_percentage;
							$assessmentarray['score_type_id']       = $values->score_type_id;
							$assessmentarray['due_date']  			= date('Y-m-d');
							$assessmentarray['attempt_taken']  		= $values->attempts;
							$assessmentarray['date_sent']  			= date('Y-m-d');;
							$assessmentarray['no_of_times']  		= $values->no_of_times;
							$assessmentarray['user_attemt_taken']  	= $values->user_attemt_taken;
							$assessmentarray['allow_transcript']  	= $values->allow_transcript;
							$assessmentarray['close_date'] 			= date('Y-m-d');
							$assessmentarray['allow_assessment'] 	= $values->allow_assessment;
							$assessmentarray['time_limit'] 	= $values->time_limit;

							
				            $questionarray = array();
							$fetchtotalquestionsofassesment = Assessments::fetchassessmentquestions($compid, $assessid);
							$fetchtotalquestionsofassesment = json_decode($fetchtotalquestionsofassesment, true);

							if($resauth[0]->randomize_ques == 1) {
								$fetchtotalquestionsofassesment = Assessments::shuffle_with_keys($fetchtotalquestionsofassesment);
							}
							
							if(count($fetchtotalquestionsofassesment)>0) {
								foreach($fetchtotalquestionsofassesment as $value)
								 {
										$question = array();
										$questid 						= $value['ques_id'];
										$question['question_id']  		= $questid;
										$question['ques_no']			= $value['ques_no'];
										$question['question']			= $value['question'];
										$question['answer']				= $value['answer'];
										$question['descriptive']		= $value['descriptive'];
										$question['question_type_id']	= $value['question_type_id'];
										$question['random_answers']		= $value['random_answers'];
										$question['question']			= $value['question'];
										
										$fetchansweroptions = Assessments::fetchansweroptions($compid, $assessid, $questid);
										$answeroptions=array();
											if(count($fetchansweroptions)>0)
												{
													foreach($fetchansweroptions as $optionsval)
													{
														$options = array();
														$options['answer_id']		=	$optionsval['answer_id'];
														$options['answer']			=	$optionsval['answer'];
														$options['random_answers']	=	$optionsval['random_answers'];
														$options['description']		=	$optionsval['description'];
														$options['is_correct']		=	$optionsval['is_correct'];
														$answeroptions[]			=	$options;
													}
												}
												$question['ansoptions']		=	$answeroptions;
												//143, 1, 1, 6,0
												//echo "$assessid, $userid, $compid, $questid,$prop_id"; exit;
												
												$fetchuserans	= Assessments::usersquestionsans($assessid, $userid, $compid, $questid,$prop_id);
												//echo '<pre>'; print_r($fetchuserans); exit;
												
												if(count($fetchuserans))
												$prop_id = $fetchuserans[0]->user_assessment_prop_id;
											
												$useranswer 	= array();
												if(count($fetchuserans)>0)
													{
														foreach($fetchuserans as $useroptionsval)
														{
															$ansoptions = array();
															$ansoptions['question_id']			=	$useroptionsval->question_id;
															$ansoptions['answer']				=	$useroptionsval->answer;
															$ansoptions['descriptive']			=	$useroptionsval->descriptive;
															$useranswer[]		=	$ansoptions;
														}
													}
													$question['userans']		=	$useranswer;
													$questionarray[]=$question;
								 }
							}
					$assessmentarray['questionarray']  			= $questionarray;
					$fetchnooftimes = Assessments::getuserassessmentsummary($assessid, $userid, $compid,$prop_id);
					if(count($fetchnooftimes) > 0 and $fetchnooftimes[0]->is_delete == 0) 
						{
							$assessmentarray['nooftimes'] 		= $fetchnooftimes[0]->no_of_times;
							$totalquestion						= $fetchnooftimes[0]->total_questions;
							$correctanswers						= $fetchnooftimes[0]->correct_answers;
							if($totalquestion!=0)
							{
								$totalrightper  				= ($correctanswers*100)/$totalquestion;
							} else {
								$totalrightper = null;
							}
							$totalrightpercent					= number_format($totalrightper,2);
							$assessmentarray['totalquestion'] 	= $totalquestion;
							$assessmentarray['correctanswers'] 	= $correctanswers;
							$assessmentarray['iscompleted'] 	= $fetchnooftimes[0]->is_completed;
							$assessmentarray['lastmodifydate']	= $fetchnooftimes[0]->last_updated_date;
							$assessmentarray['totalrightper']	= $totalrightpercent;
						}
						else
						{
							$assessmentarray['iscompleted']   = 0;
						}
				   }
				}
				return $assessmentarray;
		 }
		 
		 public static function fetchuserassessfromtrainingcatalog($edituserid, $assessid, $compid)
	     {
			$result=UsertrainingCatalog::whereuserId($edituserid)->whereprogramId($assessid)->whereuserProgramTypeId(5)->wherecompanyId($compid)->get();
			return $result;
		 }
		 
		 public static function fetchassessmentquestions($compid, $assessid)
		 {
			$result=AssessQuestion::join('assessment','assessment.course_id','=','assess_question.course_id')         
								  ->where('assessment.company_id',$compid)
								  ->where('assess_question.course_id',$assessid)
								  ->where('assess_question.status_id',1)
								  ->select('assess_question.ques_id','assess_question.ques_no','assess_question.question', 'assess_question.question_type_id','assess_question.answer','assess_question.descriptive', 'assess_question.random_answers')
								  ->orderBy('assess_question.manage_order')
								  ->get();
			return $result;
		 }
		 
		public static function shuffle_with_keys(&$array)
		{
				/* Auxiliary array to hold the new order */
				$aux = array();
				/* We work with an array of the keys */
				$keys = array_keys($array);
				/* We shuffle the keys *///enter code here`
				shuffle($keys);
				/* We iterate thru' the new order of the keys */
				foreach ($keys as $key) {
					/* We insert the key, value pair in its new order */
					$aux[$key] = $array[$key];
					/* We remove the element from the old array to save memory */
					unset($array[$key]);
				}
				/* The auxiliary array with the new order overwrites the old variable */
				$array = $aux;
				return $array;
	   }
		 
		public static function fetchansweroptions($compid, $assessid, $questid)
		{
			$result=AssessanswertypeMaster::join('assess_question',function($join) use($assessid,$questid){
				                            $join->on('assess_question.ques_id','=','assess_answer_type_master.question_id')
											  ->where('assess_question.course_id',$assessid)
											  ->where('assess_question.ques_id',$questid);
			})
			                                  
			                                ->where('assess_answer_type_master.active_status',0)
											->select('assess_question.random_answers','assess_question.answer','assess_answer_type_master.answer_id','assess_answer_type_master.description','assess_answer_type_master.is_correct')
											->get()->toArray();
											
			if (count($result) > 0)
			{
				if ($result[0]['random_answers'] == 1)
				{
					$result = Assessments::shuffle_with_keys($result);
					
				}
				else
					return $result;
			}
          return $result;			
		}
		
		public static function usersquestionsansweb($assessid, $userid, $compid, $questid,$answer_id)
		{
			$result=UserassessmentMaster::whereassessId($assessid)->whereuserId($userid)->whereanswer($answer_id)->wherequestionId($questid)->whereisActive(1)
			->select('user_assessment_prop_id','assess_id','question_id','descriptive','answer')
			->get();
			return $result;
			
		}
		
		public static function usersquestionsans($assessid, $user_id, $compid, $questionid,$prop_id)
		{
			 
			$result=UserassessmentMaster::wherecourseId($assessid)->whereuserId($user_id)->wherequestionId($questionid)->wherestatusId(1)->where('user_assessment_prop_id','=',$prop_id)
			->select('user_assessment_prop_id','course_id','question_id','descriptive','answer')
			->get();
			return $result;
		}
		
		public static function getuserassessmentsummary($assessid, $userid, $compid,$prop_id = 0) 
	    {
			$result=UserassessmentProp::whereuserId($userid)->wherecompanyId($compid)->whereisCompleted(1)->wherecourseId($assessid);
			if($prop_id > 0) {
			    $result=$result->where('user_assessment_prop_id',$prop_id);
			}
			$result=$result->get();
			return $result;
		}
	public static function getmyuploadedassessment($compid, $assessid)
		{
			$result=AssessAttachment::join('assessment as assess',function($join) use($compid){
				                       $join->on('assess_attachment.assess_id','=','assess.id')         
									         ->where('assess.company_id',$compid);    
			})
			                         ->where('assess_attachment.assess_id',$assessid)
			                         ->where('assess_attachment.is_active',1)
			                         ->orderBy('assess_attachment.last_updated_date')
									 ->select('assess_attachment.*')
									 ->get();
			return $result;						 
		}
		
		
		public static function checkUserGroupAssign($table,$assess_id,$id,$compid) {
		
		if($table == 'assessment_groups'){
			$whr = 'group_id = '.$id;	
		}
		else{
			$whr = 'user_id = '.$id;
		}

		if($table == 'user_assessment'){
			$andcondition = "";		
		}
		else{
			$andcondition = " and is_active=1 " ;
		}
		$result = DB::select("select * from $table where assess_id=$assess_id and $whr and company_id=$compid $andcondition and is_delete=0"); 
	/* 	$result = $db->fetchAll($sql); */
		return $result;
	} 
	
    public static function insertuserassessment($data){
	     user_assessment::create($data);
    }
	
	public static function fetchgeneratedurl($compid, $assessid, $urltype, $scheid)
	{
		$item_arr = array(); 
		$sql = "select aum.* from assessment_url_master as aum
		join assessment as assess on assess.id=aum.assess_id where assess.company_id=:compid and
		aum.assess_id=:assessid and aum.url_type=:urltype and aum.assess_schedule_id=:scheid";
		$item_arr['assessid'] = $assessid;
		$item_arr['urltype'] = $urltype;
		$item_arr['scheid'] = $scheid;
		$item_arr['compid'] = $compid;
	    $result= DB::select(DB::raw($sql) ,$item_arr);
		return $result;
	}
	
	public static function lastinsertid($data)
	{
		$assessment = AssessmenturlMaster::create($data);
		return $assessment->assess_url_id;
		
	}
	
	public static function checkrandomurlid($randomurlvalue)
	{
		$result=AssessmenturlMaster::whereurlId($randomurlvalue)->get();
		return $result;
	}
	
	public static function attemptedquestions($assessid, $uservalue, $compid) 
	{
			$sql = "select uam.descriptive,uam.answer as useranswer,aq.answer as correctanswer  from user_assessment_master as uam 
			join assess_question as aq on uam.question_id=aq.ques_id and aq.course_id=uam.course_id and uam.user_id=$uservalue
			join assessment as A on A.course_id=aq.course_id 
			where A.company_id = $compid  and aq.course_id=$assessid and aq.status_id=1
			order by aq.ques_id "; 
			$result = DB::select($sql);
			return $result;
	
	}
	public static function fetchassessmenanswerid($assessid,$questionid) 
	{
		$mainresult=AssessQuestion::wherecourseId($assessid)->wherequesId($questionid)->get();
		return $mainresult;
	}
	
	public static function fetchAnswerForResp($assessid, $questionid, $answerid)
	{
		$result=AssessanswertypeMaster::join('assess_question',function($join) use($assessid,$questionid){
		                               $join->on('assess_question.ques_id','=','assess_answer_type_master.question_id')
                                           ->where('assess_question.ques_id',$questionid)									   
                                           ->where('assess_question.course_id',$assessid);								   
		})
		                              ->where('assess_answer_type_master.active_status',0)
		                              ->where('assess_answer_type_master.question_id',$questionid)
		                              ->where('assess_answer_type_master.answer_id',$answerid)
									  ->select('assess_answer_type_master.description')
									  ->first();
									  
									  
        $result = ($result) ? $result->description : '';
		return $result;									  
	}
	
	public static function fetchUserAnswerForMultiple($assessid, $questionid, $userid, $qtype)
	{
		$sql = "select group_concat(uam.answer) as answer from user_assessment_master as uam
		join assess_question as aq on aq.ques_id='$questionid' and aq.course_id='$assessid' and aq.question_type_id='$qtype'
		where uam.status_id=1 and uam.question_id='$questionid' and uam.user_id='$userid'";
		$result = DB::select($sql);
		$result = ($result[0]) ? $result[0]->answer : '';
		return $result;
	}
	
	public static function fetchCorrectAnswerForMultiple($assessid, $questionid, $qtype)
	{
		 $sql = "select group_concat(aatm.answer_id) as answer_id from assess_answer_type_master as aatm
		  join assess_question as aq on aq.ques_id='$questionid' and aq.course_id='$assessid' and aq.question_type_id='$qtype'
		  where aatm.active_status=0 and aatm.question_id='$questionid' and aatm.is_correct=1";
		  $result = DB::select($sql);
		  $result = ($result[0]) ? $result[0]->answer_id : '';
		  return $result;
	}
	
	public static function getassessmentname($compid, $assessid) 
	{
		$result = Assessment::wherecompanyId($compid)->wherecourseId($assessid)->first();
		return $result;
	}
	
	public static function fetchtotalquestionsofassesment($compid, $assessid) 
	{
		$result=AssessQuestion::join('assessment as A',function($join){
			                    $join->on('A.course_id','=','assess_question.course_id');
		})              
		   ->where('A.company_id',$compid)
		   ->where('assess_question.course_id',$assessid)
		   ->where('assess_question.status_id',1)
		   ->where('assess_question.question_type_id','!=',5)
		   ->orderBy('assess_question.ques_id')
		   ->select(DB::raw('count(assess_question.ques_no) as totalquestions'))
		   ->get();
		return $result;					   
	}
	
	public static function countattemptedquestions($assessid, $uservalue, $compid) 
	{
		$sql = "select count(uam.answer) as useranswer,aq.answer as correctanswer  from user_assessment_master as uam join
		assess_question as aq on uam.question_id=aq.ques_id and aq.course_id=uam.course_id and uam.user_id='$uservalue'
		join assessment as A  where A.company_id = '$compid' and A.course_id=aq.course_id
		and aq.course_id='$assessid' and aq.status_id=1 group by uam.question_id order by aq.ques_id ";
		$result = DB::select($sql);
		return $result;
	}
	
	public static function countcorrectattemptedquestions($assessid, $uservalue, $compid) 
	{
		$sql = "select count(uam.answer) as correctanswers from user_assessment_master as uam 
		join assess_question as aq on uam.question_id=aq.ques_id and aq.course_id=uam.course_id 
		join assessment as A  where aq.question_type_id != '2' and aq.question_type_id != '5' and uam.user_id='$uservalue' and aq.answer=uam.answer and A.company_id = '$compid' 
		and A.course_id=aq.course_id and aq.course_id='$assessid' and aq.status_id=1 order by aq.ques_id "; 
		$result = DB::select($sql);
		return $result;
	}
	
	public static function usersassessmentresponse($assessid,$userid,$compid)
	{
		$mainresult =UsersAssessmentResponse::wherecourseId($assessid)->whereuserId($userid)->wherecompanyId($compid)->select('id')->get();
		return $mainresult;
	}
	
	public static function countcorrectattemptedmultiplequestions($assessid, $uservalue, $compid) 
	{
		$mulitplecorrectanscount = 0;
		$questionresult=AssessQuestion::join('assessment',function($join) use($compid){
                                   $join->on('assessment.course_id','=','assess_question.course_id')
                                       ->where('assessment.company_id',$compid);								   
 		               })
					              ->where('assess_question.question_type_id',2)
					              ->where('assess_question.status_id',1)
					              ->where('assessment.course_id',$assessid)
								  ->select('ques_id')
								  ->get();
			
		if(count($questionresult) > 0) {
			$k=0;
			foreach($questionresult as $questionid)
			{
				$ques_id = $questionresult[$k]->ques_id;
			
			
				$multiselectQuestionAns = Assessments::getanswersformultiselect($ques_id, $assessid, 2);
					
				$mutiSelectUserAswer = Assessments::attemptedquestionswithquestionid($assessid, $uservalue, $compid, $ques_id);
				
				$correctans = true;
				$correctansarray = array();
				$useransarray = array();
				foreach($multiselectQuestionAns as $fullvalues) 
				{
					$ans_id = (int)$fullvalues->answer_id;
					$correctvalue=$fullvalues->is_correct;
					if($correctvalue == '1')
					{
						$correctansarray[] = $ans_id;
					}
				}
				foreach($mutiSelectUserAswer as $userans)
				{
					$answerid = (int)$userans->useranswer;
					$useransarray[] = $answerid;
				}
				sort($correctansarray);
				sort($useransarray);
				
				if(count($correctansarray)==count($useransarray))
				{
					$var1 = array_diff($correctansarray, $useransarray);
					if(count($var1) == 0)
					{
						$mulitplecorrectanscount++;
					}
				}
				
				$k++;
			}
			return $mulitplecorrectanscount;
		}
		return $mulitplecorrectanscount;
								  
	}
	
	public static function getanswersformultiselect($questionid, $assessid, $questiontypeid)
	{
		$sql = "select aq.random_answers,aatm.answer_id,aatm.description,aatm.is_correct
		from assess_answer_type_master as aatm
		join assess_question as aq on aatm.question_id=aq.ques_id and aq.course_id='$assessid' and aatm.question_id='$questionid' and aq.question_type_id='$questiontypeid' where aatm.active_status=0";
		$result = DB::select($sql);
		if(count($result) > 0)
		{
			
			if ($result[0]->random_answers == 1) {
				$res = Assessments::shuffle_with_keys($result);
				return $res;
			}
			else
				return $result;
		}
	}
	
	
	
	
	public static function attemptedquestionswithquestionid($assessid, $uservalue, $compid, $questionid) 
	{
		
			$result =UserassessmentMaster::join('assess_question as aq','user_assessment_master.question_id','=','aq.ques_id')
			->join('assessment as A',function($join) use($compid){
			$join->on('A.course_id','=','aq.course_id')
			->on('user_assessment_master.course_id','=','aq.course_id')
				->where('A.company_id',$compid);
			})
			->where('user_assessment_master.user_id',$uservalue)
			->where('user_assessment_master.question_id',$questionid)
			/*  ->where('user_assessment_master.assess_id','=','aq.assess_id') */
			->where('aq.course_id',$assessid)
			->where('aq.status_id',1)
			->orderBy('aq.ques_id')
			->select('aq.question_type_id','user_assessment_master.answer as useranswer','user_assessment_master.descriptive','aq.answer as correctanswer')
			->get();
			return $result;							 
	}
	
	public static function getvalueofsubmiteddata($assessid, $userid, $compid,$usertrainingid) 
	{
	     $result = UserassessmentProp::join('user_transcript as ut',function($join) use($userid,$compid){
			                             $join->on('user_assessment_prop.course_id','=','ut.course_id')
										      ->where('ut.user_id',$userid)
										      ->where('ut.company_id',$compid);
		                            })
								->where('user_assessment_prop.user_id',$userid)
								->where('user_assessment_prop.user_training_id',$usertrainingid)
								->where('user_assessment_prop.company_id',$compid)
								->where('user_assessment_prop.course_id',$assessid)
								->where('user_assessment_prop.is_completed',1)
								->where('ut.status_id',1)
								->groupBy('ut.user_id','ut.course_id')
								->select('user_assessment_prop.*')
								->get();
		return  $result;
									
	}
	
	public static function userassessmentinfo($assessid,$userid,$compid)
	{
		$mainresult = UserAssessment::whereassessId($assessid)->whereuserId($userid)->wherecompanyId($compid)->select('user_assessment_id','attempt_taken as user_count','blended_prog_id')->get();
		return $mainresult;
	}
	
	public static function fetchassessmentofuser($compid, $userid, $assessid) 
	{
	    $mainresult =UsertrainingCatalog::join('user_program_type as upt','upt.user_program_type_id','=','user_training_catalog.user_program_type_id')
		                                 ->join('user_training_status as uts','uts.user_status_id','=','user_training_catalog.training_status_id')
										 ->where('user_training_catalog.user_id',$userid)
										 ->where('user_training_catalog.program_id',$assessid)
										 ->where('user_training_catalog.company_id',$compid)
										 ->where('user_training_catalog.user_program_type_id',5)
										 ->select('user_training_catalog.user_training_id','upt.type_name','user_training_catalog.training_status_id','uts.user_status','user_training_catalog.program_id')
										 ->get();
		return $mainresult;								 
										 
	}
	
	public static function getAssessmentDetailForPopup($compid, $assessid, $user_ques_view)
	{
		$result=Assessment::wherecourseId($assessid)->whereviewMode($user_ques_view)->wherecompanyId($compid)->select('attempts','pass_percentage')->get();
		return $result;
	}
	
	public static function getAssessQuesPopupExceptDescType($assessid)
	{
		$result = AssessQuestion::wherecourseId($assessid)->where('question_type_id','!=',5)->wherestatusId(1)->select(DB::raw('count(ques_id) as total_ques'))->get();
		
		return $result;
	}
	
	public static function fetchDataForReviewAllQues($compid, $assessid, $userid, $quesids)
	{
		$results = array(); $i=0;
		foreach($quesids as $question_id)
		{
			$result =Assessment::join('assess_question as asq',function($join)use($assessid,$question_id){
				                              $join->on('asq.course_id','=','assessment.course_id')
											       ->where('asq.ques_id',$question_id);
			                        })
									->join('user_assessment_master as uam',function($join)use($assessid,$question_id){
				                              $join->on('uam.course_id','=','assessment.course_id')
											       ->where('uam.question_id',$question_id);
			                        })
									->where('assessment.company_id',$compid)
									->where('assessment.course_id',$assessid)
									->where('uam.user_id',$userid)
									->where('uam.status_id',1)
									->select('asq.question_type_id','asq.answer AS correct_response','uam.answer AS student_response')
									->get();
						
									
			if(!empty($result[0])) {
				if($result[0]['question_type_id'] == 6) {
					$res =Assessments::getSelectTypeAnswer($compid, $assessid, $userid, $question_id);
					//print_r($res);exit;
					$results[$i] = $result;
					$results[$i][0]['student_response'] = $res['student_response'];
					$results[$i][0]['correct_response'] = $res['correct_response'];
					$corranswerArr = explode(",", $results[$i][0]['correct_response']);
					$useranswerArr = explode(",", $results[$i][0]['student_response']);
					$results[$i][0]['result'] = 'incorrect';
					if(count($corranswerArr) == count($useranswerArr))
					{
						$k=0; $j=0;
						foreach($corranswerArr as $answerid)
						{
							$j++;
							$answer_id = trim($answerid);
							if(in_array($answer_id,$useranswerArr))
							{ $k++; }
						}
						if($k==$j)
						{ $results[$i][0]->result = 'correct'; }
						}
					}
					else
					{
						$results[$i] = $result;
						
						$results[$i][0]->result = 'incorrect';
						if($results[$i][0]['correct_response'] == $results[$i][0]['student_response'])
						{
							$results[$i][0]->result = 'correct';
						}
					}
				}
				else {
				$res = Assessments::getUserSkipData($compid, $assessid, $userid, $question_id);
				$results[$i] = $res;
				$results[$i][0]->student_response = '';
				$results[$i][0]->result = 'skip';
			}
			//print_r($results[$i][0]);
			$results[$i][0]->qid = $question_id;
			$i++;
			
				
			}
			return $results;
		
	}
	
	public static function getSelectTypeAnswer($compid, $assessid, $userid, $question_id)
	{
		$sql ='SELECT GROUP_CONCAT(answer_id) AS correct_response FROM assessment AS ass INNER JOIN assess_answer_type_master AS asq ON asq.is_correct=1 and asq.question_id='.$question_id.' WHERE (ass.company_id='.$compid.') AND (ass.course_id = '.$assessid.')';
		$result1 = DB::select($sql);
		
		$sql1='SELECT GROUP_CONCAT(answer) AS `student_response` FROM `assessment` AS `ass` INNER JOIN `user_assessment_master` AS `uam` ON uam.assess_id='.$assessid.' and uam.question_id='.$question_id.' WHERE (ass.company_id= '.$compid.') AND (ass.course_id = '.$assessid.') AND (uam.user_id = '.$userid.') AND (uam.status_id = 1)';
		$result2 = DB::select($sql1);
		$results = array();
		$results['student_response'] = ($result2[0]) ? $result2[0]->student_response : '';
		$results['correct_response'] = ($result1[0]) ? $result1[0]->correct_response : '';
		
		return $results;
		
	}
	public static function getUserSkipData($compid, $assessid, $userid, $question_id)
	{
		$sql = 'SELECT `asq`.`question_type_id`, `asq`.`answer` AS `correct_response` FROM `assessment` AS `ass` INNER JOIN `assess_question` AS `asq` ON asq.course_id='.$assessid.' and asq.ques_id='.$question_id.' WHERE (ass.company_id= '.$compid.') AND (ass.course_id = '.$assessid.')';
		$result = DB::select($sql);
		
		if(count($result[0])>0)
		{
			if($result[0]->question_type_id == 6)
			{
				$res = Assessments::getSelectTypeAnswer($compid, $assessid, $userid, $question_id);
				//print_r($res );exit;
				$result[0]->correct_response = $res['correct_response'];
			}
		}
		
		return $result;
	}
	
	public static function fetchmultiselectansbyuser($answ_id,$questionid)
	{
	
	  $sql = "select * from assess_answer_type_master as ff where ff.question_id='$questionid' and ff.answer_id='$answ_id' and ff.active_status = 0";
	  $mainresult = DB::select($sql);
	  return $mainresult;
	}


	public static function fetchmultiselectans($questionid) {
		$sql = "select * from assess_answer_type_master as ff where ff.question_id='$questionid' and ff.active_status = 0";
		$mainresult = DB::select($sql);
		return $mainresult;
	}
	
	public static function fetchUserAssessment($userid, $programcode, $compid,$response_id)
	{
		       $resauth=array();
				$lang_id=1;
				$result1=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
				})
				->join('training_type','training_type.training_type_id','=','training_programs.training_type')
				->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
				})
				->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
				})
				->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
				})
				->join('assessment',function($join) use($compid){
					   $join->on('assessment.course_id','=','courses.course_id')
					   ->where('assessment.company_id',$compid);
				})
				->leftjoin('user_assessment_prop',function($join) use($compid,$userid){
					$join->on('user_assessment_prop.course_id','=','assessment.course_id')
					->where('user_assessment_prop.status_id',1)
					->where('user_assessment_prop.user_id',$userid)
					->where('user_assessment_prop.company_id',$compid);
				})
				->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)->where('training_programs.training_program_code','=',$programcode)
				->select(DB::raw('training_programs.training_type as training_type_id,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type,if(training_programs.training_type=1,"ON Demand",CURDATE()) as recursive_date,courses.course_image,course_details.course_description,course_details.prerequeisites,courses.average_rating,training_programs.training_program_code,GROUP_CONCAT(distinct training_category.category_name) as category_name,user_assessment_prop.attempt_taken as user_attemt_taken,user_assessment_prop.no_of_times,assessment.attempts,assessment.view_mode,assessment.score_type_id,assessment.pass_percentage,assessment.allow_transcript,assessment.hide_assessonrequ,assessment.randomize_ques,assessment.time_limit'))->get();
								 
				if(count($result1)) {
					$resauth = $result1;
				}
		   
		$assessmentarray = array();
		if(count($resauth)>0)
		{
			foreach($resauth as $values)
			{
				            $assessid=$values->course_id;
							$assessmentarray['itemtypeid'] 			= $values->training_type_id;
							$assessmentarray['id'] 					= $values->course_id;
							$assessmentarray['assessment_name']     = $values->training_title;
							$assessmentarray['assessment_desc']     = $values->course_description;
							$assessmentarray['pass_percentage']     = $values->pass_percentage;
							$assessmentarray['score_type_id']       = $values->score_type_id;
							$assessmentarray['due_date']  			= date('Y-m-d');
							$assessmentarray['attempt_taken']  		= $values->attempts;
							$assessmentarray['date_sent']  			= date('Y-m-d');;
							$assessmentarray['no_of_times']  		= $values->no_of_times;
							$assessmentarray['user_attemt_taken']  	= $values->user_attemt_taken;
							$assessmentarray['allow_transcript']  	= $values->allow_transcript;
							$assessmentarray['close_date'] 			= date('Y-m-d');
							$assessmentarray['allow_assessment'] 	= $values->allow_assessment;
							$assessmentarray['time_limit'] 	= $values->time_limit;

				$questionarray = array();
				$fetchtotalquestionsofassesment = Assessments::fetchassessmentquestions($compid, $assessid);
				
				if(count($fetchtotalquestionsofassesment)>0)
				{    
			        
					  
					foreach($fetchtotalquestionsofassesment as $value)
					{	
						$question = array();
						$questid 						= $value->ques_id;
						$question['question_id']  		= $questid;
						$question['ques_no']			= $value->ques_no;
						$question['question']			= $value->question;
						$question['answer']				= $value->answer;
						$question['descriptive']		= $value->descriptive;
						$question['question_type_id']	= $value->question_type_id;
						$question['random_answers']		= $value->random_answers;
						$question['question']			= $value->question;
						
						$fetchansweroptions = Assessments::fetchansweroptions($compid, $assessid, $questid);
						
						$answeroptions 	= array();
						if(count($fetchansweroptions)>0)
						{
							foreach($fetchansweroptions as $optionsval)
							{
								$options = array();
								$options['answer_id']			=	$optionsval['answer_id'];
								$options['answer']				=	$optionsval['answer'];
								$options['random_answers']		=	$optionsval['random_answers'];
								$options['description']			=	$optionsval['description'];
								$options['is_correct']			=	$optionsval['is_correct'];
								$answeroptions[]		=	$options;
							}
						}
					
						$question['ansoptions']		=	$answeroptions;
						$fetchuserans=Assessments::userattemptresdatanew($compid, $assessid,$response_id,$questid,$value->question_type_id);
						$useranswer 	= array();
						if(count($fetchuserans)>0)
						{
							foreach($fetchuserans as $useroptionsval)
							{
								$ansoptions = array();
								$ansoptions['question_id']			=	$useroptionsval['question_id'];
								$ansoptions['answer']				=	$useroptionsval['answer'];
								$ansoptions['descriptive']			=	$useroptionsval['descriptive'];
								$useranswer[]		=	$ansoptions;
							}
						}
						$question['userans']		=	$useranswer;
						$questionarray[]=$question;
					}
				}
				$assessmentarray['questionarray']  			= $questionarray;
				$fetchnooftimes = Assessments::userattemptresdataaaa($assessid,$compid,$response_id) ;
	
				if (count($fetchnooftimes) > 0) 
				{ 
					$totalquestion							= 	!empty($fetchnooftimes[0]['totalquestion'])? $fetchnooftimes[0]['totalquestion']:0;
					$correctanswers							=	!empty($fetchnooftimes[0]['correctanswers'])?$fetchnooftimes[0]['correctanswers']:0;
					$completion_date						=   $fetchnooftimes[0]['completion_date'];
					
					if($totalquestion!=0)
					{ 
				
						$totalrightper						=	($correctanswers*100)/$totalquestion;
					}else{
						$totalrightper=null;
					}
					$totalrightpercent						= 	number_format($totalrightper,2);
					$assessmentarray['totalquestion'] 		=	$totalquestion;
					$assessmentarray['correctanswers'] 		= 	$correctanswers;
					$assessmentarray['iscompleted'] 		= 	1;
					$assessmentarray['lastmodifydate']		=	$completion_date;
					$assessmentarray['totalrightper']		=	$totalrightpercent;
				}
				else{
					$assessmentarray['iscompleted']   = 0;
				}
			}
		}
		
		return $assessmentarray; 
	}
	
   public static function userattemptresdatanew($compid, $assessid,$response_id,$ids,$type_id) 
	{	
		$getres = Assessments::getresultresponsedata($response_id);
		
		$storresult=array();
		$final=array();
		foreach($getres as $res)
		{   
			$jsondata =$res->user_responce; 
			$jsondata = str_replace(array("\r\n", "\n", "\r"),'', $jsondata);
			
			
			$arr = json_decode($jsondata,true);
			
			
			
			 $result=Assessments::makearraynew($arr,$ids);
			
			array_push($storresult,array_values($result));
		}
		 
		
		$useranswer 	= array();
		if($type_id==1)
		{
			foreach($storresult as $useroptionsval)
			{
			 $description=isset($useroptionsval[0]) ? $useroptionsval[0] :'';
			 if($description!=''){
				
			 $res=Assessments::answertypedata($ids,$description);
				$ansoptions = array();
				$ansoptions['question_id']			=	$ids;
				$ansoptions['answer']				=	$res[0]->answer_id;
				$ansoptions['descriptive']			=	'';
				$useranswer[]		=	$ansoptions;
				}
			}
		}
		
		else if($type_id==2)
		{
			foreach($storresult as $useroptionsval)
			{
				$description=isset($useroptionsval[0]) ? $useroptionsval[0] :'';
				if($description!='')
				{
					
						if (strpos($description, '#') !== false) {
						   $des=explode('#',$description);
						}else{
							$des=explode(',',$description);
						}
			
					foreach($des as $dess)
					{
						if(strpos($description, '#') !== false){
						    $res=Assessments::answertypedata($ids,$dess);
						    $res= $res[0]->answer_id;
						}else{
							$res= $dess;
						}
						$ansoptions = array();
						$ansoptions['question_id']			=	$ids;
						$ansoptions['answer']				=   $res;	
						$ansoptions['descriptive']			=	'';
						$useranswer[]		=	$ansoptions;
					}
				}
			}
		}
		else if($type_id==6 || $type_id==3 || $type_id==4 || $type_id==5)
		{
			foreach($storresult as $useroptionsval)
			{
				$description=isset($useroptionsval[0]) ? $useroptionsval[0] :'';
				$ansoptions = array();
				$ansoptions['question_id']			=	$ids;
				$ansoptions['answer']				=	$description;
				$ansoptions['descriptive']			=	'';
				$useranswer[]		=	$ansoptions;
			}
		}
		
	   return $useranswer;
	}
	
	public static function userattemptresdataaaa($assessid,$compid,$response_id) 
	{	
		$getres = Assessments::getresultresponsedata($response_id);
		$fetchtotalquestionsofassesment = Assessments::assessmentquestionsnew($compid, $assessid);
		
		$questionsofassesment=count($fetchtotalquestionsofassesment);
		$questionarray=array();
		if(count($fetchtotalquestionsofassesment)>0)
		{
			foreach($fetchtotalquestionsofassesment as $value)
			{
				$questionid=$value->ques_id;
				$question_no=$value->ques_no;							
				$questionarray[]=$value->ques_id;
			}
		}
		$storresult=array();
		$final=array();
		foreach($getres as $res)
		{
			$jsondata =$res->user_responce;
            $completion_date =$res->completion_date; 			
			$arr = json_decode($jsondata,true);
			$storresult=array();
			foreach($questionarray as $ids)
			{
			$result=Assessments::makearray($arr,$ids);
			array_push($storresult,array_values($result));
			}
			$lock=array();
			$count=1;
			foreach($storresult as $finallre)
			{ 
				if(in_array('Correct',$finallre, TRUE))
				{
				  $lock['res']=  $count++;
				}
		    }
			array_push($final, $lock);
		}
		$passfail=array();
		
		foreach($final as $rsss)
		{
		    $ans = array();
			$ans['totalquestion']	= 	count($fetchtotalquestionsofassesment);
			$ans['correctanswers']	=	isset($rsss['res'])?$rsss['res'] :'';
			$ans['completion_date']	=	$completion_date;
			$passfail[]=$ans;
		}
		return $passfail;
	}
	
	public static function getresultresponsedata($response_id)
	{
		$sql = "select uar.user_responce,ut.completion_date from assessment_response_transcript as arut 
		join user_transcript as ut on ut.transcript_id=arut.transcript_id and ut.status_id=1
		join assessment as a on a.course_id=arut.course_id
		join users_assessment_response as uar on uar.id =arut.user_assess_resp_id
		where uar.id='$response_id'";
		$mainresult = DB::select($sql);
	    return $mainresult;
	}
	
	public static function makearray($arr,$ids)
	{
	    $storresult=array();
		$result=array();
		foreach($arr as $arr1)
		{
			$id=$arr1['qid'];

			if($id==$ids)
			{
			$result=$arr1['result'];
			}else
			{
			$result='';
			}
			if(!empty($result))
			{
			array_push($storresult,$result);
			}
		}
	  return array_values($storresult);
	}
	
	public static function makearrayviewquiznew($arr,$ids)
	{
	    $storresult=array();
		$result=array();
		foreach($arr as $arr1)
		{
			$id=$arr1['qid'];
           
			if($id==$ids)
			{
			//$result=$arr1['student_response']!=0 ? $arr1['student_response'] :'zero' ;
			    $result['student_response']	=$arr1['student_response'];
			    $result['correct_response']	=$arr1['correct_response'];
			    $result['result']			=$arr1['result'];
			 
				
			}
			if(count($result)>0)
			{
				
				   array_push($storresult,$result);
				   $result=array();
			}
			
		}
	
	  return array_values($storresult);
	}
	
	public static function makearraynew($arr,$ids)
	{
	    $storresult=array();
		$result='';
		foreach($arr as $arr1)
		{
			$id=$arr1['qid'];
           
			if($id==$ids)
			{
				
			//$result=$arr1['student_response']!=0 ? $arr1['student_response'] :'zero' ;
			    $result=$arr1['student_response'];
			 
				
			}
			if(!empty($result))
			{
				
				   array_push($storresult,$result);
				   $result=array();
			}
			
		}
	
	  return array_values($storresult);
	}
	
	public static function assessmentquestionsnew($compid, $assessid) 
	{
		$sql = " select aq.ques_id,aq.ques_no,aq.question, aq.question_type_id, aq.answer, aq.descriptive, aq.random_answers
		from assess_question as aq  
		join assessment as A on A.course_id=aq.course_id where A.company_id = '$compid' and 
		aq.course_id='$assessid' and aq.status_id=1 and aq.question_type_id!=5   order by aq.manage_order  "; 
	    $mainresult = DB::select($sql);
	    return $mainresult;
	}
	
	public static function answertypedata($ids,$description)
	{
		$sql = "select aatm.answer_id from  assess_answer_type_master as aatm where aatm.question_id='$ids' and aatm.description='$description' and aatm.active_status =0";
		$mainresult = DB::select($sql);
	    return $mainresult;
	}
	
	/* public static function fetchDataForReviewAllQues($compid, $assessid, $userid, $quesids)
	{
		$results = array(); $i=0;
		foreach($quesids as $question_id)
		{
			$result =Assessment::join('assess_question as asq',function($join)use($assessid,$question_id){
				                              $join->on('asq.course_id','=','assessment.course_id')
											       ->where('asq.ques_id',$question_id);
			                        })
									->join('user_assessment_master as uam',function($join)use($assessid,$question_id){
				                              $join->on('uam.course_id','=','assessment.course_id')
											       ->where('uam.question_id',$question_id);
			                        })
									->where('assessment.company_id',$compid)
									->where('assessment.course_id',$assessid)
									->where('uam.user_id',$userid)
									->where('uam.status_id',1)
									->select('asq.question_type_id','asq.answer AS correct_response','uam.answer AS student_response')
									->get();
						
									
			if(!empty($result[0])) {
				if($result[0]['question_type_id'] == 6) {
					$res =Assessments::getSelectTypeAnswer($compid, $assessid, $userid, $question_id);
					//print_r($res);exit;
					$results[$i] = $result;
					$results[$i][0]['student_response'] = $res['student_response'];
					$results[$i][0]['correct_response'] = $res['correct_response'];
					$corranswerArr = explode(",", $results[$i][0]['correct_response']);
					$useranswerArr = explode(",", $results[$i][0]['student_response']);
					$results[$i][0]['result'] = 'incorrect';
					if(count($corranswerArr) == count($useranswerArr))
					{
						$k=0; $j=0;
						foreach($corranswerArr as $answerid)
						{
							$j++;
							$answer_id = trim($answerid);
							if(in_array($answer_id,$useranswerArr))
							{ $k++; }
						}
						if($k==$j)
						{ $results[$i][0]->result = 'correct'; }
						}
					}
					else
					{
						$results[$i] = $result;
						
						$results[$i][0]->result = 'incorrect';
						if($results[$i][0]['correct_response'] == $results[$i][0]['student_response'])
						{
							$results[$i][0]->result = 'correct';
						}
					}
				}
				else {
				$res = Assessments::getUserSkipData($compid, $assessid, $userid, $question_id);
				$results[$i] = $res;
				$results[$i][0]->student_response = '';
				$results[$i][0]->result = 'skip';
			}
			//print_r($results[$i][0]);
			$results[$i][0]->qid = $question_id;
			$i++;
			
				
			}
			return $results;
		
	} */
     //fetchDataForReviewAllQues($compid, $assessid, $userid, $quesids)
	public static function fetchTranscriptDataForReviewAllQues($compid, $assessid, $userid, $quesids,$response_id) 
	{	
		$result =Assessment::join('assess_question as asq',function($join)use($assessid,$quesids){
				  $join->on('asq.course_id','=','assessment.course_id')
					   ->whereIn('asq.ques_id',$quesids)
					   ->where('asq.status_id',1);
		})->select('asq.question_type_id as type_id','asq.ques_id')->get();
		$useranswer 	= array();
		
		foreach($result as $data)
		{
			$type_id =$data->type_id;
			$ids     =$data->ques_id;
		/* 	echo "$type_id===$ids"; echo "||"; */
			$getres = Assessments::getresultresponsedata($response_id);
			$storresult=array();
			$final=array();
			foreach($getres as $res)
			{   
				$jsondata =$res->user_responce; 
				$jsondata = str_replace(array("\r\n", "\n", "\r"),'', $jsondata);
				$arr = json_decode($jsondata,true);
				$storresult=Assessments::makearrayviewquiznew($arr,$ids);
				//array_push($storresult,array_values($result));
			}

			/* {"question_type_id":1,"correct_response":"10","student_response":"11","result"
			:"incorrect","qid":"6"} */
			
				if($type_id==1)
				{
					if(count($storresult)>0){
						foreach($storresult as $useroptionsval)
						{
						 $description=isset($useroptionsval['student_response']) ? $useroptionsval['student_response'] :'';
						 $correctanse=isset($useroptionsval['correct_response']) ? $useroptionsval['correct_response'] :'';
						 $result=isset($useroptionsval['result']) ? $useroptionsval['result'] :'';
						 if($description!=''){
								$res=Assessments::answertypedata($ids,$description);
								$res1=Assessments::answertypedata($ids,$correctanse);
								$ansoptions = array();
								$ansoptions[0]['qid']			           	=	$ids;
								$ansoptions[0]['student_response']			=	$res[0]->answer_id;
								$ansoptions[0]['correct_response']			=	$res1[0]->answer_id;
								$ansoptions[0]['question_type_id']		   	=	$type_id;
								$ansoptions[0]['result']		   			=	strtolower($result);
								$useranswer[]		=	$ansoptions;
							}
						}
					}else{
						$description=isset($useroptionsval['student_response']) ? $useroptionsval['student_response'] :'';
						$correctanse=isset($useroptionsval['correct_response']) ? $useroptionsval['correct_response'] :'';
						$result=isset($useroptionsval['result']) ? $useroptionsval['result'] :'';
						if($description!=''){
						$res=Assessments::answertypedata($ids,$description);
						$res1=Assessments::answertypedata($ids,$correctanse);
						$ansoptions = array();
						$ansoptions[0]['qid']			           	=	$ids;
						$ansoptions[0]['student_response']			=	'';
						$ansoptions[0]['correct_response']			=	'';
						$ansoptions[0]['question_type_id']		   	=	$type_id;
						$ansoptions[0]['result']		   			=	'skip';
						$useranswer[]		=	$ansoptions;
						
						
					}
				}
			}
			
			else if($type_id==2)
			{
				foreach($storresult as $useroptionsval)
				{
					
					 $description=isset($useroptionsval['student_response']) ? $useroptionsval['student_response'] :'';
				     $description1=isset($useroptionsval['correct_response']) ? $useroptionsval['correct_response'] :'';
					 $result=isset($useroptionsval['result']) ? $useroptionsval['result'] :'';
					  $resall=array();
					if($description!='')
					{
						if (strpos($description, '#') !== false) {
						   $des=explode('#',$description);
						}else{
							$des=explode(',',$description);
						}
						
				        
						foreach($des as $dess)
						{
							if(strpos($description, '#') !== false){
								$res=Assessments::answertypedata($ids,$dess);
								$resall[]= $res[0]->answer_id;
							}else{
								$resall[]= $dess;
							}
							
						}
					}
					
				    $resall1=array();
					if($description1!='')
					{
						
						if (strpos($description1, '#') !== false) {
						   $des1=explode('#',$description1);
						}else{
							$des1=explode(',',$description1);
						}
						
				        
						foreach($des1 as $dess1)
						{
							if(strpos($description1, '#') !== false){
								$res1=Assessments::answertypedata($ids,$dess1);
								$resall1[]= $res1[0]->answer_id;
							}else{
								$resall1[]= $dess1;
							}
							
						}
					}
					
					if($resall1){
						
						$ansoptions = array();
						$ansoptions[0]['qid']			        =   $ids;
						$ansoptions[0]['student_response']		=   implode(',',$resall);	
						$ansoptions[0]['correct_response']		=	implode(',',$resall1);	
						$ansoptions[0]['question_type_id']		=	$type_id;
						$ansoptions[0]['result']		   		=	strtolower($result);
						$useranswer[]		=	$ansoptions;
					}else{
						$ansoptions = array();
						$ansoptions[0]['qid']			        =   $ids;
						$ansoptions[0]['student_response']		=  '';	
						$ansoptions[0]['correct_response']		=	'';	
						$ansoptions[0]['question_type_id']		=	$type_id;
						$ansoptions[0]['result']		   		=	'skip';
						$useranswer[]		=	$ansoptions;
						
					}
				}
			}
			
			else if($type_id==6 || $type_id==3 || $type_id==4 || $type_id==5)
			{  
		        if(count($storresult)>0){
					
					foreach($storresult as $useroptionsval)
					{ 
						 $description=isset($useroptionsval['student_response']) ? $useroptionsval['student_response'] :'';
						 $correct=isset($useroptionsval['correct_response']) ? $useroptionsval['correct_response'] :'';
						 $result=isset($useroptionsval['result']) ? $useroptionsval['result'] :'';
					
						$ansoptions = array();
						$ansoptions[0]['qid']			=	$ids;
						$ansoptions[0]['student_response']		=	$description;
						$ansoptions[0]['correct_response']		=	$correct;
						$ansoptions[0]['result']		   		=	strtolower($result);
						$ansoptions[0]['question_type_id']		=	$type_id;
						$useranswer[]		=	$ansoptions;
					}
				}else{
					   $ansoptions[0]['qid']			=	$ids;
						$ansoptions[0]['student_response']		=	'';
						$ansoptions[0]['correct_response']		=	'';
						$ansoptions[0]['result']		   		=	'skip';
						$ansoptions[0]['question_type_id']		=	$type_id;
						$useranswer[]		=	$ansoptions;
					
					
				}
			}
		}
	
	   return $useranswer;
	}
}