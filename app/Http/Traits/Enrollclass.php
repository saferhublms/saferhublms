<?php
namespace App\Http\Traits;
use DB;
use App\Models\UsertrainingCatalog;
use App\Models\UserTrainingCatalogHistory;
use App\Models\SessionScedule;
use App\Models\ClassMaster;
use App\Models\UserClassSchedule;
use App\Models\ClassroomWaitingList;
use App\Models\ClassSchedule;
use App\Models\ClassroomEnrollList;
use App\Models\ClassroomEmail;
use App\Models\Course;


trait Enrollclass{
  
      //in use
      public static function getTrainingDataForenforll($compid, $class_id,$training_program_code) {
		 $lang_id=1;
			$querydata=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.is_internal','=',1)
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
			})
			->join('training_type','training_type.training_type_id','=','training_programs.training_type')
			->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
			})
			->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
			})
			->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
			})
			->join('class_master',function($join) use($compid){
				$join->on('class_master.course_id','=','courses.course_id')
				->where('class_master.company_id','=',$compid)
				->where('class_master.status_id','=',1);
			})
			->join('class_schedule as cs',function($join) use($compid){
				$join->on('cs.class_id','=','class_master.class_id')
				->where('cs.company_id','=',$compid);
			})
			->leftjoin('course_settings',function($join) {
				$join->on('course_settings.course_id','=','courses.course_id');
			})
			->join(DB::raw('(SELECT min(ssd.dayid) as dayid,max(ssd.dayid) as last_schedule_date, ssd.class_id from session_scedule ssd  where ssd.company_id = '.$compid.' and ssd.status_id=1  group by ssd.class_id)
			   ss'), 
			function($join)
			{
			$join->on('class_master.class_id', '=', 'ss.class_id');
			})
			->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)->where('class_master.class_id','=',$class_id)
			->where('training_programs.training_program_code','=',$training_program_code)
			->select(DB::raw('training_programs.training_type as training_type_id,course_details.course_name,course_details.course_title,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.credit_value,courses.media_library_id,training_type.training_type, ss.dayid as class_date, ss.last_schedule_date,class_master.class_name,class_master.class_id,course_settings.enrollment_type as require_supervisor_aproval_status,course_settings.is_unenrollment,class_master.maximum_seat as max_seat,class_master.delivery_type_id,class_master.start_date_class,GROUP_CONCAT(distinct training_category.category_name) as category_name,course_details.course_description,course_details.prerequeisites,class_master.virtual_session_information,courses.certificate_template_id,cs.*'))->get();
			//$itemresult = json_decode(json_encode($querydata),true);
			return $querydata;
			
		/* $item_arr = array();
		$sql = "select cs.*, tp.published_date, tp.training_type_id, cm.class_name, tp.training_program_id, tp.training_title, tp.item_id, tp.class_id as cl, tp.blended_program_id, im.item_name, tc.training_type as itemtype, im.require_supervisor_aproval_status from training_program as tp 
		join class_master as cm on cm.class_id=:class_id and cm.company_id=:compid1 and cm.item_id=tp.item_id and cm.is_active=1
		join item_master as im on im.item_id=tp.item_id and im.company_id=:compid2 and im.is_active=1
		join class_schedule as cs on cs.class_id=:class_id1 and cs.company_id=:compid3 and cs.is_active=1
		join training_type as tc on tp.training_type_id=tc.training_type_id and tc.is_active=1
		where tp.training_type_id in (2,3) and tp.company_id=:compid4 group by cs.class_schedule_id";
		$item_arr['class_id'] = $class_id;
		$item_arr['class_id1'] = $class_id;
		$item_arr['compid1'] = $compid;
		$item_arr['compid2'] = $compid;
		$item_arr['compid3'] = $compid;
		$item_arr['compid4'] = $compid;
		
		$result = DB::select(DB::raw($sql),$item_arr);
		return $result; */
	}


    public static function getenrolled($compid, $course_id, $classid)
	{
		$item_arr = array();
		$mainsql = "select distinct utc.user_id,utc.is_enroll,utc.is_approved,utc.user_training_id from user_training_catalog as utc
		join courses as im on im.course_id=:itemid1 and im.status_id=1 and im.company_id=:compid1
		join class_master as cm on cm.course_id=:itemid2 and cm.is_active=1 and cm.class_id=:classid1 and cm.company_id=:compid2
		join user_master as um on um.user_id=utc.user_id and um.is_approved=1 and um.is_delete=0 and um.is_active=1 and um.company_id=:compid3
		where utc.class_id=:classid2 and utc.company_id=:compid4 and utc.course_id=:itemid3 and utc.status_id=1 and utc.training_status_id in (3,10) and utc.is_enroll=1 and utc.training_type_id IN (1,2) group by utc.user_id";
	    $item_arr['itemid1'] = $course_id;
	    $item_arr['itemid2'] = $course_id;
	    $item_arr['itemid3'] = $course_id;
	    $item_arr['compid1'] =  $compid;
	    $item_arr['compid2'] =  $compid;
	    $item_arr['compid3'] =  $compid;
	    $item_arr['compid4'] =  $compid;
	    $item_arr['classid1'] =  $classid;
	    $item_arr['classid2'] =  $classid;
		$mainresult = DB::select(DB::raw($mainsql),$item_arr);
		$enrow = 0;
		$approw = 0;
		$userids = array();
		if(count($mainresult) > 0)
		{
			foreach($mainresult as $value)
			{
				$userids[] = $value->user_id;
				if($value->is_enroll == 1)
					$enrow++;

				if($value->is_approved == 1)
					$approw++;
			}
		}
		return array('totalenroll' => $enrow, 'totalapp' => $approw, 'userids' => $userids);
	}


    public static function getWaitingUser($compid, $class_id)
	{
		$results = UsertrainingCatalog::where('training_status_id','=',4)->where('company_id','=',$compid)->where('class_id','=',$class_id)->orderBy('last_modified_time','asc')->take(1)->get();
		return $results;
	
	}	
	
	
	public static function classUserenrolled($programs,$userid,$compid,$adminid =0) 
	{    
		if($adminid == 0){
			$adminid = $userid;
		}
		
		$data = array(
			'class_id'              =>  (!empty($programs['class_id'])) ? $programs['class_id'] : 0,
			'course_id'      	   	=> 	(!empty($programs['course_id'])) ? $programs['course_id'] : 0,
			'is_approved'           => 	1,
			'is_enroll'          	=> 	1,
			'user_id'           	=>	$userid,
			'company_id'    		=>	$compid,
			'training_type_id'      => 	(!empty($programs['itemtype'])) ? $programs['itemtype'] : 0,
			'training_status_id'	=>	(!empty($programs['training_status_id'])) ? $programs['training_status_id'] : 0,
			'last_modfied_by'     	=> 	$adminid,
			'created_date'     		=> 	date("Y-m-d H:i:s"),
			'duration'     			=> '',
			'status_id'             => 1,
			'duration_log'     		=> '',
		);
		$last_id =UsertrainingCatalog::insertGetId($data);
		$data['user_training_id']=$last_id;
		UserTrainingCatalogHistory::insert($data);
		return $last_id;
	}
	
   public static function getClassSchedules($class_id, $company_id)
	{
		$res=SessionScedule::join('class_timezones','class_timezones.class_timezone_id','=','session_scedule.time_zone')->leftjoin('instructor_master','instructor_master.instructor_id','=','session_scedule.instructor_id')->where('session_scedule.dayid','>=',date('Y-m-d'))->where('session_scedule.company_id','=',$company_id)->where('session_scedule.class_id','=',$class_id)->where('session_scedule.status_id','=',1)->select('session_scedule.dayid as dayid', 'class_timezones.timezone_name', 'session_scedule.start_time_hr', 'session_scedule.start_time_min', 'session_scedule.ses_length_hr', 'session_scedule.ses_length_min', 'session_scedule.timeformat', 'session_scedule.instructor_id', 'session_scedule.location', 'session_scedule.class_id', 'instructor_master.instructor_name')->first();
	
		$data = array();
		
		if($res)
		{
			$start_date  = date('Y-m-d', strtotime($res->dayid));
			$hrs = date("H", strtotime($res->start_time_hr." ".$res->timeformat));
			$start_time = sprintf("%02s", $hrs).":".sprintf("%02s", $res->start_time_min).":00";
			$end_time = sprintf("%02s", $res->start_time_hr+$res->ses_length_hr).":".sprintf("%02s", ((($res->start_time_min+$res->ses_length_min)<=0) ? "00" : $res->start_time_min+$res->ses_length_min)).":00";
			$data['training_start_date'] = $start_date." $start_time";
			$data['training_end_date'] = $start_date." $end_time";
			$data['timezone_name'] = $res->timezone_name;
		}
		return $data;
	}
	
	public static function getSessionSchedules($classid, $compid)
	{
		
		$result = ClassMaster::where('class_id','=',$classid)->where('company_id','=',$compid)->where('status_id','=',1)->where('is_active','=',1)->get();
		
		return $result;	
	}
	
	
	public static function update_class_master($classid, $company_id, $schedule_ids){
		$data_item = array();
		$data_item['session_schedule_update'] = 	$schedule_ids;
		ClassMaster::where('class_id','=',$classid)->where('company_id','=',$company_id)->update($data_item);
	}
	
	public static function user_class_scedule($scheduleid,$compid,$userid)
	{
		$checkDuplicateSchedule=Enrollclass::checkDuplicateSchedule($scheduleid,$compid,$userid);
		if($checkDuplicateSchedule)
		{
			$datauc=array();
			$datauc['class_schedule_id']=$scheduleid;
			$datauc['company_id']=$compid;
			$datauc['user_id']=$userid;
			$datauc['confirm']=0;
			UserClassSchedule::insert($datauc);
		}
	}
	
	
	public static function checkDuplicateSchedule($scheduleid,$compid,$userid) 
	{
		$results = UserClassSchedule::where('confirm','=',0)->where('user_id','=',$userid)->where('company_id','=',$compid)
		->where('class_schedule_id','=',$scheduleid)->orderBy('user_schedule_id', 'asc')->first();
		return $results ;
	}
	
	
	public static function classroomWaitingList($tainingData, $user_id, $company_id, $class_schedule_details)
	{
		try
		{
			$datawwt=ClassroomWaitingList::where('course_id','=',$tainingData['course_id'])->where('user_id','=', $user_id)->where('company_id','=',$company_id)->where('class_id','=',$tainingData['class_id'])->first();
			if(!$datawwt){
				$data['course_id'] = $tainingData['item_id'];
				$data['user_id'] = $user_id;
				$data['company_id'] = $company_id;
				$data['class_id'] = $tainingData['class_id'];
				$data['training_start_date'] = $class_schedule_details['training_start_date'];
				$data['training_end_date'] = $class_schedule_details['training_end_date'];
				$data['timezone_name'] = $class_schedule_details['timezone_name'];
				ClassroomWaitingList::insert($data);
			}
		} catch (Exception $e) {
		}
	}
	
	public static function updateclassschedulewaiting($newwaiting,$wherecs)
	{
		$datacs=array();
		$datacs['no_of_waiting_seat']=$newwaiting;
	    ClassSchedule::where('class_schedule_id','=',$wherecs['class_schedule_id'])->where('company_id','=',$wherecs['company_id'])->update($datacs);
		
	}
	
	public static function classUserenrolledwaiting($programs,$userid,$compid) 
	{
		
			$data = array(
				'class_id'              =>  (!empty($programs['class_id'])) ? $programs['class_id'] : 0,
				'course_id'      	   	=> 	(!empty($programs['course_id'])) ? $programs['course_id'] : 0,
				'is_approved'           => 	0,
				'is_enroll'          	=> 	0,
				'user_id'           	=>	$userid,
				'company_id'    		=>	$compid,
				'training_type_id'      => 	(!empty($programs['itemtype'])) ? $programs['itemtype'] : 0,
				'training_status_id'	=>	4,
				'last_modfied_by'     	=> 	$userid,
				'created_date'     		=> 	date("Y-m-d H:i:s"),
				'duration'     			=> '',
				'status_id'             => 1,
				'duration_log'     		=> '',
			);
			$last_id =UsertrainingCatalog::insertGetId($data);
			$data['user_training_id']=$last_id;
			UserTrainingCatalogHistory::insert($data);
			return $last_id;
	}
	
	public static function fetchuserssupervisorss($compid, $userid) 
	{
		    $item_arr=array();
			$sql = "select gm.user_id,us.supervisor_id from user_master as gm
			join user_supervisor as us on us.user_id=gm.user_id and us.is_active=1
			where gm.company_id=:compid1 and gm.user_id=:user_id and gm.is_active=1 and gm.is_delete=0 group by gm.user_id";
			$item_arr['compid1'] = $compid;
			$item_arr['user_id'] = $userid;
			$result = DB::select(DB::raw($sql),$item_arr);
			return $result;
	}
	
	public static function classroomEnroll($tainingData, $user_id, $company_id, $class_schedule_details)
	{
		
		if(count($class_schedule_details) > 0)
			{
			    $clsenrllst =ClassroomEnrollList::where('course_id','=',$tainingData['course_id'])->where('user_id','=',$user_id)->where('company_id','=',$company_id)->where('class_id','=',$tainingData['class_id'])->first();
				if(!$clsenrllst){
					$data['course_id'] = $tainingData['course_id'];
					$data['user_id'] = $user_id;
					$data['company_id'] = $company_id;
					$data['class_id'] = $tainingData['class_id'];
					$data['training_start_date'] = $class_schedule_details['training_start_date'];
					$data['training_end_date'] = $class_schedule_details['training_end_date'];
					$data['timezone_name'] = $class_schedule_details['timezone_name'];
					ClassroomEnrollList::insert($data);
				}
			}
	}
	
		public static function fetchsupervisorsettting($compid, $userid) 
		{
			$sql = "select gm.* from supervisor_setting as gm where company_id=:compid and supervisor_id=:userid";
			$param['compid']	=$compid;
			$param['userid']    =$userid;
			$result = DB::select(DB::raw($sql),$param);
			return $result;
		}
	
	public static function supervisiorApprovalNotRequire($userid,$usertrainingid)
	{		
		$data['is_approved'] = 1;
		$data['is_enroll'] = 1;
		$data['training_status_id'] = 3;

		UsertrainingCatalog::where('user_training_id','=',$usertrainingid)->update($data);
	}
	
	
	public static function classroomEmailSendStatus($tainingData, $user_id, $company_id, $class_schedule_details){
            $clsdata=ClassroomEmail::where('item_id','=',$tainingData['item_id'])->where('user_id','=',$user_id)->where('company_id','=',$company_id)->where('class_id','=',$tainingData['class_id'])->first();
			if(!$clsdata){
				$data['course_id'] = $tainingData['course_id'];
				$data['user_id'] = $user_id;
				$data['company_id'] = $company_id;
				$data['class_id'] = $tainingData['class_id'];
				$data['email_notification_id'] = 1;
				$data['training_start_date'] = $class_schedule_details['training_start_date'];
				$data['training_end_date'] = $class_schedule_details['training_end_date'];
				$data['timezone_name'] = $class_schedule_details['timezone_name'];
				ClassroomEmail::insert($data);
			}
	}
	
}
