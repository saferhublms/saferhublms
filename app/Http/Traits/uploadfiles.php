<?php
namespace App\Http\Traits;
use Mail,DB,URL,CommonHelper;
use App\Models\UploadedFile;
trait uploadfiles{
	
	public static function getfilename($file_id,$company_id)
    {
		
		 $result = UploadedFile::join('media_types','uploaded_files.media_id','=','media_types.media_id')
		                         ->where('uploaded_files.files_id','=',$file_id)
								 ->where('uploaded_files.company_id','=',$company_id)
								 ->select('uploaded_files.media_id','uploaded_files.files_id', 'uploaded_files.file_name', 'uploaded_files.file_path','uploaded_files.package_type','uploaded_files.version','uploaded_files.organization','uploaded_files.title','uploaded_files.launch', 'uploaded_files.scorm_type','uploaded_files.manifest','uploaded_files.media_id', 'uploaded_files.masteryscore','uploaded_files.is_lib','media_types.*')
								 ->first();
		return $result;
	}
	
	
	
	
}
