<?php
namespace App\Http\Traits;

use App\Models\AssignmentMaster;
use App\Models\ItemMaster;
use App\Models\BlendedProgramItem;
use App\Models\TrainingType;
use App\Models\UserTranscript;
use App\Models\ScoScoreData;
use App\Models\UserTrainingHistory;
use App\Models\BlendedprogramTrainingItem;
use App\Models\AssignmentUser;

use Mail,DB;

trait Assignment{
	
	public static function getAssignmenttouser($userid, $compid)
   {
	   $result1=array();
	   $resultgroup=AssignmentMaster::join('assignment_users',function($join){
		                $join->on('assignment_users.assignment_id','=','assignment_master.assignment_id')
						->where('assignment_users.is_active',1);
	                 })
	                ->join('training_program',function($join) use($compid){
						$join->on('training_program.training_program_id','=','assignment_master.training_program_id')
						     ->where('training_program.company_id',$compid);
					})
					->where('assignment_users.user_id',$userid)
					->where('assignment_master.company_id',$compid)
					->where('assignment_master.is_active',1)
					->groupBy('assignment_master.training_program_id')
					->select('assignment_master.assignment_id','assignment_master.company_id','assignment_master.training_program_id','assignment_master.due_date','assignment_master.assignment_start_date','assignment_master.is_active','assignment_master.created_date','training_program.training_type_id','training_program.item_id','training_program.class_id','training_program.training_title','training_program.blended_program_id')
					->get();
			foreach($resultgroup as $val)
			 {	
				$date=array();
				if($val->item_id!="") {
					$resultOtherRec=ItemMaster::join('training_type','item_master.training_type_id','=','training_type.training_type_id')
					 ->where('item_master.item_id',$val->item_id)
					 ->where('item_master.is_active',1)
					 ->where('item_master.company_id',$compid)
					  ->select('item_master.item_name','item_master.credit_value','item_master.average_rating','training_type.training_type')
					 ->get();
					 
				if(count($resultOtherRec) > 0 ) {
					$date['assignment_id']		=	$val->assignment_id;
					$date['due_date']			=	date('m/d/Y', strtotime($val->due_date));
					$date['assignment_start_date']=	date('m/d/Y', strtotime($val->assignment_start_date));
					$date['training_program_id']=	$val->training_program_id;
					$date['training_type_id']	=	$val->training_type_id;
					$date['training_title']		=	$val->training_title;
					$date['created_date']		=  	date('m/d/Y',strtotime($val->created_date));
					$date['item_id']			=	$val->item_id;
					$date['class_id']			=	$val->class_id;
					$date['blended_item_id']	=	$val->blended_program_id;
					$date['item_name']			=	$resultOtherRec[0]->item_name;
					$date['average_rating']		=	$resultOtherRec[0]->average_rating;
					$date['training_type']		=	$resultOtherRec[0]->training_type;
					$date['credit_value']		=	$resultOtherRec[0]->credit_value;		
				} 
			 }
			 
			 if($val->blended_program_id!="") {
				 $resultOtherRec=BlendedProgramItem::where('blended_item_id',$val->blended_program_id)
				                 ->where('is_active',1)
				                 ->where('company_id',$compid)
								 ->select('title')
								 ->get();
								 
			if(count($resultOtherRec) > 0) {
				    $traingtypeRec=TrainingType::wheretrainingTypeId($val->training_type_id)->select('training_type')->get();
					
					$date['assignment_id']		=	$val->assignment_id;
					$date['due_date']			=	date('m/d/Y', strtotime($val->due_date));
					$date['assignment_start_date']=	date('m/d/Y', strtotime($val->assignment_start_date));
					$date['training_program_id']=	$val->training_program_id;
					$date['training_type_id']	=	$val->training_type_id;
					$date['training_title']		=	$val->training_title;
					$date['created_date']		=	date('m/d/Y',strtotime($val->created_date));
					$date['item_id']			=	$val->item_id;
					$date['class_id']			=	$val->class_id;
					$date['blended_item_id']	=	$val->blended_program_id;
					$date['item_name']			=	isset($resultOtherRec[0]->title) ? $resultOtherRec[0]->title : '';
					$date['average_rating']		=	'';
					$date['training_type']		=	$traingtypeRec[0]->training_type;
					$date['credit_value']		=	'';
				}
			 }
					if(count($date) > 0) {		
				$result1[]=$date;
		}
				
			}
			
		
		return $result1;
   }
   
   
   
   public static function checkUserItemCompletedold($userid, $itemId)
	{
		//$result3=UserTranscript::join('item_master','item_master.item_id','=','user_transcript.item_id')->where('user_transcript.item_id',$userid)->where('user_transcript.user_id',$itemId)->select('user_transcript.user_id','user_transcript.item_id','item_master.credit_value','item_master.description','item_master.prerequeisites','item_master.item_cost',DB::raw('(CASE WHEN item_master.require_supervisor_aproval_status != 1 THEN No ELSE Yes END) AS supervisior_approval'))->get();
		$sql3 = "select ut.user_id,ut.item_id,im.credit_value,im.description,im.prerequeisites,im.item_cost,
		if(im.require_supervisor_aproval_status != 1, 'No', 'Yes') AS supervisior_approval from user_transcript ut
		join item_master im ON im.item_id = ut.item_id
		where ut.user_id ='$userid' and ut.item_id='$itemId'";
		$result3=DB::select($sql3);
		return $result3;
	}
	
	public static function checkUserItemCompleted($userid, $itemId)
	{
		//$result3=UserTranscript::join('item_master','item_master.item_id','=','user_transcript.item_id')->where('user_transcript.item_id',$userid)->where('user_transcript.user_id',$itemId)->select('user_transcript.user_id','user_transcript.item_id','item_master.credit_value','item_master.description','item_master.prerequeisites','item_master.item_cost',DB::raw('(CASE WHEN item_master.require_supervisor_aproval_status != 1 THEN No ELSE Yes END) AS supervisior_approval'))->get();
		$compid = 1;
		$sql = "SELECT im.item_name, if(im.require_supervisor_aproval_status != 1, 'No', 'Yes') AS supervisior_approval, im.credit_value, im.description, im.prerequeisites, im.item_cost, ut.user_id, ut.item_id,ut.completion_date,im.training_type_id, im.item_type_id as user_program_type_id	FROM user_transcript ut 
		JOIN item_master im ON (im.item_id='$itemId') and im.company_id='$compid'
		where ut.company_id='$compid' and im.item_type_id!=4 and ut.user_id='$userid' and ut.is_active=1 and im.is_active=1 and ut.item_id='$itemId' group by ut.item_id, ut.user_id";
		$result3=DB::select($sql);
		return $result3;
	}
	
	public static function geteLearningScore($userid, $compid, $item_id)
	{
		$itemresult=ScoScoreData::whereuserId($userid)->whereitemId($item_id)->orderBy('scorm_data_id','desc')
		           ->select('user_id', 'item_id','max_score','total_questions as total_questions', 'correct_answer as correct_answer', 'status as sco_score_status')
				   ->limit(1)->get();
		return $itemresult;
		
	}
	
	
	public static function usertraininghistoryDB($userId,$item_id)
	{
		$result=UserTrainingHistory::whereuserId($userId)->whereitemId($item_id)->select('course_completion_percentage')->get();
		
		return $result;
	}
	
	
	public static function getBlendedData($userid, $blended_program_id, $due_date)
	{
		$learningPlanDataArr = array();
		$result11=BlendedprogramTrainingItem::whereblendedprogramId($blended_program_id)
		                                   ->whereisActive(1)
										   ->whereisDelete(1)
										   ->select('item_id','assessment_id')
										   ->get();
										  
		$itemIdStr = '';
		$itemCompleted = 0;
		$creditValArr=array();
		
		if(count($result11->toArray()) > 0){
		foreach($result11 as $res1)
		{
			$itemIdStr .= $res1->item_id.',';
			$itemArr[] = $res1->item_id;
			$assessment_id[] = $res1->assessment_id;
			$userItemCompleted = Assignment::checkUserItemCompletedforblended($userid, $res1->item_id, $res1->assessment_id);
			if(count($userItemCompleted)>0)
			{
				$itemCompleted++;
				$creditValArr[] = $userItemCompleted[0]->credit_value;
			}
		}
		}
		
		if(count($result11) > 0)
		{
			if($itemCompleted == count($result11))
			{
				$learningPlanDataArr['credit_value'] 	= array_sum($creditValArr);
				$percentComplete 						= floor(($itemCompleted/count($result11))*100);
				$learningPlanDataArr['percent'] 		= $percentComplete;
				$learningPlanDataArr['imagname'] 		= 'u184.png';
				$learningPlanDataArr['assignmentstatus']= 'Completed';
			}else {
				$itemIdStr = rtrim($itemIdStr,',');
				$result2 = ItemMaster::whereitemId($itemIdStr)->select(DB::raw('SUM(`credit_value`) AS totalCredit'))->get();
				$learningPlanDataArr['credit_value'] 	= $result2[0]->totalCredit;
				$percentComplete 						= floor(($itemCompleted/count($result11))*100);
				$learningPlanDataArr['percent'] 		= $percentComplete;
				$learningPlanDataArr['assignmentstatus']= ($percentComplete > 0) ? 'In-Progress':'Incomplete' ;
				
				if(strtotime($due_date) > time()) {
					$learningPlanDataArr['imagname'] = "flag_red.png";
				}else{
					$learningPlanDataArr['imagname'] = "flag_yellow.png";
				}
			}
		}
		return $learningPlanDataArr;
										   
		
	}
	
	
	
	public static function checkUserItemCompletedforblended($userid, $itemId, $assessment_id)
	{
		$compid =1;
		if($itemId!=0)
		{
			$sql = "SELECT im.item_name, if(im.require_supervisor_aproval_status != 1, 'No', 'Yes') AS supervisior_approval, im.credit_value, im.description, im.prerequeisites, im.item_cost, ut.user_id, ut.item_id, im.training_type_id, im.item_type_id as user_program_type_id FROM user_transcript ut 
			JOIN item_master im ON (im.item_id = ut.item_id) and im.company_id='$compid' and im.item_type_id!=4 and im.is_active=1
			where ut.user_id='$userid' and ut.company_id='$compid' and ut.is_active=1 and ut.item_id='$itemId' group by ut.item_id, ut.user_id";
		}
		else
		{
			$sql = "SELECT ass.assessment_name, ass.credit_value, ass.assessment_desc, ut.user_id, ut.registration_id FROM user_transcript ut 
			join assessment ass on ass.id='$assessment_id' and ass.company_id='$compid'
			where ut.user_id='$userid' and ut.company_id='$compid' and ut.is_active=1 and ut.is_blended=0 and ut.registration_id='$assessment_id' group by ut.registration_id, ut.user_id";
		}
		
		$result = DB::select($sql);
		return $result;
	}
	
	
	public static function lastinsertid(array $data)
	{
	    $id = AssignmentMaster::insertGetId($data);
		return $id;
	}
	
		public static function usersAssignmentWithTrainingId($trainingprogid, $compid){
	      $result = AssignmentMaster::join('assignment_users', function($join){
			         $join->on('assignment_users.assignment_id','=','assignment_master.assignment_id')
					      ->where('assignment_master.is_active','=',1);
		  })->join('training_program',function($join){
			     $join->on('training_program.training_program_id','=','assignment_master.training_program_id')
				      ->where('training_program.is_active','=',1);
		  })->where('assignment_master.training_program_id','=',$trainingprogid)
		    ->where('assignment_master.company_id','=',$compid)
			->where('assignment_master.is_active','=',1)
			->select('assignment_users.user_id')
			->get();
		$assignmentuserarray = array();
		if(count($result) > 0)
		{
			foreach($result as $a){
				$assignmentuserarray[] = $a->user_id;
			}
		}
		
		return array_unique($assignmentuserarray);
	}
	
	public static function lastinsertassginmentuser(array $data){
		$id = AssignmentUser::insertGetId($data);
		return $id;
	}
	
	public static function getItemidreturnnew($assid)
	{
		$result = AssignmentMaster::join('training_program',function($join){
			      $join->on('assignment_master.training_program_id', '=', 'training_program.training_program_id');
		})->where('assignment_master.assignment_id', '=', $assid)->select('training_program.item_id','training_program.blended_program_id')->select('training_program.item_id','training_program.blended_program_id')->get();
	    return $result;
	}
	
	public static function checktranscriptitem($item_id,$userid)
	{
		 $result = UserTranscript::where('item_id','=',$item_id)->where('user_id','=',$userid)->select('transcript_id')->get();
		return $result; 
	}
	
	
	public static function checktranscriptblend($blended_program_id,$userid)
	{
		  $result = UserTranscript::where('is_blended','=',1)->where('user_id','=',$userid)->where('registration_id','=',$blended_program_id)->select('transcript_id')->get();
		return $result;
	}
	
	public static function updateassignusers($data1, $userid, $traningprogid, $compid)
	{
		$result = AssignmentMaster::join('training_program',function($join){
		                            $join->on('training_program.training_program_id','=','assignment_master.training_program_id')->where('training_program.is_active','=',1);	               
		})->where('assignment_master.training_program_id','=',$traningprogid)->where('assignment_master.company_id','=',$compid)->where('assignment_master.is_active','=',1)->select('assignment_master.assignment_id')->get();
		if(count($result) > 0)
		{
			foreach($result as $a)
			{
				  AssignmentUser::where('assignment_id','=',$a->assignment_id)->where('user_id','=',$userid)->update($data1);
			}
		}
	}
	
}
