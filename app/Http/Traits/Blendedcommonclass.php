<?php
namespace App\Http\Traits;
use DB;
use App\Models\UsertrainingCatalog;
use App\Models\UserTrainingCatalogHistory;
use App\Models\UserTranscript;
use App\Models\UserTranscriptHistory;

trait Blendedcommonclass{

	
	public static function getAllTrainingItems($blended_id,$compid) 
	{
	
		$sql = "select item_id,assessment_id from blendedprogram_training_items where blendedprogram_id=$blended_id and is_active=1 and is_delete=1"; 
	    $result=  DB::select($sql);
        return $result;
	}
	
	public static function checkAssessmentCompletion($user_id,$assessment_id,$compid) {
		if($user_id!='' && $assessment_id!='' && $compid!=''){
			$sql = "select ut.credit_value,ut.completion_date from user_transcript ut where ut.user_id=$user_id and ut.registration_id=$assessment_id and ut.is_blended=0 and ut.company_id=$compid and ut.is_active=1 ";
			$result=  DB::select($sql);
			return $result;
		}
	}
	
	public static function checkItemCompletion($user_id,$item_id,$compid) {
	
		if($user_id!='' && $item_id!='' && $compid!=''){
			$sql = "select ut.credit_value,ut.completion_date from user_transcript ut where ut.user_id=$user_id and ut.item_id=$item_id and ut.company_id=$compid and ut.is_active=1 and ut.is_expired=0";
			$result=  DB::select($sql);
			return $result;
		}
		
	}	
	
	
	/* public function getAllBlendedProgram($compid) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		$sql = "select blended_item_id from blended_program_items where company_id=$compid and is_active=1 "; 
		$result = $db->fetchAll($sql); 
		return $result;
	}
	
	public function getAllAssignedUsers($blended_id,$compid) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		
		if($blended_id!='' ){ 
			$userArr= array();
			$sql = "select DISTINCT(lpu.user_id) as userlist1 from learning_plan_training_item lpti
			join learning_plan_users lpu on lpu.learning_plan_id=lpti.learning_plan_id and lpu.is_active=1 and lpu.company_id=$compid
			where lpti.blended_program_id=$blended_id and lpti.is_active=1 and lpti.is_delete=1"; 
			$result1 = $db->fetchAll($sql);
			if(count($result1)){
				foreach($result1 as $rst){
					$userArr[] = $rst['userlist1'];
				}
			}
			$sql = "select DISTINCT(ug.user_id) as userlist2 from learning_plan_training_item lpti
			join learning_plan_groups lpg on lpg.learning_plan_id=lpti.learning_plan_id and lpg.is_active=1 and lpg.company_id=$compid
			join user_group ug on ug.group_id=lpg.group_id and ug.is_active=1 and ug.user_id!='' 
			where lpti.blended_program_id=$blended_id and lpti.is_active=1 and lpti.is_delete=1"; 
			$result2 = $db->fetchAll($sql); 
			
			if(count($result2)){
				foreach($result2 as $stp){
					$userArr[] = $stp['userlist2'];
				}
			}
			$sql = "select distinct utc.user_id from user_training_catalog utc where utc.blended_program_id=$blended_id and utc.training_type_id=4 and utc.company_id=$compid"; 
			$result2 = $db->fetchAll($sql); 
			
			if(count($result2)){
				foreach($result2 as $stp){

					$userArr[] = $stp['user_id'];
				}
			}
			
			return array_unique($userArr);
		}	
	} */
	
	/* public function userBlendedCompletionCheck($user_id,$blended_id,$compid) {
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		$sql = "select ut.transcript_id from user_transcript ut where ut.user_id=$user_id and ut.registration_id=$blended_id and ut.company_id=$compid and ut.is_blended=1 and ut.is_active=1"; 
		$result = $db->fetchRow($sql);
			$transcript_id='';
		if($result!='')	$transcript_id =$result['transcript_id'];
		return 	$transcript_id;	
	} */
	
	public static function userBlendedCompletion($user_id,$blended_id,$compid,$creditArr,$completionDateArr) {
		$sql = "select user_id from user_training_catalog where user_id=$user_id and blended_program_id=$blended_id and training_type_id=4 and company_id=$compid and training_status_id!=1";
		  $result = DB::select($sql);
		if(count($result)>0){
			$user_training_id =$result[0]->user_training_id; 
			$sql = "update user_training_catalog  set training_status_id=1 where user_id=$user_id and blended_program_id=$blended_id and training_type_id=4 and company_id=$compid";
			DB::update($sql);
		}else{
			$data = array(
                'user_id'               => $user_id,
				'is_enroll'             => 1,
				'is_approved'           => 1,
				'training_type_id'      => 4,
				'user_program_type_id'  => '',
				'item_id'               => '',
				'company_id'            => $compid,
				'training_status_id'    => 1,
				'blended_program_id'    => $blended_id,
				'learning_plan_id'      => '',
                'session_id'       		=> '',
                'last_modfied_by'     	=> $user_id,
                'created_date'    		=> date('Y-m-d H-i-s'),
			);
			 $user_training_id =UsertrainingCatalog::insertGetId($data);;
			 $data['user_training_id']=$user_training_id;
             UserTrainingCatalogHistory::insert($data);
		
		}
		$sql = "select ut.transcript_id from user_transcript ut where ut.user_id=$user_id and ut.registration_id=$blended_id and ut.company_id=$compid and ut.is_blended=1 and ut.is_active=1";
        $results = DB::select($sql);		
		if(count($results)>0){
			$data = array();
			$data['registration_id'] 	= $blended_id;
			$data['user_id'] 			= $user_id;
			$data['company_id'] 		= $compid;
			$data['is_blended'] 		= 1;
			$data['is_active'] 			= 1;
			$data['credit_value'] 		= array_sum($creditArr);
			$data['completion_date'] 	= date('Y-m-d',max($completionDateArr));
			$transcriptid=UserTranscript::insertGetId($data);
			$data['transcript_id']=$transcriptid;
			UserTranscriptHistory::insert($data);
		}else{
			//unset($results['transcript_id']);
			//$transcriptid=$results[0]->transcript_id; 
			
			UserTranscriptHistory::insert($results);
		
			$ut_data['user_training_id'] 	=	$user_training_id;
			$ut_data['completion_date'] 	=	date('Y-m-d',max($completionDateArr));
			$ut_data['is_active'] 			=	1;
			$ut_data['last_modified_by']	=	$user_id;
			UserTranscript::where('user_id','=',$user_id)->where('registration_id','=',$blended_id)->where('company_id','=',$compid)->where('is_blended','=',1)->where('is_active','=',1)->update($ut_data);
		}		
		return 1;
	}
	
	
}