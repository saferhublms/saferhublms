<?php
namespace App\Http\Traits;
use App\Models\ExpirationProperties;
use App\Models\UserGroup;
use App\Models\LearningPlanMaster;
use App\Models\LearningPlanTrainingItem;
use App\Models\UserLpRequirements;
use App\Models\ScoScoreData;
use App\Models\UserLpProgress;
use DB;

trait Learningplan{
	
	public static function expirationSettings($blended_program_id, $item_id)
	{
		
		if ($blended_program_id != ''){
			$sql3 = ExpirationProperties::where('blendedprogram_id','=',$blended_program_id);
		}
		else{
			$sql3 = ExpirationProperties::where('item_id','=',$item_id);
		}
	
		$result3 = $sql3 ->get();
		return $result3;
	}
	
	public static function learnPlanArr($compid, $userid)
	{
		$allLearnPlanAssignedtoUserGroup = Learningplan::allLearnPlanAssignedtoUserGroup($compid, $userid);
		
		$learningplanarray = array ();
		$allLearnPlanAssignedArr = array ();
		if(count($allLearnPlanAssignedtoUserGroup) > 0) {
			  foreach($allLearnPlanAssignedtoUserGroup as $learnPlanAssignedtoUserGroup)
			  {
					$learningplanid = $learnPlanAssignedtoUserGroup ['learning_plan_id'];
					$group_id = $learnPlanAssignedtoUserGroup ['group_id'];
					$user_id = $learnPlanAssignedtoUserGroup ['user_id'];
					if($group_id > 0) {
						$learningplanarray = Learningplan::getLearnPlanItemByGroup($group_id, $userid, $learningplanid, $compid, $learningplanarray);
					}else if($user_id > 0 && $user_id != '') {
						$learningplanarray = Learningplan::getLearnPlanItemByUser($userid, $learningplanid, $compid, $learningplanarray);
					}
			  }
			
		}
		
		if(count($learningplanarray) > 0) {
			foreach($learningplanarray as $learningplans)
			{
				$item_id				= $learningplans ['item_id'];
				$lerningplanassigndate	= $learningplans ['assigned_date'];
				$blendedProgramId		= $learningplans ['blended_program_id'];
				$learnplanId			= $learningplans ['learning_plan_id'];
				if($learningplans ['training_type_id'] != 4) {
					$learningPlanDataArr = Learningplan::getTrainingItemCompletion($item_id, $learnplanId,$lerningplanassigndate, $userid, $compid);
					if(count($learningPlanDataArr) > 0)
					{
						$learnPlanItemArray = array ();
						$learnPlanItemArray ['learning_plan_id'] 	= $learningplans ['learning_plan_id'];
						$learnPlanItemArray ['item_id'] 			= $learningplans ['item_id'];
						$learnPlanItemArray ['blended_program_id'] 	= $learningplans ['blended_program_id'];
						$learnPlanItemArray ['training_type_id'] 	= $learningplans ['training_type_id'];
						$learnPlanItemArray ['learning_plan_name'] 	= $learningplans ['learning_plan_name'];
						$learnPlanItemArray ['credit_value'] 		= $learningPlanDataArr['credit_value'];
						$learnPlanItemArray ['due_date'] 			= $learningPlanDataArr['due_date'];
						$learnPlanItemArray['exp_date'] 			= $learningPlanDataArr['exp_date'];
						$learnPlanItemArray['start_date'] 			= $learningPlanDataArr['start_date'];
						$learnPlanItemArray['end_date'] 			= $learningPlanDataArr['end_date'];
						$learnPlanItemArray['comp_date'] 			= $learningPlanDataArr['comp_date'];
						$learnPlanItemArray['percent'] 			= $learningPlanDataArr['percent']!=''? $learningPlanDataArr['percent'] : 0;
						$learnPlanItemArray['imagname'] 			= $learningPlanDataArr['imagname'];
						$learnPlanItemArray['item_name'] 			= $learningPlanDataArr['item_name'];
						//$learnPlanItemArray ['training_type'] 		= $learningPlanDataArr['training_type']='Classroom'?'Classroom Training':$learningPlanDataArr['training_type'];
						$learnPlanItemArray['training_type'] 		= $learningPlanDataArr['training_type'];
						$learnPlanItemArray['trainingProgramId'] 	= $learningPlanDataArr['trainingProgramId'];			
						$learnPlanItemArray['class_id'] 			= $learningPlanDataArr['class_id'];
						$learnPlanItemArray['assigned_date'] 		= $learningPlanDataArr['assigned_date'];
						$allLearnPlanAssignedArr [] = $learnPlanItemArray;
						
					}
				}else {
					$learningPlanDataArr = Learningplan::getLearningPlanBlendedCompletion($blendedProgramId, $learnplanId,$lerningplanassigndate, $userid, $compid);
					if(count($learningPlanDataArr) > 0)
					{
						$learnPlanItemArray = array ();
						$learnPlanItemArray ['learning_plan_id'] 	= $learningplans ['learning_plan_id'];
						$learnPlanItemArray ['item_id'] 			= $learningplans ['item_id'];
						$learnPlanItemArray ['blended_program_id'] 	= $learningplans ['blended_program_id'];
						$learnPlanItemArray ['training_type_id'] 	= $learningplans ['training_type_id'];
						$learnPlanItemArray ['learning_plan_name'] 	= $learningplans ['learning_plan_name'];
						$learnPlanItemArray ['credit_value'] 		= $learningPlanDataArr['credit_value'];
						$learnPlanItemArray ['due_date'] 			= $learningPlanDataArr['due_date'];
						$learnPlanItemArray ['exp_date'] 			= $learningPlanDataArr['exp_date'];
						$learnPlanItemArray ['start_date'] 			= $learningPlanDataArr['start_date'];
						$learnPlanItemArray ['end_date'] 			= $learningPlanDataArr['end_date'];
						$learnPlanItemArray ['comp_date'] 			= '0000-00-00';
						$learnPlanItemArray ['percent'] 			= $learningPlanDataArr['percent']!=''? $learningPlanDataArr['percent'] : 0;
						$learnPlanItemArray ['imagname'] 			= $learningPlanDataArr['imagname'];
						$learnPlanItemArray ['item_name'] 			= $learningPlanDataArr['item_name'];
						$learnPlanItemArray ['training_type'] 		= $learningPlanDataArr['training_type'];
						$learnPlanItemArray ['trainingProgramId'] 	= $learningPlanDataArr['trainingProgramId'];
						$learnPlanItemArray ['class_id']            = '';
						$learnPlanItemArray ['assigned_date'] 		= $learningPlanDataArr['assigned_date'];
						$allLearnPlanAssignedArr [] = $learnPlanItemArray;
					}
				}
			}
		}
		return $allLearnPlanAssignedArr;
	}
	
	public static function allLearnPlanAssignedtoUserGroup($compid, $userid)
	{
		$result1=UserGroup::join('group_master',function($join){
			                        $join->on('group_master.group_id','=','user_group.group_id')
									     ->where('group_master.is_active',1)
									     ->where('group_master.is_delete',0);
		                      })
							  ->where('user_group.user_id',$userid)
							  ->where('user_group.is_active',1)
							  ->where('group_master.company_id',$compid)
							  ->pluck('user_group.group_id');
			
		$userarray = $grouparray = $allGrups = array();
		 $i=0;
		if(count($result1)>0) {
			foreach($result1 as $res)
			{
				$i++;
				if(count($result1) == $i){
					$allGrups[]= $res ['group_id'];
				}else{
					$allGrups[]= $res ['group_id'];
				}
			} 
			$grouparray=LearningPlanMaster::leftjoin('learning_plan_groups',function($join){
                                             $join->on('learning_plan_groups.learning_plan_id','=','learning_plan_master.learning_plan_id')
											      ->where('learning_plan_groups.company_id','=','learning_plan_master.company_id');
			                     })
								              ->where('learning_plan_master.company_id',$compid)
								              ->where('learning_plan_master.is_active',1)
								              ->whereIn('learning_plan_groups.group_id',$result1)
								              ->where('learning_plan_groups.is_active',1)
											  ->groupBy('learning_plan_master.learning_plan_id')
											  ->select('learning_plan_master.learning_plan_id','learning_plan_master.learning_plan_name','learning_plan_master.created_date','learning_plan_groups.group_id','learning_plan_groups.created_date as assigned_date',DB::raw('0 as user_id'))
											  ->get()->toArray();
											 
											 

		}
		
		$userarray=LearningPlanMaster::leftjoin('learning_plan_users',function($join){
                                             $join->on('learning_plan_users.learning_plan_id','=','learning_plan_master.learning_plan_id');
											    
			                     })
								              ->where('learning_plan_master.company_id',$compid)
								              ->where('learning_plan_master.is_active',1)
								              ->where('learning_plan_users.user_id',$userid)
								              ->where('learning_plan_users.is_active',1)
											  ->groupBy('learning_plan_master.learning_plan_id')
											  ->select('learning_plan_master.learning_plan_id','learning_plan_master.learning_plan_name','learning_plan_master.created_date','learning_plan_users.user_id','learning_plan_users.created_date as assigned_date',DB::raw('0 as group_id'))
											  ->get()->toArray();
		$learningplanid = array_merge($grouparray,$userarray);
		return $learningplanid;
							 
      							 
	}
	
	public static function getLearnPlanItemByGroup($group_id, $userid, $learnplanid, $compid, $learningPlanDataMainArr)
	{
		$sqls = "SELECT lpti.training_type_id, lpti.blended_program_id, lpti.training_program_id as item_id, lpm.learning_plan_name, GREATEST(lpg.created_date, ug.created_date) as assigned_date FROM learning_plan_training_item as lpti
		join learning_plan_master as lpm ON lpm.learning_plan_id='$learnplanid' and lpm.company_id='$compid' and lpm.is_active=1
		join learning_plan_groups as lpg on lpg.learning_plan_id='$learnplanid' and lpg.company_id='$compid' and lpg.is_active=1 and lpg.group_id='$group_id'
		join user_group ug on ug.group_id='$group_id' and ug.user_id='$userid' and ug.is_active=1
		WHERE lpti.learning_plan_id='$learnplanid' and lpti.is_active=1 and lpti.is_delete=1 group by lpti.learning_plan_id, lpti.training_program_id, lpti.blended_program_id order by lpti.training_item_id asc";
		$result = DB::select($sqls);
			if(count($result) > 0)
			{
				foreach($result as $res)
				{
					$learningPlanDataArr['training_type_id'] 	= $res->training_type_id;
					$learningPlanDataArr['learning_plan_name']  = $res->learning_plan_name;
					$learningPlanDataArr['blended_program_id']  = $res->blended_program_id;
					$learningPlanDataArr['item_id'] 			= $res->item_id;
					$learningPlanDataArr['learning_plan_id'] 	= $learnplanid;
					$learningPlanDataArr['assigned_date'] 		= $res->assigned_date;
					$learningPlanDataMainArr[] 					= $learningPlanDataArr;
				}
			}
			return $learningPlanDataMainArr;
	}
	
	public static function getLearnPlanItemByUser($userid, $learnplanid, $compid, $learningPlanDataMainArr)
	{
		$result=LearningPlanTrainingItem::join('learning_plan_master',function($join) use($compid,$learnplanid){
			                        $join->on('learning_plan_master.learning_plan_id','=','learning_plan_training_item.learning_plan_id')
									   ->where('learning_plan_master.company_id',$compid)
									   ->where('learning_plan_master.is_active',1);
		})
		                          ->join('learning_plan_users',function($join) use($userid,$learnplanid){
			                        $join->on('learning_plan_users.learning_plan_id','=','learning_plan_training_item.learning_plan_id')
									   ->where('learning_plan_users.user_id',$userid)
									   ->where('learning_plan_users.is_active',1);
		})
		                          ->where('learning_plan_training_item.learning_plan_id',$learnplanid)
		                          ->where('learning_plan_training_item.is_active',1)
		                          ->where('learning_plan_training_item.is_delete',1)
		                          ->orderBy('learning_plan_training_item.training_item_id','asc')
								  ->select('learning_plan_training_item.training_type_id', 'learning_plan_training_item.blended_program_id', 'learning_plan_training_item.training_program_id as item_id','learning_plan_master.learning_plan_name','learning_plan_users.created_date as assigned_date')
								  ->get();
		if(count($result) > 0) {
			 foreach($result as $res)
				{
					$learningPlanDataArr['training_type_id'] = $res->training_type_id;
					$learningPlanDataArr['learning_plan_name'] = $res->learning_plan_name;
					$learningPlanDataArr['blended_program_id'] = $res->blended_program_id;
					$learningPlanDataArr['item_id'] = $res->item_id;
					$learningPlanDataArr['learning_plan_id'] = $learnplanid;
					$learningPlanDataArr['assigned_date'] = $res->assigned_date;
					$learningPlanDataMainArr[] = $learningPlanDataArr;
				}
		}
		return $learningPlanDataMainArr;
	}
	
	public static function getTrainingItemCompletion($item_id, $learnplanId,$lerningplanassigndate, $userid, $compid)
	{
		$learningPlanDataArr = array ();		
		$itemsSql = "select ulr.assigned_date,ulr.expire_date,ulr.due_date,ulr.start_date,ulr.end_date,im.item_name,im.training_type_id,im.credit_value, ulr
		.transcript_id,'' as class_id , ulr.completion_date,im.item_cost,uth.course_completion_percentage,tp.training_program_id as trainingProgramId from user_lp_requirements ulr
		join item_master im on im.item_id=ulr.item_id and im.is_active=1 and im.company_id='$compid' and im.training_type_id=1
		join training_program tp on im.item_id=tp.item_id and tp.is_active=1 and im.training_type_id=1 and tp.company_id='$compid' 
		left join user_training_catalog utc on utc.item_id = ulr.item_id and utc.item_id = $item_id and utc.user_id = '$userid' and utc.company_id=$compid and utc.is_enroll=1 and utc.training_status_id=3
		left join user_training_history uth on uth.user_training_id = utc.user_training_id and uth.user_id = '$userid' and uth.is_active=1
		left join user_transcript ut on ut.item_id = im.item_id and ut.is_active = 1 and ut.user_id = '$userid'
		where ulr.company_id=$compid and ulr.user_id=$userid and ulr.item_id=$item_id and ulr.learning_plan_id=$learnplanId and ulr.is_active=1";
		$itemsResulttype1 = DB::select($itemsSql);
		
		$itemsSql = "select ulr.assigned_date,ulr.expire_date,ulr.due_date,ulr.start_date,ulr.end_date,im.item_name,im.training_type_id,im.credit_value, im.item_cost,ulr
		.transcript_id,cm.class_id, ulr.completion_date,tp.training_program_id as trainingProgramId from user_lp_requirements ulr
		
		join item_master im on im.item_id=ulr.item_id and im.is_active=1 and im.company_id='$compid' and im.training_type_id in (2,3)
		join training_program tp on im.item_id=tp.item_id and tp.is_active=1 and tp.company_id='$compid'
		left join class_master cm on cm.item_id = im.item_id and cm.is_active = 1 and cm.status_id != 0 and cm.start_date_class > CURDATE()
		left join user_transcript ut on ut.item_id = im.item_id and ut.is_active = 1 and ut.user_id = '$userid'
		where ulr.company_id=$compid and ulr.user_id=$userid and ulr.item_id=$item_id and ulr.learning_plan_id=$learnplanId and ulr.is_active=1";
		$itemsResulttype2 =  DB::select($itemsSql);
		
		$total_item_result =Json_decode(Json_encode(array_merge($itemsResulttype1,$itemsResulttype2)),true);	
		
		if(count($total_item_result)) {
			foreach($total_item_result as $resItems)
			{
				$item_name				= 	$resItems['item_name'];				
				$training_type_id 		=	$resItems['training_type_id'];
				$training_type			= 	($training_type_id!=1) ? 'Classroom Training' :'eLearning';
				$credit_value	 		=	$resItems['credit_value'];
				$start_date	 			=	$resItems['start_date'];
				$end_date	 			=	$resItems['end_date'];
				$item_cost		 		=	$resItems['item_cost'];
				$transcript_id			=	$resItems['transcript_id'];
				$training_program_id	= 	$resItems['trainingProgramId'];
				$assigned_date 			= 	$resItems['assigned_date'];
				$due_date				= 	$resItems['due_date'];
				$class_id				=   (isset($resItems['class_id'])) ? $resItems['class_id'] : '';
				$expiration_date 		=	'0000-00-00';
				$date_completion		= 	'';	
				$expirdate 			= '0000-00-00';	
				if($transcript_id > 0) {
					$date_completion	= 	$resItems['completion_date'];					
					$expiration_date 	=   $resItems['expire_date'];
					$scoredata 			=Learningplan::geteLearningScore($userid, $compid, $item_id);
					$passfailstatus 	= 'Complete';
					if(count($scoredata)) {
						if($scoredata[0]->sco_score_status == '2'){
							$passfailstatus = 'Complete';
						}else{
							$passfailstatus = ($scoredata[0]->sco_score_status == 1) ? 'Pass' : 'Fail';
						}
					}
					else {
						$passfailstatus = ($resItems['completion_date']!='' && $resItems['completion_date']!='1969-12-31') ?'Complete' : 'Incomplete';
					}
					$image_name			= 'u184.png';
					$percent_progress	= 100;					
					if((strtolower($passfailstatus) == 'pass' ) || (strtolower($passfailstatus) == 'complete')){
						$image_name			= 'u184.png';
						$percent_progress	= 100;
					}elseif((strtolower($passfailstatus) == 'fail' )){
						$image_name			= 'fail_icon2.png';
						$percent_progress	= 100;
					}
					if($expiration_date != '' &&  $expiration_date != '0000-00-00' && $expiration_date != NULL){
						$expirdate = date ( "Y-m-d", strtotime ( $expiration_date ));
						if($expirdate < date ( "Y-m-d")){
							$percent_progress = 0;
							$image_name		 = 'flag_yellow.png';	
						}else{
							$image_name			= 'u184.png';
							$percent_progress	= 100;
							$due_date			= '0000-00-00';
						}
					}else{
						$percent_progress 	= 0;
						$image_name		 	= 'flag_yellow.png';
						$expirdate 			= '0000-00-00';	
					}
				
				}
				else
				{ 
					$expiration_date 		=	'0000-00-00';
					$percent_progress1 = (isset($resItems['course_completion_percentage'])) ? $resItems['course_completion_percentage'] : '';
					if($percent_progress1 >= 100){
						$percent_progress = 98;
					}else if($percent_progress1 < 100){
						$percent_progress = $percent_progress1;
					}else{
						$percent_progress = 0;
					}
					$image_name		 = 'flag_red.png';	
					if($due_date != '' &&  $due_date != '0000-00-00' && $due_date != NULL){
						$due_date		= $due_date;
					}else{
						$due_date			= '0000-00-00';
					}
				}	
				$learningPlanDataArr['credit_value'] 		= $credit_value;
				$learningPlanDataArr['due_date'] 			= $due_date;
				$learningPlanDataArr['start_date'] 	 		= $start_date;
				$learningPlanDataArr['end_date']  			= $end_date;
				$learningPlanDataArr['exp_date'] 			= $expirdate;
				$learningPlanDataArr['comp_date'] 			= $date_completion;
				$learningPlanDataArr['percent'] 			= $percent_progress;
				$learningPlanDataArr['imagname'] 			= $image_name;
				$learningPlanDataArr['assigned_date'] 		= $assigned_date;
				$learningPlanDataArr['item_name'] 			= $item_name;
				$learningPlanDataArr['training_type'] 		= $training_type;
				$learningPlanDataArr['trainingProgramId'] 	= $training_program_id;				
				$learningPlanDataArr['class_id'] 			= $class_id;		
			}
		}
		return $learningPlanDataArr;
	}
	
	
	public static function geteLearningScore($userid, $compid, $item_id)
	{
		$itemresult=ScoScoreData::whereuserId($userid)->whereitemId($item_id)->orderBy('scorm_data_id','desc')->limit(1)->select('user_id','item_id','max_score','total_questions as total_questions','correct_answer as correct_answer','status as sco_score_status')->get();
		return $itemresult;
		
	}
	
	public static function getLearningPlanBlendedCompletion($blendedProgramId, $learnplanId,$lerningplanassigndate, $userid, $compid)
	{
		$learningPlanDataArr = array ();
		$blendedprogresult =UserLpRequirements::join('blended_program_items as bpi',function($join) use($compid){
			                                        $join->on('bpi.blended_item_id','=','user_lp_requirements.blended_program_id')
													   ->where('bpi.company_id','=',$compid)
													   ->where('bpi.is_active','=',1);
		                                       })
											   ->join('training_program as tp',function($join) use($compid){
			                                        $join->on('tp.blended_program_id','=','bpi.blended_item_id')
													   ->where('tp.company_id','=',$compid)
													   ->where('tp.is_active','=',1);
		                                       })
											   ->where('user_lp_requirements.company_id',$compid)
											   ->where('user_lp_requirements.is_active',1)
											   ->where('user_lp_requirements.blended_program_id',$blendedProgramId)
											   ->where('user_lp_requirements.user_id',$userid)
											   ->where('user_lp_requirements.learning_plan_id',$learnplanId)
											   ->select('user_lp_requirements.due_date','user_lp_requirements.expire_date','user_lp_requirements.assigned_date','user_lp_requirements.start_date','user_lp_requirements.end_date','bpi.title as blended_prog_name','tp.training_program_id','bpi.blended_item_id','user_lp_requirements.item_id')
											   ->get();
         if(count($blendedprogresult)>0) {
			 foreach($blendedprogresult as $resblendedProg)
				{
					$blendedProgName 		=	$resblendedProg->blended_prog_name;
					$tariningProgId			=	$resblendedProg->training_program_id;
					$blendedProgId			=	$resblendedProg->blended_item_id;
					$item_id				=	$resblendedProg->item_id;
					$total_items			= 	1;
					$total_credit 			= 	0;
					$total_cost				= 	0;
					$item_completed			= 	0;
					$date_completion		= 	$lerningplanassigndate;
					$itemIdStr				= 	'';
					$image_name				= 	'flag_red.png';
					$expiration_date		= 	'';
					$due_date				=	$resblendedProg->due_date;
					$expire_date			=   $resblendedProg->expire_date;
					$assigned_date			=	$resblendedProg->assigned_date;
					$start_date				=	$resblendedProg->start_date;
					$end_date				=	$resblendedProg->end_date;
					$percent_progress		=	0;
					$total_ass_items        =   0;
					$total_training_items   =   0 ;
					
					$blendedItemsSql = "select bti.item_id, bti.training_type_id, im.credit_value, im.item_cost, ut.transcript_id, ut.completion_date from blendedprogram_training_items bti
					join item_master im on im.item_id = bti.item_id
					left join user_transcript ut on ut.item_id = im.item_id and ut.is_active = 1 and ut.user_id = '$userid'
					where bti.blendedprogram_id=$blendedProgId and bti.is_active = 1 and bti.is_delete = 1 and im.company_id = $compid and im.is_active = 1 group by im.item_id";
					$blendedItemsResult    =DB::select( $blendedItemsResult1 );
					if(count($blendedItemsResult)) {
						$total_training_items		= count($blendedItemsResult);
						foreach($blendedItemsResult as $resblendedItems)
						{
							$itemId					= 	$resblendedItems->item_id;
							$training_type_id 		=	$resblendedItems->training_type_id;
							$credit_value	 		=	$resblendedItems->credit_value;
							$item_cost		 		=	$resblendedItems->item_cost;
							$transcript_id			=	$resblendedItems->transcript_id;
							$transcript_id			=	$resblendedItems->transcript_id;
							$completion_date		= 	$resblendedItems->completion_date;
							$total_credit			= 	$total_credit + $credit_value;
							$total_cost				=	$total_cost	+ $item_cost;
							if($transcript_id > 0)
								$item_completed 	= $item_completed + 1;
						}
					}
					
					$blendedAssessSql = "select bti.assessment_id, bti.training_type_id, ass.credit_value, ut.transcript_id, ut.completion_date from blendedprogram_training_items bti
					join assessment ass on ass.id = bti.assessment_id
					left join user_transcript ut on ut.registration_id = ass.id and ut.is_blended = 0 and ut.is_active = 1 and ut.user_id = '$userid'
					where bti.blendedprogram_id=$blendedProgId and bti.is_active = 1 and bti.is_delete = 1 and ass.company_id = $compid and ass.active_status = 1 and ass.is_delete = 0";
					$blendedAssessResult1   = DB::select($blendedAssessResult1);
					if(count($blendedAssessResult1)){
						$total_ass_items			= count($blendedAssessResult1);
						foreach ($blendedAssessResult1 as $resblendedAssessItems )
						{
							$itemId					= 	$resblendedAssessItems->assessment_id;
							$training_type_id 		=	$resblendedAssessItems->training_type_id;
							$credit_value	 		=	$resblendedAssessItems->credit_value;
							$transcript_id			=	$resblendedAssessItems->transcript_id;
							$completion_date		= 	$resblendedAssessItems->completion_date;
							$total_credit			= 	$total_credit + $credit_value;
							if($transcript_id > 0)
								$item_completed 	= $item_completed + 1;	
						}
					}
					
					$total_items = 	$total_training_items + $total_ass_items ;
					if($total_items == $item_completed)
					{
						$expiration_date 		= $expire_date;
						$assigned_date 			= $assigned_date;
						$due_date				= '';
						$image_name				= 'u184.png';
						$percent_progress		= 100;										
					}
					else
					{
						$percent_progress 		= floor(($item_completed/$total_items) * 100 );
						$image_name				= 'flag_red.png';					
						$assigned_date 			= $assigned_date;
						$due_date				= $due_date;					
						$expiration_date 		= '';
					}
					$learningPlanDataArr ['credit_value'] 		= $total_credit;
					$learningPlanDataArr ['due_date'] 			= $due_date;
					$learningPlanDataArr ['exp_date'] 			= $expiration_date;
					$learningPlanDataArr ['start_date'] 		= $start_date;
					$learningPlanDataArr ['end_date'] 			= $end_date;
					$learningPlanDataArr ['percent'] 			= $percent_progress;
					$learningPlanDataArr ['imagname'] 			= $image_name;
					$learningPlanDataArr ['item_name'] 			= $blendedProgName;
					$learningPlanDataArr ['training_type'] 		= 'Blended Program';
					$learningPlanDataArr ['trainingProgramId'] 	= $tariningProgId;
					$learningPlanDataArr ['assigned_date'] 		= $assigned_date;
						
				}



		}	
		return $learningPlanDataArr;
	}
	
	
	public static function getTotalPercentageForadmin($compid, $userid)
	{
		  $learningplanidfromusers = UserLpProgress::join('learning_plan_master','learning_plan_master.learning_plan_id','=','user_learning_plan_progress.learning_plan_id')->where('learning_plan_master.is_active','=',1)->where('user_learning_plan_progress.company_id','=',$compid)->where('learning_plan_master.company_id','=',$compid)->where('user_learning_plan_progress.user_id','=',$userid)->select('user_learning_plan_progress.no_of_items as total_items','user_learning_plan_progress.completed_items as item_completed')->get();

		  return $learningplanidfromusers; 
	}
	
	
}
