<?php
namespace App\Http\Traits;

use App\Models\CompanyCmsInfo;
use App\Models\UserMaster;
use App\Models\SubadminPermissions;
use App\Models\CompanySettings;
use App\Models\UserCustomFileds;
use App\Models\UserCustomValue;
use App\Models\RoleMaster;
use App\Models\UserSkill;
use App\Models\CountryMaster;
use App\Models\AreaMaster;
use App\Models\LocationMaster;
use App\Models\UserFieldSettings;
use App\Models\GroupMaster;
use App\Models\UserGroup;
use App\Models\UserIdentifier;
use App\Models\UserJobtitle;
use App\Models\JobTitleGroup;
use App\Models\UserBandLevel;
use App\Models\DivisionMaster;
use App\Models\InstructorMaster;
use App\Models\UserDivision;
use App\Models\SubOrganizationMaster;
use App\Models\SubAdminAssignedGroup;
use App\Models\UserOrganization;
use App\Models\UserSupervisor;
use App\Models\BandLevelMaster;
use App\Models\NotificationCompany;
use App\Models\GroupSetting;
use DB,Mailnotification,Auth;


trait Admin{
	
	public static function getcompanyname($compid) 
	{
		$result = CompanyCmsInfo::wherecompanyId($compid)->get();
		return $result;
	}
	
	public static $vSomeSpecialChars = array("quesmark", "permark", "bckslsh", "plusmark"
					, "nquot;", "singleqoute", "ngt;"
					, "nlt;", "namp;", "Ó", "Ú", "ñ", "Ñ", '“', '”'
					, "dbl_s", "##", "smlBracOpn", "smlBracCls", "bigBracOpn", "bigBracCls", 'hash', 'dollar', 'star');
					
    public static $vReplacementChars = array("?", "%", "/", '+', '"', "'", ">", "<", "&", "O", "U", "n", "N"
					, '"', '"',    "§", "&", "(", ")", "[", "]", '#', '$', '*');
					
	public static function getUserName($ids, $compid){
	    $result = UserMaster::join('role_master','role_master.role_id','=','user_master.role_id')->where('user_master.user_id','=',$ids)->where('user_master.company_id','=',$compid)->where('user_master.is_delete','=',0)->select('user_master.*','role_master.role_name')->get();
		return $result;	
	}

	
	public static function getUserInformation($ids, $compid)
	{
		$result=UserMaster::join('role_master','role_master.role_id','=','user_master.role_id')
		                  ->join('user_jobtitle','user_jobtitle.user_id','=','user_master.user_id')
		                  ->leftjoin('job_title_group','job_title_group.job_id','=','user_jobtitle.job_id')
						  ->where('user_master.user_id',$ids)
						  ->where('user_master.company_id',$compid)
						  ->where('user_master.is_delete',0)
						  ->select('user_master.*','role_master.role_name',DB::raw('GROUP_CONCAT(job_title_group.jobtitle_group) as job_title'))
						  ->get();
		return $result;				  
	}
	
	public static function getsubadminpermissiondata($compid)
	{
		$result = SubadminPermissions::where('is_active','=',1)->where('company_id','=',$compid)->get()->toArray();
		return $result;
	}
	public static function checkduplicatedataforcompsetting($compid) 
	{
		$result=CompanySettings::wherecompanyId($compid)->get();
		return $result;
	}
	
	public static function getInstructor($compid,$roleid,$userid)
    {
		$sel_arr = array();
		if($roleid!=1)
		{
			$sql = "select um.* from user_master as um 
			join user_group ug on ug.user_id=um.user_id
			join group_master gm on gm.group_id=ug.group_id
			where ug.group_id in (select CONCAT(saag.group_id) from sub_admin_assigned_group saag where saag.user_id=:user_id and saag.is_active=1) 
			and um.is_active= 1 and um.is_approved=1 and um.is_delete=0 and um.role_id in (1,5, 3) and um.company_id=:company_id1 and gm.company_id=:company_id2 order by um.user_name";
			$sel_arr['user_id'] = $userid;
			$sel_arr['company_id1'] = $compid;
			$sel_arr['company_id2'] = $compid;
		}
		else
		{
			$sql = "select um.* from user_master as um 
			where um.is_active= 1 and um.is_approved=1 and um.is_delete=0 and um.role_id in (1, 3,5) and um.company_id= :company_id1 order by um.user_name";
			$sel_arr['company_id1'] = $compid;
	
		}
		$result=DB::select(DB::raw($sql),$sel_arr);
		return $result;
	}
	
	public static function getuserdetail($ids, $compid)
	{
		 $sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as job_title,blm.band_name,rm.role_name,am.area_name,lm.location_name,divisions.division_name,comp.company,us.supervisor_id
				FROM  user_master um				
				left join user_supervisor us on us.user_id=um.user_id and us.is_active =1
				left join user_jobtitle ujt on ujt.user_id=um.user_id and ujt.is_active=1 
				left join job_title_group jtg on jtg.job_id=ujt.job_id	and jtg.is_active=1	
				left join user_band_level ubl on ubl.user_id=um.user_id and ubl.is_active=1 
				left join band_level_master blm on blm.band_id=ubl.band_id	and blm.is_active=1					
				join role_master rm on um.role_id=rm.role_id and rm.is_active=1
				left join area_master am on am.area_id=um.area
				left join location_master lm on lm.location_id=um.location
				left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt
				left join division_master dm on dm.division_id=udt.division_id and udt.is_active=1 and dm.is_delete=0 group by udt.user_id) divisions on divisions.user_id = um.user_id  
				left join (select group_concat(sb.sub_organization_name) as company,uo.sub_organization_id, uo.user_id from user_organization uo
				left join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and uo.is_active=1 and sb.company_id=:compId1 and sb.is_active=1 and sb.is_delete=0 group by uo.user_id) comp on comp.user_id = um.user_id  
				where um.company_id = :compId2 AND um.user_id=:Ids and um.is_delete = 0  group by um.user_id order by um.user_name ASC";
			$itemArr=array();
			$itemArr['compId1']=$compid;
			$itemArr['compId2']=$compid;
			$itemArr['Ids']=$ids;
			$result = DB::select(DB::raw($sql),$itemArr);

			return $result;
		
	}
	
	public static function getcustomfieldnameandidbycompnyname($compid) 
	{
		$result=UserCustomFileds::where('company_id',$compid)->get();
		return $result;
	}
	
	
	public static function getcustomfieldvalue($customfeildid,$userid,$compid) 
	{
		$result=UserCustomValue::where('custom_field_id','=',$customfeildid)->where('company_id','=',$compid)->where('user_id','=',$userid)->get();
		return $result;
		
	}
	
	public static function getrolenamesbyroleid($roleid) {
		$result = RoleMaster::where('role_id',$roleid)->get();
		return $result;
		
	}
	
	public static function getuserskills($edituserid, $compid) {
		$result=UserSkill::join('skills_master as skills',function($join){
			                          $join->on('user_skill.skill_id','=','skills.skill_id')
									       ->where('skills.is_active',1);
		})
		                              ->join('user_master as user_info','user_info.user_id','=','user_skill.user_id')
									  ->where('user_info.user_id',$edituserid)
									  ->where('user_info.is_delete',0)
									  ->where('user_info.company_id',$compid)
									  ->where('user_skill.is_active',1)
									  ->whereNotNull('user_skill.course_id')
									  ->groupBy('user_skill.skill_id')
									  ->select('user_info.*','skills.skill_name','skills.skill_id','user_skill.level_id as skill_rating')
									  ->get();
      return $result;									  
									  
		
	}
   
   
   
    public static function getCountry() {
		$result =CountryMaster::select('countryid','country')->orderby('country','asc')->get();
		return $result;
	}
	
	public static function getallareadata($compid)
	{
		$result =AreaMaster::where('company_id','=',$compid)->where('is_active', '=', 1)->where('is_delete','=',0)->where('area_name','!=','')
		->orderby('area_name','ASC')->select('area_id','area_name')->get();
		return $result;
	}
	
	public static function getalllocationdata($compid)
	{
	   $result =LocationMaster::where('company_id','=',$compid)->where('is_active', '=' ,1)->where('is_delete','=',0)
	   ->where('location_name','!=','')
			->orderby('location_name', 'ASC')->select('location_id','location_name')->get();
		return $result;
	}
	
	public static function checkfield($fieldid,$compid) {
		$result=UserFieldSettings::where('field_id','=',$fieldid)->where('company_id','=',$compid)->where('is_enable','=',1)->select('field_id')->first();
		return $result;
	}
	public static function chkSubAdminPerSetting($compid)
	{
		$result=SubadminPermissions::where('company_id',$compid)->get();
		return $result;
	}
	
	public static function getgroupnames($request,$compid,$roleid,$userid) {
		
		//return $result = ['id'=>'12','firstName'=>'vijay','lastName'=>'kumar']];
		//$result = GroupMaster::select();
		//print_r($request->input('draw'));exit;
		//echo $request->input('start');
		//echo $request->input('length');
	//	exit;
		  $standardcomp=array(83,84);

		if($roleid == 5){
			$result = GroupMaster::join('sub_admin_assigned_group','sub_admin_assigned_group.group_id','=','group_master.group_id')->where('group_master.is_delete',0)->where('sub_admin_assigned_group.user_id',$userid)->where('sub_admin_assigned_group.is_active',1)->where('group_master.company_id',$compid)->where('group_master.is_active',1)->where('group_master.group_name','!=','')->where('group_master.is_delete',0)->groupBy('group_master.group_name','ASC');
				 if(in_array($compid,$standardcomp)){
				$result = $result->whereNotNull('group_master.reference_id')->where('group_master.reference_id','!=','');
		     }
			
		}else{
			 
			    $result = GroupMaster::where('group_master.company_id',$compid)->where('group_master.is_active',1)->where('group_master.group_name','!=','')->where('group_master.is_delete',0);
				if(in_array($compid,$standardcomp)){
				$result = $result->whereNotNull('group_master.reference_id')->where('group_master.reference_id','!=','');
		     }
		}
		$result = $result->skip($request->input('start'))->take($request->input('length'));
		if($request->input('search.value')){
		   $result = $result->where('group_name','LIKE','%'.$request->input('search.value').'%')->orWhere('description','LIKE','%'.$request->input('search.value').'%');
		}	
		if($request->input('order.0.dir')){
			  if($request->input('order.0.column')==2){
				$result = $result->orderby('group_name',$request->input('order.0.dir'));
			  }
			  if($request->input('order.0.column')==3){
				$result = $result->orderby('description',$request->input('order.0.dir'));
			  }
		}
		
		return $result->get();

	}
	
	public static function getUsersForSelectedGroup($compid, $groupid)
	{
		/* $db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_OBJ); */
		/* $result = UserGroup::join('group_master','group_master.group_id','=','user_group.group_id')
		                     ->join('user_master','user_group.user_id','=','user_master.user_id')
							 ->join('user_grp_positions',funciton($query){
								 $query->on('user_grp_positions.user_id','=','user_master.user_id')->where('group_master.group_id','user_grp_positions.group_id')->where('user_grp_positions.is_active',1);
							 })
							 ->join('role_master','role_master.role_id','=','user_master.user_id')
							 ->leftJoin('manage_role_field',funciton($query){
								 $query->on('manage_role_field.role_id','=','role_master.area')->where('manage_role_field.company_id','user_master.company_id')
							 })
							 ->leftJoin('area_master','area_master.area_id','=','user_master.user_id')
							 ->leftJoin('location_master','location_master.location_id','=','user_master.location')
							 
							 ->leftJoin('divisions',funciton($query){
								 $query->join('division_master',function($sql){
									 $sql->on('division_master.division_id','=','user_division.division_id')->from('user_division')->where('user_division.is_active',1)->where('division_master.is_active',1)->where('division_master.is_delete',0)->groupBy('user_division.user_id')->select(DB::raw('group_concat(dm.division_name) as division_name'),'user_division.division_id','user_division.user_id')
								 })->on('divisions.user_id','=','user_master.user_id')
							 })
							 ->leftJoin('comp',funciton($query){
								 $query->join('sub_organization_master',function($sql){
									 $sql->on('sub_organization_master.sub_organization_id','=','user_organization.sub_organization_id')->from('user_organization')->where('user_division.is_active',1)->where('user_organization.is_active',1)->where('sub_organization_master.is_active',1)->where('sub_organization_master.is_delete',0)->groupBy('user_organization.user_id')->select(DB::raw('group_concat(sb.sub_organization_name) as company'),'user_organization.sub_organization_id','user_organization.user_id')
								 })->on('comp.user_id','=','user_master.user_id')
							 })->where('user_group.group_id',$groupid)->where('user_master.company_id',$compid)
							 
							 ->select('user_group.user_id','area_master.area_name','location_master.location_name','user_master.user_name','user_master.area','user_master.user_id','user_master.location','manage_role_field.field_name','role_master.role_name','role_master.role_id','comp.company','divisions.division_name')->get();
		
		return $result; */
 		if(in_array($compid,array(83,84))){
		$sql = "SELECT  crg.user_id, am.area_name, lm.location_name, um.user_name, um.user_id, mrf.field_name, rm.role_name, rm.role_id, comp.company, divisions.division_name, um.`area`, um.location FROM user_group crg 
		join group_master gm on gm.group_id = crg.group_id
		JOIN user_master um ON (crg.user_id = um.user_id) 
		
		
		join  user_grp_positions as ugp on ugp.user_id=um.user_id  and gm.group_id=ugp.group_id and ugp.is_active=1
		JOIN role_master rm ON (rm.role_id = um.role_id)
		left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id = um.company_id
		left join area_master am on am.area_id=um.area
		left join location_master lm on lm.location_id=um.location
		left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt
		join division_master dm on dm.division_id=udt.division_id and udt.is_active=1 and dm.is_active=1 and dm.is_delete=0 group by udt.user_id) divisions on divisions.user_id = um.user_id
		left join (select group_concat(sb.sub_organization_name) as company,uo.sub_organization_id, uo.user_id from user_organization uo
		join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and uo.is_active=1 and sb.is_active=1 and sb.is_delete=0 group by  uo.user_id ) comp on comp.user_id = um.user_id 
		
		
		WHERE  crg.group_id = :groupid AND um.company_id = :compid AND um.is_delete = 0 AND um.is_approved = 1 AND um.is_active = 1 AND crg.is_active = 1 and gm.is_active = 1 and gm.is_delete = 0 group by crg.user_id";
		}else{
		 $sql = "SELECT  crg.user_id, am.area_name, lm.location_name, um.user_name, um.user_id, mrf.field_name, rm.role_name, rm.role_id, comp.company, divisions.division_name, um.`area`, um.location FROM user_group crg 
		join group_master gm on gm.group_id = crg.group_id
		JOIN user_master um ON (crg.user_id = um.user_id) 
		JOIN role_master rm ON (rm.role_id = um.role_id)
		left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id = um.company_id
		left join area_master am on am.area_id=um.area
		left join location_master lm on lm.location_id=um.location
		left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt
		join division_master dm on dm.division_id=udt.division_id and udt.is_active=1 and dm.is_active=1 and dm.is_delete=0 group by udt.user_id) divisions on divisions.user_id = um.user_id
		left join (select group_concat(sb.sub_organization_name) as company,uo.sub_organization_id, uo.user_id from user_organization uo
		join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and uo.is_active=1 and sb.is_active=1 and sb.is_delete=0 group by  uo.user_id ) comp on comp.user_id = um.user_id 
		WHERE  crg.group_id = :groupid AND um.company_id = :compid AND um.is_delete = 0 AND um.is_approved = 1 AND um.is_active = 1 AND crg.is_active = 1 and gm.is_active = 1 and gm.is_delete = 0 group by crg.user_id";
		}
   		//$result = $db->fetchAll($sql,array($groupid,$compid)); 
		$itemArr=array();
			$itemArr['groupid']=$groupid;
			$itemArr['compid']=$compid;
		//	$itemArr['Ids']=$ids;
			$result = DB::select(DB::raw($sql),$itemArr);
		
		return $result;
	}

	
	public static function checkUserLoginIdentifier($username, $compid)
	{
		$login_name = $compid.'_#_'.$username;
		$result =UserIdentifier::where('user_identity','=',trim(strtolower($login_name)))->where('company_id','=',$compid)->first();
		return $result;
	}
	
	public static function checkemail($emailid, $compid)
	{
		$emailid = addslashes($emailid);
		$result=UserMaster::where('email_id','=',$emailid)->where('company_id','=',$compid)->first();
		return $result;
	}
	
	public static function checksettings($compid){
		$result=CompanySettings::where('company_id','=',$compid)->select('change_password')->first();
		return $result;
	}
	
	public static function adduseridentifier($user_identity){
		UserIdentifier::create($user_identity);
		return true;
	}
	
	public static function checkjob_titlename($job_title, $compid, $user_id) {
	    
		if(($job_title)){
			foreach($job_title as $job)
			{
			    $job=addslashes($job);
				$jobids =0;
				$result = JobTitleGroup::where('company_id','=',$compid)->where('job_id','=',$job)->first();
				$data = JobTitleGroup::where('company_id','=',$compid)->where('jobtitle_group','=',$job)->first();
				if($result){
					$jobids = $result->job_id;	
				}elseif($data){
					$jobids = $data->job_id;	
				}else{ 
					$dataa=array();
					$dataa['jobtitle_group']=trim(addslashes($job));
					$dataa['company_id']=$compid;
					$jobsall=JobTitleGroup::create($dataa);
					$jobids=$jobsall->job_id;
				}
				if($jobids !=0){	
				   	$start_time = date('Y-m-d -h-i-s');
					$dataas=array();
					$dataas['job_id']=trim($jobids);
					$dataas['user_id']=$user_id;
					$dataas['start_date_time']=$start_time;
					UserJobtitle::create($dataas);	
				}	
			}
		}
	}
	
	public static function check_divisionname($division_name,$compid,$user_id)
	{
		if($division_name)
		{
			foreach($division_name as $division)
			{
				$division =addslashes($division);
				$divisionids =0;
				$result = DivisionMaster::where('company_id','=',$compid)->where('division_id','=',$division)->first();
				$data = DivisionMaster::where('company_id','=',$compid)->where('division_name','=',$division)->first();
				 
				
				if($result){
					$divisionids = $result->division_id;	
				}elseif($data){
					$divisionids = $data->division_id;	
				}else{ 
					$dataas=array();
					$dataas['division_name']=trim(addslashes($division));
					$dataas['company_id']=$compid;
					$divisionid=DivisionMaster::create($dataas);
					$divisionids=$divisionid->division_id;
				}
				if($divisionids !=0)
				{
				    $start_time = date('Y-m-d -h-i-s');
					$dataa=array();
					$dataa['division_id']=trim($divisionids);
					$dataa['user_id']=$user_id;
					$dataa['start_date_time']=$start_time;
					$dataa['end_date_time'] = $start_time ;
					UserDivision::create($dataa);	
				}	
			}	
		}
	}
	
	public static function check_companyname($company_name,$compid,$user_id) {
	   
		if($company_name){

			foreach($company_name as $company)
			{
			    $company=addslashes($company);
				$companyids =0;
				$result =SubOrganizationMaster::where('company_id','=',$compid)->where('sub_organization_id','=',$company)->first();
				$data   =SubOrganizationMaster::where('company_id','=',$compid)->where('sub_organization_name','=',$company)->first();
				
				if($result){
					$companyids = $result->sub_organization_id;	
				}elseif($data){
					$companyids = $data->sub_organization_id;	
				}
				else{ 
					$dataa=array();
					$dataa['sub_organization_name']=trim(addslashes($company));
					$dataa['company_id']=$compid;
					$company=SubOrganizationMaster::create($dataa);
					$companyids =  $company->sub_organization_id;
				}
				if($companyids !=0){
					$start_time = date('Y-m-d -h-i-s');
					$dataas=array();
					$dataas['sub_organization_id']=trim($companyids);
					$dataas['user_id']=$user_id;
					$dataas['start_date_time']=$start_time;
					$dataas['end_date_time']=$start_time;
					UserOrganization::create($dataas);	   
				}	
			}	
		}
	}
	
	public static function checkgroupnames($group_name,$compid,$user_id,$roleid,$userLoggedIn) {	    
		if($group_name){
			foreach($group_name as $group)
			{  
				 $groupids =0;
				 $group=addslashes($group);
				 $result = GroupMaster::where('company_id','=',$compid)->where('group_id','=',$group)->first();
				 $data 	 = GroupMaster::where('company_id','=',$compid)->where('group_name','=',$group)->first();
				if($result){
				 $groupids= $result->group_id;	
				}elseif($data){
                  $groupids= $data->group_id;	
				}
				else{
					$update_time = date('Y-m-d -h-i-s');
					$dataas=array();
					$dataas['group_name']=trim($group);
					$dataas['company_id']=$compid;
					$dataas['created_by']=$userLoggedIn;
					$dataas['autogroup']=0;
					$dataas['last_updated_time']=$update_time;
                    $group=GroupMaster::create($dataas);		
                    $groupids=$group->group_id;	
					if($roleid==5 || $roleid==3){
					$dataa=array();
					$dataa['group_id']=$groupids;
					$dataa['user_id']=$user_id;
					SubAdminAssignedGroup::create($dataa);
			      
					}	
				}
			   if($groupids !=0){
				$start_time = date('Y-m-d -h-i-s');
				$datas=array();
				$datas['group_id']=trim($groupids);
				$datas['user_id']=$user_id;
				$datas['auto_group']=0;
				$datas['created_by']=$userLoggedIn;
				$datas['updated_by']=$userLoggedIn;
				$datas['created_date']=$start_time;
				UserGroup::create($datas);
				}
			}
		}
	}
	
	public static function check_supervisor($supervisor_id,$compid,$user_id)
	{
		if($supervisor_id)
		{
				$start_time = date('Y-m-d -h-i-s');
				$end_time = date('Y-m-d -h-i-s');
				$datas=array();
				$datas['supervisor_id']=$supervisor_id;
				$datas['user_id']=$user_id;
				$datas['company_id']=$compid;
				$datas['start_time']=$start_time;
				$datas['end_time']=$end_time;
				UserSupervisor::create($datas);
		}
	}
	
	public function check_bandlevel($band_name,$compid,$user_id)
	{
		if($band_name)
		{	
			$bandids =0;
			$band_name=addslashes($band_name);
			$result =BandLevelMaster::where('band_id','=',$band_name)->where('company_id','=',$compid)->first();
			$data 	=BandLevelMaster::where('band_name','=',$band_name)->where('company_id','=',$compid)->first();
			if($result){
				$bandids = $result->band_id;	
			}elseif($data){
				$bandids = $data->band_id;
			}else{ 		
				$dataa=array();
				$dataa['band_name']=trim($band_name);
				$dataa['company_id']=$compid;
				$dataa['created_by']=Auth::user()->user_id;
				$band=BandLevelMaster::create($dataa);
				$bandids =$band->band_id;
			}
		
			if($bandids!=0)
			{					
				$datas=array();
				$datas['band_id']=trim($bandids);
				$datas['user_id']=$user_id;
				$datas['company_id']=$compid;
				$dataa['created_by']=Auth::user()->user_id;
				UserBandLevel::create($datas);				
			}
		}
	}
	
	public static function updatecustomvalue($customfieldvalue, $where, $customfieldId, $userid, $compid) {

		$result = UserCustomValue::where('custom_field_id','=',$customfieldId)->where('company_id','=',$compid)->where('user_id','=',$userid)->select('field_value')->first();
		if(is_string($customfieldvalue)){		
		 $customfieldvalue = trim($customfieldvalue);
		}else{
		 $customfieldvalue = ($customfieldvalue);
		}
		if($result)
		{
			$data=array();
			$data['field_value']		=$customfieldvalue;
			$where['custom_field_id'] 	= $customfieldId;
			$where['user_id'] 			= $userid;
			$where['company_id'] 		= $compid;
			UserCustomValue::where('custom_field_id','=',$where['custom_field_id'])->where('user_id','=',$where['user_id'])->where('company_id','=',$where['company_id'])->update($data);
			
		}
		else
		{
			   $result = UserCustomFileds::where('company_id','=',$compid)->where('custom_field_id','=',$customfieldId)->select('column_type')->first();
				$column_type=$result->column_type;
				$dataa=array();
				if($column_type=='Date'){
				$dataa['field_value']=date('Y-m-d',strtotime(str_replace('-', '/', $customfieldvalue)));;
				}else{
				$dataa['field_value']=$customfieldvalue;
				}
			   
				$dataa['custom_field_id']=trim($customfieldId);
				$dataa['user_id']=$userid;
				$dataa['company_id']=$compid;
				UserCustomValue::create($dataa);
		}
		
	}
	
	public static function checkforwelcomemail($lastinid,$compid,$email,$username,$password)
	{
		$result1=NotificationCompany::where('notification_id','=',27)->where('company_id','=',$compid)->select('is_active')->first();
	
		$notification_company = $welcome_user_mail = '';
		if($result1)
		{ 
	     $notification_company=$result1->is_active; 
	    }
         $result=CompanySettings::where('company_id','=',$compid)->select('welcome_user_mail')->first();
	
		if($result)
		{
			$welcome_user_mail = $result->welcome_user_mail; 
	    }
		if($welcome_user_mail && $notification_company)
		{
			Mailnotification::welcomeemail($lastinid,$compid,$email,$username,$password,'');
		}

		return;
	}
	
	public static function checkgroupname($groupname, $compid)
	{
		$result = GroupMaster::where('group_name','=',$groupname)->where('company_id','=',$compid)->get();
		return $result;
	}
	
	public static function checkactivegroup($groupname, $compid)
	{
		$result = GroupMaster::where('group_name','=',$groupname)->where('company_id','=',$compid)->where('is_active','=',1)->where('is_delete','=',0)->get();
		return $result;
	}
	
	public Static function updateanywhere($groupname,$compid,$data)
	{
		GroupMaster::where('group_name','=',$groupname)->where('company_id','=',$compid)->update($data);
		return 1;
	}
	
	public static function editgroupname($groupid, $compid) {
		$result = GroupMaster::where('company_id','=',$compid)->where('group_id','=',$groupid)->where('is_active','=',1)->where('is_delete','=',0)->first();
		return $result;
	}
	
	public static function checkGroupExceptThis($groupname, $groupid, $compid){
		$result = GroupMaster::where('group_name','=',$groupname)->where('group_id','!=',$groupid)->where('company_id','=',$compid)->get();
		return $result;
	}
	
	
	public static function getmyuseronly($compid, $user_id, $roleid, $suborgId='', $subadminbasedon='', $instructorbasedon='')
	{
		/* $db = Zend_Db_Table::getDefaultAdapter();
		$db->setFetchMode(Zend_Db::FETCH_OBJ); */
		$itemArr=array();
		if($roleid == 5 || $roleid == 3)
		{
			if($roleid == 5 && $subadminbasedon == 1)
			{
				$sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as jobTitles,am.area_name,lm.location_name,rm.role_name,mrf.field_name, divisions.division_name,company.sname
				FROM  user_master um 
				join user_group ug on ug.user_id = um.user_id and ug.is_active=1
				join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0
				join user_jobtitle ujt on ujt.user_id=um.user_id
				join job_title_group jtg on jtg.job_id=ujt.job_id
				join role_master rm on um.role_id=rm.role_id
				left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id = um.company_id
				left join area_master am on am.area_id=um.area
				left join location_master lm on lm.location_id=um.location
				left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt join division_master dm on dm.division_id=udt.division_id and dm.is_active=1 and dm.is_delete=0 and udt.is_active=1 group by udt.user_id) divisions on divisions.user_id = um.user_id
				join (select group_concat(sb.sub_organization_name) as sname,uo.sub_organization_id, uo.user_id from user_organization uo
				join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and sb.is_active=1 and sb.is_delete=0 and uo.is_active=1 and sb.sub_organization_id = :suborgId group by uo.user_id) company on company.user_id = um.user_id  
				where um.company_id = :compid AND   um.is_active = 1 
				AND um.is_delete = 0 group by um.user_id order by um.user_name ASC";
			    $itemArr['suborgId']=$suborgId;
			    $itemArr['compid']=$compid;
			}
			else if($roleid == 3 && $instructorbasedon == 1)
			{
				$sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as jobTitles,am.area_name,lm.location_name,rm.role_name,mrf.field_name, divisions.division_name,company.sname
				FROM  user_master um 
				join user_group ug on ug.user_id = um.user_id and ug.is_active=1
				join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0
				join user_jobtitle ujt on ujt.user_id=um.user_id
				join job_title_group jtg on jtg.job_id=ujt.job_id
				join role_master rm on um.role_id=rm.role_id
				left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id = um.company_id
				left join area_master am on am.area_id=um.area
				left join location_master lm on lm.location_id=um.location
				left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt join division_master dm on dm.division_id=udt.division_id and dm.is_active=1 and dm.is_delete=0 and udt.is_active=1 group by udt.user_id) divisions on divisions.user_id = um.user_id
				join (select group_concat(sb.sub_organization_name) as sname,uo.sub_organization_id, uo.user_id from user_organization uo
				join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and sb.is_active=1 and sb.is_delete=0 and uo.is_active=1 and sb.sub_organization_id = :suborgId group by uo.user_id) company on company.user_id = um.user_id  
				where um.company_id = :compid AND   um.is_active = 1 
				AND um.is_delete = 0 group by um.user_id order by um.user_name ASC";
				$itemArr['suborgId']=$suborgId;
			    $itemArr['compid']=$compid;
			}
			else
			{
				$sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as jobTitles,am.area_name,lm.location_name,rm.role_name,mrf.field_name, divisions.division_name,company.sname
				FROM  user_master um 
				join user_group ug on ug.user_id = um.user_id and ug.is_active=1
				join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0
				join user_jobtitle ujt on ujt.user_id=um.user_id
				join job_title_group jtg on jtg.job_id=ujt.job_id
				join role_master rm on um.role_id=rm.role_id
				left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id = um.company_id
				left join area_master am on am.area_id=um.area
				left join location_master lm on lm.location_id=um.location
				left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt join division_master dm on dm.division_id=udt.division_id and dm.is_active=1 and dm.is_delete=0 and udt.is_active=1 group by udt.user_id) divisions on divisions.user_id = um.user_id
				join (select group_concat(sb.sub_organization_name) as sname,uo.sub_organization_id, uo.user_id from user_organization uo
				join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and sb.is_active=1 and sb.is_delete=0 and uo.is_active=1 group by uo.user_id) company on company.user_id = um.user_id  
				where  ug.group_id IN (SELECT CONCAT( group_id ) AS groups FROM sub_admin_assigned_group saag WHERE saag.user_id =:user_id AND saag.is_active =1) 
				and um.company_id = :compid AND   um.is_active = 1 
				AND   um.is_delete = 0 group by um.user_id order by um.user_name ASC";
			    $itemArr['compid']=$compid;
			    $itemArr['user_id']=$user_id;
			}
		}else{
			$sql = "SELECT um.*,group_concat(jtg.jobtitle_group) as jobTitles,am.area_name,lm.location_name,rm.role_name,mrf.field_name,divisions.division_name,company.sname 
			FROM user_master um 
			join user_jobtitle ujt on ujt.user_id=um.user_id 
			join job_title_group jtg on jtg.job_id=ujt.job_id 
			join role_master rm on um.role_id=rm.role_id
			left join manage_role_field mrf on mrf.role_id = rm.role_id and mrf.company_id = um.company_id
			left join area_master am on am.area_id=um.area
			left join location_master lm on lm.location_id=um.location
			left join (select group_concat(dm.division_name) as division_name,udt.division_id, udt.user_id from user_division udt left join division_master dm on dm.division_id=udt.division_id and dm.is_active=1 and dm.is_delete=0 and udt.is_active=1 group by udt.user_id) divisions on divisions.user_id = um.user_id 
			left join (select group_concat(sb.sub_organization_name) as sname,uo.sub_organization_id, uo.user_id from user_organization uo left join sub_organization_master sb on sb.sub_organization_id=uo.sub_organization_id and sb.is_active=1 and sb.is_delete=0 and uo.is_active=1
			group by uo.user_id) company on company.user_id = um.user_id 
			where um.company_id = :compid AND um.is_approved = 1 AND um.is_active = 1 AND um.is_delete = 0 
			group by um.user_id order by um.user_name ASC" ;
			$itemArr['compid']=$compid;
		}
		$result = DB::select(DB::raw($sql),$itemArr);
		return $result;
	}
	
	
	public static function fetchgroupsetting($compid){
		$result = GroupSetting::where('company_id','=',$compid)->get();
		return $result;
	}
	
	public static function deletegroup($id, $compid)
	{
		$idimp = count(explode(',', $id));
		if($idimp > 0){
			$id_arr = explode(',', $id);
			GroupMaster::where('company_id','=',$compid)->whereIn('group_id', $id_arr)->update(['is_delete'=>1]);
		}
		else{
			GroupMaster::where('company_id','=',$compid)->where('group_id','=', $id)->update(['is_delete'=>1]);
	}
	}
	
	public static function getUserGroup($edituserid, $compid) {
		$result=UserGroup::join('group_master','group_master.group_id','=','user_group.group_id')->join('user_master','user_master.user_id','=','user_group.user_id')
		->where('user_group.user_id','=',$edituserid)->where('user_group.is_active','=',1)->where('group_master.is_active','=',1)
		->where('group_master.is_delete','=',0)->where('group_master.company_id','=',$compid)->where('user_master.is_active','=',1)->groupBy('group_master.group_id')->select('group_master.group_id','group_master.group_name')->get();
		return $result;
	}
	
	public static function getuserjob($edituserid, $compid)
	{
		$result=UserJobtitle::join('job_title_group','job_title_group.job_id','=','user_jobtitle.job_id')->where('user_jobtitle.user_id','=',$edituserid)
		->where('user_jobtitle.is_active','=',1)->where('job_title_group.is_active','=',1)->where('job_title_group.company_id','=',$compid)
		->select('job_title_group.jobtitle_group','job_title_group.job_id')->get();
		return $result;
	}
	
	public static function getuserdivision($edituserid, $compid)
	{
		$result =UserDivision::join('division_master','division_master.division_id','=','user_division.division_id')
		->where('user_division.user_id','=',$edituserid)->where('user_division.is_active','=',1)->where('division_master.company_id','=',$compid)
		->select('division_master.division_name','division_master.division_id')->get();
		return $result;
	}
	
	public static function getusercompany($edituserid, $compid)
	{
		$result=UserOrganization::join('sub_organization_master','sub_organization_master.sub_organization_id','=','user_organization.sub_organization_id')
		->where('user_organization.user_id','=',$edituserid)->where('sub_organization_master.is_delete','=',0)->where('user_organization.is_active','=',1)->where('sub_organization_master.company_id','=',$compid)->select('sub_organization_master.sub_organization_name','sub_organization_master.sub_organization_id')->get();
		return $result;
	}
	
	public static function getuserareadata($userid,$compid)
	{
		$result = AreaMaster::join('user_master','user_master.area','=','area_master.area_id')
		->where('user_master.user_id','=',$userid)->where('area_master.company_id','=',$compid)->where('area_master.is_active','=',1)->where('area_master.is_delete','=',0)->select('area_master.area_name','area_master.area_id')->first();
		return $result;
	}
	
	public static function getuserlocationdata($userid,$compid)
	{ 
		$result =LocationMaster::join('user_master','user_master.location','=','location_master.location_id')
		->where('user_master.is_active','=',1)->where('user_master.is_delete','=',0)->where('user_master.company_id','=',$compid)
		->where('user_master.user_id','=',$userid)->where('location_master.company_id','=',$compid)->where('location_master.is_active','=',1)
		->where('location_master.is_delete','=',0)->select('location_master.location_name','location_master.location_id')->first();
		return $result;
	}
	
	public static function getusersupervisor($edituserid, $compid)
	{
		$result =UserSupervisor::join('user_master','user_master.user_id','user_supervisor.supervisor_id')->where('user_supervisor.user_id','=',$edituserid)
	    ->where('user_supervisor.company_id','=',$compid)->where('user_supervisor.is_active','=',1)->groupby('user_master.user_id')->select('user_master.user_name','user_master.user_id')->first();
		
		return $result;
	}
	
	public static function checkuserexceptthis($username, $edituserid, $compid) {
		$login_name = $compid.'_#_'.$username;
		$result=UserMaster::where('login_name','=',$login_name)->where('company_id','=',$compid)
		->where('user_id','!=',$edituserid)->first();
		return $result;
	}
	
	public static function updateUserIdentifier($edituserid,$compid,$user_identity)
	{
		UserIdentifier::where('user_id','=',$edituserid)->where('company_id','=',$compid)->update($user_identity);
	}
	
   public static function update_divisionname($division_name,$compid,$edituserid)
	{

		$datainactive=array('is_active'=>0);
		UserDivision::where('user_id','=',$edituserid)->update($datainactive);
		
		if(($division_name))
		{
			
			foreach($division_name as $divisionval)
			{
			    $division_value=isset($divisionval['division_id'])?$divisionval['division_id']:$divisionval;
				if(!is_numeric($division_value)){
					$vSomeSpecialChars=self::$vSomeSpecialChars;
					$vReplacementChars=self::$vReplacementChars;
					$division_value = trim(str_replace($vSomeSpecialChars, $vReplacementChars, $division_value));
					$division_value = htmlentities($division_value);
				}
                 
				$result = DivisionMaster::where('company_id','=',$compid)->where('division_id','=',$division_value)->first();
				$data   = DivisionMaster::where('company_id','=',$compid)->where('division_name','=',$division_value)->first();
				if($result){
					$divisionids = $result->division_id;	
				}elseif($data){
					$divisionids = $data->division_id;	
				}else{ 
					$dataas=array();
					$dataas['division_name']=trim(addslashes($division_value));
					$dataas['company_id']=$compid;
					$divisionid=DivisionMaster::create($dataas);
					$divisionids=$divisionid->division_id;
				}
				 
				$divisiondata =UserDivision::where('division_id','=',$divisionids)->where('user_id','=',$edituserid)->first();
				if($divisiondata)
				{
					$dataactive=array('is_active'=>1);
					UserDivision::where('user_id','=',$edituserid)->where('division_id','=',$divisionids)->update($dataactive);
					
				}
				else
				{
					if($divisionids !=0)
					{
						$start_time = date('Y-m-d -h-i-s');
						$dataa=array();
						$dataa['division_id']=trim($divisionids);
						$dataa['user_id']=$edituserid;
						$dataa['start_date_time']=$start_time;
						$dataa['end_date_time'] = $start_time ;
						UserDivision::create($dataa);	
					}
				}
			}	
		}
	}
	
	public static function update_companyname($company_name,$compid,$edituserid) 
	{
	   $datainactive=array('is_active'=>0);
	   UserOrganization::where('user_id','=',$edituserid)->update($datainactive);
	   if($company_name){

			foreach($company_name as $companyval)
			{
				$company_value=isset($companyval['sub_organization_id'])?$companyval['sub_organization_id']:$companyval;
				if(!is_numeric($company_value)){
					$vSomeSpecialChars=self::$vSomeSpecialChars;
					$vReplacementChars=self::$vReplacementChars;
					$company_value = trim(str_replace($vSomeSpecialChars, $vReplacementChars, $company_value));
					$company_value = htmlentities($company_value);
				}
				
			    $company=addslashes($company_value);
				$companyids =0;
				$result =SubOrganizationMaster::where('company_id','=',$compid)->where('sub_organization_id','=',$company)->first();
				$data   =SubOrganizationMaster::where('company_id','=',$compid)->where('sub_organization_name','=',$company)->first();
				
				if($result){
					$companyids = $result->sub_organization_id;	
				}elseif($data){
					$companyids = $data->sub_organization_id;	
				}
				else{ 
					$dataa=array();
					$dataa['sub_organization_name']=trim(addslashes($company));
					$dataa['company_id']=$compid;
					$company=SubOrganizationMaster::create($dataa);
					$companyids =  $company->sub_organization_id;
				}
				
				$companydata =UserOrganization::where('sub_organization_id','=',$companyids)->where('user_id','=',$edituserid)->first();
				if($companydata)
				{
					$dataactive=array(
						'is_active'=>1,
					);
					UserOrganization::where('sub_organization_id','=',$companyids)->where('user_id','=',$edituserid)->update($dataactive);
				}else{
					if($companyids !=0){
						$start_time = date('Y-m-d -h-i-s');
						$dataas=array();
						$dataas['sub_organization_id']=trim($companyids);
						$dataas['user_id']=$edituserid;
						$dataas['start_date_time']=$start_time;
						$dataas['end_date_time']=$start_time;
						UserOrganization::create($dataas);	   
					}	
				}
			}	
		}
	}
	
	public static function updatejob_titlenamenew($job_title, $compid, $edituserid)
	{
		$datainactive=array('is_active'=>0);
		UserJobtitle::where('user_id','=',$edituserid)->update($datainactive);
		if(($job_title)){
			foreach($job_title as $job)
			{
				$job_value=isset($job['job_id'])?$job['job_id']:$job;
				if(!is_numeric($job_value)){
					$vSomeSpecialChars=self::$vSomeSpecialChars;
					$vReplacementChars=self::$vReplacementChars;
					$job_value = trim(str_replace($vSomeSpecialChars, $vReplacementChars, $job_value));
					$job_value = htmlentities($job_value);
				}
				
			    $job=addslashes($job_value);
				
				$jobids =0;
				$result = JobTitleGroup::where('company_id','=',$compid)->where('job_id','=',$job)->first();
				$data = JobTitleGroup::where('company_id','=',$compid)->where('jobtitle_group','=',$job)->first();
				if($result){
					$jobids = $result->job_id;	
				}elseif($data){
					$jobids = $data->job_id;	
				}else{ 
					$dataa=array();
					$dataa['jobtitle_group']=trim(addslashes($job));
					$dataa['company_id']=$compid;
					$jobsall=JobTitleGroup::create($dataa);
					$jobids=$jobsall->job_id;
				}
				$jobdata =UserJobtitle::where('job_id','=',$jobids)->where('user_id','=',$edituserid)->first();
				if($jobdata)
				{
					$dataactive=array('is_active'=>1);
					UserJobtitle::where('job_id','=',$jobids)->where('user_id','=',$edituserid)->update($dataactive);
				}
				else
				{
					if($jobids !=0){	
						$start_time = date('Y-m-d -h-i-s');
						$dataas=array();
						$dataas['job_id']=trim($jobids);
						$dataas['user_id']=$edituserid;
						$dataas['start_date_time']=$start_time;
						UserJobtitle::create($dataas);	
					}	
			   }
			}
		}
	}
	
	public static function update_groupnames($group_name,$compid,$edituserid,$roleid,$userLoggedIn)
	{
		 $group_id = array();
		 if($group_name){
			foreach($group_name as $group)
			{  
			    $group_value=isset($group['group_id'])?$group['group_id']:$group;
				if(!is_numeric($group_value)){
					$vSomeSpecialChars=self::$vSomeSpecialChars;
					$vReplacementChars=self::$vReplacementChars;
					$group_value = trim(str_replace($vSomeSpecialChars, $vReplacementChars, $group_value));
					$group_value = htmlentities($group_value);
				}
				$groupids =0;
				$group=addslashes($group_value);
				$result = GroupMaster::where('company_id','=',$compid)->where('group_id','=',$group)->first();
				$data 	 = GroupMaster::where('company_id','=',$compid)->where('group_name','=',$group)->first();
				if($result){
				 $group_id[]=$groupids= $result->group_id;	
				}elseif($data){
                  $group_id[]=$groupids= $data->group_id;	
				}
				else{
					$update_time = date('Y-m-d -h-i-s');
					$dataas=array();
					$dataas['group_name']=trim($group);
					$dataas['company_id']=$compid;
					$dataas['created_by']=$userLoggedIn;
					$dataas['autogroup']=0;
					$dataas['last_updated_time']=$update_time;
                    $group=GroupMaster::create($dataas);		
                    $group_id[]=$groupids=$group->group_id;	
					if($roleid==5 || $roleid==3){
						$dataa=array();
						$dataa['group_id']=$groupids;
						$dataa['user_id']=$user_id;
						SubAdminAssignedGroup::create($dataa);
			      
					}	
				}
			}
		}
		return $group_id;
	}
	
	public static function update_supervisior($supervisior,$compid,$edituserid)
	{
		
		$supervisor_id =isset($supervisior['user_id'])?$supervisior['user_id']:$supervisior;
		$supervisor_data=UserSupervisor::where('user_id','=',$edituserid)->first();
		if(!$supervisor_data)
		{
				$start_time = date('Y-m-d -h-i-s');
				$end_time = date('Y-m-d -h-i-s');
				$datas=array();
				$datas['supervisor_id']=$supervisor_id;
				$datas['user_id']=$user_id;
				$datas['company_id']=$compid;
				$datas['start_time']=$start_time;
				$datas['end_time']=$end_time;
				UserSupervisor::create($datas);
		}else{
			$supervisor_data=UserSupervisor::where('user_id','=',$edituserid)->where('supervisor_id','=',$supervisor_id)->first();
			if(!$supervisor_data){
				 $datas=array();
				 $datas['supervisor_id']=$supervisor_id;
				 $datas['is_active']    =	1;
			     UserSupervisor::where('user_id','=',$edituserid)->update($datas);
			}
			else
			{
				 $datas=array();
				 $datas['is_active']    =	1;
			     UserSupervisor::where('user_id','=',$edituserid)->update($datas);
				
			}
		}
	}
	
	public static function fetchinstructordata($compid, $userid)
	{
        $result=InstructorMaster::where('company_id','=',$compid)->where('user_id','=',$userid)->first();
		return $result;
	}
	
	public static function updatebandlevel($band_name, $compid, $edituserid)
	{   
	    $datainactive=array('is_active'=>0);
	    UserBandLevel::where('user_id','=',$edituserid)->update($datainactive);
	    if($band_name)
		{	
	        $band_value=isset($band_name['band_id'])?$band_name['band_id']:$band_name;
				if(!is_numeric($band_value)){
					$vSomeSpecialChars=self::$vSomeSpecialChars;
					$vReplacementChars=self::$vReplacementChars;
					$band_value = trim(str_replace($vSomeSpecialChars, $vReplacementChars, $band_value));
					$band_value = htmlentities($band_value);
				}
				
			    $band_name=addslashes($band_value);
			$bandids =0;
			$result =BandLevelMaster::where('band_id','=',$band_name)->where('company_id','=',$compid)->first();
			$data 	=BandLevelMaster::where('band_name','=',$band_name)->where('company_id','=',$compid)->first();
			if($result){
				$bandids = $result->band_id;	
			}elseif($data){
				$bandids = $data->band_id;
			}else{ 		
				$dataa=array();
				$dataa['band_name']=trim($band_name);
				$dataa['company_id']=$compid;
				$dataa['created_by']=Auth::user()->user_id;
				$band=BandLevelMaster::create($dataa);
				$bandids =$band->band_id;
			}
			$banddata = UserBandLevel::where('band_id','=',$bandids)->where('user_id','=',$edituserid)->where('company_id','=',$compid)->first();
			if($banddata)
			{
				$dataactive = array('is_active'=>1);
				UserBandLevel::where('band_id','=',$bandids)->where('user_id','=',$edituserid)->where('company_id','=',$compid)->update($dataactive);
			}
			else
			{
			   if($bandids!=0)
				{					
					$datas=array();
					$datas['band_id']=trim($bandids);
					$datas['user_id']=$edituserid;
					$datas['company_id']=$compid;
					$dataa['created_by']=Auth::user()->user_id;
					UserBandLevel::create($datas);				
				}
			}
		}

	}
}
