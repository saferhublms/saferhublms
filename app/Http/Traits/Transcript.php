<?php
namespace App\Http\Traits;
use App\Http\Traits\Assessmentcrypt;
use App\Http\Traits\Requirement;
use Mail,DB;
use CommonHelper;
use App\Models\Course;

trait Transcript{

    /*
	*	Function Name : getAllTranscript($userid, $compid, $startDate, $endDate, $trainingType)
	*	called from TranscriptController -> transcript
	*	Created By : Siddharth   
	*   Created Date: 27 DEC, 2017
	*   return $usertranscriptArray;
	*/
    public static function getAllTranscript($userid, $compid, $startDate, $endDate, $trainingType,$start,$length,$search,$draw,$order,$orderColumn,$orderType,$searchtext)
	{
		 $transcriptdata =  Transcript::getAllTranscriptData($userid, $compid, $startDate, $endDate, $trainingType,$start,$length,$search,$draw,$order,$orderColumn,$orderType,$searchtext);
		
		 $Getdate_formatecomapny = CommonHelper::getdate($compid);
		
		$usertranscriptArray = array();
		
		if(count($transcriptdata[0]) > 0)
		{
			foreach($transcriptdata[0] as $usertranscript)
			{
			
				$training_type_id   = $usertranscript->training_type_id;
				$course_id			= $usertranscript->course_id;
				$is_internal		= $usertranscript->is_internal;
				$class_id 			= $usertranscript->class_id;
				$transcript_id		= $usertranscript->transcript_id;
				
			
				if($training_type_id==5)
				{ 
				  
							$assessmentInfoArr = array();
							$assessId = $course_id;
							//$assment = Transcript::getassmentData($userid, $compid, $assessId, $transcript_id);
							/* if(count($assment) > 0)
							{ */
								/* foreach($assment as $assmentData)
								{ */
									$assessmentInfoArr['item_name'] 	            = $usertranscript->training_title;;
									$assessmentInfoArr['training_program_code'] 	= $usertranscript->training_program_code;;
									$assessmentInfoArr['transcript_id'] 			= $usertranscript->transcript_id;
									$assessmentInfoArr['training_type'] 			= 'Assessment';
						
									$assessmentInfoArr['training_type_id'] 			= 5;
									$assessmentInfoArr['certificate_template_id'] 	= $usertranscript->certificate_template_id;
									if($usertranscript->score_type_id == 5){
										$assessmentInfoArr['score'] = $usertranscript->score;
										$assessmentInfoArr['status'] = $usertranscript->status;
									 
										
									}else{
										$assessmentInfoArr['score'] = '';
										$assessmentInfoArr['status'] = 'Complete';
									}
									
									$assessmentInfoArr['credit_value'] 			    = $usertranscript->credit_value;
									$assessmentInfoArr['credit_from'] 			    = $usertranscript->credit_from;
						
									$assessmentInfoArr['ccstatus'] 				    = $usertranscript->ccstatus;
									$assessmentInfoArr['course_id'] 				= $course_id;
									$assessmentInfoArr['view_mode'] 				= $usertranscript->view_mode;
									$assessmentInfoArr['user_assess_resp_id'] 		= $usertranscript->user_assess_resp_id;
							
									//$assessmentInfoArr['completion_date'] 		= date($Getdate_formatecomapny.' H:i',strtotime($usertranscript->created_date));
									$assessmentInfoArr['completion_date'] 		= $usertranscript->created_date;
									$assessmentInfoArr['dateformat'] 		= $Getdate_formatecomapny;
						
									$usertranscriptArray[] = $assessmentInfoArr;
								/* } */
							/* }	 */
 
				}
				else if($training_type_id ==2)
				{
					    $itemArr = array();
						$itemArr['item_name'] 			= $usertranscript->training_title;
						$itemArr['training_program_code'] = $usertranscript->training_program_code;
						$itemArr['transcript_id'] 		= $usertranscript->transcript_id;
						$itemArr['credit_value'] 		= $usertranscript->credit_value;
						$itemArr['credit_from'] 		= $usertranscript->credit_from;
						$itemArr['score'] 				= '';
						$itemArr['status'] 				= 'Complete';
						
						$itemArr['training_type'] 		= ($usertranscript->is_internal==2)?'Classroom Training':$usertranscript->training_type;
						$itemArr['training_type_id'] 	= $usertranscript->training_type_id;
			
						$itemArr['certificate_template_id'] = $usertranscript->certificate_template_id;
						$itemArr['completion_date'] 	= date($Getdate_formatecomapny,strtotime($usertranscript->completion_date));
					
						$itemArr['class_id'] 			= $class_id;
						$itemArr['ccstatus'] 			= $usertranscript->ccstatus;
					
						$itemArr['course_id'] 			= $course_id;
						$itemArr['view_mode'] 			= '';
						$itemArr['user_assess_resp_id'] = '';
				
						$usertranscriptArray[] = $itemArr;
					
					
				}
				else if($course_id != '')
				{
					
						$itemArr = array();
						$itemArr['item_name'] 			= $usertranscript->training_title;
						$itemArr['transcript_id'] 		= $usertranscript->transcript_id;
						$itemArr['training_program_code'] = $usertranscript->training_program_code;
						$itemArr['credit_value'] 		= $usertranscript->credit_value;
						$itemArr['credit_from'] 		= $usertranscript->credit_from;
						$scoredata = Transcript::geteLearningScore($userid, $compid,$course_id,$itemArr['transcript_id']);

						if(count($scoredata))
						{
						if($scoredata[0]->sco_score_status == '2'){
							$itemArr['score'] = '';
							$passfailstatus = 'Complete';
						}else{
							$itemArr['score'] 				= (isset($scoredata[0]->correct_answer) && isset($scoredata[0]->total_questions) && $scoredata[0]->total_questions!=0 && $scoredata[0]->correct_answer != 0) ? $scoredata[0]->correct_answer.'/'.$scoredata[0]->total_questions : '';
							$passfailstatus 				= ($scoredata[0]->total_questions==0 || $scoredata[0]->correct_answer == 0) ? 'Complete' : (($scoredata[0]->sco_score_status == 1) ? 'Pass' : 'Fail') ;
						}
						}else{
						$itemArr['score'] 					= '';
						$passfailstatus 					= 'Complete';
						}
						$itemArr['status'] 					= $passfailstatus ;
						if($passfailstatus != 'Fail'){
						$itemArr['certificate_template_id'] = $usertranscript->certificate_template_id;
						}else{
						$itemArr['certificate_template_id'] = 0;
						}
						$itemArr['status'] 				= 'Complete';
						$itemArr['training_type'] 		= ($usertranscript->is_internal==2)?'Classroom Training':$usertranscript->training_type;
						$itemArr['training_type_id'] 	= $usertranscript->training_type_id;
						$itemArr['completion_date'] 	= date($Getdate_formatecomapny,strtotime($usertranscript->completion_date));
						$itemArr['class_id'] 			= '';
						$itemArr['ccstatus'] 			= $usertranscript->ccstatus;
						$itemArr['course_id'] 			= $course_id;
						$itemArr['view_mode'] 			= '';
						$itemArr['user_assess_resp_id'] = '';
						$usertranscriptArray[] = $itemArr;
				
				}
			}
		}
		return array($usertranscriptArray,$transcriptdata[1]);
	}

	public static function getBlendedCompletion($blended_program_id, $userid,$compid) {
	$blendedTranscriptDataArr = array ();
    $blend_arr=array();
	$blendedprogsql = "select bpi.title as blended_prog_name, tp.training_program_id, bpi.blended_item_id
	from blended_program_items bpi
	join training_program tp on tp.blended_program_id = bpi.blended_item_id
	where bpi.blended_item_id=:blended_program_id and tp.company_id = :compid1 and bpi.company_id = :compid2 and bpi.is_active= 1";
	
	$blend_arr['blended_program_id']    =$blended_program_id;
	$blend_arr['compid1']				=$compid;
	$blend_arr['compid2']				=$compid;
	
	$blendedprogresult=DB::select(DB::raw($blendedprogsql) ,$blend_arr); 

	
	foreach ($blendedprogresult as $resblendedProg ) {
		$blendedProgName 		=	$resblendedProg->blended_prog_name;
		$tariningProgId			=	$resblendedProg->training_program_id;
		$blendedProgId			=	$resblendedProg->blended_item_id;
		$total_items			= 	1;
		$total_credit 			= 	0;
		$total_cost				= 	0;
		$item_completed			= 	0;
		$date_completion		= 	'';
		$itemIdStr				= 	'';
		
		$blendedItemsSql = "select bti.item_id, bti.training_type_id, ut.credit_value, ut.credit_from, im.item_cost, ut.transcript_id, ut.completion_date from blendedprogram_training_items bti
		join item_master im on im.item_id = bti.item_id
		left join user_transcript ut on ut.item_id = im.item_id and ut.is_active = 1 and ut.user_id = :userid
		where bti.blendedprogram_id=:blended_program_id and bti.is_active = 1 and bti.is_delete = 1 and im.company_id = :compid1 and ut.company_id = :compid2 and im.is_active = 1 ";
		
		$blend_arr['userid']    		    =$userid;
		$blend_arr['blended_program_id']    =$blended_program_id;
		$blend_arr['compid1']				=$compid;
		$blend_arr['compid2']				=$compid;
		$blendedItemsResult=DB::select(DB::raw($blendedItemsSql) ,$blend_arr); 
		
		if(count($blendedItemsResult)){
			$total_items			= count($blendedItemsResult);
			foreach ($blendedItemsResult as $resblendedItems ) {
				$itemId					= 	$resblendedItems->item_id;
				$training_type_id 		=	$resblendedItems->training_type_id;
				$credit_value	 		=	$resblendedItems->credit_value;
				$credit_from	 		=	$resblendedItems->credit_from;
				$item_cost		 		=	$resblendedItems->item_cost;
				$transcript_id			=	$resblendedItems->transcript_id;
				$completion_date		= 	$resblendedItems->completion_date;
				$total_credit			= 	$total_credit + $credit_value;
				$total_cost				=	$total_cost	+ $item_cost;
				if($transcript_id > 0)
					$item_completed 	= $item_completed + 1;
					$itemIdStr .= $resblendedItems->item_id . ',';
			}
		}

		if($total_items == $item_completed){
			$itemIdStr 				= rtrim ( $itemIdStr, ',' );
			
			$date_completion 		= Transcript::blendedCompletion ($itemIdStr, $userid,$compid );
			$blendedTranscriptDataArr ['completion_date'] 		= $date_completion;
			$blendedTranscriptDataArr ['credit_value'] 			= $total_credit;
			$blendedTranscriptDataArr ['credit_from'] 			= $credit_from;
			$blendedTranscriptDataArr ['item_name'] 			= $blendedProgName;
			$blendedTranscriptDataArr ['training_type'] 		= 'Blended Program';
			$blendedTranscriptDataArr ['trainingProgramId'] 	= $tariningProgId;
		}
	}
	return $blendedTranscriptDataArr;
	}

	public static function blendedCompletion($itemIdStr,$userid,$compid)
	{
	$blend_arr=array();
	$sql = "SELECT MAX(completion_date) as completion_date FROM `user_transcript` WHERE `user_id`= :userid AND `item_id` IN ($itemIdStr) and company_id = :compid and is_active = 1";
	$blend_arr['userid']=$userid;
	$blend_arr['compid']=$compid;
	$result=DB::select(DB::raw($sql) ,$blend_arr); 
	$date = explode(' ',$result[0]->completion_date);
	return $date[0];
	
	}
	
	public static function getassmentData($userid, $compid, $assessId, $transcript_id)
	{
	    $item_arr=array();
		 $sql = "SELECT distinct art.score,uap.total_questions,art.`status` ,art.created_date,assmnt.score_type_id,assmnt.pass_percentage 
		FROM user_transcript ut 
		JOIN  user_training_catalog utc on utc.user_training_id= ut.user_training_id and utc.company_id=:compid1
		JOIN assessment assmnt ON (ut.course_id = assmnt.course_id) and assmnt.company_id=:compid2
		join user_assessment_prop as uap  on uap.course_id=assmnt.course_id and uap.user_id=ut.user_id
		left join assessment_response_transcript as art on art.course_id=assmnt.course_id and art.transcript_id = :transcript_id
		WHERE ut.course_id =:registration_id AND ut.status_id = 1 AND assmnt.company_id =:compid4 AND ut.company_id =:compid5 AND ut.user_id = :userid "; 
		$item_arr['registration_id'] =$assessId;
		$item_arr['transcript_id'] =$transcript_id;
		$item_arr['compid1'] =$compid;
		$item_arr['compid2'] =$compid;
		$item_arr['compid4'] =$compid;
		$item_arr['compid5'] =$compid;
		$item_arr['userid'] =$userid;
		
		$result=DB::select(DB::raw($sql) ,$item_arr); 
		
		return $result;
		
	}
	
	public static function getClassroomData($userid, $compid,$item_id, $class_schedule_id, $class_id)
	{
		$item_arr=array();
		$sql = "SELECT im.item_name, im.credit_value, ut.class_id, ut.class_schedule_id, ut.item_id,im.training_type_id,tc.training_type,cm.class_name,
		im.item_type_id as user_program_type_id,cc.status as ccstatus,tp.training_program_id,
		im.certificate_template_id,cm.item_type_id
		FROM user_transcript ut 
		JOIN item_master im ON (im.item_id = ut.item_id) and im.company_id=:compid1
		join class_master cm on cm.item_id = ut.item_id and cm.is_active=1 and cm.company_id=:compid2
		left JOIN training_program tp ON (tp.class_id = ut.class_id and tp.item_id = ut.item_id)  and tp.company_id=:compid3
		JOIN training_type AS tc ON cm.item_type_id = tc.training_type_id 
		LEFT JOIN certificate_content as cc on im.certificate_template_id = cc.id and cc.status = 1 and cc.company_id=:compid4
		where ut.user_id = :userid and ut.is_active = 1 and im.is_active = 1 
		and ut.item_id = :item_id and ut.company_id = :compid5 and ut.class_id = :class_id group by ut.item_id, ut.class_id, ut.user_id";
		$item_arr['compid1'] =$compid;
		$item_arr['compid2'] =$compid;
		$item_arr['compid3'] =$compid;
		$item_arr['compid4'] =$compid;
		$item_arr['compid5'] =$compid;
		$item_arr['class_id'] =$class_id;
		$item_arr['userid'] =$userid;
		$item_arr['item_id'] =$item_id;
		$result=DB::select(DB::raw($sql) ,$item_arr); 
		return $result;
		
	}
	
	public static function geteLearningData($userid, $compid,$item_id){
		
		$item_arr=array();
		$sql = "SELECT im.item_name,im.credit_value, lpm.learning_plan_name,ut.item_id,im.training_type_id,im.item_type_id as user_program_type_id,tc.training_type,cc.status as ccstatus ,
		tp.training_program_id,im.certificate_template_id
		FROM user_transcript ut 
		JOIN item_master im ON (im.item_id = ut.item_id) and im.company_id= :compid1
		left join learning_plan_training_item as imt on imt.training_program_id=im.item_id
		left join learning_plan_master as lpm on lpm.learning_plan_id=imt.learning_plan_id and lpm.company_id=:compid2
		JOIN training_program tp ON (tp.item_id = ut.item_id)  and tp.company_id=:compid3
		JOIN training_type AS tc ON im.training_type_id = tc.training_type_id 
		LEFT JOIN certificate_content as cc on im.certificate_template_id = cc.id  and cc.status = 1 and cc.company_id=:compid4
		where tc.training_type_id = 1 and ut.user_id = :userid and ut.is_active = 1 and im.is_active = 1 
		and ut.item_id = :item_id group by ut.item_id, ut.user_id";
		$item_arr['item_id']=$item_id;
		$item_arr['userid'] =$userid;
		$item_arr['compid1'] =$compid;
		$item_arr['compid2'] =$compid;
		$item_arr['compid3'] =$compid;
		$item_arr['compid4'] =$compid;
		$result=DB::select(DB::raw($sql) ,$item_arr); 
		return $result;
	}
	public static function geteLearningScore($userid, $compid,$item_id,$transcript_id=''){
		$item_arr=array();
		$transcript = ($transcript_id!='') ? "and ut.transcript_id = :transcript_id" : "";
		 $sql = "SELECT ssd.user_id, ssd.item_id, ssd.max_score, ssd.total_questions AS total_questions, ssd.correct_answer AS correct_answer, ssd.status AS sco_score_status
		FROM sco_score_data ssd
		join user_transcript ut on ut.user_training_id = ssd.user_training_id and ut.company_id=:compid
		WHERE ssd.user_id = :userid AND ssd.item_id = :item_id $transcript";
		$item_arr['item_id']=$item_id;
		$item_arr['userid']=$userid;
		$item_arr['compid']=$compid;
		if($transcript_id!=''){
		$item_arr['transcript_id']=$transcript_id;
		}
		$result=DB::select(DB::raw($sql) ,$item_arr); 
		return $result;
		
	}
    public static function getAllTranscriptData($userid, $compid, $startDate, $endDate, $trainingType,$start,$length,$search,$draw,$order,$orderColumn,$orderType,$searchtext)
	{
	        $column='';
		   if($orderColumn==0){
				$column='course_details.course_name';
			}
			elseif($orderColumn==0){
				$column='course_details.course_name';//work here again
			}elseif($orderColumn==1){
				$column='user_transcript.credit_value';
			}elseif($orderColumn==3){
				$column='user_transcript.completion_date';
			}elseif($orderColumn==4){
				$column='training_type.training_type_id';
			}
			
		    $lang_id=1;
		    $querydata=Course::join('training_programs',function($join) use($compid){
				$join->on('training_programs.training_program_code','=','courses.training_program_code')
				 ->where('training_programs.company_id','=',$compid)
				 ->where('training_programs.status_id','=',1);
			})
			->join('user_transcript',function($join) use($compid,$userid){
				$join->on('user_transcript.course_id','=','courses.course_id')
				 ->where('user_transcript.company_id','=',$compid)
				 ->where('user_transcript.user_id','=',$userid)
				 ->where('user_transcript.status_id','=',1);
			})
			->join('training_type','training_type.training_type_id','=','training_programs.training_type')
			->leftjoin('course_categories',function($join) use($compid){
				$join->on('course_categories.course_id','=','courses.course_id')
				->where('course_categories.is_active','=',1)
				->where('course_categories.company_id','=',$compid);
			})
			->leftjoin('training_category',function($join) use($compid){
				$join->on('training_category.training_category_id','=','course_categories.training_category_id')
				->where('training_category.is_active','=',1)
				->where('training_category.is_delete','=',0)
				->where('training_category.company_id','=',$compid);
			})
			->join('course_details',function($join) use($compid,$lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.company_id','=',$compid)
				->where('course_details.lang_id','=',$lang_id);
			})
			->leftjoin('certificate_content',function($join) use($compid,$lang_id){
				$join->on('certificate_content.id','=','courses.certificate_template_id')
				->where('certificate_content.company_id','=',$compid)
				->where('certificate_content.status','=',1);
			})
			->leftjoin('assessment_response_transcript as art',function($join) use($compid,$lang_id){
				$join->on('art.course_id','=','courses.course_id')
				->on('art.transcript_id','=','user_transcript.transcript_id');
			})
			->leftjoin('assessment as assess',function($join) use($compid,$lang_id){
				$join->on('assess.course_id','=','courses.course_id');
			})
			->where('courses.status_id','=','1')->where('courses.company_id','=',$compid)
			->select(DB::raw('training_programs.training_type as training_type_id,course_details.course_name,course_details.course_title,concat(course_details.course_name,".",course_details.course_title) as training_title,courses.course_id,training_programs.training_program_code,courses.media_library_id,training_type.training_type,if(training_programs.training_type=1,"ON Demand",CURDATE()) as recursive_date,user_transcript.completion_date,user_transcript.transcript_id,user_transcript.credit_value,courses.certificate_template_id,user_transcript.class_id,user_transcript.credit_from,training_programs.is_internal,certificate_content.status as ccstatus,art.score,art.`status` ,art.created_date,assess.score_type_id,assess.view_mode,art.user_assess_resp_id'));
			
			if($startDate != '' && $endDate != ''){
				$querydata=$querydata->where('user_transcript.completion_date','>=',$startDate)->where('user_transcript.completion_date','<=',$startDate);
			
		    }else if($startDate != '' && $endDate == ''){
				$querydata=$querydata->where('user_transcript.completion_date','>=',$startDate);
				
		    }else if($startDate == '' && $endDate != ''){
				$querydata=$querydata->where('user_transcript.completion_date','<=',$endDate);
		    }

			
			//for simple filter
			 if($trainingType){
				$querydata=$querydata->where('training_type.training_type_id','=',$trainingType);
			} 
			
			
			//for grid filter
			if($searchtext){
				$querydata=$querydata->where(function($query) use($searchtext){
				$query->where('course_details.course_name','like','%'.$searchtext.'%')
				->orWhere('courses.credit_value','like','%'.$searchtext.'%')
				->orWhere('training_type.training_type','like','%'.$searchtext.'%');
				});
			}
			//group by ut.item_id,ut.registration_id,ut.user_id,ut.completion_date
			$totalcount=$querydata->groupby('user_transcript.completion_date','user_transcript.user_id','user_transcript.course_id','art.created_date')->get();
			if($column){
				$allrecords=$querydata->orderBy($column,$orderType)->groupby('user_transcript.completion_date','user_transcript.user_id','user_transcript.course_id','art.created_date')->skip($start)->take($length)->get();
			
			}else{
				$allrecords=$querydata->groupby('user_transcript.completion_date','user_transcript.user_id','user_transcript.course_id','art.created_date')->skip($start)->take($length)->get();
			}
			
			return array($allrecords,count($totalcount));
			
	
	}       
	
	public static function gettranscriptHistory($item_id,$userid,$compid)
	{
		$tr_arr=array();
		$str='';
		if($item_id != '0'){
			$tr_arr['item_id']	=$item_id;
			$str = " and utrans.course_id=:item_id ";
        }
		$sql1 = "SELECT utrans.course_id,utc.transcript_id,DATE_FORMAT(utc.completion_date,'%m-%d-%Y') as completion_date,
		tc.training_type_id,utc.credit_value,concat(cd.course_name,'',cd.course_title) as item_name,tc.training_type AS itemtype
		FROM user_training_catalog_history AS utrans
		JOIN user_transcript_history AS utc ON utc.user_id =utrans.user_id  and utc.course_id=utrans.course_id 
		JOIN courses AS im ON im.course_id = utrans.course_id AND im.company_id=:compid1 
		join training_programs as tp on tp.training_program_code=im.training_program_code and tp.status_id=1
		join  course_details as cd on cd.course_id=im.course_id and cd.lang_id=1
		JOIN training_type AS tc ON tp.training_type = tc.training_type_id 
		where utrans.company_id =:compid2 AND utrans.user_id=:userid  AND utrans.status_id=1 $str group by utc.course_id,utc.user_id,utc.completion_date ";
		$tr_arr['userid']	=$userid;
		$tr_arr['compid1']	=$compid;
		$tr_arr['compid2']	=$compid;
		$result=DB::select(DB::raw("$sql1") ,$tr_arr); 
		return $result;
	}
	  
}