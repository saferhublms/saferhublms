<?php
namespace App\Http\Traits;

use App\Models\ClassSchedule;
use App\Models\UsertrainingCatalog;
use App\Models\UserTrainingHistory;
use App\Models\UserTrainingCatalogHistory;
use App\Models\User;
use App\Models\BlendedItemCompletion;
use App\Models\TrainingCatalogTranscript;
use App\Models\UserTranscriptHistory;
use App\Models\UserTranscript;
use App\Models\UserRatingMaster;
use App\Models\ItemMaster;
use App\Models\BlendedprogramTrainingItem;
use App\Models\BlendedUserComplation;
use App\Models\UserMaster;
use App\Models\ClassMaster;
use App\Models\TrainingProgram;
use App\Models\UserAssessment;
use App\Models\UserassessmentProp;
use App\Models\AssessmentResponseTranscript;
use App\Http\Traits\Trainingprograms;
use App\Http\Traits\Blendedcommonclass;
use App\Http\Traits\Assessments;
use Mail,DB,CommonFunctions;

trait Coursecredits{
	 
	  	public static function checkClassExistance($ClassId , $compid)
		{
			$classScheduleId = 0;
		    $results = array();
			$results=ClassSchedule::whereclassId($ClassId)->wherecompanyId($compid)->get();
			if(count($results) > 0)
			{
				$classScheduleId = $results[0]->class_schedule_id;
			}
			return $classScheduleId ;
		}
		
		
		public static function getUserInfo($compid,$userid)
		{
			$result = UserMaster::wherecompanyId($compid)->whereuserId($userid)->get();
			return $result;
	    }
		
		public static function getenrolled($compid, $itemid, $classid) 
		{
			$itemresult =UsertrainingCatalog::join('item_master as im','im.item_id','=','user_training_catalog.item_id')
			                   ->join('class_master as cm','cm.item_id','=','im.item_id')
			                   ->join('user_master as um',function($join){
								                 $join->on('um.user_id','=','user_training_catalog.user_id')
												 ->where('um.is_approved',1)
												 ->where('um.is_delete',0)
												 ->where('um.is_active',1);
							   })
							   ->where('im.company_id',$compid)
							   ->where('im.item_id',$itemid)
							   ->where('im.is_active',1)
							  // ->where('cm.class_id','=','user_training_catalog.session_id')
							   ->where('user_training_catalog.session_id',$classid)
							   ->whereIn('user_training_catalog.training_status_id',[1,3,10])
							   ->whereIn('user_training_catalog.user_program_type_id',[1,2])
							   ->where('cm.is_active',1)
							   ->groupBy('user_training_catalog.user_id')
							   ->select('user_training_catalog.user_id','user_training_catalog.is_enroll','user_training_catalog.is_approved','user_training_catalog.user_training_id','user_training_catalog.training_status_id')
							   ->distinct()
							   ->get();
							   
		$enrow = 0;
		$approw = 0;
		$userids = array();
		$mainresult = json_decode(json_encode($itemresult),true);
		if (count($mainresult) > 0) {
			
			foreach ($mainresult as $value) {
				$userids[] = $value['user_id'];
				if (in_array($value['training_status_id'],array(1,3)))
					$enrow++;

				if ($value['is_approved'] == 1)
					$approw++;
			}
		}
		return array('totalenroll' => $enrow, 'totalapp' => $approw, 'userids' => $userids);
		}
		
		public static function getclassroomdata($compid,$subAdminId,$roleid) 
		{
			$array=array();
			$sel_arr = array();
			$itemsql ="SELECT cm.class_name as class_name, cm.start_date_class, cm.endate_date, cm.maximum_seat as max_seat, cm.current_enroll,
			cm.virtual_session_information, cm.is_email_checked, cm.is_automatic_require_post_class_evaluation, 
			cm.is_require_response_for_credit, cm.evaluation_pref_send_same_day,  
			cm.class_id, cm.item_type_id as itemtype, im.item_id, im.item_name, im.item_type_id, im.training_type_id, 
			im.delivery_type_id, im.description, im.prerequeisites, im.credit_value, im.item_cost, im.item_image,im.company_id,
			im.published_date,ss.dayid as recursive_date, ss.time_zone, ss.start_time_hr, ss.start_time_min, 
			ss.ses_length_hr, ss.ses_length_min, ss.timeformat, ss.instructor_id, ss.location,im.require_supervisor_aproval_status, ss.instructor_name
			from item_master im 
			join class_master cm on cm.item_id = im.item_id  and cm.status_id=1 and cm.is_active = 1
			join item_master_trainingcategory imtc on im.item_id=imtc.item_id and imtc.is_active=1
			join ( SELECT min(ssd.dayid) as dayid, ssd.time_zone, ssd.start_time_hr, ssd.start_time_min, ssd.ses_length_hr, ssd.ses_length_min, 
			ssd.timeformat, ssd.instructor_id, if(ssd.location!='0',ssd.location,'') as location, ssd.class_id, um.user_name as instructor_name from session_scedule ssd  
			left join user_master um on um.user_id = ssd.instructor_id
			where ssd.dayid >='".date('Y-m-d')."' and ssd.company_id = :compId1 group by ssd.class_id) ss ON cm.class_id = ss.class_id ";
			$sel_arr['compId1'] = $compid;
			if($roleid==5 || $roleid==3){
				$itemsql.= " JOIN sub_admin_assigned_category saac ON saac.category_id = imtc.training_category_id and saac.is_active=1 WHERE saac.user_id =:subAdminId ";
				$sel_arr['subAdminId'] = $subAdminId;				
				$whr = " AND ";
			}else{
				$whr = " where ";
			}
			$itemsql.="$whr im.company_id = :compId2 and im.training_type_id in (2,3) and im.item_type_id != 4 and  im.is_active=1 and im.training_type_id in (2,3) GROUP BY cm.class_id";		
			$sel_arr['compId2'] = $compid;
			$res= DB::select(DB::raw($itemsql), $sel_arr);
			$itemresult = json_decode(json_encode($res),true);
			return $itemresult;
			
		}
		
		
		public static function getAllTrainingProgramswithClassessofcourse($compid,$subAdminId,$roleid,$mode=1,$callfrom='') 
		{
			$itemresult=array();
			$itemresult1 = array();
			$elearningTrainingPrograms = array();
			$classroomTrainingPrograms = array();
			$assessmentTrainingPrograms = array();
			if($mode==1)
			{
				$elearningTrainingPrograms = Coursecredits::geteleanringTrainigPrograms($compid,$subAdminId,$roleid,$callfrom);
			}
			else if($mode==2)
			{
				$classroomTrainingPrograms = Coursecredits::getClassroomAll($compid,$subAdminId,$roleid,$callfrom);
			}
			else if($mode==3)
			{
				$assessmentTrainingPrograms = Coursecredits::getAssessmentAll($compid,$subAdminId,$roleid,$callfrom) ;
				foreach($assessmentTrainingPrograms as $key=>$value){
					$assessmentTrainingPrograms[$key]->training_type_id 		= 	6;
					$assessmentTrainingPrograms[$key]->item_type_id		= 	6;
					$assessmentTrainingPrograms[$key]->itemtype 			= 	'Assessment';
				}	
			}
				
			$itemresult1=array_merge($elearningTrainingPrograms,$classroomTrainingPrograms);
			$itemresult = array_merge($itemresult1 , $assessmentTrainingPrograms) ;
			return $itemresult;
		}
		
		public static function geteleanringTrainigPrograms($compid,$subAdminId,$roleid,$callfrom)
		{
		$itemresult=array();
		if($callfrom == 'supervisortool')
		{
			if($roleid==2){
			
				$userrolearray =Trainingprograms::getSupervisorSupervisorId($compid,$subAdminId);	
				$subAdminId = $userrolearray[0];
				$roleid     = $userrolearray[1];
			}
			
		
			$compSettings = CommonFunctions::getCompanySettings($compid);
			
			if(($compSettings->supervisor_training_view== 1) && ($roleid==2)) { 
				$roleid	= 1;
			}		
		}
			
		$itemsql = "Select im.item_id ,im.item_name, im.item_name as training_title, im.training_type_id , im.item_type_id, tt.training_type as itemtype from item_master as im
					join training_type as tt on tt.training_type_id = im.training_type_id";
		if($roleid==5 || $roleid==3){
			$itemsql.= " join item_master_trainingcategory imtc on im.item_id=imtc.item_id
			JOIN training_category tct ON imtc.training_category_id = tct.`training_category_id`
			JOIN sub_admin_assigned_category saac ON saac.`category_id` = imtc.training_category_id
			WHERE saac.`user_id` ='$subAdminId' ";	
			$whr = " AND ";
		}else{
			$whr = " where ";
		}
		$itemsql.= "$whr im.training_type_id=1 and im.company_id = '$compid'  and im.is_active=1 and im.status_id in (1,2) order by im.item_name asc"; 		
		 
			 $itemresult =DB::select($itemsql);
		     return $itemresult;
		}
		
		public static function getClassroomAll($compid,$userid,$roleid,$callfrom)
		{
			
			$itemidarr = array();
			
			if($callfrom == 'supervisortool')
			{
				if($roleid==2){
					
					$userrolearray =Trainingprograms::getSupervisorSupervisorId($compid,$userid);	
					$userid     = $userrolearray[0];
					$roleid     = $userrolearray[1];
				}
				
				
				$compSettings = CommonFunctions::getCompanySettings($compid);
				
				if(($compSettings->supervisor_training_view == 1) && ($roleid==2)) { 
					$roleid	= 1;
				}		
			}
			
			if($roleid == 5 || $roleid == 3)
				$itemsql = "select distinct im.*,im.item_name as training_title,group_concat(tc.category_name) as category_name,tt.training_type as itemtype,cm.class_id , cm.status_id from item_master as im  
				join training_type as tt on  im.training_type_id=tt.training_type_id 
				left join class_master as cm on  cm.item_id=im.item_id  and cm.is_active=1
				left join item_master_trainingcategory imtc on im.item_id = imtc.item_id
				JOIN training_category tc ON (imtc.training_category_id = tc.training_category_id) and tc.is_active=1 and tc.is_delete=0
				JOIN sub_admin_assigned_category saac ON saac.`category_id` = imtc.`training_category_id`
				WHERE saac.`user_id` ='$userid' and im.company_id = '$compid' and im.is_active = '1' and im.item_type_id!=4 and (im.status_id IS NULL OR im.status_id='') and im.training_type_id IN (2,3) group by im.item_id order by cm.class_id";
			else
			   $itemsql = "select distinct im.*,im.item_name as training_title,tt.training_type as itemtype,cm.class_id ,group_concat(tc.category_name) as category_name , cm.status_id from item_master as im 
				join item_master_trainingcategory as imt on imt.item_id=im.item_id
				join training_type as tt on  im.training_type_id=tt.training_type_id 
				left join class_master as cm on  cm.item_id=im.item_id  and cm.is_active=1 and cm.item_type_id != 4
				join training_category as tc on tc.training_category_id=imt.training_category_id and tc.is_active=1 and tc.is_delete=0			
				where im.company_id = '$compid' and im.training_type_id in (2,3) and (im.status_id IS NULL OR im.status_id='') and im.item_type_id!=4 and im.is_active = '1' group by im.item_id order by cm.class_id ASC";
			
			
			   $itemresult =DB::select($itemsql);
			
			return $itemresult;	
	}
	
	public static function getAssessmentAll($compid,$userid,$roleid,$callfrom)
	{
		
		$itemidarr = array();
		
		if($callfrom == 'supervisortool')
		{
			if($roleid==2){
				
				$userrolearray =Trainingprograms::getSupervisorSupervisorId($compid,$userid);	
				$userid = $userrolearray[0];
				$roleid     = $userrolearray[1];
			}
			
			$compSettings = CommonFunctions::getCompanySettings($compid);
			
			if(($compSettings->supervisor_training_view == 1) && ($roleid==2)) { 
				$roleid	= 1;
			}		
		}
		
		 if($roleid == 5 || $roleid == 3)
			$itemsql = "select assess.id as assess_id,assess.assessment_name as training_title from assessment as assess
			JOIN assessment_training_category atc ON atc.assessment_id = assess.id AND atc.is_active = 1
			JOIN training_category tc ON (atc.training_category_id = tc.training_category_id) and tc.is_active=1 and tc.is_delete=0
			JOIN sub_admin_assigned_category saac ON saac.`category_id` = atc.`training_category_id`
			WHERE saac.`user_id` ='$userid' and assess.is_delete = 0 and assess.company_id ='".$compid."'";
		else  
		   $itemsql = "select assess.id as assess_id,assess.assessment_name as training_title from assessment as assess
						where assess.is_delete = 0 and assess.company_id ='".$compid."'";
		
		$itemresult =DB::select($itemsql);
			
		return $itemresult;	
	}
	
	public static function FindAllClassessofcourse($itemId , $compId)
	{
        $param=array();
		$sql = "Select class_id,class_name,start_date_class,endate_date,item_type_id from class_master where item_id = :itemId and item_type_id != 4 and company_id = :compId and status_id IN (0,1) and is_active = 1 order by class_id ASC";
		$param['itemId']=$itemId;
		$param['compId']=$compId;
		$result = DB::select(DB::raw($sql),$param);
		return $result ;
	}
	
	public static function giveCreditElearningToSelectedUsers($adminid, $compid, $usersid, $trainingProgId, $credit_from = 0) {
		
		$trainingType = '1';
		$tariningProg = Coursecredits::getTraingProgdetail ( $compid, $trainingProgId );
		foreach ( $usersid as $key1 => $userid ) {
				$tainingData ['item_id'] = $tariningProg[0]->item_id;
				$tainingData ['training_type_id'] = $trainingType;
				$tainingData ['company_id'] = $compid;
				$tainingData ['user_program_type_id'] = '1';
				$tainingData ['learning_plan_id'] = '0';
				$tainingData ['blended_program_id'] = '0';
				$tainingData ['assignment_id'] = '0';
				$tainingData ['is_approved'] = '1';
				$tainingData ['is_enroll'] = '1';
				$tainingData ['session_id'] = 0;
				$tainingData ['program_id']=
				$tainingData ['training_status_id'] = '1';
				$tainingData ['last_modfied_by'] = $adminid;
				$tainingData ['last_modified_time'] = date ( 'Y-m-d H:i:s' );
				$tainingData ['is_active'] = '1';
				$tainingData ['user_id'] = $userid; 
				$tainingData ['class_schedule_id'] = 0; 
				$tainingData ['created_date'] =  date ('Y-m-d H:i:s'); 
				
				Coursecredits::giveCreditToUser ( $tainingData, $tariningProg[0]['credit_value'], '1', $credit_from );
			//}
		}
	}
	
	public static function getTraingProgdetail($compid,$ItemId)
	{
		$itemsql =ItemMaster::where('company_id','=',$compid)->where('item_id','=',$ItemId)->where('is_active','=',1)->get();
		return $itemsql;
	}
	
	public static function giveCreditToUser($tainingData, $credit, $trainingType, $credit_from, $Coursecompletiondate = '') {		
	
		// Check if Training_item is assigned to user in training_catalog
		$trainingCatalog =Coursecredits::checkTrainingCatalogData( $tainingData, $trainingType );
			
		if (count ( $trainingCatalog ) > 0) 
		{
			$training_catalog_id = $trainingCatalog [0]->user_training_id;
			if($trainingCatalog[0]->UTCActiveness == 0)
			{			
		         UsertrainingCatalog::where('user_training_id','=',$trainingCatalog [0]->user_training_id)->update($tainingData);
			
			}
			else if($trainingCatalog[0]->training_status_id != 1)
			{
				$UTCdata  = array();
				$UTCdata['training_status_id'] = 1;
	
				UsertrainingCatalog::where('user_training_id','=',$trainingCatalog [0]->user_training_id)->update($UTCdata);
			}
			else if($trainingCatalog[0]->training_status_id == 1)
			{
				$data= UsertrainingCatalog::where('company_id','=',$tainingData['company_id'])->where('user_id','=',$tainingData['user_id'])->where('item_id','=',$tainingData['item_id'])->where('training_status_id','=',1)->where('user_training_id','=',$trainingCatalog [0]->user_training_id)->first();
			 	
				if(!empty($data)){
					
					UserTrainingCatalogHistory::insert($data->toArray());
						
				}
				$training_catalog_id =  UsertrainingCatalog::insertGetId( $tainingData );				
			}
		}
		else 
		{ 
			$training_catalog_id = UsertrainingCatalog::insertGetId($tainingData);
			
			UserTrainingCatalogHistory::insert($tainingData);
			
		}
	
		// Check if Training_item is assigned to user in transcript
		$completiondate = ($Coursecompletiondate != '') ? ($Coursecompletiondate) : (date ('Y-m-d'));
		$tainingData['credit_from'] = $credit_from ;
		$usertranscript = Coursecredits::checkUserTranscript ( $tainingData, $training_catalog_id, $trainingType ,$completiondate );
		
		if (count ( $usertranscript ) > 0) {		 
			$updatedata = Coursecredits::updateUserTranscript ( $tainingData, $training_catalog_id, $trainingType, $completiondate );
			$usertranscriptID=$usertranscript[0]->transcript_id;
		}else{	
			
			$usertranscriptID  = Coursecredits::insertValueIntoTranscript ( $tainingData, $credit, $training_catalog_id, $trainingType,$completiondate );
				
		}
			
		CommonFunctions::getCourseSkillLevel($tainingData ['item_id'], $tainingData ['company_id'],$tainingData ['user_id'],$trainingType);
		
		$startdate	= date ( 'Y-m-d H:i:s' );
		$enddate  	= date ( 'Y-m-d H:i:s' );
		Coursecredits::insertValueIntoTranscriptHistory ( $tainingData, $training_catalog_id, $trainingType, $startdate, $enddate );
		
		Coursecredits::insertValueIntoTrainingCatalogTranscript ($tainingData, $credit, $training_catalog_id, $trainingType,$completiondate );
		
		
		$allTrainingItems =Blendedcommonclass::getAllTrainingItems($tainingData['blended_program_id'],$tainingData ['company_id']);
		// mark blended program complete when all items are completed
	
		$totalTrainingItems = count($allTrainingItems);			
		$ii = 0;
		$creditArr = array();
		$completionDateArr = array();
		if(count($allTrainingItems)>0){
			foreach ($allTrainingItems as $trainingItems) {
				$item_id = $trainingItems->item_id;
				$assessment_id = $trainingItems->assessment_id;
				if($assessment_id!=0){
					$checkItemCompletion = Blendedcommonclass::checkAssessmentCompletion($tainingData['user_id'],$assessment_id,$tainingData ['company_id']);
				}else{
					$checkItemCompletion = Blendedcommonclass::checkItemCompletion($tainingData['user_id'],$item_id,$tainingData ['company_id']);
				}
				if(count($checkItemCompletion)){
					$creditArr[] = $checkItemCompletion[0]->credit_value;
					$completionDateArr[] = strtotime($checkItemCompletion[0]->completion_date);
					$ii++;
				}
			}			
			if($ii == $totalTrainingItems){
				
				Blendedcommonclass::userBlendedCompletion($tainingData['user_id'],$tainingData['blended_program_id'],$tainingData ['company_id'],$creditArr,$completionDateArr);

			}
		}
		
	}
	
	public static function insertValueIntoTrainingCatalogTranscript($tainingData, $credit, $training_catalog_id, $trainingType,$completiondate)
	{

		$trainingCatalogTranscript ['class_schedule_id'] 		= 0;
		$trainingCatalogTranscript ['class_id'] 				= $tainingData ['session_id'];
		$trainingCatalogTranscript ['registration_id'] 			= $tainingData ['blended_program_id'];
		$trainingCatalogTranscript ['user_id'] 					= $tainingData ['user_id'];
		if ($trainingType != 4)
			$trainingCatalogTranscript ['item_id'] 				= $tainingData ['item_id'];
		$trainingCatalogTranscript ['user_training_id'] 		= $training_catalog_id;
		$trainingCatalogTranscript ['company_id'] 				= $tainingData ['company_id'];
		$trainingCatalogTranscript ['credit_value'] 			= $credit;
		$trainingCatalogTranscript ['completion_date'] 			= $completiondate;
		$trainingCatalogTranscript ['expiration_date'] 			= date ( 'Y-m-d H:i:s' );
		$trainingCatalogTranscript ['times'] 					= '0';
		$trainingCatalogTranscript ['is_approved'] 				= '1';
		$trainingCatalogTranscript ['is_active'] 				= '1';
		TrainingCatalogTranscript::insert($trainingCatalogTranscript);

	}
    	
	
	public static function insertValueIntoTranscriptHistory($tainingData, $training_catalog_id, $trainingType, $startdate, $enddate) {
		$usertraininghistory ['user_id'] 					= $tainingData ['user_id'];
		$usertraininghistory ['user_training_id'] 			= $training_catalog_id;
		$usertraininghistory ['training_start_date_time']	= $startdate;
		$usertraininghistory ['training_end_date_time'] 	= $enddate;
		$usertraininghistory ['php_session_id'] 			= session_id ();
		$usertraininghistory ['course_completion_percentage'] = '100';
		if ($trainingType != 4)
			$usertraininghistory ['item_id'] 				= $tainingData ['item_id'];
		$usertraininghistory ['is_active'] 					= '1';
		
		UserTrainingHistory::insert($usertraininghistory);
	}
	
	public static function insertValueIntoTranscript($tainingData, $credit, $training_catalog_id, $trainingType, $completiondate)
	{
		$transcriptdata ['class_schedule_id'] 		= 0;
		$transcriptdata ['class_id'] 				= $tainingData ['session_id'];
		$transcriptdata ['registration_id'] 		= $tainingData ['blended_program_id'];
		if ($tainingData ['blended_program_id'] > 0) {
			$transcriptdata ['is_blended'] 			= '1';
		} else {
			$transcriptdata ['is_blended'] 			= '0';
		}
		$transcriptdata ['user_id'] 				= $tainingData ['user_id'];
		if ($trainingType != 4)
			$transcriptdata ['item_id'] 			= $tainingData ['item_id'];
		$transcriptdata ['user_training_id'] 		= $training_catalog_id;
		$transcriptdata ['company_id'] 				= $tainingData ['company_id'];
		$transcriptdata ['credit_value'] 			= $credit!=null?$credit:0;
		$transcriptdata ['credit_from'] 			= (!empty($tainingData ['credit_from'])) ? $tainingData ['credit_from'] : 0;
		$transcriptdata ['completion_date'] 		= $completiondate;
		$transcriptdata ['times'] 					= '0';
		$transcriptdata ['is_approved'] 			= '1';
		$transcriptdata ['is_active'] 				= '1';
		$transcriptdata ['is_expired'] 				= '0';
		$transcriptdata ['last_modified_by'] 		= $tainingData ['last_modfied_by'];
		$transcriptId = UserTranscript::insertGetId($transcriptdata);
		$transcriptIdhis = UserTranscriptHistory::insert($transcriptdata);
		
		return $transcriptId;
	}
	
	public static function updateUserTranscript($tainingData, $training_catalog_id, $trainingType, $completiondate){
		
		$class_id=($tainingData['session_id']!=0) ? $tainingData['session_id'] : '';
		Coursecredits::maintainHistory($tainingData['company_id'],$tainingData['item_id'],$tainingData['user_id'],$completiondate,$class_id);
		$class_idstr='';
		if($class_id){
			$class_idstr=" and class_id=$class_id";
		}
		$itemsql = "update user_transcript set is_active = 1, completion_date = '".date ('Y-m-d')."',user_training_id='".$training_catalog_id."' where company_id = ".$tainingData['company_id']." and item_id =  ".$tainingData['item_id']."	$class_idstr and user_id = ".$tainingData['user_id'];
		DB::update($itemsql);
	}
	
	public static function maintainHistory($company_id,$item_id,$user_id,$completiondate,$class_id){

		if(!$class_id){
		$transcriptsdata =UserTranscript::where('company_id','=',$company_id)->where('user_id','=',$user_id)->where('item_id','=',$item_id)->first();
		}else{
		$transcriptsdata =UserTranscript::where('company_id','=',$company_id)->where('user_id','=',$user_id)->where('item_id','=',$item_id)->where('class_id','=',$class_id)->first();
			
		}
		if($transcriptsdata){
             UserTranscriptHistory::insert($transcriptsdata->toArray());			
					
		}
	}
	
	public static function checkTrainingCatalogData($tainingData, $trainingType){
		$itemresult=array();
		$andwhere =($tainingData['session_id']!=0) ? 'and session_id="'.$tainingData['session_id'].'"':'';
		if($trainingType == 1){
			$itemsql = "select user_training_id,is_active as UTCActiveness,training_status_id from user_training_catalog where company_id = ".$tainingData['company_id']."
			and item_id =  ".$tainingData['item_id']." 
			and user_id = ".$tainingData['user_id']."  and blended_program_id = ".$tainingData['blended_program_id'] ;
		$itemresult=DB::select($itemsql);
		}elseif($trainingType == 4){
			$itemsql = "select user_training_id,is_active as UTCActiveness,training_status_id  from user_training_catalog where company_id = ".$tainingData['company_id']."
			and  program_id = ".$tainingData['program_id']."  and user_id = ".$tainingData['user_id']."  
			and blended_program_id = ".$tainingData['blended_program_id'];
			$itemresult=DB::select($itemsql);
			
		}else{
			$itemsql = "select user_training_id,is_active as UTCActiveness, training_status_id from user_training_catalog where company_id = ".$tainingData['company_id']."
			 and user_id = ".$tainingData['user_id']."  $andwhere and item_id =  ".$tainingData['item_id']." 
			 and blended_program_id = ".$tainingData['blended_program_id'];
			$itemresult= DB::select($itemsql);
		}
		return $itemresult;
	}
	
	public static function checkUserTranscript($tainingData,$training_catalog_id, $trainingType , $completion_date){
		$itemresult=array();
		if($trainingType != 4){
			$andsession = "";
			
			if($trainingType == 2 || $trainingType == 3)
			{
				$andsession = " and class_id = '".$tainingData['session_id']."'" ;
			}			
			$itemsql = "select transcript_id,is_active as TransActivness,completion_date from user_transcript where company_id = ".$tainingData['company_id']." and item_id =  ".$tainingData['item_id']."	and user_id = ".$tainingData['user_id'].$andsession;
			$itemresult=DB::select($itemsql);
			
		}else{
			$itemsql = "select transcript_id from user_transcript where company_id = ".$tainingData['company_id']." and  registration_id = ".$tainingData['blended_program_id']."  and user_id = ".$tainingData['user_id'];
			$itemresult=DB::select($itemsql);

			
		}
		return $itemresult;
	}
	
	public static function getTraingProgramdetail($compid, $ItemId, $ClassId)
	{
		$results = array();
		$itemsql ="select im.*,cm.class_id,cm.endate_date from item_master as im
					join class_master as cm on cm.item_id=im.item_id and cm.is_active=1 and cm.item_type_id != 4					
					where im.company_id='$compid' and im.item_id='$ItemId' and cm.class_id =$ClassId  and im.is_active=1";
		$results=DB::select($itemsql);
		return $results ;
	}
	
	public static function giveCreditClassroomToSelectedUsers($adminid, $compid, $usersid, $trainingProgId, $class_id ,$credit_from = 0) {
	
		$trainingType = '2';
		if($class_id != 0)
		{
			$tariningProg = Coursecredits::getTraingProgramdetail ( $compid, $trainingProgId , $class_id);
		}
		else
		{
			$tariningProg = Coursecredits::getTraingProgramwithsubmittraining ($compid, $trainingProgId, $adminid);
		}
		foreach ( $usersid as $key1 => $userid ) {
				$tainingData ['item_id'] = $tariningProg [0]->item_id;
				$tainingData ['training_type_id'] = $trainingType;
				$tainingData ['company_id'] = $compid;
				$tainingData ['user_program_type_id'] = '1';
				$tainingData ['learning_plan_id'] = '0';
				$tainingData ['blended_program_id'] = '0';
				$tainingData ['assignment_id'] = '0';
				$tainingData ['is_approved'] = '1';
				$tainingData ['is_enroll'] = '1';
				$tainingData ['session_id'] = $tariningProg [0]-> class_id;
				$tainingData ['training_status_id'] = '1';
				$tainingData ['last_modfied_by'] = $adminid;
				$tainingData ['last_modified_time'] = date ( 'Y-m-d H:i:s' );
				$tainingData ['created_date'] = date ( 'Y-m-d H:i:s' );
				$tainingData ['is_active'] = '1';
				$tainingData ['user_id'] = $userid;
				
				Coursecredits::giveCreditToUser ( $tainingData, $tariningProg[0]->credit_value, $tariningProg[0]->training_type_id,$credit_from, $tariningProg[0]->endate_date);
		}
	}
	
	public static function giveCreditBlendedProgToSelectedUsers($adminid, $compid, $usersid, $trainingProgId, $credit_from = 0) {
	
		$trainingType = '4';
		$tariningProg = Coursecredits::getTraingProg ( $compid, $trainingProgId, $trainingType );
		foreach ( $usersid as $key1 => $userid ) {
			for($x = 0; $x < count ( $tariningProg ); $x ++) {
				$tainingData = array ();
				$tainingData ['program_id'] = $tariningProg [$x]-> training_program_id;
				$tainingData ['training_type_id'] = $trainingType;
				$tainingData ['company_id'] = $compid;
				$tainingData ['user_program_type_id'] = '1';
				$tainingData ['learning_plan_id'] = '0';
				$tainingData ['blended_program_id'] = $tariningProg [$x]->blended_program_id;
				;
				$tainingData ['item_id'] = $tariningProg [$x]->blended_program_id;
				;
				$tainingData ['assignment_id'] = '0';
				$tainingData ['is_approved'] = '1';
				$tainingData ['is_enroll'] = '1';
				$tainingData ['session_id'] = '0';
				$tainingData ['training_status_id'] = '1';
				$tainingData ['last_modfied_by'] = $adminid;
				$tainingData ['last_modified_time'] = date ( 'Y-m-d H:i:s' );
				$tainingData ['is_active'] = '1';
				$tainingData ['user_id'] = $userid;
				Coursecredits::giveCreditToUser ( $tainingData, '0', '4' );
			}
		}
	}
	public static function getTraingProg($compid,$trainingProgId,$trainingType){
		$results = array();
		if($trainingType != 4){
			$results = TrainingProgram::join('training_type','training_type.training_type_id','=','training_program.training_type_id')
			->join('item_master','item_master.item_id','=','training_program.item_id')
			->where('training_program.is_active','=',1)
			->where('training_program.company_id','=',$compid)
			->where('training_program.training_program_id','=',$trainingProgId)
			->get();
		}else{
			$results = TrainingProgram::join('blended_program_items','blended_program_items.blended_item_id','=','training_program.blended_program_id')
			->where('training_program.is_active','=',1)
			->where('training_program.company_id','=',$compid)
			->where('training_program.training_program_id','=',$trainingProgId)
			->get();
			
			
		}
		
		return $results;
	}
	
	public static function getTraingProgramwithsubmittraining($compid, $ItemId, $adminid)
	{
	
		$results = array();
		$itemsql ="select im.*,cm.class_id,cm.endate_date from item_master as im
					join class_master as cm on cm.item_id=im.item_id and cm.is_active=1 and cm.item_type_id = 4					
					where im.company_id='$compid' and im.item_id='$ItemId' and im.is_active=1";
		$results=DB::select($itemsql);
		if(count($results) == 0)
		{		
			$item_name =Coursecredits::getItemName($compid,$ItemId);
			$class_data = array(
				'item_id'              => $ItemId,
				'item_type_id'         => 4,
				'class_name'    	   => $item_name,
				'delivery_type_id'     => 1,
				'start_date_class'	   => date('Y-m-d'),
				'endate_date'		   => date('Y-m-d'),
				'status_id'            => 1,
				'is_active'            => 1,
				'company_id'           =>$compid,
				'created_by'           => $adminid,
				'updated_by'           => $adminid,
			);
			ClassMaster::insert($class_data);
			$itemsql ="select im.*,cm.class_id,cm.endate_date from item_master as im
					join class_master as cm on cm.item_id=im.item_id and cm.is_active=1 and cm.item_type_id = 4					
					where im.company_id='$compid' and im.item_id='$ItemId' and im.is_active=1";	
				$results=DB::select($itemsql);
		}
		return $results ;
	}
	
	
	
	public static function getItemName($compid,$itemId){
		
		$item_name='';
		$select="select item_id,item_name from item_master where item_id='$itemId' and company_id='$compid' and is_active=1";
		$results=DB::select($itemsql);
		if(count($results)>0){
			$item_name=$results[0]->item_name;
		}
		return $item_name;
	}
	
	
	public static function giveCreditAssessmentToSelectedUsers($adminid, $compid, $usersid, $trainingProgId, $credit_from) {
	
		$trainingType = 6;
		$tariningProg = Coursecredits::getAssessmentdetail ( $compid, $trainingProgId );
		foreach ( $usersid as $key1 => $userid ) {
				$tainingData ['program_id'] = $tariningProg[0]->id;
				$tainingData ['training_type_id'] = $trainingType;
				$tainingData ['company_id'] = $compid;
				$tainingData ['user_program_type_id'] = 5;
				$tainingData ['learning_plan_id'] = 0;
				$tainingData ['blended_program_id'] = 0;
				$tainingData ['assignment_id'] = 0;
				$tainingData ['is_approved'] = 1;
				$tainingData ['is_enroll'] = 1;
				$tainingData ['session_id'] = 0;
				$tainingData ['training_status_id'] = 1;
				$tainingData ['last_modfied_by'] = $adminid;
				$tainingData ['last_modified_time'] = date ( 'Y-m-d H:i:s' );
				$tainingData ['created_date'] = date ( 'Y-m-d H:i:s' );
				$tainingData ['is_active'] = 1;
				$tainingData ['user_id'] = $userid; 
				Coursecredits::giveAssessmentCreditToUser ( $tainingData, $tariningProg[0]->credit_value,$credit_from );
		}
	}
	
	public static function getAssessmentdetail($compid,$AssessId)
	{
			
		$results = array();
		$itemsql ="select assess.* from assessment as assess where assess.company_id='$compid' and assess.id='$AssessId'";
		$results =DB::select($itemsql);
		return $results ;
	}
	public static function checkAssessment($tainingData){
		$results= UsertrainingCatalog::where('company_id','=',$tainingData['company_id'])->where('user_program_type_id','=',5)->where('user_id','=',$tainingData['user_id'])->where('program_id','=',$tainingData['program_id'])->first();
		return $results;
		
	}
	public static function giveAssessmentCreditToUser($tainingData, $credit, $credit_from)
	{
		
		$get_data=Coursecredits::checkAssessment($tainingData);
		if($get_data){
			if($get_data->training_status_id==1){
				$training_catalog_id =UsertrainingCatalog::insertGetId($tainingData);
			
				UserTrainingCatalogHistory::insert($tainingData);
				
			}else{
				
				$utcData['training_status_id']	=	1;
				$utcData['last_modified_time']	=	date ( 'Y-m-d H:i:s' );
				$utcData ['last_modfied_by'] 	= 	$tainingData['last_modfied_by'];
				$utcData ['is_enroll'] 			= 	1;
				UsertrainingCatalog::where('company_id','=',$tainingData['company_id'])->where('user_program_type_id','=',5)->where('user_id','=',$tainingData['user_id'])->where('program_id','=',$tainingData['program_id'])->update($utcData);
				$training_catalog_id = $get_data->user_training_id;
			}
		}else{ 
				$training_catalog_id =UsertrainingCatalog::insertGetId($tainingData);
			
		}
		
		$completiondate = date('Y-m-d');
		$checkUserTranscript = Coursecredits::checkUserTranscriptforassessment($tainingData , $completiondate);
		if(!$checkUserTranscript)
		{ 
			$transcriptdata ['class_schedule_id'] 		= 0;
			$transcriptdata ['class_id'] 				= 0;
			$transcriptdata ['registration_id'] 		= $tainingData ['program_id'];
			$transcriptdata ['is_blended'] 			    = 0;
			$transcriptdata ['user_id'] 				= $tainingData ['user_id'];
			$transcriptdata ['item_id'] 			    = 0;
			$transcriptdata ['user_training_id'] 		= $training_catalog_id;
			$transcriptdata ['company_id'] 				= $tainingData ['company_id'];
			$transcriptdata ['credit_value'] 			= isset($credit) ? ((!empty($credit))? $credit : 0 ) : 0;
			$transcriptdata ['credit_from'] 			= (!empty($credit_from))? $credit_from : 0;
			$transcriptdata ['completion_date'] 		= $completiondate;
			$transcriptdata ['times'] 					= '0';
			$transcriptdata ['is_approved'] 			= '1';
			$transcriptdata ['is_active'] 				= '1';
			$transcriptdata ['is_expired'] 				= '0';
			$transcriptdata ['last_modified_by'] 		= $tainingData ['last_modfied_by'];
			$transcriptId = UserTranscript::insertGetId($transcriptdata);
		    UserTranscriptHistory::insert($transcriptdata);
		
			 
			if($transcriptId)
			{
			  
			   
			    $assessid = $tainingData ['program_id'];
				$compid	  =$tainingData ['company_id'];
				$userid	  =$tainingData ['user_id'];
				
				$totalquestionsinassess =Assessments::fetchtotalquestionsofassesment($compid, $assessid);
			
				$countuserassessmentinfo = Assessments::userassessmentinfo($assessid,$userid,$compid);
				
				$assess_data=Assessments::getassessmentname($compid, $assessid);
				$due_date   =$assess_data[0]->due_date;
				$userAssessArr['assess_id'] =$assessid;
				$userAssessArr['company_id'] =$compid;
				$userAssessArr['user_id'] =$userid;
				$userAssessArr['is_delete'] =0;  
				$userAssessArr['schedule_id']    =0;
				if(count($countuserassessmentinfo)==0)
					{
						$userAssessArr['blended_prog_id'] = 0;
						$userAssessArr['attempt_taken']  =1;
						$userAssessArr['date_sent'] =date('Y-m-d');
						$userAssessArr['assigned_date'] ='';
						$userAssessArr['due_date'] =$due_date;
					    $userassessmentid =	UserAssessment::insertGetId($userAssessArr);
						
					}
					else
					{
						if(isset($countuserassessmentinfo[0]->blended_prog_id))
						{
							$userAssessArr['blended_prog_id'] =$countuserassessmentinfo[0]->blended_prog_id;
							$userAssessArr['attempt_taken']=$countuserassessmentinfo[0]->user_count+1;
						}
						else
						{
						    $userAssessArr['blended_prog_id'] = 0;
					        $userAssessArr['attempt_taken']=$countuserassessmentinfo[0]->user_count+1;
							$userAssessArr['date_sent'] =date('Y-m-d');
						    $userAssessArr['assigned_date'] ='';
							$userAssessArr['due_date'] =$due_date;
					    }

						 UserAssessment::where('user_id','=',$userid)->where('assess_id','=',$assessid)->where('company_id','=',$compid)->update($userAssessArr);
						$userassessmentid=$countuserassessmentinfo[0]->user_assessment_id;
					}
				$fetchnooftimes = Requirement::getvalueofsubmiteddata($assessid, $userid, $compid);
			    $no_of_times=isset($fetchnooftimes[0]->no_of_times)? $fetchnooftimes[0]->no_of_times: '';
				
				$confirmuser['total_questions'] = $totalquestionsinassess[0]->totalquestions;
				$confirmuser['attempted_questions'] = $totalquestionsinassess[0]->totalquestions;
				$confirmuser['is_completed'] = 1;
				$confirmuser['due_date'] = $due_date;
				$confirmuser['assess_id'] = $assessid;
				$confirmuser['company_id'] = $compid;
				$confirmuser['user_id'] = $userid;
				$confirmuser['correct_answers'] = $totalquestionsinassess[0]->totalquestions;
				$confirmuser['last_updated_date'] = date('Y-m-d -h-i-s');
				$fetchnooftimes = Requirement::getvalueofsubmiteddata($assessid, $userid, $compid);
		
				if(count($fetchnooftimes) == 0) 
				{   $no_of_time1 = 1;
					$confirmuser['no_of_times'] = 1;
					$userassessmentpropid = UserassessmentProp::insertGetId($confirmuser);
					
				}
				else 
				{
					
					$confirmuser['is_delete'] = 0;
					$no_of_time1=$no_of_times+1;
					$confirmuser['no_of_times']=$no_of_time1;
					UserassessmentProp::where('user_id','=',$userid)->where('assess_id','=',$assessid)->where('company_id','=',$compid)->update($confirmuser);
					
					$userassessmentpropid=$fetchnooftimes[0]->id;
				}
				$correctanswers=$totalquestionsinassess[0]->totalquestions;
				$totalquestion=$totalquestionsinassess[0]->totalquestions;
				$totalrightper = ($correctanswers*100)/$totalquestion;
				$totalrightpercent	= 	number_format($totalrightper,2);
				$assessrestrandata = array();
				$assessrestrandata ['user_assess_resp_id'] 		= 0;
				$assessrestrandata ['assess_id'] 				= $tainingData ['program_id'];
				$assessrestrandata ['transcript_id'] 			= $transcriptId;
				$assessrestrandata ['score'] 			        = $totalrightpercent;
				$assessrestrandata ['status'] 			        = 'Complete';
				AssessmentResponseTranscript::insert($assessrestrandata);
				
			}
			
		}else{
			
			if($checkUserTranscript){
			UserTranscriptHistory::insert($checkUserTranscript->toArray());
			}
			$data['user_training_id']	=	$training_catalog_id;
			$data['completion_date']	=   date ('Y-m-d');
			$data['is_active']			=   1;
			$data['last_modified_by']	=   $tainingData ['last_modfied_by'];
			
			UserTranscript::where('company_id','=',$tainingData['company_id'])->where('registration_id','=',$tainingData['program_id'])->where('is_blended','=',0)->where('user_id','=',$tainingData['user_id'])->where('is_active','=',1)->update($data);
				
		}
		$allTrainingItems =Blendedcommonclass::getAllTrainingItems($tainingData['blended_program_id'],$tainingData['company_id']);
		$totalTrainingItems = count($allTrainingItems);			
		$ii = 0;
		$creditArr = array();
		$completionDateArr = array();
		if(count($allTrainingItems)>0){
			foreach ($allTrainingItems as $trainingItems) {
				$item_id = $trainingItems->item_id;
				$assessment_id = $trainingItems->assessment_id;
				if($assessment_id!=0){
					$checkItemCompletion = Blendedcommonclass::checkAssessmentCompletion($tainingData ['user_id'],$assessment_id,$tainingData ['company_id']);
				}else{
					$checkItemCompletion = Blendedcommonclass::checkItemCompletion($tainingData ['user_id'],$item_id,$tainingData ['company_id']);
				}
				if(count($checkItemCompletion)){
					$creditArr[] = $checkItemCompletion[0]->credit_value;
					$completionDateArr[] = strtotime($checkItemCompletion[0]->completion_date);
					$ii++;
				}
			}			
			if($ii == $totalTrainingItems){
				
				Blendedcommonclass::userBlendedCompletion($tainingData ['user_id'],$tainingData['blended_program_id'],$tainingData ['company_id'],$creditArr,$completionDateArr);

			}
		}
	}
	
	public static function checkUserTranscriptforassessment($tainingData , $completion_date)
	{
	    $itemresult=  UserTranscript::where('company_id','=',$tainingData['company_id'])->where('registration_id','=',$tainingData['program_id'])->where('is_blended','=',0)->where('user_id','=',$tainingData['user_id'])->where('is_active','=',1)->first();
		return $itemresult ;
	}
}
 