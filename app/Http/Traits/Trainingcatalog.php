<?php
namespace App\Http\Traits;

use App\Models\UserSupervisiorApprove;
use App\Models\UsertrainingCatalog;
use App\Models\UserTrainingCatalogHistory;
use App\Models\CompanyConfiguration;
use App\Models\UserMaster;
use App\Models\UserTranscript;
use App\Models\BlendedUserComplation;
use Mail,DB;

trait Trainingcatalog{

         public static function getMyClassScheduleDetailsforinprogress($compId, $user_id)
		 {
			 $curdate = date('Y-m-d');
			 $itemsql ="SELECT cm.class_name as schedule_name,group_concat(tc.category_name) as category_name,im.training_code,ty.training_type,im.description, cs.class_schedule_id, cs.class_id,im.item_id, im.item_name, im.training_type_id, im.delivery_type_id, im.published_date,im.credit_value,ty.training_type as itemtype,
			ss.dayid as recursive_date, ss.ses_length_hr, ss.ses_length_min, ss.timeformat, ss.location, ss.start_time_hr, ss.start_time_min,im.description,
			cm.virtual_session_information, tp.training_program_id,tp.training_title, utc.training_status_id, uts.user_status,
			utc.user_training_id,cm.item_type_id,im.require_supervisor_aproval_status FROM class_schedule cs
			join class_master cm on cm.class_id = cs.class_id and cm.is_active = 1
			join item_master im on im.item_id = cm.item_id and im.company_id = '$compId'
			join item_master_trainingcategory imt on imt.item_id =im.item_id and imt.is_active=1 and imt.company_id = '$compId'
			join training_category tc on tc.training_category_id =imt.training_category_id and tc.is_active=1 and tc.company_id='$compId' 
			join user_training_catalog utc on utc.item_id = cm.item_id and utc.session_id = cm.class_id
			join user_training_status uts on uts.user_status_id = utc.training_status_id
			INNER JOIN training_type ty ON ty.training_type_id = im.training_type_id
			join training_program tp on tp.item_id = im.item_id and tp.class_id = cm.class_id and tp.company_id='$compId' 
			JOIN (SELECT min(ssd.dayid) as dayid, ssd.class_id, ssd.ses_length_hr, ssd.ses_length_min, ssd.timeformat, ssd.location, ssd.start_time_hr, ssd.start_time_min from session_scedule ssd where ssd.company_id = '$compId' and ssd.is_active = 1 
			group by ssd.class_id) ss ON cs.class_id = ss.class_id
			WHERE cs.is_deleted = 0 and cs.is_active = 1 and
			(cm.item_type_id =2 or cm.item_type_id =3) and cm.start_date_class >= '{$curdate}' and utc.user_id = '$user_id' and utc.training_status_id not in ('0', '1', '5') GROUP BY im.item_id,cm.class_id";
			
			$itemresult=DB::select($itemsql);
			return $itemresult;
		 }
		 
		 
		 
		 public static function getMyClassScheduleDetails($compId, $user_id)
			{
				$itemsql = "SELECT cm.class_name as schedule_name,group_concat(tc.category_name) as category_name,im.training_code,ty.training_type,im.description, cs.class_schedule_id, cs.class_id,im.item_id, im.item_name,	ty.training_type_id, im.delivery_type_id, im.published_date,im.credit_value,ty.training_type as itemtype, ss.dayid as recursive_date, ss.ses_length_hr, ss.ses_length_min, ss.timeformat, ss.location, ss.start_time_hr, ss.start_time_min,im.description, cm.virtual_session_information, tp.training_program_id, tp.training_title, utc.training_status_id, uts.user_status, utc.user_training_id,cm.item_type_id,im.require_supervisor_aproval_status FROM class_schedule cs
				join class_master cm on cm.class_id = cs.class_id and cm.is_active = 1 and cm.company_id = '$compId'
				join item_master im on im.item_id = cm.item_id and im.company_id = '$compId'
				join item_master_trainingcategory imt on imt.item_id =im.item_id and imt.is_active=1 and imt.company_id = '$compId'
				join training_category tc on tc.training_category_id =imt.training_category_id and tc.is_active=1 and tc.company_id='$compId' 
				join user_training_catalog utc on utc.item_id = cm.item_id and utc.session_id = cm.class_id
				join user_training_status uts on uts.user_status_id = utc.training_status_id
				INNER JOIN training_type ty ON ty.training_type_id = cm.item_type_id
				join training_program tp on tp.item_id = im.item_id and tp.class_id = cm.class_id and tp.company_id = '$compId'
				JOIN (SELECT min(ssd.dayid) as dayid, ssd.class_id, ssd.ses_length_hr, ssd.ses_length_min, ssd.timeformat, ssd.location, ssd.start_time_hr, ssd.start_time_min from session_scedule ssd where ssd.company_id = '$compId' and ssd.is_active = 1 
				group by ssd.class_id) ss ON cs.class_id = ss.class_id
				WHERE cs.is_deleted = 0 and cs.is_active = 1 and
				(cm.item_type_id =2 or cm.item_type_id =3) and utc.user_id = '$user_id' and utc.training_status_id not in ('0', '1', '5') GROUP BY im.item_id,cm.class_id";
				$itemresult=DB::select($itemsql);
				return $itemresult;
			}
			
			
			public static function checksupervisiorapprove($usertrainingid)
			  {
				$results=UserSupervisiorApprove::wheretrainingCatalogId($usertrainingid)
				                               ->where('is_approve',0)
											   ->get();
				
				return $results;
			  }
			  
			  
	public static function EnrolledForElearningLearningplan($course_id, $userid, $userprogramtypeid, $compid)
	{

		$data = array(
			'class_id'              => 0,
			'course_id'      	   	=> 	$course_id,
			'is_approved'           => 	1,
			'is_enroll'          	=> 	1,
			'user_id'           	=>	$userid,
			'company_id'    		=>	$compid,
			'training_type_id'      => 	$userprogramtypeid,
			'training_status_id'	=>	3,
			'last_modfied_by'     	=> 	$userid,
			'created_date'     		=> 	date("Y-m-d H:i:s"),
			'duration'     			=> '',
			'status_id'             =>1,
			'duration_log'     		=> '',
		);
		$last_id =UsertrainingCatalog::insertGetId($data);
		$data['user_training_id']=$last_id;
		UserTrainingCatalogHistory::insert($data);
		return $last_id;
	}

	public static function getUserCourseInfo($userid, $item_id, $classid='', $compid)
	{
        $utc_arr=array();
	    $sqlStr = ($classid!='') ? ' and utc.class_id != :class_id' : '';
		$sql = "select utc.class_id, utc.training_status_id, uts.user_status,min(ss.dayid) as startDate, ut.transcript_id, ut.completion_date from user_training_catalog utc 
		join user_training_status uts on uts.user_status_id=utc.training_status_id
		join session_scedule ss on ss.class_id=utc.class_id and ss.status_id=1 and ss.company_id=:compid1
		join class_master as cm on cm.class_id=ss.class_id and cm.status_id=1 and  cm.is_active=1 and cm.company_id=:compid2
		left join user_transcript ut on ut.course_id=utc.course_id and ut.status_id=1 and ut.user_id=:userid1 and ut.company_id=:compid3
		where utc.user_id =:userid2 and utc.course_id=:item_id1 and utc.status_id=1 and utc.company_id=:compid4 and utc.training_status_id in (1,3,4,10) $sqlStr";
		
		$utc_arr['userid1']		=$userid;
		$utc_arr['userid2']		=$userid;
		$utc_arr['item_id1']	=$item_id;
		$utc_arr['compid1']		=$compid;
		$utc_arr['compid2']		=$compid;
		$utc_arr['compid3']		=$compid;
		$utc_arr['compid4']		=$compid;
		if($classid){
		$utc_arr['class_id']	=$classid;
		}
		$result=DB::select(DB::raw($sql) ,$utc_arr); 

		return json_decode(json_encode($result),true);
	} 
	
  public static function getuserdetail($ids, $compid)
	{
		$result=UserMaster::join('user_supervisor',function($join) use($compid){
			                         $join->on('user_supervisor.user_id','=','user_master.user_id')
									       ->where('user_supervisor.company_id',1)
									       ->where('user_supervisor.is_active',$compid);
		                              })
		                               ->where('user_master.user_id',$ids)
		                               ->where('user_master.company_id',$compid)
		                               ->where('user_master.is_delete',0)
		                               ->where('user_master.is_active',1)
									   ->select('user_master.*','user_supervisor.supervisor_id')
									   ->get();
	  return $result;								   
	}
	
	public static function getCompanyname($compid)
	{
		$result = CompanyConfiguration::wherecompanyConfigId($compid)->select('company_name')->get();
		return $result;
	}
	

	public static function getcertificate($id)
	{
         $lang_id=1;
		$results=UserTranscript::join('user_master','user_master.user_id','=','user_transcript.user_id')
		->join('courses','courses.course_id','user_transcript.course_id')
		->leftjoin('sco_score_data',function($join) use($id) {
				$join->on('sco_score_data.item_id','=','user_transcript.course_id')
				->on('sco_score_data.user_id','=','user_transcript.user_id');
			
				})
			->join('course_details',function($join) use($lang_id){
				$join->on('course_details.course_id','=','courses.course_id')
				->where('course_details.lang_id','=',$lang_id);
			})
		->leftjoin('certificate_content','certificate_content.id','=','courses.certificate_template_id')
		->where('user_transcript.transcript_id','=',$id)->select(DB::raw('user_master.user_name as USER_NAME,user_transcript.completion_date as DATE_OF_COMPLETION,concat(course_details.course_name,".",course_details.course_title) as TRAINING_TITLE,courses.credit_value as CREDIT,sco_score_data.correct_answer as SCORE,certificate_content.*'))->first();
		  	
		return $results;

    }
	
	public static function checkrecordBlended($blendedprogid,$userid,$compid) 
	{
		$result=BlendedUserComplation::where('user_id','=',$userid)->where('blended_id','=',$blendedprogid)->where('company_id','=',$compid)->get()->toArray();
		return $result;
	}
    
	public static function averageClassRating($id,$compid) 
	{
		$results =UsertrainingCatalog::join('user_rating_master','user_rating_master.user_training_id','=','user_training_catalog.user_training_id')
		->where('user_training_catalog.course_id','=',$id)
		->where('user_training_catalog.company_id','=',$compid)
		->where('user_rating_master.status_id','=',1)
		->avg('class_rate');
		return $results;
	}
	
}