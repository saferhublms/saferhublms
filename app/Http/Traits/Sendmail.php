<?php
namespace App\Http\Traits;

use App\Models\CompanyCmsInfo;
use App\Models\UserMaster;
use App\Models\IcalNotification;
use App\Models\NotificationMaster;
use App\Models\EmailNotification;
use App\Models\AssignmentMaster;
use App\Models\TrainingProgram;
use App\Models\Course;
use App\Models\BlendedProgramItem;
use App\Models\ClassSchedule;
use App\Models\TbluserlLink;
use App\Models\ClassMaster;
use App\Models\SessionScedule;
use App\Models\UsertrainingCatalog;
use App\Models\UserTranscript;
use CommonFunctions,Mail,DB;
use DateTime;
trait Sendmail{

	 public static function usertrainingcatelog($usertrainingid) {

		$sql ="SELECT user_training_catalog.*,cm.class_name as item_name, cm.class_name, cm.class_id,user_master.user_name, user_master.email_id, user_supervisor.supervisor_id, company_configuration.company_name, app_configuration.app_url,cm.class_id
		FROM user_training_catalog
		INNER JOIN class_master AS cm ON cm.class_id = user_training_catalog.class_id
		JOIN courses AS im ON im.course_id=user_training_catalog.course_id
		JOIN course_details as cd on cd.course_id=im.course_id
		INNER JOIN user_master ON (user_training_catalog.user_id = user_master.user_id)
		INNER JOIN user_supervisor ON (user_supervisor.user_id = user_master.user_id AND user_supervisor.is_active=1)
		INNER JOIN company_configuration ON (user_training_catalog.company_id = company_configuration.company_config_id)
		INNER JOIN app_configuration ON (user_training_catalog.company_id = app_configuration.company_id)
		WHERE user_training_catalog.user_training_id = :user_training_id group by  user_training_catalog.user_training_id";
	    $param	=array();
		$param['user_training_id']   =$usertrainingid;
		$result = DB::select(DB::raw($sql),$param);
		return $result;
		
	}
	 
	 public static function getinstructorname($insid) 
	{
		$result =UserMaster::where('user_id','=',$insid)->select('user_name')->first();
		return $result;
	}
	 
	 public static function getsuperemail($supervisor_id, $user_id , $compid)
	{
		
		$result = UserMaster::join('user_supervisor','user_supervisor.supervisor_id','=','user_master.user_id')
		->where('user_supervisor.is_active','=',1)->where('user_supervisor.supervisor_id','=',$supervisor_id)->where('user_supervisor.user_id','=',$user_id)->where('user_supervisor.company_id','=',$compid)->select('user_master.role_id','user_master.email_id','user_master.user_name','user_supervisor.supervisor_id')->first();
		
		return $result;
	}
	
	 public static function getLateststartdate($classid, $compid)
	{
		$result =SessionScedule::where('class_id','=',$classid)->where('company_id','=',$compid)->where('status_id','=',1)->select(DB::raw('min(dayid) as dayid'))->get();
		return $result;	
	}
	 
	    public static function getNotificationContent($notifiid, $compid, $notificationvar) 
		{
			
			$getNotificationContent = Sendmail::getEmailTemp($notifiid, $compid, $notificationvar);
			
			$subjectEnroll =$bodycontent = '';
			$rsendstatus = 'no';
			if($getNotificationContent) { 
		       if($getNotificationContent['activenotification']==1) {
                  if($getNotificationContent['is_active']==1) {  
				     if($getNotificationContent['is_default']==0) {
						 if(trim($getNotificationContent['content'])!="" && trim($getNotificationContent['notisubject'])!="" ){
							$subjectEnroll=$getNotificationContent['notisubject']; 			
							$bodycontent=$getNotificationContent['content'];
						}
						else {				
							$subjectEnroll=$getNotificationContent['subject'];                  					
							$bodycontent=$getNotificationContent['notification_content'];
						}
					 }else {
							$subjectEnroll=$getNotificationContent['subject'];
							$bodycontent=$getNotificationContent['notification_content'];
					}
					
					if(is_array($notificationvar)) {
						  if(count($notificationvar['username']) > 1) {
							    set_time_limit(0);
								$fromname 	= 	$from 		= CommonFunctions::getFromCompanyName($compid);
								$subject	=	$subjectEnroll;
							    $des		=	@$notificationvar['virtualSessionInfo'];
								foreach($notificationvar['username'] as $subKey => $subValue)
								{
									$toemailid = $notificationvar['toemailid'][$subKey];
								    $username 	= $notificationvar['username'][$subKey];
									$from 		= CommonFunctions::getFromEmailId($compid);
									$emailname = $username;
									if($bodycontent !="")
									{   
										$content = Sendmail::getNotificationContentEmail($notificationvar, $bodycontent, $username);
									}
                                    if(in_array($notifiid,array(9,31))){ //9 For drop session schedule date, 31 for remove class room
									    Sendmail::dropCalendarMeetingEmail($from, $fromname, $subject, $content, $toemailid, $emailname, $notificationvar, $notifiid);
										
									}elseif($notifiid==1){ //For update session schedule date
											Sendmail::updateCalendarMeetingEmail($from, $fromname, $subject, $toemailid, $content, $emailname, $notifiid, $notificationvar); 
									}else{
											$rsendstatus= Sendmail::sendmail($from, $fromname, $subject, $content, $toemailid, $emailname,$compid);
									}

									
								}
								
						  }else{
								$toemailid = $notificationvar['toemailid'];
								//$toemailid = 'anandrishabh724@gmail.com';
								if(isset($notificationvar['assignmentname']))
									$trainingClassName = $notificationvar['assignmentname'];
								if(isset($notificationvar['title']))				
									$title = $notificationvar['title']; 
									$description = @$notificationvar['virtualSessionInfo'];
								if(isset($notificationvar['topic']))
									$topic = $notificationvar['topic'];
								
								if($bodycontent !="") {
									if(is_array($notificationvar['username'])){
									$userarray = array_values($notificationvar['username']);
									$username	= $userarray[0];
									}else{
										$username = $notificationvar['username'];
									}
									$content = Sendmail::getNotificationContentEmail($notificationvar, $bodycontent, $username);
									$from 		= CommonFunctions::getFromEmailId($compid);
									$fromname 	= CommonFunctions::getFromCompanyName($compid);
									$subject	= $subjectEnroll;
								    $emailname	= $notificationvar['username'];
									
								}
								
                                if(in_array($notifiid,array(9,31))){ //9 For drop session schedule date, 31 for remove class room
								  
									Sendmail::dropCalendarMeetingEmail($from, $fromname, $subject, $content, $toemailid, $emailname, $notificationvar, $notifiid);
							   }elseif($notifiid==1){ 
								//For update session schedule date
									Sendmail::updateCalendarMeetingEmail($from, $fromname, $subject, $toemailid, $content, $emailname, $notifiid, $notificationvar);  
								}else{
									$rsendstatus =  Sendmail::sendmail($from, $fromname, $subject, $content, $toemailid, $emailname,$compid);
								}								
								
						  } 
					}
						
						
						
			     }
			  }
		   }
		   return $rsendstatus;	
		}
		
		
		public static function getEmailTemp($notifiid, $compid, $notificationvar)
		{
			if(isset($notificationvar['user_id']))
			{
				$user_id = $notificationvar['user_id'];
			
				if(isset($user_id)){
					$user_res = UserMaster::whereuserId($user_id)->whereisActive(1)->whereisDelete(0)->whereisApproved(1)->wherecompanyId($compid)->select('email_id')->get();
					if(!$user_res){
						return false;
					}
				}		
			}
			
			$getNotificationContent=NotificationMaster::leftjoin('notification_company','notification_company.notification_id','=','notification_master.notification_id')
			->leftjoin('notification_content',function($join) use ($compid){
			                 $join->on('notification_content.notification_id','=','notification_master.notification_id')
                                  ->where('notification_content.company_id',$compid)							 ;
			})
			->where('notification_master.notification_id',$notifiid)
			->where('notification_master.is_active',1)
			->where('notification_company.is_active',1)
			->select('notification_master.notification_id', 'notification_master.notification_name', 'notification_master.subject', 'notification_master.notification_content', 'notification_company.is_active' , 
			'notification_company.is_default as is_default', 'notification_master.is_active as activenotification', 'notification_content.content','notification_content.subject as notisubject')
			->first();
			$getNotificationContent->is_default=($getNotificationContent->is_default==null)?1:$getNotificationContent->is_default;
			
			
			return $getNotificationContent;
		}
		
		
		public static function dropCalendarMeetingEmail($from, $fromname, $subject, $content, $toemailid, $emailname, $notificationvar, $notifiid ) 
		{
			$ical_content = preg_replace("/[\\n\\r]+/", "", $content);	
			$course_id = $notificationvar["course_id"];
			$class_id = $notificationvar["class_id"];
			$schedule_id = $notificationvar["schedule_id"];
			$user_id = $notificationvar["user_id"];
			$company_id = $notificationvar["company_id"];
			
			$res=IcalNotification::wherecourseId($course_id)->whereclassId($class_id)->wherecompanyId($company_id)->whereuserId($user_id)->get();
			
			$cnt = 0;
			if($res){
			$i=1;
			    $total_session = count($res);
				foreach($res as $ical_res){
					$dtstart = $ical_res['dtstart'];
					$dtend = $ical_res['dtend'];
					$email_id = $ical_res['email_id'];
					$cal_uid = $ical_res['cal_uid'];
					$timezone_name = $ical_res['timezone_name'];
					
					$mail_subject = $subject." Session $i/$total_session";
					
					$ical = 'BEGIN:VCALENDAR' . "\r\n" .
					'VERSION:2.0' . "\r\n" .				
					'PRODID://Microsoft Corporation//Outlook 11.0 MIMEDIR//EN' . "\r\n" .
					'METHOD:CANCEL' . "\r\n" .
					'BEGIN:VEVENT' . "\r\n" .
					'STATUS:CANCELLED' . "\r\n" .
					'TZID:'.$timezone_name. "\r\n" .
					'ORGANIZER;CN="'.$fromname.'":MAILTO:'.$from. "\r\n" .
					'ATTENDEE;CN="'.$emailname.'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:'.$toemailid. "\r\n" .
					'UID:'.$cal_uid."\r\n" .
					'DTSTART:'.$dtstart. "\r\n" .
					'DTEND:'.$dtend. "\r\n" .
					'SEQUENCE:1'. "\r\n" .
					'SUMMARY:' .  " $mail_subject \r\n" .
					'X-ALT-DESC;FMTTYPE=text/html: ' . " $ical_content \r\n".
					'END:VEVENT'. "\r\n" .
					'END:VCALENDAR'. "\r\n";
					
					$content = str_replace("scheduled class","scheduled class Session $i/$total_session",$content);
					Sendmail::icalMail($ical, 'drop_calendar_'.MD5(TIME()).$i.'.ics', $from, $fromname, $toemailid, $emailname, $mail_subject, $content);
				
					$i++;
					$cnt++;
					
				}
				
				 $icalNotification=IcalNotification::wherecourseId($course_id)->whereclassId($class_id)->wherecompanyId($company_id)->whereuserId($user_id);
				if($schedule_id!='') {
				  $icalNotification=$icalNotification->wherescheduleId($schedule_id);
			    }
				$icalNotification->delete();
			}
			
			
		}
		/* */
		
		
		
		public static function getNotificationContentEmail($notificationvar, $bodycontent, $username)
		{
			
			$bodycontent=stripslashes($bodycontent);
			
			$content = str_replace("%#USER NAME#%",$username,$bodycontent);
			$content = str_replace("%#CLASS NAME#%",$notificationvar['trainingclassname'],$content);
			$content = @str_replace("%#COURSE ID#%",$notificationvar['courseid'],$content);
			$content = str_replace("%#COURSE NAME#%",$notificationvar['trainingclassname'],$content);
			$content = str_replace("%#USER ID#%",$notificationvar['usermailid'],$content);
			$content = str_replace("%#SUPERVISOR NAME#%",$notificationvar['supervisorusername'],$content);
			$content = str_replace("%#CLASS DATE#%",date('m-d-Y',strtotime($notificationvar['trdate'])),$content);
			$content = str_replace("%#DATE OF COMPLETION#%",$notificationvar['date_of_completion'],$content);
			$content = str_replace("%#DUE DATE#%",$notificationvar['duedate'],$content);
			$content = str_replace("%#LOCATION#%",$notificationvar['location'],$content);
			$content = str_replace("%#INSTRUCTOR NAME#%",$notificationvar['INstructorname'],$content);
			$content = str_replace("%#INSTRUCTOR LOGIN ID#%",$notificationvar['InstructorloginId'],$content);
			$content = str_replace("%#CLASS SCHEDULE#%",$notificationvar['sendscedule'],$content);
			$content = str_replace("%#START DATE#%",$notificationvar['starthr'],$content);
			$content = str_replace("%#START TIME#%",$notificationvar['startmin'],$content);
			$content = str_replace("%#TIME FORMAT#%",$notificationvar['timeformat'],$content);
			$content = str_replace("%#Virtual_Session_Information#%",$notificationvar['virtualSessionInfo'],$content);
			if(isset($notificationvar['virtualSessionInfo']))
			 $content = str_replace("%#DESCRIPTION#%",$notificationvar['virtualSessionInfo'],$content);		
			$content = str_replace("%#COMPANY NAME#%",$notificationvar['companyname'],$content);
			$content = str_replace("%#COMPANY URL#%",$notificationvar['companyurl'],$content);
			$content = str_replace("%#COST#%",$notificationvar['cost'],$content);
			$content = str_replace("%#ASSESSMENT NAME#%",$notificationvar['assessmentname'],$content);
			$content = str_replace("%#ASSIGNMENT NAME#%",$notificationvar['assignmentname'],$content);
			$content = str_replace("%#LOGIN NAME#%",$notificationvar['login_id'],$content);
			$content = str_replace("%#TIMEZONE#%",$notificationvar['timezone_name'],$content);
			$content = str_replace("%#SCORE#%",$notificationvar['score'],$content);
			
			$companyurl1=$_SERVER["HTTP_HOST"].'/'.'public/editorimage';

			$companyurl="https://$companyurl1";
					
			//$content = str_replace("/public/editorimage",$companyurl,$content);
			
			
			if(isset($notificationvar['toplan_name']))
				$content = str_replace("%#LEARNING PLAN NAME#%",$notificationvar['toplan_name'],$content);				
			$content = str_replace("%#PASSWORD#%",$notificationvar['password'],$content);
			$content = str_replace("%#DISCUSSION NAME#%",$notificationvar['topic'],$content);
			$content = str_replace("%#POST BY#%",$notificationvar['posted_by'],$content);
			$content = str_replace("%#BLOG NAME#%",$notificationvar['title'],$content);				
			$content = str_replace("%#PREREQUISITE#%",$notificationvar['prerequisite'],$content);	
			$content = str_replace("{LINK}",$notificationvar['urlclicklink'],$content);
			$content = str_replace("{CUSTOM URL}",$notificationvar['customlink'],$content);
			$content = str_replace("%#EXPIRE DATE#%",$notificationvar['expiredate'],$content);
			if(isset($notificationvar['credit']))
				$content = str_replace("%#CREDIT#%",$notificationvar['credit'],$content);
			if(isset($notificationvar['duedays']))
				$content = str_replace("%#DAYS#%",$notificationvar['duedays'],$content);
				if(isset($notificationvar['course_detail']))
				$content = str_replace("%#COURSE DETAIL#%",$notificationvar['course_detail'],$content);
				if(isset($notificationvar['learning_plan_name']))
				$content = str_replace("%#Learning Plan#%",$notificationvar['learning_plan_name'],$content);
	
			return $content;
	
	}
	
	public static function sendmail($from,$fromname, $subject,$content,$toemailid,$emailname,$compid = 0) 
	{ 
	      $default_email = CommonFunctions::getCompanyDefaultEmails();
		  if($default_email!='blank'){
			    $toemailid = $default_email;
		   }
		   	//$toemailid = 'siddharth.kushwah@innverse.com';
		try{
			Mail::send([], [], function ($message) use($toemailid, $emailname,$subject,$from, $fromname,$content){
				 
				    $message->from($from, $fromname);
				    $message->to($toemailid, $emailname);
					$message->subject($subject);
					$message->setBody($content, 'text/html'); // for HTML rich messages
				});	
				$data = array(
				'email_id'              => $toemailid,
				'company_id'     	   =>  $compid,
				'send_date'            => 	date('Y-m-d'),
				'subject'				=>	$subject,
				'email_content'          => $content,
				);
				if($toemailid!='') {
				   EmailNotification::create($data);
			    }
				return 'yes';
		}catch(Exception $e){
			return 'no';
		}	
		  print_r($default_email);exit;
	}
	
	public static function AssignmentRecord($assignmentId) {
		
		$result = AssignmentMaster::where('assignment_id', '=', $assignmentId)->select('company_id','training_program_id','due_date','assignment_start_date','created_date')->get();
		return $result;
	
	}
	
    public static function TrainingProgramRecord($training_program_id) {
		$result = TrainingProgram::where('training_program_id','=',$training_program_id)->select('item_id','class_id','blended_program_id')->get();
		return $result;
	}
	
	public static function ItemRecord($item_id) {
		$result = ItemMaster::where('item_id','=',$item_id)->select('title','description')->get();
		return $result;
	}
	
	public static function BlenderRecord($blended_program_id) {
		$result = BlendedProgramItem::where('blended_item_id', '=', $blended_program_id)->select('title','description')->get();
		return $result;
	}
	
	public static function getschedulenamefromscheduleclass($class_id) 
	{
		 $result = ClassSchedule::where('class_id','=',$class_id)->select('class_schedule_id','schedule_name')->get();
		return $result;
	}
	
	public static function getuseremail($userid,$compid) {
		
		$result = UserMaster::join('user_supervisor',function($join){
			           $join->on('user_supervisor.user_id','=','user_master.user_id')   
					        ->where('user_supervisor.is_active','=',1);
		           })->where('user_master.user_id','=',$userid)
				     ->where('user_master.company_id','=',$compid)
				     ->where('user_master.is_active','=',1)
				     ->where('user_master.is_delete','=',0)
					 ->select('user_master.role_id','user_master.email_id','user_master.user_name','user_supervisor.supervisor_id')
					 ->get();
		return $result;
	}
	
	
	public static function insertanywhere(array $data) {
	$last_id = TbluserlLink::insertGetId($data);
        return $last_id;
	}
		
	public static function getCourseDes($itemid,$compid) 
	{
	    $result =  Course::join('course_details','course_details.course_id','=','courses.course_id')->where('courses.company_id','=',$compid)->where('courses.course_id','=',$itemid)->select('course_details.course_description','courses.training_program_code')->first();

		return $result;
	}
	
	public static function classinfoonitemid($classid,$compid)
	{
		$result =ClassMaster::where('company_id','=',$compid)->where('class_id','=',$classid)->select('virtual_session_information','delivery_type_id','class_name')->first();
		return $result;
	}
	
	public static function getScheduleStartDate($classid, $compid)
	{
		$sql = "SELECT ss.schedule_id, ss.dayid, ss.ses_length_hr, ss.ses_length_min, ss.start_time_hr, ss.start_time_min, ss.timeformat, ss.location, um.user_name, ctz.timezone_name, ctz.timezone_regions, ctz.timezone_dst, ctz.utc_offset, ctz.utc_offset_dst FROM session_scedule AS ss LEFT JOIN user_master AS um ON (ss.instructor_id=um.user_id) LEFT JOIN class_timezones AS ctz ON (ss.time_zone=ctz.class_timezone_id) WHERE ss.class_id =:class_id  AND ss.company_id=:company_id and ss.status_id=1";
		$param	=array();
		$param['class_id'] 		=$classid;
		$param['company_id']    =$compid;
		$result = DB::select(DB::raw($sql),$param);
		return $result;
	}
	
	public static function allSessionsForClassMail($classid, $compid) {
		   $sql = "SELECT she.dayid AS enddate, she.dayid AS startdate, she.location, she.start_time_hr, she.start_time_min, she.timeformat, she.time_zone, she.dayid, she.ses_length_min, she.ses_length_hr, she.instructor_id, cm.maximum_seat FROM session_scedule she INNER JOIN class_master cm ON (she.class_id = cm.class_id)  WHERE she.class_id =:class_id AND she.status_id = 1 AND   she.company_id = :company_id1 AND cm.company_id = :company_id2 order by startdate asc";
			$param	=array();
			$param['class_id'] 		 =$classid;
			$param['company_id1']    =$compid;
			$param['company_id2']    =$compid;
			$result = DB::select(DB::raw($sql),$param);
			return $result;
	}
	
	public static function getIcalNotificationContent($notifiid, $compid, $notificationvar) 
	{ 	
				
		$getNotificationContent = Sendmail::getEmailTemp($notifiid, $compid, $notificationvar);
		$getNotificationContent = json_decode(json_encode($getNotificationContent),true);
		$subjectEnroll =$bodycontent = '';
   
		if($getNotificationContent)
		{  
			if($getNotificationContent['activenotification']==1)
			{
				if($getNotificationContent['is_active']==1)
				{          
					if($getNotificationContent['is_default']==0)
					{	
						if(trim($getNotificationContent['content'])!="" && trim($getNotificationContent['notisubject'])!="" ){
							$subjectEnroll=$getNotificationContent['notisubject']; 					
							$bodycontent=$getNotificationContent['content'];
						}
						else
						{				
							$subjectEnroll=$getNotificationContent['subject'];                  					
							$bodycontent=$getNotificationContent['notification_content'];
						}
					}
					else
					{
							$subjectEnroll=$getNotificationContent['subject'];
							$bodycontent=$getNotificationContent['notification_content'];
					}
					
					if(is_array($notificationvar))
					{	
						if(is_array($notificationvar['username'])){
							foreach($notificationvar['username'] as $subKey => $subValue)
							{  
								$toemailid = $notificationvar['toemailid'][$subKey];
								$username = $notificationvar['username'][$subKey];
								if($bodycontent !="")
								{
									$content = Sendmail::getNotificationContentEmail($notificationvar, $bodycontent, $username);
		
                                     $from 		= CommonFunctions::getFromEmailId($compid);
								     $fromname 	= CommonFunctions::getFromCompanyName($compid);
									 if($notificationvar['total_session']=="1/1"){
											$subject	= $subjectEnroll;
										}else{
											$subject	= $subjectEnroll." Session ".$notificationvar['total_session'];
										}
									
									$emailname 	= $username;
									$data = array(
										'email_id'              =>	$toemailid,
										'company_id'     	    => 	$compid,
										'send_date'             => 	date('Y-m-d'),
										'subject'				=>	$subject,
										'email_content'         => 	$content,
									);
									
									$email_notification_id = EmailNotification::insertGetId($data);
									
									$notificationvar['email_notification_id']	= $email_notification_id;
									
									sendmail::calendarMeetingEmail($from, $fromname, $subject, $toemailid, $content, $emailname, $notifiid, $notificationvar); 									
								} 
							}	
						}else{
							 
							$toemailid = $notificationvar['toemailid'];
							$username = $notificationvar['username'];
							if($bodycontent !="")	
							{ 
						
								$bodycontent=stripslashes($bodycontent);
								
								$content = Sendmail::getNotificationContentEmail($notificationvar, $bodycontent, $username);
								
								$from 		= CommonFunctions::getFromEmailId($compid);
								$fromname 	= CommonFunctions::getFromCompanyName($compid);
								
								 if($notificationvar['total_session']=="1/1"){
											$subject	= $subjectEnroll;
										}else{
											$subject	= $subjectEnroll." Session ".$notificationvar['total_session'];
									}
								
								$emailname 	= $notificationvar['username'];
								$location	= $notificationvar['location'];
								$trdate		= $notificationvar['trdate'];
								$starthr	= $notificationvar['starthr'];  
					
								$data = array(
									'email_id'              =>	$toemailid,
									'company_id'     	    => 	$compid,
									'send_date'             => 	date('Y-m-d'),
									'subject'				=>	$subject,
									'email_content'         => 	$content,
								);
								//echo '<pre>'; print_r($data); exit;
								
								$email_notification_id =EmailNotification::insertGetId($data);

								$notificationvar['email_notification_id']	= $email_notification_id;
								
								sendmail::calendarMeetingEmail($from, $fromname, $subject, $toemailid, $content, $emailname, $notifiid, $notificationvar); 
							} 
						}
					}	
				}
			}
		}		
	}
	
	
	public static function calendarMeetingEmail($from_address, $from_name, $subject, $toemailid, $emailcontent, $emailname, $notifiid="", $notificationvar)
	{
		//execution time 
		set_time_limit(0);
		
		
		$emailname = ucwords($emailname);
		
		$from_name = ucwords($from_name);
		
		$location = $notificationvar['location'];
		
		list($timezone_name, $tzname, $timezone_dst, $utc_offset, $utc_offset_dst, $meetingstartstamp, $dtstart, $dtend) = explode("||",sendmail::getIcalDTStartDTEnd($notificationvar));
		
		//Create unique identifier
		$cal_uid = DATE('Ymd').'T'.DATE('His')."-".RAND()."@mylms.com";		
		
		
		$icalcontent = preg_replace("/[\\n\\r]+/", "", $emailcontent);	
		
		
		$ical = 'BEGIN:VCALENDAR' . "\n" .
		'VERSION:2.0' . "\n" .
		'PRODID://Microsoft Corporation//Outlook 11.0 MIMEDIR//EN' . "\n" .
		'METHOD:REQUEST' . "\n" .
		'BEGIN:VTIMEZONE' . "\n" .
		'TZID:'.$timezone_name . "\n" .
		'BEGIN:STANDARD' . "\n" .
		'DTSTART:19701101T020000' . "\n" .
		'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11' . "\n" .
		'TZOFFSETFROM:'.$utc_offset_dst . "\n" .
		'TZOFFSETTO:'.$utc_offset . "\n" .
		'TZNAME:'.$tzname . "\n" .
		'END:STANDARD' . "\n" .
		'BEGIN:DAYLIGHT' . "\n" .
		'DTSTART:19700308T020000' . "\n" .
		'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3' . "\n" .
		'TZOFFSETFROM:'.$utc_offset . "\n" .
		'TZOFFSETTO:'.$utc_offset_dst . "\n" .
		'TZNAME:'.$timezone_dst . "\n" .
		'END:DAYLIGHT' . "\n" .
		'END:VTIMEZONE' . "\n" .
		'BEGIN:VEVENT' . "\n" .
		'STATUS:CONFIRMED' . "\n" .
		'ORGANIZER;CN="'.$from_name.'":MAILTO:'.$from_address. "\n" .
		'ATTENDEE;CN="'.$emailname.'";CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE:MAILTO:'.$toemailid. "\n" .
		'LAST-MODIFIED:' . date("Ymd\THis") . "\n" .
		'UID:'.$cal_uid."\n" .
		'DTSTAMP:'.date("Ymd\THis"). "\n" .
		'DTSTART;TZID='.$timezone_name.':'.$dtstart. "\n" .
		'DTEND;TZID='.$timezone_name.':'.$dtend. "\n" .
		'TRANSP:OPAQUE'. "\n" .
		'SEQUENCE:1'. "\n" .
		'SUMMARY:' .  " $subject \n" .
		'X-ALT-DESC;FMTTYPE=text/html: ' . " $icalcontent \n".
		'LOCATION:' . " $location \n" .
		'CLASS:PUBLIC'. "\n" .
		'X-MICROSOFT-CDO-IMPORTANCE:1'. "\n" .
		'PRIORITY:5'. "\n" .
		'BEGIN:VALARM' . "\n" .
		'TRIGGER:-PT15M' . "\n" .
		'ACTION:DISPLAY' . "\n" .
		'DESCRIPTION:Reminder' . "\n" .
		'END:VALARM' . "\n" .
		'END:VEVENT'. "\n" .
		'END:VCALENDAR'. "\n";
	    
		sendmail::icalMail($ical, 'calendar_'.MD5(TIME()).'.ics', $from_address, $from_name, $toemailid, $emailname, $subject, $emailcontent);
		
		try {
			
			if(isset($notificationvar)){
				$course_id = $notificationvar['course_id'];
				$class_id = $notificationvar['class_id'];
				$schedule_id = $notificationvar['schedule_id'];
				$user_id = $notificationvar['user_id'];
				$company_id = $notificationvar['company_id'];
				$email_notification_id = $notificationvar['email_notification_id'];
		
				$ical_notification_query = "INSERT INTO ical_notification(course_id, class_id, schedule_id, user_id, company_id, email_id, timezone_name, dtstart, dtend, cal_uid) VALUES ('{$course_id}', '{$class_id}', '{$schedule_id}', '{$user_id}', '{$company_id}', '{$toemailid}', '{$timezone_name}', '{$dtstart}', '{$dtend}', '{$cal_uid}' ) ";
				DB::insert($ical_notification_query);
		
				if($notifiid!="" && $notifiid=="7"){					
					$sql_query = "UPDATE classroom_email SET email_notification_id='{$email_notification_id}', send_status='yes' WHERE course_id = '{$course_id}' AND class_id = '{$class_id}' AND user_id = '{$user_id}' AND company_id = '{$company_id}' ";
					DB::update($sql_query);
					//$db->query($sql_query);
				}
			}
		}
		catch (Exception $e) {
			
		}
		
	}
	
	public static function getIcalDTStartDTEnd($notificationvar){
	
		try{
			$timezone_name = trim($notificationvar['timezone_regions']);
			date_default_timezone_set("$timezone_name");
			
			$tzname = trim($notificationvar['timezone_name']);
			$timezone_dst = trim($notificationvar['timezone_dst']);
			$utc_offset = trim($notificationvar['utc_offset']);
			$utc_offset_dst = trim($notificationvar['utc_offset_dst']);

			$st_time_hr = sprintf("%02s",$notificationvar['start_time_hr']);
			$st_time_min = sprintf("%02s",$notificationvar['start_time_min']);
			$st_time_hr_min_sec = $st_time_hr.":".$st_time_min.":00 ".$notificationvar['timeformat'];
			
			$date = new \Carbon\Carbon($notificationvar['trdate'].' '.$st_time_hr_min_sec); 
			$date->setTimezone($timezone_name); 
			$meetingstartstamp = $date->getTimestamp(); 
			$dtvalue =$date->toDateTimeString();
			$dtstart=$date->parse($dtvalue)->format('Ymd\THis');
		
			
		    $dureation_hrs=sprintf("%02s",$notificationvar['dureation_hrs']);
			$dureation_min=sprintf("%02s",$notificationvar['dureation_min']);
			$dureation_sec="00";
			$cenvertedTime = date('Y-m-d H:i:s',strtotime("+$dureation_hrs hour +$dureation_min minutes ",strtotime($dtvalue)));
			
			$date1 = new \Carbon\Carbon($cenvertedTime); 
			$date1->setTimezone($timezone_name); 
			$dtvalue1 =$date1->toDateTimeString();
			$dtend=$date1->parse($dtvalue1)->format('Ymd\THis');
			
			return "$timezone_name||$tzname||$timezone_dst||$utc_offset||$utc_offset_dst||$meetingstartstamp||$dtstart||$dtend";
		}catch(Excetion $e){
			
		}
		
	}
	
	 private static  function icalMail($ical, $filename, $from_address, $from_name, $toemailid, $emailname, $subject, $emailcontent){
	
		try {
			

			$default_email = CommonFunctions::getCompanyDefaultEmails();
			if($default_email!='blank'){
				$toemailid = $default_email;
			}
			
		    $toemailid = 'siddharth.kushwah@innverse.com';
			/* $attach = new Zend_Mime_Part($ical);
			$attach->type = 'text/calendar';
			$attach->disposition = Zend_Mime::DISPOSITION_INLINE;
			$attach->encoding = Zend_Mime::ENCODING_8BIT;
			$attach->filename = $filename;

			//Create email
			$email = new Zend_Mail('UTF-8');
			$email->setFrom($from_address, $from_name);
			$email->addTo($toemailid, $emailname);
			$email->setSubject($subject);
			$email->setBodyHtml($emailcontent);	
			$email->addAttachment($attach);
			$email->send();	
			 */
			header("text/calendar");
			file_put_contents($filename, $ical);
			
			Mail::send([], [], function ($message) use($toemailid, $emailname,$subject,$from_address, $from_name,$emailcontent,$filename){

			$message->from($from_address, $from_name);
			$message->to($toemailid, $emailname);
			$message->subject($subject);
			$message->setBody($emailcontent, 'text/html'); // for HTML rich messages
			$message->attach($filename, array('mime' => "text/calendar"));
			});
			return 'yes';
			
		}
		catch (Exception $e) {
			
		}
	}
	
	public static function updateCalendarMeetingEmail($from_address, $from_name, $subject, $toemailid, $emailcontent, $emailname, $notifiid="", $notificationvar)
	{
		//execution time 
		set_time_limit(0);
		
		
		$emailname = ucwords($emailname);
		
		$from_name = ucwords($from_name);
		
		$location = $notificationvar['location'];
		
		list($timezone_name, $tzname, $timezone_dst, $utc_offset, $utc_offset_dst, $meetingstartstamp, $dtstart, $dtend) = explode("||",Sendmail::getIcalDTStartDTEnd($notificationvar));
		
		$icalcontent = addslashes(preg_replace("/[\\n\\r]+/", "", $emailcontent));		
		$item_id = $notificationvar["item_id"];
		$class_id = $notificationvar["class_id"];
		$schedule_id = $notificationvar["schedule_id"];
		$user_id = $notificationvar["user_id"];
		$company_id = $notificationvar["company_id"];
	
	
		$ical_res = IcalNotification::where('item_id','=',$item_id)->where('class_id','=',$class_id)->where('schedule_id','=',$schedule_id)->where('user_id','=',$user_id)->where('company_id','=',$company_id)->first();
	
		if(empty($ical_res)){
			$cal_uid = DATE('Ymd').'T'.DATE('His')."-".RAND()."@mylms.com";
			$email_notification_id = $notificationvar['email_notification_id'];
			
			$data=array(
				'item_id'=>$item_id,
				'class_id'=>$class_id,
				'schedule_id'=>$schedule_id,
				'user_id'=>$user_id,
				'company_id'=>$company_id,
				'email_id'=>$toemailid,
				'timezone_name'=>$timezone_name,
				'dtstart'=>$dtstart,
				'dtend'=>$dtend,
				'cal_uid'=>$cal_uid
			);
			IcalNotification::insert($data);
			
			$ical_res = IcalNotification::where('item_id','=',$item_id)->where('class_id','=',$class_id)->where('schedule_id','=',$schedule_id)->where('user_id','=',$user_id)->where('company_id','=',$company_id)->first();
			
			$cal_uid = DATE('Ymd').'T'.DATE('His')."-".RAND()."@mylms.com";
			$email_notification_id = $notificationvar['email_notification_id'];
			
			$data=array(
				'timezone_name'=>$timezone_name,
				'dtstart'=>$dtstart,
				'dtend'=>$dtend
			);
		
			 IcalNotification::where('item_id','=',$item_id)->where('class_id','=',$class_id)->where('schedule_id','=',$schedule_id)->where('user_id','=',$user_id)->where('company_id','=',$company_id)->update($data);
			 
			
			$ical_res = IcalNotification::where('item_id','=',$item_id)->where('class_id','=',$class_id)->where('schedule_id','=',$schedule_id)->where('user_id','=',$user_id)->where('company_id','=',$company_id)->first();
		}
		if($ical_res){
			$cal_uid = $ical_res->cal_uid;
			$ical = 'BEGIN:VCALENDAR' . "\r\n" .
			'VERSION:2.0' . "\r\n" .			
			'PRODID://Microsoft Corporation//Outlook 11.0 MIMEDIR//EN' . "\r\n" .
			'METHOD:REFRESH' . "\r\n" .
			'BEGIN:VTIMEZONE' . "\r\n" .
			'TZID:'.$timezone_name . "\r\n" .
			'BEGIN:STANDARD' . "\r\n" .
			'DTSTART:19701101T020000' . "\r\n" .
			'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11' . "\r\n" .
			'TZOFFSETFROM:'.$utc_offset_dst . "\r\n" .
			'TZOFFSETTO:'.$utc_offset . "\r\n" .
			'TZNAME:'.$tzname . "\r\n" .
			'END:STANDARD' . "\r\n" .
			'BEGIN:DAYLIGHT' . "\r\n" .
			'DTSTART:19700308T020000' . "\r\n" .
			'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3' . "\r\n" .
			'TZOFFSETFROM:'.$utc_offset . "\r\n" .
			'TZOFFSETTO:'.$utc_offset_dst . "\r\n" .
			'TZNAME:'.$timezone_dst . "\r\n" .
			'END:DAYLIGHT' . "\r\n" .
			'END:VTIMEZONE' . "\r\n" .	
			'BEGIN:VEVENT' . "\r\n" .
			'STATUS:TENTATIVE' . "\r\n" .
			'ORGANIZER;CN="'.$from_name.'":MAILTO:'.$from_address. "\r\n" .
			'ATTENDEE;CN="'.$emailname.'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:'.$toemailid. "\r\n" .
			'LAST-MODIFIED:' . date("Ymd\TGis",$meetingstartstamp) . "\r\n" .
			'UID:'.$cal_uid."\r\n" .
			'DTSTAMP:'.date("Ymd\TGis",$meetingstartstamp). "\r\n" .
			'DTSTART:'.$dtstart. "\r\n" .
			'DTEND:'.$dtend. "\r\n" .
			'TRANSP:OPAQUE'. "\r\n" .
			'SEQUENCE:1'. "\r\n" .
			'SUMMARY:' .  " $subject \r\n" .
			'X-ALT-DESC;FMTTYPE=text/html: ' . " $icalcontent \r\n".
			'LOCATION:' . " $location \r\n" .
			'CLASS:PUBLIC'. "\r\n" .
			'X-MICROSOFT-CDO-IMPORTANCE:1'. "\r\n" .
			'PRIORITY:5'. "\r\n" .
			'BEGIN:VALARM' . "\r\n" .
			'TRIGGER:-PT15M' . "\r\n" .
			'ACTION:DISPLAY' . "\r\n" .
			'DESCRIPTION:Reminder' . "\r\n" .
			'END:VALARM' . "\r\n" .
			'END:VEVENT'. "\r\n" .
			'END:VCALENDAR'. "\r\n";
			
			
			Sendmail::icalMail($ical, 'update_calendar_'.MD5(TIME()).'.ics', $from_address, $from_name, $toemailid, $emailname, $subject, $emailcontent);
		}
		
	}
	
	public static function usertrainingcatelogforsubmittraining($usertrainingid) 
	{
		$result = UsertrainingCatalog::leftjoin('class_master as cm',function($join){
			                         $join->on('cm.item_id','=','user_training_catalog.item_id')
									      ->where('cm.class_id','=','user_training_catalog.session_id')
									      ->where('cm.is_active','=',1);
		})
		                    ->join('user_master','user_training_catalog.user_id','=','user_master.user_id')
		                    ->join('user_supervisor','user_supervisor.user_id','=','user_master.user_id')
		                    ->join('user_transcript','user_training_catalog.item_id','=','user_transcript.item_id')
		                    ->join('company_configuration','user_training_catalog.company_id','=','company_configuration.company_config_id')
		                    ->join('app_configuration','user_training_catalog.company_id','=','app_configuration.company_id')
							->where('user_training_catalog.user_training_id',$usertrainingid)
							->select('user_training_catalog.*','cm.class_id','cm.class_name','user_master.user_name','user_master.email_id','user_supervisor.supervisor_id','company_configuration.company_name','app_configuration.app_url','user_transcript.completion_date')
							->get();
		return $result;					
							
	}
	
	public static function getcreditval($usertrainingid)
	{
		$result = UserTranscript::whereuserTrainingId($usertrainingid)->select('credit_value')->get();
		return $result;
		
	}
	
	public static function getLatestScheduleStartDate($classid, $compid)
	{
		$result=SessionScedule::leftjoin('user_master AS um','session_scedule.instructor_id','=','um.user_id')
		              ->leftjoin('class_timezones AS ctz','session_scedule.time_zone','=','ctz.class_timezone_id')
					  ->where('session_scedule.class_id',$classid)
					  ->where('session_scedule.company_id',$compid)
					  ->select('session_scedule.schedule_id',DB::raw('min(session_scedule.dayid) AS dayid'),'session_scedule.ses_length_hr','session_scedule.ses_length_min','session_scedule.start_time_hr','session_scedule.start_time_min','session_scedule.timeformat','session_scedule.location','um.user_name','ctz.timezone_name')
					  ->get();
       return $result;						  
	}
	
}