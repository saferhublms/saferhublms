<?php
namespace App\Http\Traits;

use App\Models\ClassSchedule;
use Mail,DB;

trait ClassSchedules{
 
           public static function getscheduleid($classid, $recursivedate, $compid)
	       {
			   $result=ClassSchedule::join('class_master',function($join) use($compid){
				                 $join->on('class_schedule.class_id','=','class_master.class_id')
								      ->where('class_master.status_id',1)
								      ->where('class_master.is_active',1)
								      ->where('class_master.company_id',$compid);
			   })
			                         ->where('class_schedule.company_id',$compid)
			                         ->where('class_schedule.is_active',1)
			                         ->where('class_schedule.is_deleted',0)
			                         ->where('class_schedule.class_id',$classid)
			                         ->where('class_schedule.recursive_date',$recursivedate)
									 ->select('class_schedule.*','class_master.class_name')
									 ->get();
									 return $result;
		   }
		   
		   
		   public static function getClsscheduleid($classid, $compid)
			{
				$result=ClassSchedule::join('class_master',function($join) use($compid){
				                 $join->on('class_schedule.class_id','=','class_master.class_id')
								      ->where('class_master.status_id',1)
								      ->where('class_master.is_active',1)
								      ->where('class_master.company_id',$compid);
			   })
			                         ->where('class_schedule.company_id',$compid)
									 ->where('class_schedule.is_active',1)
			                         ->where('class_schedule.is_deleted',0)
			                         ->where('class_schedule.class_id',$classid)
									 ->select('class_schedule.*','class_master.class_name')
									 ->get();
									
				return $result;					 
			}

}