<?php
namespace App\Http\Traits;
use App\Models\TrainingProgram;
use App\Models\ItemMasterTrainingcategory;
use App\Models\TargetAudience;
use App\Models\UserMaster;
use App\Models\HandoutsMaster;
use App\Models\UserTranscript;
use App\Models\ItemMaster;
use App\Models\UploadedFile;
use App\Models\UsertrainingCatalog;
use App\Http\Traits\Trainingcatalog;
use App\Http\Traits\Admin;
use Mail,DB;

trait Trainingprograms{
	
	/**
     * fetchdatesforclass() - Method to get data schedule for training program
     * 
	 * Created By: Rishabh Anand
	 * Created Date: 27 December, 2017
     */
	 
	public static function fetchdatesforclass($classid, $itemid)
	{
		$compid=1;
		$result=TrainingProgram::join('session_scedule',function($join) use($compid,$classid){
			                          $join->on('session_scedule.class_id','=','training_program.class_id')
									       ->where('session_scedule.company_id',$compid)
									       ->where('session_scedule.is_active',1);
		})
		                       ->join('class_master','class_master.class_id','=','session_scedule.class_id') 
							   ->join('item_master',function($join) use($compid,$itemid){
								      $join->on('item_master.item_id','=','training_program.item_id')
									       ->where('item_master.company_id',$compid);
							   })
							   ->join('training_type','training_type.training_type_id','=','training_program.training_type_id')
							   ->where('training_program.class_id',$classid)
							   ->where('training_program.training_title','')
							   ->where('training_program.item_id',$itemid)
							   ->where(function($query){
								   $query->where('training_type.training_type_id',2)
								         ->orWhere('training_type.training_type_id',3)  ;
							   })
							   ->select(DB::raw('max(dayid) as enddate'),DB::raw('min(dayid) as startdate'),'session_scedule.location','session_scedule.start_time_hr','session_scedule.start_time_min','session_scedule.timeformat','session_scedule.time_zone','session_scedule.dayid', 'session_scedule.ses_length_min','session_scedule.ses_length_hr','session_scedule.instructor_id','class_master.maximum_seat')
							   ->get();
							  return $result;
	}
	
	
	
	public static function getSelectedTrainingCategoryforelearning($id,$compid)
	{
		
		$category=ItemMasterTrainingcategory::join('training_category',function($join) use($compid){
								      $join->on('training_category.training_category_id','=','item_master_trainingcategory.training_category_id')
									   ->where('training_category.company_id',$compid)
									   ->where('training_category.is_delete',0)
									   ->where('training_category.is_active',1);
		})
		->where('item_master_trainingcategory.item_id','=',$id)
		->where('item_master_trainingcategory.company_id','=',$compid)
		->select(DB::raw('GROUP_CONCAT(distinct training_category.category_name) as category_name'))->first();
		
		return $category->category_name;
	
	}
   
   
  
	
	public static function checkIsallaudience($compid, $itemid) 
	{
        $result = TargetAudience::where('company_id','=',$compid)->where('item_id','=',$itemid)->where('is_all_audience','=',0)->first();
        return $result;
	}
	
	public static function targetAudienceDescription($itemid,$compid) 
	{
		 $result = TargetAudience::where('company_id','=',$compid)->where('item_id','=',$itemid)->where('is_active','=',1)->select('target_audience.description')->get();
        return $result;
	}
	
	public static function checkhandout($item_id, $compid) {
		$results = HandoutsMaster::where('company_id','=',$compid)->where('item_id','=',$item_id)->where('status','=',1)->select(DB::raw('COUNT(handout_title) as hnd'))->first();
		if($results->hnd > 0)
			return 1;
		else
			return 0;
	
	}
	
	
    
	public static function blendedUtcEntry($blendedprogid,$userid,$compid) {
		$results =UsertrainingCatalog::where('blended_program_id','=',$blendedprogid)
		->where('company_id','=',$compid)->select('user_training_id')->get()->toArray();
	
		if(count($results)<1)
			{
				$data = array(
					'user_id'               => $userid,
					'program_id'          	=> 0,
					'is_enroll'             => 1,
					'is_approved'           => 1,
					'training_type_id'      => 4,
					'user_program_type_id'	=> 0,
					'item_id'           	=> 0,
					'company_id'            => $compid,
					'training_status_id'    => 3,
					'blended_program_id'    => $blendedprogid,
					'learning_plan_id'    	=> 0,
					'session_id'       		=> '',
					'last_modfied_by'     	=> $userid,
					'created_date'    		=> date('Y-m-d H-i-s'),
				);
				UsertrainingCatalog::insert($data);
			}
    }
	
	
	
	

	
	

	
	public static function getClassroomdcatalog($compid,$userid,$roleid)
	{
		return Trainingprograms::getClassroomTrainingPrograms($compid, $userid, $roleid,'','','');
	}
	
	public static function getClassroomdcatalogFilter($compid,$userid,$roleid,$cattype,$traingTypeId='')
	{
		return Trainingprograms::getClassroomTrainingPrograms($compid, $userid, $roleid,$cattype,$traingTypeId,'');
	}
	
	
	
	public static function getClassroomTrainingPrograms($compid, $userid, $roleid,$cattype,$traingTypeId='',$callfrom){
		$traingTypeId =($traingTypeId == '') ? '2,3' : $traingTypeId;
		$strQuery2 = "";
		$suroleid = $roleid;
		if($cattype != '')
			$strQuery2 = "and imtc.training_category_id = '$cattype'";
		
		$strQuery5 = $strQuery4 = $strQuery3 = $strQuery3 = "";
		if($traingTypeId == 3){
			
			$strQuery5	= "and cm.item_type_id in ($traingTypeId)";
			}else {
			$strQuery5	= "and im.training_type_id in ($traingTypeId)";
		}
		if($callfrom == 'assignment'){
			if($roleid==2){
				$userrolearray =Trainingprogram::getSupervisorSupervisorId($compid,$userid);	
				$userid = $userrolearray[0];
				$suroleid = $userrolearray[1];
			}
			
			$compSettings = Admin::checkduplicatedataforcompsetting($compid);
			
			if(($compSettings[0]->supervisor_training_view == 1) && ($roleid==2)) {
				$roleid	= 1;
				$statusId = " cm.status_id = 1";
			}else{
				if($suroleid != 1){
					$strQuery3 = "join sub_admin_assigned_category subc on (imtc.training_category_id = subc.category_id)";
					$strQuery4 = "and user_id = $userid";
					$statusId = " cm.status_id != 0 ";
				}else{
					$roleid	= 1;
					$statusId = " cm.status_id = 1";
				}
			}
		}else
			$statusId = " cm.status_id = 1";
		$item = array();
		if($roleid != 1){
			$checkTargetAudience="select distinct ta.job_group_id,im.item_id ,ta.type_name,im.item_type_id 
			from item_master as im  
			left join target_audience as ta on ta.item_id=im.item_id and ta.is_active=1 and ta.company_id=:compid1 and ta.job_group_id != 0 
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id  and imtc.company_id=:compid2
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) ".$strQuery3." and tct.company_id=:compid3
			join class_master cm on cm.item_id=im.item_id and cm.company_id=:compid4
			where im.is_active=1 and im.company_id=:compid5 and im.item_type_id !=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 $strQuery4 $strQuery5";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
			$item['compid4'] = $compid;
			$item['compid5'] = $compid;
		}else{
		 $checkTargetAudience="select im.item_id , 0 as job_group_id ,'' as type_name,im.item_type_id from item_master as im  
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id=:compid1
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id=:compid2
			join class_master cm on cm.item_id=im.item_id and cm.company_id=:compid3
			where im.is_active=1 and im.company_id=:compid4 and im.item_type_id !=4 and tct.is_delete=0 and tct.is_active=1 ".$strQuery2." $strQuery5";	
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
			$item['compid4'] = $compid;
		}
		
              $target = DB::select(DB::raw($checkTargetAudience),$item);
			 
			  $itemresult=array();
		      $itemArr=array();
		      $uniqueClassId = array();
			  $param = array();
			  if(count($target)>0){
				  foreach($target as $key => $value){
					  $item_type_id=$value->item_type_id;
				      $item_id=$value->item_id;
				      $type_name=$value->type_name;
				      $itemid = $item_id;
				      if($type_name=='job'){
						  $itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
					cs.expiry_date, im.item_id, im.item_name,im.training_code, im.item_type_id, cm.item_type_id as training_type_id, im.delivery_type_id, im.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM item_master im 
					join class_master cm on im.item_id = cm.item_id and $statusId and cm.company_id=:compid1
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid2 and ta.is_active = 1
					join user_jobtitle as uj on uj.job_id = ta.job_group_id and uj.user_id=:userid and uj.is_active=1
					join job_title_group as jtg on jtg.job_id=uj.job_id	and jtg.is_active = 1 and jtg.company_id=:compid3	
					join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id=:compid4	
					join training_type ty ON ty.training_type_id  = cm.item_type_id
					join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = :compid5 and ssd.is_active=1  group by ssd.class_id) ss ON cs.class_id = ss.class_id 
					left join training_program tp on tp.item_id = im.item_id and cm.class_id = tp.class_id and tp.company_id=:compid6 and tp.is_active = 1
					where im.company_id = :compid7 and im.item_type_id!=4 and cm.item_type_id in ($traingTypeId) and im.item_id=:itemid and cs.is_deleted = 0  group by im.item_id, cm.class_id";
					
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['compid5'] = $compid;
					$param['compid6'] = $compid;
					$param['compid7'] = $compid;
					$param['userid'] = $userid;
					$param['itemid'] = $itemid;
					$res=  DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['class_id'],$uniqueClassId)){
							$uniqueClassId[] = $res[0]['class_id'];
							$itemresult[] = $res;
						}
					}
						  
				   }else if($type_name=='group'){
					   $itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
					cs.expiry_date, im.item_id, im.item_name,im.training_code, im.item_type_id, cm.item_type_id as training_type_id ,im.delivery_type_id, im.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM item_master im 
					join class_master cm on im.item_id = cm.item_id and $statusId and cm.company_id=:compid1
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid2 and ta.is_active = 1
					join user_group as ugp on ugp.group_id = ta.job_group_id and ugp.user_id=:userid and ugp.is_active=1 	
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0  and gm.company_id=:compid3
					join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id=:compid4
					join training_type ty ON ty.training_type_id  = cm.item_type_id
					join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = :compid5 and ssd.is_active=1  group by ssd.class_id) ss ON cs.class_id = ss.class_id 
					left join training_program tp on tp.item_id = im.item_id and cm.class_id = tp.class_id and tp.company_id=:compid6 and tp.is_active = 1
					where im.company_id = :compid7 and im.item_id=:itemid and cm.item_type_id in ($traingTypeId) and im.item_type_id != 4 and im.training_type_id in (2,3) and  cs.is_deleted = 0 group by im.item_id, cm.class_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['compid5'] = $compid;
					$param['compid6'] = $compid;
					$param['compid7'] = $compid;
					$param['userid'] = $userid;
					$param['itemid'] = $itemid;
					$res=  DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['class_id'],$uniqueClassId)){
							$uniqueClassId[] = $res[0]['class_id'];
							$itemresult[] = $res;
						}
					}
				   }else{
					    $itemsql ="SELECT cm.class_name, cm.class_id,cm.maximum_seat as max_seat,im.item_id, im.item_name,im.training_code, 
					im.item_type_id, cm.item_type_id as training_type_id, im.delivery_type_id, im.company_id,tp.training_program_id, im.published_date,im.credit_value, ty.training_type as itemtype,cm.start_date_class as recursive_date
					FROM item_master im 
					join class_master cm on im.item_id = cm.item_id and $statusId and cm.company_id=:compid1
					join training_type ty ON ty.training_type_id = cm.item_type_id
					left join training_program tp on tp.item_id = im.item_id and cm.class_id = tp.class_id and tp.company_id=:compid2 and tp.is_active = 1
					where im.company_id = :compid3 and im.item_id=:itemid and cm.item_type_id in ($traingTypeId) and im.item_type_id != 4 and  cm.is_active = 1 and im.training_type_id in (2,3) and DATE_FORMAT(cm.start_date_class,'%Y-%m-%d') >= CURRENT_DATE() 
					group by im.item_id, cm.class_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['itemid'] = $itemid;
					$res=  DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['class_id'],$uniqueClassId)){
							$uniqueClassId[] = $res[0]['class_id'];
							$itemresult[] = $res;
						}
					}
				   }
				  }
			  }
			  $itemresult = json_decode(json_encode($itemresult),true);
		

		return $itemresult;
	}
	
	public static function getblendedcatalog($compid,$userid,$roleid)
	{
		return TrainingPrograms::getblendedTrainingPrograms($compid,$userid,$roleid,'','');
	}
	public static function getblendeddataFilter($compid,$userid,$roleid,$cattype)
	{
		return TrainingPrograms::getblendedTrainingPrograms($compid,$userid,$roleid,$cattype, '');
	}
	
	public static function getblendedTrainingPrograms($compid,$userid,$roleid,$cattype,$callfrom){
		$strQuery2 = "";
		$suroleid = $roleid;
		if($cattype != '')
			$strQuery2 = " and bptc.training_category_id = '$cattype' ";
		$strQuery4 = $strQuery3 =  $statusId = "";
		if($callfrom == 'assignment'){
			if($roleid==2){
				$userrolearray =Trainingprogram::getSupervisorSupervisorId($compid,$userid);	
				$userid = $userrolearray[0];
				$suroleid = $userrolearray[1];
			}
			$compSettings = Admin::checkduplicatedataforcompsetting($compid);
			if(($compSettings[0]->supervisor_training_view == 1) && ($roleid==2)) {
				$roleid	= 1;
				$statusId = " bpi.status_id = 1 ";
			}else{	
				if($suroleid != 1){
					$strQuery3 = "join sub_admin_assigned_category subc on (bptc.training_category_id = subc.category_id)";
					$strQuery4 = "and subc.user_id = $userid";
					$statusId = " bpi.status_id != 0 ";
				}else{
					$roleid	= 1;
					$statusId = " bpi.status_id = 1 ";
				}
			}
		}else
			$statusId = " bpi.status_id = 1 ";
		   $blendedIdArr=array();
		   $item = array();
		   if($roleid != 1){
			   $checkTargetAudience="select distinct bta.job_group_id,bpi.blended_item_id ,bta.type_name 
			from blended_program_items as bpi  
			left join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compid1 and bta.job_group_id!=0 
			left join blended_program_training_category bptc on bpi.blended_item_id = bptc.blended_program_id
			left join training_category tct ON (bptc.training_category_id = tct.training_category_id) ".$strQuery3." and tct.company_id=:compid2
			where bpi.is_active=1 and bpi.company_id=:compid3 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 $strQuery4";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
		   }else{
			$checkTargetAudience="select bpi.blended_item_id , 0 as job_group_id ,'' as type_name 
			from blended_program_items as bpi  
			left join blended_program_training_category bptc on bpi.blended_item_id = bptc.blended_program_id
			left join training_category tct ON (bptc.training_category_id = tct.training_category_id) and tct.company_id=:compid1
			where bpi.is_active=1 and bpi.company_id=:compid2 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 group by bpi.blended_item_id";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			
		}
		
		$target = DB::select(DB::raw($checkTargetAudience),$item);
		$itemresult=array();
		$param = array();
		if(count($target)>0){
			foreach($target as $key => $value){
				$blended_item_id	=	$value->blended_item_id;
				$type_name			=	$value->type_name;
				
				if($type_name=='job'){
					$jobTitleBasedItem="select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and $statusId and bpi.company_id=:compid1
					join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compid2 and bta.job_group_id!=0 
					join user_jobtitle as uj on uj.job_id = bta.job_group_id and uj.user_id=:userid and uj.is_active=1  
					join job_title_group as jtg on jtg.job_id=uj.job_id and jtg.is_active = 1 and jtg.company_id=:compid3
					where tp.company_id = :compid4 and tp.blended_program_id=:blended_item_id1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blended_item_id2 and bpi.is_active='1' group by bpi.blended_item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['userid'] = $userid;
					$param['blended_item_id1'] = $blended_item_id;
					$param['blended_item_id2'] = $blended_item_id;
					$res  = DB::select(DB::raw($jobTitleBasedItem),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['blended_item_id'],$blendedIdArr)){
							$blendedIdArr[] = $res[0]['blended_item_id'];
							$itemresult[] = $res;
						}
					}
				}else if($type_name=='group'){
					$groupBasedItem="select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and $statusId and bpi.company_id=:compid1
					join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compid2 and bta.job_group_id!=0 
					join user_group as ugp on ugp.group_id = bta.job_group_id and ugp.user_id=:userid and ugp.is_active=1   
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0 and gm.company_id=:compid3
					where tp.company_id = :compid4 and tp.blended_program_id=:blended_item_id1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blended_item_id2 and bpi.is_active='1' group by bpi.blended_item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['compid4'] = $compid;
					$param['userid'] = $userid;
					$param['blended_item_id1'] = $blended_item_id;
					$param['blended_item_id2'] = $blended_item_id;
					$res  = DB::select(DB::raw($groupBasedItem),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['blended_item_id'],$blendedIdArr)){
							$blendedIdArr[] = $res[0]['blended_item_id'];
							$itemresult[] = $res;
						}
					}
				}else {
					$itemsql = "select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and $statusId and bpi.company_id=:compid1
					where tp.company_id = :compid2 and tp.blended_program_id=:blended_item_id1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blended_item_id2 and bpi.is_active='1' group by bpi.blended_item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['blended_item_id1'] = $blended_item_id;
					$param['blended_item_id2'] = $blended_item_id;
					$res  = DB::select(DB::raw($itemsql),$param);
					$res = json_decode(json_encode($res),true);
					if(count(@$res)>0){
						if(!in_array($res[0]['blended_item_id'],$blendedIdArr)){
							$blendedIdArr[] = $res[0]['blended_item_id'];
							$itemresult[] = $res;
						}
					}
				}
			}
		}
		$itemresult = json_decode(json_encode($itemresult),true);
		return $itemresult;
	}
	
	
	public static function getelearningdata($compid,$userid,$roleid)
	{
		return TrainingPrograms::getElearningTrainingPrograms($compid,$userid,$roleid, '','');
	}
	public static function getelearningdataFilter($compid,$userid,$roleid,$cattype)
	{
		return TrainingPrograms::getElearningTrainingPrograms($compid,$userid,$roleid,$cattype,'');
	}
	
	public static function getElearningTrainingPrograms($compid,$userid,$roleid,$cattype,$callfrom)
	{
		$strQuery2 = "";
		$su_roleid = $roleid;
		if($cattype != '')
			$strQuery2 = " and imtc.training_category_id = '$cattype'";		
		$strQuery4 = $strQuery3 =  $statusId = "";
		if($callfrom == 'assignment'){
			if($roleid==2){
				$userrolearray =Trainingprogram::getSupervisorSupervisorId($compid,$userid);	
				$su_userid = $userrolearray[0];
				$su_roleid = $userrolearray[1];
			}else{
				$su_userid = $userid;
				}
				
		   $compSettings = Admin::checkduplicatedataforcompsetting($compid);
            if(($compSettings[0]->supervisor_training_view == 1) && ($roleid==2)) { 
				$roleid	= 1;
				$statusId = " im.status_id = 1 ";
			}else{	
                if($su_roleid != 1){
					$strQuery3 = "join sub_admin_assigned_category subc on (imtc.training_category_id = subc.category_id)";
					$strQuery4 = "and user_id = $su_userid";
					$statusId = " im.status_id != 0 ";
				}else{ 
					$roleid	= 1;
					$statusId = " im.status_id = 1 ";
				}			
			}   
		}else
			$statusId = " im.status_id = 1 ";
		$item = array();
		if($roleid != 1){
			$checkTargetAudience="select ta.job_group_id,im.item_id ,ta.type_name,im.item_type_id 
			from item_master as im  
			left join target_audience as ta on ta.item_id=im.item_id and ta.is_active=1 and ta.company_id=:compid1 and ta.job_group_id!=0 
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id=:compid2
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) ".$strQuery3." and tct.company_id=:compid3
			where im.is_active=1 and im.training_type_id=1 and im.company_id=:compid4 and im.item_type_id !=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 $strQuery4";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
			$item['compid4'] = $compid;
		}else{
			$checkTargetAudience="select im.item_id , 0 as job_group_id ,'' as type_name,im.item_type_id from item_master as im  
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id=:compid1
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id=:compid2
			where im.is_active=1 and im.training_type_id=1 and im.company_id=:compid3 and item_type_id!=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 group by im.item_id";
			$item['compid1'] = $compid;
			$item['compid2'] = $compid;
			$item['compid3'] = $compid;
		}
		  $target = DB::select(DB::raw($checkTargetAudience),$item);
		  $itemresult=array();
		  $uniuqeItemIdArray=array();
		  if(count($target)>0){
			  foreach($target as $key => $value){
				  
				  if(is_object($value)){ 
					$item_type_id	=	$value->item_type_id;
					$item_id		=	$value->item_id;
					$type_name		=	$value->type_name;
				  }
				 else{ 
					$item_type_id	=	$value['item_type_id'];
					$item_id		=	$value['item_id'];
					$type_name		=	$value['type_name'];
				} 
				
				 $itemid=$item_id;
				 $param = array();
				 if($type_name=='job'){
					 $jobTitleBasedItem="select im.training_type_id,im.item_name as training_title,im.item_id,im.training_code,im.item_name,im.average_rating,im.credit_value,
					im.item_type_id,im.delivery_type_id,tp.blended_program_id,tp.class_id,tp.published_date,tp.training_program_id,	im.file_id,tc.training_type as itemtype from item_master as im 
					join training_type as tc on tc.training_type_id=im.training_type_id
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid1 and ta.is_active=1
					join user_jobtitle as uj on uj.job_id = ta.job_group_id and uj.user_id=:userid and uj.is_active=1  
					join job_title_group as jtg on jtg.job_id=uj.job_id and jtg.is_active = 1  and jtg.company_id=:compid2
					left join training_program as tp on im.item_id= tp.item_id and tp.is_active=1 and (tp.blended_program_id = '' or  tp.blended_program_id is  null)
					where im.company_id = :compid3 and ".$statusId." and im.item_id=:itemid 
					and im.training_type_id=1 and im.is_active='1' and im.item_type_id!=4  group by im.item_id";
					$param['compid1'] = $compid;
					$param['compid2'] = $compid;
					$param['compid3'] = $compid;
					$param['userid'] = $userid;
					$param['itemid'] = $itemid;
					$res  = DB::select(DB::raw($jobTitleBasedItem),$param);
					if(count(@$res)>0)
					{
						if(!in_array($itemid, $uniuqeItemIdArray)){
							$uniuqeItemIdArray[] = $itemid;
							$itemresult[] = $res;
						}
					}
					 
				 }else if($type_name=='group'){
					 $groupBasedItem="select im.training_type_id,im.item_name as training_title,im.item_id,im.training_code,im.item_name,im.average_rating,im.credit_value,
					im.item_type_id,im.delivery_type_id,tp.blended_program_id,tp.class_id,tp.published_date,tp.training_program_id,	im.file_id,tc.training_type as itemtype from item_master as im 
					join training_type as tc on tc.training_type_id=im.training_type_id
					join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compid1 and ta.is_active=1
					join user_group as ugp on ugp.group_id = ta.job_group_id and ugp.user_id=:userid and ugp.is_active=1   
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0 and gm.company_id=:compid2
					left join training_program as tp on im.item_id= tp.item_id and tp.is_active=1 and (tp.blended_program_id = '' or  tp.blended_program_id is  null)
					where im.company_id = :compid3 and ".$statusId." and im.item_id=:itemid 
					and im.training_type_id=1 and im.item_type_id!=4 group by im.item_id";
					 $param['compid1'] = $compid;
					 $param['compid2'] = $compid;
					 $param['compid3'] = $compid;
					 $param['userid'] = $userid;
					 $param['itemid'] = $itemid;
					 $res  = DB::select(DB::raw($groupBasedItem),$param);
					 if(count(@$res)>0){
						if(!in_array($itemid, $uniuqeItemIdArray)){
							$uniuqeItemIdArray[] = $itemid;
							$itemresult[] = $res;
						}
						
					}
				 }else {
					$itemid=$item_id;
					$itemsql = "select im.training_type_id,im.item_name as training_title,im.item_id,im.training_code,im.item_name,im.average_rating,im.credit_value,
					im.item_type_id,im.delivery_type_id,tp.blended_program_id,tp.class_id,tp.published_date,tp.training_program_id,	im.file_id,tc.training_type as itemtype from item_master as im 
					join training_type as tc on tc.training_type_id=im.training_type_id
					left join training_program as tp on im.item_id= tp.item_id and tp.is_active=1 and (tp.blended_program_id = '' or  tp.blended_program_id is  null)
					where im.company_id = '$compid' and ".$statusId." and im.item_id=$itemid and im.training_type_id=1 and im.item_type_id!=4 group by im.item_id";
					 $param['compid1'] = $compid;
					 $param['itemid'] = $itemid;
					 $res  = DB::select(DB::raw($itemsql),$param);
					 if(count(@$res)>0){
						if(!in_array($itemid, $uniuqeItemIdArray)){
							$uniuqeItemIdArray[] = $itemid;
							$itemresult[]=$res;
						}
					}
				 }
			  }
		  } 
		 $itemresult = json_decode(json_encode($itemresult),true);
		 return $itemresult;
		
	}
	
	
	public static function getSupervisorSupervisorId($compid,$userid)
	{
		$check_role_array =array(1,3,5);
		for($i=0; $i<10; $i++)
		{
			$result=UserMaster::join('user_supervisor as us',function($join) use($compid){
				                       $join->on('us.user_id','=','user_master.user_id')
									        ->where('us.is_active',1)
									        ->where('us.company_id',$compid);
									})
			                       ->where('user_master.user_id',$userid)
			                       ->where('user_master.company_id',$compid)
								   ->get()->toArray();
			
			if(count($result)>0)
			{				
				if(!in_array($result[0]['role_id'],$check_role_array)){										
					$userid =$result[0]['supervisor_id'];
					$role_id =$result[0]['role_id'];
				}else{
					$userid =$result[0]['user_id'];
					$role_id =$result[0]['role_id'];
				}				
			}				
		}	
		return array($userid,$role_id);
	}
	
	public static function geteLearningAdvanceSearch($userid, $roleid, $compid, $categoryid, $trainingTitle, $instructor, $cattype, $creditvalue, $cost, $dateFrom,$dateTo, $searchedFrom='', $status=0) 
	{
		$strQuery2 = "";
		if($cattype != 0)
			$strQuery2 = " and imtc.training_category_id = '$cattype'";
		     $var_arr = array();
		     $array=array();
			 if($roleid != 1){
					$checkTargetAudience="select distinct ta.job_group_id,im.item_id ,ta.type_name,im.training_type_id,im.item_type_id 
					from item_master as im  
					left join target_audience as ta on ta.item_id=im.item_id and ta.is_active=1 and ta.company_id=:compId1 and ta.job_group_id!=0 
					left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id =:compId2
					left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id =:compId3
					where im.is_active=1 and im.training_type_id=1 and im.company_id =:compId4 and im.item_type_id !=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1";
					$var_arr['compId1']=$compid;
					$var_arr['compId2']=$compid;
					$var_arr['compId3']=$compid;
					$var_arr['compId4']=$compid;
		
			 }else{
					$checkTargetAudience="select im.item_id , 0 as job_group_id ,'' as type_name,im.item_type_id,im.training_type_id from item_master as im  
					left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id =:compId1
					left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id =:compId2
					where im.is_active=1 and im.training_type_id=1 and im.company_id =:compId3 and item_type_id!=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1 ";
					$var_arr['compId1']=$compid;
					$var_arr['compId2']=$compid;
					$var_arr['compId3']=$compid;
 
			 }
			 
			 $whereCondition = '';
			 if($searchedFrom)
			{
				$whereCondition .= " and (im.item_name like '%$trainingTitle%' or im.description like '%$trainingTitle%' ) ";
				
			}
			if($trainingTitle && !$searchedFrom)
			{
				$whereCondition .= " and im.item_name like '%$trainingTitle%' ";
				
			}
			if($creditvalue && !$cost)
			{
				$whereCondition .= " and im.credit_value = :creditvalue ";
				$var_arr['creditvalue'] = $creditvalue;
			}
			if(!$creditvalue && $cost)
			{
				$whereCondition .= " and im.item_cost = :cost";
				$var_arr['cost'] = $cost;
			}
			if($creditvalue && $cost)
			{
				$whereCondition .= " and im.credit_value = :creditvalue1 ";
				$whereCondition .= " and im.item_cost = :cost1 ";
				$var_arr['creditvalue1'] = $creditvalue;
				$var_arr['cost1'] = $cost;
			}
			$whereCondition .= " group by im.item_id";
			$checkTargetAudience = $checkTargetAudience.$whereCondition;
			$target = DB::select(DB::raw($checkTargetAudience),$var_arr);
			$itemresult=array();
			$itemstatus = ' im.status_id = 1' ;
			if($status = 1)
				$itemstatus = ' im.status_id IN (1,2)' ;
			
			if(count($target)>0)
			{
				foreach($target as $key => $value)
				{
					$item_type_id	=	$value->item_type_id;
					$item_id		=	$value->item_id;
					$type_name		=	$value->type_name;
					$itemid=$item_id;
					if($type_name=='job')
					{
						$jobTitleBasedItem = "select tp.class_id,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.item_id,
						im.item_name,im.average_rating,im.credit_value,im.item_type_id,im.delivery_type_id,tp.blended_program_id,
						im.file_id,tc.training_type as itemtype,im.status_id from training_program as tp 
						join training_type as tc on tc.training_type_id=tp.training_type_id
						join item_master as im on im.item_id= tp.item_id and im.company_id =:compId1
						join target_audience as ta on ta.item_id=im.item_id and ta.company_id =:compId2 and ta.is_active=1
						join user_jobtitle as uj on uj.job_id = ta.job_group_id and uj.user_id=:userId and uj.is_active=1  
						join job_title_group as jtg on jtg.job_id=uj.job_id and jtg.is_active = 1 and jtg.company_id =:compId3
						where tp.company_id =:compId4 and ".$itemstatus."and tp.item_id=:itemId1 and tp.is_active=1 
						and tp.training_type_id=1 and tp.blended_program_id is NULL and im.item_id=:itemId2 and im.is_active='1' and im.item_type_id!=4 and im.training_type_id=1 group by im.item_id";
						$itemArr['compId1']=$compid;
						$itemArr['compId2']=$compid;
						$itemArr['compId3']=$compid;
						$itemArr['compId4']=$compid;
						$itemArr['userId']=$userid;
						$itemArr['itemId1']=$itemid;
						$itemArr['itemId2']=$itemid;
						
						$res  = DB::select(DB::raw($jobTitleBasedItem),$itemArr);
						if(count(@$res)>0)
						{
							$itemresult[] = $res;
						}
					}else if($type_name=='group') {
						$groupBasedItem = "select tp.class_id,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.item_id,
						im.item_name,im.average_rating,im.credit_value,im.item_type_id,im.delivery_type_id,tp.blended_program_id,
						im.file_id,tc.training_type as itemtype,im.status_id from training_program as tp 
						join training_type as tc on tc.training_type_id=tp.training_type_id 
						join item_master as im on im.item_id=:itemId1 and im.is_active='1' and im.company_id =:compId1
						join target_audience as ta on ta.item_id=im.item_id and ta.company_id =:compId2 and ta.is_active=1
						join user_group as ugp on ugp.group_id = ta.job_group_id and ugp.user_id=:userId and ugp.is_active=1   
						join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete = 0 and gm.company_id =:compId3
						where tp.training_type_id=1 and tp.company_id =:compId4 and ".$itemstatus." and tp.item_id=:itemId2 and tp.is_active=1 
						and im.training_type_id=1 and tp.blended_program_id is NULL and im.item_type_id!=4 group by im.item_id";
						$itemArr['compId1']=$compid;
						$itemArr['compId2']=$compid;
						$itemArr['compId3']=$compid;
						$itemArr['compId4']=$compid;
						$itemArr['userId']=$userid;
						$itemArr['itemId1']=$itemid;
						$itemArr['itemId2']=$itemid;
						
						$res  = DB::select(DB::raw($groupBasedItem),$itemArr);
						if(count(@$res)>0)
						{
							$itemresult[] = $res;
						}
					}
					else 
					{
						$itemsql = "select tp.class_id,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.item_id,im.item_name,im.average_rating,im.credit_value,im.item_type_id,im.delivery_type_id,tp.blended_program_id,im.file_id,tc.training_type as itemtype,im.status_id  from training_program as tp join item_master as im on im.item_id = tp.item_id and im.is_active=1 and im.company_id =:compId1 join training_type as tc on tc.training_type_id=tp.training_type_id where tp.training_type_id=1 and im.training_type_id=1 and tp.company_id =:compId2 and ".$itemstatus." and tp.is_active=1 and im.item_id =:itemId1 and im.item_type_id!=4 group by im.item_id";
						$itemArr['compId1']=$compid;
						$itemArr['compId2']=$compid;
						$itemArr['itemId1']=$itemid;
						
						
						$res  = DB::select(DB::raw($itemsql),$itemArr);
						if(count(@$res)>0)
						{
							$itemresult[] = $res;
						}
					}
				}
			}
			return $itemresult;
			
	}
	
	public static function getCoursesAdvanceSearch($userid, $roleid, $compid, $traingTypeId, $trainingTitle, $instructor, $category, $creditvalue, $cost, $dateFrom,$dateTo, $searchedFrom='') 
	{
		
		$traingTypeId =($traingTypeId == '') ? '2,3' : $traingTypeId;
		$strQuery2 = "";
		if($category != 0)
			$strQuery2 = "and imtc.training_category_id = '$category'";
		$instructorQry	= "";
		if($instructor != '' && $instructor != '0')
			$instructorQry .= " and ss.instructor_id = '".$instructor."'";
		$array=array();
		
		if($roleid != 1){
			$checkTargetAudience="select distinct ta.job_group_id,im.item_id ,ta.type_name,im.item_type_id,im.training_type_id 
			from item_master as im  
			left join target_audience as ta on ta.item_id=im.item_id and ta.is_active=1 and ta.company_id=$compid and ta.job_group_id!=0 and ta.company_id=$compid
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id = $compid
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id = $compid 
			join training_program tp on im.item_id=tp.item_id and tp.company_id = $compid
			join class_master cm on cm.class_id=tp.class_id and cm.company_id = $compid
			join session_scedule ss on cm.class_id=ss.class_id ".$instructorQry." and ss.company_id = $compid
			where im.is_active=1 and cm.item_type_id in ($traingTypeId) and im.company_id=$compid and im.item_type_id !=4 ".$strQuery2." and tct.is_delete=0 and tct.is_active=1";
		}else{
			$checkTargetAudience="select im.item_id , 0 as job_group_id ,'' as type_name,im.training_type_id,im.item_type_id from item_master as im  
			left join item_master_trainingcategory imtc on im.item_id = imtc.item_id and imtc.company_id = $compid
			left join training_category tct ON (imtc.training_category_id = tct.training_category_id) and tct.company_id = $compid 
			join training_program tp on im.item_id=tp.item_id and tp.company_id = $compid
			join class_master cm on cm.class_id=tp.class_id and cm.company_id = $compid
			join session_scedule ss on cm.class_id=ss.class_id ".$instructorQry." and ss.company_id = $compid
			where im.is_active=1 and cm.item_type_id in ($traingTypeId) and im.company_id=$compid and im.item_type_id !=4 and tct.is_delete=0 and tct.is_active=1 ".$strQuery2;
		}
		
		$whereCondition = '';
		$var_arr = array();
		if($searchedFrom)
		{
			$whereCondition .= " and (im.item_name like ? or im.description like ? ) ";
			$var_arr[] = "%$trainingTitle%";
			$var_arr[] = "%$trainingTitle%";
		}
		if($trainingTitle && !$searchedFrom)
		{
			$whereCondition .= " and im.item_name like ? ";
			$var_arr[] = "%$trainingTitle%";
		}
		if($creditvalue && !$cost)
		{
			$whereCondition .= " and im.credit_value = ?";
			$var_arr[] = "$creditvalue";
		}
		if(!$creditvalue && $cost)
		{
			$whereCondition .= " and im.item_cost = ?";
			$var_arr[] = "$cost";
		}
		if($creditvalue && $cost)
		{
			$whereCondition .= " and im.credit_value = ?";
			$whereCondition .= " and im.item_cost = ?";
			$var_arr[] = "$creditvalue";
			$var_arr[] = "$cost";
		}
		
		if($dateFrom!='' && $dateTo!=''){
			$whereCondition .= " and (cm.start_date_class >= $dateFrom and cm.endate_date <= $dateTo)";
			
		}elseif($dateFrom!='' && $dateTo==''){
			$whereCondition .= " and cm.start_date_class >= $dateFrom ";
			
		}elseif($dateFrom=='' && $dateTo!=''){
			$whereCondition .= " and cm.endate_date <= $dateTo ";
			
		}
		
		$whereCondition .= " group by im.item_id";
		$checkTargetAudience = $checkTargetAudience.$whereCondition;
	
		$target =DB::select(DB::raw($checkTargetAudience), $var_arr);
		$itemresult=array();
		$itemArr=array();
		if(count($target)>0)
		{
		foreach($target as $key => $value)
			{
					$item_type_id=$value->item_type_id;
					$item_id=$value->item_id;
					$type_name=$value->type_name;
					
					$itemid=$item_id;
					if($type_name=='job')
					{
						$itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
						cs.expiry_date, im.item_id, im.item_name, im.item_type_id, im.training_type_id, im.delivery_type_id, tp.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM training_program tp 
						join class_master cm on cm.class_id = tp.class_id and cm.status_id = 1 and cm.company_id = :compId1
						join item_master im on tp.item_id = im.item_id and im.item_id = cm.item_id and im.is_active=1 and im.company_id=:compId2
						join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compId3
						join user_jobtitle as uj on uj.job_id = ta.job_group_id and uj.user_id=:userId and uj.is_active=1
						join job_title_group as jtg on jtg.job_id=uj.job_id	and jtg.is_active = 1 and jtg.company_id = :compId4
						join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id = :compId5
						join training_type ty ON ty.training_type_id = tp.training_type_id
						join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = '".$compid."' group by ssd.class_id) ss ON cs.class_id = ss.class_id 
						where tp.company_id = '".$compid."' and im.item_type_id!=4 and cm.item_type_id in ($traingTypeId) and tp.is_active = 1 and tp.item_id=:itemId and cs.is_deleted = 0 ";
						$item_arr['compId1']=$compid;
						$item_arr['compId2']=$compid;
						$item_arr['compId3']=$compid;
						$item_arr['compId4']=$compid;
						$item_arr['compId5']=$compid;
						$item_arr['userId']=$userid;
						$item_arr['itemId']=$itemid;
						$res= DB::select(DB::raw($itemsql),$item_arr);
						
						if(count(@$res)>0){
							if(!in_array($res[0]->item_id,$itemArr)){
								$itemArr[] =$res[0]->item_id;
								$itemresult[] = $res;
							}
						}
					}else if($type_name=='group')
					{
						$itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
						cs.expiry_date, im.item_id, im.item_name, im.item_type_id, im.training_type_id, im.delivery_type_id, tp.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM training_program tp 
						join class_master cm on cm.class_id = tp.class_id and cm.status_id = 1 and cm.company_id = :compId2
						join item_master im on tp.item_id = im.item_id and im.item_id = cm.item_id and im.is_active=1 and im.company_id=:compId1
						join target_audience as ta on ta.item_id=im.item_id and ta.company_id = :compId3
						join user_group as ugp on ugp.group_id = ta.job_group_id and ugp.user_id=:userId and ugp.is_active=1 	
						join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete=0 and gm.company_id=:compId4
						join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id = :compId5
						join training_type ty ON ty.training_type_id = tp.training_type_id
						join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = '".$compid."' group by ssd.class_id) ss ON cs.class_id = ss.class_id 
						where tp.company_id = '".$compid."' and tp.item_id=:itemId and cm.item_type_id in ($traingTypeId) and im.item_type_id != 4 and im.training_type_id in (2,3) and tp.is_active = 1 and cs.is_deleted = 0 group by cm.class_id,im.item_id,tp.training_program_id";
						$item_arr['compId1']=$compid;
						$item_arr['compId2']=$compid;
						$item_arr['compId3']=$compid;
						$item_arr['compId4']=$compid;
						$item_arr['compId5']=$compid;
						$item_arr['userId']=$userid;
						$item_arr['itemId']=$itemid;
						$res= DB::select(DB::raw($itemsql),$item_arr);
						if(count(@$res)>0){
							if(!in_array($res[0]->item_id,$itemArr)){
								$itemArr[] = $res[0]->item_id;
								$itemresult[] = $res;
							}
						}
					}else{
						$itemsql ="SELECT cs.publish_date, cs.schedule_name,cm.class_name, cs.class_schedule_id, cs.class_id,cs.max_seat,
						cs.expiry_date, im.item_id, im.item_name, im.item_type_id, im.training_type_id, im.delivery_type_id, tp.company_id,tp.training_program_id, im.published_date,im.credit_value, cs.is_deleted, ty.training_type as itemtype,ss.dayid as recursive_date FROM training_program tp 
						join class_master cm on cm.class_id = tp.class_id and cm.status_id = 1
						join item_master im on tp.item_id = im.item_id and im.item_id = cm.item_id and im.is_active=1 and im.company_id=:compId1
						join  class_schedule cs on cs.class_id = cm.class_id and cs.company_id = :compId2
						join training_type ty ON ty.training_type_id = tp.training_type_id
						join ( SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd  where ssd.company_id = '".$compid."' group by ssd.class_id) ss ON cs.class_id = ss.class_id 
						where tp.company_id = '".$compid."' and tp.item_id=:itemId and cm.item_type_id in ($traingTypeId) and im.item_type_id != 4 and im.training_type_id in (2,3) and tp.is_active = 1 and cs.is_deleted = 0 group by cm.class_id,im.item_id,tp.training_program_id";
						$item_arr['compId1']=$compid;
						$item_arr['compId2']=$compid;
						$item_arr['itemId']=$itemid;
						$res= DB::select(DB::raw($itemsql),$item_arr);
						if(count(@$res)>0){
							if(!in_array($res[0]->item_id,$itemArr)){
								$itemArr[] = $res[0]->item_id;
								$itemresult[] = $res;
							}
						}
				}
			}			
		}
		
		return $itemresult;
		
	}
	
	public static function getBlendedAdvanceSearch($userid, $roleid, $compid, $categoryid, $trainingTitle, $category, $searchedFrom='') 
	{
		$array =array();
		$strQuery2 = "";
		if($category != 0)
			$strQuery2 = " and bptc.training_category_id = '$category' ";
		
		$blendedIdArr=array();
		if($roleid != 1){
			$checkTargetAudience="select distinct bta.job_group_id,bpi.blended_item_id ,bta.type_name 
			from blended_program_items as bpi  
			left join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compId1 and bta.job_group_id!=0 
			left join blended_program_training_category bptc on bpi.blended_item_id = bptc.blended_program_id
			left join training_category tct ON (bptc.training_category_id = tct.training_category_id) and tct.company_id=:compId2
			where bpi.is_active=1 and bpi.company_id=:compId3 and tct.is_delete=0 and tct.is_active=1 $strQuery2";
			$item_arr['compId1']=$compid;
			$item_arr['compId2']=$compid;
			$item_arr['compId3']=$compid;
			
		}else{
			$checkTargetAudience="select bpi.blended_item_id , 0 as job_group_id ,'' as type_name 
			from blended_program_items as bpi  
			left join blended_program_training_category bptc on bpi.blended_item_id = bptc.blended_program_id
			left join training_category tct ON (bptc.training_category_id = tct.training_category_id) and tct.company_id=:compId1
			where bpi.is_active=1 and bpi.company_id=$compid and tct.is_delete=0 and tct.is_active=1 $strQuery2 ";
			$item_arr['compId1']=$compid;

		}
		$whereCondition = '';
		if($searchedFrom)
		{
			$whereCondition .= " and (bpi.title like '%$trainingTitle%' or bpi.description like '%$trainingTitle%' ) ";	
		}
		if($trainingTitle && !$searchedFrom)
		{
			$whereCondition .= " and bpi.title like '%$trainingTitle%'";	
		}
		$checkTargetAudience = $checkTargetAudience.$whereCondition.' group by bpi.blended_item_id';
		$target = DB::select(DB::raw($checkTargetAudience), $item_arr);
		$itemresult=array();
		if(count($target)>0) {
			foreach($target as $key => $value)
			{
				$blended_item_id	=	$value->blended_item_id;
				$type_name			=	$value->type_name;
				if($type_name=='job')
				{
					$jobTitleBasedItem="select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and bpi.status_id = 1 and bpi.is_active=1 and bpi.company_id=:compId1
					join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compId2 and bta.job_group_id!=0 
					join user_jobtitle as uj on uj.job_id = bta.job_group_id and uj.user_id=:userId and uj.is_active=1  
					join job_title_group as jtg on jtg.job_id=uj.job_id and jtg.is_active = 1 and jtg.company_id=:compId3
					where tp.company_id = :compId4 and tp.blended_program_id=:blendedItemId1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blendedItemI2 and bpi.is_active='1' group by bpi.blended_item_id";
					$var_arr['compId1']=$compid;
					$var_arr['compId2']=$compid;
					$var_arr['compId3']=$compid;
					$var_arr['compId4']=$compid;
					$var_arr['userId']=$userid;
					$var_arr['blendedItemId1']=$blended_item_id;
					$var_arr['blendedItemI2']=$blended_item_id;
					$res  = DB::select(DB::raw($jobTitleBasedItem),$var_arr);
					if(count(@$res)>0){
						if(!in_array($res[0]->blended_item_id,$blendedIdArr)){
							$blendedIdArr[] = $res[0]->blended_item_id;
							$itemresult[] = $res;
						}
					}
				}else if($type_name=='group') {
					$groupBasedItem="select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and bpi.status_id = 1 and bpi.is_active=1 and bpi.company_id=:compId1
					join blended_target_audience as bta on bta.blended_program_id=bpi.blended_item_id and bta.is_active=1 and bta.company_id=:compId2 and bta.job_group_id!=0 
					join user_group as ugp on ugp.group_id = bta.job_group_id and ugp.user_id=:userId and ugp.is_active=1   
					join group_master as gm on gm.group_id = ugp.group_id and gm.is_active = 1 and gm.is_delete = 0 and gm.company_id=:compId3
					where tp.company_id = :compId4 and tp.blended_program_id=:blendedItemId1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blendedItemI2 and bpi.is_active='1' group by bpi.blended_item_id";
					$var_arr['compId1']=$compid;
					$var_arr['compId2']=$compid;
					$var_arr['compId3']=$compid;
					$var_arr['compId4']=$compid;
					$var_arr['userId']=$userid;
					$var_arr['blendedItemId1']=$blended_item_id;
					$var_arr['blendedItemI2']=$blended_item_id;
					$res  = DB::select(DB::raw($groupBasedItem),$var_arr);
					if(count(@$res)>0){
						if(!in_array($res[0]->blended_item_id,$blendedIdArr)){
							$blendedIdArr[] = $res[0]->blended_item_id;
							$itemresult[] = $res;
						}
					}
				}else {
					$itemsql = "select bpi.*,tp.published_date,tp.training_type_id,tp.training_program_id,tp.training_title,tp.blended_program_id 
					from training_program as tp 
					join training_type as tc on tc.training_type_id=tp.training_type_id
					join blended_program_items bpi on bpi.blended_item_id= tp.blended_program_id and bpi.status_id = 1 and bpi.is_active=1 and bpi.company_id=:compId1
					where tp.company_id = :compId2 and tp.blended_program_id=:blendedItemId1 and tp.is_active=1 
					and tp.training_type_id=4 and bpi.blended_item_id=:blendedItemI2 and bpi.is_active='1' group by bpi.blended_item_id";
					$var_arr['compId1']=$compid;
					$var_arr['compId2']=$compid;
					$var_arr['blendedItemId1']=$blended_item_id;
					$var_arr['blendedItemI2']=$blended_item_id;
					$res  = DB::select(DB::raw($itemsql),$var_arr);
					if(count(@$res)>0){
						if(!in_array($res[0]->blended_item_id,$blendedIdArr)){
							$blendedIdArr[] = $res[0]->blended_item_id;
							$itemresult[] = $res;
						}
					}
				}
			}
		}
		return $itemresult;
	}
	
	

}
