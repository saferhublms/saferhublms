<?php
namespace App\Http\Traits;

use App\Models\SupervisorSetting;
use App\Models\UserSupervisor;
use App\Models\UserMaster;
use App\Models\UsertrainingCatalog;
use App\Models\ClassSchedule;
use App\Models\UserSupervisiorApprove;
use App\Models\CertificateContent;
use App\Models\ClassMaster;
use App\Models\TrainingProgram;
use Mail,DB;

trait Supervisior{

            public static function supervisorsetttings($compid, $userid) 
			{
				$result =SupervisorSetting::where('supervisor_id', $userid)->where('company_id', $compid)->select('supervisor_id','notify_update','auto_approve')->get();
				if($result){
					$result[0]->notify_update=($result[0]->notify_update!='')?$result[0]->notify_update:0;
					$result[0]->auto_approve=($result[0]->auto_approve!='')?$result[0]->auto_approve:0;
				}
				
				return $result;
			}
			
			public static function getsupervisorfu($compid, $userid)
			{
				$user_res=UserSupervisor::join('user_master',function($join){
					                     $join->on('user_master.user_id','=','user_supervisor.user_id')
                                              ->where('user_master.is_active',1)										 
                                              ->where('user_master.is_delete',0);
				})
				                      ->where('user_supervisor.is_active',1)
				                      ->where('user_supervisor.user_id',$userid)
				                      ->where('user_supervisor.company_id',$compid)
				                      ->where('user_master.company_id',$compid)
									  ->distinct()
									  ->select('user_supervisor.supervisor_id')
									  ->get();
				return $user_res;					  
			}
			
			
			
			/**
     * getSupervisorRows() - function used for get supervisor rows accroding to passed lavel and user id
     * 
	 * Created By: Akshay Kumar Tyagi
	 * Created Date: 10 January, 2018
     */
	public static function getSupervisorRows($level, $user_id, $roleid, $compid, $suborgId = 0 , $subadminbasedon=0, $instructorbasedon=0)
	{
		/* $db = Zend_Db_Table::getDefaultAdapter();	
		$db->setFetchMode(Zend_Db::FETCH_ASSOC); */
		$sel_arr = array();
		if($roleid==5 || $roleid==3){		
			if($level==0){
				if($subadminbasedon == 1 && $roleid==5)
				{
					$sql="SELECT group_concat(DISTINCT jtg.jobtitle_group) as jobTitles,um.user_id,um.role_id, um.user_name FROM user_supervisor as us
					join `user_master` as um on us.user_id=um.user_id 
					 join user_jobtitle uj on uj.user_id=um.user_id and uj.is_active=1
					LEFT join job_title_group jtg on jtg.job_id=uj.job_id and jtg.is_active=1
					join user_group ug on ug.user_id = um.user_id and ug.is_active=1
					join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0
					join user_organization uo on uo.user_id=um.user_id and uo.is_active = 1
					join sub_organization_master som on som.sub_organization_id=uo.sub_organization_id and som.is_active = 1 and som.is_delete = 0 and som.company_id = :compid1
					where um.company_id = :compid2 and um.is_active = 1 and um.is_delete = 0 and um.is_approved = 1 and us.is_active = 1 and us.company_id = :compid3 and som.sub_organization_id = :suborgId and um.is_user_internal!=1 group by um.user_id order by um.user_name ASC";
					$sel_arr['compid1'] = $compid;
					$sel_arr['compid2'] = $compid;
					$sel_arr['compid3'] = $compid;
					$sel_arr['suborgId'] = $suborgId;						
				}
				else if($instructorbasedon == 1 && $roleid==3)
				{
					$sql="SELECT group_concat(DISTINCT jtg.jobtitle_group) as jobTitles,um.user_id,um.role_id, um.user_name FROM user_supervisor as us
					join `user_master` as um on us.user_id=um.user_id 
					 join user_jobtitle uj on uj.user_id=um.user_id and uj.is_active=1
					LEFT join job_title_group jtg on jtg.job_id=uj.job_id and jtg.is_active=1
					join user_group ug on ug.user_id = um.user_id and ug.is_active=1
					join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0
					join user_organization uo on uo.user_id=um.user_id and uo.is_active = 1
					join sub_organization_master som on som.sub_organization_id=uo.sub_organization_id and som.is_active = 1 and som.is_delete = 0 and som.company_id = :compid1
					where um.company_id = :compid2 and um.is_active = 1 and um.is_delete = 0 and um.is_approved = 1 and us.is_active = 1 and us.company_id = :compid3 and som.sub_organization_id = :suborgId and um.is_user_internal!=1 group by um.user_id order by um.user_name ASC";
					$sel_arr['compid1'] = $compid;
					$sel_arr['compid2'] = $compid;
					$sel_arr['compid3'] = $compid;
					$sel_arr['suborgId'] = $suborgId;						
				}
				else
				{
					$sql="SELECT group_concat(DISTINCT jtg.jobtitle_group) as jobTitles,um.user_id,um.role_id, um.user_name FROM user_supervisor as us
					join `user_master` as um on us.user_id=um.user_id  and um.is_user_internal!=1
					 join user_jobtitle uj on uj.user_id=um.user_id and uj.is_active=1
					LEFT join job_title_group jtg on jtg.job_id=uj.job_id and jtg.is_active=1
					join user_group ug on ug.user_id = um.user_id and ug.is_active=1
					join group_master gm on gm.group_id = ug.group_id and gm.is_active=1 and gm.is_delete=0
					where um.company_id = :compid1 and um.is_active = 1 and um.is_delete = 0 and um.is_approved = 1 and us.is_active = 1 and us.company_id = :compid2 and ug.group_id IN (SELECT CONCAT( group_id ) AS groups FROM sub_admin_assigned_group saag WHERE saag.user_id =:user_id 
					AND saag.is_active =1)  group by um.user_id order by um.user_name ASC";
					$sel_arr['compid1'] = $compid;
					$sel_arr['compid2'] = $compid;
					$sel_arr['user_id'] = $user_id;				
				}
				
			}else{
				$sql = "SELECT group_concat(DISTINCT jtg.jobtitle_group) as jobTitles,um.user_id,um.role_id, um.user_name 
				FROM user_supervisor as us
				join `user_master` as um on us.user_id=um.user_id and um.is_user_internal!=1
				join user_jobtitle uj on uj.user_id=um.user_id and uj.is_active=1
				LEFT join job_title_group jtg on jtg.job_id=uj.job_id and jtg.is_active=1
				WHERE us.is_active = 1 and um.is_active=1 AND um.is_delete=0 AND us.supervisor_id=:user_id and  um.company_id = :compid1 and um.is_approved = 1 group by um.user_id order by um.user_name ASC";
				$sel_arr['compid1'] = $compid;
				$sel_arr['user_id'] = $user_id;	
			}
		}else{
			$sql = "SELECT group_concat(DISTINCT jtg.jobtitle_group) as jobTitles,um.user_id,um.role_id, um.user_name ,us.is_active ,us.supervisor_id
			FROM user_supervisor as us
			join `user_master` as um  on us.user_id=um.user_id and um.is_active = 1 and um.is_delete = 0 and um.is_user_internal!=1
			join role_master rm on um.role_id=rm.role_id
			left join user_jobtitle uj on uj.user_id=um.user_id and uj.is_active=1
			LEFT join job_title_group jtg on jtg.job_id=uj.job_id and jtg.is_active=1
			WHERE us.is_active = 1 and um.company_id = :compid1 and um.is_approved = 1 and um.login_id != '' ";
			
			$sel_arr['compid1'] = $compid;
			
			if($level!=0)  $sql .= " AND us.supervisor_id='{$user_id}'";
			
			$sql .= " group by um.user_id order by um.user_name ASC";
		}		
		$user_res = DB::select(DB::raw($sql),$sel_arr);
		$resultArray = json_decode(json_encode($user_res), true);
		return $resultArray;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static function getunaapproveduserstosupervisorforelearning($compid, $supervisorid, $userprogtypeid)
	{
		$userswithoutalllevelArr = array();
		$userswithoutalllevelArray = array();
		$collectallusersArr = array();
		$collectallusers = Supervisior::getalllevelsupervisorusersforsupervisor($compid, $supervisorid);
		$userswithoutalllevel = Supervisior::getallsupervisorusers($compid, $supervisorid);
		if(count($userswithoutalllevel) > 0) 
		{
			$supervisor = array();
			foreach ($userswithoutalllevel as $val) 
			{
				$textvalue = $val;
				$needle = '||';
				$userswithoutalllevelArr[] = substr($textvalue, 0, strpos($textvalue, $needle));
			}
			if ($userswithoutalllevelArr != null) 
			{
				$userswithoutalllevelArray = array_unique($userswithoutalllevelArr);
			}
		}
		$collectallusersArr = array_merge($userswithoutalllevelArray, $collectallusers);
		$itemresult = array();
		$blendresult = array();
		$itemres = array();
		$mainresult = array();
		if (count(array_unique($collectallusersArr)) > 0) {
			foreach (array_unique($collectallusersArr) as $value) {
				$userids = $value;
				$itemresult[]=UsertrainingCatalog::join('training_program',function($join) use ($compid){
					                                $join->on('training_program.training_program_id','=','user_training_catalog.program_id')
													->where('training_program.company_id',$compid);
				                                 })
				                                 ->join('item_master',function($join) use ($compid){
					                                $join->on('training_program.item_id','=','item_master.item_id')
													->where('item_master.company_id',$compid);
												})
												->join('training_type',function($join) use ($compid){
					                                $join->on('training_program.training_type_id','=','training_type.training_type_id');
												})
												->join('user_master',function($join) use ($compid){
					                                $join->on('user_training_catalog.user_id','=','user_master.user_id')
													->where('user_master.company_id',$compid);
												})
												->join('class_master',function($join) use ($compid){
					                                $join->on('user_training_catalog.session_id','=','class_master.class_id')
													->where('class_master.company_id',$compid);
												})
												->join('class_schedule',function($join) use ($compid){
					                                $join->on('training_program.class_id','=','class_schedule.class_id')
													->where('class_schedule.company_id',$compid);
												})
												->where('user_training_catalog.is_approved',0)
												->where('user_training_catalog.is_enroll',1)
												->whereIn('user_training_catalog.user_program_type_id',[1,2])
												->where('training_program.company_id',$compid)
												->where('user_training_catalog.user_id',$userids)
												->where('user_training_catalog.training_status_id',3)
												->where('user_master.is_delete',0)
												->where('user_master.is_active',1)
												->select('item_master.item_id','item_master.training_type_id','user_training_catalog.user_id','user_training_catalog.user_training_id','user_master.user_name','training_type.training_type AS itemtype','item_master.item_name','class_schedule.class_schedule_id','class_master.class_name','class_schedule.recursive_date','training_program.training_program_id','user_training_catalog.session_id')
												->get();
			}
			
		}
		$mainresult = $itemresult;
		return $mainresult;
		
	}
	
	public static function getalllevelsupervisorusersforsupervisor($compid, $loggeduserid) 
	{
		$supArray = array();
		$result=UserMaster::join('user_supervisor','user_supervisor.user_id','=','user_master.user_id')
		                  ->join('role_master','role_master.role_id','=','user_master.role_id')
						  ->where('user_master.company_id',$compid)
						  ->where('user_supervisor.supervisor_id',$loggeduserid)
						  ->where('user_master.is_approved',1)
						  ->where('user_master.is_delete',0)
						  ->where('user_master.is_active',1)
						  ->select('user_master.*','role_master.*')
						  ->get();
		$subuser = array();
		$userofsubuser = array();
		$userofsubuserArray = array();
		$collectuserofloggeduser = array();
		$getuserofcheckeduser = array();
		$getuserofcheckeduserag = array();
		$collectusersArr = array();
		if (count($result) > 0) {

			foreach ($result as $values) {
				$collectuserofloggeduser[] = $values->user_id;
			}
		}
		$checkforitsubuser = 0;
		$forfirsttime = 1;
		if (count($collectuserofloggeduser) > 0) {
			foreach ($collectuserofloggeduser as $useridoflogged) {
				$checkotherusers = Supervisior::checkotheruser($useridoflogged, $compid);
				if ($checkotherusers['hasusers'] == 'yes') {
					$fetchuseridsfromchekeusers[] = $checkotherusers['userids'];
					$collectusersArr[] = $checkotherusers['userids'];
					$last_item = end($checkotherusers['userids']);
					$last_item = each($checkotherusers['userids']);
					foreach ($fetchuseridsfromchekeusers as $keyvalue) {
						foreach ($keyvalue as $key => $useridvalue) {
							if ($useridvalue != $last_item['value'] and $key != $last_item['key']) {
								$checkusersofuser = Supervisior::checkotheruser($useridvalue, $compid);
								$getuserofcheckeduser = array();
								if ($checkusersofuser['hasusers'] == 'yes') {
									$getuserofcheckeduser[] = $checkusersofuser['userids'];
									$collectusersArr[] = $checkusersofuser['userids'];
								}

								$last_iteminsec = end($checkusersofuser['userids']);
								$last_iteminsec = each($checkusersofuser['userids']);
								if (count($getuserofcheckeduser) > 0) {
									foreach ($getuserofcheckeduser as $userofsubuservalue) {
										foreach ($userofsubuservalue as $subkey => $subuusersvalue) {

											if ($subuusersvalue != $last_iteminsec['value']
													and $subkey != $last_iteminsec['key']) {
												$checkusersofuserag = $this->checkotheruser($subuusersvalue, $compid);

												if ($checkusersofuserag['hasusers'] == 'yes') {
													$getuserofcheckeduserag[] = $checkusersofuserag['userids'];
													$collectusersArr[] = $checkusersofuserag['userids'];
												}
											}
										}
									}
									unset($getuserofcheckeduser);
								}
							}
							
						}
					}
					unset($fetchuseridsfromchekeusers);

				}

			}
			
		}
		$wholeusers = array();
		$makeuniqueArr = array();
		if (count($collectusersArr) > 0) {
			foreach ($collectusersArr as $keyvalues) {
				foreach ($keyvalues as $useridsssall) {
					$wholeusers[] = $useridsssall;
				}
			}
		}
		$makeuniqueArr = array_unique($wholeusers);
		return $makeuniqueArr;
		
		
	}
	
	
	public static function checkotheruser($userids, $compid) 
	{
		$userid = array();
		$fetcres=UserMaster::join('user_supervisor','user_supervisor.user_id','=','user_master.user_id')
						  ->where('user_master.company_id',$compid)
						  ->where('user_supervisor.supervisor_id',$userids)
						  ->where('user_master.is_approved',1)
						  ->where('user_master.is_delete',0)
						  ->where('user_master.is_active',1)
						  ->select('user_master.user_id','user_supervisor.supervisor_id')
						  ->get();
		if (count($fetcres) > 0) {
			foreach ($fetcres as $value) {
				$userid[] = $value->user_id;

			}
			array_push($userid, $userids);


			return array('hasusers' => 'yes', 'userids' => $userid);
		}
			if (count($fetcres) == 0)
			return array('hasusers' => 'no', 'userids' => $userid);		
		
	}
	
	public static function getallsupervisorusers($compid, $userid) 
	{
		$result = UserMaster::join('user_supervisor','user_supervisor.user_id','=','user_master.user_id')
		                  ->join('role_master','role_master.role_id','=','user_master.role_id')
						  ->where('user_master.company_id',$compid)
						  ->where('user_supervisor.supervisor_id',$userid)
						  ->where('user_master.is_approved',1)
						  ->where('user_master.is_delete',0)
						  ->where('user_master.is_active',1)
						  ->where('user_master.role_id','!=',1)
						  ->select('user_master.*','role_master.*','user_supervisor.supervisor_id as supervisor')
						  ->get();
		$subuser = array();
		$supArray = array();
		if (count($result) > 0) {
			foreach ($result as $values) {
				$subuser = array();
				$userid = $values->user_id;
				$supres = UserMaster::join('user_supervisor','user_supervisor.user_id','=','user_master.user_id')
						  ->where('user_master.company_id',$compid)
						  ->where('user_supervisor.supervisor_id',$userids)
						  ->where('user_master.is_approved',1)
						  ->where('user_master.is_delete',0)
						   ->where('user_master.is_active',1)
						  ->select('user_master.user_id')
						  ->get();
				if (count($supres) > 0) {
					foreach ($supres as $value) {						
						$subuser[] = $value->user_id;						
					}
					$subuserimp = implode('-', $subuser);
					$supArray[] = $userid . "||" . $subuserimp;
					//
				}
				if (count($supres) == 0) {
					$supArray[] = $userid . "||" . 'nouser';
				}
				unset($subuser);
				
			}

		}
            return array_unique($supArray);		
	}
	
	public static function getClassScheduleData($scheduleId, $companyID) 
	{
		$result = ClassSchedule::whereclassId($scheduleId)->wherecompanyId($companyID)->get();
		return $result;
	}
	
	public static function fetchsubmittraininginsupervisorfor($compid, $supervisorid)
	{
		$userswithoutalllevelArr = array();
		$userswithoutalllevelArray = array();
		$collectallusersArr = array();
		$collectallusers = Supervisior::getalllevelsupervisorusersforsupervisor($compid, $supervisorid);
		$userswithoutalllevel = Supervisior::getallsupervisorusers($compid, $supervisorid);
		if (count($userswithoutalllevel) > 0)
		{
			$supervisor = array();
			foreach ($userswithoutalllevel as $val)
			{
				$textvalue = $val;
				$needle = '||';
				$userswithoutalllevelArr[] = substr($textvalue, 0, strpos($textvalue, $needle));
			}
			if ($userswithoutalllevelArr != null) {
				$userswithoutalllevelArray = array_unique($userswithoutalllevelArr);
			}
		}
		$collectallusersArr = array_merge($userswithoutalllevelArray, $collectallusers);
		$itemresult = array();
		$blendresult = array();
		$itemres = array();
		$mainresult = array();
		if (count(array_unique($collectallusersArr)) > 0)
		{
			foreach (array_unique($collectallusersArr) as $value)
			{
				$userids = $value;
				$itemresult[]=UsertrainingCatalog::join('training_program',function($join) use ($compid){
					                                $join->on('training_program.training_program_id','=','user_training_catalog.program_id');
													
				                                 })
				                                 ->join('item_master',function($join) use ($compid){
					                                $join->on('training_program.item_id','=','item_master.item_id');
													
												})
												->join('training_type',function($join) use ($compid){
					                                $join->on('training_program.training_type_id','=','training_type.training_type_id');
												})
												->join('user_master',function($join) use ($compid){
					                                $join->on('user_training_catalog.user_id','=','user_master.user_id');
													
												})
												
												->where('user_training_catalog.is_approved',0)
												->where('user_training_catalog.is_enroll',1)
												->where('user_training_catalog.user_program_type_id',4)
												->where('training_program.company_id',$compid)
												->where('training_program.require_supervisor_approval',1)
												->where('user_master.is_delete',0)
												->where('user_master.is_active',1)
												->where('user_master.is_approved',1)
												->where('item_master.is_active',1)
												->select('item_master.item_id','item_master.training_type_id','user_training_catalog.user_id','user_training_catalog.user_training_id','user_master.user_name','user_training_catalog.training_date','training_type.training_type AS itemtype','item_master.item_name','training_program.training_program_id')
												->get();
				
			}
		}
		$mainresult = $itemresult;
		return $mainresult;
	}
	
	public static function getunaapprovedusersofelearningclassroom($compid, $userprogtypeid)
	{
		$mainresult = array();
		$result = UsertrainingCatalog::join('training_program AS tp','user_training_catalog.program_id','=','tp.training_program_id')
		                            ->join('item_master as im','im.item_id','=','tp.item_id')
		                            ->join('training_type AS tc','tp.training_type_id','=','tc.training_type_id')
		                            ->join('user_master AS um','user_training_catalog.user_id','=','um.user_id')
		                            ->join('class_master AS cm','user_training_catalog.session_id','=','cm.class_id')
		                            ->join('class_schedule AS cs','cs.class_id','=','tp.class_id')
									->where('user_training_catalog.is_approved',0)
									->where('user_training_catalog.is_enroll',1)
									->whereIn('user_training_catalog.user_program_type_id',[1,2])
									->where('tp.company_id',$compid)
									->where('im.is_active',1)
									->where('um.is_delete',0)
									->where('um.is_approved',1)
									->whereIn('user_training_catalog.training_status_id',[3,10])
									->select('im.item_id','user_training_catalog.session_id','im.training_type_id','user_training_catalog.user_id','user_training_catalog.user_training_id','um.user_name','tc.training_type AS itemtype','tp.training_title','im.item_name','cs.class_schedule_id','cm.class_name','cs.recursive_date','tp.training_program_id')
									->distinct()
									->get()
									->toArray();
		$mainresult[] = $result;
		return array_unique($mainresult);							
		
	}
	
	public static function fetchelearningplusclassinsubmittraining($compid, $userprogtypeid)
	{
		$mainresult = array();
		$result = UsertrainingCatalog::join('training_program AS tp','user_training_catalog.program_id','=','tp.training_program_id')
		                            ->join('item_master as im',function($join) use($compid){
										              $join->on('im.item_id','=','tp.item_id')
													       ->where('im.company_id',$compid);
									})
		                            ->join('training_type AS tc','tp.training_type_id','=','tc.training_type_id')
		                            ->join('user_master AS um',function($join) use($compid){
										               $join->on('user_training_catalog.user_id','=','um.user_id')
													        ->where('um.company_id',$compid);
									})
		                           
									->where('user_training_catalog.is_approved',0)
									->where('user_training_catalog.is_enroll',1)
									->where('user_training_catalog.user_program_type_id',4)
									->where('tp.company_id',$compid)
									->where('tp.require_supervisor_approval',1)
									->where('im.is_active',1)
									->where('um.is_delete',0)
									->where('um.is_approved',1)
									->where('um.is_active',1)
									->whereIn('user_training_catalog.training_status_id',[3,10])
									->select('im.item_id','im.training_type_id','user_training_catalog.user_id','user_training_catalog.user_training_id','um.user_name','user_training_catalog.training_date','tc.training_type AS itemtype','im.item_name','tp.training_program_id')
									->distinct()
									->get()
									->toArray();
		$mainresult[] = $result;
		return $mainresult;
		
	}
	
	public static function classroomSupervisorPendingApprovals($compid, $supervisorid) 
	{
		$sel_arr = array();
		$sql = "SELECT tp.training_program_id, utc.user_id, tp.item_id, tp.training_type_id, utc.user_training_id, um.user_name, um.user_id, um.role_id, sup.user_name as supervisor, tp.training_title, tp.description, ss.dayid as recursive_date,tc.training_type AS itemtype FROM user_supervisior_approve usa 
		join user_training_catalog utc on utc.user_training_id = usa.training_catalog_id
		join training_program AS tp ON utc.program_id = tp.training_program_id 
		join user_master as um on um.user_id = utc.user_id
		join user_supervisor as us on us.user_id = um.user_id and us.is_active=1
		join user_master as sup on sup.user_id = us.supervisor_id
		JOIN class_master cm ON cm.class_id = utc.session_id
		join training_type tc on tp.training_type_id = tc.training_type_id 
		join (SELECT min(ssd.dayid) as dayid, ssd.class_id from session_scedule ssd where ssd.company_id = '".$compid."' group by ssd.class_id) ss ON cm.class_id = ss.class_id
		where usa.company_id=:compId and usa.is_approve=0 and utc.is_approved = 0 and utc.is_enroll = 1 and um.is_active = 1 and um.is_delete = 0
		and utc.user_program_type_id != 4 ";
		$sel_arr['compId'] = $compid;
		
		if($supervisorid != -1)
		{
			$sql.= " and us.supervisor_id:supervisorid";
			$sel_arr['supervisorid'] = $supervisorid;
		}
		$sql.= " group by utc.user_training_id";
		$result = DB::select(DB::raw($sql), $sel_arr);
		return $result;
	}
	
	public static function submittedTrainingForSupervisorApprovals12($compid, $supervisorid,$usertrainingid) 
	{
		
		$sql = "SELECT tp.training_program_id, tp.item_id, tp.training_type_id, utc.user_training_id, um.user_name, um.user_id, um.role_id, sup.user_name as supervisor, tp.training_title, tp.description, ut.completion_date, tc.training_type AS itemtype FROM user_supervisior_approve usa 
		join user_training_catalog utc on utc.user_training_id = usa.training_catalog_id
		join training_program AS tp ON utc.program_id = tp.training_program_id 
		join user_transcript ut on ut.item_id = tp.item_id and ut.user_id = utc.user_id
		join user_master as um on um.user_id = utc.user_id
		join user_supervisor as us on us.user_id = um.user_id
		left join user_master as sup on sup.user_id = us.supervisor_id
		join training_type tc on tp.training_type_id = tc.training_type_id
		where usa.company_id='".$compid."' and utc.user_training_id ='".$usertrainingid."' and usa.is_approve=0 and utc.is_approved = 0 and utc.is_enroll = 1 and um.is_active = 1 and um.is_delete = 0 and utc.user_program_type_id = 4";
		if($supervisorid != -1)
		{
			$sql.= " and supervisior_id='".$supervisorid."'";
		}
		$result1 = DB::select($sql);
		return $result1;
	}
	
	public static function SubmitTrainingSupervisorApprovals($compid, $supervisorid) 
	{
		
		$sel_arr = array();
		$sql ="SELECT distinct utc.user_training_id
			FROM user_supervisior_approve usa
			JOIN user_training_catalog utc ON utc.user_training_id = usa.training_catalog_id AND utc.company_id='$compid'
			JOIN user_master AS um ON um.user_id = utc.user_id
			JOIN user_supervisor AS us ON us.user_id = um.user_id AND us.is_active=1 AND us.company_id='$compid'
			LEFT JOIN user_master AS sup ON sup.user_id = us.supervisor_id
			WHERE usa.company_id=:compid AND usa.is_approve=0 AND utc.is_approved = 0 AND utc.is_enroll = 1 AND um.is_active = 1 AND um.is_delete = 0 AND utc.user_program_type_id = 4";
		
		$sel_arr['compid'] = $compid;
		
		if($supervisorid != -1)
		{
			$sql.= " and us.supervisor_id=:supervisorid";
			$sel_arr['supervisorid'] = $supervisorid;
		}

		$result = DB::select(DB::raw($sql),$sel_arr);
		return $result;
	}
	
	public static function countclassroomSupervisorPendingApprovals($compid, $supervisorid) 
	{
		$sel_arr = array();
		$sql = "SELECT usa.user_id FROM user_supervisior_approve usa
			JOIN user_training_catalog utc ON utc.user_training_id = usa.training_catalog_id AND utc.company_id=$compid
			JOIN training_program AS tp ON utc.program_id = tp.training_program_id
			join item_master im on im.item_id = utc.item_id and im.is_active = 1 AND im.company_id=$compid
			join user_master um on um.user_id = usa.user_id and um.is_active =1 and um.is_delete = 0
			JOIN user_supervisor AS us ON us.user_id = um.user_id AND us.is_active=1
			JOIN user_master AS sup ON sup.user_id = us.supervisor_id
			WHERE usa.company_id =:compid AND usa.is_approve=0 and utc.is_active =1  
			and utc.is_approved = 0 and im.item_type_id = 2 AND utc.is_enroll = 1"; 
		$sel_arr['compid'] = $compid;
		if($supervisorid != -1)
		{
			$sql.= " and us.supervisor_id=:supervisorid";
			$sel_arr['supervisorid'] = $supervisorid;
		}
		$sql.= " GROUP BY um.user_id,utc.item_id order by utc.item_id";
		$result = DB::select(DB::raw($sql), $sel_arr);
		return $result;
	}
	
	
	public static function submittedTrainingForSupervisorApprovals($compid, $supervisorid) 
	{
		$sel_arr = array();
		$sql = "SELECT distinct tp.training_program_id, utc.user_id,uf.file_name, tp.item_id, tp.training_type_id, utc.user_training_id, um.user_name, 
		um.user_id, um.role_id, sup.user_name as supervisor, tp.training_title, tp.description,  if(ut.completion_date !='',ut.completion_date , if(tct.completion_date !='',tct.completion_date , tm.created_date)) as completion_date,tc.training_type AS itemtype
		FROM user_supervisior_approve usa 
		join user_training_catalog utc on utc.user_training_id = usa.training_catalog_id
		join training_program AS tp ON utc.program_id = tp.training_program_id and tp.is_active=1
		left join user_transcript ut on ut.item_id = tp.item_id and ut.user_id = utc.user_id
		left join training_catalog_transcript tct on  tct.item_id = tp.item_id and tct.user_id = utc.user_id 
		join user_master as um on um.user_id = utc.user_id
		join user_supervisor as us on us.user_id = um.user_id and us.is_active=1
		left join user_master as sup on sup.user_id = us.supervisor_id
		join training_type tc on tp.training_type_id = tc.training_type_id and tc.is_active=1
		left join item_master as tm on tm.item_id = tp.item_id and tm.is_active=1
		left join uploaded_files as uf on uf.files_id = tm.file_id and uf.is_active=1
		where usa.company_id=:compId and usa.is_approve=0 and utc.is_approved = 0 and utc.is_enroll = 1 and um.is_active = 1 and um.is_delete = 0 and 
		utc.user_program_type_id = 4 ";
		$sel_arr['compId'] = $compid;
		if($supervisorid != -1)
		{
			 $sql.= " and us.supervisor_id=:supervisorid";
			 $sel_arr['supervisorid'] = $supervisorid;
		}
		$sql.= " group by tp.training_program_id";
		$result1 = DB::select(DB::raw($sql), $sel_arr);
		
		$sel_arr1 = array();
		$sql1 = "SELECT distinct '' as 'training_program_id', utc.user_id,uf.file_name, utc.program_id as item_id, utc.training_type_id, utc.user_training_id, um.user_name, 
		um.user_id, um.role_id, sup.user_name as supervisor, a.assessment_name as 'training_title',a.assessment_desc as description,  ut.completion_date as completion_date,tc.training_type AS itemtype
		FROM user_supervisior_approve usa 
		join user_training_catalog utc on utc.user_training_id = usa.training_catalog_id
		left join user_transcript ut on ut.registration_id = utc.program_id and ut.user_id = utc.user_id
		join assessment as a on a.id=utc.program_id
		left join training_catalog_transcript tct on  tct.registration_id = utc.program_id and tct.user_id = utc.user_id 
		join user_master as um on um.user_id = utc.user_id
		join user_supervisor as us on us.user_id = um.user_id and us.is_active=1
		left join user_master as sup on sup.user_id = us.supervisor_id
		join training_type tc on utc.training_type_id = tc.training_type_id and tc.is_active=1
		left join item_master as tm on tm.item_id = utc.program_id and tm.is_active=1
		left join uploaded_files as uf on uf.files_id =tm.file_id  and uf.is_active=1
		where usa.company_id=:compid and usa.is_approve=0 and utc.is_approved = 0 and utc.is_enroll = 1 and um.is_active = 1 and um.is_delete = 0 and 
		utc.user_program_type_id = 4";
		$sel_arr1['compid'] = $compid;
		if($supervisorid != -1)
		{
			 $sql1.= " and us.supervisor_id=:supervisorid";
			 $sel_arr1['supervisorid'] = $supervisorid;
		}
		
		$result2 = DB::select(DB::raw($sql1), $sel_arr1);
		$res=array_merge($result2,$result1);
		return $res;
	}
	
	public static function classroomSupervisorforselected($compid, $supervisorid) 
	{
		$result=UserSupervisiorApprove::join('user_training_catalog as utc','utc.user_training_id','=','user_supervisior_approve.training_catalog_id')
		                              ->join('training_program AS tp','utc.program_id','=','tp.training_program_id')
		                              ->join('user_master as um','um.user_id','=','utc.user_id')
		                              ->join('user_supervisor as us','us.user_id','=','um.user_id')
		                              ->join('user_master as sup','sup.user_id','=','us.supervisor_id')
		                              ->join('class_schedule as cs','cs.class_id','=','utc.session_id')
		                              ->leftjoin('session_scedule as ss','ss.class_id','=','cs.class_id')
		                              ->join('training_type as tc','tp.training_type_id','=','tc.training_type_id')
									  ->where('utc.program_id',$supervisorid)
									  ->where('user_supervisior_approve.company_id',$compid)
									  ->where('user_supervisior_approve.is_approve',0)
									  ->where('utc.is_approved',0)
									  ->where('utc.is_enroll',1)
									  ->where('um.is_active',1)
									  ->where('um.is_delete',0)
									  ->where('utc.user_program_type_id','!=',4)
									  ->select('tp.training_program_id','tp.item_id','tp.training_type_id','utc.user_training_id','um.user_name','um.user_id','um.role_id','sup.user_name as supervisor','tp.training_title','tp.description','cs.class_schedule_id','cs.schedule_name','cs.max_seat','ss.dayid','tc.training_type AS itemtype')
		                              ->get();
			return $result;				  
	}
	
	public static function submittedTrainingForSelectedSupervisor($compid, $supervisorid) 
	{
		$sql = "SELECT distinct tp.training_program_id, uf.file_name, tp.item_id, tp.training_type_id, utc.user_training_id, um.user_name, um.user_id, um.role_id, sup.user_name as supervisor, tp.training_title, tm.credit_value, tp.description, if(ut.completion_date !='', ut.completion_date, tct.completion_date) as completion_date, tc.training_type AS itemtype FROM user_supervisior_approve usa 
		join user_training_catalog utc on utc.user_training_id = usa.training_catalog_id
		join training_program AS tp ON utc.program_id = tp.training_program_id 
		left join user_transcript ut on ut.item_id = tp.item_id and ut.user_id = utc.user_id
		left join training_catalog_transcript tct on tct.item_id = tp.item_id and tct.user_id = utc.user_id 
		join user_master as um on um.user_id = utc.user_id
		join user_supervisor as us on us.user_id = um.user_id
		left join user_master as sup on sup.user_id = us.supervisor_id
		join training_type tc on tp.training_type_id = tc.training_type_id
		left join item_master as tm on tm.item_id = tp.item_id
		left join uploaded_files as uf on uf.files_id = tm.file_id
		where utc.user_training_id='".$supervisorid."' and usa.company_id='".$compid."' and usa.is_approve=0 and utc.is_approved = 0 and utc.is_enroll = 1 and um.is_active = 1 and um.is_delete = 0 and utc.user_program_type_id = 4";
		$result = DB::select($sql);
		
		$sql1="SELECT DISTINCT tp.training_program_id, uf.file_name, tp.item_id, tp.training_type_id, utc.user_training_id, um.user_name, um.user_id, um.role_id, sup.user_name AS supervisor, tp.training_title, tm.credit_value, tp.description, IF(ut.completion_date !='', ut.completion_date, tct.completion_date) AS completion_date, tc.training_type AS itemtype
		FROM user_supervisior_approve usa
		JOIN user_training_catalog utc ON utc.user_training_id = usa.training_catalog_id
		left JOIN training_program AS tp ON utc.program_id = tp.training_program_id
		LEFT JOIN user_transcript ut ON ut.registration_id = utc.program_id AND ut.user_id = utc.user_id
		LEFT JOIN training_catalog_transcript tct ON tct.registration_id = utc.program_id AND tct.user_id = utc.user_id
		JOIN user_master AS um ON um.user_id = utc.user_id
		JOIN user_supervisor AS us ON us.user_id = um.user_id
		LEFT JOIN user_master AS sup ON sup.user_id = us.supervisor_id
		left JOIN training_type tc ON tp.training_type_id = tc.training_type_id
		LEFT JOIN item_master AS tm ON tm.item_id = tp.item_id
		LEFT JOIN uploaded_files AS uf ON uf.files_id = tm.file_id
		WHERE utc.user_training_id='".$supervisorid."' AND usa.company_id='".$compid."' AND usa.is_approve=0 AND utc.is_approved = 0 AND utc.is_enroll = 1 AND um.is_active = 1 AND um.is_delete = 0 AND utc.user_program_type_id = 4";
		$result1 = DB::select($sql);
		$res=array_merge($result,$result1);
		return $res;
		
	}
	
	// For delete Approve Record
	public static function deleteApproverecord($compid, $usertrainingid) 
	{
		UserSupervisiorApprove::wherecompanyId($compid)->wheretrainingCatalogId($usertrainingid)->delete();
		
	}
	
	public static function getUserTrainingData($usertrainingid) 
	{
		$result=UsertrainingCatalog::whereuserTrainingId($usertrainingid)->get();
		return $result;
	}
	
	public static function UpadateApproverecord($compid, $userprogtypeid) 
	{
		UserSupervisiorApprove::wherecompanyId($compid)->wheretrainingCatalogId($userprogtypeid)->update(['is_approve'=>1]);
	}
	
	public static function getStartDateOfClass($compid,$class_id)
	{
		$result=ClassMaster::where('company_id',$compid)->where('is_active',1)->where('status_id',1)->where('class_id',$class_id)->select('class_name','class_id','start_date_class','item_id')->first();		
		return $result;
	}
	public static function getTrainingProgramDetail($item_id,$class_id,$compid)
	{
		$result=TrainingProgram::join('item_master as I','I.item_id','=','training_program.item_id')
		                         ->where('training_program.company_id',$compid)
								 ->where('I.is_active', 1)
								 ->where('I.item_id',$item_id)
								 ->where('training_program.class_id', $class_id)
								 ->get();
		return $result;
								 
	}
	
	
	
	
	public static function getcertificates($compid) 
	{
		$cc_rows=CertificateContent::where('status','=',1)->where('company_id','=',$compid)->orderby('title','ASC')->select('id','title')->get()->toArray();
		if(count($cc_rows)>0)
		{
			return $cc_rows; 
		}else{
			return array(); 
		}
		
	}
	
	public static function getTtrainingType()
	{	
	    $whereRows = "training_type_id!=4";
		$itemsql ="SELECT * FROM training_type where ".$whereRows;	
		$trainingtypess = DB::select($itemsql);
		$trainingtypessarray = array ();
		if (count ( $trainingtypess ) > 0) {
			foreach ( $trainingtypess as $rows )
			{
				$type=array();
				$type ['type'] 		= $rows->training_type;
				$type ['id']	 	= $rows->training_type_id;
				$trainingtypessarray[]=$type;
			}
		}
		return $trainingtypessarray;
	}
	
	 public static function getTrainingCategory($compid) {
	
		$traningcategoryType =Supervisior::getskill($compid);
		$traningcategoryArray = array ();
		foreach ( $traningcategoryType as $row ) 
		{
			$category['id']			=$row->training_category_id;
			$category['text']		=$row->category_name;
			$traningcategoryArray[] =$category;
		}
		return $traningcategoryArray;
	}
	
	public static function getskill($comp_id)
	{
		$sql = "select * from training_category where is_delete=0 and is_active=1 and company_id =".$comp_id." order by category_name asc";
		$data = DB::select($sql);
		return $data;
	}
   
}