<?php
namespace App\Http\AdminTraits;

use App\Models\TrainingLibrary;
use DB;

trait Traininglibraries
{
    public static function getTrainingLibraryNameAndSize()
	{
		$results=TrainingLibrary::leftjoin('training_library_category as cat','training_library.training_library_category_id','=','cat.id')
		                        ->leftjoin('training_library_name as lib','training_library.training_library_category_id','=','lib.id')
								->where('training_library.is_active','=', 1)
								->groupBy('training_library.training_library_category_id','training_library.training_library_name_id')
								->select(DB::raw('count(*) as course_count'),'training_library.training_library_category_id','training_library.training_library_name_id','cat.category_name','lib.training_library_name')
								->get();
					return $results;
	}

}