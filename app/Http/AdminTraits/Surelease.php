<?php
namespace App\Http\AdminTraits;

use App\Models\SupreAdminReleasenote;
use DB;

trait Surelease
{
    public static function fetchAllReleasenotes()
	{
		$results=SupreAdminReleasenote::whereisDelete(0)->orderBy('id','desc')->get();
		return $results;
	}
	
	public static function updatepublish($is_active, $id)
	 {
		 $data = array();
		 $data['is_active'] 		= $is_active;
		 $data['is_maintenance'] = 0;
		 SupreAdminReleasenote::whereid($id)->update($data);
		 
	 }
	
	public static function updatedelete($is_delete, $id)
	 {
		 $data = array();
		 $data['is_delete'] 		= $is_delete;
		 SupreAdminReleasenote::whereid($id)->update($data);
		 
	 }
	
	

}