<?php
namespace App\Http\AdminTraits;

use App\Models\NotificationMaster;
use App\Models\NotificationContents;
use App\Models\NotificationCompany;
use App\Models\CustomNotificationFields;
use DB;

trait Notificationcontent
{
             public static function getSUallNotificationlist() 
			 {
				 $result=NotificationMaster::select('notification_master.notification_id','notification_master.notification_name','notification_master.subject','notification_master.notification_content','notification_master.is_active as activenotification')->get();
		         return $result;
			 }
			 
			  public static function updateNotificationShow($hideshow, $notification_id, $compid)
			  {
					 NotificationMaster::where('notification_id',$notification_id)->update(['is_active'=>$hideshow]);
			  }
			 
			 public static function getNotificationContent($notification_id,$defaltid, $compid)
			 {
					$noti_arr =array();
					if($defaltid == 1)
					{
						$result=NotificationMaster::where('notification_id',$notification_id)
						->select('notification_master.notification_id','notification_master.notification_name','notification_master.subject','notification_master.notification_content')
						->get();
					}else{
						$result=NotificationContent::join('notification_master as nm','nm.notification_id','=','notification_content.notification_id')
						->where('notification_content.notification_id',$notification_id)
						->where('notification_content.company_id',$compid)
						->select('nc.id','nc.notification_id','nc.subject','nc.content as notification_content','notification_master.notification_name')
						->get();
					}
					return $result;
			 }
			 
			 public static function getNotificationCustomField($notification_id)
			 {
				 $result=CustomNotificationFields::where('notification_id',$notification_id)->orderBy('manage_order')->get();
				 return $result;
			 }
			 
			 public static function updateNotificationStatus($is_active, $notification_id, $compid)
			 { 
				 $notificationCompany=NotificationCompany::where('notification_id',$notification_id)->where('company_id',$compid)->first();
				 if($notificationCompany){
					  NotificationCompany::where('notification_id',$notification_id)->where('company_id',$compid)->update(['is_active'=>$is_active]);
				 }else{
					  $notificationCompany=new NotificationCompany;
					  $notificationCompany->notification_id=$notification_id;
					  $notificationCompany->is_active=$is_active;
					  $notificationCompany->company_id=$compid;
					  $notificationCompany->save();
				 } 
			 }
			 
			 public static function updateNotificationDefault($is_default, $notification_id, $compid)
			 {
				 $notificationCompany=NotificationCompany::where('notification_id',$notification_id)->where('company_id',$compid)->first();
				 if($notificationCompany){
					 NotificationCompany::where('notification_id',$notification_id)->where('company_id',$compid)->update(['is_default'=>$is_default]);
				 }else{
					  $notificationCompany=new NotificationCompany;
					  $notificationCompany->notification_id=$notification_id;
					  $notificationCompany->is_default=$is_default;
					  $notificationCompany->company_id=$compid;
					  $notificationCompany->save();
				 }	 
			 }
}