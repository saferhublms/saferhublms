<?php

namespace App\Http\Middleware;

use Closure;
use Session,Auth;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(Auth::user()){
		   $userId=Auth::user()->user_id;
		   $roleId=Auth::user()->role_id;
		   
		   if($userId && in_array($roleId,array(1,3,5)) ){
			   return $next($request);
		   }else{
			   return redirect('/#/index/apps');
		   }
	   }else{
		   return redirect('/#/login');
	   }
    }
}
