window.last_ulli_element='undefined';
window.skip_ques=[];

var maxLength=500;
function charLimit(el)
{
	if (el.value.length > maxLength)
	{
		return false;
	}
	return true;
}

function characterCount(el)
{
	if (el.value.length > maxLength) {
		el.value = el.value.substring(0,maxLength);
		alert('You have exceeded the maximum limit of 500 characters. Please edit the text and try again.');
	}
	
	var textareaid = el.id;
	var SplitTextareaId = textareaid.split("_");
	var charCount = document.getElementById('charCount_'+SplitTextareaId[1]);
	if (charCount) charCount.innerHTML = maxLength - el.value.length;
	return true;
}

$(document).ready(function()
{
	var countques = $('#countques').val();
	if(countques!=1) { $('#button1').css('display','none'); }
	if(countques==1) {
		$('#button3').css('display','none');
		$('#button2').css('display','none');
	}
	
	if(!$('#ulli_1').hasClass('active'))
	{ $('#ulli_1').addClass(' active'); }
});

$(".all_ques_li").live('click', function()
{
	$('#button_nxt').show();
	btnStatus();
	
	$('.ques_divs').css('display','none');
	$(".all_ques_li").removeClass('active');
	
	var id = $(this).attr('id').split('ulli_');
	if(last_ulli_element!='undefined'){
		res=commonToInputType(last_ulli_element);
		if(res){
			//$('#ulli_'+last_ulli_element).addClass('selected');
		}
	}
	id = id[1];
	last_ulli_element=id;
	$('#div_'+id).css('display','block');
	$('#activeques').val(id);
	if(!$('#ulli_'+id).hasClass('active'))
	{
		$('#ulli_'+id).removeClass('selected');
		$('#ulli_'+id).addClass(' active');
	}
	
	var countques = $('#countques').val();
	if(countques==id)
	{
		$('#button_nxt').hide();
		atLastQuestion();
	}
});



$("#button1").live('click', function()
{
	var id = $('#activeques').val();
	var popupdiv = document.getElementById("div_"+id);
	var textarea = popupdiv.getElementsByTagName("textarea");
	var res = (textarea) ? textarea.length : '';
	if(!res)
	{
		var textbox = popupdiv.getElementsByTagName("input");
		res = commonToInputType(id);
	}
	else
	{
		res = textarea[0].value;
	}
	
	if(res)
	{
		if(!$('#ulli_'+id).hasClass('selected'))
		{ $('#ulli_'+id).addClass(' selected'); }
	}else{
		var ques_id= $("#div_"+id+" #ques_id").val();
		if($.inArray(ques_id,skip_ques)==-1){
			skip_ques.push(ques_id);
		}
	}
	count_skip_ques=skip_ques.length;
	var quesids = $('#quesids').val();
	count_quesids = quesids.split(',').length-1;
	
	if(count_skip_ques==count_quesids){
		alertify.confirm("You have skipped all questions.", function(e){
			if(e){
				window.location.reload();
			}return false;
		});
		return false;
	}
	
	$('#skipids').val(skip_ques.join(','));
	alertify.confirm("Are you confirm to submit the assessment.", function(e)
	{
		if(e)
		{
			$('#preloader_popup').show();
			var url = "/mylearning/submitassessment";
			$.post(url, $("#popupForm").serialize(), function (result)
			{
				if(result)
				{
					alertify.alert("Assessment Submitted.");
					
					var pp = result.details[0].pass_percentage;
					var ys = result.details[0].user_score;
					var rs = result.details[0].user_status;
					
					var attempt_taken = result.details[0].attempt_taken;
					if(!attempt_taken)
					{ $('#button_rtk').hide(); $('#button5').hide(); }
				
					$('#pp').text(pp);
					$('#ys').text(ys);
					$('#rs').text(rs);
					$('#preloader_popup').hide();
					$('#ques_popup_div').hide();
					$('#user_score_div').show();
				}
			}, "json");
		}
	});
});

$("#button2").live('click', function()
{
	var id = $('#activeques').val();
	var ques_id= $("#div_"+id+" #ques_id").val();
	var index = skip_ques.indexOf(ques_id);
	if(index!=-1){
	   skip_ques.splice(index, 1);
	}

	var res = commonQuestionType(id);
	
	if(res)
	{
		if(!$('#ulli_'+id).hasClass('selected'))
		{ $('#ulli_'+id).addClass(' selected'); }
		
		commonForMore();
	}
});

$("#button3").live('click', function()
{
	commonForMore();
	
	var id = $('#activeques').val();
	id--;
	var ques_id= $("#div_"+id+" #ques_id").val();
	if($.inArray(ques_id,skip_ques)==-1){
		skip_ques.push(ques_id);
	}
	if($('#ulli_'+id).hasClass('selected'))
	{ $('#ulli_'+id).removeClass('selected'); }
});

$("#button4").live('click', function()
{
	$('#preloader_popup').show();
	var user = $('#user').val();
	var token = $('#token').val();
	var burl = $('#baseurl').val();
	var quesids = $('#quesids').val();
	quesids = quesids.substring(0,quesids.length-1);
	var url = "/mylearning/reviewassessment";
	$.ajax({
		method:"POST",
		url:"/mylearning/reviewassessment",
		dataType: 'json',
		data:{user_key:user, token:token, quesids:quesids},
		 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		 success:function(result){

		if(result)
		{
			var k = 1;
			quesids = quesids.split(",");
			$.each(result.data, function(key, value)
			{
				console.log(value);
				var qtype = value[0].question_type_id;
				var qid = value[0].qid;
				var res = value[0].result;
				var ures = value[0].student_response;
				var cres = value[0].correct_response;
				
				if(qtype == '4' || qtype == 4)
				{
					if(res == 'correct' && ures == cres)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						$('input[type=radio][id=yesradio_'+qid+'][value='+cres+']').prop('checked', 'checked');
						$('#yesradio_'+qid+'_'+cres).show();
						$('#yesradio_'+qid+'_'+cres).empty();
						$('#yesradio_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						$('input[type=radio][id=yesradio_'+qid+'][value='+ures+']').prop('checked', 'checked');
						$('#yesradio_'+qid+'_'+cres).show();
						$('#yesradio_'+qid+'_'+cres).empty();
						$('#yesradio_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						$('#yesradio_'+qid+'_'+ures).show();
						$('#yesradio_'+qid+'_'+ures).empty();
						$('#yesradio_'+qid+'_'+ures).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
					}
					else
					{
						$('#yesradio_'+qid+'_'+cres).show();
						$('#yesradio_'+qid+'_'+cres).empty();
						$('#yesradio_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
				}
				else if(qtype == '3' || qtype == 3)
				{
					if(res == 'correct' && ures == cres)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						$('input[type=radio][id=trueval_'+qid+'][value='+cres+']').prop('checked', 'checked');
						$('#trueval_'+qid+'_'+cres).show();
						$('#trueval_'+qid+'_'+cres).empty();
						$('#trueval_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						$('input[type=radio][id=trueval_'+qid+'][value='+ures+']').prop('checked', 'checked');
						$('#trueval_'+qid+'_'+cres).show();
						$('#trueval_'+qid+'_'+cres).empty();
						$('#trueval_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						$('#trueval_'+qid+'_'+ures).show();
						$('#trueval_'+qid+'_'+ures).empty();
						$('#trueval_'+qid+'_'+ures).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
					}
					else
					{
						$('#trueval_'+qid+'_'+cres).show();
						$('#trueval_'+qid+'_'+cres).empty();
						$('#trueval_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
				}
				else if(qtype == '1' || qtype == 1)
				{
					if(res == 'correct' && ures == cres)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						$('input[type=radio][id=multichoiceoption_'+qid+'][value="'+qid+'||'+cres+'"]').prop('checked', 'checked');
						$('#multichoice_'+qid+'_'+cres).show();
						$('#multichoice_'+qid+'_'+cres).empty();
						$('#multichoice_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						$('input[type=radio][id=multichoiceoption_'+qid+'][value="'+qid+'||'+ures+'"]').prop('checked', 'checked');
						$('#multichoice_'+qid+'_'+cres).show();
						$('#multichoice_'+qid+'_'+cres).empty();
						$('#multichoice_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						$('#multichoice_'+qid+'_'+ures).show();
						$('#multichoice_'+qid+'_'+ures).empty();
						$('#multichoice_'+qid+'_'+ures).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
					}
					else
					{
						$('#multichoice_'+qid+'_'+cres).show();
						$('#multichoice_'+qid+'_'+cres).empty();
						$('#multichoice_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
				}
				 else if(qtype == '2' || qtype == 2)
				{

					
					ures = ures.split(",");
					
					cres = cres.split(",");
					
					if(res == 'correct' && cres.length == ures.length)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						for(var i=0; i<cres.length; i++)
						{
							$('input[type=checkbox][id=mulseloption_'+qid+'_'+cres[i]+'][value="'+qid+'||'+cres[i]+'"]').prop('checked', 'checked');
							$('#multiselect_'+qid+'_'+cres[i]).show();
							$('#multiselect_'+qid+'_'+cres[i]).empty();
							$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						}
					}
					else if(res == 'incorrect' && cres && ures)
					{
						
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						 for(var i=0; i<cres.length; i++)
						{
							if($.inArray(cres[i], ures) != -1)
							{
								$('input[type=checkbox][id=mulseloption_'+qid+'_'+cres[i]+'][value="'+qid+'||'+cres[i]+'"]').prop('checked', 'checked');
								$('#multiselect_'+qid+'_'+cres[i]).show();
								$('#multiselect_'+qid+'_'+cres[i]).empty();
								$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
							}
							else
							{
								$('#multiselect_'+qid+'_'+cres[i]).show();
								$('#multiselect_'+qid+'_'+cres[i]).empty();
								$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
							}
						}
						 for(var i=0; i<ures.length; i++)
						{
							if($.inArray(ures[i], cres) == -1)
							{
								$('input[type=checkbox][id=mulseloption_'+qid+'_'+ures[i]+'][value="'+qid+'||'+ures[i]+'"]').prop('checked', 'checked');
								$('#multiselect_'+qid+'_'+ures[i]).show();
								$('#multiselect_'+qid+'_'+ures[i]).empty();
								$('#multiselect_'+qid+'_'+ures[i]).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
							}
						} 
					}
					else
					{
						for(var i=0; i<cres.length; i++)
						{
							$('#multiselect_'+qid+'_'+cres[i]).show();
							$('#multiselect_'+qid+'_'+cres[i]).empty();
							$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						}
					}
				} 
				else if(ures && (qtype == '5' || qtype == 5))
				{
					$('#descriptive_'+qid).val(ures);
				}
				else { }
				
				k++;
			});
			
			id = 1;
			$('.ques_divs').css('display','none');
			$('#div_'+id).css('display','block');
			$('#activeques').val(id);
			$('.all_ques_li').removeClass('active');
			$('#ulli_'+id).addClass(' active');
			
			if(!$('.review_class').hasClass('ml40'))
			{ $('.review_class').addClass(' ml40'); }
			
			var countques = $('#countques').val();
			if(countques==id)
			{
				$('#button_nxt').hide();
			}
			$('#preloader_popup').hide();
			$('#bottom_btn_div').hide();
			$('#user_score_div').hide();
			$('#ques_popup_div').show();
			$('#bottom_rev_div').show();
			$('input[type=radio]').prop('disabled', 'disabled');
			$('input[type=checkbox]').prop('disabled', 'disabled');
			$('textarea').attr('disabled', 'disabled');
		}
	  }
	});
});

$("#button5").live('click', function()
{
	commonRetake();
});

$("#button_rtk").live('click', function()
{
	commonRetake();
});

function commonRetake()
{
	$('#preloader_popup').show();
	//setTimeout command is commented because commonRetake function is used in two conditions and in both conditions setTimeout is not making sense.
	// setTimeout(function()
	// {
		$('.review_class').removeClass('ml40');
		$('#bottom_rev_div').hide();
		$('.review_img_div').hide();
	
		$('input[type=radio]').prop('disabled', false);
		$('input[type=checkbox]').prop('disabled', false);
		$('textarea').removeAttr('disabled');
		btnStatus();
		$('input[type=radio]').prop('checked', false);
		$('input[type=checkbox]').prop('checked', false);
		$('textarea').val('');
		id = 1;
		$('.ques_divs').css('display','none');
		$('#div_'+id).css('display','block');
		$('#activeques').val(id);
		$('.all_ques_li').removeClass('selected');
		$('.all_ques_li').removeClass('active');
		$('.all_ques_li').removeClass('correct');
		$('.all_ques_li').removeClass('incorrect');
		$('#ulli_'+id).addClass(' active');
		
		var countques = $('#countques').val();
		if(countques==id)
		{
			atLastQuestion();
		}
		
		$('#ys').text(0);
		$('#user_score_div').hide();
		$('#ques_popup_div').show();
		$('#bottom_btn_div').show();
		$('#preloader_popup').hide();
	// }, 500);
}

$(".exit_popup").live('click', function()
{ 
	alertify.confirm("Are you confirm to exit the assessment.", function(e)
	{
		if(e)
		{		
		
		      //window.parent.location.href = $('#baseurl').val() + "/elearningmedia/launchtraining";
		      //  window.parent.location.href = $('#baseurl').val() + "/elearningmedia/launchtraining";
			   //var urlloc =$('#baseurl').val() + "/elearningmedia/launchtraining";
			 //  window.location.replace('www.google.com);
 
		      // top.location.href == $('#baseurl').val() + "/elearningmedia/launchtraining";
		      window.top.location.href  = $('#baseurl').val() + "/elearningmedia/launchtraining";
			
		}
	});
});

function btnStatus()
{
	$('.button_lblue_r4').removeAttr('disabled');
	$('.button_lblue_r4').css('background-color','#026e96');
	$('#button1').css('display','none');
	$('#button2').css('display','inline-block');
	$('#button3').css('display','inline-block');
}

function atLastQuestion()
{
	$('#button2').attr('disabled','disabled');
	$('#button3').attr('disabled','disabled');
	$('#button2').css('background-color','#555');
	$('#button3').css('background-color','#555');
	$('#button2').css('display','none');
	$('#button3').css('display','none');
	$('#button1').css('display','inline-block');
}

function commonForMore()
{
	var id = $('#activeques').val();
	if(id){
		res=commonToInputType(id);
		if(res){
			$('#ulli_'+id).addClass('selected');
		}
	}
	id++;
	
	$('.ques_divs').css('display','none');
	$(".all_ques_li").removeClass('active');
	
	$('#div_'+id).css('display','block');
	$('#activeques').val(id);
	
	if(!$('#ulli_'+id).hasClass('active'))
	{
		$('#ulli_'+id).removeClass('selected');
		$('#ulli_'+id).addClass(' active');
	}
	
	var countques = $('#countques').val();
	if(countques==id)
	{
		atLastQuestion();
	}
}

function commonQuestionType(id)
{
	var res = '';
	var popupdiv = document.getElementById("div_"+id);
	var textbox = popupdiv.getElementsByTagName("input");
	var questype = textbox[0].value;
	if(questype>=1 && questype<=4)
		res = questionChoiceType(id);
	if(questype==5)
		res = questionTextType(id);
	if(questype==6)
		res = questionSelectType(id);
	
	return res;
}

//if question type 1,2,3,4
function questionChoiceType(id)
{
	var res = commonToInputType(id);
	if(!res) { alertify.alert("Please choose any answer."); }
	return res;
}

//if question type 5
function questionTextType(id)
{
	var popupdiv = document.getElementById("div_"+id);
	var textarea = popupdiv.getElementsByTagName("textarea");
	var res = textarea[0].value;
	if(!res) { alertify.alert("Please fill answer into textarea."); }
	return res;
}

//if question type 6
function questionSelectType(id)
{
	var res = commonToInputType(id);
	if(!res) { alertify.alert("Please select any answer."); }
	return res;
}

function commonToInputType(id)
{
	var popupdiv = document.getElementById("div_"+id);
	var input = popupdiv.getElementsByTagName("input");
	$is_textarea = input[0].value;
	var res = '';
	if(parseInt($is_textarea)==5){
		var descriptive = popupdiv.getElementsByTagName("textarea");
		if(descriptive[0].value!=''){
			res=1;
		}
	}else{
		for(var i=1;i<input.length;i++)
		{
			var questype = input[i].checked;
			if(questype==true) res=1;
		}
	}
	return res;
}

$("#button_nxt").live('click', function()
{
	var id = $('#activeques').val();
	id++;
	
	$('.ques_divs').css('display','none');
	$(".all_ques_li").removeClass('active');
	$('#ulli_'+id).addClass(' active');
	
	$('#div_'+id).css('display','block');
	$('#activeques').val(id);
	
	var countques = $('#countques').val();
	if(countques==id)
	{
		$('#button_nxt').hide();
	}
});

$("#submit").live('click', function()
{
	var id = $('#activeques').val();
	var popupdiv = document.getElementById("div_"+id);
	var textarea = popupdiv.getElementsByTagName("textarea");
	var res = (textarea) ? textarea.length : '';
	if(!res)
	{
		var textbox = popupdiv.getElementsByTagName("input");
		res = commonToInputType(id);
	}
	else
	{
		res = textarea[0].value;
	}
	
	if(res)
	{
		if(!$('#ulli_'+id).hasClass('selected'))
		{ $('#ulli_'+id).addClass(' selected'); }
	}else{
		var ques_id= $("#div_"+id+" #ques_id").val();
		if($.inArray(ques_id,skip_ques)==-1){
			skip_ques.push(ques_id);
		}
	}
	count_skip_ques=skip_ques.length;
	var quesids = $('#quesids').val();
	count_quesids = quesids.split(',').length-1;
	
	if(count_skip_ques==count_quesids){
		alertify.confirm("You have skipped all questions.", function(e){
			if(e){
				window.location.reload();
			}return false;
		});
		return false;
	}
	// else{
		// document.popupForm.submit();
	// }
	
	$('#skipids').val(skip_ques.join(','));
	alertify.confirm("Are you confirm to submit the assessment.", function(e)
	{
		if(e)
		{
			$('#preloader_popup').show();
			var url = $('#baseurl').val() + "/survey/viewquizpopup-submit";
			$.post(url, $("#popupForm").serialize(), function (result)
			{
				if(result)
				{
					$('#guest_id').val(result);
					alertify.alert("Assessment Submitted.");
					// var pp = result.details[0].pass_percentage;
					// var ys = result.details[0].user_score;
					// var rs = result.details[0].user_status;
					// var attempt_taken = result.details[0].attempt_taken;
					// if(!attempt_taken)
					// { $('#button_rtk').hide(); $('#button5').hide(); }
					$('#button_rtk').hide(); $('#button5').hide();
					// $('#pp').text(pp);
					// $('#ys').text(ys);
					// $('#rs').text(rs);
					$('#preloader_popup').hide();
					$('#ques_popup_div').hide();
					$('#user_score_div').show();
				}
			}, "json");
		}
	});
});

$("#review").live('click', function()
{
	$('#preloader_popup').show();
	var guest_id = $('#guest_id').val();
	var assessid = $('#assessid').val();
	var burl = $('#baseurl').val();
	var quesids = $('#quesids').val();
	quesids = quesids.substring(0,quesids.length-1);
	var url = burl + "/survey/reviewassessment";
	$.post(url, {guest_id:guest_id, assessid:assessid, quesids:quesids}, function (result)
	{
		if(result)
		{
			var k = 1;
			quesids = quesids.split(",");
			$.each(result.data, function(key, value)
			{
				var qtype = value.question_type_id;
				var qid = value.qid;
				var res = value.result;
				var ures = value.student_response;
				var cres = value.correct_response;
				if(qtype == '4' || qtype == 4)
				{
					if(res == 'correct' && ures == cres)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						$('input[type=radio][id=yesradio_'+qid+'][value='+cres+']').prop('checked', 'checked');
						$('#yesradio_'+qid+'_'+cres).show();
						$('#yesradio_'+qid+'_'+cres).empty();
						$('#yesradio_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						$('input[type=radio][id=yesradio_'+qid+'][value='+ures+']').prop('checked', 'checked');
						$('#yesradio_'+qid+'_'+cres).show();
						$('#yesradio_'+qid+'_'+cres).empty();
						$('#yesradio_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						$('#yesradio_'+qid+'_'+ures).show();
						$('#yesradio_'+qid+'_'+ures).empty();
						$('#yesradio_'+qid+'_'+ures).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
					}
					else
					{
						$('#yesradio_'+qid+'_'+cres).show();
						$('#yesradio_'+qid+'_'+cres).empty();
						$('#yesradio_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
				}
				else if(qtype == '3' || qtype == 3)
				{
					if(res == 'correct' && ures == cres)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						$('input[type=radio][id=trueval_'+qid+'][value='+cres+']').prop('checked', 'checked');
						$('#trueval_'+qid+'_'+cres).show();
						$('#trueval_'+qid+'_'+cres).empty();
						$('#trueval_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						$('input[type=radio][id=trueval_'+qid+'][value='+ures+']').prop('checked', 'checked');
						$('#trueval_'+qid+'_'+cres).show();
						$('#trueval_'+qid+'_'+cres).empty();
						$('#trueval_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						$('#trueval_'+qid+'_'+ures).show();
						$('#trueval_'+qid+'_'+ures).empty();
						$('#trueval_'+qid+'_'+ures).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
					}
					else
					{
						$('#trueval_'+qid+'_'+cres).show();
						$('#trueval_'+qid+'_'+cres).empty();
						$('#trueval_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
				}
				else if(qtype == '1' || qtype == 1)
				{
					if(res == 'correct' && ures == cres)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						$('input[type=radio][id=multichoiceoption_'+qid+'][value="'+qid+'||'+cres+'"]').prop('checked', 'checked');
						$('#multichoice_'+qid+'_'+cres).show();
						$('#multichoice_'+qid+'_'+cres).empty();
						$('#multichoice_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						$('input[type=radio][id=multichoiceoption_'+qid+'][value="'+qid+'||'+ures+'"]').prop('checked', 'checked');
						$('#multichoice_'+qid+'_'+cres).show();
						$('#multichoice_'+qid+'_'+cres).empty();
						$('#multichoice_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						$('#multichoice_'+qid+'_'+ures).show();
						$('#multichoice_'+qid+'_'+ures).empty();
						$('#multichoice_'+qid+'_'+ures).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
					}
					else
					{
						$('#multichoice_'+qid+'_'+cres).show();
						$('#multichoice_'+qid+'_'+cres).empty();
						$('#multichoice_'+qid+'_'+cres).append('<img src="'+burl+'/public/images/check_icon_new.png">');
					}
				}
				else if(qtype == '6' || qtype == 6)
				{
					ures = ures.split(",");
					cres = cres.split(",");
					if(res == 'correct' && cres.length == ures.length)
					{
						if(!$('#ulli_'+k).hasClass('correct'))
						{
							$('#ulli_'+k).addClass(' correct');
						}
						
						for(var i=0; i<cres.length; i++)
						{
							$('input[type=checkbox][id=mulseloption_'+qid+'_'+cres[i]+'][value="'+qid+'||'+cres[i]+'"]').prop('checked', 'checked');
							$('#multiselect_'+qid+'_'+cres[i]).show();
							$('#multiselect_'+qid+'_'+cres[i]).empty();
							$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						}
					}
					else if(res == 'incorrect' && cres && ures)
					{
						if(!$('#ulli_'+k).hasClass('incorrect'))
						{
							$('#ulli_'+k).addClass(' incorrect');
						}
						
						for(var i=0; i<cres.length; i++)
						{
							if($.inArray(cres[i], ures) != -1)
							{
								$('input[type=checkbox][id=mulseloption_'+qid+'_'+cres[i]+'][value="'+qid+'||'+cres[i]+'"]').prop('checked', 'checked');
								$('#multiselect_'+qid+'_'+cres[i]).show();
								$('#multiselect_'+qid+'_'+cres[i]).empty();
								$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
							}
							else
							{
								$('#multiselect_'+qid+'_'+cres[i]).show();
								$('#multiselect_'+qid+'_'+cres[i]).empty();
								$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
							}
						}
						for(var i=0; i<ures.length; i++)
						{
							if($.inArray(ures[i], cres) == -1)
							{
								$('input[type=checkbox][id=mulseloption_'+qid+'_'+ures[i]+'][value="'+qid+'||'+ures[i]+'"]').prop('checked', 'checked');
								$('#multiselect_'+qid+'_'+ures[i]).show();
								$('#multiselect_'+qid+'_'+ures[i]).empty();
								$('#multiselect_'+qid+'_'+ures[i]).append('<img src="'+burl+'/public/images/cross_icon_new.png">');
							}
						}
					}
					else
					{
						for(var i=0; i<cres.length; i++)
						{
							$('#multiselect_'+qid+'_'+cres[i]).show();
							$('#multiselect_'+qid+'_'+cres[i]).empty();
							$('#multiselect_'+qid+'_'+cres[i]).append('<img src="'+burl+'/public/images/check_icon_new.png">');
						}
					}
				}
				else if(ures && (qtype == '5' || qtype == 5))
				{
					$('#descriptive_'+qid).val(ures);
				}
				else { }
				
				k++;
			});
			
			id = 1;
			$('.ques_divs').css('display','none');
			$('#div_'+id).css('display','block');
			$('#activeques').val(id);
			$('.all_ques_li').removeClass('active');
			$('#ulli_'+id).addClass(' active');
			
			if(!$('.review_class').hasClass('ml40'))
			{ $('.review_class').addClass(' ml40'); }
			
			var countques = $('#countques').val();
			if(countques==id)
			{
				$('#button_nxt').hide();
			}
			
			$('#preloader_popup').hide();
			$('#bottom_btn_div').hide();
			$('#user_score_div').hide();
			$('#ques_popup_div').show();
			$('#bottom_rev_div').show();
			$('input[type=radio]').prop('disabled', 'disabled');
			$('input[type=checkbox]').prop('disabled', 'disabled');
			$('textarea').attr('disabled', 'disabled');
		}
	}, "json");
});