var inprogressservice = angular.module('inprogressservice',[]);

inprogressservice.service('inprogressservice', function($http,$localStorage) {
	 var tokenuse=$("input[name=_token]").val();
	 var authtoken = $localStorage.authtoken;
	 
    this.getData = function(callbackFunc) {
        $http({
            method: 'post',
            url:'/api/inprogress-data',
			data:{_token:tokenuse,authtoken:authtoken}
        }).success(function(data){
            callbackFunc(data);
        }).error(function(){
            alert("error");
        });
     }
	 
	 this.deletedata = function(id){
		$http({
            method: 'post',
            url:'/api/deleteelearningprogress',
			data:{_token:tokenuse,authtoken:authtoken,id:id}
        }).success(function(data){
			
        }).error(function(){
            alert("error");
        });
	 }
});