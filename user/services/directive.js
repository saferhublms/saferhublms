app.directive("assignCoursesGridDirective", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/assigncoursesgrid.html'	
    };
});
app.directive("modeluiGrid", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/modeluigrid.html'	
    };
});
app.directive("classhandOut", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/classhandout.html'	
    };
});
app.directive("learningPlanDirective", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/learningplan.html'	
    };
});
app.directive("pendingApprovalDirective", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/pendingApproval.html'	
    };
});
app.directive("giveCreditDirective", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/givecredit.html'	
    };
});

app.directive("enrollUserToClass", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/enrollUserToClass.html'	
    };
});
app.directive("userpopup", function() {
    return {
		restrict : "E",
		templateUrl : 'user/services/directive_template/userpopup.html'	
    };
});