var requirementservice = angular.module('requirementservice',[]);

requirementservice.service('requirementservice', function($http,$localStorage) {
	 var tokenuse=$("input[name=_token]").val();
	 var authtoken = $localStorage.authtoken;
	 
    this.getData = function(callbackFunc) {
        $http({
            method: 'post',
            url:'/api/requirement-data',
			data:{_token:tokenuse,authtoken:authtoken}
        }).success(function(data){
            callbackFunc(data);
        }).error(function(){
            alert("error");
        });
     }
});