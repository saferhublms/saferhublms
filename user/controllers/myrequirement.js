var myrequirementmodule = angular.module("myrequirementmodule",[]);

myrequirementmodule.controller('MyRequirementController',function($scope,$http,$location,$stateParams,$rootScope,Alertify,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$compile,$filter){



       /* $scope.getmyRequirements=function(){
		   $http({
			   method:"POST",
			   url:"/mylearning/myrequirements"
		   }).success(function(response){
			   alert(response);
		   })
	   }
	   
	    $scope.getmyRequirements();
		
		 */
		
		
 $scope.tokenuse=$("input[name=_token]").val();
	$scope.init=function()
	{
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('ajax', {
			 url: '/mylearning/myrequirements',
			 type: 'POST',
			  data:{_token:$scope.tokenuse},
		 }).withDataProp(function(json) {
			 json.recordsTotal = json.count;
			 json.recordsFiltered = json.count;
			 json.draw = json.draw;
			 $scope.courseData=json.data;
			 //alert(JSON.stringify($scope.courseData));
			
			 return $scope.courseData;
		})
        .withOption('processing', true)
        .withOption('serverSide', true)
		.withOption('createdRow', function (row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		})
		
        .withPaginationType('full_numbers');
		
		
		
		$scope.dtColumns = [
			DTColumnBuilder.newColumn('courseName').withTitle($rootScope.responseLib.coursename).notSortable(),
			DTColumnBuilder.newColumn('trainingType').withTitle($rootScope.responseLib.trainingtype).notSortable(), 
			DTColumnBuilder.newColumn(null).withTitle($rootScope.responseLib.duedate).notSortable().renderWith(datedata),
			DTColumnBuilder.newColumn('assignType').withTitle($rootScope.responseLib.assign).notSortable(),
			DTColumnBuilder.newColumn('isMandatory').withTitle($rootScope.responseLib.assigntype).notSortable(),
			DTColumnBuilder.newColumn(null).withTitle($rootScope.responseLib.view).notSortable()
            .renderWith(actionsHtml)
		];
		
	}
	$scope.init();
	
	
	function actionsHtml(data, type, full, meta) {
		if(data.trainingTypeId==1){
			return '<a class="change_col button_lblue_r4" href="#/elearningmedia/training/'+data.traningProgramId+'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
		}else if(data.trainingTypeId==5){
			return '<a class="change_col button_lblue_r4" href="#/course/viewassessment/'+data.traningProgramId+'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
		}else if(data.trainingTypeId==2 || data.trainingTypeId==3){
			return '<a class="change_col button_lblue_r4" href="#/classroomcourse/training/'+data.traningProgramId+'" onfocus="if(this.blur)this.blur();">View<span>&#187;</span></a>';
		}else{
			return '';
		}   
    }
	
	function datedata(data, type, full, meta){
		 $scope.formatedDate = $filter('date')(data.dueDate, data.dateFormat);
		return  $scope.formatedDate
	}
	
	
	

});
