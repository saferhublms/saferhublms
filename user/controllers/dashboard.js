
var dashboardmodule = angular.module("dashboardmodule",[]);
dashboardmodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("dashboard", {
		url:"/dashboard",
		views: {
			
			"mainView":{
				templateUrl: '/user/templates/dashboard.html',
				controller: 'DashboardController'
			}
		},
	});	
	
}]);
	
	
dashboardmodule.controller('DashboardController', function($scope,$location,$localStorage,$http){

	    $scope.getLeftNav=function(){
			$http({
				method:"POST",
				url:"/api/getLeftNav",
			}).success(function(response){
				$scope.leftMenuData=response.leftMenu;
				//alert(JSON.stringify($scope.leftMenuData));
			})
		}
		
		$scope.getLeftNav();
		
		
		$scope.addclass=function(menu_item_id){ 
			var menuItem=angular.element(document.querySelector('#cat_nav_'+menu_item_id));
			menuItem.addClass('active');
			var menuItem1=angular.element(document.querySelector('#cat_nav_'+menu_item_id+' ul')).css({ display: "block" });
			//alert(menu_item_id);
		}
});	
