
var loginmodule = angular.module("loginmodule",[]);
loginmodule.config(["$stateProvider",function($stateProvider){
	
   $stateProvider.state("home", {
		url:"",
		views: {
			/* "mainView":{
				templateUrl: '/user/templates/index.html',
				controller: 'HomeController'
			} */
      "mainView":{
				templateUrl: '/user/templates/login.html',
				controller: 'LoginController'
			}
		},onEnter: ['$rootScope', function( $rootScope) { $rootScope.notShowHeader = 1;	}]
	});	
	 
	$stateProvider.state("login", {
		url:"/login",
		views: {
			"mainView":{
				templateUrl: '/user/templates/login.html',
				controller: 'LoginController'
			}
		},
		onEnter: ['$rootScope', function( $rootScope) { $rootScope.notShowHeader = 1;	}]
	});	
	
	$stateProvider.state("userapp", {
	url:"/index/apps",
	views: {
		"mainView":{
			templateUrl: '/user/templates/apps.html',
			controller: 'appsController'
		}
	},onEnter: ['$rootScope', function( $rootScope) { $rootScope.notShowHeader = 0;	}]
	});	
	
	
	
}]);
 




loginmodule.controller('HomeController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams)
{   


});

loginmodule.controller('LoginController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams)
	{       
	         $scope.tokenuse=$("input[name=_token]").val();
			 //console.log($scope.tokenuse);
		     $scope.userdata={'authtoken':''};
			 
             $scope.login=function(errorcode){
				
					if(errorcode){
						var logindata={'user_name':$scope.username,'password':$scope.password,errorcode:errorcode,_token:$scope.tokenuse};
					}else{
						var logindata={'user_name':$scope.username,'password':$scope.password,_token:$scope.tokenuse}
					}
				 $http({
					 method:"post",
					 url:'/api/login',
					 data:logindata
				 }).success(function(response){
					 if(response.status==200){
						//alert(response.authToken);
						 $scope.userdata.authtoken=response.authToken;
						 $localStorage.$default($scope.userdata);
						 $window.location.href='#/index/apps';
					   }else if(response.errorcode){ 
					          $scope.login(response.errorcode);return false;
					   }else{
						   alert(response.message);
					   }
				   }
				 );
			 }

	});

loginmodule.controller('appsController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams)
{   



$scope.tokenuse=$("input[name=_token]").val();
	   $scope.init = function(){
		  $http({
				url:'/api/getAppMenus',
				method:'post',
				data:{'authToken':$localStorage.authtoken,_token:$scope.tokenuse}
			}).success(function(response){
		    $scope.userapp=response.navigation_str;
		    $scope.userappDynamic=response.boxData;
			});
	  }
	
   $scope.init();
   
   
   $scope.getAppNav = function(){
		  $http({
				url:'/api/getAppNav',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.appsheadernav=response;
				//alert(response);
		    
			});
	  }
	
   $scope.getAppNav();
   
   $scope.getFooterNav = function(){
		  $http({
				url:'/api/getFooterNav',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.footernav=response;
				//alert(response);
		    
			});
	  }
	
   $scope.getFooterNav();
   
   
   $scope.companyCmsInfo=function(){
		  $http({
				url:'/api/companyCmsInfo',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.companyCmsInfo=response;
				//alert(JSON.stringify(response.companyInfo));
		    
			});
	  }
	
   $scope.companyCmsInfo();
 
  $scope.getLabel=function(){
	   $http({
		   method:"POST",
		   url:"/api/changelocale/locale/us"
	   }).success(function(response){
		   $scope.responseLib=response;
		   alert($scope.responseLib.translationInfoModule.TranslationRepository.getDefaultLocale());
		   alert(JSON.stringify(response));
	   })  
  }
  $scope.getLabel(); 

  
   
  
   


});
