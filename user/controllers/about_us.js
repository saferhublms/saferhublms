'use strict';

var aboutusmodule = angular.module("aboutusmodule",[]);
/* aboutusmodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("aboutus", {
		url:"/index/about",
		views: {
			
			"mainView":{
				templateUrl: '/user/templates/helpcontent/about_us.html',
				controller: 'aboutusController'
			}
		},
	});	
	$stateProvider.state("announcement", {
		url:"/index/announcement",
		views: {
			
			"mainView":{
				templateUrl: '/user/templates/helpcontent/announcements.html',
				controller: 'announcementsController'
			}
		},
	});	
	
 $stateProvider.state("help", {
		url:"/index/help",
		views: {
			
			"mainView":{
				templateUrl: '/user/templates/helpcontent/help.html',
				controller: 'helpController'
			}
		},
	}); 
		$stateProvider.state("helpreference", {
		url:"/index/view-reference/viewid/:viewid",
		views: {
			
			"mainView":{
				templateUrl: '/user/templates/helpcontent/view-reference.html',
				controller: 'referenceController'
			}
		},
	}); 

	
}]);
	 */
	
aboutusmodule.controller('aboutusController', function($scope,$location,$http,$window){
	
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.viewAbout=function(){
	$http({
				method:"POST",
				url:'/index/getaboutus',
				data:{_token:$scope.tokenuse}
			}).success(function(response){
				$scope.page_name=response.aboutus[0].page_name;
				$scope.page_description=response.aboutus[0].page_description;
				
			}) 
	}
	$scope.viewAbout();
  
});
	
aboutusmodule.controller('announcementsController', function($scope,$location,$http,$window){
	
	$scope.tokenuse=$("input[name=_token]").val();
 $scope.viewannouncement=function(){
	  $http({
					method:"POST",
					url:'/index/getannouncement',
					data:{_token:$scope.tokenuse}
				}).success(function(response){
					$scope.announcementstr=response.announcement;
					$scope.newdate=response.newdate;
					$scope.systemuser=response.systemuser;
				}) 
	}
	$scope.viewannouncement();
	
	$scope.display12=function(i,counter)
	{
		
		for(var j=1;j<=counter;j++){
			if( j==i ){
			document.getElementById("readmore_"+i).style.display = 'inline';
			document.getElementById("reads_"+i).style.display = 'none';
			document.getElementById("readnotmore_"+i).style.display = 'none';
			}
			else{
			document.getElementById("readmore_"+i).style.display = 'inline';
			document.getElementById("reads_"+i).style.display = 'none';
			document.getElementById("readnotmore_"+i).style.display = 'none';
			}
		}
	}
	
	$scope.less=function(i,counter)
	{
		document.getElementById("readmore_"+i).style.display = 'none';
		document.getElementById("reads_"+i).style.display = 'inline';
		document.getElementById("readnotmore_"+i).style.display = 'inline';
	}
	
});

aboutusmodule.controller('helpController', function($scope,$location,$http,$window){
	
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.gethelpcontent=function(){
	$http({
				method:"POST",
				url:'/index/helpAction',
				data:{_token:$scope.tokenuse}
			}).success(function(response){
				$scope.topic_detail=response;
			}) 
	}
	$scope.gethelpcontent();
	
	var myAcc;
	var eventIndex = 1;
	
	$scope.questionlist=function(topic_id)
	{
		
		$http({
			method:"POST",
			url:'/index/topicdetails',
			data:{_token:$scope.tokenuse,topic_id:topic_id}
		}).success(function(jsonText){
		if(parseInt(jsonText.length)!=0)
		{
			myAcc = new dhtmlXAccordion({parent: "accObj", multi_mode: true});
			myAcc.forEachItem(function(item){myAcc.cells(item._idd).hide()});
			var open_id=0;
			var cnt=0;

			jQuery.each( jsonText, function( i, val ) {			
			myAcc.addItem(val.qid, val.question).attachHTMLString(val.answer);
			myAcc.cells(val.qid).setHeight(200);
			myAcc.cells(val.qid).close();
			$('#question_'+val.qid).removeClass('active');
			if(cnt==0) open_id=val.qid;
			cnt++;
			});	

			myAcc.cells(open_id).open();
			$('#question_'+topic_id).addClass('active');
		}
		})
	}
	
	$scope.gethelpcontent=function(){
	$http({
				method:"POST",
				url:'/index/allreferences',
				data:{_token:$scope.tokenuse}
			}).success(function(response){
				$scope.gridOptions.data = response;
				//$scope.topic_detail=response;
			}) 
	}
	$scope.gethelpcontent();
  
		$scope.gridOptions = { 
		cellEditableCondition: true,
		enableRowSelection: false,
		enableSelectAll: true,
		enableFiltering: true,
		paginationPageSize:10,
		paginationPageSizes: [10, 20, 30],
		rowHeight: 40,
		onRegisterApi : function(gridApi){

		},


		columnDefs: [	
		{ name: 'title',displayName: 'Title' ,cellTemplate:"<div compile=row.entity.title>",width:'20%'},
		{ name: 'category_name',displayName: 'Category',width:'20%'},
		{ name: 'view',displayName: 'View',cellTemplate:"<div compile=row.entity.view>",enableFiltering: false,width:'20%'},
		{ name: 'button',displayName: 'Actions',cellTemplate:"<div compile=row.entity.button>",enableFiltering: false,width:'50%'},
		]
		};
  
  
});

aboutusmodule.controller('referenceController', function($scope,$location,$http,$window,$stateParams){
    var viewid=	$stateParams.viewid;
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.getreference=function(){
	$http({
				method:"POST",
				url:'/index/getreference',
				data:{_token:$scope.tokenuse,'viewid':viewid}
			}).success(function(response){
				$scope.buttonclass 				=response.buttonclass;
				$scope.ref_id     				=response.ref_id;
				$scope.getmedia   				=response.getmedia;
				$scope.references  			 	=response.references;
				$scope.referencecategory   		=response.referencecategory;
				$scope.company_id   	     	=response.company_id;
				
			}) 
	}
	$scope.getreference();
	$scope.viewuser=function(id,media){
		
	 	var media_type=media[0].media_id;
		var title='';
		if(media_type==5){		
		tb_show(title='','/index/embeddedplayer/file_name/'+id+'?height=400&width=700&inlineId=hiddenModalContent&modal=false','');
		}else{
		tb_show(title='','/index/mediaplayer/file_name/'+id+'?height=600&width=1000&inlineId=hiddenModalContent&modal=false','');
		} 
		
	}
  
});