var supervisiortool = angular.module("supervisiortool",[]);
/* supervisiortool.config(["$stateProvider",function($stateProvider){
	
   $stateProvider.state("/supervisor/supervisortools", {
		url:"/supervisor/supervisortools",
		views: {
			"mainView":{
				templateUrl: '/user/templates/supervisor/supervisortools.html',
				controller: 'SupervisiorTool'
			}
		},
	});	
	 
}]);
 
 */



supervisiortool.controller('SupervisiorTool', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams)
{   
    $scope.tokenuse=$("input[name=_token]").val();
    $scope.loader = false;
      var init = function(){
		 $http({
					 method:"post",
					 url:'/api/supervisor-reports',
					 data:{_token:$scope.tokenuse}
				 }).success(function(response){
					  $scope.gridOptions.data = response;
				   });
	}
	
	init();
	
	 $scope.gridOptions = { 
			  cellEditableCondition: true,
			/*   enableFullRowSelection : true, */
			   enableRowSelection: false,
               enableSelectAll: true,
			  enableFiltering: true,
              paginationPageSize:10,
			  paginationPageSizes: [10, 20, 30],
			  rowHeight: 40,
			  onRegisterApi : function(gridApi){
				$scope.gridApis= gridApi;
			    // $scope.gridOptions.gridApi.refresh();
			},

			 
			 columnDefs: [	
			  { name: 'name',displayName: 'Name' ,cellTemplate:"<div compile=row.entity.name>"},
			  { name: 'title',displayName: 'Title'},
			  { name: 'learning_plan',displayName: 'Learning Plan',cellTemplate:"<div compile=row.entity.learning_plan>",enableFiltering: false},
			  { name: 'traning',displayName: 'Traning',displayName: 'Learning Plan',cellTemplate:"<div compile=row.entity.traning>",enableFiltering: false},
			  { name: 'properties',displayName: 'Properties',cellTemplate:"<div compile=row.entity.properties>",enableFiltering: false},
			 ]
	 
	 };
	 
	 
	 
	  
	 
	 
	 $scope.openviewpopup = function(type,edituserid){
		  
	    if(type=='unassignedtraining'){
			$scope.loader = true;
	         $http({
					 method:"post",
					 url:'/api/supervisor/trainingcatolgdataarr',
					 data:{edituserid:edituserid,'_token':$scope.tokenuse}
				 }).success(function(response){
					 $scope.loader = false;
					 $scope.edituserids = edituserid;
					  $scope.gridAssign.data = response.data;
					  $scope.category = response.category;
					   $('#myModal').modal('show');
				   });
	    }
	   if(type=='viewassignment'){
		   $scope.loader = true;
	         $http({
					 method:"post",
					 url:'/api/supervisor/supervisorlearningplan',
					 data:{edituserid:edituserid}
				 }).success(function(response){
					  $scope.loader = false;
					  $scope.gridLearningplan.data = response;
					  $('#learningplan').modal('show');
				   });
		   
		
	 }
	if(type=='viewprofile'){	
	     $http({
			method:"POST",
			url:"/supervisor/userpopup",
			data:{edituserid:edituserid}
		}).success(function(response){
			$scope.userpopupData=response;
			//$scope.selectedviewuser();
			$('#userpopup').modal('show');
			//alert(response);
		})
	} 
		 
	 }
	 
	  $scope.gridAssign = { 
			   cellEditableCondition: true,
			/*   enableFullRowSelection : true, */
			   enableRowSelection: false,
               enableSelectAll: true,
			  enableFiltering: true,
              /* paginationPageSize:10,
			  paginationPageSizes: [10, 20, 30], */
			  rowHeight: 40,
			  onRegisterApi : function(gridApi){
				$scope.assigngrid = gridApi;
			   // $scope.gridApi.refresh();
				$scope.gridApi.grid.handleWindowResize();
				
			},

			 
			 columnDefs: [	
			  { name: 'item_name',displayName: 'Item Name'},
			  { name: 'course_id',displayName: 'Course ID'},
			  { name: 'start_date',displayName: 'Start Date'},
			  { name: 'credits',displayName: 'Credits'},
			  { name: 'item_type',displayName: 'Item Type'},
			 ]
	 
	 }; 
	 
	 
	 
	 
	 
	 
	$scope.categoryfillter = function(){
		
		if(!$scope.setitemtypelist){
			$scope.setitemtypelist = 'ZzNQc';
		} 
		
		if(!$scope.setcategorylist){
			$scope.setcategorylist = '';
		}
		
		$scope.loader = true;
	         $http({
					 method:"post",
					 url:'/api/supervisor/categoryfillter',
					 data:{itemType:$scope.setitemtypelist,categoryType:$scope.setcategorylist,edituserid:$scope.edituserids,_token:$scope.tokenuse}
				 }).success(function(response){
					 $scope.loader = false;
					 $scope.gridAssign.data = response.data;
					 
				   });
		
	}
	
	
	
	
	
	$scope.assignselectedcoursestouser = function(){
		var assignselectedcourses='assignselectedcourses';
		$scope.course = $scope.assigngrid.selection.getSelectedRows();
		alert($scope.course);
		$scope.selectcourse = [];

		for(var i=0; i<$scope.course.length;i++){
			var id1=$scope.course[i].id.split('||');
			var id2=id1[1];
			$scope.selectcourse.push(id2);
		} 
		var arr = $scope.selectcourse.join(",");
		if(!$scope.modal.dt){
			alertify.alert("Please Select Due date to assign any Assignment.");
		    return false;
		}
		if($scope.selectcourse.length == 0){
		alertify.alert("Please Select Courses.");
		return false;
	   }
	   
	   
		$http({
			method:"post",
			 url:'/api/supervisor/assigntrainingtousers',
			 data:{mode:assignselectedcourses,selectedusers:$scope.edituserids,training_program:arr,duedate:$scope.modal.dt,assign_startdate:'',_token:$scope.tokenuse}
		 }).success(function(response){
			 $scope.loader = false;
			 if(response.status == 200){
			   alertify.alert("The assignment has been sent successfully.");
				$('#myModal').modal('hide');
			 }					 
		   });
		
	}
	
	
	
	
	 $scope.gridLearningplan = { 
			   cellEditableCondition: true,
			   enableRowSelection: false,
               enableSelectAll: true,
			   enableFiltering: true,
			   rowHeight: 40,
			   onRegisterApi : function(gridApi){
				$scope.gridApi = gridApi;
			  //  $scope.gridApi.refresh();
				$scope.gridApi.grid.handleWindowResize();
				
			 },

			 
			 columnDefs: [	
			  { name: 'learning_plan',displayName: 'Learning Plan'},
			  { name: 'learning_plan_items',displayName: 'Learning Plan Items'},
			  { name: 'traning_type',displayName: 'Traning Type'},
			  { name: 'credits',displayName: 'Credits'},
			  { name: 'flag',displayName: 'Flag',cellTemplate:"<div compile=row.entity.flag>"},
			  { name: 'expiration',displayName: 'Expiration'},
			  { name: 'due_date',displayName: 'Due Date'},
			  { name: 'progress',displayName: 'Progress',cellTemplate:"<div compile=row.entity.progress>"},
			 ]
	 
	 }; 
	 
	 $scope.getSuperVisorSetting=function(){
		 $http({
			 method:"GET",
			 url:"/supervisor/getSupervisorSetting"
		 }).success(function(response){
			 $scope.supervisorSetting=response;
			 //alert($scope.supervisorSetting.notify);
		 })
	 }
	 $scope.getSuperVisorSetting();
	 
	 
	 $scope.checkoperationforcourseornotify=function(operation)
	 {
		 if($scope.supervisorSetting.autoapprove==true){
			var courseapp=1; 
		 }else{
			var courseapp=0; 
		 }
			
		if($scope.supervisorSetting.notify==true){
			var notify=1;
		}else{
			var notify=0;
		}
		
		$http({
			method:"POST",
			url:"supervisor/supervisortools",
			data:{"mode":operation,"notify":notify,"courseapp":courseapp}
		}).success(function(response){
			if(response=='tonotifyupdate1'){
			alertify.alert("You will be notified through email with any approval requests.");
			}
			if(response=='toapprovecourse1'){
				alertify.alert("You have set all Pending Approvals as Approved.");
			}
			if(response=='toapprovecourse0'){
				alertify.alert("You have set all Pending Approvals as not auto Approved.");
			}
			if(response=='tonotifyupdate0'){
				alertify.alert("You Will no longer notifiy for any approval request.");
			}
		})
	 }
	
	
	 /*  $scope.submittraining = function(){
		  
	  } */
	  
	  $scope.opentrainingpopup = function (){
		  
		$scope.usersids = $scope.gridApis.selection.getSelectedRows();
		$scope.selectuser = [];

		for(var i=0; i<$scope.usersids.length;i++){
			var id1=$scope.usersids[i].id;
			$scope.selectuser.push(id1);
		} 
	    var arr = $scope.selectuser.join(",");
	    $scope.openviewpopup('unassignedtraining',arr);
		   }
		   $scope.setcat='0';
		   $scope.gridDataLoad=function()
			{
				$scope.dynamicTop='33px';
				 $scope.modeView='';
				$http({
					method:"POST",
					url:"supervisor/pendingpop1",
					data:{'operation': $scope.setcat}
				}).success(function(response){
					//$scope.pendingApprovalData=response;
					$scope.gridPendingApproval.data=response.gridData;
					$('#pendingApproval').modal('show');
					//alert(response);
				})
			}
			
			
			$scope.gridPendingApproval = { 
			//data :'pendingApprovalData.gridData',
			   cellEditableCondition: true,
			   enableRowSelection: false,
               enableSelectAll: true,
			   enableFiltering: true,
			   rowHeight: 40,
			   onRegisterApi : function(gridApi){
				$scope.gridApiPending = gridApi;
			  //  $scope.gridApiPending.refresh();
				$scope.gridApiPending.grid.handleWindowResize();
				
			 },

			 
			 columnDefs: [	
			  { name: 'name',displayName: 'Name'},
			  { name: 'item',displayName: 'Item'},
			  { name: 'training_date',displayName: 'Traning Data'},
			  { name: 'item_type',displayName: 'Item Type'},
			  { name: 'view',displayName: 'Details',cellTemplate:"view.html" ,enableFiltering: false},
			  { name: 'uri',displayName: 'Action',cellTemplate:"approve.html" ,enableFiltering: false},
			  { name: 'functionName',displayName: 'Action',cellTemplate:"deney.html" ,enableFiltering: false},
			 ]
	 
	 }; 
	 
	 
	 $scope.filterPendingRequest=function(){
		  $scope.dynamicTop='33px';
		  $scope.modeView='';
		 //$scope.pendingApprovalData.gridData=[];
		 $http({
					method:"POST",
					url:"supervisor/pendingpop1",
					data:{'operation': $scope.setcat}
				}).success(function(response){
					//$scope.pendingApprovalData=response;
					$scope.gridPendingApproval.data=response.gridData;
				})
	 }
	 $scope.dynamicTop='33px';
	 $scope.viewuser=function(usertrainingid)
	 {
		 $scope.modeView='selectedviewuser';
		$http({
			method:"POST",
			url:"/supervisor/pendingpop",
			data:{'mode':'selectedviewuser','usertrainingid':usertrainingid}
		}).success(function(response){
			if(response.mode=='selectedviewuser'){	
				$scope.responseData=response;
				$scope.userdetail=response.userdetail;
				$scope.dynamicTop='202px';
			}
		})
	 }
	 
	 
	  $scope.viewclassroom=function(usertrainingid)
	  {
		 $scope.modeView='selectedclassroom';
		$http({
			method:"POST",
			url:"/supervisor/pendingpop",
			data:{'mode':'selectedclassroom','usertrainingid':usertrainingid}
		}).success(function(response){
			if(response.mode=='selectedclassroom'){
				$scope.responseData=response;
				$scope.userdetail=response.userdetail;
				$scope.dynamicTop='202px';
			}
		})
	 }
	 
	 $scope.approveagainprofilebysupervisor=function(mode,usertrainingid,row)
  	 {
		 //alert(mode+" "+usertrainingid);return false;
		$scope.loadingiddis=true;
		 if(mode=='classroom'){
			var operation='approved'; 
		 }
		if(mode=='submittraining'){
			var operation='submitted';
		}
		
         $http({
			 method:"POST",
			 url:"supervisor/pendingpop",
			 data:{mode:operation,usertrainingid:usertrainingid}
		 }).success(function(response){
			alertify.alert("Approve Successfully.");
			$scope.loadingiddis=false;
			if(row){
				var index = $scope.gridPendingApproval.data.indexOf(row.entity);
				$scope.gridPendingApproval.data.splice(index, 1);
			}else{
					$scope.modeView='';
					$scope.dynamicTop='33px';
					$scope.gridDataLoad();
				}
		 })
	 }

	 
	 $scope.denyagainprofilebysupervisor=function(mode,usertrainingid,row)
	{    
			var operation='';
			$scope.loadingiddis=true;
			if(mode=='classroom'){
				var operation='deny';
			} 
			if(mode=='submittraining'){
				var operation='deny';	
			}
			$http({
				 method:"POST",
				 url:"supervisor/pendingpop",
				 data:{mode:operation,usertrainingid:usertrainingid}
			 }).success(function(response){
				alertify.alert("Denied Successfully.");
				$scope.loadingiddis=false;
				if(row){
					var index = $scope.gridPendingApproval.data.indexOf(row.entity);
				$scope.gridPendingApproval.data.splice(index, 1);
				}else{
					$scope.modeView='';
					$scope.dynamicTop='33px';
					$scope.gridDataLoad();
				}
			 })
				
	}
	
	
	$scope.usersubmittrainingstatus=function()
	{
		$scope.usersids = $scope.gridApiPending.selection.getSelectedRows();
		$scope.selectuser = '';

		for(var i=0; i<$scope.usersids.length;i++){
			$scope.selectedUserId=$scope.usersids[i].id;
			if($scope.selectuser==''){
				$scope.selectuser=$scope.selectedUserId;
			}else{
				$scope.selectuser=$scope.selectuser+","+$scope.selectedUserId;
			}
		} 
		
		if(!$scope.operationvalueofsubmit) {
			alertify.alert("Please select a bulk option.");
		   return false;
		}
		if($scope.setcat == 0) {
			  if($scope.selectuser=='') {
				  alertify.alert("Please Select Users to apply Action.");
			      return false;
			  }
			  
			  if($scope.operationvalueofsubmit=='approveselected') {
				  $scope.loadingiddis=true;
				 var operation='bothapprove';
				   $http({
						 method:"POST",
						 url:"supervisor/pendingpop",
						 data:{mode:operation,selectedusers:$scope.selectuser}
					 }).success(function(response){
						alertify.alert("Approved Successfully.");
						$scope.loadingiddis=false;
						 $scope.gridDataLoad();
					 })
			  }else if($scope.operationvalueofsubmit=='denyselected'){
				   $scope.loadingiddis=true;
				   var operation='bothdeny';
				   $http({
						 method:"POST",
						 url:"supervisor/pendingpop",
						 data:{mode:operation,selectedusers:$scope.selectuser}
					 }).success(function(response){
						alertify.alert("Denied Successfully.");
						$scope.loadingiddis=false;
						 $scope.gridDataLoad();
					 })			
			  }
		}else{
			if($scope.selectuser=='') {
				  alertify.alert("Please Select Users to apply Action.");
			      return false;
			  }
			  
			  if($scope.setcat==1)
				{ 
					if($scope.operationvalueofsubmit=='approveselected') {
						 $scope.loadingiddis=true;
						 var operation='approveselected';
						   $http({
								 method:"POST",
								 url:"supervisor/pendingpop",
								 data:{mode:operation,selectedusers:$scope.selectuser}
							 }).success(function(response){
								 alertify.alert("Approved Successfully.");
								 $scope.loadingiddis=false;
								 $scope.gridDataLoad();
							 })
					}else if($scope.operationvalueofsubmit=='denyselected'){
						$scope.loadingiddis=true;
						 var operation='denyselected';
						   $http({
								 method:"POST",
								 url:"supervisor/pendingpop",
								 data:{mode:operation,selectedusers:$scope.selectuser}
							 }).success(function(response){
								 alertify.alert("Denied Successfully.");
								 $scope.loadingiddis=false;
								 $scope.gridDataLoad();
							 })
					}
			 }else if($scope.setcat==2){
				  $scope.loadingiddis=true;
			     if($scope.operationvalueofsubmit=='approveselected'){
					 var operation='approvesubmit';
					 
					 $http({
						 method:"POST",
						 url:"supervisor/pendingpop",
						 data:{mode:operation,selectedusers:$scope.selectuser}
					 }).success(function(response){
						 alertify.alert("Approved Successfully.");
						 $scope.loadingiddis=false;
						 $scope.gridDataLoad();
					 })
				 }else if($scope.operationvalueofsubmit=='denyselected'){
					 var operation='denysubmit';
					 $http({
						 method:"POST",
						 url:"supervisor/pendingpop",
						 data:{mode:operation,selectedusers:$scope.selectuser}
					 }).success(function(response){
						 alertify.alert("Denied Successfully.");
						 $scope.loadingiddis=false;
						 $scope.gridDataLoad();
					 })
				 }
			 }
		}
	}
	
	
	
	$scope.openclasspopup=function(){
		$scope.usersids = $scope.gridApis.selection.getSelectedRows();
		$scope.selectuserSuperviSorUser = '';

		for(var i=0; i<$scope.usersids.length;i++){
			$scope.selectedUserId=$scope.usersids[i].normalId;
			if($scope.selectuserSuperviSorUser==''){
				$scope.selectuserSuperviSorUser=$scope.selectedUserId;
			}else{
				$scope.selectuserSuperviSorUser=$scope.selectuserSuperviSorUser+","+$scope.selectedUserId;
			}
		} 
		
		if($scope.selectuserSuperviSorUser=='') {
				  alertify.alert("Please Select Users to apply Action.");
			      return false;
			  }
			  $http({
				  method:"POST",
				  url:"/supervisor/get-all-class",
				  data:{edituserid:$scope.selectuserSuperviSorUser}
			  }).success(function(response){
				  $scope.enrollUser.data=response.gridData;
				 // alert(JSON.stringify(response));
				  $('#enrollusertoclass').modal('show');
			  })
			  
	}
	
	
	$scope.enrollUser = { 
			//data :'pendingApprovalData.gridData',
			   cellEditableCondition: true,
			   enableRowSelection: false,
               enableSelectAll: true,
			   enableFiltering: true,
			   rowHeight: 40,
			   onRegisterApi : function(gridApi){
				$scope.gridApienrollUser = gridApi;
			//    $scope.gridApienrollUser.refresh();
				$scope.gridApienrollUser.grid.handleWindowResize();
			 },

			 
			 columnDefs: [	
			  { name: 'class_name',displayName: 'Title'},
			  { name: 'instructor_name',displayName: 'Instructor'},
			  { name: 'location',displayName: 'Location'},
			  { name: 'recursive_date',displayName: 'Date'},
			  { name: 'max_seat',displayName: 'Seat'},
			  { name: 'leftseat',displayName: 'Available'},
			 
			 ]
	 
	 }; 
	 
/* $scope.usersids = $scope.grid.selection.getSelectedRows(); */
	 $scope.submittraining=function(){
		$scope.usersids = $scope.gridApis.selection.getSelectedRows();
		$scope.selectuser = [];

		for(var i=0; i<$scope.usersids.length;i++){
		var id1=$scope.usersids[i].id;
			$scope.selectuser.push(id1);
		} 
		var arr = $scope.selectuser.join(",");
		if(!arr)
		{
			alertify.alert("You must first select who you want to receive training credit.");
			return false;
		}
		
		$http({
		  method:"POST",
		  url:"/supervisor/givecredit",
		  data:{checkeduser:arr}
		}).success(function(response){
			   $scope.userid=response.userid;
			   $scope.compid=response.compid;
			   $scope.checkeduser=response.checkeduser;
			   $scope.allusers=response.allusers;
			   $scope.instructorlist=response.instructorlist;
			   $scope.LockedStatus=response.LockedStatus;
			   $scope.certificatename=response.certificatename;
			   $scope.Trainingtypessarray=response.Trainingtypessarray;
			   $scope.Getdate_formatecomapny=response.Getdate_formatecomapny;
			   $scope.calenderdateformate=response.calenderdateformate;
			   $scope.skillcategory=response.skillcategory;
		    /*    $scope.enrollUser.data=response.gridData; */
		
			$scope.checkcatset =$scope.LockedStatus[0].category_management;
			if($scope.checkcatset == 1)
			{
				
				$("#skillcategoryid").select2({
				tags:$scope.skillcategory,
				createSearchChoice: function() { return null; },
				separator:",",
				multiple: true});
			}
			else
			{
				$("#skillcategoryid").select2({
				tags:$scope.skillcategory,
				separator: ",",
				multiple: true});
			}
			
           	$("#assess_duedate").datepicker({dateFormat:  $scope.calenderdateformate,changeMonth: true, changeYear: true, showOn: 'button', buttonImageOnly: true, buttonImage: '/eliteprodlms/public/images/cal_new_img.png'});
		
			
		    $('#givecredit').modal('show');
		})

	 }

	

	 
		$scope.gridcreditOptions = { 
		cellEditableCondition: true,
		enableRowSelection: false,
		enableSelectAll: true,
		enableFiltering: true,
		paginationPageSize:10,
		paginationPageSizes: [10, 20, 30],
		rowHeight: 40,
		expandableRowTemplate: 'expandableRowTemplate.html',
		expandableRowHeight: 150,
		expandableRowScope: {
		  subGridVariable: 'subGridScopeVariable'
		},
		onRegisterApi : function(gridApi){
		$scope.gridcreditApis= gridApi;
		// $scope.gridOptions.gridApi.refresh();
		},


		columnDefs: [
				
		{ name: 'training_title',displayName: 'Item Name'},
		{ name: 'itemtype',displayName: 'Training Type'}
		]

		};
	 
	//
	 $scope.checkselectedclass=function()
	 {
		 $scope.classIds = $scope.gridApienrollUser.selection.getSelectedRows();
		 $scope.selectClassId = '';

		for(var i=0; i<$scope.classIds.length;i++){
			$scope.selectedUserId=$scope.classIds[i].id;
			if($scope.selectClassId==''){
				$scope.selectClassId=$scope.selectedUserId;
			}else{
				$scope.selectClassId=$scope.selectClassId+","+$scope.selectedUserId;
			}
		} 
		if($scope.selectClassId=='')
		{
			alertify.alert("Please Select Class.");		
			return false;
		}
		$scope.loadingiddis=true;
		$http({
			method:"POST",
			url:"/supervisor/enroll-user-to-class",
			data:{class_id:$scope.selectClassId,user_id:$scope.selectuserSuperviSorUser}
		}).success(function(response){
			alertify.alert(response);
			$('#enrollusertoclass').modal('hide');
			 $scope.loadingiddis=false;
		})
		
	 }
	 
	 $scope.tab1=function()
	{

		document.getElementById('tab1').style.display = 'block';
		document.getElementById('tab2').style.display = 'none';
		document.getElementById('tab_pp').className='tab_current-184 hand w176 h25 fl fs12  fc01 fta b pl07 pt05';
		document.getElementById('tab_zz').className='tab_blue-147 hand w145 h25 fl fs12  fc01 fta b pl10 pr20 pt05';
	}
	
	$scope.tab2=function()
	{
		document.getElementById('tab1').style.display = 'none';
		document.getElementById('tab2').style.display = 'block';
		document.getElementById('tab_zz').className='tab_current-147  hand w145 h25 fl fs12  fc01 fta b pl10 pr20 pt05';
		document.getElementById('tab_pp').className='tab_blue_first-184 hand w175 h25 fl fs12 fc01 fta b pl07 pt05';
	}
	
	
	
		$scope.clsrating=function(rateVal){
        for(var i=1;i<=rateVal;i++){
            document.getElementById('clsrate_'+i).innerHTML = '<img  src="/eliteprodlms/public/images/star_yes.png" onclick="clsrating('+i+');" />';
        }
        for(var j=rateVal+1;j<=5;j++){

            document.getElementById('clsrate_'+j).innerHTML = '<img  src="/eliteprodlms/public/images/star_no.png" onclick="clsrating('+j+');" />';
        }

        document.getElementById('clas_star').value = rateVal;

    }
	
	$scope.insrating=function(rateVal){
        for(var i=1;i<=rateVal;i++){
            document.getElementById('insrate_'+i).innerHTML = '<img  src="/eliteprodlms/public/images/star_yes.png" onclick="insrating('+i+');" />';
        }
        for(var j=rateVal+1;j<=5;j++){

            document.getElementById('insrate_'+j).innerHTML = '<img  src="/eliteprodlms/public/images/star_no.png" onclick="insrating('+j+');" />';
        }
        document.getElementById('ins_star').value = rateVal;
    }
	
	$scope.changetrainingtype=function()
	{
		
		if($scope.traningtypeid==1)
		{
			document.getElementById("course_rating").style.display='none';
			document.getElementById("instructor_rating").style.display='none';
			document.getElementById("instuctornames").style.display='none';
			document.getElementById("elrning_passfail").style.display='block';
			document.getElementById("scorediv").style.display='block';
			document.getElementById("costdiv").className ='elearning-all-categories1 wp100 fl';
			document.getElementById("creditdiv").className ='credit-value-inp wp100 fl';
			document.getElementById("credit_value").style.width='100%';
		}
		else
		{
			document.getElementById("course_rating").style.display='block';
			document.getElementById("instructor_rating").style.display='block';
			document.getElementById("instuctornames").style.display='block';
			document.getElementById("elrning_passfail").style.display='none';
			document.getElementById("scorediv").style.display='none';
			document.getElementById("costdiv").className ='elearning-all-categories1  wp100 fl';
			document.getElementById("creditdiv").className ='credit-value-inp wp100 fl';
			document.getElementById("credit_value").style.width='100%';
		}
	   
	}
	
	
	$scope.submit_form1=function()
	{	
	   
		var title= $scope.title;
		var desc=  $scope.desc;
		var traningtypeid= $scope.traningtypeid;
		var instructor_type= $scope.instructor_type;
		if(instructor_type == 1)
		var instructor_name=  $scope.instructor_id;
		else if(instructor_type == 2)
		var instructor_name= $scope.instructor_name;

		var compledate=  $scope.assess_duedate;
	
		var cost= $scope.cost;
		var score= $scope.score;
		var status= $scope.status;
		var credit_value=  $scope.credit_value;
		var certificate_template_id = $scope.certificate_template_id;
		var skillcategoryid= document.getElementById("skillcategoryid").value;
			
		var starr1=  document.getElementById("clas_star").value;
		var starr2=  document.getElementById("ins_star").value;
		
		var usersid =$scope.checkeduser;
		var userid = 	usersid.replace(/-/g,",");
		
		if(!title)
		{
			alertify.alert("Please enter title.");
			return false;
		}
		else if(!usersid)
		{
			alertify.alert("You must first select who you want to receive training credit.");
			return false;
		}
		else if(!compledate)
		{
			alertify.alert("Please enter Completion Date.");
			return false;
		}else if(!traningtypeid)
		{
		
			alertify.alert("Please enter Traning Type.");
			return false;
		}
		
		else
		{
			var compledate= compledate.replace("/","-");
	
          
			$("#waiingforrefresh").show();
			var title1=title.replace(/'/gi, "singlequote"); 
			$http({
			method:"POST",
			url:"/coursescredit/manuallyadditem",
			data:{title: title1,traningtypeid:traningtypeid,instructor_name:instructor_name,compledate:compledate,certificate_template_id:certificate_template_id,cost:cost,credit_value:credit_value,score:score,status:status,users:userid,desc:desc, star1:starr1, star2:starr2,skillcategoryid:skillcategoryid, action: 'PostComment'},
			}).success(function(response){
                  alertify.alert("Training item added to transcript successfully.");
				    $('#givecredit').modal('hide');
					document.getElementById('waiingforrefresh').style.display='none';
					document.getElementById('elrning_passfail').style.display='none';
					document.getElementById('scorediv').style.display='none';
					document.getElementById('costdiv').className='elearning-all-categories1  ml65 wp42 h22 fl';
					document.getElementById('creditdiv').className='credit-value-inp wp20 ml01 fl';
					document.getElementById("credit_value").style.width="105px";
					document.getElementById("course_rating").style.display='block';
					document.getElementById("instructor_rating").style.display='block';
					document.getElementById("instuctornames").style.display='block';
					$scope.title='';
					$scope.instructor_name='';
					$scope.desc='';
					$scope.traningtypeid='';
					$scope.credit_value='';
					$scope.cost='';
					$scope.score='';
					$scope.certificate_template_id='';
					$scope.status='';
					$scope.compledate='';
					$scope.instructor_type='';
					$scope.skillcategoryid='';
			})
			
		}
	}
	
	/* $scope.openviewpopup=function(type,edituserid){
		$http({
			method:"POST",
			url:"/supervisor/userpopup",
			data:{edituserid:edituserid}
		}).success(function(response){
			$scope.userpopupData=response;
			//$scope.selectedviewuser();
			$('#userpopup').modal('show');
			//alert(response);
		})
	} */
	
	$scope.getCheckBoxValue1=function(value,type){
		var fieldValue=value.split(',');
		if(type==1){
			return fieldValue[0];
		}else{
			return fieldValue[1];
		}
		
		
	}
	
	$scope.dropDown=function(dropData){
		$scope.customDropDownList=[];
		$scope.customDropDown=dropData.split(',');
		for(var i=0;i<$scope.customDropDown.length;i++){
			$scope.customDropDownList.push({'dropValue':$scope.customDropDown[i]});
		}
		//alert(JSON.stringify($scope.customDropDownList));
	}
	
	$scope.selectedviewuser=function(){
		$http({
			method:"POST",
			url:"/supervisor/getassignedgroups",
			data:{userid:$scope.userpopupData.userskills[0].user_id}
		}).success(function(response){
			alert(response);
		})
	}
	 	$scope.allexistcourses={'typeId':1};
	
	$scope.allcoursestype=[{'typeId':1,'typeName':'e-Learning'},{'typeId':2,'typeName':'Classroom'},{'typeId':3,'typeName':'Assessment'}];
	
	 $scope.gridcreditchange=function(){
		  $scope.getgridvalue();
         $http({
		  method:"POST",
		  url:"/supervisor/givecreditforselected",
		  data:{checkeduser:$scope.arr,mode:$scope.allexistcourses.typeId}
		}).success(function(response){
			
			var arraycredit=response.creditarray;
		     $scope.userid=arraycredit.userid;
			$scope.compid=arraycredit.compid;
			$scope.checkeduser=arraycredit.checkeduser;
			$scope.allusers=arraycredit.allusers;
			$scope.instructorlist=arraycredit.instructorlist;
			$scope.LockedStatus=arraycredit.LockedStatus;
			$scope.certificatename=arraycredit.certificatename;
			$scope.Trainingtypessarray=arraycredit.Trainingtypessarray;
			$scope.Getdate_formatecomapny=arraycredit.Getdate_formatecomapny;
			$scope.calenderdateformate=arraycredit.calenderdateformate;
			$scope.skillcategory=arraycredit.skillcategory;

			$scope.checkcatset =$scope.LockedStatus[0].category_management;
			if($scope.checkcatset == 1)
			{				
			$("#skillcategoryid").select2({
			tags:$scope.skillcategory,
			createSearchChoice: function() { return null; },
			separator: ",",
			multiple: true});
			}
			else
			{
			$("#skillcategoryid").select2({
			tags:$scope.skillcategory,
			separator: ",",
			multiple: true});
			}			
			$("#assess_duedate").datepicker({dateFormat:  $scope.calenderdateformate,changeMonth: true, changeYear: true, showOn: 'button', buttonImageOnly: true, buttonImage: '/eliteprodlms/public/images/cal_new_img.png'});		
	       	//-------------------------//
			$scope.gridcreditOptions.data = response.dataArray;
		 	$scope.assignMentData=response.dataArray;
			for(i = 0; i < $scope.assignMentData.length; i++){
			$scope.assignMentData[i].subGridOptions = {
			columnDefs: [ {name:"description", field:"Description",cellTemplate:"<div compile=row.entity.description>"}],
			data: $scope.assignMentData[i].subgrid,
			rowHeight:120

			}
			} 
		
      })
		 
	 }
	 
	$scope.submit_form=function()
	{	
	  var clsids=getclassIDsbyradio();
	  $scope.getSelectedExistingItems();
	  if($scope.arritem=='' && clsids =='')
		{
			alertify.alert("Please Select Training Items.");
			return false;
		}else{
			$http({
			method:"POST",
			url:"/supervisor/addexistingitems",
			data:{users:$scope.arr,items:$scope.arritem,ClassIds:clsids}
			}).success(function(response){
                  alertify.alert("Training Items added Successfully.");
				  $('#givecredit').modal('hide');
			});
			
		}
	}
	
	
	function getclassIDsbyradio()
	{
		var classIDs = '';
		$('input:radio').each(function() 
		{    
			if($(this).is(':checked'))
			{
				classIDs += $(this).prop('id') + ',';
			}
		});
		
		return classIDs ;
	}

	
	 $scope.getSelectedExistingItems=function(){
		 $scope.itemrows = $scope.gridcreditApis.selection.getSelectedRows();
		$scope.selectitem = [];
		for(var i=0; i<$scope.itemrows.length;i++){
		var id1=$scope.itemrows[i].id;
		$scope.selectitem.push(id1);
		} 
		$scope.arritem = $scope.selectitem.join(",");
	 }
	 $scope.submittraining=function(){
		  $scope.getgridvalue();
		if(!$scope.arr)
		{
		alertify.alert("You must first select who you want to receive training credit.");
		return false;
		}
		$http({
		  method:"POST",
		  url:"/supervisor/givecreditforselected",
		  data:{checkeduser:$scope.arr,mode:1}
		}).success(function(response){
			
			var arraycredit=response.creditarray;
		     $scope.userid=arraycredit.userid;
			$scope.compid=arraycredit.compid;
			$scope.checkeduser=arraycredit.checkeduser;
			$scope.allusers=arraycredit.allusers;
			$scope.instructorlist=arraycredit.instructorlist;
			$scope.LockedStatus=arraycredit.LockedStatus;
			$scope.certificatename=arraycredit.certificatename;
			$scope.Trainingtypessarray=arraycredit.Trainingtypessarray;
			$scope.Getdate_formatecomapny=arraycredit.Getdate_formatecomapny;
			$scope.calenderdateformate=arraycredit.calenderdateformate;
			$scope.skillcategory=arraycredit.skillcategory;

			$scope.checkcatset =$scope.LockedStatus[0].category_management;
			if($scope.checkcatset == 1)
			{				
			$("#skillcategoryid").select2({
			tags:$scope.skillcategory,
			createSearchChoice: function() { return null; },
			separator: ",",
			multiple: true});
			}
			else
			{
			$("#skillcategoryid").select2({
			tags:$scope.skillcategory,
			separator: ",",
			multiple: true});
			}			
			$("#assess_duedate").datepicker({dateFormat:  $scope.calenderdateformate,changeMonth: true, changeYear: true, showOn: 'button', buttonImageOnly: true, buttonImage: '/eliteprodlms/public/images/cal_new_img.png'});		

			$('#givecredit').modal('show');
			
			$scope.gridcreditOptions.data = response.dataArray;
		
      })
	 }
	 
	  $scope.getgridvalue=function(){
		 $scope.usersids = $scope.gridApis.selection.getSelectedRows();
		$scope.selectuser = [];
		for(var i=0; i<$scope.usersids.length;i++){
		var id1=$scope.usersids[i].id;
		$scope.selectuser.push(id1);
		} 
		$scope.arr = $scope.selectuser.join(",");
		if(!$scope.arr)
		{
			alertify.alert("You must first select who you want to receive training credit.");
			return false;
		}
		 
	 }
});





