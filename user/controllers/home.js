'use strict';

var homemodule = angular.module("homemodule",[]);
homemodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("home", {
		url:"",
		views: {
			
			/* "mainView":{
				templateUrl: '/resources/views/user/templates/home.html',
				controller: 'homeController'
			} */
      "mainView":{
				templateUrl: '/user/templates/login.html',
				controller: 'LoginController'
			}
		},
	});	
	
}]);
	
	
homemodule.controller('homeController', function($scope,$location,$localStorage){
console.log('homemodule Controller');
	if($localStorage.authToken){
			$location.path('store');
		}
});	
