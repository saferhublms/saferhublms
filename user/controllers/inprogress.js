var inprogressmodule = angular.module("inprogressmodule",[]);
/* inprogressmodule.config(["$stateProvider",function($stateProvider){
	
   $stateProvider.state("/mylearning/inprogress", {
		url:"/mylearning/inprogress",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/inprogress.html',
				controller: 'InProgressController'
			}
		},
	});	
	 
  $stateProvider.state("/trainingcatalog/blendedprogram", {
		url:"/trainingcatalog/blendedprogram/viewId/:code",
		views: {
			"mainView":{
				templateUrl: '/user/templates/trainingcatalog/blendedprogram.html',
				controller: 'BlendedProgramController'
			}
		},
	});
	
	 $stateProvider.state("/blendedprogramnew", {
		url:"/trainingcatalog/blendedprogram/viewId/:code/itemTypeId/:itemTypeId/categoryTypeID/:categoryTypeID",
		views: {
			"mainView":{
				templateUrl: '/user/templates/trainingcatalog/blendedprogram.html',
				controller: 'BlendedProgramController'
			}
		},
	});
	
}]);
 

 */


inprogressmodule.controller('InProgressController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,inprogressservice)
{   
    $scope.data = null;
	$scope.init = function(){
    inprogressservice.getData(function(dataResponse) {
		$scope.gridOptions.data = dataResponse;
        $scope.assignMentData=dataResponse;
	      for(i = 0; i < $scope.assignMentData.length; i++){
            $scope.assignMentData[i].subGridOptions = {
              columnDefs: [ {name:"description", field:"Description",cellTemplate:"<div compile=row.entity.description>"}],
			   data: $scope.assignMentData[i].subgrid,
			   rowHeight:120
              
		  }
		  }
	   
    });
	}
	
	$scope.init();
	
	
	 $scope.gridOptions = { 
			 
			  cellEditableCondition: true,
			  enableFiltering: true,
              paginationPageSize:10,
			  paginationPageSizes: [10, 20, 30],
			  rowHeight: 40,
			  expandableRowTemplate: 'expandableRowTemplate.html',
              expandableRowHeight: 150,
              expandableRowScope: {
                  subGridVariable: 'subGridScopeVariable'
              },
			  onRegisterApi : function(gridApi){
				$scope.gridApis= gridApi;
			    $scope.gridOptions.gridApis.refresh()
			},

			 
			 columnDefs: [
						
			  { name: 'item_name',displayName: 'Item Name' ,width: "25%"},
			  { name: 'training_type',displayName: 'Training Type'},
			  { name: 'credits',displayName: 'Credits'},
			  { name: 'status',displayName: 'Status',cellTemplate:"<div ng-bind-html=row.entity.status>"},
			  { name: 'action',displayName: 'Action',cellTemplate:"<div compile=row.entity.action>",enableFiltering: false},
			  { name: 'properties',displayName: 'Properties' ,cellTemplate:"<div ng-bind-html=row.entity.properties>",enableFiltering: false},
			 
			 ]
	 
	 };
	 $scope.launchlearning = function(expiration_status, media_id, enrollelearning_data, scorm_param, courseparam, id){
			viewId = id;
			if(expiration_status=="retake")
			{
			if(!confirm("You have previously completed this course, do you want to retake this training course?")){
			return false;
			}
			}
			$scorm_param =scorm_param;
			$courseparam =courseparam;
			if(media_id==""){
			 alert("Media file does not exist.");
			 return false;
			}

			 if(media_id==7){
			  tb_remove();
			}

			$http({
				method:"POST",
				url:'/api/enrollelearning',
				data:{'enrollelearning_data':enrollelearning_data}
			}).success(function(response){
				$scope.launch_player(response);			
			});
			
			
	 }
	
	
	
	$scope.launch_player=function(response){
		if(response.mode=='tb_show')
		{			
					var completepath='/'+response.param+'?height=600&width=900&inlineId=hiddenModalContent&modal=true';
					
					tb_show(title='', completepath,'')
					
		}
		else if(response.mode=='scorm'){
				if(response.status==""){
					if(response.param!=""){
						$window.location = '/'+response.param+'/viewId/'+$stateParams.code;
					
					}
				}
		}
	}
	
	 $scope.openPreview  =function(enroll,id,su_status,class_id,row){
		alertify.confirm("Are you sure?", function (e) {
		  if (e) {
			  $scope.closepopup(enroll,'', id,'','','',class_id,row);
		  } else {
		   }
	    });
	 }
	 
	 
	 $scope.closepopup = function(var1,var2,id,allocateSeat,userprogramtypeid,blendedprogid,scheduleid,row){
		 var assignmentsend_id=0;
	     var learningplanid=0;
		  $http({
					 method:"post",
					 url:'/api/drop-classroom',
					 data:{'id':id,'allocateSeat':allocateSeat,'userprogramtypeid':userprogramtypeid,'blendedprogid':blendedprogid,'scheduleid':scheduleid,'enroll':var1,'assignmentsend_id':assignmentsend_id,'learningplanid':learningplanid}
				 }).success(function(response){
					     if(response.status == 200){ 
							alertify.alert("Class room Successfully Drop");
							var index = $scope.gridOptions.data.indexOf(row.entity);
                            $scope.gridOptions.data.splice(index, 1);
						 }  
				   });
		 
	 }
      
	  
	 $scope.hides = function(id,row){ 
		alertify.confirm("Are you sure?", function (e) {
		if (e) {
			$scope.inprogressdelete(id,row);
		} else {
		}
	   })
	 } 
	 
	 $scope.inprogressdelete = function(id,row){
		   inprogressservice.deletedata(id);
		   alertify.alert("Successfully Deleted");
		   var index = $scope.gridOptions.data.indexOf(row.entity);
           $scope.gridOptions.data.splice(index, 1);
		   
	}
	 	 
	 $scope.deleteblended = function(id,row){
	    alertify.confirm("Are you sure?", function (e) {
		   if (e) {
			    $scope.deleteblendedprogram(id,row);
		   } else {
		   }
	    });
	 }
	 
	 $scope.deleteblendedprogram = function(id,row){
		 $http({
					 method:"post",
					 url:'/api/delete-blended',
					 data:{id:id}
				 }).success(function(response){
					    if(response.status == 200){
							alertify.alert("Successfully Deleted");
							var index = $scope.gridOptions.data.indexOf(row.entity);
                            $scope.gridOptions.data.splice(index, 1);
						}
				   });
	 }
	 
	 $scope.slaunchlearning = function(){
		 alert('slaunchlearning');
	 }
});




inprogressmodule.controller('BlendedProgramController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,inprogressservice)
{   
  
	
	
	var init = function(){
		 $http({
					 method:"post",
					 url:'/api/mygrid-blended',
					 data:{viewid:$stateParams.code}
				 }).success(function(response){
					    $scope.gridOptions.data = response;
				   });
	}
	
	init();
	
	 $scope.gridOptions = { 
			 
			  cellEditableCondition: true,
			  enableFiltering: true,
              paginationPageSize:10,
			  paginationPageSizes: [10, 20, 30],
			  rowHeight: 40,
			  onRegisterApi : function(gridApi){
				$scope.gridApis= gridApi;
			    $scope.gridOptions.gridApis.refresh()
			},

			 
			 columnDefs: [
						
			  { name: 'item_name',displayName: 'Item Name' ,width: "25%"},
			  { name: 'credit',displayName: 'Credits'},
			  { name: 'itme_type',displayName: 'Item Type'},
			  { name: 'traning_status',displayName: 'Training Status'},
			  { name: 'sequence',displayName: 'Sequence'},
			  { name: 'button',displayName: 'Properties' ,cellTemplate:"<div compile=row.entity.button>",enableFiltering: false},
			 
			 ]
	 
	 };
	 
	 $scope.completioncheck = function(order, status1,sequencenum,url,due_date,assess_id,is_sequence,blenedprogid){
		  var status = document.getElementById('record_'+order).value;
		   status= parseInt(status,10);
		   if(sequencenum == 1){
		   window.location=url;
		  }else{
			var prvItem = parseInt(order,10)-1;
			status = document.getElementById('record_'+prvItem).value;
			var xx = parseInt(sequencenum,10)-1;
			if((is_sequence ==1) || (status == 1)){
				window.location=url;
			}
			else{
				alert("You must complete sequence item number, to access this training program, need to complete the training sequence "+xx);
			}
		}
	 }
	
});


