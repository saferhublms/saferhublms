var mylearning = angular.module("mylearning",[]);
/* mylearning.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("mylearningassignment", {
		url:"/mylearning/assignment",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/mylearningassignment.html',
				controller: 'LearningAssignemtController'
			}
		},
	});	
	
	$stateProvider.state("mylearningmyenrollments", {
		url:"/mylearning/myenrollments",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/mylearningenrollement.html',
				controller: 'MyEnrollmentsController'
			}
		},
	});

	$stateProvider.state("learnassessments", {
		url:"/mylearning/learnassessments",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/learnassessments.html',
				controller: 'AssessmentController'
			}
		},
	});	
	$stateProvider.state("mylearningmysubmittraining", {
		url:"/mylearning/learnsubmittraining",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/mylearningmysubmittraining.html',
				controller: 'MySubmitTrainingController'
			}
		},
	});	
	
	$stateProvider.state("viewtraining", {
		url:"/elearningmedia/training/viewId/:code",
		views: {
			"mainView":{
				templateUrl: '/user/templates/elearningmedia/training.html',
				controller: 'elearningmediaController'
			}
		},
	});	
 
    $stateProvider.state("elearningviewtraining", {
		url:"/elearningmedia/training/viewId/:code/itemTypeId/:itemId/categoryTypeID/:categorytypeid",
		views: {
			"mainView":{
				templateUrl: '/user/templates/elearningmedia/training.html',
				controller: 'elearningmediaController'
			}
		},
	});	
	
	 $stateProvider.state("elearningscormtraining", {
		url:"/elearningmedia/training/viewId/:code/itemTypeId/:itemId/categoryTypeID/:categorytypeid/scormparam/:scormparam",
		views: {
			"mainView":{
				templateUrl: '/user/templates/elearningmedia/training.html',
				controller: 'elearningmediaController'
			}
		},
	});
	 $stateProvider.state("elearningscormviewtraining", {
		url:"/elearningmedia/training/viewId/:code/scormparam/:scormparam",
		views: {
			"mainView":{
				templateUrl: '/user/templates/elearningmedia/training.html',
				controller: 'elearningmediaController'
			}
		},
	});
 	$stateProvider.state("mylearningtranscript", {
		url:"/mylearning/transcript",
		views: {
			"mainView":{
				templateUrl: '/user/templates/transcript.html',
				controller: 'MyTranscriptController'
			}
		},
	});

	$stateProvider.state("mediaplayer", {
		url:"/elearningmedia/mediaplayer/viewId/:mediacode",
		views: {
			"mainView":{
				templateUrl: '/user/templates/elearningmedia/playernew.html',
				controller: 'mediaplayerController'
			}
		},
	});	
	
   	$stateProvider.state("classroomcourse", {
		url:"/classroomcourse/training/viewId/:code",
		views: {
			"mainView":{
				templateUrl: '/user/templates/classroomcourse/training.html',
				controller: 'classroomcourseController'
			}
		},
	});	
	$stateProvider.state("classroomcoursenew", {
		url:"/classroomcourse/training/viewId/:code/itemTypeId/:itemTypeId/categoryTypeID/:categoryTypeID",
		views: {
			"mainView":{
				templateUrl: '/user/templates/classroomcourse/training.html',
				controller: 'classroomcourseController'
			}
		},
	});	
	$stateProvider.state("mylearninglearningplan", {
		url:"/mylearning/learningplan",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/mylearningplan.html',
				controller: 'LearningPlanController'
			}
		},
	});	
	
	$stateProvider.state("survayviewassessment", {
		url:"/survey/viewassessment/user/:user/token/:token/:red_page/:learnassessments",
		views: {
			"mainView":{
				templateUrl: '/user/templates/mylearning/survayviewassessment.html',
				controller: 'ViewAssessmentController'
			}
		},
	});	
	
 
 
}]);
 
 */



mylearning.controller('LearningAssignemtController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams){   
  
                $scope.learningAssignment=function(){
					$http({
						method:"POST",
						url:'/mylearning/assignment-data'
					}).success(function(response){
						$scope.assignMentData=response;
						//alert(JSON.stringify(response.viewData));
					})
				}
				$scope.learningAssignment();
				
				
				
			var mytemplate='<img src="/user/assets/images/u178.png" width="21" height="19" />'	 
			 $scope.gridOptions = { 
			  data :'assignMentData.gridData',
			 cellEditableCondition: true,
			enableFiltering: true,

			 paginationPageSize:10,
			 paginationPageSizes: [10, 20, 30],
			paginationPageSize: 10,
					rowHeight: 40,
			onRegisterApi : function(gridApi){
			  $scope.gridApis= gridApi;
			  $scope.gridOptions.gridApis.refresh()
			},

			 
			 columnDefs: [		
			  { name: 'assigments',displayName: 'Assignments' ,width: "25%"},
		   // { name: 'task_detail',width:150,displayName: 'Description'},
			  { name: 'training_type',displayName: 'Training Type'},
			  { name: 'credits',displayName: 'Credits'},
			  { name: 'assigned',displayName: 'Assigned'},
			  { name: 'due_date',displayName: 'Due Date',enableFiltering: false},
			  { name: 'status',displayName: 'Status' ,enableFiltering: false},
			  { name: 'flag',headerCellTemplate: mytemplate,cellTemplate:"flag.html" ,enableFiltering: false},
		   // {name:'view', displayName:'Properties',cellTemplate:"view.html" ,enableFiltering: false},
	  
			]
	 
	 };
				
				
				

});


mylearning.controller('MyEnrollmentsController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify){   
                  
				   $scope.learningEnrollment=function(){
					$http({
						method:"POST",
						url:'/mylearning/myenrollments-data'
					}).success(function(response){
						$scope.enrollmentData=response;
						//alert(JSON.stringify(response));
					})
				}
				$scope.learningEnrollment();
				
				
				
				$scope.gridOptions = { 
				  data :'enrollmentData',
				 cellEditableCondition: true,
				enableFiltering: true,

				 paginationPageSize:10,
				 paginationPageSizes: [10, 20, 30],
				paginationPageSize: 10,
						rowHeight: 40,
				onRegisterApi : function(gridApi){
					
				  $scope.gridApis= gridApi;
				  $scope.gridOptions.gridApis.refresh()
				},

				 
				 columnDefs: [
							
				  { name: 'item_name',displayName: 'Itm Name' ,width: "25%"},
			   //   { name: 'task_detail',width:150,displayName: 'Description'},
				  { name: 'start_date',displayName: 'Start Date'},
				  { name: 'training_type',displayName: 'Training Type'},
				  { name: 'credits',displayName: 'Credits'},
				  { name: 'location',displayName: 'Location',enableFiltering: false},
				  { name: 'status',displayName: 'Status' ,enableFiltering: false},
                  {name:'drop', displayName:'Drop',cellTemplate:"drop.html" ,enableFiltering: false},				  
                  {name:'view', displayName:'Properties',cellTemplate:"view.html" ,enableFiltering: false},				  
				]
		 
		 };
		 
		 $scope.hides=function(encodedEnrollId)
		 {
				alertify.confirm("Are you sure?", function (e) {
				if (e) {
				$scope.myenrollmentdelete(encodedEnrollId);
				} else {
				}
	         })

		 } 
		 
		 $scope.myenrollmentdelete=function(itemid)
			{
				$http({
					method:"POST",
					url:"/mylearning/deletemyenrollprogram/id/"+itemid
				}).success(function(response){
					alert(JSON.stringify(response));
				})
				
			}
	 

});
 



mylearning.controller('AssessmentController',function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify){   
                  
				   $scope.learningAssessment=function(){
					$http({
						method:"POST",
						url:'/mylearning/learnassessments-data'
					}).success(function(response){
						$scope.assessmentData=response;
						//alert(JSON.stringify(response));
					})
				}
				$scope.learningAssessment();
				
				
				
				$scope.gridOptions = { 
				  data :'assessmentData.gridData',
				 cellEditableCondition: true,
				enableFiltering: true,

				 paginationPageSize:10,
				 paginationPageSizes: [10, 20, 30],
				paginationPageSize: 10,
						rowHeight: 40,
				onRegisterApi : function(gridApi){
					
				  $scope.gridApis= gridApi;
				  $scope.gridOptions.gridApis.refresh()
				},

				 
				 columnDefs: [
							
				  { name: 'assessment_title',displayName: 'Assessment Title' ,width: "25%"},
			   
				  { name: 'credits',displayName: 'Credits'},
				  { name: 'due_date',displayName: 'Due Date'},
				  { name: 'credits',displayName: 'Credits'},
				  { name: 'status',displayName: 'Status'},
				  { name: 'progress',displayName: 'Progress' ,enableFiltering: false},
                  			  
                  {name:'view', displayName:'Properties',cellTemplate:"<span compile=row.entity.view></span>",enableFiltering: false},				  
				]
		 
		 };
		 
		 $scope.redirect1=function(encodeduserid,token){
			 
			  var url ='/survey/successfullsubmitasse/userid/'+encodeduserid+'/token/'+token;
	         $window.open(url,'_blank');	
		 }
		 
		
});




mylearning.controller('LearningPlanController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify){   
                  
				   $scope.learningPlan=function(){
					$http({
						method:"POST",
						url:'/mylearning/learning-data'
					}).success(function(response){
						$scope.learninPlanData=response;
						//alert(JSON.stringify(response));
					})
				}
				$scope.learningPlan();
				
				var mytemplate='<img src="/user/assets/images/u178.png" width="21" height="19" />'	
				$scope.gridOptions = { 
				  data :'learninPlanData.gridData',
				 cellEditableCondition: true,
				enableFiltering: true,
				 paginationPageSize:10,
				 paginationPageSizes: [10, 20, 30],
				paginationPageSize: 10,
						rowHeight: 40,
				onRegisterApi : function(gridApi){
					
				  $scope.gridApis= gridApi;
				 // $scope.gridOptions.gridApis.refresh()
				},
				 columnDefs: [
				  { name: 'learnin_plan_name',displayName: 'Learning Plan' },
				  { name: 'item_name',displayName: 'Learning Plan Items' },
				  { name: 'training_type',displayName: 'Training Type' },
				  { name: 'credit_value',displayName: 'Credits'},
				  { name: 'due_date',displayName: 'Due Date'},
				  { name: 'expiration_date',displayName: 'Expiration Date'},
				  { name: 'percent',displayName: 'Progress' ,enableFiltering: false,cellTemplate:"progress.html"},
                  { name: 'imageUrl',headerCellTemplate: mytemplate,cellTemplate:"flag.html" ,enableFiltering: false},
                  {name:  'view', displayName:'Properties',cellTemplate:"view.html",enableFiltering: false},				  
				]
		 };
});



mylearning.controller('MySubmitTrainingController',function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify){  
 
 
                 $scope.submitTraining=function(){
					 if(!$scope.title){
						 Alertify.alert('Please enter title and training details.');
					 }else if(!$scope.myDate){
						 Alertify.alert('Please enter Completion Date.');
					 }else if(!$scope.traningtypeid){
						 Alertify.alert('Please select a training type.');
					 }else{
						 var attachfiledata=attachFile.files.item(0);
						 if(attachfiledata){
							 var isAttch=1;
						 }else{
							 var isAttch=0;
						 }
						
						 $http({
							 method:"POST",
							 url:"/mylearning/submittraining",
							 headers: {'Content-Type': undefined },
							 transformRequest: function () {
								 var formData = new FormData();
								 formData.append('title',$scope.title);
								 formData.append('description',$scope.description);
								 formData.append('completionDate',$scope.myDate);
								 formData.append('credit',$scope.credit_value);
								 formData.append('traningtype',$scope.traningtypeid);
								 formData.append('attachFile',attachfiledata); 
								 formData.append('isAttch',isAttch); 
								 return formData;
							 }
							
						 }).success(function(response){
							 if(response.status==201){
								 $scope.title='';
								 $scope.description='';
								 $scope.credit_value='';
								 $scope.traningtypeid='';
								 $scope.myDate='';
								 attachfiledata='';
							 }
							 Alertify.alert(response.message);
							 
						 })
					 }
					 
					 
				 }
	 

});

mylearning.controller('MyTranscriptController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$compile,$filter){   

            $scope.tokenuse=$("input[name=_token]").val();
			var paramdata={_token:$scope.tokenuse}
	
			$scope.trainingTypes=[
				{
				"id":"IeMQY",
				"type":"eLearning",
				},
				{
				"id":"FQMgI",
				"type":"Classroom"

				},
				{
				"id":"LjNQo",
				"type":"Assessment"

				}
				
			];
			
		$scope.filtertranscript=function(){
			$scope.userTranscriptDataGrid('training');
		}
		
			$scope.userTranscriptDataGrid=function(type){
					var mydate1 = mydate2='';
					if($scope.userstartdt){
					   var mydate1 =moment($scope.userstartdt).format('Y-MM-DD');
					}
					if($scope.userenddt){
						 var mydate2 = moment($scope.userenddt).format('Y-MM-DD');
					}
				
					if(type=='training'){
						var allparam={_token:$scope.tokenuse,'startDate':mydate1,'endDate':mydate2,'trainingType':$scope.trainingtype_id,page:1,id:0}
						var dataurl='/api/transcript-data';
					}
					
					$scope.dtOptions = DTOptionsBuilder.newOptions()
					.withOption('ajax', {
					data:allparam,
					url: dataurl,
					type: 'POST'
					}).withDataProp(function(json) {
					json.recordsTotal = json.count;
					json.recordsFiltered = json.count;
					json.draw = json.draw;
					return json.data;
					}).withDOM('C<"clear">lfrtip')
						.withOption('processing', true)
						.withOption('serverSide', true)
						.withOption('createdRow', function(row, data, dataIndex) {
						$compile(angular.element(row).contents())($scope);
					})
					.withOption('headerCallback', function(header) {
					if (!$scope.headerCompiled) {
						$scope.headerCompiled = true;
						$compile(angular.element(header).contents())($scope);
					}
					})
					.withPaginationType('full_numbers');

					$scope.dtColumns = [
					DTColumnBuilder.newColumn('item_name').withTitle($rootScope.responseLib.itemname),
					DTColumnBuilder.newColumn('credit_value').withTitle($rootScope.responseLib.creditvalue),
					DTColumnBuilder.newColumn('status').withTitle($rootScope.responseLib.status),
					DTColumnBuilder.newColumn(null).withTitle($rootScope.responseLib.datecompleted).renderWith(datedata),
					DTColumnBuilder.newColumn('type').withTitle($rootScope.responseLib.trainingtype),
					DTColumnBuilder.newColumn('button').withTitle($rootScope.responseLib.view).notSortable()
					];
					return ;
				}
				
				
				function datedata(data, type, full, meta){
					 $scope.formatedDate = $filter('date')(data.completion_date, data.dateFormat);
					return  $scope.formatedDate
				}
	
	
				
				$scope.userTranscriptDataGrid('training');
				
				$scope.init=function(allparam){
				    var dataurl='mylearning/get-transcript-history';
					
					$scope.dtOptions1 = DTOptionsBuilder.newOptions()
					.withOption('ajax', {
					data:allparam,
					url: dataurl,
					type: 'POST'
					}).withDataProp(function(json) {
					json.recordsTotal = json.count;
					json.recordsFiltered = json.count;
					json.draw = json.draw;
					return json.data;
					}).withDOM('C<"clear">lfrtip')
					.withOption('processing', true)
					.withOption('serverSide', true)
					.withOption('createdRow', function(row, data, dataIndex) {
					$compile(angular.element(row).contents())($scope);
					})
					.withOption('headerCallback', function(header) {
						if (!$scope.headerCompiled) {
						$scope.headerCompiled = true;
						$compile(angular.element(header).contents())($scope);
						}
					})
					.withPaginationType('full_numbers');
					$scope.dtColumns1 = [
						DTColumnBuilder.newColumn('item_name').withTitle('Item Name'),
						DTColumnBuilder.newColumn('credit_value').withTitle('Credit Value'),
						DTColumnBuilder.newColumn('status').withTitle('Status'),
						DTColumnBuilder.newColumn(null).withTitle('Date Completed').renderWith(datedata),
						DTColumnBuilder.newColumn('type').withTitle('Training Type')
					];
					
					return ;
				}
				
			$scope.init([]);
									
				 $scope.openviewpopup=function(uid,itmid){
					var allparam={_token:$scope.tokenuse,page:1,id:0,'user_id':uid,'item_id':itmid};
					$scope.init(allparam);
					$('#myModal').modal('show');
				} 
				
		$scope.launch_player=function(response){
		
			var player_url='/'+response;
			var nWidth = screen.availWidth - 10;
			var nHeight = screen.availHeight - 10;
			$.fancybox.open({	
			href : player_url,
			'width':nWidth,
			'height':nHeight,
			'autoDimensions':false,
			'type':'iframe',
			'autoSize':false,
			'scrolling'          	: 'no',
			'padding'            	: 0,
			'closeBtn'			    : true,
			'helpers': { overlay: { closeClick: false } },
			'iframe': {
			scrolling : 'no',
			preload   : true
			},
			'afterClose': function (data) {
			 window.location = '#/mylearning/transcript';
			}
			});
		}
	 
/* 	$scope.openpopup=function(token,userid,transcript_id)
	{
		$window.location.href='/survey/successfullsubmitasse/userid/'+userid+'/token/'+token;

					tb_show(title='', completepath,'')
	} */

/* 	$scope.openassesspopup=function()
	{
		alertify.alert('This assessment has been Credited by Admin.');
		return false;
	} */
	 
	 /* $scope.gridApis={};
			$scope.gridAll = { 
			cellEditableCondition: true,
			enableRowHeaderSelection :false,
			enableRowSelection: false,
			enableSelectAll: true,
			enableFiltering: true,
			rowHeight: 60,
			onRegisterApi : function(gridApi){
			},
			columnDefs: [	
			{ name: 'item_name',displayName: 'Item Name'},
			{ name: 'credit_value',displayName: 'Credits'},
			{ name: 'status',displayName: 'Status'},
			{ name: 'completion_date',displayName: 'Date Completed'},
			{ name: 'type',displayName: 'Training Type'},
			]
			};  */

});

mylearning.controller('elearningmediaController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify){
	
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.viewIdsend=$stateParams.code;
	$scope.viewEletrainings=function(){
	  $http({
					method:"POST",
					url:'/api/viewElearningTraning',
					data:{_token:$scope.tokenuse,'viewId':$stateParams.code}
				}).success(function(response){
					$scope.courseDetailArr=response.courseDetailArr;
					$scope.enrollelearning=response.enrollelearning;
					
				}) 
	}
	$scope.viewEletrainings();
	
		$scope.enrollelearningfn = function(){
			$scope.media_id=$scope.courseDetailArr.fileDetails.media_id;
			if(!$scope.media_id){
				Alertify.alert('Media file does not exist.');
				return false;
			}
			 $http({
					method:"POST",
					url:'/api/enrollelearning',
					data:{_token:$scope.tokenuse,'enrollelearning_data':$scope.enrollelearning}
				}).success(function(response){
					$scope.launch_player(response);			
         });
	}
	
	$scope.show_media_player=function(){
		  if($scope.courseDetailArr.expiration_status!="new"){
			alertify.confirm("You have previously completed this course, do you want to retake this training course?", function (e) {
			if (e) {

			   $scope.enrollelearningfn();
			} else {
			}
			});
		  }else{
			     $scope.enrollelearningfn();
		  }
	}
	
	$scope.callfunction=function(average_rating){
		
		    var strhtml='';
			for($i=1; $i<=average_rating; $i++) { 
						strhtml+='<i class="fa fa-star"></i>';
			 }
			for($j=average_rating;$j<5;$j++){
					strhtml+='<i class="fa fa-star-o"></i>';
		    }
			return strhtml;
	}

	
	$scope.launch_player=function(response){
		if(response.mode=='tb_show')
		{			
					var completepath='/'+response.param+'?height=600&width=900&inlineId=hiddenModalContent&modal=true';
					
					tb_show(title='', completepath,'')
					/* tb_show(title='', completepath,'');	
	               $window.location.href = '#/'+response.param;
		           return false; */
		}
		else if(response.mode=='scorm'){
				if(response.status==""){
					if(response.param!=""){
						$window.location = '/'+response.param+'/viewId/'+$stateParams.code;
						/* var completepath='/'+response.param+'/viewId/'+$stateParams.code+'?height=600&width=900&inlineId=hiddenModalContent&modal=true';
					
					     tb_show(title='', completepath,'') */
					}
				}
		}
	}
 

});	

mylearning.controller('mediaplayerController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,$sce){  

		$scope.tokenuse=$("input[name=_token]").val();
//$scope.videoSrc='sdfdsaf';
		$scope.mediaplayer=function(){
		  $http({
						method:"POST",
						url:'/api/mediaplayer',
						data:{_token:$scope.tokenuse,'viewId':$stateParams.mediacode}
					}).success(function(response){
						$scope.mediafile=response.mediafile;
						$scope.compid=response.compid;
						$scope.filepath=response.filepath;
						$scope.filesname=response.filesname;
						$scope.openmedia=response.openmedia;
						$scope.protocol=response.protocol;
						$scope.view=response.view;
						//$scope.videoSrc =  $scope.filesname;
						
					}) 
		   }
		   
		$scope.mediaplayer();
		
		
			$scope.trustSrc = function(src) {
			return $sce.trustAsResourceUrl(src);
			}
	
});	

mylearning.controller('classroomcourseController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify){

	
	$scope.tokenuse=$("input[name=_token]").val();
	
	$scope.viewClassroomTraining=function(){
	  $http({
					method:"POST",
					url:'/api/classroomtraining',
					data:{_token:$scope.tokenuse,'viewId':$stateParams.code,classid:$stateParams.classid}
				}).success(function(response){
					
					$scope.courseDetailArr=response.courseDetailArr;
					$scope.item_type_id=response.item_type_id;
					$scope.scheduleArr=response.scheduleArr;
					$scope.userenrollment=response.Usertrainingcatalog;
					$scope.scheduleIs=response.scheduleIs;
					$scope.buttonclass=response.buttonclass;
					$scope.supervisior_approval=response.supervisior_approval;
					$scope.allocateSeat=response.allocateSeat;
					$scope.strhtml=response.strhtml;
					$scope.targetstr=response.targetstr;
					$scope.IslastScheduleDiv=response.IslastScheduleDiv;
					//$scope.isHandoutAvailable=response.isHandoutAvailable;
					$scope.handouts=response.handouts;
					//$scope.curriculams=response.curriculams;
					//$scope.isCurriculumAvailableforall=response.isCurriculumAvailableforall;
					////$scope.isCurriculumAvailableforins=response.isCurriculumAvailableforins;
					//$scope.isCurriculumAvailableforenroll=response.isCurriculumAvailableforenroll;

					//$scope.IsTargetAudience=$scope.targetAudience.length;
					
				}) 
	}
	
	$scope.viewClassroomTraining();

	
	
	$scope.submitraining=function(itemId){
		alertify.confirm("Currently there are no classes scheduled for this course. Would you like to send a request that a class be scheduled for this course?", function (e) {
			if (e) {
				 $scope.enroll(itemId);
			} else {
			}
		});
		
	}
	

	$scope.enroll=function(itemId){
		$http({
			method:"POST",
			url:'/classroomcourse/trainingrequestpopup',
			data:{_token:$scope.tokenuse,'itemId':itemId,request:'submit'}
		}).success(function(response){
			 alertify.alert('Request send successfully scheduled for this course');
		})
	}
	
	
	$scope.checkwt=function(enroll,scheduleid){
	alertify.confirm("This class is full, you have been added to a waiting list.<br> You will be notified if a seat becomes available.", function (e) {
		if (e) {
			 $scope.closepopup(enroll,'0', '','','',scheduleid);
		} else {
		}
	});
  }

$scope.pending_apprv=function(enroll,id,scheduleid){
	alertify.confirm("This course may require supervisor approval.<br>(view your approval status in the My Enrollments tab of the My Learning area).", function (e) {
		if (e) {
			$scope.closepopup(enroll,'',id,'','',scheduleid);
		} else {
		}
	});
}
	
	$scope.nocuriculum=function(){
		alertify.alert('There are no curriculum for this class.');
	}
	
	$scope.nohandouts=function(){
		 alertify.alert('There are no current handouts for this class.');
	}
	
	$scope.viewcurriculams=function(itemid){
		        $http({
					method:"POST",
					url:'/classroomcourse/curriculams',
					data:{_token:$scope.tokenuse,'id':itemid}
				}).success(function(response){
					  $scope.modelname = 'Class Room  Curriculum';
					  $scope.strhandoutandcariculam=response.carriculahtml;
					  $('#myHandout').modal('show');
				})
	};

	$scope.viewhandouts=function(yesno,itemid){
	//	yesno=1;
		if(yesno=='1'){
		       $http({
					method:"POST",
					url:'/classroomcourse/classhandouts',
					data:{_token:$scope.tokenuse,'id':itemid}
				}).success(function(response){
					  $scope.modelname = 'Class Room Handouts';
					  $scope.strhandoutandcariculam=response.strhandout;
					  $('#myHandout').modal('show');
				})
		}else{
			alertify.alert('<div class="fc18 fla fs13 b mt05 ml10 fl">Please Enroll to View Class Handouts</div><div class="fc19 fla fs13  mt05 ml10 fl tal w492">You must enroll in the course before viewing class handouts. Please close this window and click the "Enroll" button.Once you are enrolled for the course you may view the class handouts.</div>');
		}
	}
	
	$scope.viewroaster=function(item,classid,isenroll,statusid)
	{
         // isenroll=1;
		if(isenroll==0){
		alertify.alert('<div class="fc18 fla fs13 b mt05 ml10 fl">Please Enroll to View Roster </div><div class="fc19 fla fs13  mt05 ml10 fl tal w492">You must enroll in the course before viewing class roster. Please close this window and click the "Enroll" button.Once you are enrolled for the course you may view the class roster.</div>');
		}else{
			   
			     $scope.modelname = 'Class Roaster';
			     $scope.loader = true;
				$http({
					method:"POST",
					url:'/classroomcourse/classroaster',
					data:{_token:$scope.tokenuse,'itemid':item,'classid':classid}
				}).success(function(response){
						$scope.loader = false;
						$scope.gridAll.data = response;
						$('#myModal').modal('show');
				}) 
			
		}
	}
	
	$scope.enroll_class=function(enroll,id,supervisor_approval,class_id){
		alertify.confirm("Please confirm that you would like to enroll for this event?", function (e) {
		if (e) {
			 $scope.closepopup(enroll,'', id,'','',class_id);
		} else {
		}
	  });	
	}
	

	$scope.re_enroll_drop_class_drop=function(enroll,id,supervisor_approval,class_id)
	{
		alertify.confirm("Please confirm that you would like to enroll for this event?", function (e) {
		if (e) {
			 $scope.closepopup(enroll,'', id,'','',class_id);
		} else {
		}
		});	
	}
	
	$scope.enroll_class_drop=function(enroll,id,supervisor_approval,class_id)
	{ 
		alertify.confirm("Are you sure you want to drop this class?", function (e) {
			if (e) {
				 $scope.closepopup(enroll,'1', id,'','',class_id);
			} else {
			}
		});
	}
	
	$scope.closepopup=function(var1, var2, id, allocateSeat, userprogramtypeid, scheduleid){
		
			/* alert(var1+'=='+var2+'=='+id+'=='+allocateSeat+'==='+userprogramtypeid+'=='+blendedprogid+'=='+scheduleid); */
			assignmentsend_id=0;
			learningplanid=0;
			var url = "";
			if(var2 == 2){
				tb_remove();
				return false;
			}
			 
            url = "/classroomcourse/confirmenroll";
			
			if(var1 == 1)
			{
				var datasend={_token:$scope.tokenuse,'scheduleid':scheduleid,'enroll':var1,token:id};
			
			}else{
				var datasend={_token:$scope.tokenuse,'scheduleid':scheduleid,'enroll':var1,token:id};
			}
			$scope.loader = true;
			$http({
			method:"POST",
			url:url,
			data:datasend
			}).success(function(response){
				   $scope.loader = false;
					$scope.viewClassroomTraining();
				
			})
			
	}
	
	$scope.gridApis={};
			$scope.gridAll = { 
			cellEditableCondition: true,
			enableRowHeaderSelection :false,
			enableRowSelection: false,
			enableSelectAll: true,
			enableFiltering: true,
			rowHeight: 40,
			onRegisterApi : function(gridApi){
			},


			columnDefs: [	
			{ name: 'first_name',displayName: 'First Name'},
			{ name: 'last_name',displayName: 'Last Name'},
			{ name: 'jobTitles',displayName: 'Job Titles'},
			{ name: 'dates',displayName: 'Dates'},
			{ name: 'status',displayName: 'Status'},
			]

			}; 
	
});



mylearning.controller('ViewAssessmentController', function($scope,$location,$http,$timeout,$localStorage,$window,$stateParams,Alertify){
	
	    $scope.tokenuse=$("input[name=_token]").val();
	     $scope.code=$stateParams.code;
		 $scope.viewAssessment=function(){
			 $http({
				 method:"POST",
				 url:"/mylearning/viewassessmentdata",
				 data:{'viewId':$scope.code}
			 }).success(function(response){
				 if(response.status!=408){
					 $scope.courseDetailArr=response.courseDetailArr;
					 $scope.enrollelearning=response.enrollelearning;
					// alert(JSON.stringify(response.courseDetailArr));
					// $scope.assementdetailsCount=response.assementdetailsCount;
					  
					
				 }else{
					 Alertify.alert(response.message);
				 }
				
			 })
		 }
		 
		 $scope.viewAssessment();
	    
		$scope.enrollelearningfn = function(){
			 $http({
					method:"POST",
					url:'/mylearning/enrollelearning',
					data:{_token:$scope.tokenuse,'enrollelearning_data':$scope.enrollelearning}
				}).success(function(response){
					$scope.launch_player(response);			
         });
	}
	
	$scope.show_media_player=function(){
		  if($scope.courseDetailArr.completion_status!="new"){
			alertify.confirm("You have previously completed this course, do you want to retake this training course?", function (e) {
			if (e) {

			   $scope.enrollelearningfn();
			} else {
			}
			});
		  }else{
			     $scope.enrollelearningfn();
		  }
	 
	}
	
	$scope.callfunction=function(average_rating){
		
		    var strhtml='';
			for($i=1; $i<=average_rating; $i++) {
						strhtml+='<i class="fa fa-star"></i>';
			 }
			for($j=average_rating;$j<5;$j++){
					strhtml+='<i class="fa fa-star-o"></i>';
		    }
			return strhtml;
	}

	
	$scope.launch_player=function(response){
		var player_url='/'+response.param;
		var nWidth = screen.availWidth - 10;
		var nHeight = screen.availHeight - 10;
		$.fancybox.open({	
			href : player_url,
			'width':nWidth,
			'height':nHeight,
			'autoDimensions':false,
			'type':'iframe',
			'autoSize':false,
			'scrolling'          	: 'no',
			'padding'            	: 0,
			'closeBtn'			    : true,
			'helpers': { overlay: { closeClick: false } },
			'iframe': {
				scrolling : 'no',
				preload   : true
			},
			'afterClose': function (data) {
				//window.location = '<?php echo $baseUrl;?>/elearningmedia/closelaunchtraining';
				window.location = '#/course/viewassessment/'+$scope.code;
			}
		});
		
		/*  if(response.mode=='tb_show')
		{			
					var completepath='/'+response.param+'?height=600&width=1000&inlineId=hiddenModalContent&modal=true';
					
					tb_show(title='', completepath,'')
					
		}  */
	}
		
	
});