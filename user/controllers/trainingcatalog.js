var trainingcatalogModule=angular.module('trainingcatalogModule',[]);
/* trainingcatalogModule.config(['$stateProvider',function($stateProvider){
                   
		  $stateProvider.state("/trainingcatalogindex", {
				url:"/trainingcatalog/index",
				views: {
					"mainView":{
						templateUrl: '/user/templates/trainingcatalog/trainingcatalog.html',
						controller: 'TrainingCatalogController'
					}
				},
			});	

}]); */
 

trainingcatalogModule.controller('TrainingCatalogController', function($scope,$location,$http,$rootScope,$timeout,$localStorage,$window,$stateParams,Alertify,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$compile){  
                  
                  
                  $scope.tokenuse=$("input[name=_token]").val();
                  $scope.setItemType=5; 
				  $scope.advancebutton=true;
                  $scope.trainingType=[
					  {'training_type_id':5,'training_type':'All'},
					  {'training_type_id':1,'training_type':'eLearning'},
					  {'training_type_id':2,'training_type':'Classroom Training'},
					  {'training_type_id':3,'training_type':'Virtual Classroom'},
					  {'training_type_id':4,'training_type':'Blended Programs'}
				  ];
				
					
					$scope.trainingCatalogDataGrid=function(data,type){
						//advancedata,'advancefilter'
						if(type=='training'){
							var allparam={_token:$scope.tokenuse,page:1,id:0};
							var dataurl='/mylearning/trainingcatolg-data';
						}else if(type=='advancefilter'){
							var allparam=data;
							var dataurl='/mylearning/advanceFilter';
							
						}else{
							var allparam=data;
							var dataurl='/mylearning/categoryfilter';
						}
						
						$scope.dtOptions = DTOptionsBuilder.newOptions()
						.withOption('ajax', {
						data:allparam,
						url: dataurl,
						type: 'POST'
						}).withDataProp(function(json) {
						json.recordsTotal = json.count;
						json.recordsFiltered = json.count;
						json.draw = json.draw;
						return json.data;
						}).withDOM('C<"clear">lfrtip')
						.withOption('processing', true)
						.withOption('serverSide', true)
						.withOption('createdRow', function(row, data, dataIndex) {
						$compile(angular.element(row).contents())($scope);
						})
						.withOption('headerCallback', function(header) {
						if (!$scope.headerCompiled) {
							$scope.headerCompiled = true;
							$compile(angular.element(header).contents())($scope);
							}
						})
						.withPaginationType('full_numbers');

						$scope.dtColumns = [
							DTColumnBuilder.newColumn('item_name').withTitle($rootScope.responseLib.itemname),
							DTColumnBuilder.newColumn('start_date').withTitle($rootScope.responseLib.startdate),
							DTColumnBuilder.newColumn('credit_value').withTitle($rootScope.responseLib.creditvalue),
							DTColumnBuilder.newColumn('item_type').withTitle($rootScope.responseLib.itemtype),
							DTColumnBuilder.newColumn('view').withTitle($rootScope.responseLib.view).notSortable()
						];
						return ;
					}
					
					$scope.trainingCatalogDataGrid([],'training');
					
			

		 $scope.getTrainingCatalogFilter=function(){
			 $http({
				 method:"POST",
				 url:"/mylearning/trainingcatalog-filterlist"
			 }).success(function(response){
				 $scope.filterlist=response;
				 $scope.preloader_popup=false;
				 //alert(JSON.stringify(response));
			 })
		 }
		 $scope.getTrainingCatalogFilter();
		 
		 
		 $scope.getTrainingCatalogFilterData=function(){
		
			 $scope.preloader_popup=true;
			 if(!$scope.setcat){$scope.catType='';}
			 else{$scope.catType=$scope.setcat;}
			 if($scope.setItemType==5){$scope.itemtype='';}
			 else{$scope.itemtype=$scope.setItemType;}
			 var datafilter={_token:$scope.tokenuse,page:1,id:0,'itemType':$scope.itemtype,'categoryType':$scope.catType};
			 $scope.trainingCatalogDataGrid(datafilter,'filter');
			 
			/* $scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('ajax', {
			data:{_token:$scope.tokenuse,page:1,id:0,'itemType':$scope.itemtype,'categoryType':$scope.catType},
			url: '/mylearning/categoryfilter',
			type: 'POST'
			}).withDataProp(function(json) {
			json.recordsTotal = json.count;
			json.recordsFiltered = json.count;
			json.draw = json.draw;
			return json.data;
			}).withDOM('C<"clear">lfrtip')
			.withOption('processing', true)
			.withOption('serverSide', true)
			.withOption('createdRow', function(row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
			})
			.withOption('headerCallback', function(header) {
			if (!$scope.headerCompiled) {
			$scope.headerCompiled = true;
			$compile(angular.element(header).contents())($scope);
			}
			})
			.withPaginationType('full_numbers');

			$scope.dtColumns = [
			DTColumnBuilder.newColumn('item_name').withTitle('Item Name'),
			DTColumnBuilder.newColumn('start_date').withTitle('Start Data'),
			DTColumnBuilder.newColumn('credit_value').withTitle('Credit Value'),
			DTColumnBuilder.newColumn('item_type').withTitle('Item Type'),
			DTColumnBuilder.newColumn('view').withTitle('View').notSortable()
			]; */
		 }
		 
		 $scope.hidecatalog=function(){
			 $scope.advancebutton=false;
			 $scope.advanceSearch=true;
			 //$scope.trainingtypeid=5;
			 $scope.trainingtitle='';
			 $scope.instructorname='';
			 $scope.category='';
			 $scope.creditvalue='';
			 $scope.costvalue='';
			 $scope.startDate='';
			 $scope.endDate='';
		 }
		 
		 $scope.settrainingtype=function(){
			
			 $scope.type=$scope.trainingtypeid;
			 if($scope.type==1){
				 $scope.instructordiv=false;
				 $scope.tdt=false;
				 $scope.trctg=true;
				 $scope.cst=true;
				 $scope.crdt=true;
			 }else if($scope.type==2){
				 $scope.trctg=true;
				 $scope.cst=true;
				 $scope.crdt=true;
				 $scope.tdt=true;
				 $scope.instructordiv=true;
			 }else if($scope.type==4){
				 $scope.trctg=false;
				 $scope.cst=false;
				 $scope.crdt=false;
				 $scope.tdt=false;
				 $scope.instructordiv=false;
				 $scope.trainingtitleDiv=true;
			 }else{
				 $scope.trctg=true;
				 $scope.cst=true;
				 $scope.crdt=true;
				 $scope.tdt=true;
				 $scope.instructordiv=true;
				 $scope.trainingtitleDiv=true;
			 }
		 }
		 
		 $scope.searchresult=function(){
			  $scope.preloader_popup=true;
			 if($scope.trainingtypeid==undefined){$scope.trainingtypeid='';}
			 if($scope.trainingtypeid.trim!=""){
				$scope.trainingtypeid=$scope.trainingtypeid;
			}else{
				$scope.trainingtypeid='';
			}
			if($scope.trainingtitle.trim!=""){
				$scope.trainingtitle=$scope.trainingtitle;
			}else{
				$scope.trainingtitle='';
			}
			if($scope.instructorname.trim!=""){
				$scope.instructorname=$scope.instructorname;
			}else{
				$scope.instructorname='';
			}
			if($scope.category.trim!=""){
				$scope.category=$scope.category;
			}else{
				$scope.category='';
			}
			if($scope.creditvalue.trim!=""){
				$scope.creditvalue=$scope.creditvalue;
			}else{
				$scope.creditvalue='';
			}
			if($scope.costvalue.trim!=""){
				$scope.costvalue=$scope.costvalue;
			}else{
				$scope.costvalue='';
			}
			if($scope.startDate.trim!=""){
				$scope.startDate=$scope.startDate;
			}else{
				$scope.startDate='';
			}
			if($scope.endDate.trim!=""){
				$scope.endDate=$scope.endDate;
			}else{
				$scope.endDate='';
			}
			var filterdata={trainingtypeid:$scope.trainingtypeid,trainingtitle:$scope.trainingtitle,instructorname:$scope.instructorname,category:$scope.category,creditvalue:$scope.creditvalue,costvalue:$scope.costvalue,startdate:$scope.startDate,endDate:$scope.endDate};
			var advancedata={_token:$scope.tokenuse,'filterdata':filterdata};
			$scope.trainingCatalogDataGrid(advancedata,'advancefilter');
			 $scope.advancebutton=true;
			 $scope.advanceSearch=false;
			/* $http({
				method:"POST",
				url:"/mylearning/advanceFilter",
				data:
			}).success(function(response){
				 if(!$scope.trainingtypeid){$scope.trainingtypeid=5;}
					 $scope.setcat=$scope.category;
					 $scope.setItemType=$scope.trainingtypeid;
					 $scope.advancebutton=true;
					 $scope.advanceSearch=false;
					 $scope.preloader_popup=false;
					 $scope.trainingCatalogData=response;
					 $scope.trainingtypeid='';
					 $scope.trainingtitle='';
					 $scope.instructorname='';
					 $scope.category='';
					 $scope.creditvalue='';
					 $scope.costvalue='';
					 $scope.startDate='';
					 $scope.endDate='';
					 $scope.settrainingtype();
			}) */
			 
		 }
		 
		
		/*   $scope.getLabel=function(){
			   $http({
				   method:"POST",
				   url:"/user/locale.json"
			   }).success(function(response){
				  
				   $scope.responseLib=response[$scope.lang];
				   
				   //alert($scope.responseLib.TranslationRepository);
				   alert(JSON.stringify($scope.responseLib));
			   })  
		  }
	  $scope.getLabel(); */
		 
		
});

