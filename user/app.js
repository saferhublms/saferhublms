/**
 * Main AngularJS Web Application
 */
var modules = [
	'ui.router',
	'ngStorage',
	'ui.bootstrap',
	'ngMaterial',
	'loginmodule',
	'requirementservice',
	'ui.grid',
	'ngAnimate',
	'Alertify',
	'ui.grid.pagination',
	'ngSanitize',
	'inprogressservice',
	'ui.grid.pinning',
	'ui.grid.expandable',
	'ui.grid.selection',
	'ui.grid.resizeColumns',
	'ui.grid.autoResize',
	'oc.lazyLoad',
	'datatables'
];


var app = angular.module('eLightApp', modules, function($interpolateProvider) {
	
});

app.provider('routes', function($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider,$mdDateLocaleProvider){
    this.$get = function($http){
        return {
            initialize: function(){
				var tokenuse=$("input[name=_token]").val();
				return	$http({
				url:'/api/getDateFormate',
				method:'get',
				data:{'_token':tokenuse}
				}).success(function(response){
				$mdDateLocaleProvider.formatDate = function(date) {
                  if(date){
				    return moment(date).format(response.toUpperCase());
				  }else{
					  return '';
				  }
				};  
				});
            }
        };
    }; 
});

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider','$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider,$httpProvider, $locationProvider,$ocLazyLoadProvider){
	
	   $stateProvider.state('aboutus', {
        url: "/index/about",
        views : {
            "mainView" : {
				 controller: 'aboutusController',
                templateUrl:"/user/templates/helpcontent/about_us.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/about_us.js');
            }]
        }
    })
	
	 $stateProvider.state('announcement', {
        url: "/index/announcement",
        views : {
            "mainView" : {
				 controller: 'announcementsController',
                templateUrl:"/user/templates/helpcontent/announcements.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/about_us.js');
            }]
        }
    })
	
	 $stateProvider.state('help', {
        url: "/index/help",
        views : {
            "mainView" : {
				 controller: 'helpController',
                templateUrl:"/user/templates/helpcontent/help.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/about_us.js');
            }]
        }
    })
	
	 $stateProvider.state('helpreference', {
        url: "/index/view-reference/viewid/:viewid",
        views : {
            "mainView" : {
				 controller: 'referenceController',
                templateUrl:"/user/templates/helpcontent/view-reference.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/about_us.js');
            }]
        }
    })
	
	 
	
	$stateProvider.state('/mylearning/inprogress', {
        url: "/mylearning/inprogress",
        views : {
            "mainView" : {
				 controller: 'InProgressController',
                templateUrl:"/user/templates/mylearning/inprogress.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/inprogress.js');
            }]
        }
    })
	
	 $stateProvider.state('/trainingcatalog/blendedprogram', {
        url: "/trainingcatalog/blendedprogram/viewId/:code",
        views : {
            "mainView" : {
				 controller: 'BlendedProgramController',
                templateUrl:"/user/templates/trainingcatalog/blendedprogram.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/inprogress.js');
            }]
        }
    })
	
	 $stateProvider.state('/mylearning/requirement', {
        url: "/mylearning/requirement",
        views : {
            "mainView" : {
				 controller: 'RequirementController',
                templateUrl:"/user/templates/requirement.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/requirement.js');
            }]
        }
    })
	
	  $stateProvider.state('/blendedprogramnew', {
        url: "/trainingcatalog/blendedprogram/viewId/:code/itemTypeId/:itemTypeId/categoryTypeID/:categoryTypeID",
        views : {
            "mainView" : {
				 controller: 'BlendedProgramController',
                templateUrl:"/user/templates/trainingcatalog/blendedprogram.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/inprogress.js');
            }]
        }
    })
	
	 $stateProvider.state('/supervisor/supervisortools', {
        url: "/supervisor/supervisortools",
        views : {
            "mainView" : {
				 controller: 'SupervisiorTool',
                templateUrl:"/user/templates/supervisor/supervisortools.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/supervisiortool.js');
            }]
        }
    })
	$stateProvider.state('trainingcatalogindex', {
        url: "/trainingcatalog/index",
        views : {
            "mainView" : {
				 controller: 'TrainingCatalogController',
                templateUrl:"/user/templates/trainingcatalog/trainingcatalog.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/trainingcatalog.js');
            }]
        }
    })
	
	 $stateProvider.state('mylearningassignment', {
        url: "/mylearning/assignment",
        views : {
            "mainView" : {
				 controller: 'LearningAssignemtController',
                templateUrl:"/user/templates/mylearning/mylearningassignment.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	 $stateProvider.state('mylearningmyenrollments', {
        url: "/mylearning/myenrollments",
        views : {
            "mainView" : {
				 controller: 'MyEnrollmentsController',
                templateUrl:"/user/templates/mylearning/mylearningenrollement.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	$stateProvider.state('learnassessments', {
        url: "/mylearning/learnassessments",
        views : {
            "mainView" : {
				 controller: 'AssessmentController',
                templateUrl:"/user/templates/mylearning/learnassessments.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	 
	$stateProvider.state('mylearningmysubmittraining', {
        url: "/mylearning/learnsubmittraining",
        views : {
            "mainView" : {
				 controller: 'MySubmitTrainingController',
                templateUrl:"/user/templates/mylearning/mylearningmysubmittraining.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	 $stateProvider.state('viewtraining', {
        url: "/elearningmedia/training/:code",
        views : {
            "mainView" : {
				 controller: 'elearningmediaController',
                templateUrl:"/user/templates/elearningmedia/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	  $stateProvider.state('elearningviewtraining', {
        url: "/elearningmedia/training/viewId/:code/itemTypeId/:itemId/categoryTypeID/:categorytypeid",
        views : {
            "mainView" : {
				 controller: 'elearningmediaController',
                templateUrl:"/user/templates/elearningmedia/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	$stateProvider.state('elearningscormtraining', {
        url: "/elearningmedia/training/viewId/:code/itemTypeId/:itemId/categoryTypeID/:categorytypeid/scormparam/:scormparam",
        views : {
            "mainView" : {
				 controller: 'elearningmediaController',
                templateUrl:"/user/templates/elearningmedia/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	$stateProvider.state('elearningscormviewtraining', {
        url: "/elearningmedia/training/viewId/:code/scormparam/:scormparam",
        views : {
            "mainView" : {
				 controller: 'elearningmediaController',
                templateUrl:"/user/templates/elearningmedia/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })

	 $stateProvider.state('viewassessment', {
        url: "/course/viewassessment/:code",
        views : {
            "mainView" : {
				 controller: 'ViewAssessmentController',
                templateUrl:"/user/templates/assessment/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	 $stateProvider.state('mylearningtranscript', {
        url: "/mylearning/transcript",
        views : {
            "mainView" : {
				 controller: 'MyTranscriptController',
                 templateUrl:"/user/templates/transcript/transcript.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	$stateProvider.state('mediaplayer', {
        url: "/elearningmedia/mediaplayer/viewId/:mediacode",
        views : {
            "mainView" : {
				 controller: 'mediaplayerController',
                templateUrl:"/user/templates/elearningmedia/playernew.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	 $stateProvider.state('classroomcourse', {
        url: "/classroomcourse/training/:code/:classid",
        views : {
            "mainView" : {
				 controller: 'classroomcourseController',
                templateUrl:"/user/templates/classroomcourse/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	 $stateProvider.state('classroomcoursenew', {
        url: "/classroomcourse/training/viewId/:code/itemTypeId/:itemTypeId/categoryTypeID/:categoryTypeID",
        views : {
            "mainView" : {
				 controller: 'classroomcourseController',
                templateUrl:"/user/templates/classroomcourse/training.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	  $stateProvider.state('mylearninglearningplan', {
        url: "/mylearning/learningplan",
        views : {
            "mainView" : {
				 controller: 'LearningPlanController',
                templateUrl:"/user/templates/mylearning/mylearningplan.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	$stateProvider.state('survayviewassessment', {
        url: "/survey/viewassessment/user/:user/token/:token/:red_page/:learnassessments",
        views : {
            "mainView" : {
				 controller: 'ViewAssessmentController',
                templateUrl:"/user/templates/mylearning/survayviewassessment.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/mylearning.js');
            }]
        }
    })
	
	
	$stateProvider.state('learningrequirement', {
        url: "/mylearning/learningrequirement",
        views : {
            "mainView" : {
				 controller: 'MyRequirementController',
                templateUrl:"/user/templates/mylearning/learningrequirement.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/user/controllers/myrequirement.js');
            }]
        }
    })	

	
	
	 
	
	
	
	
}]);

app.run(["$rootScope","$state", "$interval","$window","$http","$location","$localStorage","$window","routes", function($rootScope, $state,$interval,$window,$http,$location,$localStorage,$window,routes)
{
	
	
 routes.initialize();
 $rootScope.footer_class ='footer_links_new';
 $rootScope.footer_link_class='bluecol_cmid30_footer_link footer_links_new';
 $rootScope.newData=new Date();
 $rootScope.year = $rootScope.newData.getFullYear();
	
	
	
 
  if($localStorage.user_id && $localStorage.user_id != undefined){
		    $rootScope.login = 2;
	    }else{
			 $rootScope.login = 1;
	}


   $rootScope.logout = function(){
	  $http({
		     method:"GET",
	         url:"/logout",
		}).success(function(response){
			 if(response.status ==200){
				 $rootScope.login = 1;
				$localStorage.$reset();
				 $location.path('#/login');
			 } 
		})
   }
   
   
   
	   $rootScope.getAppNav = function(){
		  $http({
				url:'/api/getAppNav',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.appsheadernav=response;
				//alert(response);
		    
			});
	  }
	
   $rootScope.getAppNav();
   
   $rootScope.getFooterNav = function(){
		  $http({
				url:'/api/getFooterNav',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.footernav=response;
				//alert(response);
		    
			});
	  }
	
   $rootScope.getFooterNav();
   
   
   $rootScope.companyCmsInfo = function(){
		  $http({
				url:'/api/companyCmsInfo',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.companyCmsInfo=response;
				//alert(JSON.stringify(response.companyInfo));
		    
			});
	  }
	
   $rootScope.companyCmsInfo();
   
   
    $rootScope.getLeftNav=function(){
			$http({
				method:"POST",
				url:"/api/getLeftNav",
			}).success(function(response){
				$rootScope.leftMenuData=response.leftMenu;
				//alert(JSON.stringify($scope.leftMenuData));
			})
		}
		
		$rootScope.getLeftNav();
		
		
		 $rootScope.lang='en';
		/* $rootScope.getLabel=function(){
			   $http({
				   method:"POST",
				   url:"/user/locale.json"
			   }).success(function(response){
				   $rootScope.responseLib=response[$rootScope.lang];
				   //alert(JSON.stringify( $rootScope.responseLib));
			   })  
		  }
	  $rootScope.getLabel(); */
	  
	  
			
		$rootScope.getLabel=function(){
			   $http({
				   method:"POST",
				   url:"/api/getmultilanguallabel",
				   data:{language:$rootScope.lang}
			   }).success(function(response){
				   $rootScope.responseLib=response.data;
				   //alert(JSON.stringify( $rootScope.responseLib));
			   })  
		  }
	  $rootScope.getLabel();
		
		
   
   

}]);

app.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function(value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
}]);
