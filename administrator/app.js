/**
 * Main AngularJS Web Application
 */
var modules = [
	'ui.router',
	'ngStorage',
	'ui.bootstrap',
	'ngMaterial',
	'ui.grid',
	'ngAnimate',
	'Alertify',
	'ui.grid.pagination',
	'ngSanitize',
	'ui.select',
	'ui.grid.pinning',
	'ui.grid.expandable',
	'ui.grid.selection',
	'ui.grid.resizeColumns',
	'ui.grid.autoResize',
	'oc.lazyLoad',
	'angularUtils.directives.dirPagination',
	'angAccordion',
	  'datatables',
	  'ngAria',
	  'mdPickers',
	  'summernote',
	/* 'dashboardmodule', */
	/* 'coursemodule', */
/* 	'traininglibrarymodule', */
	/* 'adminmodule', */
/* 	'releasemodule', */
	/* 'notificationmgmtmodule', */
	'angularFileUpload',
	/* 'angularTrix', */
/* 	'systemmanagemodule' */,
  'oc.lazyLoad', 
    /*  'ngTagsInput', */
	'ui.switchery',
	'ui.toggle',
	/* 'toggle-switch', */
	'uiSwitch',
	
	
	
	
	
	'subadminmodule',
	'admingroupmodule', 
	
];


var app = angular.module('eLightApp', modules, function($interpolateProvider) {
	
});
app.provider('routes', function($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider,$mdDateLocaleProvider){
    this.$get = function($http){
        return {
            initialize: function(){
				var tokenuse=$("input[name=_token]").val();
				return	$http({
				url:'/api/getDateFormate',
				method:'get',
				data:{'_token':tokenuse}
				}).success(function(response){
				$mdDateLocaleProvider.formatDate = function(date) {
                  if(date){
				    return moment(date).format(response.toUpperCase());
				  }else{
					  return '';
				  }
				};  
				});
            }
        };
    }; 
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider','$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider,$httpProvider, $locationProvider,$ocLazyLoadProvider,$mdAriaProvider){
	
	$urlRouterProvider.otherwise("/dashboard");

	

	 $stateProvider.state('dashboard', {
        url: "/dashboard",
        views : {
            "mainView" : {
				 controller: 'DashboardController',
                templateUrl:"/administrator/templates/dashboard.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/dashboard.js');
            }]
        }
    })
	
	
		$stateProvider.state('adminindex', {
        url: "/admin/manageuserlist",
        views : {
            "mainView" : {
				 controller: 'UserListController',
                templateUrl:"/administrator/templates/admin/userlist.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/admin.js');
            }]
        }
    })
	
	$stateProvider.state('adduser', {
        url: "/admin/adduser",
        views : {
            "mainView" : {
				 controller: 'AddUserController',
                templateUrl:"/administrator/templates/admin/adduser.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/admin.js');
            }]
        }
    })
	
	$stateProvider.state('coursesindex', {
        url: "/courses/index",
        views : {
            "mainView" : {
				 controller: 'CourseListController',
                templateUrl:"/administrator/templates/courses/courselist.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/courses.js');
            }]
        }
    })
	
	$stateProvider.state('coursesactivity', {
        url: "/courses/coursesactivity",
        views : {
            "mainView" : {
				 controller: 'CoursesactivityController',
                templateUrl:"/administrator/templates/courses/courses.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/courses.js');
            }]
        }
    })
	
	$stateProvider.state('courseaddvideo', {
        url: "/courses/addvideo/:courseId",
        views : {
            "mainView" : {
				 controller: 'CoursesAddvideoController',
                templateUrl:"/administrator/templates/courses/addvideo.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/courses.js');
            }]
        }
    })
	
	$stateProvider.state('classroomcourse', {
        url: "/courses/classroomcourses",
        views : {
            "mainView" : {
				 controller: 'ClassroomCoursesController',
                 templateUrl:"/administrator/templates/courses/classroomcourses.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/courses.js');
            }]
        }
    })
	
	$stateProvider.state('editclassroomcourse', {
        url: "/courses/editclassroomcourse/:courseCode",
        views : {
            "mainView" : {
				 controller: 'EditClassroomCoursesController',
                 templateUrl:"/administrator/templates/courses/editclassroomcourses.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/classroomcourse.js');
            }]
        }
    })
	$stateProvider.state('assessment', {
        url: "/courses/assessment",
        views : {
            "mainView" : {
				 controller: 'AssessmentController',
                templateUrl:"/administrator/templates/courses/assessment.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/assessment.js');
            }]
        }
    })
	
	$stateProvider.state('notificationmanagements', {
        url: "/notificationmanagements/notificationmgts",
        views : {
            "mainView" : {
				 controller: 'NotificationmgtsController',
                templateUrl:"/administrator/templates/SuperAdmin/notificationmgts.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/notificationmgmt.js');
            }]
        }
    })
	
	$stateProvider.state('notificationmanagementsedit', {
        url: "/notificationmanagements/edit/:notificationId",
        views : {
            "mainView" : {
				 controller: 'NotificationmgtsEditController',
                templateUrl:"/administrator/templates/SuperAdmin/notificationmgtsedit.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/notificationmgmt.js');
            }]
        }
    })
	
	
	$stateProvider.state('surealeasereleases', {
        url: "/surealease/releases",
        views : {
            "mainView" : {
				 controller: 'ReleaseController',
                templateUrl:"/administrator/templates/SuperAdmin/release.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/release.js');
            }]
        }
    })
	
	
		$stateProvider.state('systemmanagementsystemmgt', {
        url: "/systemmanagement/systemmgt",
        views : {
            "mainView" : {
				 controller: 'SystemmgtController',
                templateUrl:"/administrator/templates/systemmanagement/systemmgt.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/systemmanagement.js');
            }]
        }
    })
	
	
		$stateProvider.state('traininglibraryindex', {
        url: "/traininglibrary/index",
        views : {
            "mainView" : {
				 controller: 'TraininglibraryController',
                templateUrl:"/administrator/templates/SuperAdmin/traininglibrary.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/traininglibrary.js');
            }]
        }
    })
	$stateProvider.state("managegroups", {
		url:"/admin/managegroups",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/admin/grouplist.html',
				controller: 'GroupListController'
			}
		},
		resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/admingroup.js');
            }]
        }
	});	
	
	
		$stateProvider.state('reportsschedulereport', {
        url: "/reports/schedulereport",
        views : {
            "mainView" : {
				 controller: 'ScheduleReportController',
                templateUrl:"/administrator/templates/Reporting/schedulereport.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/reporting.js');
            }]
        }
    })
	
	
		$stateProvider.state('elearning-courseslist-elearning', {
        url: "/elearning-courses/list-elearning",
        views : {
            "mainView" : {
				 controller: 'elearningListController',
                templateUrl:"/administrator/templates/elearning/elearninglist.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/elearning.js');
            }]
        }
    })
	
	$stateProvider.state('manageusersedit', {
        url: "/admin/manageusersedit/:selectedusertoken",
        views : {
            "mainView" : {
				 controller: 'EditUserController',
                  templateUrl:"/administrator/templates/admin/edituser.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/admin.js');
            }]
        }
    })
	
	$stateProvider.state('manageclasses', {
        url: "/courses/manageclasses/:courseCode",
        views : {
            "mainView" : {
				 controller: 'ManageClassesController',
                  templateUrl:"/administrator/templates/courses/manageclasses.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/classroomcourse.js');
            }]
        }
    })
	$stateProvider.state('addclasses', {
        url: "/courses/addclasses/:courseCode",
        views : {
            "mainView" : {
				 controller: 'AddClassesController',
                  templateUrl:"/administrator/templates/courses/addclasses.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/classroomcourse.js');
            }]
        }
    })
		
	$stateProvider.state('editelearning', {
        url: "/courses/editelearning/:courseCode",
        views : {
            "mainView" : {
				 controller: 'EditElearningController',
                  templateUrl:"/administrator/templates/courses/editelearning.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/elearning.js');
            }]
        }
    })
		
	$stateProvider.state('editassessment', {
        url: "/courses/editassessment/:courseCode",
        views : {
            "mainView" : {
				 controller: 'EditAssessmentController',
                  templateUrl:"/administrator/templates/courses/editassessment.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/assessment.js');
            }]
        }
    })
		
	$stateProvider.state('editclass', {
        url: "/courses/editclass/:courseCode",
        views : {
            "mainView" : {
				 controller: 'EditClassController',
                  templateUrl:"/administrator/templates/courses/editclass.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/classroomcourse.js');
            }]
        }
    })
	

	$stateProvider.state('distribution', {
        url: "/courses/coursedistribtion/:courseCode",
        views : {
            "mainView" : {
				 controller: 'CoursedistributionController',
                  templateUrl:"/administrator/templates/courses/coursedistribution.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/coursedistribution.js');
            }]
        }
    })

	$stateProvider.state('notificationmgt', {
        url: "/notificationtemplate/notificationmgt",
        views : {
            "mainView" : {
				 controller: 'SystemManageNotificationController',
                  templateUrl:"/administrator/templates/systemmanagement/notificationmgt.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/notificationmgmt.js');
            }]
        }
    })
		
		
	$stateProvider.state('notificationedit', {
        url: "/notificationtemplete/edit/:notificationId",
        views : {
            "mainView" : {
				 controller: 'NotificatioEditController',
                templateUrl:"/administrator/templates/systemmanagement/notificationedit.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/notificationmgmt.js');
            }]
        }
    })	

	$stateProvider.state('learningrequirement', {
        url: "/learningrequirement",
        views : {
            "mainView" : {
				 controller: 'MyRequirementmoduleController',
                templateUrl:"/administrator/templates/systemmanagement/learningrequirement.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/myrequirement.js');
            }]
        }
    })	

	$stateProvider.state('labelmanagemnet', {
        url: "/label-management",
        views : {
            "mainView" : {
				 controller: 'LabelMangementcontroller',
                templateUrl:"/administrator/templates/labelmanagement/labelmanagement.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/labelmanagement.js');
            }]
        }
    })	

	$stateProvider.state('addlabel', {
        url: "/label-management/addlabel",
        views : {
            "mainView" : {
				 controller: 'AddLabelcontroller',
                templateUrl:"/administrator/templates/labelmanagement/addlabel.html"
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/administrator/controllers/labelmanagement.js');
            }]
        }
    })	

	
	//courses/coursedistribtion/'+courseCode
	
	
}]);

app.run(["$rootScope","$state", "$interval","$window","$http","$location","$localStorage","$window","routes", function($rootScope, $state,$interval,$window,$http,$location,$localStorage,$window,routes)
{

 routes.initialize();
 $rootScope.footer_class ='footer_links_new';
 $rootScope.footer_link_class='bluecol_cmid30_footer_link footer_links_new';
 $rootScope.newData=new Date();
 $rootScope.year = $rootScope.newData.getFullYear();
 

	
	
	
 
  if($localStorage.user_id && $localStorage.user_id != undefined){
		    $rootScope.login = 2;
	    }else{
			 $rootScope.login = 1;
	}


   $rootScope.logout = function(){
	  $http({
		     method:"GET",
	         url:"/logout",
		}).success(function(response){
			 if(response.status==200){
				 $rootScope.login = 1;
				$localStorage.$reset();
				 $location.path('#/login');
			 } 
		})
   }
   
   
   
	   $rootScope.getAppNav = function(){
		  $http({
				url:'/api/getAppNav',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.appsheadernav=response;
				//alert(response);
		    
			});
	  }
	
   $rootScope.getAppNav();
   
   $rootScope.getFooterNav = function(){
		  $http({
				url:'/api/getFooterNav',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.footernav=response;
				//alert(response);
		    
			});
	  }
	
   $rootScope.getFooterNav();
   
   
   $rootScope.companyCmsInfo = function(){
		  $http({
				url:'/api/companyCmsInfo',
				method:'post',
				data:{'authToken':$localStorage.authtoken}
			}).success(function(response){
				$rootScope.companyCmsInfo=response;
				//alert(JSON.stringify(response.companyInfo));
		    
			});
	  }
	
   $rootScope.companyCmsInfo();
   
   
    $rootScope.getLeftNav=function(){
			$http({
				method:"POST",
				url:"/api/getLeftNav",
			}).success(function(response){
				$rootScope.leftMenuData=response.leftMenu;
				//alert(JSON.stringify($scope.leftMenuData));
			})
		}
		
		$rootScope.getLeftNav();
		
		
		$rootScope.addclass=function(menu_item_id){ 
		if(angular.element('#cat_nav_'+menu_item_id).hasClass('active')==true){
			angular.element(document.querySelector('#cat_nav_'+menu_item_id)).removeClass('active');
			angular.element(document.querySelector('#cat_nav_'+menu_item_id+' ul')).css({ display: "none" });
			return false;
		}
		angular.forEach($rootScope.leftMenuData,function(list){
			angular.element(document.querySelector('#cat_nav_'+list.menu_item_id)).removeClass('active');
			angular.element(document.querySelector('#cat_nav_'+list.menu_item_id+' ul')).css({ display: "none" });
		})
		
			var menuItem=angular.element(document.querySelector('#cat_nav_'+menu_item_id));
			menuItem.addClass('active');
			angular.element(document.querySelector('#cat_nav_'+menu_item_id+' ul')).css({ display: "block" });
			
			//alert(menu_item_id);
			
		}
		
		$rootScope.getTrainingType=function(){
			$http({
				method:"GET",
				url:"/admin/gettrainingtypeplusmediatype"
			}).success(function(response){
				$rootScope.mediatrainigType=response;
				$rootScope.trainingType=response.trainingtype;
				angular.forEach($rootScope.trainingType,function(list){
					
					
					if(list.training_type_id==2){
						list.logo='../user/assets/images/2.png';
						list.url='#/courses/classroomcourses';
						list.training_type='Virtual Classroom/Classroom';
					}else if(list.training_type_id==5){
						list.logo='../user/assets/images/3.png';
						list.url='#/courses/assessment';
					}else if(list.training_type_id==1){
						list.logo='../user/assets/images/1.png';
						list.url='#/courses/coursesactivity';
					}else{
						list.logo='../user/assets/images/4.png';
						list.url='#/courses/coursesactivity';
					}
					
				})
			})
		}
		$rootScope.getTrainingType();
		
		
		$rootScope.getaudiovideoPageData=function(){
			$http({
				method:"POST",
				url:"/admin/getAudioVideoPageData"
			}).success(function(response){
				$rootScope.audioVideoData=response;
				//alert(response);
			})
		}
		$rootScope.getaudiovideoPageData();
		
		
		
   
   

}]);

app.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function(value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
}]);
