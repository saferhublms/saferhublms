var xmlHttpRequest;
function createXMLHttpRequest()
{
	if (xmlHttpRequest != null) {
		return;
	}
	if (window.ActiveXObject) {
		try {
			xmlHttpRequest = new ActiveXObject('Msxml2.XMLHTTP');
		} catch (exception_1) {
			try {
				xmlHttpRequest = new ActiveXObject('Microsoft.XMLHTTP');
			} catch (exception_2) {
			}
		}
	} else if (window.XMLHttpRequest) {
		xmlHttpRequest = new XMLHttpRequest();
	}
}
var SITEROOT;

function cmpDate(dat1, dat2) {
	// define local variables
	var day, mSec1, mSec2;
	// number of milliseconds in one day
	day = 1000 * 60 * 60 * 24;
	// define number of milliseconds for both dates
	mSec1 = (new Date(dat1.substring(6, 10), dat1.substring(3, 5) - 1, dat1.substring(0, 2))).getTime();
	mSec2 = (new Date(dat2.substring(6, 10), dat2.substring(3, 5) - 1, dat2.substring(0, 2))).getTime();
	// return number of days between dat1 and dat2
	return Math.ceil((mSec2 - mSec1) / day);

}

function getMonthNumber(monthname){
	var monthnumber='';
	if(monthname=='Jan')
		monthnumber='01';
	if(monthname=='Feb')
		monthnumber='02';
	if(monthname=='Mar')
		monthnumber='03';
	if(monthname=='Apr')
		monthnumber='04';
	if(monthname=='May')
		monthnumber='05';
	if(monthname=='Jun')
		monthnumber='06';
	if(monthname=='Jul')
		monthnumber='07';
	if(monthname=='Aug')
		monthnumber='08';
	if(monthname=='Sep')
		monthnumber='09';
	if(monthname=='Oct')
		monthnumber='10';
	if(monthname=='Nov')
		monthnumber=11;
	if(monthname=='Dec')
		monthnumber=12;
	return monthnumber;


}
var $recurrvalue;
function updaterecsetting()
{
	var baseurl=document.getElementById('baseurl').value;
	var extndval;
	var assessid=document.getElementById('assessid').value;
	var selecteddate = document.getElementById("selecteddate").value;
	var weekselected = document.getElementById("weekselected").value;
	var dayselected = document.getElementById("dayselected").value;
	var monthchecked = document.getElementById("sessionlastmontchecked").value;
	var upcoming=document.getElementById('upcoming').value;

	var customdayselected = document.getElementById("customdayselected").value;
	var reoccprop=document.getElementById('learningpropselected').value;
	var choice_type=document.recform.choice_type;
	var choosen=0;
	var recurrvalue1=document.getElementById("is_recurr");
	var recurrvalue='';
	if(upcoming>25){
		alert('Assessment List has exceeded the Limit across 25.');
		return false;
	}
	if(recurrvalue1.checked==true){
		recurrvalue=1;
		document.getElementById("recval").value=1;
	}
	else{
		recurrvalue=0;
		document.getElementById("recval").value=0;
	}
	$recurrvalue=recurrvalue;
	extndval='/is_recurr/'+recurrvalue;

	for(var i=0; i < choice_type.length; i++)
	{

		if(choice_type[i].checked ){
			choosen=choice_type[i].value;
		}
		
	}
	extndval=extndval+'/choicetype/'+choosen;
	var currentdate=document.getElementById('currentdate').value;
	var surveyclosedate=document.getElementById('closesurveydate').value;
	var selendingtype=document.getElementById('endingtype').value;
	var endafter=document.getElementById('afterocc').value;
	var selendlength=document.getElementById('endingtype').length;
	var surveyclosedatemode=document.getElementById('nodateofending');
	var enddatemode='';
	var closedatemode='';
	var i = document.getElementById("endingtype").selectedIndex;
	var selectedoperation=  document.getElementById("endingtype").options[i].value;
	if(selectedoperation){
		extndval=extndval+'/endingtype/'+selectedoperation;
		enddatemode=selectedoperation;
		closedatemode=selectedoperation;
		//	alert(surveyclosedate);
		if(enddatemode=='endafterocc'){
			if(endafter=='' || endafter==0){
				
				alertify.alert('Please Enter value for Occurrence greater than 0.');
				return false;
			}
			extndval=extndval+'/endafter/'+endafter;
		}
		if(enddatemode=='closedate'){

			if(surveyclosedate==''){
				enddatemode='';
			//	aler('Pelase Select Close Date');
			//	return false;
			}
			else
			{
				if (surveyclosedate.indexOf("/") != -1)
				{
				}
				else
				{
					var a=surveyclosedate.split(' ');
					var monthname=a[1];
					var mdate=a[0];
					var myear=a[2];
					var monthnumber=getMonthNumber(monthname);
					surveyclosedate=monthnumber+"/"+mdate+"/"+myear;
				}

			}

		}
	}
	
	if(recurrvalue==1)
	{

		if(enddatemode==''){
			if(reoccprop!='')
			{
				alertify.alert('Please Select Close Date for Assessement.');
				return false;
			}

		}

		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		//		alert(surveyopendate);
		//		alert(currentdate);
		//		alert(surveyclosedate);

		if(closedatemode=='closedate'){
			if(surveyclosedate!=''){
				if (cmpDate(currentdate,surveyclosedate) < 0) {
					
					alertify.alert('Close  Date of Recurrence should not be Smaller than Current Date!');
					return false;
				}
				var spclosedate=surveyclosedate.replace("/","-");
				var rpclosedate=spclosedate.replace("/","-");
				var splitclosedate=rpclosedate.split('-');
				var closedateofsurvey=splitclosedate[2]+"-"+splitclosedate[0]+"-"+splitclosedate[1];
				extndval=extndval+'/closesurveydate/'+closedateofsurvey;
			}
		}
	}
	
	if(reoccprop==1){
		var inevery=document.getElementById('everyids').value;
		if(inevery<=0 || inevery=='')
		{
			alertify.alert('Days Value should be greater than 0');
			return false;
		}
		extndval=extndval+'/daysintval/'+inevery;
	}
	if(reoccprop==2){
		var ineveryweek=document.getElementById('everyid').value;
		if(ineveryweek>= 0 && ineveryweek>5 ||  ineveryweek=='')
		{
			alertify.alert('Days Value should be greater than 0 and lesser than 5');
			return false;
		}
		if(dayselected==''){
			
			alertify.alert('Please Select Week Day(s).');
			return false;
		}
		extndval=extndval+'/weekselected/'+ineveryweek+'/dayselected/'+dayselected;
	}
	if(reoccprop==3){

		if(choosen==0)
		{
			if(recurrvalue=='1')
			{
				alertify.alert('Please Select "Each" or "On the" Field to make Dates.');
				
				return false;
			}
			
		}
		if(choosen==1 )
		{
			if(recurrvalue=='1')
			{

				if(selecteddate==0){
					
					alertify.alert('Please Select Date for the Month.');
					return false;
				}
			}
			extndval=extndval+'/selecteddate/'+selecteddate;
		//	alert(selecteddate)
		}

		if(choosen==2)
		{
			if(recurrvalue=='1')
			{

				//alert(customdayselected);
				//alert(weekselected);
				if(customdayselected=='' || weekselected==''){
					
					alertify.alert('Please Select Week and Day.');
					return false;
				}
			}
			extndval=extndval+'/customdayselected/'+customdayselected+'/weekselected/'+weekselected;
		}
	}

	if(reoccprop==4)
	{
		if(choosen==0)
		{
			if(recurrvalue=='1')
			{
				alertify.alert('Please Select "Each" or "On the" Field to make Dates.');
				return false;
			}
		}
		if(choosen==1)
		{
			if(recurrvalue=='1')
			{
				if(selecteddate==0){
					
					alertify.alert('Please Select Date for the Month.');
					return false;
				}
			}
			extndval=extndval+'/selecteddate/'+selecteddate;
		}
		if(choosen==2)
		{
			if(recurrvalue=='1')
			{
				if(customdayselected=='' || weekselected==''){
				
					alertify.alert('Please Select Week and Day.');
					return false;
				}
			}
			extndval=extndval+'/customdayselected/'+customdayselected+'/weekselected/'+weekselected;
		}
		if(recurrvalue=='1')
		{
			if(monthchecked=='')
			{
				alertify.alert('Please Select Month for Yearly Mode.');
				return false;
			}
			extndval=extndval+'/monthchecked/'+monthchecked;
		}
	}
	//alert(extndval);

	var url=SITEROOT+'/assessment/recsetting'
	+'/mode'+'/updaterecprop/assessid/'+assessid+'/upcoming/'+upcoming
	+'/selectedtaboption/'+reoccprop+extndval;
	//alert(url);
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {

		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleupdaterecsetting ;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}

}
function handleupdaterecsetting()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {

			var xmlDoc=xmlHttpRequest.responseText;
			if($recurrvalue==1){
				//alert(xmlDoc);
				  var url=SITEROOT+'/assessment/assessmenttools';
		window.location=url;
				}
			else
			alertify.alert('Mark Checked to Activate Recurrence Properties');
		//tb.remove();
		}
	}
}
function submitrecassessment(event)
{
	var baseurl=document.f1.baseurl.value;
	var topic=document.getElementById('assessname').value;
	var scoretype=document.f1.scoretype.value;
	var randques=document.f1.randomques;
	var assessid=document.f1.assessid.value;
	var description=document.f1.instructions.value;
	var noofdaystext=document.f1.noofdaystext.value;
	var randomquestion=0;
	if(randques.checked==true){
		randomquestion=1;
		document.getElementById("randomize_ques").value=1;
	}
	else{
		randomquestion=0;
		document.getElementById("randomize_ques").value=0;
	}


	var regexNum = /\d/;
	if(noofdaystext=='')
	{
		alertify.alert("Pleas Enter No. of Days in  Due Date Field.");
		return false;
	}
	if (!regexNum.test(noofdaystext))
	{
		alertify.alert( "Please Enter No. of Days in Digits in  Due Date Field." );
		return false;
	}

	$assessid=assessid;
	SITEROOT=baseurl;
	var e = event.keyCode;
	if(e == 9)
	{
		return false;
	}
	var iChars = "!@#$%^()+=[]\\\';,/{}|\":<>?";
	var reWhiteSpace = new RegExp(/^\s+$/);
	for (var i = 0; i < topic.length; i++)
	{
		if (iChars.indexOf(topic.charAt(i)) != -1)
		{
			alertify.alert("Assessment Name has special characters.");
			// document.getElementById("compname").focus();
			return false;
		}
	}

	if (reWhiteSpace.test(topic)|| topic.length==0 || topic.value=='')
	{
		alertify.alert("Please enter an assessment name");
		return false;
	}

	if (reWhiteSpace.test(description)|| description.length==0 || description.value=='')
	{
		alertify.alert("Description should not be blank");
		return false;
	}
	var compid = document.getElementById('compid').value;
		var generate_code = $('#code_name').val();
		if(generate_code ==''){
			var code ='A'.concat(compid).concat(Math.floor(Math.random() * 101)).concat(item_id);	   
			$('#code_name').val(code);	
		}

	var arr = document.getElementsByName("checkbox2[]");
	var arr_active=0;
	var existuser=0;


	if(assessid!='')
	{
	document.f1.submit();
	}
	
}
function submit_assessment(event,item_id)
	{
		 
		var baseurl=document.f1.baseurl.value;
		var topic=document.getElementById('assessname').value;
		var scoretype=document.f1.scoretype.value;
		var statusfield=document.f1.statusfield.value;
		var randques=document.f1.randomques;
		var assessid=document.f1.assessid.value;
		var description=tinyMCE.get('instructions').getContent();
		var creditvalue=document.f1.creditvalue.value;	
		var assess_closedate=$("#assess_closedate").val();		
		var crdate=document.f1.crdate.value;	
		var template_id	=$("#certificate_template_id").val();
		
		if($('#assessmentyes').is(':checked')) $('#assessmentyes').val(0); 
		if($('#assessmenttran').is(':checked')) $('#assessmenttran').val(0); 
		if($('#hidessessonreqm').is(':checked')) $('#hidessessonreqm').val(0); 
		
		if(template_id=='')template_id=0;
		var skillcategoryid = $('#skillcategoryid').val();
		if(skillcategoryid == ''){
			
			alertify.alert('Please select a traininig category.');
			return false;
		}
		var compid = document.getElementById('compid').value;
		var generate_code = $('#code_name').val();
		if(generate_code ==''){
			var code ='A'.concat(compid).concat(Math.floor(Math.random() * 101)).concat(item_id);	   
			$('#code_name').val(code);
		}
		
		var randomquestion=0;
		if(randques.checked==true){
			randomquestion=1;
			$("#randomize_ques").val(randomquestion);
		}
		else{
			randomquestion=0;
			$("#randomize_ques").val(randomquestion);
		}

		var regexNum = /\d/;

		if(creditvalue=='')
		{
			alertify.alert("Please Enter Credit Value.");
			return false;
		}
		if (!regexNum.test(creditvalue))
		{
			alertify.alert( "Please Enter Credit Value in Digits." );
			return false;
		}

		$assessid=assessid;
		SITEROOT=baseurl;
		var e = event.keyCode;
		if(e == 9)
		{
			return false;
		}
		// var iChars = "!@#$%^()+=[]\\\';,/{}|\":<>?";
		 var reWhiteSpace = new RegExp(/^\s+$/);
		// for (var i = 0; i < topic.length; i++)
		// {
			// if (iChars.indexOf(topic.charAt(i)) != -1)
			// {
				// alert ("Assessment Name has special characters.");
				// return false;
			// }
		// }

		if (reWhiteSpace.test(topic)|| topic.length==0 || topic.value=='')
		{
			alertify.alert("Please enter an assessment name.");
			return false;
		}
		if(assess_closedate=='')
		{
			alertify.alert('Please enter a close date.');
			return false;
		}
		var k =parseInt($('#maintdskils').children().length);
		if(k!=0){
		var arr=parseInt($("#skillidno").val())+1;		
		var arr_active=0;
		var skilldiv ='';	
		var stralert='';	
		for(i=1;i<arr;i++)
		{			
		var skillid=$("#skillid"+i).val();
		var levelid=$("#levelid"+i).val();
		var credit=$("#credit"+i).val();
		var maxcredit=$("#maxcredit"+i).val();

		if(skillid=='Pick a Skill')
			{
				
				 var errorskill='Skill is blank.\n'; 
				 stralert+= errorskill;
				 $('.errorskill'+i).html(errorskill);
				
			}else{
				$('.errorskill'+i).html('');
			}
			
			if(levelid=='Level')
			{
				var errorlevel='Skill level is blank. \n'; 
				stralert+= errorlevel;
				$('.errorlevel'+i).html(errorlevel);
				
			}else{
				$('.errorlevel'+i).html('');
			}
			
			if(!credit)
			{
				 var errorcredit ='Course credit is blank.\n'; 
				 stralert+= errorcredit;
				 $('.errorcredit'+i).html(errorcredit);
				
			}else{
				$('.errorcredit'+i).html('');
			}
			
			if(!maxcredit)
			{
				 var errormaxcredit ='Max credit is blank.\n'; 
				 stralert+= errormaxcredit;
				 $('.errormaxcredit'+i).html(errorcredit);
				
			}else{
				$('.errormaxcredit'+i).html('');
			}
			
			 if(!stralert){
			   var data = skillid+'#$$'+levelid+'#$$'+credit+'#$$'+maxcredit;
			   skilldiv += data+'!@';	
			 }			 

		}

		if(stralert){
		alertify.alert("Some errors in skills.");
		return false;
		}else
		{
		$('#skill_level_credit').val(skilldiv); 
		}
		}
		if(assessid=='')
		{			
			document.f1.submit();
		}
	}
	function update_assessment(asess_id)
	{
		
		var baseurl=document.f1.baseurl.value;
		var topic=document.getElementById('assessname').value;
		var scoretype=document.f1.scoretype.value;
		var statusfield=document.f1.statusfield.value;
		var randques=document.f1.randomques;
		var assessid=document.f1.assessid.value;
		var description=tinyMCE.get('instructions').getContent();
		var creditvalue=document.f1.creditvalue.value;	
		var assess_closedate=$("#assess_closedate").val();		
		var crdate=document.f1.crdate.value;	
		var template_id	=$("#certificate_template_id").val();
		if(template_id=='')template_id=0;
		if($('#assessmentyes').is(':checked')) $('#assessmentyes').val(0); 
		if($('#assessmenttran').is(':checked')) $('#assessmenttran').val(0); 
		if($('#hidessessonreqm').is(':checked')) $('#hidessessonreqm').val(0); 
		var skillcategoryid = $('#skillcategoryid').val();
		if(skillcategoryid == ''){
			
			alertify.alert('Please select a traininig category');
			return false;
		}
		
		var randomquestion=0;
		if(randques.checked==true){
			randomquestion=1;
			$("#randomize_ques").val(randomquestion);
		}
		else{
			randomquestion=0;
			$("#randomize_ques").val(randomquestion);
		}

		var regexNum = /\d/;

		if(creditvalue=='')
		{
			alertify.alert("Please Enter Credit Value.");
			return false;
		}
		if (!regexNum.test(creditvalue))
		{
			alertify.alert( "Please Enter Credit Value in Digits." );
			return false;
		}

		$assessid=assessid;
		SITEROOT=baseurl;
		
		// var iChars = "!@#$%^()+=[]\\\';,/{}|\":<>?";
		var reWhiteSpace = new RegExp(/^\s+$/);
		// for (var i = 0; i < topic.length; i++)
		// {
			// if (iChars.indexOf(topic.charAt(i)) != -1)
			// {
				// alert ("Assessment Name has special characters.");
				// return false;
			// }
		// }

		if (reWhiteSpace.test(topic)|| topic.length==0 || topic.value=='')
		{
			alertify.alert("Please enter an assessment name");
			return false;
		}
		if(assess_closedate=='')
		{
			alertify.alert('Please enter a close date.');
			return false;
		}
		var k =parseInt($('#maintdskils').children().length);
		if(k!=0){
		var arr=parseInt($("#skillidno").val())+1;				
		var arr_active=0;
		var skilldiv ='';	
		var stralert='';	
		for(i=1;i<arr;i++)
		{			
		var isupdate=$("#trskill"+i).data('isupdate');
		if(!isupdate)
		{
			var skillid=$("#skillid"+i).val();
			var levelid=$("#levelid"+i).val();
			var credit=$("#credit"+i).val();
			var maxcredit=$("#maxcredit"+i).val();

			if(skillid=='Pick a Skill')
			{
				
				 var errorskill='Skill is blank.\n'; 
				 stralert+= errorskill;
				 $('.errorskill'+i).html(errorskill);
				
			}else{
				$('.errorskill'+i).html('');
			}

			if(levelid=='Level')
			{
				var errorlevel='Skill level is blank. \n'; 
				stralert+= errorlevel;
				$('.errorlevel'+i).html(errorlevel);
				
			}else{
				$('.errorlevel'+i).html('');
			}

			if(!credit)
			{
				 var errorcredit ='Course credit is blank.\n'; 
				 stralert+= errorcredit;
				 $('.errorcredit'+i).html(errorcredit);
				
			}else{
				$('.errorcredit'+i).html('');
			}

			if(!maxcredit)
			{
				 var errormaxcredit ='Max credit is blank.\n'; 
				 stralert+= errormaxcredit;
				 $('.errormaxcredit'+i).html(errormaxcredit);
				
			}else{
				$('.errormaxcredit'+i).html('');
			}

			if(!stralert){
			var data = skillid+'#$$'+levelid+'#$$'+credit+'#$$'+maxcredit;
			skilldiv += data+'!@';	
			}			 
		}
		}

		if(stralert){
		alertify.alert("Some errors in skills.");
		return false;
		}else
		{
		$('#skill_level_credit').val(skilldiv); 
		}
		}
		
		var all_id =mygrid.getAllItemIds();
		 $("#sequence_order_id").val(all_id);		
		document.f1.submit();
		
	}
	function reset_assessment(asess_id)
	{
	var baseurl=document.f1.baseurl.value;
	var assessid=asess_id;
	$.post(baseurl+"/assessment/resetattempttaken" ,{assessid : assessid}, function(response){
			$("#noofattempt").val('0');
	});
	}
	
	function submitassessment(event,item_id)
	{
		var baseurl=document.f1.baseurl.value;
		var topic=document.getElementById('assessname').value;
		var scoretype=document.f1.scoretype.value;
		var statusfield=document.f1.statusfield.value;
		var randques=document.f1.randomques;
		var assessid=document.f1.assessid.value;
		var description=tinyMCE.get('instructions').getContent();
		var creditvalue=document.f1.creditvalue.value;
		// var assess_duedate=document.f1.assess_duedate.value;
		var assess_duedate='';
		var assess_closedate=document.f1.assess_closedate.value;
		var clsedate=document.f1.clsedate.value;
		var crdate=document.f1.crdate.value;
		
		var parts = assess_closedate.split("/");
		var year=parts[2];
		var month=parts[0];
		var day=parts[1];
		var date_close=year+month+day;
		
		// var parts_clsdate = assess_duedate.split("/");
		// var year_clsdate=parts_clsdate[2];
		// var month_clsdate=parts_clsdate[0];
		// var day_clsdate=parts_clsdate[1];
		//var date_due=year_clsdate+month_clsdate+day_clsdate;
		var skillcategoryid = $('#skillcategoryid').val();
		if(skillcategoryid == ''){
		
			alertify.alert('Please select a training category.');
			return false;
		}
		var compid = document.getElementById('compid').value;
		var generate_code = $('#code_name').val();
		if(generate_code ==''){
			var code ='A'.concat(compid).concat(Math.floor(Math.random() * 101)).concat(item_id);	   
			$('#code_name').val(code);
		}
		var randomquestion=0;
		if(randques.checked==true){
			randomquestion=1;
			document.getElementById("randomize_ques").value=1;
		}
		else{
			randomquestion=0;
			document.getElementById("randomize_ques").value=0;
		}

		var regexNum = /\d/;

		if(creditvalue=='')
		{
			alertify.alert("Please Enter Credit Value.");
			return false;
		}
		if (!regexNum.test(creditvalue))
		{
			alertify.alert( "Please enter credit value in digits." );
			return false;
		}

		$assessid=assessid;
		SITEROOT=baseurl;
		var e = event.keyCode;
		if(e == 9)
		{
			return false;
		}
		var iChars = "!@#$%^()+=[]\\\';,/{}|\":<>?";
		var reWhiteSpace = new RegExp(/^\s+$/);
		for (var i = 0; i < topic.length; i++)
		{
			if (iChars.indexOf(topic.charAt(i)) != -1)
			{
				alertify.alert("The assessment name must not contain special characters.");
				return false;
			}
		}

		if (reWhiteSpace.test(topic)|| topic.length==0 || topic.value=='')
		{
			alertify.alert("The assessment name should not be blank.");
			return false;
		}

		// if(assess_duedate=='')
		// {
			// alert('Please Enter Due Date.');
			// return false;
		// }
		
		if(assess_closedate=='')
		{
			alertify.alert('Please enter a close date.');
			return false;
		}
		var addnewarray=new Array();
		var addnewtextdiv=document.getElementById("addnew");
		var addnewparam = addnewtextdiv.getElementsByTagName("*");

		for(var ic=0;ic<addnewparam.length;ic++){
			addnewarray=addnewparam[ic].value;
		}
		var arr = document.getElementsByName("checkbox2[]");
		var arr_active=0;
		var existuser=0;

		if(assessid!='')
		{
			var adddataassess='adddataassess';
			document.f1.submit();
		}
		if(assessid=='')
		{
			assessid='';
			var closeddate=0;
			var opendate=0;
			var closedate=0;
			var selectedtaboption=0;
			document.f1.submit();
			
			assessid=0;
			var randomval=document.f1.randomval.value;
			var url=SITEROOT+'/assessment/addassessment/topic/'+topic+'/description/'+description+'/scoretype/'+scoretype+'/statusfield/'+statusfield
			+'/mode'+'/datawithtempassessid'+'/randomval/'+randomval+'/assessid/'+assessid+'/duedate/'+address2+'/creditvalue/'+creditvalue+'/nonecredit/'
			+nonecredit+'/onday/'+onday+'/onweek/'+onweek+'/selectedtaboption/'+selectedtaboption+'/yearlymonth/'+yearlymonth+'/opendate/'+opendate+'/closedate/'+closedate
			+'/closeddate/'+closeddate+'/endingtype/'+endingtype+'/assess_duedate/'+assess_duedate+'/assess_closedate/'+assess_closedate;
				
			createXMLHttpRequest();
			var strURL=url;
			var query ="";

			if (xmlHttpRequest != null) {
				xmlHttpRequest.open("post", strURL, true);
				xmlHttpRequest.onreadystatechange = handlepromevalues ;
				xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlHttpRequest.send(query);

			}
		}
	}

function handlepromevalues()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//alert('xmlDoc');
			//return false;
			if(xmlDoc=='success')
			{
				alertify.alert("Data Updated Successfully.");
			}
			var pos=xmlDoc.indexOf('Added');

			if(pos==0)
			{
				alertify.alert("Data Added Successfully.");
				$('#submitassessementhide').hide();
				$('#multichoiceadd').hide();
				$('#linkertchoiceadd').hide();
				$('#truefalseadd').hide();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#questionbar1').show();
				//    $('#minusimage_'+$questionno).show();
				//        $('#plusimage_'+$questionno).hide();
				$('#chooseval').hide();
				$('#upload').hide();
				$('#upload_content').show();
				//				$('#lableaddques').hide();
				//				$('#questiondiv').hide();
				var spl1=xmlDoc.split('|');
				var assess_id=spl1[1];
				var user_id=spl1[2];
				$('#previewcopy').show();
				document.getElementById("assess_id").value=assess_id;

			}

		//  var url=SITEROOT+'/prometheus/promestore';
		//window.location=url;
		}
	}
}
function notallowedtoupload()
{
//alert('Please ');
}
function submitbyuserassessment(event)
{
	//alert(event);
	//return false;
	var e = event.keyCode;
	if(e == 9)
	{
		return false;
	}
	var linkertvalueinedit=0;
	var trueval=0;
	var yesradio1=0
	var descriptive=0;
	var multichoicecorrectineditradio=0
	var mseloptinedit=0;
	var totalquestion=document.getElementById("toalquestionasked").value;
	var questiontypeans=document.getElementById("questiontypeans").value;
	var questiontypeansids=document.getElementById("questiontypeansids").value;
	var multiquestionansvids=document.getElementById("multiquestionansv").value;
	
	var splitquestiontypes=questiontypeans.split(",");
	var splitquestiontypesids=questiontypeansids.split(",");
	var mulidsdis=multiquestionansvids.split(",");
	var mulop=0;
	var startcount=0;
	var loopstr=splitquestiontypes.length-1;
	for(qt=0;qt<=loopstr;qt++)
	{
		if(splitquestiontypes[qt]!="")
		{
			if(splitquestiontypes[qt]==1)
			{
				var fieldname="multichoicecorrectineditradio_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==2)
			{
				var fieldname="linkertvalueinedit_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==3)
			{
				var fieldname="trueval_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==4)
			{
				var fieldname="yesradio1_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==5)
			{
				var fieldname="descriptive_"+splitquestiontypesids[qt];
				//var group = document.getElementsByName(fieldname);
				if(document.getElementById(fieldname).value!="")
				{
					startcount++;
					
					//break;
				}
				
			}
			if(splitquestiontypes[qt]==6)
			{
				var totalmultpleoptionid=mulidsdis[mulop];
				var splitoptionmul=totalmultpleoptionid.split("_");
				var mulsel=0;
				for(mt=0;mt<splitoptionmul.length;mt++)
				{
					var fieldname="mseloptinedit_"+splitquestiontypesids[qt]+"_"+splitoptionmul[mt];
					var group = document.getElementsByName(fieldname);
					for (var i=0; i<group.length; i++) 
					{
						if (group[i].checked)
						{
							mulsel++;
							break;
						}
					}
					if(mulsel!=0)
					{
						startcount++;
						break;
					}
				}
				mulop++;
			}
		}
	}
	var multiSelArr = new Array();
	$('input[type="checkbox"]:checked').each(function() {
	   multiSelArr.push(this.value);
	});
	
	
	var multiSelJsonString = JSON.stringify(multiSelArr);
	$("#multiSelJsonString").val(multiSelJsonString);
	
	// alert(myJsonString);
	// return false;
	var baseurl=document.f1.baseurl.value;
	var userid=document.f1.userid.value;
	var assessid=document.f1.assessid.value;

	var confirmedradio=document.getElementById('confirmedradio');
	var choosen=1;

	SITEROOT=baseurl;
	//alert(startcount+"!="+totalquestion+"======"+document.getElementById('score_id').value);
	//return false;
	if(assessid!='')
	{
		var scoreid=document.getElementById('score_id').value;
/*		if(scoreid==4)
		{
			document.f1.submit();
		}
		else*/ if(startcount!=totalquestion)
		{
			// if(confirm('You have not responded to all questions.  Are you sure you want to submit your results?')==true)
			// {
				// document.f1.submit();
			// }
			// else
			// {
				// return false;	
			// }
			alertify.confirm("You have not responded to all questions.  Are you sure you want to submit your results?", function (e) {
						if (e) {
							document.f1.submit();
						} 
					});
		}
		else
		{
			document.f1.submit();	
		}
		/*var adddataassess='adddataassess';
		for(var i=0; i < document.f1.confirmedradio.length; i++)
		{
			if(document.f1.confirmedradio[i].checked)
				choosen=1;
		}

		if(choosen==0 && confirmedradio.value!='insurvey')
		{
			alert('Have you completed the Assessment?');
			return false;
		}
		else
			document.f1.submit();*/
	}



}
function handlesubmitbyuserassessment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			if(xmlDoc=='success')
			{
				alertify.alert("Data Updated Successfully.");
			}
			if(xmlDoc=='Added')
			{
				
				alertify.alert("Data Added Successfully.");

			}

		//  var url=SITEROOT+'/prometheus/promestore';
		//window.location=url;
		}
	}
}
function expiredassessment(duedate)
{
	
	alertify.alert('Failed to Submit this Assessment,Close date has passed, \n Close date was " '+duedate+'". Please contact your administrator');
	return false;
}

var $questionno;
var $operationforshowandhide;
function showfullanswerdiv(questionno,operation)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;

	//$('#fullanswer_'+questionno).addClass('shankhan');
	//$('.shankhan').hide();
/*	var hidedivid = $('.shankhan').hide().attr('id');
    var mytool_array=hidedivid.split("_");
    var finalid = mytool_array[1];
	$('#plusimage_'+finalid).show();
	$('#minusimage_'+finalid).hide();*/
	
	
	if(operation=='showfullanswer') //haider ans edit mode
	{
	$('#editquestionname_'+questionno).removeClass('bg_input');
	$('#editquestionname_'+questionno).attr('disabled', false); 
	}
	 else
	{
	 $('#editquestionname_'+questionno).addClass('bg_input');	
	 $('#editquestionname_'+questionno).attr('disabled', true);
	}
	//end
	SITEROOT=baseurl;
	$operationforshowandhide=operation;
	$questionno=questionno;

	var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/questionid/'+questionno+'/assessid/'+assessid;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";

	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleshowfullanswerdiv;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}
}
function handleshowfullanswerdiv()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			if($operationforshowandhide=='showfullanswer'){
				$('#fullanswer_'+$questionno).show();
				$('#minusimage_'+$questionno).show();
				$('#plusimage_'+$questionno).hide();
				var typeval=$("#Items").val();
				if (typeval==4)
				{
					$('div[id^="viewques_chkbx"]').hide();
					$('div[id^="viewquesmltichice"]').hide();
					$('input[name^="linkertvalueinedit_"]').hide();
				}
				else if (typeval==5)
				{
					$('div[id^="viewques_chkbx"]').show();
					$('div[id^="viewquesmltichice"]').show();
				}
				
			}
			if($operationforshowandhide=='hidefullanswer'){
				$('#fullanswer_'+$questionno).hide();
				$('#minusimage_'+$questionno).hide();
				$('#plusimage_'+$questionno).show();
			}
		}
	}
}
function showfullanswerdivofaddedquestion(questionno)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;
	var assess_id=document.f1.assess_id.value;
	var randomval=document.f1.randomval.value;
	if(assess_id!=''){
		assessid=assess_id;
	}


	SITEROOT=baseurl;
	$operationforshowandhide='showfullanswer';
	var operation='showfullanswer';
	$questionno=questionno;
	if(randomval!='')
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/questionid/'+questionno+'/randomval/'+randomval;
	if(assessid!='')
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/questionid/'+questionno+'/assessid/'+assessid;

	createXMLHttpRequest();
	var strURL=url;
	var query ="";

	if (xmlHttpRequest != null) {
		$('#editquestionname_'+questionno).attr('disabled', false); //haider
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleshowfullanswerdivofaddedquestion;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}
}
function handleshowfullanswerdivofaddedquestion()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			$('#fullanswer_'+$questionno).show();
			$('#minusimage_'+$questionno).show();
			$('#plusimage_'+$questionno).hide();
		}
	}
}
function showfullanswerdiv1(questionno,operation)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;
	SITEROOT=baseurl;

	$questionno=questionno;
	$operationforshowandhide=operation;
	var url=SITEROOT+'/assessment/previewassessment/mode/'+operation+'/questionid/'+questionno+'/assessid/'+assessid;

	createXMLHttpRequest();
	var strURL=url;
	var query ="";

	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleshowfullanswerdiv1;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}
}
function handleshowfullanswerdiv1()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			if($operationforshowandhide=='showfullanswer'){
				$('#fullanswer1_'+$questionno).show();
				$('#minusimage1_'+$questionno).show();
				$('#plusimage1_'+$questionno).hide();

			}
			if($operationforshowandhide=='hidefullanswer'){
				$('#fullanswer1_'+$questionno).hide();
				$('#minusimage1_'+$questionno).hide();
				$('#plusimage1_'+$questionno).show();

			}

		}
	}
}
function hidefullanswerdiv(questionno)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;
	var assess_id=document.f1.assess_id.value;
	if(assess_id!='') {
		assessid=assess_id;
	}
	SITEROOT=baseurl;

	$questionno=questionno;

	var url=SITEROOT+'/assessment/addassessment/mode/'+'hidefulldiv'+'/questionid/'+questionno+'/assessid/'+assessid;

	createXMLHttpRequest();
	var strURL=url;
	var query ="";

	if (xmlHttpRequest != null) {
		$('#editquestionname_'+questionno).attr('disabled', true);      //haider add new
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlehidefullanswerdiv;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}
}
function handlehidefullanswerdiv()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			$('#fullanswer_'+$questionno).hide();
			$('#minusimage_'+$questionno).hide();
			$('#plusimage_'+$questionno).show();


		}
	}
}
function deleteassessmentques(questionid,assessid)
{
	var confirmmess=confirm('Are you sure you want to delete?');
	if(confirmmess==true)
	{
		var baseurl=document.f1.baseurl.value;
		var assess_id=document.f1.assess_id.value;
		if(assess_id!=''){
			assessid=assess_id;
		}
		SITEROOT=baseurl;
		$assessid=assessid;
		$questionno=questionid;
		var operation='deletefromassessmentques';
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/assessid/'+assessid+'/questionid/'+questionid;
	
		createXMLHttpRequest();
		var strURL=url;
		var query ="";
	
		if (xmlHttpRequest != null) {
			xmlHttpRequest.open("post", strURL, true);
			xmlHttpRequest.onreadystatechange = handledeleteassessmentques;
			xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlHttpRequest.send(query);
	
		}
	}
		
}
function handledeleteassessmentques()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			$('#fullanswer_'+$questionno).hide();
			$('#questionbar_'+$questionno).hide();


		}
	}
}
function disablecreditvalue(creditvalue)
{
	var nonecredit=document.f1.nonecredit;
	if(nonecredit.checked==true)
	{
		document.getElementById('creditvalue').value='';
		document.getElementById('creditvalue').readOnly=true;
	}
	if(nonecredit.checked==false)
	{
		document.getElementById('creditvalue').value=creditvalue;
		document.getElementById('creditvalue').readOnly=false;
	}
}
var $assessid;
function checkedotherfalse(id)
{
	if(id==1)
	{
		var val=1;
		document.f1.radioanswer2.checked=false;
		document.f1.radioanswer3.checked=false;
		document.f1.radioanswer4.checked=false;
		document.f1.nocorrectans.checked=false;

	}
	if(id==2)
	{
		var val=1;
		document.f1.radioanswer1.checked=false;
		document.f1.radioanswer3.checked=false;
		document.f1.radioanswer4.checked=false;
		document.f1.nocorrectans.checked=false;
	}
	if(id==3)
	{
		var val=1;
		document.f1.radioanswer2.checked=false;
		document.f1.radioanswer1.checked=false;
		document.f1.radioanswer4.checked=false;
		document.f1.nocorrectans.checked=false;
	}
	if(id==4)
	{
		var val=1;
		document.f1.radioanswer2.checked=false;
		document.f1.radioanswer3.checked=false;
		document.f1.radioanswer1.checked=false;
		document.f1.nocorrectans.checked=false;
	}

	if(id==7){
		var radioanswerchecked=document.f1.radioanswer1;
		for(var i=0;i<radioanswerchecked.length;i++)
		{
			if(radioanswerchecked[i].checked==true)
				radioanswerchecked[i].checked=false;

		}
	}


}
function checkedotherfalseineditmode(id,questionid)
{
	if(id==1)
	{
		var addnewtextdiv=document.getElementById("dataofmultichoicediv_"+questionid);
		var addnewparam = addnewtextdiv.getElementsByTagName("input");
		for(var ic=0;ic<addnewparam.length;ic++){
			var a=addnewparam[ic];
			//	if(a.type=='text')
			//		a.value='';
			if(a.type=='radio' && a.name=='multichoicecorrectineditradio_'+questionid)
				a.checked=false;

		}
		document.getElementById("nocorrectanswer_"+questionid).checked=false;
	}
	if(id==2)
	{

		//var othermultichoicebuttons=document.f1.'multichoicecorrectineditradio_'+questionid;
		var addnewtextdiv=document.getElementById("dataofmultichoicediv_"+questionid);
		var addnewparam = addnewtextdiv.getElementsByTagName("input");
		for(var ic=0;ic<addnewparam.length;ic++){
			var a=addnewparam[ic];
			//	if(a.type=='text')
			//		a.value='';
			if(a.type=='radio' && a.name=='radiobuttoninquespanel_'+questionid)
				a.checked=false;
		}
		document.getElementById('nocorrectanswer_'+questionid).checked=false;
	}
	if(id==3)
	{
		var addnewtextdiv=document.getElementById("dataofmultichoicediv_"+questionid);
		var addnewparam = addnewtextdiv.getElementsByTagName("input");
		for(var ic=0;ic<addnewparam.length;ic++){
			var a=addnewparam[ic];
			//	if(a.type=='text')
			//		a.value='';
			if(a.type=='radio' && a.name=='multichoicecorrectineditradio_'+questionid)
				a.checked=false;

		}
	//		for(var ic=0;ic<addnewparam.length;ic++){
	//			var a=addnewparam[ic];
	//			//	if(a.type=='text')
	//			//		a.value='';
	//			if(a.type=='radio' )
	//				a.checked=false;
	//		}

	}
	if(id=='6')
	{

		var quesansw=questionid.split('||');

		var quesid=quesansw[0];

		var answerid=quesansw[1];
		var mselchckbox=document.getElementById('mseloptinedit_'+quesid+'_'+answerid);
		if(mselchckbox.checked==false){
			var chckvalue=mselchckbox.value;
			//document.getElementById('uncheckidsmsel').value=
			var uncheckid=document.getElementById('uncheckidsmsel').value;
			document.getElementById('uncheckidsmsel').value=uncheckid+","+chckvalue;
		}
		if(mselchckbox.checked==true){
			a=document.getElementById("uncheckidsmsel").value;

			if (a.indexOf(","+quesid+"||"+answerid) != -1){
				var b=a.replace(","+quesid+"||"+answerid,"");
				b=b.replace(/^\s+|\s+$/g,"");
				document.getElementById("uncheckidsmsel").value=b;
			}
		}
	}
//	if(id=='linkertvalueinedit2')
//	{
//		document.getElementById('linkertvalueinedit1_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit3_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit4_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit5_'+questionid).checked=false;;
//	}
//	if(id=='linkertvalueinedit3')
//	{
//		document.getElementById('linkertvalueinedit2_'+questionid).checked=false;
//
//		document.getElementById('linkertvalueinedit1_'+questionid).checked=false;
//
//		document.getElementById('linkertvalueinedit4_'+questionid).checked=false;
//				document.getElementById('linkertvalueinedit5_'+questionid).checked=false;;
//	}
//	if(id=='linkertvalueinedit4')
//	{
//		document.getElementById('linkertvalueinedit2_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit3_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit1_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit5_'+questionid).checked=false;;
//	}
//	if(id=='linkertvalueinedit5')
//	{
//		document.getElementById('linkertvalueinedit2_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit3_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit4_'+questionid).checked=false;
//		document.getElementById('linkertvalueinedit1_'+questionid).checked=false;;
//	}
}


function addquestionsyesno(operation,ques_id)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=$("#assessid").val();
	var spinnerval=$("#red").val();
	var ques_name=tinyMCE.get('quesname').getContent();
	ques_name=$.trim(ques_name);
	var quesname=ques_name;
	quesname = quesname.replace(/&nbsp;/g, " ");
	quesname=quesname.replace("&","andsign");
	quesname=quesname.replace("(","smlBracOpn");
	quesname=quesname.replace(")","smlBracCls");
	quesname=quesname.replace("[","bigBracOpn");
	quesname=quesname.replace("]","bigBracCls");
	quesname=quesname.replace(/#/g,"hash");
	quesname=quesname.replace("$","dollar");
	//quesname=quesname.replace("*","star");
	
	quesname=quesname.replace("?","quesmarkss");
	quesname=quesname.replace(new RegExp("\\+", "g"),"plusmarkss");
	quesname=quesname.replace(new RegExp("\\%", "g"),"permarkss");
	quesname=quesname.replace(/\//gi,"bckslsh");
	
	quesname = quesname.replace(/'/g, "singleqoute");
	
	
	SITEROOT=baseurl;
	var regexNum = /\d/;	
	var reWhiteSpace = new RegExp(/^\s+$/);

	if (reWhiteSpace.test(quesname)|| quesname.length==0 || quesname.value=='')
	{
		alertify.alert("Question cannot not be blank");
		return false;
	}
	var check =0;
	if($('#yesradio').is(':checked')) check =$("#yesradio").val();
	if($('#noradio').is(':checked')) check =$("#noradio").val();
	var Items=$("#Items").val();
	if(!$('#yesradio').is(':checked') && !$('#noradio').is(':checked') && Items==5)
	{
		alertify.alert("Please Select a Correct Answer");
		return false;
	}
	
	if(assessid!='' && operation=='yesnoadd' && ques_id!=0)
	{
		var url=SITEROOT+'/assessment/updatenewquestions';
		$.post(url , {mode : operation , assessid : assessid , yesnovalue : check , spinnerval : spinnerval , quesname : quesname , ques_id : ques_id} , function(response) 
		{
			reload_page(response);
		});
	}
	else
	{
		$assessid='existingid';		
		var url=SITEROOT+'/assessment/addnewquestions';
		$.post(url , {mode : operation , assessid : assessid , yesnovalue : check , spinnerval : spinnerval , quesname : quesname} , function(response)  
		{
			reload_page(response);
		});
	}
}

function addtruefalse(operation,ques_id)
{
	var baseurl=document.f1.baseurl.value;

	var assessid=$("#assessid").val();
	var spinnerval=$("#red").val();
	var ques_name=tinyMCE.get('quesname').getContent();
	ques_name=$.trim(ques_name);
	var quesname=ques_name;
	quesname = quesname.replace(/&nbsp;/g, " ");
	quesname=quesname.replace("&","andsign");
	quesname=quesname.replace("(","smlBracOpn");
	quesname=quesname.replace(")","smlBracCls");
	quesname=quesname.replace("[","bigBracOpn");
	quesname=quesname.replace("]","bigBracCls");
	quesname=quesname.replace(/#/g,"hash");
	quesname=quesname.replace("$","dollar");
	//quesname=quesname.replace("*","star");
	quesname=quesname.replace("?","quesmarkss");
	quesname=quesname.replace(new RegExp("\\+", "g"),"plusmarkss");
	quesname=quesname.replace(new RegExp("\\%", "g"),"permarkss");
	quesname=quesname.replace(/\//gi,"bckslsh");
	
	quesname = quesname.replace(/'/g, "singleqoute");
	
	
	var tfanswertext=$("#tfanswertext").val();	
	tfanswertext=0;
	
	SITEROOT=baseurl;
	var regexNum = /\d/;
	
	var reWhiteSpace = new RegExp(/^\s+$/);


	if (reWhiteSpace.test(quesname)|| quesname.length==0 || quesname.value=='')
	{
		alertify.alert("Question cannot be  not be blank");
		return false;
	}
	var check =0;
	if($('#trueval').is(':checked')) var check =$("#trueval").val();
	if($('#falseval').is(':checked')) var check =$("#falseval").val();
	var Items=$("#Items").val();
	if(!$('#trueval').is(':checked') && !$('#falseval').is(':checked') && Items==5)
	{
		alertify.alert("Please Select a Correct Answer");
		return false;
	}
	
	if(assessid!='' && operation=='truefalseadd' && ques_id!=0)
	{	
		var url=SITEROOT+'/assessment/updatenewquestions';	
		$.post(url , {mode : operation , assessid : assessid , tfvalue : check , spinnerval : spinnerval ,quesname : quesname ,	tfanswertext : tfanswertext , ques_id : ques_id} , 
		function(response) {
			reload_page(response);
		});
	}
	else
	{		
		var url=SITEROOT+'/assessment/addnewquestions';	
		$.post(url, {mode : operation, assessid : assessid, tfvalue : check, spinnerval : spinnerval, quesname : quesname, tfanswertext : tfanswertext } , 
		function(response) {
			reload_page(response);
		});
	}
}
function addlinkert(operation,ques_id)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=$("#assessid").val();
	var spinnerval=$("#red").val();
	var ques_name=tinyMCE.get('quesname').getContent();
	ques_name.trim();
	var quesname=ques_name;
	quesname = quesname.replace(/&nbsp;/g, " ");
	quesname=quesname.replace("&","andsign");
	quesname=quesname.replace("(","smlBracOpn");
	quesname=quesname.replace(")","smlBracCls");
	quesname=quesname.replace("[","bigBracOpn");
	quesname=quesname.replace("]","bigBracCls");
	quesname=quesname.replace(/#/g,"hash");
	quesname=quesname.replace("$","dollar");
	//quesname=quesname.replace("*","star");	
	quesname=quesname.replace("?","quesmarkss");
	quesname=quesname.replace(new RegExp("\\+", "g"),"plusmarkss");
	quesname=quesname.replace(new RegExp("\\%", "g"),"permarkss");
	quesname=quesname.replace(/\//gi,"bckslsh");

	
	
	SITEROOT=baseurl;
	var regexNum = /\d/;
	
	var iChars = "!@#$^()[]\\\';,/{}|<>";
	var reWhiteSpace = new RegExp(/^\s+$/);
	if (reWhiteSpace.test(quesname)|| quesname.length==0 || quesname.value=='')
	{
		alertify.alert("Question cannot be  not be blank");
		return false;
	}
	var check =0;
	if($('#chooselinkval_33').is(':checked')) check =$("#chooselinkval_33").val();
	if($('#chooselinkva1_1l').is(':checked')) check =$("#chooselinkva1_1l").val();
	if($('#chooselinkval_55').is(':checked')) check =$("#chooselinkval_55").val();
	if($('#chooselinkval_44').is(':checked')) check =$("#chooselinkval_44").val();
	if($('#chooselinkval_22').is(':checked')) check =$("#chooselinkval_22").val();
	if(!$('#chooselinkval_33').is(':checked') && !$('#chooselinkva1_1l').is(':checked') && !$('#chooselinkval_55').is(':checked') && !$('#chooselinkval_44').is(':checked') && !$('#chooselinkval_22').is(':checked'))
	{
		check ='anyone';
	}	
	if(assessid!='' && operation=='linkertadd' && ques_id!=0)
	{	
		var url=SITEROOT+'/assessment/updatenewquestions';	
		
		$.post(url , {mode : operation , assessid : assessid , linkertval : check , spinnerval : spinnerval , quesname : quesname, ques_id : ques_id },  function(response) 
		{
			reload_page(response);
		});
	}
	else
	{		
		var url=SITEROOT+'/assessment/addnewquestions';	
		$.post(url , {mode : operation , assessid : assessid , linkertval : check , spinnerval : spinnerval , quesname : quesname }, function(response) 
		{
			reload_page(response);
		});
	}
}

function addmultichoicedata(operation,ques_id)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=$("#assessid").val();
	var spinnerval=$("#red").val();	
	var nocorrectans=$("#nocorrectans").val();
	var ques_name=tinyMCE.get('quesname').getContent();
	var randomanswersoption=$("#randomanswerss").val();
	 ques_name=$.trim(ques_name);
	var quesname=ques_name;
	quesname = quesname.replace(/&nbsp;/g, " ");
	quesname=quesname.replace(/§/g,"dbl_s");
	quesname=quesname.replace("&","andsign");
	quesname=quesname.replace("(","smlBracOpn");
	quesname=quesname.replace(")","smlBracCls");
	quesname=quesname.replace("[","bigBracOpn");
	quesname=quesname.replace("]","bigBracCls");
	quesname=quesname.replace(/#/g,"hash");
	quesname=quesname.replace("$","dollar");
	quesname=quesname.replace("?","quesmarkss");
	quesname=quesname.replace(new RegExp("\\+", "g"),"plusmarkss");
	quesname=quesname.replace(new RegExp("\\%", "g"),"permarkss");
	quesname=quesname.replace(/\//gi,"bckslsh");
	quesname = quesname.replace(/&/g, "&amp;");
	quesname = quesname.replace(/>/g, "&gt;");
	quesname = quesname.replace(/</g, "&lt;");
	quesname = quesname.replace(/"/g, "&quot;");
	quesname = quesname.replace(/'/g, "singleqoute");
	
	var alltextvalues='hmi|';
	
	var radname='';
	var val=0;
	var addnewtextdiv=document.getElementById("wholediv");
	var addnewparam = addnewtextdiv.getElementsByTagName("input");
	
	var textboxname='hmi|2';
	var check1=0;
	for(var ic=0;ic<addnewparam.length;ic++){
		var a=addnewparam[ic];
		
		if(a.type=='text')
		{
			if(alltextvalues!='hmi|')
			{
				answer4=addnewparam[ic].value;
				answer4 = answer4.replace(/&nbsp;/g, " ");
				answer4=answer4.replace(/§/g,"dbl_s");
				answer4=answer4.replace("&","andsign");
				answer4=answer4.replace("(","smlBracOpn");
				answer4=answer4.replace(")","smlBracCls");
				answer4=answer4.replace("[","bigBracOpn");
				answer4=answer4.replace("]","bigBracCls");
				answer4=answer4.replace(/#/g,"hash");
				answer4=answer4.replace("$","dollar");
				//answer4=answer4.replace("*","star");				
				answer4=answer4.replace("?","quesmarkss");
				answer4=answer4.replace(new RegExp("\\+", "g"),"plusmarkss");
				answer4=answer4.replace(new RegExp("\\%", "g"),"permarkss");
				answer4=answer4.replace(/\//gi,"bckslsh");
				
				alltextvalues=alltextvalues+"vkd"+answer4;
			}else{
				answer4=addnewparam[ic].value;
				answer4=answer4.replace(/§/g,"dbl_s");
				answer4=answer4.replace("&","andsign");
				answer4=answer4.replace("(","smlBracOpn");
				answer4=answer4.replace(")","smlBracCls");
				answer4=answer4.replace("[","bigBracOpn");
				answer4=answer4.replace("]","bigBracCls");
				answer4=answer4.replace(/#/g,"hash");
				answer4=answer4.replace("$","dollar");
			//	answer4=answer4.replace("*","star");			
				answer4=answer4.replace("?","quesmarkss");
				answer4=answer4.replace(new RegExp("\\+", "g"),"plusmarkss");
				answer4=answer4.replace(new RegExp("\\%", "g"),"permarkss");
				answer4=answer4.replace(/\//gi,"bckslsh");
				
				answer4 = answer4.replace(/'/g, "singleqoute");
			
				alltextvalues=answer4;
			}
			if(textboxname!='hmi|2')
				textboxname=textboxname+"vkd"+addnewparam[ic].name;
			else
				textboxname=addnewparam[ic].name;
		}
		if(a.type=='radio' && a.checked==true){
			radname=a.value;
			nocorrectans.checked=false;
			check1=1;
		}
	}
	
	var getItems=document.getElementById("Items").value;
	if(check1==0 && getItems==5)
	{
		alertify.alert('Please Select a Correct Answer');
		return false;
	}
	

	var spl1=textboxname.split('vkd');

	if(alltextvalues.substring(0, 3)=='vkd')
	{
		alertify.alert('Answer Should not be blank.');
		return false;
	}
	for(var f=0;f<spl1.length;f++)
	{
		if(radname==spl1[f])
		{
			var val=document.getElementById(radname).value;
			val=val.replace(/&nbsp;/g, " ");
			val=val.replace("?","quesmarkss");
			val=val.replace(new RegExp("\\+", "g"),"plusmarkss");
			val=val.replace(new RegExp("\\%", "g"),"permarkss");		
			val=val.replace(/§/g,"dbl_s");
			val=val.replace("&","andsign");
			val=val.replace("(","smlBracOpn");
			val=val.replace(")","smlBracCls");
			val=val.replace("[","bigBracOpn");
			val=val.replace("]","bigBracCls");
			val=val.replace(/#/g,"hash");
			val=val.replace("$","dollar");
			//val=val.replace("*","star");
			
			val=val.replace(/\//gi,"bckslsh");
			
			val = val.replace(/'/g, "singleqoute");
			
		}
		if(nocorrectans.checked==true)
			var val=nocorrectans.value;
	}



	SITEROOT=baseurl;
	var regexNum = /\d/;

	var iChars = "!@#$%^()[]\\\';,/{}|<>";
	var reWhiteSpace = new RegExp(/^\s+$/);


	if (reWhiteSpace.test(quesname)|| quesname.length==0 || quesname.value=='')
	{
		alertify.alert("Question cannot be  not be blank");
		return false;
	}
	
	var randomanswersoptval=0;
	if($('#randomanswerss').is(':checked'))randomanswersoptval=1;
	
	var allfetchedspinnervalues=document.f1.allfetchedspinnervalues.value;
	var arr1 = new Array();
	arr1=allfetchedspinnervalues.split(',');
	if(assessid!='' && operation=='multichoiceadd' && ques_id!=0)
	{	
		var url=SITEROOT+'/assessment/updatenewquestions';
		$.post(url , {mode : operation , assessid : assessid , spinnerval : spinnerval , quesname : quesname , alldescriptions : alltextvalues , correctchoice : val , randomanswers : randomanswersoptval , ques_id : ques_id} , 
		function(response) {
			reload_page(response);
		});
	
	}
	else
	{
		var url=SITEROOT+'/assessment/addnewquestions';
		
		$.post(url , {mode : operation , assessid : assessid , spinnerval : spinnerval , quesname : quesname , alldescriptions : alltextvalues , correctchoice : val , randomanswers : randomanswersoptval } , 
		function(response) {
			reload_page(response);
		});
	}	
}

function addmultiselectdata(operation,ques_id)
{
	
	var baseurl=document.f1.baseurl.value;
	var assessid=$("#assessid").val();
	var ques_name=tinyMCE.get('quesname').getContent();
	ques_name=$.trim(ques_name);
	var quesname=ques_name;
	quesname=quesname.replace(/&nbsp;/g, " ");
	quesname=quesname.replace(/§/g,"dbl_s");
	quesname=quesname.replace("&","andsign");
	quesname=quesname.replace("(","smlBracOpn");
	quesname=quesname.replace(")","smlBracCls");
	quesname=quesname.replace("[","bigBracOpn");
	quesname=quesname.replace("]","bigBracCls");
	quesname=quesname.replace(/#/g,"hash");
	quesname=quesname.replace("$","dollar");
	//quesname=quesname.replace("*","star");
	quesname=quesname.replace("?","quesmarkss");
	quesname=quesname.replace(new RegExp("\\+", "g"),"plusmarkss");
	quesname=quesname.replace(new RegExp("\\%", "g"),"permarkss");
	quesname=quesname.replace(/\//gi,"bckslsh");	
	quesname = quesname.replace(/'/g, "singleqoute");
	
	
	
	var randomanswersoption=$("#randomanswers").val();
	
	var randomanswersoptval=0;
	if($('#randomanswerss').is(':checked'))randomanswersoptval=1;

	SITEROOT=baseurl;
	var regexNum = /\d/;
	var reWhiteSpace = new RegExp(/^\s+$/);


	if (reWhiteSpace.test(quesname)|| quesname.length==0 || quesname.value=='')
	{
		alertify.alert("Question cannot be  not be blank");
		return false;
	}
	var alltextvalues='hmi|';
	var correctvalues=0;
	var incorrectvalues='';
	var addnewtextdiv=document.getElementById("choosemultiselect");
	
	var addnewparam = addnewtextdiv.getElementsByTagName("input");
	var textboxname=0;
	var check1=0;
	
	for(var ic=0;ic<addnewparam.length;ic++){
		var a=addnewparam[ic];
		if(a.type=='text')
		{
			
			if(alltextvalues!='hmi|')
			{
				answer4=addnewparam[ic].value;
				answer4=answer4.replace(/&nbsp;/g, " ");
				answer4=answer4.replace(/§/g,"dbl_s");
				answer4=answer4.replace("&","and");
				answer4=answer4.replace("(","smlBracOpn");
				answer4=answer4.replace(")","smlBracCls");
				answer4=answer4.replace("[","bigBracOpn");
				answer4=answer4.replace("]","bigBracCls");
				answer4=answer4.replace(/#/g,"hash");
				answer4=answer4.replace("$","dollar");
			//	answer4=answer4.replace("*","star");	
				answer4=answer4.replace("?","quesmarkss");
				answer4=answer4.replace(new RegExp("\\+", "g"),"plusmarkss");
				answer4=answer4.replace(new RegExp("\\%", "g"),"permarkss");
				answer4=answer4.replace(/\//gi,"bckslsh");
				answer4 = answer4.replace(/&/g, "&amp;");
				answer4 = answer4.replace(/>/g, "&gt;");
				answer4 = answer4.replace(/</g, "&lt;");
				answer4 = answer4.replace(/"/g, "&quot;");
				answer4 = answer4.replace(/'/g, "singleqoute");				
				alltextvalues=alltextvalues+"vkd"+answer4+"||";
			}else{
				answer4=addnewparam[ic].value;
				answer4=answer4.replace(/&nbsp;/g, " ");
				answer4=answer4.replace(/§/g,"dbl_s");
				answer4=answer4.replace("&","and");
				answer4=answer4.replace("(","smlBracOpn");
				answer4=answer4.replace(")","smlBracCls");
				answer4=answer4.replace("[","bigBracOpn");
				answer4=answer4.replace("]","bigBracCls");
				answer4=answer4.replace(/#/g,"hash");
				answer4=answer4.replace("$","dollar");
			//	answer4=answer4.replace("*","star");				
				answer4=answer4.replace("?","quesmarkss");
				answer4=answer4.replace(new RegExp("\\+", "g"),"plusmarkss");
				answer4=answer4.replace(new RegExp("\\%", "g"),"permarkss");
				answer4=answer4.replace(/\//gi,"bckslsh");
				answer4 = answer4.replace(/&/g, "&amp;");
				answer4 = answer4.replace(/>/g, "&gt;");
				answer4 = answer4.replace(/</g, "&lt;");
				answer4 = answer4.replace(/"/g, "&quot;");
				answer4 = answer4.replace(/'/g, "singleqoute");
				alltextvalues=answer4+"||";
			}	
		
		}
		if(a.type=='checkbox' && a.checked==true){
			val=a.value;
			val=val.replace(/&nbsp;/g, " ");
			val=val.replace(/§/g,"dbl_s");
			val=val.replace("&","and");
			val=val.replace("(","smlBracOpn");
			val=val.replace(")","smlBracCls");
			val=val.replace("[","bigBracOpn");
			val=val.replace("]","bigBracCls");
			val=val.replace(/#/g,"hash");
			val=val.replace("$","dollar");
		//	val=val.replace("*","star");
			val=val.replace("?","quesmarkss");
			val=val.replace(new RegExp("\\+", "g"),"plusmarkss");
			val=val.replace(new RegExp("\\%", "g"),"permarkss");
			val=val.replace(/\//gi,"bckslsh");
			val = val.replace(/&/g, "&amp;");
			val = val.replace(/>/g, "&gt;");
			val = val.replace(/</g, "&lt;");
			val = val.replace(/"/g, "&quot;");
			val = val.replace(/'/g, "singleqoute");
			
			//correctvalues=a.value+","+correctvalues;
			correctvalues=val+","+correctvalues;
			
			//alltextvalues=alltextvalues+a.value;
			alltextvalues=alltextvalues+val;
			check1=1;
		}
	}
	
	var getItems=document.getElementById("Items").value;

	if(check1==0 && getItems==5)
	{
		alertify.alert('Please Select a Correct Answer');
		return false;
	}
	if(alltextvalues.substring(0, 2)=='||')
	{
		alertify.alert('Answer Should not be blank.');
		return false;
	}
	
	if(assessid!='' && operation=='multiselectadd' && ques_id!=0)
	{
		$assessid='existingid';

		var url=SITEROOT+'/assessment/updatenewquestions';
		
		$.post(url , {mode : operation , assessid : assessid , quesname : quesname , alldescriptions : alltextvalues , correctchoice : correctvalues , randomanswers : randomanswersoptval , ques_id : ques_id } , 
		function(response) {
			reload_page(response);
		});
	}
	else
	{
		$assessid='existingid';
		var url=SITEROOT+'/assessment/addnewquestions';
		
		$.post(url , {mode : operation , assessid : assessid , quesname : quesname , alldescriptions : alltextvalues , correctchoice : correctvalues , randomanswers : randomanswersoptval} , 
		function(response) {
			reload_page(response);
		});
	}
}

function addtextdata(operation,ques_id)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=$("#assessid").val();
	var spinnerval=$("#red").val();
	var ques_name=tinyMCE.get('quesname').getContent();
	ques_name=$.trim(ques_name);
	var quesname=ques_name;
	quesname=quesname.replace(/&nbsp;/g, " ");
	quesname=quesname.replace("?","quesmarkss");
	quesname=quesname.replace(new RegExp("\\+", "g"),"plusmarkss");
	quesname=quesname.replace(new RegExp("\\%", "g"),"permarkss");
	quesname=quesname.replace(/\//gi,"bckslsh");
	quesname = quesname.replace(/&/g, "&amp;");
	quesname = quesname.replace(/>/g, "&gt;");
	quesname = quesname.replace(/</g, "&lt;");
	quesname = quesname.replace(/"/g, "&quot;");
	quesname = quesname.replace(/'/g, "singleqoute");
	
	SITEROOT=baseurl;
	var regexNum = /\d/;
	
	var reWhiteSpace = new RegExp(/^\s+$/);

	if (reWhiteSpace.test(quesname)|| quesname.length==0 || quesname.value=='')
	{
		alertify.alert("Question cannot be  not be blank");

	}
	if(assessid!='' && operation=='descriptive' && ques_id!=0)
	{
		
		var checkdescriptiveblank=0;
		var descriptive=$('#descriptive').val();
		if(descriptive=='')
		{
			checkdescriptiveblank=1;
		}
		var url=SITEROOT+'/assessment/updatenewquestions';
		
		$.post(url , {mode : operation , assessid : assessid , descriptive : descriptive, spinnerval : spinnerval , quesname : quesname  ,checkdescriptiveblank : checkdescriptiveblank , ques_id : ques_id} , 
		function(response) {
			reload_page(response);
		});
	}
	else
	{
		var checkdescriptiveblank=0;
		var descriptive=$('#descriptive').val();
		if(descriptive=='')
		{
			checkdescriptiveblank=1;
		}
		var url=SITEROOT+'/assessment/addnewquestions';
		
		$.post(url , {mode : operation , assessid : assessid , descriptive : descriptive, spinnerval : spinnerval , quesname : quesname  ,checkdescriptiveblank : checkdescriptiveblank , ques_id : ques_id} , 
		function(response) {
			reload_page(response);
		});
	}
}

var $groupid;
function selectedgroupname(groupid,operation)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;
	SITEROOT=baseurl;
	$groupid=groupid;
	var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedgroupid/'+groupid+'/assessid/'+assessid;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleselectedgroupname;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function deletgroupname(groupid)
{
	if(confirm('Are you want to delete')==true)
	{
		var baseurl=document.f1.baseurl.value;
		var scheid=document.f1.scheid.value;
		var assessid=document.f1.assessid.value;
		//020 6500 7905
		SITEROOT=baseurl;
		$groupid=groupid;
		var operation='delselectedgroup';
		if(assessid!='')
			var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedgroupid/'+groupid
			+'/assessid/'+assessid+'/scheid/'+scheid;
		if(assessid=='')
			var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedgroupid/'+groupid+'/scheid/'+scheid;
			
		createXMLHttpRequest();
		var strURL=url;
		var query ="";
		if (xmlHttpRequest != null) {
			xmlHttpRequest.open("post", strURL, true);
			xmlHttpRequest.onreadystatechange = handleselectedgroupname;
			xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlHttpRequest.send(query);
		}
	}
}
function selectedgroupnameafterchoosing(groupid)
{
	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;
	SITEROOT=baseurl;
	$groupid=groupid;
	var operation='groupselected';
	if(assessid!='')
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedgroupid/'+groupid+'/assessid/'+assessid;
	if(assessid=='')
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedgroupid/'+groupid;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleselectedgroupname;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}


function handleselectedgroupname()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			if(xmlDoc!='delselectedgroup')
			{
//				document.getElementById("selectedgroups").innerHTML=xmlDoc;
//				document.getElementById($groupid).className="bgc04 wp87";

			}
			if(xmlDoc=='delselectedgroup')
			{
		
			$('#delgrouprow_'+$groupid).hide();
			var chnggrp;
		var	a=document.getElementById("myexistgrp").value;
		
		if(a!=''){
			if (a.indexOf(","+$groupid) != -1){
			chnggrp=a;
			 var b=a.replace(","+$groupid,"");
			document.getElementById("myexistgrp").value=b;
			if(b==''){
				$('#userlistheader').hide();
				}
			}
			}
			}
		//           oTable = $('#trainingCateglog').dataTable({
		//            "bRetrieve":true,
		//		"bJQueryUI": true,
		//		"sPaginationType": "full_numbers",
		//		"totaldisplyrows":12,
		//		"tableHeadername":"<span class='dash_mid_td_upcoming'>Training Items</span>"
		////		"headerImage":"icon_manage_users-active.png"
		//	});


		}
	}
}
var allusers;
function checkselectedusers()
{

	var baseurl=document.f1.baseurl.value;
	var assessid=document.f1.assessid.value;
	SITEROOT=baseurl;
	var arr=document.getElementsByName("checksingle[]");
	// var checkedgroupsselected=document.getElementById('checkedgroups').value;
	var arr_active=0;

	for(i=0;i<arr.length;i++)
	{
		if(arr[i].checked==true)
		{
			if(arr_active!=0)
			{
				arr_active = arr_active+","+arr[i].value;
			}
			else
			{
				arr_active=arr[i].value;
			}

		}
	}

	if(arr_active==0)
	{
		alertify.alert('Please Select User');
		return false;

	}else{
		if(assessid!='')
			var url=SITEROOT+'/assessment/addassessment/checkedusers/'+arr_active+'/assessid/'+assessid+'/mode/'+'selectedusers';
		if(assessid=='')
			var url=SITEROOT+'/assessment/addassessment/checkedusers/'+arr_active+'/mode/'+'selectedusers';
		;
		
		allusers=arr_active;
		createXMLHttpRequest();
		var strURL=url;
		var query="";
		if (xmlHttpRequest != null) {
			// alert(query);
			xmlHttpRequest.open("post", strURL, true);
			xmlHttpRequest.onreadystatechange = handleaddusertoselectedgroup ;
			xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlHttpRequest.send(query);

		}   //window.location.replace(strURL);

	}

}
function handleaddusertoselectedgroup(){
	//alert('1');
	if (xmlHttpRequest.readyState == 4) {
		//alert('2');
		if (xmlHttpRequest.status == 200) {
			//  alert('3');

			var xmlDoc=xmlHttpRequest.responseText;
			//document.getElementById("myselectedusers").innerHTML=xmlDoc;
			//			$('#maintablehedader').hide();
			//
			//			document.getElementById("maintablehedader").style.display = 'none';
			var new_div1 = window.document.createElement('div');
			new_div1.innerHTML = xmlDoc;
			document.getElementById("myselectedusers").appendChild(new_div1,xmlDoc);
 //document.getElementById("savedusers").value=document.f1.usertosave.value;
 //document.getElementById("savedgroups").value=document.f1.grouptosave.value;

			tb_remove();
		//    var SITEROOT='<?php echo $baseUrl ?>';
		//     var url=SITEROOT+'/admin/managegroups'
		//  window.location=url;
		}
	}
}
var $fileid;
function deluploadedfile(fileid,operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	if(operation=='deluploadedfile'){
		$fileid=fileid;
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/fileid/'+fileid;
	}
	if(operation=='addnewtext')
	{
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation;
	}
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handledeluploadedfile;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function deluploadedfilefromfrontend(fileid)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var operation='deluploadedfile';
	$fileid=fileid;
	
	alertify.confirm("Are you sure?", function (e) {
						if (e) {
								var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/fileid/'+fileid;
								createXMLHttpRequest();
								var strURL=url;
								var query ="";
								if (xmlHttpRequest != null) {
									xmlHttpRequest.open("post", strURL, true);
									xmlHttpRequest.onreadystatechange = handledeluploadedfile;
									xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
									xmlHttpRequest.send(query);
								}
						} 
					});
}
function handledeluploadedfile()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//			alert(xmlDoc);
			//			if(xmlDoc=='deluploadedfile')
			//			{
			$('#delfilerow_'+$fileid).hide();
		//}

		}
	}
}
var $userid;
function deleteuserfromassessment(userid)
{
	var confirmmess=confirm('Are you sure to delete?');
	if(confirmmess==true)
	{
		var baseurl=document.f1.baseurl.value;
		var assessid=document.f1.assessid.value;
		var scheid=document.f1.scheid.value;
		SITEROOT=baseurl;
		var operation='deleteuserfromassessment';
		$userid=userid;
		//alert(assessid);
		if(assessid!='')
		{
			var extnd='/assessid/'+assessid;
		}
		if(assessid=='')
		{
			var randomval=document.f1.randomval.value;
			assessid='';
			var	extnd='/randomval/'+randomval+'/assessid/'+assessid;
		}
		
		var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/deleteuserid/'+userid+extnd+'/scheid/'+scheid;
		createXMLHttpRequest();
		var strURL=url;
		var query ="";
		if (xmlHttpRequest != null) 
		{
			xmlHttpRequest.open("post", strURL, true);
			xmlHttpRequest.onreadystatechange = handledeldeleteuserfromassessment;
			xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlHttpRequest.send(query);
		}
	}
}
function handledeldeleteuserfromassessment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//			if(xmlDoc=='deleteuserfromassessment')
			//			{
			$('#deluserrow_'+$userid).hide();
			var	usr=document.getElementById("myexistusr").value;
			if(usr!=''){
			if (usr.indexOf(","+$userid) != -1){
			 var b=usr.replace(","+$userid,"");
			document.getElementById("myexistusr").value=b;
			if(b==''){
				$('#userlistheader').hide();
				}
		}
		}

		}
	}
}
var $divname;
function addnewtext(divname,operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	$divname=divname;
	if(divname=='ofmultichoice'){
		operation=='addnewtext'
	}
	if(divname=='ofmultiselect'){
		operation=='addnewtextmsel';
	}
	var url=SITEROOT+'/assessment/editassessment/mode/'+operation;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handledeladdnewtext;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function handledeladdnewtext()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			
			var new_div1 = window.document.createElement('div');
			new_div1.innerHTML = xmlDoc;
			if($divname=='ofmultichoice'){
			
				document.getElementById("addmoretextbox").appendChild(new_div1,xmlDoc);
				}
			if($divname=='ofmultiselect')
				document.getElementById("addmoretextboxmsel").appendChild(new_div1,xmlDoc);
		
			var typeval=$("#Items").val();
			if (typeval==4)
			{
				$('div[id^="addquesradio"]').show();
				$('span[id^="repidv"]').hide();
				
				$('div[id^="addquesmltisel"]').show();
			}
			else if (typeval==5)
			{
				$('div[id^="addquesradio"]').show();
				$('div[id^="addquesmltisel"]').show();
			}
		}
	}
}
var $questiontype;
function addnewtext1(questionno,answerid,questiontypeid)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	$questionno=questionno;
	$questiontype=questiontypeid;
	var operation;
	if(questiontypeid=='1')
		operation='addnewtext1';
	if(questiontypeid=='6')
		operation='addnewtext2';
	var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/questionid/'+questionno+'/answerid/'+answerid;
	$.post(url,function(){},"");
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handledeladdnewtext1;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function handledeladdnewtext1()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			var div_node = document.getElementById('newtextwithnoanswdiv');
			var new_div1 = window.document.createElement('div');
			new_div1.innerHTML = xmlDoc;
			var divname;
			if($questiontype=='1')
				divname="dataofmultichoicediv_";
			if($questiontype=='6'){
				divname="dataofmultiselectdiv_";
				
			//	document.getElementById("dataofmultiselectdiv_"+$questionno).appendChild(new_div1,xmlDoc);
			}
			document.getElementById(divname+$questionno).appendChild(new_div1,xmlDoc);
			divname='';
			var typeval=$("#Items").val();
		if (typeval==4)
		{
			$('div[id^="viewquesaddradio"]').hide();
			$('div[id^="viewquesmselchkbx"]').hide();
		}
		else if (typeval==5)
		{
			$('div[id^="viewquesaddradio"]').show();
			$('div[id^="viewquesmselchkbx"]').show();
		}
		}
	}
}

var $selectedivradioid;
var $questionid;
function checkdivasradio(id)
{
	var baseurl=document.f1.baseurl.value;

	SITEROOT=baseurl;
	$selectedivradioid=id;
	var operation;
	if(id==1){
		operation='addnewtext12';
		var div_node = document.getElementById('newtextwithnoanswdiv');
		if(div_node==null){
			div_node = document.getElementById('newtextwithnoanswdivofmultiselect');
		}
		div_node.id='newtextwithnoanswdivofmultichoice';
		var onclick =document.getElementById( "addnew" ).getAttribute("onclick");
		
		document.getElementById( "addnew" ).setAttribute('onclick','addnewtext("ofmultichoice","addnewtext")');
	}
	if(id==2)
		operation='linkertscale';
	if(id==4)
		operation='yesno';
	if(id==3)
		operation='truefalse';
	if(id==5)
		operation='describe';
	if(id==6){
		var divid=1;
		operation='multiselect';
		var div_node = document.getElementById('newtextwithnoanswdiv');
		if(div_node==null){
			div_node = document.getElementById('newtextwithnoanswdivofmultichoice');
		}
		div_node.id='newtextwithnoanswdivofmultiselect';
		var onclick =document.getElementById( "addnew" ).getAttribute("onclick");
		document.getElementById( "addnew" ).setAttribute('onclick','addnewtext("ofmultiselect","addnewtextmsel")');
	}
	if(id=='chooselikfrommulti'){
		operation='chooselikfrommulti';
		$questionid=questionid;
	}
	var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedivradioid/'+id;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlecheckdivasradio;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function handlecheckdivasradio()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {

			var xmlDoc=xmlHttpRequest.responseText;

			if($selectedivradioid==1)
			{
				
				$('#addmoretextbox').show();
				$('#wholediv').show();
				$('#addnew').show();
				$('#showcorectans').hide();
				$('#choosetruefalse').hide();
				$('#chooseval').hide();
				$('#chooselinkert').hide();
				$('#describe').hide();
				$('#multichoiceadd').show();
				$('#newtextwithnoanswdivofmultichoice').show();


				$('#linkertchoiceadd').hide();
				$('#truefalseadd').hide();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#linkshowblack').hide();
				$('#yesnoblack').hide();
				$('#tfblack').hide();
				$('#textansblack').hide();
				$('#multichoiceblack').show();
				$('#mutichoiceshow').hide();
				$('#textansshow').show();
				
				var typeval=$("#Items").val();
				if (typeval==4)
				{
					$('#linkshow').show();
					$('[name="radioanswer1"]').hide();
				}
				else if (typeval==5)
				{
					$('#linkshow').hide();
					$('[name="radioanswer1"]').show();
				}
				
				
				$('#yesnoshow').show();
				$('#tfblack').hide();
				$('#tfshow').show();
				$('#multiselectadd').hide();
				$('#choosemultiselect').hide();
				$('#multiselectblack').hide();
				$('#multiselectshow').show();

			//        var new_div1 = window.document.createElement('div');
			//         new_div1.innerHTML = xmlDoc;
			//       document.getElementById("addmoretextbox").appendChild(new_div1,xmlDoc);
			}
			if($selectedivradioid==2)
			{
				$('#addmoretextbox').hide();
				$('#wholediv').hide();
				$('#addnew').hide();
				$('#choosetruefalse').hide();
				$('#chooseval').hide();
				$('#chooselinkert').show();
				$('#multichoiceadd').hide();
				$('#describe').hide();
				$('#linkertchoiceadd').show();
				$('#truefalseadd').hide();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#linkexpertshow').hide();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#linkshowblack').show();
				$('#yesnoblack').hide();
				$('#textansblack').hide();
				$('#multichoiceblack').hide();
				$('#mutichoiceshow').show();
				$('#textansshow').show();
				$('#linkshow').hide();
				$('#yesnoshow').show();
				$('#newtextwithnoanswdivofmultiselect').hide();
				$('#newtextwithnoanswdivofmultichoice').hide();
				$('#tfblack').hide();
				$('#tfshow').show();
				$('#multiselectadd').hide();
				$('#choosemultiselect').hide();
				$('#multiselectblack').hide();
				$('#multiselectshow').show();
			}
			if($selectedivradioid==3)
			{
				$('#addmoretextbox').hide();
				$('#wholediv').hide();
				$('#addnew').hide();
				$('#choosetruefalse').show();
				$('#chooseval').hide();
				$('#chooselinkert').hide();
				$('#describe').hide();
				$('#multichoiceadd').hide();
				$('#newtextwithnoanswdivofmultiselect').hide();
				$('#newtextwithnoanswdivofmultichoice').hide();

				$('#linkertchoiceadd').hide();
				$('#truefalseadd').show();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#linkshowblack').hide();
				$('#yesnoblack').hide();
				$('#tfblack').show();
				$('#tfshow').hide();
				$('#textansblack').hide();
				$('#multichoiceblack').hide();
				$('#mutichoiceshow').show();
				$('#textansshow').show();
				var typeval=$("#Items").val();
				if (typeval==4)
				{
					$('#linkshow').show();
					$('input[name="trueval"]').attr('disabled', 'disabled');
				}
				else if (typeval==5)
				{
					$('#linkshow').hide();
					$('input[name="trueval"]').removeAttr('disabled');
				}
				$('#yesnoshow').show();
				$('#multiselectadd').hide();
				$('#choosemultiselect').hide();
				$('#multiselectblack').hide();
				$('#multiselectshow').show();



			//        var new_div1 = window.document.createElement('div');
			//         new_div1.innerHTML = xmlDoc;
			//       document.getElementById("addmoretextbox").appendChild(new_div1,xmlDoc);
			}
			if($selectedivradioid==4)
			{
				$('#addmoretextbox').hide();
				$('#wholediv').hide();
				$('#addnew').hide();
				$('#chooseval').show();
				$('#chooselinkert').hide();
				$('#choosetruefalse').hide();
				$('#describe').hide();
				$('#multichoiceadd').hide();
				$('#linkertchoiceadd').hide();
				$('#truefalseadd').hide();
				$('#yesnoadd').show();
				$('#textansadd').hide();
				$('#linkshowblack').hide();
				$('#yesnoblack').show();
				$('#textansblack').hide();
				$('#multichoiceblack').hide();
				$('#mutichoiceshow').show();
				$('#textansshow').show();
				var typeval=$("#Items").val();
				if (typeval==4)
				{
					$('#linkshow').show();
					$('input[name="yesradio"]').attr('disabled', 'disabled');
				}
				else if (typeval==5)
				{
					$('#linkshow').hide();
					$('input[name="yesradio"]').removeAttr('disabled');
				}
				$('#yesnoshow').hide();
				$('#newtextwithnoanswdivofmultiselect').hide();
				$('#newtextwithnoanswdivofmultichoice').hide();
				$('#tfblack').hide();
				$('#tfshow').show();
				$('#multiselectadd').hide();
				$('#choosemultiselect').hide();
				$('#multiselectblack').hide();
				$('#multiselectshow').show();
			}
			if($selectedivradioid==5)
			{
				$('#addmoretextbox').hide();
				$('#wholediv').hide();
				$('#addnew').hide();
				$('#chooseval').hide();
				$('#chooselinkert').hide();
				$('#choosetruefalse').hide();
				$('#multichoiceadd').hide();
				$('#linkertchoiceadd').hide();
				$('#truefalseadd').hide();
				$('#yesnoadd').hide();
				$('#textansadd').show();
				$('#describe').show();
				$('#linkshowblack').hide();
				$('#yesnoblack').hide();
				$('#textansblack').show();
				$('#multichoiceblack').hide();
				$('#mutichoiceshow').show();
				$('#textansshow').hide();
				var typeval=$("#Items").val();
				if (typeval==4)
				{
					$('#linkshow').show();
				}
				else if (typeval==5)
				{
					$('#linkshow').hide();
				}
				$('#yesnoshow').show();
				$('#newtextwithnoanswdivofmultiselect').hide();
				$('#newtextwithnoanswdivofmultichoice').hide();
				$('#tfblack').hide();
				$('#tfshow').show();
				$('#multiselectadd').hide();
				$('#choosemultiselect').hide();
				$('#multiselectblack').hide();
				$('#multiselectshow').show();
			}
			if($selectedivradioid==6)
			{
				$('#addmoretextbox').show();
				$('#wholediv').hide();
				$('#addnew').show();
				$('#choosetruefalse').hide();
				$('#chooseval').hide();
				$('#chooselinkert').hide();
				$('#multichoiceadd').hide();
				$('#describe').hide();
				$('#linkertchoiceadd').hide();
				$('#truefalseadd').hide();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#linkexpertshow').hide();
				$('#yesnoadd').hide();
				$('#textansadd').hide();
				$('#linkshowblack').hide();
				$('#yesnoblack').hide();
				$('#textansblack').hide();
				$('#multichoiceblack').hide();
				$('#mutichoiceshow').show();
				$('#textansshow').show();
				var typeval=$("#Items").val();
				if (typeval==4)
				{
					$('#linkshow').show();
					$("#multiselect_1").hide();
					$("#multiselect_2").hide();
					$("#multiselect_3").hide();
					$("#multiselect_4").hide();
				}
				else if (typeval==5)
				{
					$('#linkshow').hide();
					$("#multiselect_1").show();
					$("#multiselect_2").show();
					$("#multiselect_3").show();
					$("#multiselect_4").show();
				}
				$('#yesnoshow').show();
				$('#newtextwithnoanswdivofmultiselect').show();
				$('#newtextwithnoanswdivofmultichoice').hide();
				$('#tfblack').hide();
				$('#tfshow').show();
				$('#multiselectadd').show();
				$('#choosemultiselect').show();
				$('#multiselectblack').show();
				$('#multiselectshow').hide();
				$('#showcorectans').hide();
					
			}


		}
	}
}
function checkdivasradioonedit(operation,questionid)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	if(operation=='chooselikfrommulti'){

		$questionid=questionid;
		$selectedivradioid=2;
	}
	if(operation=='chooseyesnofrommulti'){

		$questionid=questionid;
		$selectedivradioid=4;
	}
	if(operation=='choosetextfrommulti'){

		$questionid=questionid;
		$selectedivradioid=5;
	}
	if(operation=='choosemultifrommulti'){

		$questionid=questionid;
		$selectedivradioid=1;
	}
	if(operation=='chooselinkertfromlinkert'){

		$questionid=questionid;
		$selectedivradioid='secondinlinkert';
	}
	if(operation=='choosemultifromlinkert'){

		$questionid=questionid;
		$selectedivradioid='firstinlinkert';
	}
	if(operation=='chooseyesnofromlinkert'){

		$questionid=questionid;
		$selectedivradioid='fourthinlinkert';
	}



	var url=SITEROOT+'/assessment/addassessment/mode/'+operation+'/selectedivradioid/'+$selectedivradioid;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleactiveorinactiveinassignment;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}

function handleactiveorinactiveinassignment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			if($selectedivradioid==2)
			{
				$('#linkertchooseddiv_'+$questionid).show();
				$('#mutichoicechooseddiv_'+$questionid).hide();
				$('#multichoiceinblackupdate_'+$questionid).hide();
				$('#textanschooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#editlinkimg_'+$questionid).hide();
				$('#editlinkimgshow_'+$questionid).show();
				$('#multichoiceinblackupdateshow_'+$questionid).show();
				$('#dataofmultichoicediv_'+$questionid).hide();
				$('#yesnofrommultishow_'+$questionid).show();
				$('#yesnofrommultihide_'+$questionid).hide();
				$('#chooselinkert_'+$questionid).hide();
				$('#textansblackshow_'+$questionid).hide();
				$('#textanswhiteshow_'+$questionid).show();
			}
			if($selectedivradioid=='secondinlinkert')
			{
				$('#linkertchooseddiv_'+$questionid).hide();
				$('#mutichoicechooseddiv_'+$questionid).hide();
				$('#chooselinkert_'+$questionid).show();

				$('#multichoiceinwhitelinkert_'+$questionid).show();
				$('#multichoiceinlinkertblack_'+$questionid).hide();
				$('#textanschooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#linkertinwhitelinkert_'+$questionid).hide();
				$('#linkertinblacklinkert_'+$questionid).show();

				$('#dataofmultichoicediv_'+$questionid).hide();
				$('#yesnofrommultishow_'+$questionid).show();
				$('#yesnofrommultihide_'+$questionid).hide();

				$('#textansblackshow_'+$questionid).hide();
				$('#textanswhiteshow_'+$questionid).show();

				$('#yesnoinwhitelinkert_'+$questionid).show();
				$('#yesnoinblacklinkert_'+$questionid).hide();
			}
			//
			if($selectedivradioid==4)
			{
				$('#linkertchooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).show();
				$('#textanschooseddiv_'+$questionid).hide();
				$('#multichoiceinblackupdate_'+$questionid).hide();
				$('#editlinkimg_'+$questionid).show();
				$('#editlinkimgshow_'+$questionid).hide();
				$('#multichoiceinblackupdateshow_'+$questionid).show();
				$('#dataofmultichoicediv_'+$questionid).hide();
				$('#yesnofrommultishow_'+$questionid).hide();
				$('#yesnofrommultihide_'+$questionid).show();
				$('#yesnochooseddiv_'+$questionid).show();
				$('#textansblackshow_'+$questionid).hide();
				$('#textanswhiteshow_'+$questionid).show();
				$('#mutichoicechooseddiv_'+$questionid).hide();
				$('#chooselinkert_'+$questionid).hide();
			}
			if($selectedivradioid==5)
			{
				$('#linkertchooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#textanschooseddiv_'+$questionid).show();
				$('#multichoiceinblackupdate_'+$questionid).hide();
				$('#editlinkimg_'+$questionid).show();
				$('#editlinkimgshow_'+$questionid).hide();
				$('#multichoiceinblackupdateshow_'+$questionid).show();
				$('#dataofmultichoicediv_'+$questionid).hide();
				$('#yesnofrommultishow_'+$questionid).show();
				$('#yesnofrommultihide_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#textansblackshow_'+$questionid).show();
				$('#textanswhiteshow_'+$questionid).hide();
				$('#mutichoicechooseddiv_'+$questionid).hide();
				$('#chooselinkert_'+$questionid).hide();
			}
			if($selectedivradioid==1)
			{
				$('#linkertchooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#textanschooseddiv_'+$questionid).hide();
				$('#multichoiceinblackupdate_'+$questionid).show();
				$('#editlinkimg_'+$questionid).show();
				$('#editlinkimgshow_'+$questionid).hide();
				$('#multichoiceinblackupdateshow_'+$questionid).hide();
				$('#dataofmultichoicediv_'+$questionid).show();
				$('#yesnofrommultishow_'+$questionid).show();
				$('#yesnofrommultihide_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#textansblackshow_'+$questionid).hide();
				$('#mutichoicechooseddiv_'+$questionid).hide();
				$('#chooselinkert_'+$questionid).hide();
				$('#textanswhiteshow_'+$questionid).show();
				$('#yesnoinwhitelinkert_'+$questionid).show();
				$('#yesnoinblacklinkert_'+$questionid).hide();
			}
			if($selectedivradioid=='firstinlinkert')
			{
				$('#mutichoicechooseddiv_'+$questionid).show();
				$('#linkertchooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#textanschooseddiv_'+$questionid).hide();
				$('#multichoiceinwhitelinkert_'+$questionid).hide();
				$('#multichoiceinlinkertblack_'+$questionid).show();
				$('#linkertinwhitelinkert_'+$questionid).show();
				$('#linkertinblacklinkert_'+$questionid).hide();

				$('#dataofmultichoicediv_'+$questionid).hide();
				$('#yesnofrommultishow_'+$questionid).show();
				$('#yesnofrommultihide_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).hide();
				$('#textansblackshow_'+$questionid).hide();
				$('#textanswhiteshow_'+$questionid).show();
				$('#chooselinkert_'+$questionid).hide();
				$('#yesnoinwhitelinkert_'+$questionid).show();
				$('#yesnoinblacklinkert_'+$questionid).hide();
			//$('#multichoiceinblacklinkert_'+$questionid).show();

			}
			if($selectedivradioid=='fourthinlinkert')
			{
				$('#mutichoicechooseddiv_'+$questionid).hide();
				$('#linkertchooseddiv_'+$questionid).hide();
				$('#yesnochooseddiv_'+$questionid).show();
				$('#textanschooseddiv_'+$questionid).hide();
				$('#multichoiceinwhitelinkert_'+$questionid).show();
				$('#multichoiceinlinkertblack_'+$questionid).hide();
				$('#linkertinwhitelinkert_'+$questionid).show();
				$('#linkertinblacklinkert_'+$questionid).hide();

				$('#dataofmultichoicediv_'+$questionid).hide();
				$('#yesnofrommultishow_'+$questionid).hide();
				$('#yesnofrommultihide_'+$questionid).hide();


				$('#textansblackshow_'+$questionid).hide();
				$('#textanswhiteshow_'+$questionid).show();
				$('#chooselinkert_'+$questionid).hide();
				$('#yesnoinwhitelinkert_'+$questionid).hide();
				$('#yesnoinblacklinkert_'+$questionid).show();


			//$('#multichoiceinblacklinkert_'+$questionid).show();

			}

			if(xmlDoc=='delete')
			{
				$('#delrow_'+$usertrainingid).hide();
			}
			if(xmlDoc=='approved')
			{
				document.getElementById("approve_"+$usertrainingid).style.display = 'none';
				var url=SITEROOT+'/supervisor/supervisortools';
			//   window.location=url;

			}
			if(xmlDoc=='deny')
			{
				document.getElementById("deny_"+$usertrainingid).style.display = 'none';
			}
			if(xmlDoc=='toenroll')
			{
				$('#delenrolledrow_'+$usertrainingid).hide();
				var url=SITEROOT+'/supervisor/supervisortools';

			}


		}
	}
}
var $selectedgroupids;
function choosegroupforassessment()
{

	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var arr=document.getElementsByName("checksingle[]");
	var assessid=document.getElementById('assessid').value;
	var userlist='userlist';
	var arr_active=0;

	for(i=0;i<arr.length;i++)
	{
		if(arr[i].checked==true)
		{
			if(arr_active!=0)
			{
				arr_active = arr_active+","+arr[i].value;
			}
			else
			{
				arr_active=arr[i].value;
			}

		}
	}
	if(arr_active==0)
	{
		alertify.alert('Select Group Name');
		return false;
	}
	var mode='selectegrouponedit';
	if(arr_active!=0)
		$selectedgroupids=arr_active;
	if(arr_active==0)
		selectedgroupids=0;
	if(assessid!='')
	{

		var url=SITEROOT+'/assessment/addassessment/assessid/'+assessid+'/selectedgroup/'+arr_active+'/mode/'+mode;
	}
	if(assessid=='')
		var url=SITEROOT+'/assessment/addassessment/selectedgroup/'+arr_active+'/mode/'+mode;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlechoosegroupforassessment;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function handlechoosegroupforassessment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			tb_remove();
			document.getElementById("selectedgroupsfrommanagpop").innerHTML=xmlDoc;

		}
	}
}
function bulkaction()
{
	//var arr=document.getElementsByName("checkedassessid");
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var i = document.getElementById("operationvalue").selectedIndex;
	var selectedoperation=  document.getElementById("operationvalue").options[i].value;

	var arr_active=0;
        arr_active=getSelectedIds()
//	for(i=0;i<arr.length;i++)
//	{
//		if(arr[i].checked==true)
//		{
//			if(arr_active!=0)
//			{
//				arr_active = arr_active+","+arr[i].value;
//			}
//			else
//			{
//				arr_active=arr[i].value;
//			}
//
//		}
//	}
	if(arr_active==0)
	{
		alertify.alert('Please Select Assessment to apply any action.');
		return false;
	}
	if(selectedoperation=='activate')
		var url=SITEROOT+'/assessment/bulkopt/mode/active/check1/'+arr_active;
	if(selectedoperation=='delete')
		var url=SITEROOT+'/assessment/bulkopt/mode/delete/check1/'+arr_active;
	if(selectedoperation=='inactivate')
		var url=SITEROOT+'/assessment/bulkopt/mode/inactive/check1/'+arr_active;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlebulkaction;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}

//window.location.replace(url);
}
function handlebulkaction()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			//var user='';
			var xmlDoc=xmlHttpRequest.responseText;
			
			alertify.alert('Action Taken Successfully.');
			//var url=SITEROOT+'/assessment/viewassessment/userid/'+$adminuserid+'/assessid/'+viewassessid;
			var url=SITEROOT+'/assessment/assessmenttools/';
			window.location.replace(url);
		}
	}
}
var viewassessid;
var $adminuserid;
function viewassessment(adminuserid)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var assessid=document.f1.assessid.value;
	var assess_id=$('#assessid').val();
	if(assess_id!='') assessid=assess_id;
	if(assessid!='' ){
		viewassessid=assessid;
		$adminuserid=adminuserid;

		var url=SITEROOT+'/assessment/previewassessment/mode/'+'view'+'assessid/'+assessid;
	}
	//window.location.replace(url
	if(assessid==''){
		
		alertify.alert('Please Create Assessment then check its view');
		return false;
	}
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleviewassessment;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}

}
function handleviewassessment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			//var user='';
			var xmlDoc=xmlHttpRequest.responseText;
			var url=SITEROOT+'/assessment/previewassessment/userid/'+$adminuserid+'/assessid/'+viewassessid;
			tb_show(title='', url+'?height=500&width=950&inlineId=hiddenModalContent&modal=false','');
		//	var url=SITEROOt+'/assessment/viewassessment/assessid/'+viewassessid;
		//window.location.replace(url);
		}
	}
}

function generateurl(operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var assessid=document.f1.assessid.value;
	var assess_id=$('#assessid').val();
	if(operation=='notrecassess')
		var scheid='0';
	if(operation=='recassess')
		var scheid=document.f1.scheid.value;

	if(assess_id!='') assessid=assess_id;
	if(assessid!='' ){
		var url=SITEROOT+'/assessment/generateurl/mode/generateurl/'+'assessid/'+assessid+'/scheid/'+scheid;
	}
	var httphost=document.getElementById('httphost').value;
	if(assessid==''){
	
		alertify.alert('Please Create Assessment then check its view');
		return false;
	}
	
	$.post(url , function(response){
			document.getElementById("urlid").value='http://'+httphost+SITEROOT+'/survey/viewsurvey/surveyid/'+response;
			document.getElementById("copy-button").setAttribute('data-clipboard-text','http://'+httphost+SITEROOT+'/survey/viewsurvey/surveyid/'+response);
		});
}
/* function handlegenerateurl()
{
	if (xmlHttpRequest.readyState == 4)
	{
		var httphost=document.getElementById('httphost').value;
		if (xmlHttpRequest.status == 200) {
			//var user='';
			var xmlDoc=xmlHttpRequest.responseText; 
			var a='';
			document.getElementById("urlid").value='http://'+httphost+SITEROOT+'/survey/viewsurvey/surveyid/'+xmlDoc;
			document.getElementById("copy-button").setAttribute('data-clipboard-text','http://'+httphost+SITEROOT+'/survey/viewsurvey/surveyid/'+xmlDoc);
		
		//	alert("URL has been Genrerated Successfully");
		//var url=SITEROOT+'/assessment/previewassessment/userid/'+$adminuserid+'/assessid/'+viewassessid;
		//tb_show(title='', url+'?height=420&width=628&inlineId=hiddenModalContent&modal=false','');
		//	var url=SITEROOt+'/assessment/viewassessment/assessid/'+viewassessid;
		//window.location.replace(url);
		}
	}
} */

function sendToClipboard()
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var urlid=document.f1.urlid.value;
	//var urlid=SITEROOT+'/survey/viewsurvey/surveyid/'+urlid;
	var holdtext=document.f1.holdtext;
	holdtext.innerText = urlid;
	//	Copied = holdtext.createTextRange();
	//Copied.execCommand("Copy");
	if(urlid=='')
	{
		alertify.alert('Please Copy the Url ');
		return false;
	}
	if( window.clipboardData && clipboardData.setData )
	{
		clipboardData.setData("Text", urlid);

	}
	else if (window.netscape)
	{
		//#pref("toolkit.defaultChromeURI", "chrome://esc/content/settings.xul");
		// netscape.security.PrivilegeManager.enablePrivilege('UniversalPreferencesRead');

		//pref("signed.applets.codebase_principal_support",true);

		//user_pref("signed.applets.codebase_principal_support", true);
		netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

		var clip = Components.classes['@mozilla.org/widget/clipboard;1']
		.createInstance(Components.interfaces.nsIClipboard);
		if (!clip) return;

		var trans = Components.classes['@mozilla.org/widget/transferable;1']
		.createInstance(Components.interfaces.nsITransferable);
		if (!trans) return;

		trans.addDataFlavor('text/unicode');

		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"]
		.createInstance(Components.interfaces.nsISupportsString);
		var copytext=urlid;
		str.data=copytext;
		trans.setTransferData("text/unicode",str,copytext.length*2);
		var clipid=Components.interfaces.nsIClipboard;
		if (!clip) return false;
		clip.setData(trans,null,clipid.kGlobalClipboard);
	}
	
	alertify.alert("The URL has copied:\n\n" + urlid);
}
function userselectionlist(operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var assessid=document.getElementById('assessid').value;
	var assess_id=document.getElementById('assess_id').value;
	//	var urlid=document.getElementById('urlid').value;
	//	var surveyid = urlid.split(/[/ ]+/).pop();

	if(assess_id!='') {
		assessid=assess_id;
	}
	//var assignselectedcourses='selectedusersandgroups';
	var checkeduserval=document.getElementsByName("checkbox2[]");

	if(operation=='selectall')
	{
		for(var i=0;i<checkeduserval.length; i++){
			checkeduserval[i].checked=true;
		}
		if(checkeduserval.length==undefined)
			document.getElementsById("checkbox2").checked=true;
		var grr=document.getElementsByName("groupcheck");
		for(var i=0;i<grr.length; i++){
			grr[i].checked=true;
		}
		if(grr.length==undefined)
			document.getElementsById("groupcheck").checked=true;
	}
	if(operation=='deselectall')
	{
		for( var i=0;i<checkeduserval.length; i++){
			checkeduserval[i].checked=false;
		}
		if(checkeduserval.length==undefined)
			document.getElementsById("checkbox2").checked=false;
		var grr=document.getElementsByName("groupcheck");
		for(var i=0;i<grr.length; i++){
			grr[i].checked=false;
		}
		if(grr.length==undefined)
			document.getElementsById("groupcheck").checked=false;
	}

}

function adduserbulkaction()
{
	selectedusersandgroups('checkoperation');
}

var $deleteusers;
var $usercheckbox;
function selectedusersandgroups(operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var assessid=document.getElementById('assessid').value;
	var assess_id=document.getElementById('assess_id').value;
	var scheid=document.getElementById('scheid').value;
	var clsedate=document.f1.clsedate.value;
	var crdate=document.f1.crdate2.value;
	
	// var assess_duedate=document.f1.crdate2.value;
	
	if(assess_id!='') 
	{
		assessid=assess_id;
	}
	
	var arr=document.getElementsByName("checkbox2[]");
	var grr=document.getElementsByName("groupcheck");
	var arr_active=0;
	var group_tag=0;
	var mode;
	var action;

	if(assessid!='')
	{
		if(clsedate < crdate )
		{
			alertify.alert('You cannot distribute an assessment that is closed');
			return false;
		}
	
		for(i=0;i<arr.length;i++)
		{
			if(arr[i].checked==true)
			{
				//alert(arr[i].value);
				if(arr_active!=0)
				{
					arr_active = arr_active+","+arr[i].value;
				}
				else
				{
					arr_active=arr[i].value;
				}
			}
		}
		//return false;
		for(i=0;i<grr.length;i++)
		{
			if(grr[i].checked==true)
			{
				if(group_tag!=0)
				{
					group_tag = group_tag+","+grr[i].value;
				}
				else
				{
					group_tag=grr[i].value;
				}
			}
		}	
		
		if(arr_active==0 && operation!='selectedusersandgroups' && group_tag==0)
		{
			alertify.alert('Please Select User.');
			return false;
		}
		else
		{
			action='addassessment';
			if(operation=='checkoperation')
			{
				var i = document.getElementById("checkaction").selectedIndex;
				var selectedoperation=  document.getElementById("checkaction").options[i].value;
				if(selectedoperation=='deleteusers')
				{
					$deleteusers='deleteusers';
					mode='deleteusers';
					if(confirm('Are you sure you want to delete?')==false)
					{
						return false;
					}
				}
				/*alert(selectedoperation);
				return false;*/
				if(selectedoperation=='sendassess')
				{
					document.getElementById('loadingiddis').style.display='block';
					$deleteusers='sendassess';
					mode='selectedusersandgroups';
					//var url=SITEROOT+'/assessment/addassessment/mode/'+'selectedusersandgroups'+'/selectedusersfinally/'+arr_active+'/selectedgroupsfinally/'+group_tag+'/assessid/'+assessid;
					var url=SITEROOT+'/assessment/addassessment/mode/selectedusersandgroups/selectedusersfinally/'+arr_active+'/selectedgroupsfinally/'+group_tag+'/assessid/'+assessid;
				}
				var url=SITEROOT+'/assessment/'+action+'/mode/'+mode+'/selectedusersfinally/'+arr_active+'/selectedgroupsfinally/'+group_tag+'/assessid/'+assessid+'/scheid/'+scheid;
			}
		}
	}
	//alert(url);
	if(assessid=='')
	{
		var randomval=document.f1.randomval.value;
		if(operation =='selectedusersandgroups')
		{
			var url=SITEROOT+'/assessment/addassessment/mode/blankid';
		}
		if(operation!='selectedusersandgroups')
		{
			for(i=0;i<arr.length;i++)
			{
				if(arr[i].checked==true)
				{
					if(arr_active!=0)
					{
						arr_active = arr_active+","+arr[i].value;
					}
					else
					{
						arr_active=arr[i].value;
					}
				}
			}
			if(arr_active==0)
			{
				alertify.alert('Please Select User.');
				return false;
			}
			var i = document.getElementById("checkaction").selectedIndex;
			var selectedoperation=  document.getElementById("checkaction").options[i].value;
			$deleteusers=selectedoperation;
			document.getElementById('loadingiddis').style.display='block';
			if(selectedoperation=='deleteusers')
			{
				var url=SITEROOT+'/assessment/addassessment/mode/'+selectedoperation+'/selectedusersfinally/'+arr_active+'/scheid/'+scheid;
			}
			if(selectedoperation=='sendassess')
			{
				var url=SITEROOT+'/assessment/addassessment/mode/blankid';
			}
		}
	}
	createXMLHttpRequest();
	var strURL=url;

	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleassignselectedcoursestouser;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}

function handleassignselectedcoursestouser()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {

			var xmlDoc=xmlHttpRequest.responseText;
			var val1=xmlDoc;
			
			if(val1=='blankid')
			{
				alertify.alert('Create an assessment before sending to a Distribution List');
			}

			if($deleteusers=='deleteusers')
			{
				var arr1=new Array();
				arr2=xmlDoc;
				arr3=xmlDoc.split('###');
				grplist=arr3[0].split(',');
				groupid=arr3[0];
				
				groupuserid=arr3[1];
				
				
				var	a=document.getElementById("myexistgrp").value;
				if(a!='')
				{
					
					if(a.indexOf(","+groupid) != -1)
					{
						var b=a.replace(","+groupid,"");
						
						document.getElementById("myexistgrp").value=b;
						if(b=='')
						{
							$('#userlistheader').hide();
						}
					}
				}
				var	u=document.getElementById("myexistusr").value;
				if(u!='')
				{
					//alert(groupuserid);
					
					if(u.indexOf(","+groupuserid) != -1)
					{
						var b=u.replace(","+groupuserid,"");
						
						document.getElementById("myexistusr").value=b;
						if(b=='')
						{
							$('#userlistheader').hide();
						}
					}
				}
				for(a1=0;a1<=grplist.length-1;a1++)
				{
					deluserid=grplist[a1];
					
					deluserid=deluserid.split(' ').join('');
					if($("#delgrouprow_"+deluserid))
						$("#delgrouprow_"+deluserid).hide();
				}
				
				arr1=arr3[1].split(',');
				var deluserid;
				for(a1=0;a1<=arr1.length-1;a1++)
				{
					deluserid=arr1[a1];
					//alert(arr1[a1]);
					deluserid=deluserid.split(' ').join('');

					//document.getElementsById("deluserrow_"+deluserid).style.display='none';
					$("#deluserrow_"+deluserid).hide();
					
				}
				var arr=document.getElementsByName("checkbox2[]");
				for(i=0;i<arr.length;i++)
				{
					if(arr[i].checked==true)
					{
						arr[i].value='';


					}
				}
				var grr=document.getElementsByName("groupcheck");
				for(i=0;i<grr.length;i++)
				{
					if(grr[i].checked==true)
					{
						grr[i].value='';


					}
				}

			}

			if(xmlDoc=='existtingid')
			{

				
				alertify.alert('Assessment/Survey sent successfully to selected users/Groups. \n\n     Please click the update button to save these changes.');
				//alert('Please click on update to save these changes.');
				if($deleteusers!='sendassess'){
					var arr=document.getElementsByName("checkbox2[]");
					for(i=0;i<arr.length;i++)
					{
						arr[i].checked=true;
					}
					var grr=document.getElementsByName("groupcheck");
				for(i=0;i<grr.length;i++)
				{
					grr[i].checked==true;
				}
				}

			}
			document.getElementById('loadingiddis').style.display='none';
		}
		xmlDoc='';
	}
}
function OnKeyPress(field, event)
{

	if (event.keyCode == 13) {
		for (i = 0; i < field.form.elements.length; i++)
			if (field.form.elements[i].tabIndex == field.tabIndex+1) {
				field.form.elements[i].focus();

				if (field.form.elements[i].type == "text")
					field.form.elements[i].select();
				//if (field.form.elements[i].type == "radio")
				//field.form.elements[i].select();
				break;
			}

		return false;
	}
	return true;
}
function closeWindow() {
	window.open('','_self','');
	var browserName=navigator.appName;
	if (navigator.userAgent.indexOf("Firefox")!=-1)
	{
		netscape.security.PrivilegeManager.enablePrivilege('UniversalBrowserWrite');
		window.close();
	}
	else
	{

		setTimeout("location.reload();",20);
		close();
	//		var url=SITEROOT+'/assessment/viewassessment';
	//window.open('','_parent','');
	//window.close();
	}

}

function checkendingtype()
{

	if (document.getElementById('endingtype').value== 'closedate')
	{
		$('#closedatediv').show();
		$('#noofoccdiv').hide();
		return false;
	}
	if (document.getElementById('endingtype').value== 'endafterocc')
	{
		$('#noofoccdiv').show();
		$('#closedatediv').hide();
		return false;
	}
	if (document.getElementById('endingtype').value== 'noendingdate')
	{
		$('#noofoccdiv').hide();
		$('#closedatediv').hide();
		return false;
	}
}
function selecttypeofduedate(indexvalue)
{

	if (indexvalue== ''){
		$('#noofdaysindex').hide();
		$('#duedatevalueindex').hide();
		return false;
	}
	if (indexvalue== 'duedatevalue')
	{
		$('#duedatevalueindex').show();
		$('#noofdaysindex').hide();
		return false;
	}
	if (indexvalue== 'noofdays')
	{
		$('#noofdaysindex').show();
		$('#duedatevalueindex').hide();
		return false;
	}

}
function selectfrequencychange(indexvalue)
{

	if (indexvalue== 'Daily'){
		$('#custondailyid').show();
		$('#custonmonthlyid').hide();
		$('#custonweeklyid').hide();
		$('#custonyearlyid').hide();
		return false;
	}
	if (indexvalue== 'Monthly')
	{
		$('#custommonthlyinnerid').show();
		$('#custonmonthlyid').show();
		$('#custondailyid').hide();
		$('#custonweeklyid').hide();
		$('#custonyearlyid').hide();
		return false;
	}
	if (indexvalue== 'Yearly')
	{
		$('#custommonthlyinnerid').show();
		$('#custonmonthlyid').show();
		$('#custonweeklyid').hide();
		$('#custondailyid').hide();
		$('#custonyearlyid').show();
		return false;
	// 				document.getElementById("custommonthlyinnerid").style.display = 'show';
	//				document.getElementById("custonyearlyid").style.display = 'show';
	//				document.getElementById("custonmonthlyid").style.display = 'show';
	//				document.getElementById("custondailyid").style.display = 'none';
	//				 document.getElementById("custonweeklyid").style.display = 'none';

	}
	if(indexvalue=='Weekly')
	{
		$('#custommonthlyinnerid').hide();
		$('#custonmonthlyid').hide();
		$('#custonweeklyid').show();
		$('#custondailyid').hide();
		$('#custonyearlyid').hide();
		return false;
	//				document.getElementById("custommonthlyinnerid").style.display = 'none';
	//				document.getElementById("custonyearlyid").style.display = 'none';
	//				document.getElementById("custonmonthlyid").style.display = 'none';
	//				document.getElementById("custondailyid").style.display = 'none';
	//				 document.getElementById("custonweeklyid").style.display = 'show';
	}

}

function getfilterforcurrentassessment()
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	$('#loadingiddis').show();
	var operation='forcurrentassessment';
	$('#currentassessments').hide;

	//var url=SITEROOT+'/assessment/assessmenttools/mode/'+operation;
	var url=SITEROOT+'/assessment/currentassessmenttools/mode/'+operation;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlefilterforcurrentassessment;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function handlefilterforcurrentassessment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//$('#currenttablediv').show();
			//$('#allleveltablediv').hide();
			//$('#pasttablediv').hide();
		
		document.getElementById("allleveltablediv").innerHTML=xmlDoc;
		
			//document.getElementById("currentdiv").style.fontWeight='bold';
					$(document).ready(function() {
						oTable = $('#currenttable').dataTable({
							"bJQueryUI": true,
							"tableId": '234123',
							"sPaginationType": "full_numbers",
							"totaldisplyrows":20,
							"tablepaginationname": "<div class='dash_mid_td_upcoming fl' style='width: 80%;'><div class='fl'><a href='#' onclick='selecall();' >Select All </a> | <a href='#' onclick='deselt()' > Deselect All</a></div></div>",
							//"tableId": 'nopage',
							"tableHeadername":"<div class='dash_mid_td_upcoming fl wp97 '><div class='dashboard-upcomming-bg1 wp95 h30 fl'><div class='grey-button1_active fl hand mt02' id='currentassessments' onclick='getfilterforcurrentassessment()' >Open Assessments</div><div class='grey-button1  fl hand ml05 mt02' onclick='getfilterforpastassessment()'>Close Assessments</div><div class='grey-button1 fl hand mt02 ml05' onclick='getfilterforaallassessment()'>All Assessments</div></div></div>"
						});
					} );
		       $('#loadingiddis').hide();
		}

	}
}
function getfilterforpastassessment()
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var operation='forpastassessment';
	$('#loadingiddis').show();

	var url=SITEROOT+'/assessment/passassessmenttools/mode/'+operation;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlefilterforpastassessment;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}
function handlefilterforpastassessment()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//$('#currenttablediv').hide();
			//$('#allleveltablediv').hide();
			//$('#pasttablediv').show();
		document.getElementById("allleveltablediv").innerHTML=xmlDoc;
		
			//document.getElementById("currentdiv").style.fontWeight='bold';
			$(document).ready(function() {
				oTable = $('#currenttable').dataTable({
					"bJQueryUI": true,
					"tableId": '234123',
					"sPaginationType": "full_numbers",
					"totaldisplyrows":10,
					"tablepaginationname": "<div class='dash_mid_td_upcoming fl' style='width: 80%;'><div class='fl'><a href='#' onclick='selecall();' >Select All </a> | <a href='#' onclick='deselt()' > Deselect All</a></div></div>",
					//"tableId": 'nopage',
					"tableHeadername":"<div class='dash_mid_td_upcoming fl wp97 '><div class='dashboard-upcomming-bg1 wp95 h30 fl'><div class='grey-button1 fl hand mt02' id='currentassessments' onclick='getfilterforcurrentassessment()' >Open Assessments</div><div class='grey-button1_active fl hand ml05 mt02' onclick='getfilterforpastassessment()'>Close Assessments</div><div class='grey-button1 fl hand mt02 ml05' onclick='getfilterforaallassessment()'>All Assessments</div></div></div>"
				});
			} );
			$('#loadingiddis').hide();
		}

	}
}
function getfilterforaallassessment()
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var operation='forallassessment';
	$('#loadingiddis').show();
	//$('#currentassessments').hide;
	//var url=SITEROOT+'/assessment/assessmenttools/mode/'+operation;
	var url=SITEROOT+'/assessment/allassessmenttools/mode/'+operation;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";
	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlefilterfortraining;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);
	}
}

function handlefilterfortraining()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//$('#currenttablediv').hide();
			//$('#allleveltablediv').show();
			//$('#pasttablediv').hide();
			document.getElementById("allleveltablediv").innerHTML=xmlDoc;
		
			//document.getElementById("currentdiv").style.fontWeight='bold';
			$(document).ready(function() {
				oTable = $('#currenttable').dataTable({
					"bJQueryUI": true,
					"tableId": '234123',
					"sPaginationType": "full_numbers",
					"totaldisplyrows":10,
					"tablepaginationname": "<div class='dash_mid_td_upcoming fl' style='width: 80%;'><div class='fl'><a href='#' onclick='selecall();' >Select All </a> | <a href='#' onclick='deselt()' > Deselect All</a></div></div>",
					//"tableId": 'nopage',
					"tableHeadername":"<div class='dash_mid_td_upcoming fl wp97 '><div class='dashboard-upcomming-bg1 wp95 h30 fl'><div class='grey-button1 fl hand mt02' id='currentassessments' onclick='getfilterforcurrentassessment()' >Open Assessments</div><div class='grey-button1 fl hand ml05 mt02' onclick='getfilterforpastassessment()'>Close Assessments</div><div class='grey-button1_active fl hand mt02 ml05' onclick='getfilterforaallassessment()'>All Assessments</div></div></div>"
				});
			} );
			$('#loadingiddis').hide();
		}

	}
}
var $selgroups;
var $selusers;
function selectusers()
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var selectallusers=document.f11.selectallusers;
	var checkeduserval=document.f11.checkeduserval;
	////var selectallusers=document.f11.selectallusers;
	if(selectallusers.checked==true)
	{
		for(var i=0;i<checkeduserval.length; i++){
			checkeduserval[i].checked=true;
		}
		if(selectallusers.length==undefined)
			document.f11.checkeduserval.checked=true;
	}
	if(selectallusers.checked==false)
	{
		for( var i=0;i<checkeduserval.length; i++){
			checkeduserval[i].checked=false;
		}
		if(selectallusers.length==undefined)
			document.f11.checkeduserval.checked=false;
	}

}
function selectgroups()
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var selectallgroups=document.f11.selectallgroups;
	var checkedgroupval=document.f11.checkedgroupval;

	////var selectallusers=document.f11.selectallusers;
	if(selectallgroups.checked==true)
	{
		if(checkedgroupval.length==undefined)
			document.f11.checkedgroupval.checked=true;
		for(var i=0;i<checkedgroupval.length; i++){
			checkedgroupval[i].checked=true;
		}

	}
	
	if(selectallgroups.checked==false)
	{
		for( var i=0;i<checkedgroupval.length; i++){
			checkedgroupval[i].checked=false;
		}
		if(checkedgroupval.length==undefined)
			document.f11.checkedgroupval.checked=false;
	}

}
function checkselectedusers()     
{
	var baseurl=document.f1.baseurl.value;
	var scheid=document.f1.scheid.value;
	SITEROOT=baseurl;
	var selectallusers=document.f11.selectallusers;
	var checkeduserval=document.f11.checkeduserval;
	var selectallgroups=document.f11.selectallgroups;
	var checkedgroupval=document.f11.checkedgroupval;
	var assessid=document.f1.assessid.value;
	var assess_id=document.getElementById('assess_id').value;
	if(assess_id!='') assessid=assess_id;
	var arr_active=0;
	var induser=0;
	var indgroup=0;
	var grr_active=0;
	
	var setfirstvalue=document.getElementById('setfirstvalue').value;
	
	if(selectallusers.checked==true)
	{
		for(var i=0;i<checkeduserval.length; i++){
			if(checkeduserval[i].checked==true)
			{
				if(arr_active!=0)
				{
					arr_active = arr_active+","+checkeduserval[i].value;
				}
				else
				{
					arr_active=checkeduserval[i].value;
				}

			}
		}
		
	}
	else
	{
		if(selectallusers.checked==false)
		{
			/*for(var j=0;j<checkeduserval.length; j++){
				if(checkeduserval[j].checked==true)
				{
					if(induser!=0)
					{
						induser = induser+","+checkeduserval[j].value;
					}
					else
					{
						induser=checkeduserval[j].value;
					}
					
					
				}
			}*/
			induser=document.getElementById('selecteduser').value;
			
		}
	
	}				
					
					
	
	if(checkeduserval.length==undefined && checkeduserval.checked==true)
	{
		//induser=checkeduserval.value;
		
	}

	//	if(arr_active==0 && induser==0)
	//	{
	//		alert('Please Select User.');
	//		return false;
	//	}
	//	else{
	//		if(induser==0)
	//		{
	//			return  false;
	//		}
	//	}

	if(selectallgroups.checked==true)
	{
		for(var k=0;k<checkedgroupval.length; k++){
			if(checkedgroupval[k].checked==true)
			{
				if(grr_active!=0)
				{
					grr_active = grr_active +","+checkedgroupval[k].value;
				}
				else
				{
					grr_active=checkedgroupval[k].value;
				}

			}
		}
	
	}
	else
	{
		//	alert(selectallgroups.checked);
		
		if(checkedgroupval)
		{
			if(checkedgroupval.length==undefined && checkedgroupval.checked==true)
			{
				indgroup=checkedgroupval.value;
			//	savedgroup.value=indgroup;
			}
		
			if(selectallgroups.checked==false)
			{
				for(var l=0;l<checkedgroupval.length; l++){
					if(checkedgroupval[l].checked==true)
					{
						if(indgroup!=0)
						{
							indgroup = indgroup+","+checkedgroupval[l].value;
						}
						else
						{
							indgroup=checkedgroupval[l].value;
						}

					}
				}
				
			}
		}
	}
	/* here code from govind */

	lastval=new Array();
	Array1="";
/*	if(document.getElementsByName("grouptosave[]"))
	{
		var chk_arr =  document.getElementsByName("grouptosave[]");
		var chklength = chk_arr.length; 
		k2=0;
		for(k=0;k< chklength;k++)
		{
			var grpidval=chk_arr[k].value;
			var spilvar=grpidval.split(",");
			for(k1=0;k1<spilvar.length;k1++)
			{
				lastval[k2]=spilvar[k1];			
				k2++;
			}
		}
	} */
	if(lastval.length>0)
	{
		commonstrval = new Array();
		var indigroup=indgroup.split(",");
		var Array1 = indigroup;
		var Array2 = lastval;
		for (var i = 0; i<Array2.length; i++) 
		{
			var arrlen = Array1.length;
			for (var j = 0; j<arrlen; j++) {
				if (Array2[i] == Array1[j]) {
					Array1 = Array1.slice(0, j).concat(Array1.slice(j+1, arrlen));
				}//if close
			}//for close
		}//for close
		
	}
	if(Array1!=""){
		indgroup=Array1;
	}
	//alert(indgroup);
	/* end here */
	
	var operation='checkedusersandgroups';
	var urlvar=SITEROOT+'/assessment/addassessment/mode/'+operation+'/indgroup/'+indgroup+'/induser/'+induser+'/allcheckedusers/'+arr_active+'/allcheckedgroups/'+grr_active+'/scheid/'+scheid+'/setfirstvalue/'+setfirstvalue;
	
	if(assessid!='')
	{
		var extn=urlvar+'/assessid/'+assessid;
	}
	if(assessid==''){
		assessid=0;
		var randomval=document.f1.randomval.value;
		var extn=urlvar+'/randomval/'+randomval;
		
	}
	//alert(urlvar);
	var url=extn;
	createXMLHttpRequest();
	var strURL=url;
	var query ="";

	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handlecheckselectedusers;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}


}

function handlecheckselectedusers()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			//var allgroupid=document.getElementById('grouptosave').value;
			//alert(xmlDoc);
			//document.getElementById("myselectedusers").innerHTML=xmlDoc;
			//document.getElementById("myselectedusers").innerHTML=xmlDoc;
			$("#myselectedusers").append(xmlDoc);
			document.getElementById("setfirstvalue").value=1;
			tb_remove();

		}
	}
}
function viewpopup(operation,assessid)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	//var assessid=document.f1.assessid.value;
	if(operation=='recpage'){
		tb_show(title='', SITEROOT+'/assessment/reclist/assessid/'+assessid+'?height=450&width=750&inlineId=hiddenModalContent&modal=false','');
	}
	
	if(operation=='viewusers'){
		var spl=assessid.split('||');
		var assess_id=spl[0];
		var groupid=spl[1];
		tb_show(title='', SITEROOT+'/assessment/userpop/assessid/'+assess_id+'/groupid/'+groupid+'?height=400&width=590&inlineId=hiddenModalContent&modal=false','');
	}
}
function checkinputvalue(quesitonid)
{
	
	var baseurl=document.f1.baseurl.value;
	var check;
	var inputname=document.getElementById('randomansinedit_'+quesitonid);
	if(inputname.checked==true){
		check=1;
	}
	if(inputname.checked==false){
		check=0;
	}
	
	document.getElementById('randomizeansinedit_'+quesitonid).value=check;
}

function assessmentcopy(tblid,assessid)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	
	if(tblid=='allleveltable'){

		var url=SITEROOT+'/assessment/assessmentcopy'+'/assessid/'+assessid;
	}
	$.post(url,function(data){
		if(data=='success'){
			var url=SITEROOT+'/assessment/assessmenttools';
			//$(location).attr('href',url);
			window.location=url;
		}
	});
}

function ascheaction(assessid,scheid,operation)
{
	var baseurl=document.reclist.baseurl.value;
	SITEROOT=baseurl;
	if(operation==''){
		var i = document.getElementById("selaction").selectedIndex;
		operation=  document.getElementById("selaction").options[i].value;
	}

	var aschecheck=0;
	var checkedgroupval=document.reclist.aschecheck;
	if(operation=='action'){
		
		alertify.alert('Please Select Action to apply on Assessment');
		return false;
	}
	if(operation!='delsche'){
		if(checkedgroupval.length==undefined)
		{
			aschecheck=checkedgroupval.value;
		}
		var option='';
		var scheidstat='';
		var asch;
		var stid,excloseid;
		for(var l=0;l<checkedgroupval.length; l++){
			var invalid =0;
			if(checkedgroupval[l].checked==true)
			{
				scheidstat=checkedgroupval[l].value.split('||');
				asch=scheidstat[0];
				stid=scheidstat[1];
				
				if(operation=='delete')
					$('#scherow_'+asch).hide();

				if(operation=='draft' )
				{
					option='Draft';

				}
				if(operation=='closed' )
				{
					option='Closed';
						$('#showprop_'+asch).hide();
				}
				if(operation=='open' )
				{
					option='Open';
				}
				if(stid==2){

					if(operation=='open' || operation=='draft'){
						invalid=1;
						//alert("Closed Status can't be changed to Open or Draft");
						asch='';
					}
				}
				
				if(option!='' && invalid==0){
					var new_div1 = document.getElementById("showstatus_"+asch);
					new_div1.innerHTML = option;
				}
				if(aschecheck!=0)
				{
					if(asch!='')
						aschecheck = aschecheck+","+asch;
				}
				else
				{
					if(asch!='')
						aschecheck=asch;
				}
				
			}
		}
		if(aschecheck==0)
		{
			alertify.alert('Please Select Assessment');
			return false;
		}
		else{
			scheid=aschecheck;
		}

	}
	var url=SITEROOT+'/assessment/reclist'+'/assessid/'+assessid+'/scheid/'+scheid+'/mode/'+operation;
	$operationforshowandhide=operation;
	//alert(url);
	if(operation=='delsche'){
		$('#scherow_'+scheid).hide();
	}

	createXMLHttpRequest();
	var strURL=url;
	var query ="";

	if (xmlHttpRequest != null) {
		xmlHttpRequest.open("post", strURL, true);
		xmlHttpRequest.onreadystatechange = handleassessmentcopy;
		xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlHttpRequest.send(query);

	}
}
function handleassessmentcopy()
{
	if (xmlHttpRequest.readyState == 4)
	{
		if (xmlHttpRequest.status == 200) {
			var xmlDoc=xmlHttpRequest.responseText;
			  var url=SITEROOT+'/assessment/assessmenttools';
		window.location=url;
		}
	}
}

function filterasessementsched(operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var hideinact=new Array();
	var showact=new Array();
	var myval='';
	var inactlen='';
	var actlen='';
	var i='';
	//	alert(operation);
	var closesche=document.getElementById('closesche').value;
	var draftsche=document.getElementById('draftsche').value;
	var opensche=document.getElementById('opensche').value;
	if(operation=='open'){
		if(opensche!='')
			showact=opensche.split(',');
		if(draftsche!='')
			hideinact=draftsche.split(',');
		if(closesche!='')
			hideinact=hideinact+closesche.split(',');
	}


	else if(operation=='alllist'){
		if(closesche!='')
			showact=closesche.split(',');
		if(draftsche!='')
			showact=showact+draftsche.split(',');
		if(opensche!='')
			showact=showact+opensche.split(',');
		showact=showact.split(',');
	
	}
	else if(operation=='draft'){
		if(draftsche!='')
			showact=draftsche.split(',');
		if(closesche!='')
			hideinact=closesche.split(',');
		if(opensche!='')
			hideinact=hideinact+opensche.split(',');
	}

	else
	{
		if(operation=='closed'){
			if(closesche!='')
				showact=closesche.split(',');
			if(draftsche!='')
				hideinact=draftsche.split(',');
			if(opensche!=''){
				hideinact=hideinact+opensche.split(',');
			}
		//alert(hideinact);
		}
	}
	actlen=showact.length;
	if(actlen>0){
		for(i=0;i<=actlen-1;i++){
			myval=showact[i];
			//	alert(myval);
			$('#scherow_'+myval).show();
		}
	}
	//alert(hideinact);
	
	var HideArr=new Array();
	HideArr=hideinact.split(',');
	//alert(HideArr);
	inactlen=HideArr.length;
	
	if(inactlen>0){
		for(i=0;i<=inactlen-1;i++){
			myval=HideArr[i];
			//	alert(myval);
			$('#scherow_'+myval).hide();
		}
	}
	
	
}
function cmpDatethree(dat1, dat2,dat3) {
	// define local variables
	var day, mSec1, mSec2,mSec3;
	// number of milliseconds in one day
	day = 1000 * 60 * 60 * 24;
	// define number of milliseconds for both dates
	mSec1 = (new Date(dat1.substring(6, 10), dat1.substring(3, 5) - 1, dat1.substring(0, 2))).getTime();
	mSec2 = (new Date(dat2.substring(6, 10), dat2.substring(3, 5) - 1, dat2.substring(0, 2))).getTime();
	mSec3 = (new Date(dat3.substring(6, 10), dat3.substring(3, 5) - 1, dat3.substring(0, 2))).getTime();
	// return number of days between dat1 and dat2
	var from=Math.ceil((mSec2-mSec3) / day);
	var to=Math.ceil((mSec3-mSec1 ) / day);
	//	alert(from);
	if(from>=0 && to>=0)
		return 1;
//alert(to);
//return Math.ceil((mSec2 - mSec1) / day);

}
function aschefilterstend(operation)
{
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var sdate=document.reclist.sdate.value;
	var edate=document.reclist.edate.value;
	var recdatearr=document.reclist.recdatearr.value;
	var i;
	var spl=new Array();
	spl=recdatearr.split(',');
	var dateasche=new Array();
	var asche;
	var recdate;
	var fun;
	var dmyrecdate;
	var dmysdate,dmyedate;
	var sdatedmy,recdatedmy,edatedmy;
	//alert(sdate);
	dmysdate=sdate.split('/');
	dmyedate=edate.split('/');
	sdatedmy=dmysdate[1]+"-"+dmysdate[0]+"-"+dmysdate[2];
	edatedmy=dmyedate[1]+"-"+dmyedate[0]+"-"+dmyedate[2];
	if (cmpDate(sdatedmy,edatedmy,recdatedmy) < 0) {
		
		alertify.alert('Start Date should not preceed End Date');
		return false;
	}
	for(i=0;i<=spl.length-1;i++)
	{
		if(spl[i]!=''){
			dateasche= spl[i].split('||');
			asche=dateasche[0];
			recdate=dateasche[1];
			dmyrecdate=recdate.split('-');
			recdatedmy=dmyrecdate[2]+"-"+dmyrecdate[1]+"-"+dmyrecdate[0];
			if (cmpDatethree(sdatedmy,edatedmy,recdatedmy) > 0) {
				$('#scherow_'+asche).show();
			}
			else
				$('#scherow_'+asche).hide();

		}
	}
}
function delchoices(i,operation) {
	
	var baseurl=document.f1.baseurl.value;
	SITEROOT=baseurl;
	var assessid=document.f1.assessid.value;
	var scheid=document.f1.scheid.value;
	if(operation=='delansfromadd'){
				$('#delchoice_'+asche).hide();
		}
	
}
//for removeing extra answer div--  haider
function removeansdiv(id) 
{

$("#div_"+id).fadeOut(300, function(){ $(this).remove();});
}
// for deleting answer haider
function delans(ansid)
{
$.post(SITEROOT+'/assessment/addassessment/mode/deleteanswer',{ id: ansid},
	function(response)
	{
       removeansdiv(ansid);      
	});
}

function reload_page(response)
{
	if(response == 'ok')
	{
		window.location.reload();
	}
}