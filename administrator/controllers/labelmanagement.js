var labelmanagementmodule = angular.module("labelmanagementmodule",['datatables', 'ngResource','Alertify']);

labelmanagementmodule.controller('LabelMangementcontroller', function($scope,$location,$localStorage,$http,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$compile,$rootScope,$window){
	
	
});

labelmanagementmodule.controller('AddLabelcontroller', function($scope,$location,$localStorage,$http,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$compile,$rootScope,$window,Alertify){
	
	         $scope.language='en';
	         $scope.getAddLabelInfo=function(){
				 $http({
					 method:"GET",
					 url:"/api/getpagewithlable"
				 }).success(function(response){
					 $scope.allLabelReal=response;
					 $scope.allLables=response;
				 })
			 }
			 
			 $scope.getAddLabelInfo();
			 
			 
			 $scope.saveLabel=function(){
				 if(!$scope.language){
					 Alertify.alert('Please select Language.');
				 }else{
					 $http({
						 method:"POST",
						 url:"/api/saveLabel",
						 data:{langId:$scope.language,'label':$scope.allLables}
					 }).success(function(response){
						 Alertify.success('Label addedd successfully');
					 })
					 
				 }
			 }
			 
			 $scope.changeLang=function(){
				 
			 }
	
});