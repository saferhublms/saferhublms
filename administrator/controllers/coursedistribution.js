var coursedistributionmodule = angular.module("coursedistributionmodule",[]);
	
coursedistributionmodule.controller('CoursedistributionController', function($scope,$location,$localStorage,$http,$stateParams,Alertify,DTOptionsBuilder,DTColumnBuilder,$compile){
	
    $scope.trainingCode=$stateParams.courseCode;
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.dtInstance={};
	$scope.dtInstance1={};
	
	$scope.getcourseditributiondataData=function(){
		$http({
			method:"POST",
			url:"/admin/getcoursedistributionDataAction",
			data:{'trainingCode':$scope.trainingCode}
		}).success(function(response){
			$scope.allDistributionUserAndGroup=response;
		})
	}
	
	$scope.getcourseditributiondataData();
		
   $scope.usergrid=function()
	{	
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('ajax', {
			 url: '/admin/getAssignedUsers',
			 type: 'POST',
			  data:{_token:$scope.tokenuse,'trainingCode':$scope.trainingCode},
		 }).withDataProp(function(json) {
			 json.recordsTotal = json.count;
			 json.recordsFiltered = json.count;
			 json.draw = json.draw;
			 $scope.courseData=json.data;
			 console.log($scope.courseData);
			 return $scope.courseData;
		})
        .withOption('processing', true)
        .withOption('serverSide', true)
		.withOption('lengthChange', false)
		.withOption('searching', false)
		.withOption('createdRow', function (row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		})
        .withPaginationType('full_numbers');
		$scope.dtColumns = [
			DTColumnBuilder.newColumn('user_name').notSortable(),
			DTColumnBuilder.newColumn(null).withTitle('Assign Type').renderWith(function(data) {
				if(data.is_mandatory==1){
					return 'Mandatory'
				}else{
					return 'Optional'
				}
				
				
			}).notSortable(),
			DTColumnBuilder.newColumn('due_date').notSortable(), 
			DTColumnBuilder.newColumn('created_date').notSortable(),
		
		 DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
         .renderWith(actionsHtml)
		];
		
		function actionsHtml(data, type, full, meta) {
	
			 return '<span id="grp_4779" class=" fl ml01 view "><a href="" ng-click="removeAssignUser('+data.course_users_id+')" class="removeGroup button_lblue_r4" name="4779">Remove<span>»</span></a> </span>';
        }
	}
	
	$scope.usergrid();
	
	$scope.groupgrid=function()
	{
		$scope.dtOptions1 = DTOptionsBuilder.newOptions()
			.withOption('ajax', {
			 url: '/admin/getAssignedGroups',
			 type: 'POST',
			  data:{_token:$scope.tokenuse,'trainingCode':$scope.trainingCode},
		 }).withDataProp(function(json) {
			 json.recordsTotal = json.count;
			 json.recordsFiltered = json.count;
			 json.draw = json.draw;
			 $scope.courseData=json.data;
			//console.log($scope.courseData);
			 return $scope.courseData;
		})
        .withOption('processing', true)
        .withOption('serverSide', true)
		.withOption('lengthChange', false)
		.withOption('searching', false)
		.withOption('createdRow', function (row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		})
        .withPaginationType('full_numbers');
		$scope.dtColumns1 = [
			DTColumnBuilder.newColumn('group_name').notSortable(),
			DTColumnBuilder.newColumn(null).withTitle('Assign Type').renderWith(function(data) {
				if(data.is_mandatory==1){
					return 'Mandatory'
				}else{
					return 'Optional'
				}
				
				
			}).notSortable(),
			DTColumnBuilder.newColumn('due_date').notSortable(), 
			DTColumnBuilder.newColumn('created_date').notSortable(),		
			DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
            .renderWith(actionsHtml1)
		];
		
		 function actionsHtml1(data, type, full, meta) {
			 return '<span id="grp_4779" class=" fl ml01 view "><a href="" ng-click="removeAssignGroup('+data.course_groups_id+')" class="removeGroup button_lblue_r4" name="4779">Remove<span>»</span></a> </span>';
        }
	}
	
	$scope.groupgrid();
	
	
		$scope.multiple={};
		$scope.multiple.selectedGroup=[];
		$scope.multiple.selectedUserNameOptional=[];
		$scope.multiple.selectedUserName=[];
		$scope.multiple.selectedGroupOptional=[];
		$scope.assign=function(){
			
			//return false;
			if(($scope.multiple.selectedGroup.length==0) && ($scope.multiple.selectedUserName.length==0) && ($scope.multiple.selectedUserNameOptional.length==0) && ($scope.multiple.selectedGroupOptional.length==0)){
				Alertify.alert('Please select any user or groups');
				return false;
			}
			if(!$scope.dueDate){
				Alertify.alert('Please select due date');
				return false;
			}
			$http({
				method:"POST",
				url:"/admin/assignCourseToUserAndGroup",
				data:{'trainingCode':$scope.trainingCode,groups:$scope.multiple.selectedGroup,users:$scope.multiple.selectedUserName,due_date:$scope.dueDate,'optionalGroup':$scope.multiple.selectedGroupOptional,'optionalUser':$scope.multiple.selectedUserNameOptional}
			}).success(function(response){
				$scope.dtInstance._renderer.rerender(); 
				$scope.dtInstance1._renderer.rerender(); 
				$scope.multiple.selectedGroup=[];
		        $scope.multiple.selectedUserName=[];
				$scope.getcourseditributiondataData();
				Alertify.alert(response.message);		
			})   
		}
		
		$scope.removeAssignUser=function(courseUserId){
			 Alertify.confirm('Are you sure you want to remove this user?').then(
			function onOk() {
					$http({
					method:"POST",
					url:"/admin/unassignuser",
					data:{assignedUserId:courseUserId}
				}).success(function(response){
					Alertify.success('User removed successfully');
					$scope.dtInstance._renderer.rerender(); 
					$scope.getcourseditributiondataData();
				})
		  });
		}
		
		$scope.removeAssignGroup=function(courseGroupId){
			Alertify.confirm('Are you sure you want to remove this group?').then(
			function onOk() {
					$http({
					 method:"POST",
					 url:"/admin/unassigngroup",
					 data:{assignedGroupId:courseGroupId}
				}).success(function(response){
					Alertify.success('Group removed successfully');
					$scope.dtInstance1._renderer.rerender(); 
					$scope.getcourseditributiondataData();
				});
		  });	
		}
		
});	
