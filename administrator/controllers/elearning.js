var elearningModule=angular.module('elearningModule',[]);

elearningModule.controller('elearningListController',function($scope,DTOptionsBuilder,$http,$resource,DTColumnBuilder,$q){
	
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.init=function()
	{
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('ajax', {
			 url: '/admin/elearning-courses/elearningList',
			 type: 'POST',
			  data:{_token:$scope.tokenuse},
		 }).withDataProp(function(json) {
			 json.recordsTotal = json.count;
			 json.recordsFiltered = json.count;
			 json.draw = json.draw;
			 $scope.courseData=json.data;
			
			 return json.data;
		})
        .withOption('processing', true)
        .withOption('serverSide', true)
		
        .withPaginationType('full_numbers');
		
		
		
		$scope.dtColumns = [
			DTColumnBuilder.newColumn('course_title').withTitle('Course Title'),
			DTColumnBuilder.newColumn('training_program_code').withTitle('Course ID').notSortable(),
			DTColumnBuilder.newColumn('media_name').withTitle('Media Type').notSortable(),
			DTColumnBuilder.newColumn('categoryName').withTitle('Category Name').notSortable(),
			
			DTColumnBuilder.newColumn(null).withTitle('Status').notSortable()
            .renderWith(function(data) {
				if(data.status_id==0){
					 return '<span>Drafted</span>'
				}else if(data.status_id==1){
					 return '<span>Published</span>'
				}else if(data.status_id==2){
					return '<span>Save</span>'
				}
             
            }),
			DTColumnBuilder.newColumn(null).withTitle('Prview').notSortable()
            .renderWith(function(data) {
				return '<span class="view padd_table hand"><a class="thickbox btn1 btn-primary" "><font color="#fff"><i class="fa fa-eye"></i> View</font></a></span>';
			}),
		];
		
	}
	$scope.init();
	
		
	
});


elearningModule.controller('EditElearningController',function($scope,$http,$stateParams,$timeout,$rootScope,Alertify,FileUploader){
	
			$scope.disabled = undefined;
	
	         $scope.multiple={
				 SelectedCategoryName:[],
				 selectedGroup:[]
			 };
			 
			  var HandOut = $scope.HandOut = new FileUploader({
			   url:'/admin/uploadHandout',
			   alias:'handoutFile',
			});
			
		  console.info('uploader', HandOut);
		  HandOut.onAfterAddingFile = function(fileItem) {
				console.info('onAfterAddingFile',fileItem);
			};

       $scope.removeHandoutQueue=function(index){
			HandOut.queue.splice(index,1);
		}
			 
			 $scope.selectedTab=0;
			 $scope.trainingCode=$stateParams.courseCode;
			 $scope.tagArray=[{
				 course_tag_id:'',
				tags:''
			 }]
			 
			 $scope.tagTransformCategories = function (newTag) {
				var item = {
				category_name: newTag, 
				training_category_id: newTag, 
				};

				return item;
			}; 
			
			$scope.tagTransformTags = function (newTag) {
				var item = {
					
					course_tag_id: newTag, 
					tags: newTag, 
				};

				return item;
			};
			//end
			
			var uploader = $scope.uploader = new FileUploader({
			   url:'/admin/uploadfile',
			   alias:'courseFile',
			});
			
			
			// FILTERS

		 /* uploader.filters.push({
				name: 'imageFilter',
				fn: function(item, options) {
					
					var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
					console.log(type);
					return $scope.canUploadMedia.indexOf(type) !== -1;
				}
			});

 */
		   
		
			// CALLBACKS

		   
			uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
				Alertify.alert('Please upload only '+$scope.uploadType+' files');
				console.info('onWhenAddingFileFailed', item, filter, options);
			};
			uploader.onAfterAddingFile = function(fileItem) {
				
				 if (uploader.getNotUploadedItems().length > 1) {
					uploader.removeFromQueue(0);
				  }
				console.info('onAfterAddingFile', fileItem);
			};
			
			
			$scope.courseMediaIdArray=[];
			uploader.onCompleteItem = function(fileItem, response, status, headers) {
				$scope.courseMediaIdArray.push(response.uploadedId);
				console.info('onCompleteItem', fileItem, response, status, headers);
				console.info($scope.courseMediaIdArray);
				
			};
			uploader.onProgressAll = function(progress) {
			  $scope.selectedTab=2;
			};
		


			console.log(uploader);
			
		
		
			 $scope.getcoursedetail=function()
			 {
				 $http({
					 method:"POST",
					 url:"/admin/getelearningdata",
					 data:{code:$scope.trainingCode}
				 }).success(function(response){
					  $scope.elearningData=response.data;
					  $scope.courseImageData=$scope.elearningData.course_image;
					  $scope.multiple.SelectedCategoryName=response.data.couseCategory;
					  $scope.multiple.selectedGroup=response.data.groupAudienceTag;
					  $scope.categories=$rootScope.audioVideoData.traningcategoryType;
					  $scope.userAllSkills= $scope.elearningData.courseSkill;
					  $scope.i=$scope.userAllSkills.length;
					 /*  $timeout(function(){
						 tinymce.get('desc_text').setContent($scope.elearningData.course_description);
					 },1000) */
					  
					// alert(JSON.stringify(response));
				 })
			 }
			 
			 
			 $scope.getcoursedetail();
		
			 $scope.courseImageFunc = function(element) {
				var reader = new FileReader();
				reader.onload = function(e){
					$scope.$apply(function(){
						$scope.courseImageData = e.target.result;
						$scope.theFile = element.files[0];
					});
				}
				reader.readAsDataURL(element.files[0]);
				
			};
		
			 
			 
			 $scope.addCourseFirstData=function(){
				 if(!$scope.elearningData.course_name){
					 Alertify.alert("Course Name is required.");		
				 }else if($scope.multiple.SelectedCategoryName.length<1){
					 Alertify.alert("Training category is required.");
				 }else if(!$scope.elearningData.media_type_id){
				     Alertify.alert("Please select training content.");
				 }else{
					 if($scope.elearningData.media_type_id==11 || $scope.elearningData.media_type_id==3){
							$scope.canUploadMedia='|zip|||';
							$scope.uploadType='zip,rar';
						}else if($scope.elearningData.media_type_id==2){
							$scope.canUploadMedia='|mp3|x-ms-wma|aac|m4a|vnd.dlna.adts|x-m4a|';
							$scope.uploadType='mp3,Wma,aac,m4a';
						}else if($scope.elearningData.media_type_id==8){
							$scope.canUploadMedia='|doc|docx|ppt|pptx|xls|xlsx|pdf|vnd.openxmlformats-officedocument.presentationml.presentation|vnd.openxmlformats-officedocument.wordprocessingml.document|||vnd.ms-excel|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.spreadsheetml.template|';
							$scope.uploadType='doc,pdf,pptx,xls';
						}else if($scope.elearningData.media_type_id==4){
							$scope.canUploadMedia='|doc|docx|pdf|xlsx|xls|ppt|pptx|vnd.openxmlformats-officedocument.presentationml.presentation|vnd.openxmlformats-officedocument.wordprocessingml.document|||vnd.ms-excel|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.spreadsheetml.template|';
							$scope.uploadType='doc,pdf,ppt,xls';
						}else if($scope.elearningData.media_type_id==7){
							$scope.canUploadMedia='|ppt|pptx|doc|docx|pdf|vnd.openxmlformats-officedocument.presentationml.presentation|vnd.openxmlformats-officedocument.wordprocessingml.document|||vnd.ms-excel|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.spreadsheetml.template|';
							$scope.uploadType='doc,pdf,ppt,xls';
						}else if($scope.elearningData.media_type_id==1){
							$scope.canUploadMedia='|mp4|m4v|flv|x-ms-wmv|mov|swf|';
							$scope.uploadType='mp4,wmv,m4v,flv';
						}
						
					 $scope.selectedTab=1;
				 }
			 }
			 
			 $scope.previousFirst=function(){
				 $scope.selectedTab=0;
			 }
			 
			 
			 $scope.nextFile=function(){
				if($scope.elearningData.media_type_id!=10){
					if(uploader.queue.length>0){
						$scope.uploader.onBeforeUploadItem = function(item) {
								item.formData.push({media_type:$scope.elearningData.media_type_id});	
						};

						$scope.uploader.uploadAll(); 
						
					}else{
						$scope.selectedTab=2;
					}
				}else{
					
					if($scope.embededcode==undefined || !$scope.embededcode){
						 $scope.selectedTab=2;
						}else{
						$http({
							method:"POST",
							url:"/admin/uploadfile",
							data:{embededcode:$scope.embededcode,media_type:10}
						}).success(function(response){
							$scope.courseMediaIdArray.push(response.uploadedId);
							 $scope.selectedTab=2;
						})
					}
				 }
		}
		
		$scope.saveEmbededCode=function(){
			$scope.embededCodeData=tinyMCE.get('descem').getContent();
			if(!$scope.embededcodeTitle || $scope.embededcodeTitle==undefined){
				Alertify.alert('Please enter title.');
			}else if(!$scope.embededCodeData || $scope.embededCodeData==undefined){
				Alertify.alert('Please Enter Embedded Code.');
			}else{
				
				$http({
					method:"POST",
					url:"/admin/saveembededcode",
					data:{embededTitle:$scope.embededcodeTitle,embededCode:$scope.embededCodeData,embededCodefileId:$scope.embededCodefileId,mediaType:$scope.elearningData.media_type_id}
				}).success(function(response){
					if($scope.embededCodefileId==''){
						$scope.embededCodefileId=response.uploadedId;
						$scope.courseMediaIdArray.push(response.uploadedId);
						 $scope.selectedTab=2;
					}else{
						 $scope.selectedTab=2;
					}
				})
			}
		}
		
		$scope.userSkillLevel=function(index){
			$scope.userAllSkills[index].level_id='';
			$scope.userAllSkills[index].skill_level_credit='';
			$scope.userAllSkills[index].course_credit='';
			
		}
		
		$scope.addSkillLevel=function(index){
			$scope.couseSkillId=$scope.userAllSkills[index].skill_id;
			$scope.skillLevelId=$scope.userAllSkills[index].level_id;
			$scope.userAllSkills[index].skillLevels=$rootScope.audioVideoData.skillarray[$scope.couseSkillId].levels;
			angular.forEach($scope.userAllSkills[index].skillLevels,function(levels){
				if(levels.level_id==$scope.skillLevelId){
					$scope.userAllSkills[index].skill_level_credit=levels.skill_credit;
				}
			})	
		}
		
		$scope.checkCredit=function(index){
			if($scope.userAllSkills[index].skill_level_credit<$scope.userAllSkills[index].course_credit){
				Alertify.alert("Credit Value must be less than or equel to max credit value");	
				$scope.userAllSkills[index].course_credit='';
			}
		}
		
		$scope.deletelevel=function(userSkill){
			var index = $scope.userAllSkills.indexOf(userSkill);
            $scope.userAllSkills.splice(index, 1);
			$scope.i--;
			
		}
		
		$scope.addMoreSkill=function(){
			if($scope.i<3){
				$scope.i++;
				$scope.userAllSkills.push({'skill_id':'','level_id':'','skill_credit':'','course_credit':''});	
			}else{
				Alertify.alert("You can add only three skills.");	
			}
		}
		
		
		$scope.classroomCourseData=function(){
			$http({
				method:"POST",
				url:"/admin/getClassRoomCourseData"
			}).success(function(response){
				$scope.classroomAddData=response;
				//alert(JSON.stringify(response));
			})
		}
		$scope.classroomCourseData();
		
		
		$scope.previousThirdPage=function(){
			$scope.selectedTab=1;
		}
		
		
		$scope.finalSubmit=function(){
		 
			 if(!$scope.elearningData.course_name){
					 Alertify.alert("Course Name is required.");		
				 }else if($scope.multiple.SelectedCategoryName.length<1){
					 Alertify.alert("Training category is required.");
				 }else if(!$scope.elearningData.media_type_id){
				     Alertify.alert("Please select training content.");
				 }else{
					 $scope.loader=true;
					 var course_pic_data=courseImage.files.item(0);
						if(course_pic_data){
							var isCourseImage=1;
						}else{
							var isCourseImage=0;
						}
						
						$scope.checkNotification=[];
						angular.forEach($scope.elearningData.notification,function(list){
							if(list.checkNotification==1){
								$scope.checkNotification.push(list.notification_id);
							}
						})
						$scope.mediaLibraryId='';
						if($scope.courseMediaIdArray.length>0){
							$scope.mediaLibraryId=$scope.courseMediaIdArray[0];
						}
						
						if(!$scope.mediaLibraryId){
							$scope.mediaLibraryId=$scope.elearningData.media_library_id;
						}
						var course={
							'credit_value':$scope.elearningData.credit_value,
							'points':$scope.elearningData.points,
							'media_library_id':$scope.mediaLibraryId,
							'status_id':1,
							'certificate_template_id':$scope.elearningData.certificate_template_id
							};
							
						var courseDetail={
							'course_name':$scope.elearningData.course_name,
							'course_title':$scope.elearningData.course_title,
							'course_description':$scope.elearningData.course_description
						};
						
						var trainingPrograms={
							'media_type_id':$scope.elearningData.media_type_id,
							'status_id':$scope.elearningData.status_id
						};
						if($scope.showNotification==0){
							$scope.checkNotification=[];
						}
						
						$http({
							method:"POST",
							url:"/admin/editCourse",
							
							headers: {'Content-Type': undefined },
							transformRequest:function(){
								var formData=new FormData();
								formData.append('code',$scope.trainingCode);
								formData.append('course',angular.toJson(course));
								formData.append('courseDetail', angular.toJson(courseDetail));
								formData.append('trainingPrograms',angular.toJson(trainingPrograms));
								formData.append('isCourseImage',isCourseImage);
								formData.append('userAllSkills',angular.toJson($scope.userAllSkills));
								formData.append('categories',angular.toJson($scope.multiple.SelectedCategoryName));
								formData.append('checkNotification',angular.toJson($scope.checkNotification));
								formData.append('courseImage',course_pic_data);
								formData.append('targetAudienceGroups',angular.toJson($scope.elearningData.groupAudienceTag));
								formData.append('targetAudienceusers',angular.toJson($scope.elearningData.userAudienceTag));
								formData.append('permissionGroups',angular.toJson($scope.elearningData.groupEditTag));
								formData.append('permissionUsers',angular.toJson($scope.elearningData.userEditTag));
								formData.append('courseTags',angular.toJson($scope.elearningData.courseTags));
								return formData;
							}
						}).success(function(response){
							$scope.loader=false;
							$scope.HandOut.onBeforeUploadItem = function(item) {
								item.formData.push({courseId:$scope.elearningData.course_id});	
							};
							if(HandOut.queue.length>0){
								$scope.HandOut.uploadAll();
							}
					
							Alertify.alert('Your Elearning Course has been Edited Successfully');
							//alert(JSON.stringify(response));
						})
			
				 }
		}
		  
		
		$scope.showNotification=1;
		$scope.showNoticationOption=function(value){
			$scope.showNotification=value;
		}
		
		$scope.removeHandOut=function(handoutId,index){
			$http({
				method:"POST",
				url:"/admin/removeHandout",
				data:{handoutId:handoutId}
			}).success(function(response){
				$scope.elearningData.handouts.splice(index,1);
			})
			
		}
	
		
		
		
		
			 
			 
			 
			 
	        
});