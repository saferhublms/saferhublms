
var coursemodule = angular.module("coursemodule",[]);
/* coursemodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("coursesindex", {
		url:"/courses/index",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/courses/courselist.html',
				controller: 'CourseListController'
			}
		},
	});	
	$stateProvider.state("coursesactivity", {
		url:"/courses/coursesactivity",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/courses/courses.html',
				controller: 'CoursesactivityController'
			}
		},
	});
	
	$stateProvider.state("courseaddvideo", {
		url:"/courses/addvideo",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/courses/addvideo.html',
				controller: 'CoursesAddvideoController'
			}
		},
	});	
	
	$stateProvider.state("classroomcourse", {
		url:"/courses/classroomcourses",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/courses/classroomcourses.html',
				controller: 'ClassroomCoursesController'
			}
		},
	});	
	$stateProvider.state("assessment", {
		url:"/courses/assessment",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/courses/assessment.html',
				controller: 'AssessmentController'
			}
		},
	});	
	
}]);
	
*/

coursemodule.controller('CourseListController', function($window,$scope,$location,$localStorage,$http,$timeout,DTOptionsBuilder,DTColumnBuilder,$compile)
{
	
  $scope.coursesactivity=function(url){
	
	   $('#addtask').modal('hide');
	     //10 seconds delay
        $timeout( function(){
            $window.location=url;
        }, 300 );
	   return false;  
  }
  $scope.dtInstance = {};
$scope.grid=true;
$scope.selected = {};
$scope.selectedmain = {};
$scope.selectAll = false;
 $scope.tokenuse=$("input[name=_token]").val();
	$scope.init=function()
	{
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('ajax', {
			 url: '/admin/elearning-courses/elearningList',
			 type: 'POST',
			 
			  data:{_token:$scope.tokenuse,category:$scope.filterCategory,training:$scope.filterTraining},
		 }).withDataProp(function(json) {
			 json.recordsTotal = json.count;
			 json.recordsFiltered = json.count;
			 json.draw = json.draw;
			 $scope.courseData=json.data;
			 $scope.categoryName=json.CategoryName;
			 //$scope.trainingType=json.TrainingType;
			 //console.log($scope.courseData);
			 // console.log($scope.categoryName);
			 // console.log($scope.trainingType);			
			 return $scope.courseData;
			 // return $scope.categoryName;
			 // return $scope.trainingType;
		})
        .withOption('processing', true)
		.withOption('lengthChange', false)
        .withOption('serverSide', true)
		.withOption('createdRow', function (row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		}) 
		.withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
		
        .withPaginationType('full_numbers');

	 var titleHtml = '<div class="checkbox"><input type="checkbox" ng-model="selectAll" ng-click="checkCouresAll(selectAll, selectedmain)" ><label></label></div>';
	
		$scope.dtColumns = [
			
			DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
            .renderWith(function(data, type, full, meta) {
			    $scope.selectedmain[full.training_program_code] = false;
                return '<div class="checkbox"><input type="checkbox" ng-model="selectedmain[' + data.training_program_code + ']" ng-click="checkCouresOne(selectedmain)"><label></label></div>';
			   
            }),
			DTColumnBuilder.newColumn(null).withTitle('Course Name').renderWith(function(data) {
				if(data.course_title!=null){
					var name=data.course_name+" "+data.course_title
				}else{
					var name=data.course_name
				}
				
				return name;
			}),
			
			DTColumnBuilder.newColumn('training_program_code').withTitle('Course ID'),
			/* DTColumnBuilder.newColumn('media_name').withTitle('Media Type').notSortable(), */
			DTColumnBuilder.newColumn('training_type').withTitle('Training Type'), 
			DTColumnBuilder.newColumn('categoryName').withTitle('Category Name'),
			DTColumnBuilder.newColumn(null).withTitle('Status')
            .renderWith(function(data, type, full, meta) {

				if(data.status_id==0||data.status_id==1||data.status_id==2)
				{ 
					if(data.status_id==1)
					{
					$scope.selected[full.training_program_code] = true;
					}
					else
					{
					$scope.selected[full.training_program_code] = false;

					}
					
					return '<div switch ng-change="updateStatus(selected,' + data.training_program_code + ')" name="publishedUnpublished" ng-model="selected[' + data.training_program_code + ']" on="Published" off="Draft" class="wide"></div>'
					
					//return '<input type="checkbox" ng-model="selected[' + data.training_program_code + ']" ng-click="updateStatus(selected,' + data.training_program_code + ')">';
				}
             
            }),
			 DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withOption('className', 'text-right')
            .renderWith(actionsHtml)
			
			/*DTColumnBuilder.newColumn(null).withTitle('Prview').notSortable()
            .renderWith(function(data) {
				return '<span class="view padd_table hand"><a class="editButton btn btn-success" href="#/courses/editelearning/'+data.training_program_code+'" onfocus="if(this.blur)this.blur()"><font color="#fff"><i class="fa fa-pencil"></i> Edit</font></a></span><span class="fl view fs12 hand"><a class="thickbox btn1 btn-primary" ng-click="preview()" onfocus="if(this.blur)this.blur()"><font color="#fff"><i class="fa fa-eye"></i> View</font></a><font color="#fff"> </font></span>';
			}), */
			
			/* DTColumnBuilder.newColumn(null).withTitle('Properties').notSortable()
            .renderWith(function(data) {
				return '<span class="fl view fs12 hand"><a class="editButton btn btn-success" href="#/courses/editelearning/'+data.training_program_code+'" onfocus="if(this.blur)this.blur()"><font color="#fff"><i class="fa fa-pencil"></i> Edit</font></a><font color="#fff"> </font></span>';
			}),  */
		];
		//console.log($scope.dtColumns.data);
}
	$scope.init();

	$scope.updateStatus=function(selectedItems,tid)
	{
		$http({
					method:"POST",
					url:'/api/updateTrainingStatus',
					data:{items:selectedItems,tid:tid}
				}).success(function(response){
					//alert(response.success);
					
								
         });
	}
$scope.checkCouresAll=function(selectAll, selectedItems)
	{ 
		$scope.newarray=[];
	    for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
				$scope.newarray.push({key:id});	
            }
        }
				
	}
	
	$scope.checkCouresOne=function(selectedItems)
	{ 
		console.log(selectedItems);
		$scope.newarray=[];
		angular.forEach(selectedItems, function (value,key) {
			if(selectedItems[key]) {
                    $scope.newarray.push({key});
             } 
		});
        $scope.selectAll = false;

	}
	$scope.bulkStatus="2";

		$scope.bulkoeration=function()
	{ 
			if($scope.newarray!=null)
		{
				if($scope.bulkStatus!=2)
				{
					$http({
							method:"POST",
							url:'/api/updateMultipleTrainingStatus',
							data:{data:$scope.newarray,value:$scope.bulkStatus}
							}).success(function(response){
							//alert(response.message);
							$scope.dtInstance._renderer.rerender(); 
							$scope.bulkStatus="2";
        			 });
				}
				else
				{
					alert('Please Choose Any Options');
				}
		}
		else
		{
			alert('Please Check To update Status');
		}		
		
	}
		$scope.categoryFilter=function(categoryName)
		{
			$scope.filterCategory=categoryName;
			$scope.init();
		}
	$scope.trainingFilter=function(trainingType)
		{
			$scope.filterTraining=trainingType;
			$scope.init();

		}


	function actionsHtml(data, type, full, meta) {
		if(data.trainingTypeId==1){
			return '<div class="btn-group"><a class="btn-white btn" onfocus="if(this.blur)this.blur()" ng-click="preview('+data.training_program_code+')"><i class="fa fa-eye"></i></a><a class="btn-white btn" ng-click="editCourse('+data.training_program_code+','+data.trainingTypeId+')"><i class="fa fa-pencil"></i></a><a ng-click="deleteCourse('+data.training_program_code+')" class="btn-white btn" onfocus="if(this.blur)this.blur()"><i class="fa fa-trash"></i></a><span class="settingsicon_btn" settings></span></div>';
			
			//return '<span  class="fl view fs12 hand" ng-click="preview('+data.training_program_code+')"><a class="thickbox btn1 btn-primary"  onfocus="if(this.blur)this.blur()"><font color="#fff"><i class="fa fa-eye"></i> View</font></a><font color="#fff"> </font></span><span class="fl ml10 view fs12" ng-click="distribute('+data.training_program_code+')" role="button" tabindex="0"><a class="thickbox btn1 btn-primary" onfocus="if(this.blur)this.blur()" style="font-size: 12px;"><font color="#fff"><i class="fa fa-eye"></i> Course Distribute</font></a><font color="#fff"> </font></span><span class="view padd_table hand"><a ng-click="deleteCourse('+data.training_program_code+')" class="editButton btn btn-danger"  onfocus="if(this.blur)this.blur()"><font color="#fff"> Delete</font></a></span><span class="hand fl ml10 view fs12"><a class="delete_course btn btn-success" ng-click="editCourse('+data.training_program_code+','+data.trainingTypeId+')"><font color="#fff"><i class="fa fa-pencil"></i> Edit</font></a></span>';
		}else{
			 return '<div class="btn-group"><a class="btn-white btn" onfocus="if(this.blur)this.blur()" ng-click="preview('+data.training_program_code+')"><i class="fa fa-eye"></i></a><a class="btn-white btn" ng-click="editCourse('+data.training_program_code+','+data.trainingTypeId+')"><i class="fa fa-pencil"></i></a><a ng-click="deleteCourse('+data.training_program_code+')" class="btn-white btn" onfocus="if(this.blur)this.blur()"><i class="fa fa-trash"></i></a><span settings></span></div>';
			 
			// return '<span class="view padd_table hand"><a ng-click="deleteCourse('+data.training_program_code+')" class="editButton btn btn-danger"  onfocus="if(this.blur)this.blur()"><font color="#fff"> Delete</font></a></span><span class="hand fl ml10 view fs12"><a class="delete_course btn btn-success" ng-click="editCourse('+data.training_program_code+','+data.trainingTypeId+')"><font color="#fff"><i class="fa fa-pencil"></i> Edit</font></a></span>';
		}
       
    }
	//"href=
	
	$scope.showGrid=function(){
		$scope.list=false;
		$scope.grid=true;
	}
	
	$scope.showList=function(){
		$scope.list=true;
		$scope.grid=false;
	}
	
	$scope.editCourse=function(courseCode,trainingType){
		if(trainingType==1){
			$window.location.href='#/courses/editelearning/'+courseCode;
		}else if(trainingType==2){
			$window.location.href='#/courses/editclassroomcourse/'+courseCode;
		}else if(trainingType==5){
			$window.location.href='#/courses/editassessment/'+courseCode;
		}
	}
	
	$scope.preview=function(courseCode){
		$http({
			method:"POST",
			url:"/admin/getpreviewdata",
			data:{code:courseCode}
		}).success(function(response){
			$scope.previewData=response.courseData;
			$scope.enrollelearning=response.enrollelearning;
			$('#previewPopup').modal('show');
			
		})
	}
	
	$scope.distribute=function(courseCode){
		$window.location.href='#/courses/coursedistribtion/'+courseCode;
	}
	
	$scope.callfunction=function(average_rating){

		var strhtml='';
		for($i=1; $i<=average_rating; $i++) {
					strhtml+='<i class="fa fa-star"></i>';
		 }
		for($j=average_rating;$j<5;$j++){
				strhtml+='<i class="fa fa-star-o"></i>';
		}
		return strhtml;
	}
	
	$scope.show_media_player=function(){
		  $scope.enrollelearningfn();
	}
	
	$scope.enrollelearningfn = function(){
			$scope.media_id=$scope.previewData.fileDetails.media_id;
			if(!$scope.media_id){
				Alertify.alert('Media file does not exist.');
				return false;
			}
			 $http({
					method:"POST",
					url:'/api/enrollelearning',
					data:{_token:$scope.tokenuse,'enrollelearning_data':$scope.enrollelearning,'tview':'view'}
				}).success(function(response){
					$('#previewPopup').modal('hide');
					$scope.launch_player(response);			
         });
	}
	
	$scope.launch_player=function(response){
		if(response.mode=='tb_show')
		{			
					var completepath='/'+response.param+'?height=600&width=900&inlineId=hiddenModalContent&modal=true';
					
					tb_show(title='', completepath,'')
					/* tb_show(title='', completepath,'');	
	               $window.location.href = '#/'+response.param;
		           return false; */
		}
		else if(response.mode=='scorm'){
				if(response.status==""){
					if(response.param!=""){
						$window.location = '/'+response.param+'/viewId/'+$stateParams.code;
						/* var completepath='/'+response.param+'/viewId/'+$stateParams.code+'?height=600&width=900&inlineId=hiddenModalContent&modal=true';
					
					     tb_show(title='', completepath,'') */
					}
				}
		}
	}
	
	$scope.deleteCourse=function(courseCode){
		
		$http({
			method:"POST",
			url:"/admin/deletecourse",
			data:{courseCode:courseCode}
		}).success(function(response){
			//$scope.courseData.splice(1,1);
		    $scope.dtInstance._renderer.rerender(); 
			//alert(JSON.stringify(response));
		})
	}
	
	 
	
	    
});

coursemodule.controller('CoursesController', function($scope,$location,$localStorage,$http){      
	    
});	
/* 
coursemodule.controller('AssessmentController', function($scope,$location,$localStorage,$http){
  $scope.answerList=['1'];
  $scope.questionData=['1'];
  $scope.quest=1;
  $scope.answerCount=1;
$scope.add_fields=function(){
	$scope.answerCount++
	$scope.answerList.push($scope.answerCount);
}
  $scope.deleteAns=function(index){
	  $scope.answerList.splice(index,1);
	    
  }
  $scope.deleteAns=function(index){
	  $scope.questionData.splice(index,1);
	    
  }
  
  $scope.addQuestion=function(){
	 
	 $scope.quest++;
	  $scope.questionData.push($scope.quest);
	  
  }
  angular.module('tabsDemoDynamicHeight', ['ngMaterial']);
  
	    
});	 */
coursemodule.controller('CoursesAddvideoController', function($scope,$location,$localStorage,$http,FileUploader,Alertify){
	$scope.media_type=11;
	$scope.disabled = undefined;
	$scope.multiple={
		selectedCategoryName:[]
	}
	
	
		
       var uploaderVideo = $scope.uploader = new FileUploader({
           url:'/addproductuserguid',
		   alias:'product_user_guid',
        });

        // FILTERS
          $scope.supportFileType='|zip|rar|';
          uploaderVideo.filters.push({
            name: 'imageFilter',
            fn: function(item , options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return $scope.supportFileType.indexOf(type) !== -1;
            }
        }); 

        // CALLBACKS

        uploaderVideo.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
			Alertify.alert("Please upload "+$scope.supportFileType+" file");
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploaderVideo.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploaderVideo.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploaderVideo.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploaderVideo.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploaderVideo.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploaderVideo.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploaderVideo.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploaderVideo.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploaderVideo.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploaderVideo.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploaderVideo);
		
		$scope.tagTransformCategory = function (newTag) {
			var item = {
			category_name: newTag, 
			};

			return item;
		};

});	

coursemodule.controller('CoursesactivityController', function($scope,$location,$localStorage,$http,FileUploader,Alertify,$rootScope,$window){
	
	/* 
	$scope.togglelabel1={value:'Yes'};//'Yes';
	$scope.togglelabel2={value:'No'};//'No'; */
	$scope.multiple={
		selectedCategoryName:[]
	}
	
	$scope.tagTransformCategory = function (newTag) {
			var item = {
			category_name: newTag, 
			training_category_id: newTag, 
			};

			return item;
		};
		
		
	$scope.tagArray=[{tag:''}];	
		
	
		
		$scope.tagTransformTags = function (newTag) {
			var item = {
			tag: newTag, 
			
			};

			return item;
		};
		
		
	$scope.multiple.selectedCategoryNames=[];
		$scope.multiple.selectedGroup=[];
		$scope.multiple.selectedGroupName2=[];
		$scope.multiple.selectedUserName2=[];
		$scope.tagTransformGroupName = function (newTag) {
			var item = {
			group_name: newTag, 
			};

			return item;
		};
		//end
		//start add new tag here for user
			
		$scope.tagTransformUserName = function (newTag) {
			var item = {
			user_name: newTag, 
			};

			return item;
		};
		//end
		//start add new tag here for user2
			
		$scope.tagTransformUserName2 = function (newTag) {
			var item = {
			user_name: newTag, 
			};

			return item;
		};
		//end
		
		
		$scope.classroomCourseData=function(){
			$http({
				method:"POST",
				url:"/admin/getClassRoomCourseData"
			}).success(function(response){
				$scope.classroomAddData=response;
				//alert(JSON.stringify(response));
			})
		}
		$scope.classroomCourseData();
		
	$scope.firstView=true;
	/* $scope.secondView=true;
	$scope.thirdPage=true; */
		
		$scope.reloadData=function(){		
			$scope.$on('onBeforeUnload', function (e, confirmation) {
				confirmation.message = "All data willl be lost.";
				e.preventDefault();
			});
			$scope.$on('onUnload', function (e) {
				console.log('leaving page'); // Use 'Preserve Log' option in Console
			});
		}
		$scope.reloadData();
	
		
		var uploader = $scope.uploader = new FileUploader({
           url:'/admin/uploadfile',
		   alias:'courseFile',
        });
		
		
		
		
        // FILTERS

	 uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
				
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				console.log(type);
                return $scope.canUploadMedia.indexOf(type) !== -1;
            }
        });


      var HandOut = $scope.HandOut = new FileUploader({
           url:'/admin/uploadHandout',
		   alias:'handoutFile',
        });
		
	  console.info('uploader', HandOut);
	  HandOut.onAfterAddingFile = function(fileItem) {
            //console.info('onAfterAddingFile',fileItem);
        };

       
    
        // CALLBACKS

       
		uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
			Alertify.alert('Please upload only '+$scope.uploadType+' files');
            //console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
			
			 if (uploader.getNotUploadedItems().length > 1) {
				uploader.removeFromQueue(0);
			  }
            //console.info('onAfterAddingFile', fileItem);
        };
		
		/* HandOut.onAfterAddingFile = function(fileItem) {
			
			 if (HandOut.getNotUploadedItems().length > 1) {
				HandOut.removeFromQueue(0);
			  }
            console.info('onAfterAddingFile', fileItem);
        };
		 */
		
		$scope.courseMediaIdArray=[];
		uploader.onCompleteItem = function(fileItem, response, status, headers) {
			$scope.courseMediaIdArray.push(response.uploadedId);
			//console.info('onCompleteItem', fileItem, response, status, headers);
			//console.info($scope.courseMediaIdArray);
			
		};


	/* 	uploader.onProgressAll = function(progress) {
			
		};
		 */
		 $scope.isUploaded=0;
		 uploader.onCompleteAll = function() {
            $scope.secondPage=false;
			$scope.sixthPage=true;
			$scope.isUploaded=1;
        };
		
		
		//console.log(uploader);
		$scope.generateReferenceCode=function(){
			if($scope.media_type==undefined || !$scope.media_type){
				Alertify.alert("Please select training content first.");
				$scope.referenceCode='';
			}else{
				$http({
					method:"POST",
					url:"/admin/generatereferancecode",
					data:{trainingType:1,mediaType:$scope.media_type}
				}).success(function(response){
					$scope.referenceCode=response;
					//alert(JSON.stringify(response));
				})
			}
		}
		  $scope.point_show=0;
          $scope.credit_show=0;
		$scope.addCourseFirstData=function(){
			if($scope.courseName==undefined || !$scope.courseName){
				Alertify.alert("Course Name is required.");		
			}else if($scope.multiple.selectedCategoryName.length<1){
				Alertify.alert("Training category is required.");		
			}else if($scope.media_type==undefined || !$scope.media_type){
				Alertify.alert("Please select training content.");
				$scope.referenceCode='';
			}else if(!$scope.referenceCode){
				Alertify.alert("Please generate referance code.");
				$scope.referenceCode='';
			}else{
				
				$scope.media_type=$scope.media_type;
				$http({
					method:"POST",
					url:"/admin/saveFirstCourseData",
					data:{courseName:$scope.courseName,referenceCode:$scope.referenceCode,courseTitle:$scope.activityTitle}
				}).success(function(response){
					if(response.status==401){
						Alertify.alert(response.message);
					}else{
						
						$scope.firstView=false;
						$scope.secondView=true;
						$scope.secondPage=true;
						if($scope.media_type==11 || $scope.media_type==3){
							$scope.canUploadMedia='|zip|||';
							$scope.uploadType='zip,rar';
						}else if($scope.media_type==2){
							$scope.canUploadMedia='|mp3|x-ms-wma|aac|m4a|vnd.dlna.adts|x-m4a|';
							$scope.uploadType='mp3,Wma,aac,m4a';
						}else if($scope.media_type==8){
							$scope.canUploadMedia='|doc|docx|ppt|pptx|xls|xlsx|pdf|vnd.openxmlformats-officedocument.presentationml.presentation|vnd.openxmlformats-officedocument.wordprocessingml.document|||vnd.ms-excel|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.spreadsheetml.template|';
							$scope.uploadType='doc,pdf,pptx,xls';
						}else if($scope.media_type==4){
							$scope.canUploadMedia='|doc|docx|pdf|xlsx|xls|ppt|pptx|vnd.openxmlformats-officedocument.presentationml.presentation|vnd.openxmlformats-officedocument.wordprocessingml.document|||vnd.ms-excel|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.spreadsheetml.template|';
							$scope.uploadType='doc,pdf,ppt,xls';
						}else if($scope.media_type==7){
							$scope.canUploadMedia='|ppt|pptx|doc|docx|pdf|vnd.openxmlformats-officedocument.presentationml.presentation|vnd.openxmlformats-officedocument.wordprocessingml.document|||vnd.ms-excel|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.spreadsheetml.template|';
							$scope.uploadType='doc,pdf,ppt,xls';
						}else if($scope.media_type==1){
							$scope.canUploadMedia='|mp4|m4v|flv|x-ms-wmv|mov|swf||||';
							$scope.uploadType='mp4,wmv,m4v,flv';
						}else if($scope.media_type==5){
							
						}
						/* $scope.savesCourseId=response.courseId;
					    if(uploader.queue.length>0){
							$scope.uploader.onBeforeUploadItem = function(item) {
								if($scope.savesCourseId==''){
									item.formData.push({courseId:response.courseId});	
								}else{
									item.formData.push({courseId:$scope.savesCourseId});	
								}
								
							};

							$scope.uploader.uploadAll(); 
							
						}else{
							
						} */
					}		
				})
			}	
		}
		
		$scope.previousFirst=function(){
			$scope.firstView=true;
				$scope.secondView=false;
				$scope.secondPage=false;
		}
		
		$scope.embededCodefileId='';
		$scope.courseImageFunc = function(element) {

			var reader = new FileReader();
			reader.onload = function(e){
				$scope.$apply(function(){
					$scope.courseImageData = e.target.result;
					$scope.theFile = element.files[0];
				});
			}
			reader.readAsDataURL(element.files[0]);
			
		};
		
		$scope.nextFile=function(){
			if($scope.media_type!=10){

				if(uploader.queue.length>0){
					$scope.uploader.onBeforeUploadItem = function(item) {
							item.formData.push({});	
					};
					
					if($scope.media_type==11 || $scope.media_type==3){
						$scope.uploader.onBeforeUploadItem = function(item) {
						item.formData.push({passing_score:$scope.passing_score,media_type:$scope.media_type});	
						};
					}else{
						$scope.uploader.onBeforeUploadItem = function(item) {
						item.formData.push({media_type:$scope.media_type});	
						};
					}
					$scope.uploader.uploadAll(); 
					if($scope.isUploaded==1){
						$scope.secondPage=false;
			            $scope.sixthPage=true;
					}

					
					
				}else{
					Alertify.alert("Please upload media.");		
				}
			}else{
				
				if($scope.embededcode==undefined || !$scope.embededcode){
					$scope.secondPage=false;
						$scope.sixthPage=true;
					}else{
					$http({
						method:"POST",
						url:"/admin/uploadfile",
						data:{embededcode:$scope.embededcode,media_type:10}
					}).success(function(response){
						$scope.courseMediaIdArray.push(response.uploadedId);
						$scope.secondPage=false;
						$scope.sixthPage=true;
					})
				}
			 }
		}
		
		$scope.saveEmbededCode=function(){
			$scope.embededCodeData=$scope.descem;
			if(!$scope.embededcodeTitle || $scope.embededcodeTitle==undefined){
				Alertify.alert('Please enter title.');
			}else if(!$scope.embededCodeData || $scope.embededCodeData==undefined){
				Alertify.alert('Please Enter Embedded Code.');
			}else{
				
				$http({
					method:"POST",
					url:"/admin/saveembededcode",
					data:{embededTitle:$scope.embededcodeTitle,embededCode:$scope.embededCodeData,embededCodefileId:$scope.embededCodefileId,mediaType:$scope.media_type}
				}).success(function(response){
					if($scope.embededCodefileId==''){
						$scope.embededCodefileId=response.uploadedId;
						$scope.courseMediaIdArray.push(response.uploadedId);
						$scope.secondPage=false;
						$scope.sixthPage=true;
					}else{
						$scope.secondPage=false;
						$scope.sixthPage=true;
					}
					
				})
			}
		}
		
		$scope.activitySave=function(){
			if($scope.activityTitle==undefined || !$scope.activityTitle){
				Alertify.alert("Activity title is required.");		
			}else if($scope.multiple.selectedCategoryName.length<1){
				Alertify.alert("Training category is required.");		
			}else{
				$scope.thirdPage=false;
				$scope.sixthPage=true;
			}
		}
		$scope.optionalpage=function(){
			$scope.thirdPage=false;
			$scope.sevenPage=true;
			$scope.sixthPage=false;
		}
		$scope.previousfourthPage=function(){
			$scope.thirdPage=false;
			$scope.sixthPage=true;
		}
			
		$scope.previousThirdPage=function(){
				$scope.sixthPage=false;
				$scope.secondPage=true;
		}
		
		$scope.removeHandoutQueue=function(index){
			HandOut.queue.splice(index,1);
		}
		
		$scope.removeQueue=function(index){
			if($scope.courseMediaIdArray.length==0){
				uploader.queue.splice(index,1);
			}else{
				$http({
					method:"POST",
					url:"/admin/deletefile",
					data:{fileId:$scope.courseMediaIdArray[0]}
				}).success(function(response){
					if(response.status=='201'){
						uploader.queue.splice(index,1);
						$scope.courseMediaIdArray=[];
					}
				})
			}
		}
		
		$scope.previousThird=function(){
			$scope.secondPage=true;
			$scope.thirdPage=false;
		}
		$scope.previousFrouth=function(){
			$scope.thirdPage=true;
			$scope.fourthPage=false;
		}
		$scope.userAllSkills=[];
		var i=1;
		$scope.userAllSkills.push({'id':i,'courseSkill':'','courseSkillLevels':'','skillLevelCredit':'','creditValue':'','skillLevels':[]});
		
		$scope.addMoreSkill=function(){
			if(i<3){
				i++;
				$scope.userAllSkills.push({'id':i,'courseSkill':''});
			}else{
				Alertify.alert("You can add only three skills.");	
			}
		}
		
		$scope.deletelevel=function(index){
			$scope.userAllSkills.splice(index,1);
			i--;
		}
		
		$scope.userSkillLevel=function(index){
			$scope.userAllSkills[index].courseSkillLevels='';
			$scope.userAllSkills[index].skillLevelCredit='';
			$scope.userAllSkills[index].creditValue='';
			//alert(JSON.stringify($rootScope.audioVideoData.skillarray[$scope.couseSkillId]));
		}
		
		$scope.addSkillLevel=function(index){
			$scope.couseSkillId=$scope.userAllSkills[index].courseSkill;
			$scope.skillLevelId=$scope.userAllSkills[index].courseSkillLevels;
			$scope.userAllSkills[index].skillLevels=$rootScope.audioVideoData.skillarray[$scope.couseSkillId].levels;
			angular.forEach($scope.userAllSkills[index].skillLevels,function(levels){
				if(levels.level_id==$scope.skillLevelId){
					$scope.userAllSkills[index].skillLevelCredit=levels.skill_credit;
				}
			})	
		}
		
		$scope.checkCredit=function(index){
			if($scope.userAllSkills[index].skillLevelCredit<$scope.userAllSkills[index].creditValue){
				Alertify.alert("Credit Value must be less than or equel to max credit value");	
				$scope.userAllSkills[index].creditValue='';
			}
		}
		
		
		
		$scope.finalSubmit=function(){
			if($scope.status==undefined || !$scope.status){
				Alertify.alert("Please select status.");	
			}else{
				
				$scope.loader=true;
			var course_pic_data=courseImage.files.item(0);
			if(course_pic_data){
				var isCourseImage=1;
			}else{
				var isCourseImage=0;
			}
			$scope.checkNotification=[];
			angular.forEach($rootScope.audioVideoData.notification,function(list){
				if(list.checkNotification==true){
					$scope.checkNotification.push(list.notification_id);
				}
			})
			
			$scope.mediaLibraryId='';
			if($scope.courseMediaIdArray.length>0){
				$scope.mediaLibraryId=$scope.courseMediaIdArray[0];
			}
			
			var course={
				'credit_value':$scope.credit_value,
				'points':$scope.points,
				'media_library_id':$scope.mediaLibraryId,
				'status_id':1,
				'certificate_template_id':$scope.certificate,
				'point_show':$scope.point_show,
				'credit_show':$scope.credit_show
			};
				
			var courseDetail={
				'course_name':$scope.courseName,
				'course_title':$scope.activityTitle,
				'course_description':$scope.courseDescription
			};
			
			
			var trainingPrograms={
				'training_type':1,
				'is_internal':1,
				'media_type_id':$scope.media_type,
				'status_id':$scope.status
			};
			if($scope.showNotification==0){
				$scope.checkNotification=[];
			}
			$http({
				method:"POST",
				url:"/admin/saveCourse",
				headers: {'Content-Type': undefined },
				transformRequest:function(){
					var formData=new FormData();
					formData.append('course',angular.toJson(course));
					formData.append('courseDetail', angular.toJson(courseDetail));
					formData.append('trainingPrograms',angular.toJson(trainingPrograms));
					formData.append('isCourseImage',isCourseImage);
					formData.append('userAllSkills',angular.toJson($scope.userAllSkills));
					formData.append('categories',angular.toJson($scope.multiple.selectedCategoryName));
					formData.append('checkNotification',angular.toJson($scope.checkNotification));
					formData.append('courseImage',course_pic_data);
					formData.append('referenceCode',$scope.referenceCode);
					formData.append('targetAudienceGroups',angular.toJson($scope.multiple.selectedGroup));
					formData.append('targetAudienceusers',angular.toJson($scope.multiple.selectedUserName1));
					formData.append('permissionGroups',angular.toJson($scope.multiple.selectedGroupName2));
					formData.append('permissionUsers',angular.toJson($scope.multiple.selectedUserName2));
					formData.append('courseTags',angular.toJson($scope.multiple.selectedTag));
					return formData;
				}
			}).success(function(response){
				$scope.loader=false;
				if(response.status==201){
					Alertify.alert('Course Addedd Successfully.');
					$scope.trainingCode=response.trainingCode;
					$scope.HandOut.onBeforeUploadItem = function(item) {
								item.formData.push({courseId:response.courseId});	
					};
					if(HandOut.queue.length>0){
						$scope.HandOut.uploadAll();
					}else{
						
						//$location.path('/courses/editelearning/'+response.trainingCode);
						$location.path('/courses/index');
					}
                     
					 HandOut.onCompleteAll = function(progress) {
						
						//$location.path('/courses/editelearning/'+$scope.trainingCode);
						$location.path('/courses/index');
					};
							
							
					//$location.path('/courses/editelearning/'+response.trainingCode);
					//$window.location.reload();
				}
				//alert(JSON.stringify(response));
			})
			//alert(JSON.stringify($rootScope.audioVideoData.notification));
		}
	}
	
	
	$scope.showNotification=1;
	$scope.showNoticationOption=function(value){
		$scope.showNotification=value;
	}
	
	    
});	

coursemodule.directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);
coursemodule.controller('ClassroomCoursesController', function($scope,$location,$localStorage,$http,Alertify,$rootScope,FileUploader){
	
	$scope.showNotification=1;
	$scope.showNoticationOption=function(value){
		$scope.showNotification=value;
	}
	
	
	 var HandOut = $scope.HandOut = new FileUploader({
           url:'/admin/uploadHandout',
		   alias:'handoutFile',
        });
		
	  console.info('uploader', HandOut);
	  HandOut.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile',fileItem);
        };
		
		
        
		
		
		$scope.courseMediaIdArray=[];
	

		console.log(HandOut);
		
	$scope.tagArray=[{tag:''}];	
		
	$scope.tagTransformCategory = function (newTag) {
			var item = {
			category_name: newTag, 
			training_category_id: newTag, 
			};

			return item;
		};
		
		$scope.tagTransformTags = function (newTag) {
			var item = {
			tag: newTag, 
			tag: newTag, 
			};

			return item;
		};
		$scope.expdate='';
		$scope.firstPage=true;
		//start add new tag here for group1
		$scope.multiple=[];
		$scope.multiple.selectedGroupName= [{group_name:'Everyone'}];
		$scope.multiple.selectedCategoryNames=[];
		$scope.multiple.selectedGroup=[];
		$scope.multiple.selectedGroupName2=[];
		$scope.multiple.selectedUserName2=[];
		$scope.tagTransformGroupName = function (newTag) {
			var item = {
			group_name: newTag, 
			};

			return item;
		};
		//end
		//start add new tag here for user
			
		$scope.tagTransformUserName = function (newTag) {
			var item = {
			user_name: newTag, 
			};

			return item;
		};
		//end
		//start add new tag here for user2
			
		$scope.tagTransformUserName2 = function (newTag) {
			var item = {
			user_name: newTag, 
			};

			return item;
		};
		//end
		
		
		$scope.classroomCourseData=function(){
			$http({
				method:"POST",
				url:"/admin/getClassRoomCourseData"
			}).success(function(response){
				$scope.classroomAddData=response;
				//alert(JSON.stringify(response));
			})
		}
		$scope.classroomCourseData();
		
		
		$scope.userAllSkills=[];
		var i=1;
		$scope.userAllSkills.push({'id':i,'courseSkill':'','courseSkillLevels':'','skillLevelCredit':'','creditValue':'','skillLevels':[]});
		$scope.addMoreSkill=function(){
			if(i<3){
				i++;
				$scope.userAllSkills.push({'id':i,'courseSkill':''});
			}else{
				Alertify.alert("You can add only three skills.");	
			}
		}
		
		$scope.deletelevel=function(index){
			$scope.userAllSkills.splice(index,1);
			i--;
		}
		
		$scope.userSkillLevel=function(index){
			$scope.userAllSkills[index].courseSkillLevels='';
			$scope.userAllSkills[index].skillLevelCredit='';
			$scope.userAllSkills[index].creditValue='';
			//alert(JSON.stringify($rootScope.audioVideoData.skillarray[$scope.couseSkillId]));
		}
		
		$scope.addSkillLevel=function(index){
			$scope.couseSkillId=$scope.userAllSkills[index].courseSkill;
			$scope.skillLevelId=$scope.userAllSkills[index].courseSkillLevels;
			
			$scope.userAllSkills[index].skillLevels=$rootScope.audioVideoData.skillarray[$scope.couseSkillId].levels;
			angular.forEach($scope.userAllSkills[index].skillLevels,function(levels){
				if(levels.level_id==$scope.skillLevelId){
					$scope.userAllSkills[index].skillLevelCredit=levels.skill_credit;
				}
			})
		}
		
		$scope.checkCredit=function(index){
			if($scope.userAllSkills[index].skillLevelCredit<$scope.userAllSkills[index].creditValue){
				Alertify.alert("Credit Value must be less than or equel to max credit value");	
				$scope.userAllSkills[index].creditValue='';
			}
		}
		
		$scope.courseImageFunc = function(element)
		{

			var reader = new FileReader();
			reader.onload = function(e){
				$scope.$apply(function(){
					$scope.courseImageData = e.target.result;
					$scope.theFile = element.files[0];
				});
			}
			reader.readAsDataURL(element.files[0]);
			
		};
		
		$scope.generateReferenceCode=function(){
				$http({
					method:"POST",
					url:"/admin/generatereferancecode",
					data:{trainingType:2,mediaType:00}
				}).success(function(response){
					$scope.referenceCode=response;
					//alert(JSON.stringify(response));
				})
			
		}
		
		
		$scope.enrollement_type='2';
		$scope.firstPageSubmit=function()
		{
			if(!$scope.courseName){
				Alertify.alert('Course name is required');
			}else if(!$scope.referenceCode){
				Alertify.alert('Reference code is required');
			}else if($scope.multiple.selectedCategoryNames.length==0){
				Alertify.alert('Category is required');
			}else{
				$scope.firstPage=false;
				$scope.secondPage=true;
				$scope.thirdPage=false;
			//	$scope.fourthPage=false;
			}
		}
		
		$scope.previousSecondPage=function()
		{
			$scope.firstPage=true;
			$scope.secondPage=false;
			$scope.thirdPage=false;
		}
		
		$scope.previousThirdPage=function()
		{
			$scope.secondPage=true;
			$scope.firstPage=false;
			$scope.thirdPage=false;
		}
		
		$scope.previousFourthPage=function()
		{
			$scope.thirdPage=true;
			$scope.fourthPage=false;
		}
	
		$scope.secondPageSubmit=function()
		{
			$scope.thirdPage=true;
			$scope.secondPage=false;
			$scope.fourthPage=false;
		}
		
		$scope.thirdPageSubmit=function(){
			$scope.thirdPage=false;
			$scope.secondPage=false;
			$scope.fourthPage=true;
			
		}
		
		$scope.fourthPageSubmit=function()
		{
			var course_pic_data=courseImage.files.item(0);
			if(course_pic_data){
				var isCourseImage=1;
			}else{
				var isCourseImage=0;
			}
			
			$scope.checkNotification=[];
			angular.forEach($rootScope.audioVideoData.notification,function(list){
				if(list.checkedStatus==1){
					$scope.checkNotification.push(list.notification_id);
				}
			})
			
			var course={
				'credit_value':$scope.credit_value,
				'points':$scope.points,
				'status_id':1,
				'certificate_template_id':$scope.certificate,
				'point_show':$scope.point_show,
				'credit_show':$scope.credit_show
			};
			
			var trainingPrograms={
				'training_type':2,
				'is_internal':1,
				'media_type_id':0,
				'status_id':$scope.status
			};
				
			var courseDetail={
				'course_name':$scope.courseName,
				'course_description':$scope.courseDescription
			};
				
			
			var courseSetting={
				'ilt_assessment_id':$scope.itl_assessment_id,
				'required_approval':$scope.required_approval,
				'enrollment_type':$scope.enrollement_type,
				'is_unenrollment':$scope.is_unenrollment
				};
				
			if($scope.showNotification==0){
				$scope.checkNotification=[];
			}	
			
           $http({
			   method:"POST",
			   url:"/admin/saveClassroomCourse",
			  headers: {'Content-Type': undefined },
				transformRequest:function(){
					var formData=new FormData();
					    formData.append('course',angular.toJson(course));
					    formData.append('courseDetail',angular.toJson(courseDetail));
					    formData.append('courseSetting',angular.toJson(courseSetting));
						formData.append('isCourseImage',isCourseImage);
						formData.append('categories',angular.toJson($scope.multiple.selectedCategoryNames));
						formData.append('checkNotification',angular.toJson($scope.checkNotification));
						formData.append('courseImage',course_pic_data);
						formData.append('referenceCode',$scope.referenceCode);
						formData.append('courseTags',angular.toJson($scope.multiple.courseTags));
						formData.append('targetAudienceGroups',angular.toJson($scope.multiple.selectedGroup));
						formData.append('targetAudienceusers',angular.toJson($scope.multiple.selectedUserName1));
						formData.append('permissionGroups',angular.toJson($scope.multiple.selectedGroupName2));
						formData.append('permissionUsers',angular.toJson($scope.multiple.selectedUserName2));
						formData.append('userAllSkills',angular.toJson($scope.userAllSkills));
						formData.append('trainingPrograms',angular.toJson(trainingPrograms));
						return formData;
				   
			   }
		   }).success(function(response){
			   $scope.loader=false;
				if(response.status==201){
					Alertify.alert('Course Addedd Successfully.');
					$scope.trainingCode=response.trainingCode;
					$scope.HandOut.onBeforeUploadItem = function(item) {
								item.formData.push({courseId:response.courseId});	
					};
					if(HandOut.queue.length>0){
						$scope.HandOut.uploadAll();
					}else{
						
						//$location.path('/courses/editclassroomcourse/'+response.trainingCode);
						$location.path('/courses/index');
					}
                     
					 HandOut.onCompleteAll = function(progress) {
						
						$location.path('/courses/editclassroomcourse/'+$scope.trainingCode);
						$location.path('/courses/index');
					};
				}
		   })			
			
			
			
			
		}
		
		$scope.removeHandoutQueue=function(index){
			HandOut.queue.splice(index,1);
		}
		$scope.fourthPageSubmit=function()
		{
			$scope.fourthPage=false;
			$scope.fifthPage=true;
		}
		
		$scope.previousFifthPage=function()
		{
			$scope.fourthPage=true;
			$scope.fifthPage=false;
		}
		
		
		
});


coursemodule.factory('beforeUnload', function ($rootScope, $window) {
    // Events are broadcast outside the Scope Lifecycle
    
    $window.onbeforeunload = function (e) {
        var confirmation = {};
        var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
        if (event.defaultPrevented) {
            return confirmation.message;
        }
    };
    
    $window.onunload = function () {
        $rootScope.$broadcast('onUnload');
    };
    return {};
})
coursemodule.run(function (beforeUnload) {
    // Must invoke the service at least once
});


coursemodule.controller('controllerTestUpload2', function($scope,$location,$localStorage,$http,Alertify,$rootScope,FileUploader){
	var HandOut = $scope.HandOut = new FileUploader({
           url:'/admin/uploadHandout',
		   alias:'handoutFile',
        });
		 console.info('uploader', HandOut);
		 HandOut.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
		
	
});

