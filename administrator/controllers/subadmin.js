var subadminmodule = angular.module("subadminmodule",['datatables', 'ngResource']);
subadminmodule.config(["$stateProvider",function($stateProvider){
	
   $stateProvider.state("subadminmodule", {
		url:"/admin/manage-sub-admin",
		views: {
			"mainView":{
				templateUrl: '/administrator/templates/SubAdmin/subadmin.html',
				controller: 'subadmincontroller'
			}
		},
	});	
         
}]);
subadminmodule.controller('subadmincontroller',ServerSideProcessingCtrl);
function ServerSideProcessingCtrl(DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,$scope,$compile,$rootScope,$http) { 
    var vm = this;
	$scope.tokenuse=$("input[name=_token]").val();
    vm.selected = {};
    vm.selectedcat = {};
    vm.selectedgroup= {};
	
    vm.selectAll = false;
    vm.selectAllcat = false;
    vm.selectAllgroup = false;
	
    vm.groplist = false;
    vm.groplistcat = false;
	
    vm.toggleAll = toggleAll;
    vm.toggleAllcat = toggleAllcat;
    vm.toggleAllgroup = toggleAllgroup;
	
    vm.toggleOne = toggleOne;
    vm.toggleOnecat = toggleOnecat;
    vm.toggleOnegroup = toggleOnegroup;
	
    vm.addgroup = addgroup;
    vm.addcategory = addcategory;
    vm.bulkaction = bulkaction;
    vm.operationvalue = operationvalue;
    vm.addgroupSave = addgroupSave;
    vm.addcategorySave = addcategorySave;
    vm.init = init;
    vm.usergroup = [];
    vm.flag =0;
	
	
	
 function init(selected_rowsuser,selected_rowsgroup)
 {  vm.selected = {};
    vm.usergroup = [];
	vm.selectAll = false;
    var titleHtml = '<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll, showCase.selected)">';
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
		 data:{_token:$scope.tokenuse,page:1,test:selected_rowsuser,selected_rowsgroup:selected_rowsgroup},
         url: '/managesubadmindata',
         type: 'POST'
         }).withDataProp(function(json) {
         console.log(json);
		  vm.selectAll = false;
		 json.recordsTotal = json.count;
		 json.recordsFiltered = json.count;
		 json.draw = json.draw;
		 return json.data;
        }).withDOM('C<"clear">lfrtip')
        .withOption('processing', true)
        .withOption('serverSide', true)
		.withOption('createdRow', function(row, data, dataIndex) {
         $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
         
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            
        })
        .withPaginationType('full_numbers');
		
        vm.dtColumns = [
		 DTColumnBuilder.newColumn('id').withTitle('ID').notVisible() ,
         DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
            .renderWith(function(data, type, full, meta) {
                  vm.selected[full.id] = false;
                return '<input type="checkbox" ng-model="showCase.selected[' + data.id + ']" ng-click="showCase.toggleOne(showCase.selected)">';
            }),
        DTColumnBuilder.newColumn('user_name').withTitle('Name'),
        DTColumnBuilder.newColumn('groups').withTitle('groups name'),
        DTColumnBuilder.newColumn('categories').withTitle('categories name'),
        DTColumnBuilder.newColumn('buttons').withTitle('Permission').notSortable()
    ];
 }
	vm.init(0,0);
	
	
	
	
	// user
	
	 function toggleAll (selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne (selectedItems) { 
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if(!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
		
    }
      
	 // group select 
	 
	 function toggleAllgroup(selectAllgroup, selectedItemsgroup){ 
        for (var idgroup in selectedItemsgroup) {
            if (selectedItemsgroup.hasOwnProperty(idgroup)) {
                selectedItemsgroup[idgroup] = selectAllgroup;
            }
        }
    }
    function toggleOnegroup (selectedItemsgroup) {  
        for (var idgroup in selectedItemsgroup) {
            if (selectedItemsgroup.hasOwnProperty(idgroup)) {
                if(!selectedItemsgroup[idgroup]) {
                    vm.selectAllgroup = false;
                    return;
                }
            }
        }
        vm.selectAllgroup = true;
		
    }
	
	// cat select
	   
	 function toggleAllcat(selectAllcat, selectedItemscat){ 
        for (var idcat in selectedItemscat) {
            if (selectedItemscat.hasOwnProperty(idcat)) {
                selectedItemscat[idcat] = selectAllcat;
            }
        }
    }
    function toggleOnecat (selectedItemscat) {  
        for (var idcat in selectedItemscat) {
            if (selectedItemscat.hasOwnProperty(idcat)) {
                if(!selectedItemscat[idcat]) {
                    vm.selectAllcat = false;
                    return;
                }
            }
        }
        vm.selectAllcat = true;
		
    }
	
function addgroup (id) {  
    
     vm.selectAllgroup = false;
       vm.groplist = true;
	    vm.dtOptions1 = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
		 data:{_token:$scope.tokenuse,page:1,id:id},
         url: '/assignedGroupAction',
         type: 'POST'
         }).withDataProp(function(json) {
         console.log(json);
		  vm.selectAllgroup = false;
		 json.recordsTotal = json.count;
		 json.recordsFiltered = json.count;
		 json.draw = json.draw;
		 return json.data;
        }).withDOM('C<"clear">lfrtip')
        .withOption('processing', true)
        .withOption('serverSide', true)
		.withOption('createdRow', function(row, data, dataIndex) {
         $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
        })
        .withPaginationType('full_numbers');
		
		 var titleHtmlgroup = '<input type="checkbox" ng-model="showCase.selectAllgroup" ng-click="showCase.toggleAllgroup(showCase.selectAllgroup, showCase.selectedgroup)">';
	     vm.dtColumns1 = [
		 DTColumnBuilder.newColumn('id').withTitle('ID').notVisible() ,
         DTColumnBuilder.newColumn(null).withTitle(titleHtmlgroup).notSortable()
            .renderWith(function(data, type, full, meta) {
				 if(full.checked)
				 {  
					 vm.selectedgroup[full.id] = true;
					  return '<input type="checkbox" ng-model="showCase.selectedgroup[' + data.id + ']" ng-click="showCase.toggleOnegroup(showCase.selectedgroup)" checked>';
				 }else{
                  vm.selectedgroup[full.id] = false;
                return '<input type="checkbox" ng-model="showCase.selectedgroup[' + data.id + ']" ng-click="showCase.toggleOnegroup(showCase.selectedgroup)">';
				 }
            }), 
        DTColumnBuilder.newColumn('group_name').withTitle('groups name'),
        DTColumnBuilder.newColumn('status').withTitle('status')
       ]; 
        
	   
		vm.usergroup = [];
		if(id==0)
		{
	     var selected_rows = new Array();
		 angular.forEach(vm.selected, function(value, key)
		 {    if(value)
			 { 
			  selected_rows.push(key);
			 }
         });
		  vm.usergroup=selected_rows;
		    vm.flag =0;
		}
		else
		{   
           vm.flag =1;
		   vm.usergroup.push(id);
		}
		$('#assign-group').modal();
    }
	
	
	function addgroupSave()
	{
         var selected_rowsgroup = new Array();
		 angular.forEach(vm.selectedgroup, function(value, key)
		 {    if(value)
			 { 
			  selected_rowsgroup.push(key);
			 }
         });
		
		if(selected_rowsgroup.length==0) 
		{
			alertify.alert("Please Select Group.");
			return false;
		}
     else
    {	
		$http({
					method:"POST",
					url:"/saveAssignedGroupAction",
					data:{_token:$scope.tokenuse,sub_admin_id:vm.usergroup,selected_groups:selected_rowsgroup,flag:vm.flag}
				}).success(function(response){
					$scope.referenceCode=response;
					
						vm.selected = {};
						vm.selectAll = false;
						vm.selectAllcat = false;
						vm.selectAllgroup = false;
						vm.init(vm.usergroup);
						 $("#assign-group").modal("hide");
				     
				})
	   
    }
	
    }
	
	
	
	function addcategorySave () 
	{
		  var selected_rowsgroup = new Array();
		 angular.forEach(vm.selectedcat, function(value, key)
		 {    if(value)
			 { 
			  selected_rowsgroup.push(key);
			 }
         });
		
		if(selected_rowsgroup.length==0) 
		{
			alertify.alert("Please Select Category.");
			return false;
		}
     else
    {	
		$http({
					method:"POST",
					url:"/saveAssignedCategoryAction",
					data:{_token:$scope.tokenuse,sub_admin_id:vm.usergroup,selected_training_category_id:selected_rowsgroup,flag:vm.flag}
				}).success(function(response){
					$scope.referenceCode=response;
					vm.selected = {};
					vm.selectAll = false;
					vm.selectAllcat = false;
					vm.selectAllgroup = false;
					vm.init(vm.usergroup,selected_rowsgroup,selected_rowsgroup);
					 $("#assign-category").modal("hide");
				     
				})
	   
      }
    }
	function addcategory (id) {   
	
    vm.selectAllcat = false;
	vm.groplistcat = true;
	    vm.dtOptions2 = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
		 data:{_token:$scope.tokenuse,page:1,id:id},
         url: '/assignedCategoryAction',
         type: 'POST'
         }).withDataProp(function(json) {
         console.log(json);
		  vm.selectAllcat = false;
		 json.recordsTotal = json.count;
		 json.recordsFiltered = json.count;
		 json.draw = json.draw;
		 return json.data;
        }).withDOM('C<"clear">lfrtip')
        .withOption('processing', true)
        .withOption('serverSide', true)
		.withOption('createdRow', function(row, data, dataIndex) {
         $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
        })
        .withPaginationType('full_numbers') ;
		
		 var titleHtmlgroup = '<input type="checkbox" ng-model="showCase.selectAllcat" ng-click="showCase.toggleAllcat(showCase.selectAllcat, showCase.selectedcat)">';
	     vm.dtColumns2 = [
		 DTColumnBuilder.newColumn('id').withTitle('ID').notVisible() ,
         DTColumnBuilder.newColumn(null).withTitle(titleHtmlgroup).notSortable()
            .renderWith(function(data, type, full, meta) {
				 if(full.checked)
				 {  
					 vm.selectedcat[full.id] = true;
					  return '<input type="checkbox" ng-model="showCase.selectedcat[' + data.id + ']" ng-click="showCase.toggleOnecat(showCase.selectedcat)" checked>';
				 }else{
                  vm.selectedcat[full.id] = false;
                return '<input type="checkbox" ng-model="showCase.selectedcat[' + data.id + ']" ng-click="showCase.toggleOnecat(showCase.selectedcat)">';
				 }
            }), 
        DTColumnBuilder.newColumn('category_name').withTitle('category name'),
        DTColumnBuilder.newColumn('status').withTitle('status')
       ]; 
        
		vm.usergroup = [];
		if(id==0)
		{
	     var selected_rows = new Array();
		 angular.forEach(vm.selected, function(value, key)
		 {    if(value)
			 { 
			  selected_rows.push(key);
			 }
         });
		  vm.usergroup=selected_rows;
		}
		else
		{  
		   vm.usergroup.push(id);
		}
		
      $('#assign-category').modal();
		
    }
function  bulkaction()
{   
	var operationvalue = document.getElementById("operationvalue").value;
	var operationvalue = operationvalue;
	if(operationvalue!="")
	{    
		 var selected_rows = new Array();
		 angular.forEach(vm.selected, function(value, key)
		 {    if(value)
			 { 
			  selected_rows.push(key);
			 }
			 
         });
		 if(selected_rows.length==0) {
		alertify.alert("Please select at least one Sub-Admin.");
		return false;
		}	
		if(operationvalue=="assign-group")
		{   
        	vm.addgroup (0);
			return false;
		}
		if(operationvalue=="assign-category")
		{  
	       vm.addcategory (0);
			return false;
			
		} 
	}
	else
	{
	alertify.alert("Please select options.");
	return false;
	}
	return false;
}
	
	
	

	
}
 
