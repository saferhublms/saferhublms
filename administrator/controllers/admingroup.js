var admingroupmodule = angular.module("admingroupmodule",['datatables','ngSanitize']);
admingroupmodule.controller('GroupListController', WithPromiseCtrl);
	
function WithPromiseCtrl(DTOptionsBuilder ,DTColumnBuilder, $http, $q,$scope,$compile) {
   //alert();
   $scope.tokenuse=$("input[name=_token]").val();
    var vm = this;
	//for checkbox
	vm.selected = {};
    vm.selectAll = false;
    vm.authorized = false;
    vm.toggleAll = toggleAll;
    vm.toggleOne = toggleOne;
    vm.init = init;
    vm.addgroup = addgroup;
    vm.editgroup = editgroup;
    vm.selectoperation = selectoperation;
    vm.bulkaction = bulkaction;
    vm.deletefromusergroup = deletefromusergroup;
    vm.checkselectedgroups = checkselectedgroups;
	//for buttons
	vm.message = '';
    vm.edit = edit;
	vm.openviewpopup = openviewpopup;
    vm.deletefromassessmentmaster = deletefromassessmentmaster;
    vm.delete = deleteRow;
    vm.dtInstance = {};
    vm.persons = {};
	//for callback
	 vm.dtInstances = [];

	 var titleHtml = '<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll, showCase.selected)">';
	 function init(descri){
	  vm.dtOptions = DTOptionsBuilder.newOptions()
	 
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: '/admin/managegroupsnew',
		 data:{'_token':$scope.tokenuse,descri:descri},
         type: 'POST'
		 
     }) .withDataProp(function(response){
	       $scope.setting = response.group_setting;	
		   vm.selectAll = false;
		   response.recordsTotal = parseInt(response.total);
		   response.recordsFiltered = parseInt(response.total);
		   response.draw = parseInt(response.draw); 
		   return response.data;
	 })
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('order',[0,'A'])
		//.withOption('paging', true)
        .withOption('createdRow', function(row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
            if (!vm.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
       // .withOption('rowCallback', rowCallback)
		
		
		.withPaginationType('full_numbers');
     }
		init(0);
		vm.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
            .renderWith(function(data, type, full, meta) {
			//	console.log(full);
                vm.selected[full.id] = false;
                return '<input type="checkbox" ng-model="showCase.selected[' + data.id + ']" ng-click="showCase.toggleOne(showCase.selected)">';
            }), 
       DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(),
        DTColumnBuilder.newColumn('groupName').withTitle('Group Name'),
        DTColumnBuilder.newColumn('description').withTitle('Description'),
		//DTColumnBuilder.newColumn('actions').withTitle('Actions').notVisible(),
        DTColumnBuilder.newColumn(null).withTitle('Asigned Users').notSortable()
          .renderWith(AsignedUsers) ,
		 DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
          .renderWith(Actions)
    ];

	// for checkbox
	function toggleAll (selectAll, selectedItems) {
		//alert(JSON.stringify());
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne (selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if(!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
		//console.log( vm.selected);
        vm.selectAll = true;
    }
	
	
    function edit(person) {
        vm.message = 'You are trying to edit the row: ' + JSON.stringify(person);
        // Edit some data and call server to make changes...
        // Then reload the data so that DT is refreshed
        vm.dtInstance.reloadData();
    }
    function deleteRow(person) {
        vm.message = 'You are trying to remove the row: ' + JSON.stringify(person);
        // Delete some data and call server to make changes...
        // Then reload the data so that DT is refreshed
        vm.dtInstance.reloadData();
    }
    function createdRow(row, data, dataIndex) {
        // Recompiling so we can bind Angular directive to the DT
        $compile(angular.element(row).contents())($scope);
    }
    function Actions(data, type, full, meta) {
        vm.persons[data.actions] = data;
        return data.actions;
    } 
	
	function AsignedUsers(data, type, full, meta) {
        vm.persons[data.viewbutton] = data;
        return data.viewbutton;
    }
	
	
	var titleHtml = '<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll, showCase.selected)">';
	  vm.dtOptions1 = DTOptionsBuilder.newOptions()
	 
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: '/admin/getuserpop/'+"kDMQv",
		 data:{'_token':$scope.tokenuse},
         type: 'POST'
		 
     }) .withDataProp(function(response){
		    vm.selectAll = false;
		   response.recordsTotal = response.total;
		   response.recordsFiltered = response.total;
		   response.draw = response.draw; 
		   return response.data;
	 })
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('order',[0,'A'])
		//.withOption('paging', true)
        .withOption('createdRow', function(row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
            if (!vm.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
       // .withOption('rowCallback', rowCallback)
		
		
		.withPaginationType('full_numbers');
		
		vm.dtColumns1 = [
        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
            .renderWith(function(data, type, full, meta) {
			//	console.log(full);
                vm.selected[full.id] = false;
                return '<input type="checkbox" ng-model="showCase.selected[' + data.id + ']" ng-click="showCase.toggleOne(showCase.selected)">';
            }), 
       DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(),
        DTColumnBuilder.newColumn('groupName').withTitle('Group Name'),
        DTColumnBuilder.newColumn('description').withTitle('Description'),
		//DTColumnBuilder.newColumn('actions').withTitle('Actions').notVisible(),
        DTColumnBuilder.newColumn(null).withTitle('Asigned Users').notSortable()
          .renderWith(AsignedUsers) ,
		 DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
          .renderWith(Actions)
    ];

	// for checkbox
	function toggleAll (selectAll, selectedItems) {
		//alert(JSON.stringify());
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne (selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if(!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
		//console.log( vm.selected);
        vm.selectAll = true;
    }
	
	
    function edit(person) {
        vm.message = 'You are trying to edit the row: ' + JSON.stringify(person);
        // Edit some data and call server to make changes...
        // Then reload the data so that DT is refreshed
        vm.dtInstance.reloadData();
    }
    function deleteRow(person) {
        vm.message = 'You are trying to remove the row: ' + JSON.stringify(person);
        // Delete some data and call server to make changes...
        // Then reload the data so that DT is refreshed
        vm.dtInstance.reloadData();
    }
    function createdRow(row, data, dataIndex) {
        // Recompiling so we can bind Angular directive to the DT
        $compile(angular.element(row).contents())($scope);
    }
    function Actions(data, type, full, meta) {
        vm.persons[data.actions] = data;
        return data.actions;
    } 
	
	function AsignedUsers(data, type, full, meta) {
        vm.persons[data.viewbutton] = data;
        return data.viewbutton;
    }
	
	
	
	
	
	
 function openviewpopup(groupid,type){
	
	 //alert();
       //var vm = this;	
	    vm.authorized = true;
	if(type=='viewusers'){
		//console.log(vm.authorized);
	  vm.dtOptions1 = DTOptionsBuilder.newOptions()
	 
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: '/admin/getuserpop/'+groupid,
		 data:{'_token':$scope.tokenuse},
         type: 'POST'
		 
     }) .withDataProp(function(response){
		// alert(JSON.stringify(response.data));
		    
		   return response.data;
	 })
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('order',[0,'A'])
		//.withOption('paging', true)
        .withOption('createdRow', function(row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
            if (!vm.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
       // .withOption('rowCallback', rowCallback)
		
		
		.withPaginationType('full_numbers');
		
		vm.dtColumns1 = [
        DTColumnBuilder.newColumn('user_name').withTitle('Username'),
        DTColumnBuilder.newColumn('company').withTitle('Compnay'),
        DTColumnBuilder.newColumn('division_name').withTitle('Division'),
        DTColumnBuilder.newColumn('area').withTitle('Area'),
        DTColumnBuilder.newColumn('location').withTitle('Location'),
        DTColumnBuilder.newColumn('role_name').withTitle('Role'),
        DTColumnBuilder.newColumn('delButton').withTitle('Action'), 
     
    ];
		

		
			$('#viewusers').modal('show');
		}if(type=='editgroup'){
			$http({
			  url: '/admin/editgroupdata/'+groupid,
              method: 'POST',
			  data:{'_token':$scope.tokenuse}
			}).success(function(response){
				$('#groupid').val(response.group_id);
				$('#editgroupname').val(response.group_name);
				$('#Description').val(response.description);
				$('#editgroup').modal('show');
				
	       });
			
		}
	

	//}	
	
} 
	
  function addgroup(){

	  var addnewgroup='addnewgroup';
	  var groupname=$scope.groupname;
	 
	  var descri=$scope.description;
	  var iChars = "!@#$%^()+=-[]\\\';,/{}|\":<>?";
	  var reWhiteSpace = new RegExp(/^\s+$/);
	if(!groupname){
		alertify.alert(" Group name should not be blank.");
		return false;
	}
    if(groupname.length > 250){
		alertify.alert(" Group name is too large.");
		return false;
	}
	if(descri){
	if(descri.length > 500){
		alertify.alert(" Group description is too large.");
		return false;
	}
	}
    groupname = groupname.replace("?","quesmark");
	groupname = groupname.replace("+","plusmark");
	groupname = groupname.replace("%","permark");
	groupname = groupname.replace(/\//gi,"bckslsh");
	groupname = groupname.replace(/\//g,"fwckslsh");
	groupname = groupname.replace(/&/g, "&amp;");
	groupname = groupname.replace(/>/g, "&gt;");
	groupname = groupname.replace(/</g, "&lt;");
	groupname = groupname.replace(/"/g, "&quot;");
	groupname = groupname.replace(/'/g, "singleqoute"); 
	grpname=groupname;	
	
	$http({
			url:'/admin/addgroup',
			method:'POST',
			data:{groupname:grpname,description:descri,addnewgroup:addnewgroup,_token:$scope.tokenuse}
			}).success(function(response){
				if(response.Status == 200){
				 alertify.alert(response.Message);
				 init(descri);
				/*   WithPromiseCtrl(DTOptionsBuilder ,DTColumnBuilder, $http, $q,$scope,$compile); */
				}else{
					alertify.alert(response.Message);
				}
				
	   });
	
}

 function editgroup (){
    var editgroupname=document.getElementById("editgroupname").value;
	var descri=document.getElementById('Description').value;
	var groupid=document.getElementById("groupid").value;
	var iChars = "!@#$%^()+=-[]\\\';,/{}|\":<>?";
	var reWhiteSpace = new RegExp(/^\s+$/);	
	if (reWhiteSpace.test(editgroupname)|| editgroupname.length==0 || editgroupname.value=='')
	{
		alertify.alert("Group Name should not be blank");
		return false;
	}
	if(editgroupname.length > 250)
	{
		alertify.alert(" Group Name is too large");
		return false;
	}
	if(descri.length > 500)
	{
		alertify.alert(" Group description is too large");
		return false;
	}
	
	$http({
			url:'/admin/editgrouppop',
			method:'POST',
			data:{groupid:groupid,groupname:editgroupname,description:descri,_token:$scope.tokenuse}
			}).success(function(response){
				if(response.Status == 200){
				 alertify.alert(response.Message);
				 $('#editgroup').modal('hide');
			       init(editgroupname);
				}else{
					alertify.alert(response.Message);
				}
				
	   });
	
 }
 
 
 function selectoperation(operation){
	 
	if(operation=='fword'){
		var fword=$scope.setting.first_word;
		if(fword==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(fword==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	 
	if(operation=='lword'){
		var lword=$scope.setting.last_word;
		if(lword==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(lword==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	if(operation=='exact'){
		var exact=$scope.setting.exact_word;
		if(exact==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(exact==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	if(operation=='companies'){
		var companies=$scope.setting.user_company_name;
		if(companies==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(companies==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	if(operation=='divisions'){
		var divisions=$scope.setting.division;
		if(divisions==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(divisions==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	if(operation=='location'){
		var location=$scope.setting.location;
		if(location==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(location==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	if(operation=='band'){
		var band=$scope.setting.band;
		if(band==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(band==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	if(operation=='area'){
		var area=$scope.setting.area;
		if(area==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(area==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}   
	if(operation=='department'){
		var department=$scope.setting.first_word;
		if(department==true){
			var url='/admin/automatedgroups/'+operation;
		}
		if(department==false){
			var url='/admin/automatedgroups/'+operation+'/1';
		}
	}
	
	
	$http({
			url:url,
			method:'POST',
			}).success(function(response){
				alertify.alert(response);
	   });
	 
 }
 
 
  function deletefromassessmentmaster(assessid,operation){
	  
	alertify.confirm("Are you sure you want to delete this group? This cannot be undone.", function (e) {
		if (e) {
			
			$http({
			url:'/admin/deletegroup/',
			method:'POST',
			data:{mode:operation,groupid:assessid}
			}).success(function(response){
				if(response.Status == 200){
					init('editgroupname');
					alertify.alert(response.Message);
				}
	       });
			
		} 
	},'alertifyremove');		
}


function bulkaction(){
	var selected_rows = new Array();
	 angular.forEach(vm.selected, function(value, key){
          if(value == true){
			  selected_rows.push(key);
		  }
	  });
	 var arr_active=0;
	 var arr_active  = selected_rows.join(",");
	 if(arr_active==0){
		alertify.alert('Please select Groups to apply any action.', function(){},'alertifyapplygroup');
		return false;
	}
	else if(!$scope.operationvalue){
		alertify.alert('Please Select Bulk Option.');
		return false;
	}
	
	
	if($scope.operationvalue=='delete')
	{
		alertify.confirm("Are you sure?", function (e) {
			if (e) {
				
					$http({
						url:'/admin/bulkoptgroup',
						method:'POST',
						data:{mode:'deleteselectedgroups',check1:arr_active}
						}).success(function(response){
							if(response.Status == 200){
								init('editgroupname');
								alertify.alert('Selected groups were successfully deleted');
							}else{
								alertify.alert('Something Went Wrong');
							}
					   });
			} 
		});
	}
}


function deletefromusergroup(groupid,userid)
{
	
	alertify.confirm("Are you sure?", function (e)
	{
		if (e) {
			
			$http({
						url:'/admin/deleteusergroup',
						method:'POST',
						data:{groupid:groupid,userid:userid}
						}).success(function(response){
							if(response.Status == 200){
								openviewpopup(groupid,'viewusers');
								alertify.alert(response.Message);
							}else{
								alertify.alert('Something Went Wrong');
							}
					   });
			} 
	});
}

function checkselectedgroups(operation){
	 vm.auth = true;
	 if(operation=='mailtoselectedusers'){
		 var selected_rows = new Array();
	      angular.forEach(vm.selected, function(value, key){
          if(value == true){
			  selected_rows.push(key);
		  }
	      });
	      var arr_active=0;
	      var arr_active  = selected_rows.join(",");
	
	  } else {
		 var selected_rows = new Array();
	      angular.forEach(vm.selected, function(value, key){
          if(value == true){
			  selected_rows.push(key);
		  }
	      });
	      var arr_active=0;
	      var arr_active  = selected_rows.join(",");
	}
	
	if(arr_active==0)
	{
		alertify.alert('Please select a group.');
		return false;
	}else{
		if(operation=='toadduser')
		{
			 vm.dtOptions2 = DTOptionsBuilder.newOptions()
	 
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
         // dataSrc: 'data',
         url: '/admin/addusertoselectedgroups',
		 data:{'_token':$scope.tokenuse,check1:arr_active},
         type: 'POST'
		 
     }) .withDataProp(function(response){
		   return response;
	 })
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('order',[0,'A'])
		//.withOption('paging', true)
        .withOption('createdRow', function(row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
            if (!vm.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
       // .withOption('rowCallback', rowCallback)
		
		
		.withPaginationType('full_numbers');
		
		vm.dtColumns2 = [
		/* DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
            .renderWith(function(data, type, full, meta) {
			//	console.log(full);
                vm.selected[full.id] = false;
                return '<input type="checkbox" ng-model="showCase.selected[' + data.id + ']" ng-click="showCase.toggleOne(showCase.selected)">';
            }), 
       DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(), */
        DTColumnBuilder.newColumn('user_name').withTitle('Username'),
        DTColumnBuilder.newColumn('company').withTitle('Compnay'),
        DTColumnBuilder.newColumn('division_name').withTitle('Division'),
        DTColumnBuilder.newColumn('area').withTitle('Area'),
        DTColumnBuilder.newColumn('location').withTitle('Location'),
        DTColumnBuilder.newColumn('role_name').withTitle('Role'),
     
    ];
		

		
			$('#addusertogroup').modal('show');
		}
	}
} 
	
}

admingroupmodule.run(["$rootScope","$http","routes", function($rootScope,$http,routes)
{

  $rootScope.callfunction=function(){
		$http({
		url:'/admin/getuserfieldname',
		method:'get',
		}).success(function(response){
		    $rootScope.get_field_name=response;
		});
	}
    $rootScope.callfunction();

}]);

 admingroupmodule.service('WithPromiseCtrl', function () { 
//alert();
});  

/* app.factory('myService',['$resource',function($resource,$http,$scope){
   $http({
			url:'/admin/managegroupsnew',
			method:'GET',
			data:{'_token':$scope.tokenuse}
			}).success(function(response){
				//alert(JSON.stringify(response));
				return response.data.data;
	        });
}]); */