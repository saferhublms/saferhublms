var assessmentmodule = angular.module("assessmentmodule",[]);

assessmentmodule.controller('AssessmentController', function($scope,$rootScope,$location,$localStorage,$http,Alertify,FileUploader,$timeout){
  $scope.multiple={};
  $scope.multiple.selectedCategoryName=[];

  $scope.firstPage=true;
 
  $scope.answerList=[];
  $scope.questionData=[];
  
  $scope.quest=1;
  $scope.answerCount=0;
  $scope.answers=[];
   $scope.questionData.push({id:$scope.quest,'multiChoice':$scope.answers,'multiSelect':$scope.answers,'randomAnswer':'','question':'','tinyMCE':'first','txtId':'id_1'});
  
 
 
  
   var uploader = $scope.uploader = new FileUploader({
          url:'/admin/uploadHandout',
		   alias:'handoutFile',
        });

        // FILTERS
         
         /*  uploaderVideo.filters.push({
            name: 'imageFilter',
            fn: function(item , options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return $scope.supportFileType.indexOf(type) !== -1;
            }
        }); 
 */
		uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
		
		 console.info('uploader', uploader);
  

     $scope.allowSkill=1;
$scope.generateReferenceCode=function(){
	$http({
		method:"POST",
		url:"/admin/generatereferancecode",
		data:{trainingType:5,mediaType:00}
	}).success(function(response){
		$scope.referenceCode=response;
	})
}
  
  
  $scope.classroomCourseData=function(){
			$http({
				method:"POST",
				url:"/admin/getClassRoomCourseData"
			}).success(function(response){
				$scope.classroomAddData=response;
				//alert(JSON.stringify(response));
			})
		}
		$scope.classroomCourseData();

$scope.addCourseFirstData=function(){
			if($scope.courseName==undefined || !$scope.courseName){
				Alertify.alert("Course Name is required.");		
			}else if($scope.multiple.selectedCategoryName.length<1){
				Alertify.alert("Training category is required.");		
			}else{
				
				$scope.media_type=$scope.media_type;
				$http({
					method:"POST",
					url:"/admin/saveFirstCourseData",
					data:{courseName:$scope.courseName,referenceCode:$scope.referenceCode,courseTitle:$scope.activityTitle}
				}).success(function(response){
					if(response.status==401){
						Alertify.alert(response.message);
					}else{
						$scope.firstPage=false;
						$scope.secondPage=true;
					}		
				})
			}	
		}
		
	$scope.secondNext=function(){
		$scope.thirdPage=true;
		$scope.secondPage=false;
	}
	
	$scope.thirdNext=function(){
		var allCorrect=0;
		if($scope.questionData.length>0){
		for(var i=0;i<$scope.questionData.length;i++){
			var que=tinyMCE.get('txt_'+$scope.questionData[i].txtId).getContent();
			if(!que){
				Alertify.alert("Question "+(i+1)+" cannot be not be blank");return false;
			}else if(!$scope.questionData[i].answerType){
				Alertify.alert("Question "+(i+1)+" answer type cannot be not be blank");return false;
			}else{
				if($scope.questionData[i].answerType==1){
					
					var correct=0;
					var answers=0;
				   for(var j=0;j<$scope.questionData[i].multiChoice.length;j++){
					   if($scope.questionData[i].multiChoice[j].answerText){
						   answers++
					   }
					   if($scope.questionData[i].multiChoice[j].correctAnswer){
						   correct++
					   }
				   }
				   if(answers==0){
					   Alertify.alert("Question "+(i+1)+" Answer Should not be blank.");return false;
				   }
				   if(correct==0){
					   Alertify.alert("Please Select Question "+(i+1)+"  Correct Answer");return false;
				   }
				}else if($scope.questionData[i].answerType==2){
					//alert(JSON.stringify($scope.questionData[i].multiSelect));
					var correctSelect=0;
					var answersSelect=0;
					 for(var k=0;k<$scope.questionData[i].multiSelect.length;k++){
						if($scope.questionData[i].multiSelect[k].answerText){
							   answersSelect++
						 }
						if($scope.questionData[i].multiSelect[k].correctAnswer){
							   correctSelect++
						 }
					 }
				  if(answersSelect==0){
						Alertify.alert("Question "+(i+1)+" Answer Should not be blank.");return false;
				  }
				  if(correctSelect==0){
					   Alertify.alert("Please Select Question "+(i+1)+" Correct Answer");return false;
				   }
				}else if($scope.questionData[i].answerType==3){
					if(!$scope.questionData[i].tf){
						   Alertify.alert("Please Select Question "+(i+1)+" Correct Answer");return false;
					 }
					
				}else if($scope.questionData[i].answerType==4){
					if(!$scope.questionData[i].yn){
						   Alertify.alert("Please Select Question "+(i+1)+" Correct Answer");return false;
					 }
					
				}else if($scope.questionData[i].answerType==5){
					if(!$scope.questionData[i].ak){
						   Alertify.alert("Please Select Question "+(i+1)+"  Correct Answer");return false;
					 }
					
				}
				$scope.questionData[i].question=que;
				allCorrect++;
				if(allCorrect==$scope.questionData.length){
					$scope.fifthPage=true;
					$scope.thirdPage=false;
				}
			}
		}
		}else{
			$scope.fifthPage=true;
					$scope.thirdPage=false;
		}
		/* $scope.fifthPage=true;
		$scope.thirdPage=false; */
	}	
	
	$scope.spinOption3 = {
        postfix: '%'
    }
	$scope.fourthNext=function(){
		$scope.fourthPage=false;
		$scope.fifthPage=true;
	}
	
		
	$scope.secondPrevious=function(){
		$scope.firstPage=true;
		$scope.secondPage=false;
	}
	$scope.thirdPrevious=function(){
		$scope.thirdPage=false;
		$scope.secondPage=true;
	}
	$scope.fourthPrevious=function(){
		$scope.fourthPage=false;
		$scope.thirdPage=true;
	}
	$scope.fifthPrevious=function(){
		$scope.thirdPage=true;
		$scope.fifthPage=false;
	}
	
	
	
	
	
	
$scope.tagTransformCategory = function (newTag) {
	var item = {
	category_name: newTag, 
	training_category_id: newTag, 
	};

	return item;
};	
	
        $scope.multiple.selectedGroup=[];
		$scope.multiple.selectedGroupName2=[];
		$scope.multiple.selectedUserName2=[];
	    $scope.userAllSkills=[];
		
	    var i=0;
	    $scope.addMoreSkill=function(){
		if(i<3){
			i++;
			$scope.userAllSkills.push({'id':i,'courseSkill':''});
		}else{
			Alertify.alert("You can add only three skills.");	
		}
	  }
	
	$scope.deletelevel=function(index){
		$scope.userAllSkills.splice(index,1);
		i--;
	}
	
	$scope.userSkillLevel=function(index){
		$scope.userAllSkills[index].courseSkillLevels='';
		$scope.userAllSkills[index].skillLevelCredit='';
		$scope.userAllSkills[index].creditValue='';
		
	}
	
	$scope.addSkillLevel=function(index){
		$scope.couseSkillId=$scope.userAllSkills[index].courseSkill;
		$scope.skillLevelId=$scope.userAllSkills[index].courseSkillLevels;
		$scope.userAllSkills[index].skillLevels=$rootScope.audioVideoData.skillarray[$scope.couseSkillId].levels;
		angular.forEach($scope.userAllSkills[index].skillLevels,function(levels){
			if(levels.level_id==$scope.skillLevelId){
				$scope.userAllSkills[index].skillLevelCredit=levels.skill_credit;
			}
		})	
	}
	
	$scope.checkCredit=function(index){
			if($scope.userAllSkills[index].skillLevelCredit<$scope.userAllSkills[index].creditValue){
				Alertify.alert("Credit Value must be less than or equel to max credit value");	
				$scope.userAllSkills[index].creditValue='';
			}
		}
		
	$scope.answerTypes=[];
    $scope.answerType=[];
	
	function AddRemoveTinyMce(editorId) {
			if(tinyMCE.get(editorId)) {
				tinyMCE.EditorManager.execCommand('mceFocus', false, editorId);                    
				tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, editorId);
			} else{
				tinyMCE.EditorManager.execCommand('mceAddEditor', false, editorId);			
			}
		}
		
		
	$scope.addQuestion=function(){
	
	  $scope.answerTypes[$scope.quest]='';
	  $scope.quest++;
	  var divId = 'id_'+jQuery.now();
	  var tinyClass='second'+$scope.quest;
	  $scope.questionData.push({id:$scope.quest,'multiChoice':$scope.answers,'multiSelect':$scope.answers,'randomAnswer':'','question':'','txtId':divId});
	   setTimeout(function(){
		    $scope.content = jQuery('#type-container-'+divId+' .type-row');
			console.log( $scope.content);
			element = null;
			for(var i = 0; i<1; i++){
				element = $scope.content.clone();
				element.attr('id', divId);
				element.find('.tinymce-enabled-message-new').attr('id', 'txt_'+divId);
				element.appendTo('#type_containerText_'+divId);
				AddRemoveTinyMce('txt_'+divId);
			}
       },500);
	
	
  }	
		
	$scope.setAnswerType=function(index)
	{
		 $scope.answerTypes[index]=$scope.answerType[index];
		 $scope.questionData[index].multiChoice=[];
		 $scope.questionData[index].multiSelect=[];
		 $scope.questionData[index].answerType=$scope.answerType[index];
	}	
	
	 $scope.add_fields=function(index,type)
	 {
		 if(type=='muls'){
			 $scope.questionData[index].multiSelect.push({answeId:'muls'+index,answerText:"","correctAnswer":0});
		 }else{
			 $scope.questionData[index].multiChoice.push({answeId:'mulc'+index,answerText:"","correctAnswer":0});
			/*  setTimeout(function(){
			 var divId='id_'+jQuery.now();
					 $scope.content = jQuery('#type-answercontainer-'+index+' .type-row');
					 console.log( $scope.content);
					 element = null;
					 for(var k = 0; k<1; k++){
						 element = $scope.content.clone();
						 element.attr('id', divId);
						 element.find('.tinymce-enabled-message-newanswer').attr('id', 'txt_'+divId);
						 element.appendTo('#type_answercontainerText_'+index);
						 AddRemoveTinyMce('txt_'+divId);
						 
					 }
					  },500); */
		 }
		
    }
	
	$scope.deleteAns=function(childIndex,index,type)
	{
	   if(type=='muls'){
			 $scope.questionData[index].multiSelect.splice(childIndex,1);
	     }else{
			 $scope.questionData[index].multiChoice.splice(childIndex,1);
	     }
   }
  
  $scope.deleteQuestion=function(index,questId)
  {
	  var editorId='txt_'+questId;
	  if(tinyMCE.get(editorId)) {
		  tinyMCE.EditorManager.execCommand('mceFocus', false, editorId);                    
		  tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, editorId);
		  jQuery('#txt_'+questId).remove();
	  }
	  $scope.quest--;
	  $scope.questionData.splice(index,1);
  }
  
 $scope.tagArray=[{tag:''}];	
 $scope.tagTransformTags = function (newTag) 
 {
			var item = {
			tag: newTag, 
			tag: newTag, 
			};
			return item;
 };
 
  $scope.randomized=0;
  $scope.diplayView=0;
  $scope.allowLearner=0;
  $scope.allowtrans=0;
  $scope.point_show=0;
  $scope.credit_show=0;
  
  $scope.finalSubmit=function(){
	 
		var course_pic_data=courseImage.files.item(0);
			if(course_pic_data){
				var isCourseImage=1;
			}else{
				var isCourseImage=0;
			}
			$scope.checkNotification=[];
			angular.forEach($rootScope.audioVideoData.notification,function(list){
				if(list.checkNotification==true){
					$scope.checkNotification.push(list.notification_id);
				}
		})
			
			
			var course={
				'credit_value':$scope.credit_value,
				'points':$scope.points,
				'status_id':1,
				'certificate_template_id':$scope.certificate,
				'point_show':$scope.point_show,
				'credit_show':$scope.credit_show
				};
				
			var courseDetail={
				'course_name':$scope.courseName,
				'course_title':$scope.activityTitle,
				'course_description':$scope.courseDescription
			};
			
			var trainingPrograms={
				'training_type':5,
				'is_internal':1,
				'media_type_id':00,
				'status_id':$scope.status
			};		
			
			if($scope.showNotification==0){
				$scope.checkNotification=[];
			}
		
		  var assessment={
			  pass_percentage:$scope.passper,
			  hide_assessonrequ:$scope.hideRecord,
			  allow_assessment:$scope.allowLearner,
			  allow_transcript:$scope.allowtrans,
			  randomize_ques:$scope.randomized,
			  view_mode:$scope.diplayView,
			  post_quiz_message:$scope.psmessage,
			  post_quiz_action:$scope.pqaction,
			  attempts:$scope.attempts,
			  time_limit:$scope.hourse+":"+$scope.minutes,
			  postQuizMessage:$scope.psmessage,
		  }
 		
			
			$http({
				method:"POST",
				url:"/admin/saveAssessment",
				headers: {'Content-Type': undefined },
				transformRequest:function(){
					var formData=new FormData();
					formData.append('course',angular.toJson(course));
					formData.append('courseDetail', angular.toJson(courseDetail));
					formData.append('trainingPrograms',angular.toJson(trainingPrograms));
					formData.append('assessment',angular.toJson(assessment));
					formData.append('isCourseImage',isCourseImage);
					formData.append('userAllSkills',angular.toJson($scope.userAllSkills));
					formData.append('categories',angular.toJson($scope.multiple.selectedCategoryName));
					formData.append('checkNotification',angular.toJson($scope.checkNotification));
					formData.append('courseImage',course_pic_data);
					formData.append('referenceCode',$scope.referenceCode);
					formData.append('targetAudienceGroups',angular.toJson($scope.multiple.selectedGroup));
					formData.append('targetAudienceusers',angular.toJson($scope.multiple.selectedUserName1));
					formData.append('permissionGroups',angular.toJson($scope.multiple.selectedGroupName2));
					formData.append('permissionUsers',angular.toJson($scope.multiple.selectedUserName2));
					formData.append('courseTags',angular.toJson($scope.multiple.selectedTag));
					formData.append('question',angular.toJson($scope.questionData));
					return formData;
				}
			}).success(function(response){
				$scope.loader=false;
				if(response.status==201){
					Alertify.success('Course Addedd Successfully.');
					$scope.trainingCode=response.trainingCode;
					$scope.uploader.onBeforeUploadItem = function(item) {
								item.formData.push({courseId:response.courseId});	
					};
					if(uploader.queue.length>0){
						$scope.uploader.uploadAll();
					}else{
						
						$location.path('/courses/index');
						//$location.path('/courses/editassessment/'+response.trainingCode);
					}
                     
					 uploader.onCompleteAll = function(progress) {
						
						$location.path('/courses/index');
						//$location.path('/courses/editassessment/'+$scope.trainingCode);
					};
							
							
					//$location.path('/courses/editelearning/'+response.trainingCode);
					//$window.location.reload();
				}
				//alert(JSON.stringify(response));
			})
			
			
  }
 
  $scope.courseImageFunc = function(element)
		{

			var reader = new FileReader();
			reader.onload = function(e){
				$scope.$apply(function(){
					$scope.courseImageData = e.target.result;
					$scope.theFile = element.files[0];
				});
			}
			reader.readAsDataURL(element.files[0]);
			
		};
		
		
		$scope.checkOtherFalse=function(index,parentIndex){
			if($scope.questionData[parentIndex].answerType==1){
				for(var i=0;i<$scope.questionData[parentIndex].multiChoice.length;i++){
					if(index!=i){
						$scope.questionData[parentIndex].multiChoice[i].correctAnswer=0;
					}
				}
			}
			
			//alert(JSON.stringify($scope.questionData));
		}
  
 $scope.mathGenerate=function(){
	 
	  $('#qusetionModel').modal('show');
 }
 
     $scope.submitCode=function(){alert('d');
		 $scope.generateCode=tinyMCE.get('textAreaName').getContent()
	 }
		
  angular.module('tabsDemoDynamicHeight', ['ngMaterial']);
  
	    
});	

assessmentmodule.controller('EditAssessmentController', function($scope,$rootScope,$location,$localStorage,$http,Alertify,FileUploader,$stateParams){
	
	     $scope.trainingCode=$stateParams.courseCode;
		 $scope.multiple={
				 SelectedCategoryName:[],
				 selectedGroup:[]
			 };
		
			 
			  var HandOut = $scope.HandOut = new FileUploader({
			   url:'/admin/uploadHandout',
			   alias:'handoutFile',
			});
			
		  console.info('uploader', HandOut);
		  HandOut.onAfterAddingFile = function(fileItem) {
				console.info('onAfterAddingFile',fileItem);
			};

       $scope.removeHandoutQueue=function(index){
			HandOut.queue.splice(index,1);
		}
		
		
			 $scope.tagArray=[{
				 course_tag_id:'',
				tags:''
			 }]
			 
			 $scope.tagTransformCategories = function (newTag) {
				var item = {
					category_name: newTag, 
					training_category_id: newTag, 
				};
				return item;
			}; 
			
			$scope.tagTransformTags = function (newTag) {
				var item = {	
					course_tag_id: newTag, 
					tags: newTag, 
				};
				return item;
			};
			
			function AddRemoveTinyMce(editorId) {
			if(tinyMCE.get(editorId)) {
				tinyMCE.EditorManager.execCommand('mceFocus', false, editorId);                    
				tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, editorId);
			} else{
				tinyMCE.EditorManager.execCommand('mceAddEditor', false, editorId);			
			}
		}
				 
		 
	     $scope.getAssessmentData=function(){
			 $http({
				 method:"POST",
				 url:"/admin/getAssessmentData",
				 data:{code:$scope.trainingCode}
			 }).success(function(response){
				 $scope.elearningData=response.data;
				 $scope.courseImageData=$scope.elearningData.course_image;
				 $scope.multiple.SelectedCategoryName=response.data.couseCategory;
				 $scope.categories=$rootScope.audioVideoData.traningcategoryType;
				 $scope.userAllSkills= $scope.elearningData.courseSkill;
				 
				 $scope.quest=$scope.userAllSkills.length;
				 setTimeout(function(){
				 for(var i=0;i<$scope.elearningData.questionAnswer.length;i++){
					 var divId='id_'+$scope.elearningData.questionAnswer[i].questionId;
					 $scope.elearningData.questionAnswer[i].newQuestionId=divId;
					 $scope.content = jQuery('#type-container-'+$scope.elearningData.questionAnswer[i].questionId+' .type-row');
					 console.log( $scope.content);
					 element = null;
					 for(var k = 0; k<1; k++){
						 element = $scope.content.clone();
						 element.attr('id', divId);
						 element.find('.tinymce-enabled-message-new').attr('id', 'txt_'+divId);
						 element.appendTo('#type_containerText_'+$scope.elearningData.questionAnswer[i].questionId);
						 AddRemoveTinyMce('txt_'+divId);
						 
					 }
				 }
				 },1000);
			 })
		 }
		 $scope.getAssessmentData();
		 $scope.tab1=true;
		 $scope.tab2=false;
		 $scope.tab3=false;
		 $scope.tab4=false;
		 $scope.tab5=false;
		 $scope.addCourseFirstData=function(){
				 if(!$scope.elearningData.course_name){
					 Alertify.alert("Course Name is required.");		
				 }else if($scope.multiple.SelectedCategoryName.length<1){
					 Alertify.alert("Training category is required.");
				 }else{
					 $scope.tab1=false;
					 $scope.tab2=true;
				 }
			 }
			 
			 $scope.previousFirst=function(){
				
				 $scope.tab1=true;
				 $scope.tab2=false;
			 } 
			 $scope.previousThirdPage=function(){
				 $scope.tab2=true;
				 $scope.tab3=false;
			 }
			 $scope.previousFourthPage=function(){
				 $scope.selectedTab=2;
			 }
			 
			 
			 $scope.secondNext=function(){
				
				$scope.tab2=false;
				$scope.tab3=true;
		
			 }
			
			/* $scope.nextThirdPage=function(){
				 $scope.tab3=false;
				 $scope.tab4=true;
				
			 } */
			 
			 $scope.showNotification=1;
		$scope.showNoticationOption=function(value){
			$scope.showNotification=value;
		}
		
		
		$scope.deleteQuestion=function(index,questionId)
		 {
			   var editorId='txt_'+questionId;
	           if(tinyMCE.get(editorId)) {
				tinyMCE.EditorManager.execCommand('mceFocus', false, editorId);                    
				tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, editorId);
				 jQuery('#'+editorId).remove();
				 /* $scope.quest--;
	                $scope.questionData.splice(index,1); */
	    }
	    $scope.elearningData.questionAnswer.splice(index,1);
	}
		 
		 $scope.deleteAns=function(childIndex,index,type){
		   if(type=='muls'){
				 $scope.elearningData.questionAnswer[index].multiSelect.splice(childIndex,1);
			 }else{
				$scope.elearningData.questionAnswer[index].multiChoice.splice(childIndex,1);
			 }
         }
		 
		 $scope.add_fields=function(index,type)
		 {
			 if(type=='muls'){
				$scope.elearningData.questionAnswer[index].multiSelect.push({answeId:'muls'+index,answerText:"","correctAnswer":0});
			 }else{
				 $scope.elearningData.questionAnswer[index].multiChoice.push({answeId:'mulc'+index,answerText:"","correctAnswer":0});
			 }
		}
		
		
		
		$scope.nextThirdPage=function(){
		var allCorrect=0;
		
		if($scope.elearningData.questionAnswer.length>0){
		for(var i=0;i<$scope.elearningData.questionAnswer.length;i++){
			var que=tinyMCE.get('txt_'+$scope.elearningData.questionAnswer[i].newQuestionId).getContent();
			if(!que){
				Alertify.alert("Question"+(i+1)+" cannot be not be blank");return false;
			}else if(!$scope.elearningData.questionAnswer[i].answerType){
				Alertify.alert("Question"+(i+1)+" answer type cannot be not be blank");return false;
			}else{
				if($scope.elearningData.questionAnswer[i].answerType==1){
					
					var correct=0;
					var answers=0;
				   for(var j=0;j<$scope.elearningData.questionAnswer[i].multiChoice.length;j++){
					   if($scope.elearningData.questionAnswer[i].multiChoice[j].answerText){
						   answers++
					   }
					   if($scope.elearningData.questionAnswer[i].multiChoice[j].correctAnswer){
						   correct++
					   }
				   }
				   if(answers==0){
					   Alertify.alert("Question"+(i+1)+" Answer Should not be blank.");return false;
				   }
				   if(correct==0){
					   Alertify.alert("Please Select Question"+(i+1)+"  Correct Answer");return false;
				   }
				}else if($scope.elearningData.questionAnswer[i].answerType==2){
					//alert(JSON.stringify($scope.questionData[i].multiSelect));
					var correctSelect=0;
					var answersSelect=0;
					 for(var k=0;k<$scope.elearningData.questionAnswer[i].multiSelect.length;k++){
						if($scope.elearningData.questionAnswer[i].multiSelect[k].answerText){
							   answersSelect++
						 }
						if($scope.elearningData.questionAnswer[i].multiSelect[k].correctAnswer){
							   correctSelect++
						 }
					 }
				  if(answersSelect==0){
						Alertify.alert("Question"+(i+1)+" Answer Should not be blank.");return false;
				  }
				  if(correctSelect==0){
					   Alertify.alert("Please Select Question"+(i+1)+"  Correct Answer");return false;
				   }
				}else if($scope.elearningData.questionAnswer[i].answerType==3){
					if($scope.elearningData.questionAnswer[i].tf==undefined){
						   Alertify.alert("Please Select Question"+(i+1)+"  Correct Answer");return false;
					 }
				}else if($scope.elearningData.questionAnswer[i].answerType==4){
					if($scope.elearningData.questionAnswer[i].yn==undefined){
						   Alertify.alert("Please Select Question"+(i+1)+"  Correct Answer");return false;
					 }
				}else if($scope.elearningData.questionAnswer[i].answerType==5){
					if(!$scope.elearningData.questionAnswer[i].ak){
						   Alertify.alert("Please Select Question"+(i+1)+"  Correct Answer");return false;
					 }
				}
				$scope.elearningData.questionAnswer[i].question=que;
				allCorrect++;
				if(allCorrect==$scope.elearningData.questionAnswer.length){
					$scope.tab3=false;
					$scope.tab4=true;
				}
			}
		}
		}else{
			    $scope.tab3=true;
				// $scope.tab4=true;
		}
		/* $scope.fifthPage=true;
		$scope.thirdPage=false; */
	}	
	
	$scope.answers=[];
	$scope.addQuestion=function(){
		 var divIdTime = jQuery.now();
		 var divId = 'id_'+divIdTime;
	  $scope.quest++;
	  $scope.elearningData.questionAnswer.push({id:$scope.quest,'multiChoice':$scope.answers,'multiSelect':$scope.answers,'randomAnswer':'','question':'','questionId':divId,'newQuestionId':divId});
	  
	  setTimeout(function(){
		 $scope.content = jQuery('#type-container-'+divId+' .type-row');
	     console.log($scope.content);
			element = null;
			for(var i = 0; i<1; i++){
				element = $scope.content.clone();
				 console.log(element);
				element.attr('id', divId);
				element.find('.tinymce-enabled-message-new').attr('id', 'txt_'+divId);
				element.appendTo('#type_containerText_'+divId);
				AddRemoveTinyMce('txt_'+divId);
			}
},500);
	 /*  alert(JSON.stringify($scope.questionData)); */
  }
  
  
         $scope.finalSubmit=function(){
			    if(!$scope.elearningData.course_name){
					 Alertify.alert("Course Name is required.");
				 }else if($scope.multiple.SelectedCategoryName.length<1){
					 Alertify.alert("Training category is required.");
				 }else{
					  $scope.loader=true;
					 var course_pic_data=courseImage.files.item(0);
						if(course_pic_data){
							var isCourseImage=1;
						}else{
							var isCourseImage=0;
						}
						
						$scope.checkNotification=[];
						angular.forEach($scope.elearningData.notification,function(list){
							if(list.checkNotification==1){
								$scope.checkNotification.push(list.notification_id);
							}
						})
						if($scope.showNotification==0){
							$scope.checkNotification=[];
						}
						
						var course={
							'credit_value':$scope.elearningData.credit_value,
							'points':$scope.elearningData.points,
							'media_library_id':$scope.mediaLibraryId,
							'status_id':1,
							'certificate_template_id':$scope.elearningData.certificate_template_id
							};
							
						var courseDetail={
							'course_name':$scope.elearningData.course_name,
							'course_title':$scope.elearningData.course_title,
							'course_description':$scope.elearningData.course_description
						};
						
						var trainingPrograms={
							'status_id':$scope.elearningData.status_id
						};
						var assessment={
						  pass_percentage:$scope.elearningData.assessmentData.pass_percentage,
						  hide_assessonrequ:$scope.elearningData.assessmentData.hide_assessonrequ,
						  allow_assessment:$scope.elearningData.assessmentData.allow_assessment,
						  allow_transcript:$scope.elearningData.assessmentData.allow_transcript,
						  randomize_ques:$scope.elearningData.assessmentData.randomize_ques,
						  view_mode:$scope.elearningData.assessmentData.view_mode,
						  post_quiz_message:$scope.elearningData.assessmentData.post_quiz_message,
						  post_quiz_action:$scope.elearningData.assessmentData.post_quiz_action,
						  attempts:$scope.elearningData.assessmentData.attempts,
						  time_limit:$scope.elearningData.assessmentData.hours+":"+$scope.elearningData.assessmentData.minutes,
						  postQuizMessage:$scope.elearningData.assessmentData.postQuizMessage,
						  score_type_id:5
					  }
					  
					  
					  
					  $http({
						method:"POST",
						url:"/admin/editAssessment",
						headers: {'Content-Type': undefined },
						transformRequest:function(){
							var formData=new FormData();
							formData.append('code',$scope.trainingCode);
							formData.append('course',angular.toJson(course));
							formData.append('courseDetail', angular.toJson(courseDetail));
							formData.append('trainingPrograms',angular.toJson(trainingPrograms));
							formData.append('assessment',angular.toJson(assessment));
							formData.append('isCourseImage',isCourseImage);
							formData.append('userAllSkills',angular.toJson($scope.userAllSkills));
							formData.append('categories',angular.toJson($scope.multiple.SelectedCategoryName));
							formData.append('checkNotification',angular.toJson($scope.checkNotification));
							formData.append('courseImage',course_pic_data);
					        formData.append('targetAudienceGroups',angular.toJson($scope.elearningData.groupAudienceTag));
							formData.append('targetAudienceusers',angular.toJson($scope.elearningData.userAudienceTag));
							formData.append('permissionGroups',angular.toJson($scope.elearningData.groupEditTag));
							formData.append('permissionUsers',angular.toJson($scope.elearningData.userEditTag));
							formData.append('courseTags',angular.toJson($scope.elearningData.courseTags));
							formData.append('question',angular.toJson($scope.elearningData.questionAnswer));
							return formData;
						}
					}).success(function(response){
						$scope.loader=false;
						if(response.status==201){
							
							//$scope.trainingCode=response.trainingCode;
							$scope.HandOut.onBeforeUploadItem = function(item) {
										item.formData.push({courseId:response.courseId});	
							};
							if(HandOut.queue.length>0){
								$scope.HandOut.uploadAll();
							}else{
								Alertify.success('Course has been updated successfully');
								//$location.path('/courses/editassessment/'+response.trainingCode);
							}
							 
							 HandOut.onCompleteAll = function(progress) {
								Alertify.success('Course has been updated successfully');
								//$location.path('/courses/editassessment/'+$scope.trainingCode);
							};
									
									
							//$location.path('/courses/editelearning/'+response.trainingCode);
							//$window.location.reload();
						}
						//alert(JSON.stringify(response));
					})
			
			
					 
				 }
	  
	  
		}
		
		 $scope.courseImageFunc = function(element){
				var reader = new FileReader();
				reader.onload = function(e){
					$scope.$apply(function(){
						$scope.courseImageData = e.target.result;
						$scope.theFile = element.files[0];
					});
				}
				reader.readAsDataURL(element.files[0]);
				
			};
			
			 $scope.classroomCourseData=function(){
			$http({
				method:"POST",
				url:"/admin/getClassRoomCourseData"
			}).success(function(response){
				$scope.classroomAddData=response;
				//alert(JSON.stringify(response));
			})
		}
		$scope.classroomCourseData();
		
		$scope.checkOtherFalse=function(index,parentIndex){
			if($scope.elearningData.questionAnswer[parentIndex].answerType==1){
				for(var i=0;i<$scope.elearningData.questionAnswer[parentIndex].multiChoice.length;i++){
					if(index!=i){
						$scope.elearningData.questionAnswer[parentIndex].multiChoice[i].correctAnswer=0;
					}
				}
			}
			
			//alert(JSON.stringify($scope.questionData));
		}
		
		
		$scope.userSkillLevel=function(index){
			$scope.userAllSkills[index].level_id='';
			$scope.userAllSkills[index].skill_level_credit='';
			$scope.userAllSkills[index].course_credit='';
			
		}
		
		$scope.addSkillLevel=function(index){
			$scope.couseSkillId=$scope.userAllSkills[index].skill_id;
			$scope.skillLevelId=$scope.userAllSkills[index].level_id;
			$scope.userAllSkills[index].skillLevels=$rootScope.audioVideoData.skillarray[$scope.couseSkillId].levels;
			angular.forEach($scope.userAllSkills[index].skillLevels,function(levels){
				if(levels.level_id==$scope.skillLevelId){
					$scope.userAllSkills[index].skill_level_credit=levels.skill_credit;
				}
			})
		}
		
		$scope.checkCredit=function(index){
			if($scope.userAllSkills[index].skill_level_credit<$scope.userAllSkills[index].course_credit){
				Alertify.alert("Credit Value must be less than or equel to max credit value");	
				$scope.userAllSkills[index].course_credit='';
			}
		}
		
		$scope.deletelevel=function(userSkill){
			var index = $scope.userAllSkills.indexOf(userSkill);
            $scope.userAllSkills.splice(index, 1);
			$scope.i--;
		}
		
		$scope.addMoreSkill=function(){
			if($scope.i<3){
				$scope.i++;
				$scope.userAllSkills.push({'skill_id':'','level_id':'','skill_credit':'','course_credit':''});	
			}else{
				Alertify.alert("You can add only three skills.");	
			}
		}
  
			$scope.mathGenerate=function(){
	  $('#qusetionModel').modal('show');
 }
 //$scope.generateCode='';
     $scope.submitCode=function(){alert('d');
		 $scope.generateCode=tinyMCE.get('textAreaName').getContent()
	 }
		 
			 
			 
	
});







