
var releasemodule = angular.module("releasemodule",[]);
/* releasemodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("surealeasereleases", {
		url:"/surealease/releases",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/SuperAdmin/release.html',
				controller: 'ReleaseController'
			}
		},
	});	
	
}]);
	 */
	
releasemodule.controller('ReleaseController', function($scope,$location,$localStorage,$http,Alertify,$window){
	$scope.addRelease=0;
	$scope.addeditReleaseButton=0;
	$scope.submitform=0;
	$scope.sumupdatfom=0;
	$scope.getReleseData=function(){
		 $http({
			 method:"POST",
			 url:"/admin/surealease/allreleases"
		 }).success(function(response){
			 $scope.gridOptions.data=response.gridData;
		 })
	 }
	 
	   $scope.getReleseData();
	   
	   
	    $scope.gridOptions = { 
				  //data :'releaseData',
				 cellEditableCondition: true,
				enableFiltering: true,

				 paginationPageSize:10,
				 paginationPageSizes: [10, 20, 30],
				paginationPageSize: 10,
						rowHeight: 40,
				onRegisterApi : function(gridApi){
					
				  $scope.gridApis= gridApi;
				 // $scope.gridOptions.gridApis.refresh()
				},

				 
				 columnDefs: [	
				  { name:'date',displayName: 'Date' ,width: "25%"},
				  { name:'title',displayName: 'Title'},
				  { name:'release',displayName: 'Release'},
				  {name:'view', displayName:'Action',cellTemplate:"view.html" ,enableFiltering: false},		
				  {name:'delete', displayName:'Delete',cellTemplate:"delete.html" ,enableFiltering: false},		
				]
		 
		 };
		 
		 $scope.addreleasenote=function(){
			 $scope.releasedate='';
			 $scope.version='';
			 $scope.releasetitle='';
		 	 tinymce.get('desc_text').setContent('');
			 tinymce.get('short_desc').setContent('');
			 $scope.addRelease=1;
			 $scope.addeditReleaseButton=1;
			 $scope.submitform=1;
		 }
		 
		 $scope.submitUpdateForm=function(operation){
			 if(!$scope.editid){
				 $scope.editid='';
			 }
			 var id=$scope.editid;
			 var date=$scope.releasedate;
			 var version=$scope.version;
			 var title=$scope.releasetitle;
			
				if(date == undefined)
				{
					Alertify.alert("Please enter Date");
					return false;
				}
				if(title == undefined)
				{
					Alertify.alert("Please enter Title");
					return false;
				}
				title=title.replace(/§/g,"dbl_s");
				title=title.replace("&","and");
				title=title.replace("(","smlBracOpn");
				title=title.replace(")","smlBracCls");
				title=title.replace("[","bigBracOpn");
				title=title.replace("]","bigBracCls");
				title=title.replace(/#/g,"hash");
				title=title.replace("$","dollar");
				title=title.replace("*","star");
				title=title.replace("?","quesmark");
				title=title.replace("+","plusmark");
				title=title.replace("%","permark");
				title=title.replace(/\//gi,"bckslsh");
				title=title.replace(/&/g, "namp;");
				title=title.replace(/>/g, "ngt;");
				title=title.replace(/</g, "nlt;");
				title=title.replace(/"/g, "nquot;");
				title=title.replace(/'/g, "singleqoute");
				var short_desc=tinyMCE.get('short_desc').getContent();
				var desc=tinyMCE.get('desc_text').getContent();
			
			
				$http({
					method:"POST",
					url:"/admin/surealease/addeditreleasenotes",
					data:{type:operation,date:date,title:title,id:id,version:version,description:desc,short_desc:short_desc}
				}).success(function(response){
					Alertify.alert((response));
					$window.location.reload();
				})
			 
		 }
		 
		 
		 $scope.releaseEdit=function(id){
			 $scope.editid=id;
			 $http({
				 method:"POST",
				 url:"/admin/surealease/releasenotevalbyid",
				 data:{id:id}
			 }).success(function(response){
				 $scope.addRelease=1;
				 $scope.addeditReleaseButton=1;
				 $scope.sumupdatfom=1;
				 $scope.releasedate=response.release_date;
				 $scope.version=response.version;
				 $scope.releasetitle=response.title;
				 tinymce.get('desc_text').setContent(response.description);
				 tinymce.get('short_desc').setContent(response.short_desc);
				 //alert(JSON.stringify(response));
			 })
		 }
		 
		 $scope.cancelhomeannounce=function(){
			 $scope.releasedate='';
			 $scope.addRelease=0;
			 $scope.addeditReleaseButton=0;
			 $scope.submitform=0;
			 $scope.sumupdatfom=0;
		 }
		 
		 $scope.publish_submit=function(id,isActive,row){
			 $http({
				 method:"POST",
				 url:"/admin/surealease/publish",
				 data:{id:id,isActive:isActive}
			 }).success(function(response){
				 row.entity.is_active=isActive;
				
			 })
		 }
		 
		 $scope.releaseDel=function(id,row){
			 $http({
				 method:"POST",
				 url:"/admin/surealease/deleterelease",
				 data:{id:id}
			 }).success(function(response){
				 Alertify.alert(('Deleted successfully'));
				 var index = $scope.gridOptions.data.indexOf(row.entity);
				$scope.gridOptions.data.splice(index, 1);
			 })
		 }
		 
		
		 
	 
	 
	   

	    
});	
