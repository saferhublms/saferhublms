
var notificationmgmtmodule = angular.module("notificationmgmtmodule",[]);
/* notificationmgmtmodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("notificationmanagements", {
		url:"/notificationmanagements/notificationmgts",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/SuperAdmin/notificationmgts.html',
				controller: 'NotificationmgtsController'
			}
		},
	});	
	
	$stateProvider.state("notificationmanagementsedit", {
		url:"/notificationmanagements/edit/:notificationId",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/SuperAdmin/notificationmgtsedit.html',
				controller: 'NotificationmgtsEditController'
			}
		},
	});	
	
	
}]);
	 */
	
notificationmgmtmodule.controller('NotificationmgtsController', function($scope,$location,$localStorage,$http,Alertify,$window){

					$scope.viewList=true
					$scope.getNotificationData=function(){
					 $http({
						 method:"POST",
						 url:"/admin/notificationmanagements/allnotificationmgts"
					 }).success(function(response){
						 $scope.initialData=response;
						 $scope.gridOptions.data=response.gridData;
					 })
				 }
	 
					$scope.getNotificationData();
					
					
					$scope.gridOptions = { 
				  //data :'releaseData',
				 cellEditableCondition: true,
				enableFiltering: true,

				 paginationPageSize:10,
				 paginationPageSizes: [10, 20, 30],
				paginationPageSize: 10,
						rowHeight: 40,
				onRegisterApi : function(gridApi){
					
				  $scope.gridApis= gridApi;
				 // $scope.gridOptions.gridApis.refresh()
				},

				 
				 columnDefs: [	
				 
				  { name:'notification_name',displayName: 'Notification Name'},
				  
				  {name:'view', displayName:'Status',cellTemplate:"view.html" ,enableFiltering: false},		
				  {name:'delete', displayName:'Action',cellTemplate:"delete.html" ,enableFiltering: false},		
				]
		 
		 };
		 
		 
		 $scope.shownotification=function(id,checkboxData){
			 if(checkboxData==true) {
				var checkedfld = 1;
			} else {
				var checkedfld = 0;
			}
	          $http({
				  method:"POST",
				  url:"/admin/notificationmanagements/shownotification",
				  data:{notification_id:id,hideshow:checkedfld}
			  }).success(function(response){
				  Alertify.success('Status updated successfully');
			  })
		 }
		 
		 
		 $scope.editNotification=function(NotificationId){
			 $window.location.href="#/notificationmanagements/edit/"+NotificationId;
		 }
		 
		 $scope.cancelEdit=function(){
			 $scope.viewEdit=false;
			 $scope.viewList=true;
		 }
		 
		
		 
	   
	    
});	

notificationmgmtmodule.controller('NotificationmgtsEditController', function($scope,$location,$localStorage,$http,Alertify,$stateParams,$timeout,$window){
	
	$scope.NotificationId=$stateParams.notificationId;
    $scope.resultDataEditL=[];
	$scope.editNotification=function(){
		
			 $http({
				 method:"POST",
				 url:"/admin/notificationmanagements/edit",
				 data:{notification_id:$scope.NotificationId,is_default:1}
			 }).success(function(response){
				 $scope.editData=response;
				 $scope.resultDataEditL=response.result;
				
				 
				 //alert(response);
			 })
		 }
		 
		 $scope.editNotification();
		 
		 
		 $scope.updateNotification=function(){
			 if(!$scope.editData.notification[0].notification_name){
				 Alertify.alert('Notification title is required');
			 }else{
				 var desc=tinyMCE.get('desc').getContent();
				 $scope.editData.notification[0].notification_content=desc;
				 $http({
					 method:"POST",
					 url:"/admin/notificationmanagements/updatenotification",
					 data:{notificationData:$scope.editData.notification,notification_id:$scope.NotificationId}
				 }).success(function(response){
					 $window.location.reload();
					 $window.location.href="#/notificationmanagements/notificationmgts";
					 
				 })
			 }
		 }
		 
		 $scope.cancelNotification=function(){
			 $window.location.reload();
			 $window.location.href="#/notificationmanagements/notificationmgts";
		 }
		 
		
		
		 
		 
		 
		 
});


notificationmgmtmodule.controller('SystemManageNotificationController', function($scope,$location,$localStorage,$http,Alertify,$window){
	
	
	$scope.getNotification=function(){
		$http({
			method:"POST",
			url:"/admin/notificationtemplate/getnotifications"
		}).success(function(response){
			$scope.notificationList=response;
			for(var i=0;i<$scope.notificationList.data.length;i++){
				if($scope.notificationList.data[i].is_active==1){
					$scope.notificationList.data[i].is_active=true;
				}else{
					$scope.notificationList.data[i].is_active=false;
				}
			}
			console.log($scope.notificationList);
			
		})
	}
	   
	   $scope.getNotification();
	   
	   
	   $scope.updateNotification=function(index){
		   $scope.notificationData=$scope.notificationList.data[index];
		   $scope.notificationStatus=($scope.notificationData.is_active==true)?1:0;
		   $http({
			   method:"POST",
			   url:"/admin/notificationtemplate/updatestatus",
			   data:{notificationId:$scope.notificationData.notification_id,status:$scope.notificationStatus}
		   }).success(function(response){
			  Alertify.success('Status updated successfully.');
		   })
		   
	   }
	   
	   $scope.restoredefault=function(notificationId){
			 $http({
				 method:"POST",
				 url:"/admin/notificationtemplate/resoredefault",
				 data:{notificationId:notificationId}
			 }).success(function(response){
				 Alertify.success('Restored successfully.');
			 })
		 }
		 
	    
	
});


notificationmgmtmodule.controller('NotificatioEditController', function($scope,$location,$localStorage,$http,Alertify,$window,$stateParams){
	$scope.NotificationId=$stateParams.notificationId;
    $scope.resultDataEditL=[];
	
	$scope.editNotification=function(){
		
			 $http({
				 method:"POST",
				 url:"/admin/notificationmanagements/edit",
				 data:{notification_id:$scope.NotificationId,is_default:1}
			 }).success(function(response){
				 $scope.editData=response;
				 $scope.resultDataEditL=response.result;
				
				 
				 //alert(response);
			 })
		 }
		 
		 $scope.editNotification();
		 
		  $scope.updateNotification=function(){
			 if(!$scope.editData.notification[0].notification_name){
				 Alertify.alert('Notification title is required');
			 }else{
				 var desc=tinyMCE.get('desc').getContent();
				 $scope.editData.notification[0].notification_content=desc;
				 $http({
					 method:"POST",
					 url:"/admin/notificationmanagements/updatenotification",
					 data:{notificationData:$scope.editData.notification,notification_id:$scope.NotificationId}
				 }).success(function(response){
					 $window.location.reload();
					 $window.location.href="#/notificationtemplate/notificationmgt";
					 
				 })
			 }
		 }
		 
		 
		  $scope.cancelNotification=function(){
			 $window.location.reload();
			 $window.location.href="#/notificationtemplate/notificationmgt";
		 }
		 
		 
		
		 
		 
	
});

