var classroomcoursemodule = angular.module("classroomcoursemodule",[]);

classroomcoursemodule.controller('EditClassroomCoursesController',function($scope,$http,$location,$stateParams,FileUploader,$rootScope,Alertify){
	   
		 $scope.multiple={
				 SelectedCategoryName:[],
				 selectedGroup:[]
			 };
			 
			  var HandOut = $scope.HandOut = new FileUploader({
			   url:'/admin/uploadHandout',
			   alias:'handoutFile',
			});
			
		  console.info('uploader', HandOut);
		  HandOut.onAfterAddingFile = function(fileItem) {
				console.info('onAfterAddingFile',fileItem);
			};

       $scope.removeHandoutQueue=function(index){
			HandOut.queue.splice(index,1);
		}
			 
			 $scope.selectedTab=0;
			 $scope.trainingCode=$stateParams.courseCode;
			 $scope.tagArray=[{
				 course_tag_id:'',
				tags:''
			 }]
			 
			 $scope.tagTransformCategories = function (newTag) {
				var item = {
				category_name: newTag, 
				training_category_id: newTag, 
				};

				return item;
			}; 
			
			$scope.tagTransformTags = function (newTag) {
				var item = {
					
					course_tag_id: newTag, 
					tags: newTag, 
				};

				return item;
			};
			//end
			
			 $scope.getcoursedetail=function()
			 {
				 $http({
					 method:"POST",
					 url:"/admin/getClassroomdata",
					 data:{code:$scope.trainingCode}
				 }).success(function(response){
					  $scope.elearningData=response.data;
					  $scope.courseImageData=$scope.elearningData.course_image;
					  $scope.multiple.SelectedCategoryName=response.data.couseCategory;
					  $scope.multiple.selectedGroup=response.data.groupAudienceTag;
					  $scope.categories=$rootScope.audioVideoData.traningcategoryType;
					  $scope.userAllSkills= $scope.elearningData.courseSkill;
					  $scope.i=$scope.userAllSkills.length;
					 /*  $timeout(function(){
						 tinymce.get('desc_text').setContent($scope.elearningData.course_description);
					 },1000) */
					  
					// alert(JSON.stringify(response));
				 })
			 }
			 
			 
			 $scope.getcoursedetail();
			 
			 
			 $scope.userSkillLevel=function(index){
			$scope.userAllSkills[index].level_id='';
			$scope.userAllSkills[index].skill_level_credit='';
			$scope.userAllSkills[index].course_credit='';
			
		}
		
		$scope.addSkillLevel=function(index){
			$scope.couseSkillId=$scope.userAllSkills[index].skill_id;
			$scope.skillLevelId=$scope.userAllSkills[index].level_id;
			$scope.userAllSkills[index].skillLevels=$rootScope.audioVideoData.skillarray[$scope.couseSkillId].levels;
			angular.forEach($scope.userAllSkills[index].skillLevels,function(levels){
				if(levels.level_id==$scope.skillLevelId){
					$scope.userAllSkills[index].skill_level_credit=levels.skill_credit;
				}
			})	
		}
		
		$scope.checkCredit=function(index){
			if($scope.userAllSkills[index].skill_level_credit<$scope.userAllSkills[index].course_credit){
				Alertify.alert("Credit Value must be less than or equel to max credit value");	
				$scope.userAllSkills[index].course_credit='';
			}
		}
		
		$scope.deletelevel=function(userSkill){
			var index = $scope.userAllSkills.indexOf(userSkill);
            $scope.userAllSkills.splice(index, 1);
			$scope.i--;
		}
		
		$scope.addMoreSkill=function(){
			if($scope.i<3){
				$scope.i++;
				$scope.userAllSkills.push({'skill_id':'','level_id':'','skill_credit':'','course_credit':''});	
			}else{
				Alertify.alert("You can add only three skills.");	
			}
		}
		
		
		$scope.showNotification=1;
		$scope.showNoticationOption=function(value){
			$scope.showNotification=value;
		}
		
		$scope.removeHandOut=function(handoutId,index){
			$http({
				method:"POST",
				url:"/admin/removeHandout",
				data:{handoutId:handoutId}
			}).success(function(response){
				$scope.elearningData.handouts.splice(index,1);
			})
		}	
				 $scope.addCourseFirstData=function(){

				 if(!$scope.elearningData.course_name){
					 Alertify.alert("Course Name is required.");		
				 }else if($scope.multiple.SelectedCategoryName.length<1){
					 Alertify.alert("Training category is required.");
				 }else{
					 
					 $scope.selectedTab=1;
				 }
			 }
			 
			 $scope.previousThirdPage=function(){
				 $scope.selectedTab=0;
			 } 
			 
			 $scope.finalSubmit=function(){
				 if(!$scope.elearningData.course_name){
					 Alertify.alert("Course Name is required.");		
				 }else if($scope.multiple.SelectedCategoryName.length<1){
					 Alertify.alert("Training category is required.");
				 }else{
					    var course_pic_data=courseImage.files.item(0);
						if(course_pic_data){
							var isCourseImage=1;
						}else{
							var isCourseImage=0;
						}
						
						$scope.checkNotification=[];
						angular.forEach($scope.elearningData.notification,function(list){
							if(list.checkNotification==1){
								$scope.checkNotification.push(list.notification_id);
							}
						})
						if($scope.showNotification==0){
							$scope.checkNotification=[];
						}
						
						var course={
							'credit_value':$scope.elearningData.credit_value,
							'points':$scope.elearningData.points,
							'media_library_id':$scope.mediaLibraryId,
							'status_id':1,
							'certificate_template_id':$scope.elearningData.certificate_template_id
							};
							
						var courseDetail={
							'course_name':$scope.elearningData.course_name,
							'course_title':$scope.elearningData.course_title,
							'course_description':$scope.elearningData.course_description
						};
						
						var trainingPrograms={
							'status_id':$scope.elearningData.status_id
						};
						
						var courseSetting={
							'ilt_assessment_id':$scope.elearningData.CourseSetting.itl_assessment_id,
							'required_approval':$scope.elearningData.CourseSetting.required_approval,
							'enrollment_type':$scope.elearningData.CourseSetting.enrollement_type,
							'is_unenrollment':$scope.elearningData.CourseSetting.is_unenrollment
						};
						
						$http({
							method:"POST",
							url:"/admin/editClassroomCourse",
							headers: {'Content-Type': undefined },
							transformRequest:function(){
								var formData=new FormData();
								formData.append('code',$scope.trainingCode);
								formData.append('course',angular.toJson(course));
								formData.append('courseDetail', angular.toJson(courseDetail));
								formData.append('trainingPrograms',angular.toJson(trainingPrograms));
								formData.append('courseSetting',angular.toJson(courseSetting));
								formData.append('isCourseImage',isCourseImage);
								formData.append('userAllSkills',angular.toJson($scope.userAllSkills));
								formData.append('categories',angular.toJson($scope.multiple.SelectedCategoryName));
								formData.append('checkNotification',angular.toJson($scope.checkNotification));
								formData.append('courseImage',course_pic_data);
								formData.append('targetAudienceGroups',angular.toJson($scope.elearningData.groupAudienceTag));
								formData.append('targetAudienceusers',angular.toJson($scope.elearningData.userAudienceTag));
								formData.append('permissionGroups',angular.toJson($scope.elearningData.groupEditTag));
								formData.append('permissionUsers',angular.toJson($scope.elearningData.userEditTag));
								formData.append('courseTags',angular.toJson($scope.elearningData.courseTags));
								return formData;
						}
					}).success(function(response){
						$scope.loader=false;
						if(response.status==201){
							//$scope.trainingCode=response.trainingCode;
							$scope.HandOut.onBeforeUploadItem = function(item) {
										item.formData.push({courseId:response.courseId});	
							};
							if(HandOut.queue.length>0){
								$scope.HandOut.uploadAll();
							}else{
								Alertify.alert('Course has been updated successfully');
								//$location.path('/courses/editassessment/'+response.trainingCode);
							}
							 HandOut.onCompleteAll = function(progress) {
								Alertify.alert('Course has been updated successfully');
								//$location.path('/courses/editassessment/'+$scope.trainingCode);
							};			
							//$location.path('/courses/editelearning/'+response.trainingCode);
							//$window.location.reload();
						}
					})
			 
				 }
				 
			 }
			 
			 $scope.classroomCourseData=function(){
			$http({
				method:"POST",
				url:"/admin/getClassRoomCourseData"
			}).success(function(response){
				$scope.classroomAddData=response;
				//alert(JSON.stringify(response));
			})
		}
		$scope.classroomCourseData();
});




classroomcoursemodule.controller('ManageClassesController', function($scope,$location,$localStorage,$http,Alertify,$rootScope,$stateParams){
	$scope.trainingCode=$stateParams.courseCode;
	
	$scope.getCourseClasses=function(){
		$http({
			method:"POST",
			url:"/admin/getcourseClasses",
			data:{code:$scope.trainingCode}
		}).success(function(response){
			$scope.courseClasses=response;
			//alert(response);
		})
	}
	
	$scope.getCourseClasses();
	
	$scope.deleteClass=function(classId){
		 alert(classId);
	 }
	
});







classroomcoursemodule.controller('AddClassesController', function($scope,$location,$localStorage,$http,Alertify,$rootScope,$mdpTimePicker,$stateParams){
	 $scope.session=1;
	$scope.trainingCode=$stateParams.courseCode;
	$scope.startTime=new Date();
	//console.log($scope.startTime);
	$scope.sessionData=[{'sessionId':$scope.session,'expdate':new Date(),'eexpdate':new Date(),'startTime':$scope.startTime,'endTime':new Date(),'location':'','instructor':'','timezone':''}];
    
	 $scope.multiple={
		selectedGroupName1:[] 
	 };
	$scope.addNewSession=function(){
	  $scope.session++;
	  $scope.sessionData.push({'sessionId':$scope.session,'expdate':new Date(),'eexpdate':new Date(),'startTime':$scope.startTime,'endTime':new Date(),'location':'','instructor':'','timezone':''});  
	  
     }
	$scope.deleteSession=function(index){
	  $scope.sessionData.splice(index,1);
	    
  }
   $scope.showTimePicker = function(ev) {alert('d');
    	$mdpTimePicker($scope.currentTime, {
        targetEvent: ev
      }).then(function(selectedDate) {
        $scope.currentTime = selectedDate;
      });
    } 

	$scope.showPicker=function(){
		alert('d');
	}
		 	
  $scope.tagTransformGroupName = function (newTag) {
			var item = {
			group_name: newTag, 
			};

			return item;
	};
		
		$scope.getAddclassData=function(){
			$http({
				method:"POST",
				url:"/admin/getAddClassdata"
			}).success(function(response){
				$scope.addClassData=response;
			})
		}
		
		$scope.getAddclassData();
		
		
		$scope.deliveryType='2';
		$scope.status_id='0';
	 $scope.saveClass=function(){
		
		 if($scope.seatLimit != parseInt($scope.seatLimit)){
			Alertify.alert("Maximum seats should be a number.");return false;
		 }else if($scope.sessionData.length==0){
			 Alertify.alert('Please add a session and start time to create this class.');return false;
		 }
		 
		 for(var i=0;i<$scope.sessionData.length;i++){
			 if(!$scope.sessionData[i].expdate){
				 Alertify.alert('Please select class '+$scope.sessionData[i].sessionId+' starting date.');return false;
			 }else if(!$scope.sessionData[i].eexpdate){
				  Alertify.alert('Please select class '+$scope.sessionData[i].sessionId+' ending date.');return false;
			 }else if(!$scope.sessionData[i].startTime){
				  Alertify.alert('Please select class '+$scope.sessionData[i].sessionId+' starting time.');return false;
			 }else if(!$scope.sessionData[i].endTime){
				  Alertify.alert('Please select class '+$scope.sessionData[i].sessionId+' ending date.');return false;
			 }else if(!$scope.sessionData[i].instructor){
				  Alertify.alert('Please select class '+$scope.sessionData[i].sessionId+' instructor.');return false;
			 }else if(!$scope.sessionData[i].timezone){
				  Alertify.alert('Please select class '+$scope.sessionData[i].sessionId+' time zone.');return false;
			 }
		 }
		 $scope.loader=true;
		 
		 var classData={
			 class_name:$scope.classTitle,
			 virtual_session_information:$scope.classDescription,
			 maximum_seat:$scope.seatLimit,
			 delivery_type_id:$scope.deliveryType,
			 status_id:$scope.status_id,	 
		 }
		 
		 $http({
			 method:"POST",
			 url:"/admin/addCourseClass",
			 data:{code:$scope.trainingCode,classData:angular.toJson(classData),sessionData:$scope.sessionData}
		 }).success(function(response){
			 Alertify.success('Class added successfully.');
			 $scope.loader=false;
			 $location.path('courses/manageclasses/'+response.code);
		 }) 
	 }
	 
	 
	 
	 
});


classroomcoursemodule.filter('noOfSession',function(){
	return function(x) {
		var data=JSON.parse(x);
		return data.length;
	}	
})



classroomcoursemodule.controller('EditClassController', function($scope,$location,$localStorage,$http,Alertify,$rootScope,$stateParams){
	$scope.classId=$stateParams.courseCode;
	$scope.editClassData=function(){
		$http({
			method:"POST",
			url:"/admin/getClassData",
			data:{classId:$scope.classId}
		}).success(function(response){
			$scope.editClassData=response;
			$scope.sessionLength=$scope.editClassData.sessionData.length;
			for(var i=0;i<$scope.editClassData.sessionData.length;i++){
				$scope.editClassData.sessionData[i].expdate=new Date($scope.editClassData.sessionData[i].expdate);
				$scope.editClassData.sessionData[i].eexpdate=new Date($scope.editClassData.sessionData[i].eexpdate);
				$scope.editClassData.sessionData[i].startTime=new Date($scope.editClassData.sessionData[i].startTime);
				$scope.editClassData.sessionData[i].endTime=new Date($scope.editClassData.sessionData[i].endTime);
			}
				
		})
	}
	
	$scope.editClassData();
	
	
	$scope.getAddclassData=function(){
			$http({
				method:"POST",
				url:"/admin/getAddClassdata"
			}).success(function(response){
				$scope.addClassData=response;
			})
		}
		
		$scope.getAddclassData();
	
	
	$scope.deleteSession=function(index,scheduleId){
		 Alertify.confirm('Are you sure?').then(
			function onOk() {
				$http({
					 method:"POST",
					 url:"/admin/deleteSchedule",
					 data:{scheduleId:scheduleId}
				 }).success(function(response){
					 Alertify.success('Session deleted successfully.');
					 $scope.editClassData.sessionData.splice(index,1);
					 $scope.sessionLength--;
				 })	
		  });
	}
	
	
	
	$scope.updateClass=function(){
		 if($scope.editClassData.classData.maximum_seat != parseInt($scope.editClassData.classData.maximum_seat)){
			Alertify.alert("Maximum seats should be a number.");return false;
		 }else if($scope.editClassData.sessionData.length==0){
			 Alertify.alert('Please add a session and start time to create this class.');return false;
		 }
		 
		 for(var i=0;i<$scope.editClassData.sessionData.length;i++){
			 if(!$scope.editClassData.sessionData[i].expdate){
				 Alertify.alert('Please select class '+$scope.editClassData.sessionData[i].sessionId+' starting date.');return false;
			 }else if(!$scope.editClassData.sessionData[i].eexpdate){
				  Alertify.alert('Please select class '+$scope.editClassData.sessionData[i].sessionId+' ending date.');return false;
			 }else if(!$scope.editClassData.sessionData[i].startTime){
				  Alertify.alert('Please select class '+$scope.editClassData.sessionData[i].sessionId+' starting time.');return false;
			 }else if(!$scope.editClassData.sessionData[i].endTime){
				  Alertify.alert('Please select class '+$scope.editClassData.sessionData[i].sessionId+' ending date.');return false;
			 }else if(!$scope.editClassData.sessionData[i].instructor_id){
				  Alertify.alert('Please select class '+$scope.editClassData.sessionData[i].sessionId+' instructor.');return false;
			 }else if(!$scope.editClassData.sessionData[i].timezone){
				  Alertify.alert('Please select class '+$scope.editClassData.sessionData[i].sessionId+' time zone.');return false;
			 }
		 }
		 $scope.loader=true;
		 
		 var classData={
			 class_name:$scope.editClassData.classData.class_name,
			 virtual_session_information:$scope.editClassData.classData.virtual_session_information,
			 maximum_seat:$scope.editClassData.classData.maximum_seat,
			 delivery_type_id:$scope.editClassData.classData.delivery_type_id,
			 status_id:$scope.editClassData.classData.status_id,	 
		 }
		 
		 $http({
			 method:"POST",
			 url:"/admin/editCourseClass",
			 data:{classId:$scope.classId,classData:angular.toJson(classData),sessionData:$scope.editClassData.sessionData}
		 }).success(function(response){
			 Alertify.success('Class updated successfully.');
			 $scope.loader=false; 
		 })
	}
	
	
	$scope.addNewSession=function(){
	  $scope.sessionLength++;
	  $scope.editClassData.sessionData.push({'sessionId':$scope.sessionLength,'expdate':new Date(),'eexpdate':new Date(),'startTime':new Date(),'endTime':new Date(),'location':'','instructor':'','timezone':'','scheduleId':''});  
     }
	
	
});