
var traininglibrarymodule = angular.module("traininglibrarymodule",[]);
/* traininglibrarymodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("traininglibraryindex", {
		url:"/traininglibrary/index",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/SuperAdmin/traininglibrary.html',
				controller: 'TraininglibraryController'
			}
		},
	});	
	
}]); */
	
	
traininglibrarymodule.controller('TraininglibraryController', function($scope,$location,$localStorage,$http){
	
	 $scope.getTraininglibraryData=function(){
		 $http({
			 method:"POST",
			 url:"/admin/traininglibrary/view-library-name"
		 }).success(function(response){
			 $scope.trainingLibraryData=response.gridData;
		 })
	 }
	 
	   $scope.getTraininglibraryData();
	   
	 $scope.gridOptions = { 
				  data :'trainingLibraryData',
				 cellEditableCondition: true,
				enableFiltering: true,

				 paginationPageSize:10,
				 paginationPageSizes: [10, 20, 30],
				paginationPageSize: 10,
						rowHeight: 40,
				onRegisterApi : function(gridApi){
					
				  $scope.gridApis= gridApi;
				 // $scope.gridOptions.gridApis.refresh()
				},

				 
				 columnDefs: [
							
				  { name: 'category_name',displayName: 'Category Name' ,width: "25%"},
				  { name: 'trining_library',displayName: 'Training Library'},
				  { name: 'course_count',displayName: 'Course Count'},
				 //  {name:'view', displayName:'',cellTemplate:"view.html" ,enableFiltering: false},		
				 			  
				]
		 
		 };
		 
		 
	 
	

	    
});	
