//created by-Siddharth Kushwah
'use strict';
var adminmodule = angular.module("adminmodule",['datatables', 'ngResource']);

	
adminmodule.controller('UserListController', function($scope,$location,$localStorage,$http,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$compile,$rootScope,$window){
	
	$scope.selectedview=false;
	
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.userlist={};
	$scope.userlist.selected = {};
  
	$scope.name='';
	var titleHtml = '<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll, showCase.selected)">';
	$scope.userlist.dtOptions = DTOptionsBuilder.newOptions()
	.withOption('ajax', {
	data:{_token:$scope.tokenuse,page:1},
	url: '/admin/manageuserlist',
	type: 'POST'
	}).withDataProp(function(json) {
	json.recordsTotal = json.count;
	json.recordsFiltered = json.count;
	json.draw = json.draw;
	return json.data;
	}).withDOM('C<"clear">lfrtip')
	.withOption('processing', true)
	.withOption('serverSide', true)
	.withOption('createdRow', function(row, data, dataIndex) {
	$compile(angular.element(row).contents())($scope);
	})
	.withOption('headerCallback', function(header) {
	if (!$scope.userlist.headerCompiled) {
		$scope.userlist.headerCompiled = true;
		$compile(angular.element(header).contents())($scope);
	}
	})
	.withPaginationType('full_numbers');

		$scope.userlist.dtColumns = [
			DTColumnBuilder.newColumn('id').withTitle('ID').notVisible() ,
			DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
			.renderWith(function(data, type, full, meta) {
			$scope.userlist.selected[full.id] = false;
			return '<input type="checkbox" ng-model="showCase.selected[' + data.id + ']" ng-click="showCase.toggleOne(showCase.selected)">';
			}),
			DTColumnBuilder.newColumn('name').withTitle('Name'),
			DTColumnBuilder.newColumn('user_name').withTitle($rootScope.get_field_name.login_id),
			DTColumnBuilder.newColumn('division_name').withTitle($rootScope.get_field_name.division_name),
			DTColumnBuilder.newColumn('area').withTitle($rootScope.get_field_name.area),
			DTColumnBuilder.newColumn('location').withTitle($rootScope.get_field_name.location),
			DTColumnBuilder.newColumn('subOrgName').withTitle($rootScope.get_field_name.company),
			DTColumnBuilder.newColumn('button').withTitle('Details').notSortable()
		];

      
	$scope.viewuser=function(token)
	{
		$http({
		url:'/admin/manageuserlist',
		method:'post',
		data:{mode:'selectedviewuser','usertoken':token}
		}).success(function(response){
			 $scope.selectedview=true;
			 $scope.user_detail=response.data;
			 $scope.selecteduser=response.selecteduserid;
		});
	}
	
	$scope.moveprofile=function(tokenselected)
	{
		$window.location="#/admin/manageusersedit/"+tokenselected;
		return false;
	}
	    
});	

adminmodule.run(["$rootScope","$http","routes", function($rootScope,$http,routes)
{

  $rootScope.callfunction=function(){
		$http({
		url:'/admin/getuserfieldname',
		method:'get',
		}).success(function(response){
		    $rootScope.get_field_name=response;
		});
	}
    $rootScope.callfunction();

}]);



adminmodule.controller('AddUserController', function($scope,$location,$localStorage,$http,$timeout,$interval){
	
   $scope.tokenuse=$("input[name=_token]").val();
   $scope.disabled = undefined;
   $scope.multiple = {};
   
	$http({
	url:'/admin/getAddUserViewData',
	method:'post',
	}).success(function(response){
	  $scope.lockedStatus  =response.lockedStatus;
      $scope.jobtitle =response.jobtitle;
      $scope.Country  =response.Country;
      $scope.allcompany  =response.allcompany;
      $scope.alldivision  =response.alldivision;
      $scope.allarea  =response.allarea;
      $scope.alllocation  =response.alllocation;
      $scope.supervisior  =response.supervisior;
      $scope.allgroup  =response.allgroup;
      $scope.checkFieldArray  =response.checkFieldArray;
      $scope.allband  =response.allband;
      $scope.customfieldArr  =response.customfieldArr;
      $scope.customfieldvalueArr  =response.customfieldvalueArr;
      $scope.customfieldnameArr  =response.customfieldnameArr;
      $scope.strhml  =response.strhml;
      $scope.PasswordValidateSetting  =response.PasswordValidateSetting;
      $scope.tag_str  =response.tag_str;
	  $scope.timeZones=response.timeZones;
	});

   $scope.multiple.selectedJobTittle = [];
   $scope.multiple.selectedCompany = [];
   $scope.multiple.selectedDivision = [];
   $scope.multiple.selectedArea = [];
   $scope.multiple.selectedLocation = [];
   $scope.multiple.selectedGroup = [];
   $scope.multiple.selectedSupervisor = [];
   $scope.multiple.selectedBand = [];
    $scope.expdate=new Date();
 $scope.tagTransformTitle = function (newTag) {
    var item = {
        jobtitle_group: newTag, 
        job_id: newTag, 
    };

    return item;
  };
  
  $scope.tagTransformCompany = function (newTag) {
    var item = {
        sub_organization_name: newTag, 
        sub_organization_id: newTag, 
    };
    return item;
  };
  
   $scope.tagTransformDivision = function (newTag) {
    var item = {
        division_name: newTag, 
        division_id: newTag, 
    };

    return item;
  };
  
  $scope.tagTransformArea = function (newTag) {
	 
		 var item = {
		area_name: newTag, 
		area_id: newTag, 
		};
		return item;
	
    
  };
  
  $scope.tagTransformLocation = function (newTag) {
    var item = {
        location_name: newTag, 
        location_id: newTag, 
    };

    return item;
  };
  $scope.tagTransformGroup = function (newTag) {
    var item = {
        group_name: newTag, 
        group_id: newTag, 
    };

    return item;
  };
  
    $scope.insertUser = {};
    $scope.insertUser.status_id="1";
	
	$scope.validate_form=function()
	{
	
		if($scope.PasswordValidateSetting.length > 0)
		{
			var passLengthValid = $scope.PasswordValidateSetting[0].password_length ;
			var passcomplexityValid = $scope.PasswordValidateSetting[0].password_complexity ;
			var customPassSetting = $scope.PasswordValidateSetting[0].custom_pass_setting ;
		}
		else
		{
			var  passLengthValid = 4 ;
			var  passcomplexityValid = 1 ;
			var  customPassSetting = 0;
		}
		
		var iChars = "!@#$%^()+=-[]\\\';,/{}|\":<>?";
		var iChars1 = "!#$%^()+=[]\\\';,/{}|\":<>?";
		var reWhiteSpace = new RegExp(/^\s+$/);
		var fname= $scope.insertUser.first_name;
		var lname= $scope.insertUser.last_name;
		var loginname= $scope.insertUser.loginname;
		var password= $scope.insertUser.password;
		var status_id= $scope.insertUser.status_id;
		var emp_id= $scope.insertUser.emp_id;
		var rolename= $scope.insertUser.rolename;
		var jobtitle= $scope.multiple.selectedJobTittle;
		var country= $scope.country;
		var compname= $scope.multiple.selectedCompany;
		var group_name=$scope.multiple.selectedGroup;
		var supervisior=$scope.multiple.selectedSupervisor;
		var status =$scope.insertUser.status_id;
		var phone_no =$scope.insertUser.phone_no;
		var phone_code =$scope.insertUser.phone_code;
		var emailid =$scope.email_id;
		var timezonesSelect =$scope.timezonesSelect;
	
		if(!loginname)
		{
			alertify.alert("User Name should not be blank");
			return false;
		}
		
        	var where_is_space=loginname.indexOf(" ");
			
		 if(where_is_space!=-1)
		{
			alertify.alert("Please type username without giving any space.");
			return false;
		}
		
		if(!password)
		{ 
			alertify.alert("Password should not be blank.");
			return false;
		} 
		if(password.length < passLengthValid)
		{
			alertify.alert("Password must be contain more than  " +passLengthValid + " characters");
			return false;
		}
		var hasCaps = /[A-Z]/.test(password);
		var hasSmall = /[a-z]/.test(password);
		var hasNums = /\d/.test(password);
		var hasSpecials = /[~!,@#%&_\$\^\*\?\-]/.test(password);
		if(passcomplexityValid == 2)
		{
			if(!((hasSmall || hasCaps) && hasNums || hasSpecials))
			{
				alertify.alert("Password must contain Alpha-Numeric character");
				return false ;
			}
		}
		if(passcomplexityValid == 3)
		{
			if(!((hasSmall || hasCaps) && hasNums && hasSpecials))
			{
				alertify.alert("Password must contain Alpha-Numeric Character and Special character");
				
				return false ;
			}
		}
		if(passcomplexityValid == 4)
		{
			if(!(hasSmall && hasCaps && hasNums || hasSpecials))
			{
				alertify.alert("Password must contain at least one upper-case,lower-case Character and Numeric Value");
				return false ;
			}
		}
		if(passcomplexityValid == 5)
		{
			if(!(hasSmall && hasCaps && hasNums && hasSpecials))
			{
				alertify.alert("Password must contain at least one upper-case, lower-case number and special character");
				return false ;
			}
		}

		if(!rolename)
		{
			alertify.alert( "Please select Role." );
			return false;
		}

		if(!fname)
		{
			alertify.alert("First Name should not be blank");
			return false;
		}

		if(!lname)
		{
			alertify.alert("Last Name should not be blank");
			return false;
		}
        
		if(reWhiteSpace.test(jobtitle)|| jobtitle.length==0 || jobtitle=='')
		{
			alertify.alert("Job Title should not be blank");
			return false;
		}
       
		if(!country)
		{
			alertify.alert ("Please select country");
			return false;
		}
		if(!timezonesSelect)
		{
			alertify.alert ("Please select timezone");
			return false;
		}

		if(reWhiteSpace.test(compname)|| compname.length==0 || compname=='')
		{
			alertify.alert ("Please enter company name");
			return false;
		}
       
     
		 var countcustom = $('#customfieldcount').val() ;
		
		if(countcustom > 0)
		{
			for(var n = 0; n < countcustom; n++)
			{
				if($('#customfieldvalue_'+n).data('valid_req') == 1)
				{
					if($('#customfieldvalue_'+n).val() == 'radiobutton')
					{ 
						if(!((($('#radiobuttonvalue_'+n+'_1')).is(":checked")) || (($('#radiobuttonvalue_'+n+'_2')).is(":checked"))))
						{
							var fieldname = $('#customfieldvalue_'+n).data('name');
							alertify.alert(fieldname+' field mandatory');
							return false;
						}
					}
					else if($('#customfieldvalue_'+n).val() == 'checkbox')
					{
						var totalcheckbox = $('#totalcheckbox_'+n).val();
						
						var flag = 0;
						for(var v = 0; v < totalcheckbox; v++)
						{
							if($('#checkboxvalue_'+n+'_'+v).is(":checked"))
							{
								flag = 1;
							}
						}
						
						if(flag == 0)
						{
							var fieldname = $('#customfieldvalue_'+n).data('name');
							alertify.alert(fieldname+' field mandatory');
							return false;
						}
					}
					else if($('#customfieldvalue_'+n).val() == '')
					{
						
						var fieldtype = $('#customfieldvalue_'+n).data('type');
						if(fieldtype=='Date'){
							    var iddate	= $('#customfieldId_'+n).val();
								try {
									if(!$scope.myDate[iddate]){
										var fieldname = $('#customfieldvalue_'+n).data('name');
										alertify.alert(fieldname+' field mandatory');
										return false;
									}
							    }
								catch(err) {
									var fieldname = $('#customfieldvalue_'+n).data('name');
									alertify.alert(fieldname+' field mandatory');
									return false;
								}
						}else{
											var fieldname = $('#customfieldvalue_'+n).data('name');
											alertify.alert(fieldname+' field mandatory');
											return false;
									
						}
						
					}
				}
			}
		}
        
		if(!emailid)
		{
			alertify.alert("enter e-mail address");
			return false;
		}
		var atpos=emailid.indexOf("@");
		var dotpos=emailid.lastIndexOf(".");
		if(atpos<1 || dotpos<atpos+2 || dotpos+2>=emailid.length)
		{
			alertify.alert("Not a valid e-mail address");
			return false;
		}
       if(phone_no){
		var regexNum = /\d/;
		var phonechars = "!@#$%^+=[]\\\';,/{}|\":<>?";
		for(var i = 0; i < phone_no.length; i++)
		{
			if(phonechars.indexOf(phone_no.charAt(i)) != -1)
			{
				alertify.alert("Phone No. has special characters.");
				return false;
			}
		}
	   }

		if(reWhiteSpace.test(group_name)|| group_name.length==0 || group_name=='')
		{
			alertify.alert("Group should not be blank");
			return false;
		}
   
		if(!supervisior)
		{
			alertify.alert("Please select Supervisor");
			return false;
		}
		var arrayText=[];
       if(countcustom > 0)
		{
			
			for(var n = 0; n < countcustom; n++)
			{
				var newarr={};
				if($('#customfieldvalue_'+n).data('valid_req') == 1)
				{
					 var id=$('#customfieldId_'+n).val();
					 
					if($('#customfieldvalue_'+n).val() == 'radiobutton')
					{ 
						if(((($('#radiobuttonvalue_'+n+'_1')).is(":checked")) || (($('#radiobuttonvalue_'+n+'_2')).is(":checked"))))
						{
							
							
							 var val1=$('input[name=radiobuttonvalue_'+n+']:checked').val();
						     newarr={'id':id,'val':val1,'is_date':'no'};
						    
						}
					}
					else if($('#customfieldvalue_'+n).val() == 'checkbox')
					{
						var totalcheckbox = $('#totalcheckbox_'+n).val();
						
						var flag = 0;
						var strchekval=[];
						for(var v = 0; v < totalcheckbox; v++)
						{
							if($('#checkboxvalue_'+n+'_'+v).is(":checked"))
							{
								var strval=$('input[name=checkboxvalue_'+n+'_'+v+']:checked').val();
							    strchekval.push(strval);
								flag = 1;
							}
						}
						
						if(flag == 1)
						{

								newarr={'id':id,'val':strchekval,'is_date':'no'};
						} 
					}
					 else if((($('#customfieldvalue_'+n).val()!='checkbox') || ($('#customfieldvalue_'+n).val()!='radiobutton') || ($('#customfieldvalue_'+n).data('type')!='Date')) && $('#customfieldvalue_'+n).val()!='')
					{
						var val2 =$('#customfieldvalue_'+n).val();
						/* alert(val2+'=='+n); */
						newarr={'id':id,'val':val2,'is_date':'no'};
						
						
						
					}else if($('#customfieldvalue_'+n).data('type')=="Date"){
                          var fieldtype = $('#customfieldvalue_'+n).data('type');
						if(fieldtype=='Date'){
							    var iddate	= $('#customfieldId_'+n).val();
								try {
								
									if($scope.myDate[iddate]){
										newarr={'id':id,'val':$scope.myDate[iddate],'is_date':'yes'};
									}
							    }
								catch(err) {
									  newarr={'id':id,'val':$scope.myDate[iddate],'is_date':'yes'};
								}
						}

					}					
					 arrayText.push(newarr);
				}
			}
		}

		$http({
		url:'/admin/adduser',
		method:'post',
		data:{'_token':$scope.tokenuse,first_name:fname,last_name:lname,loginname:loginname,password:password,
		status_id:status_id,emp_id:emp_id,rolename:rolename,jobtitle:jobtitle,country:country,compname:compname,group_name:group_name,supervisior:supervisior,status:status,phone_no:phone_no,phone_code:phone_code,emailid:emailid,customfield:JSON.stringify(arrayText),location:$scope.multiple.selectedLocation,divison_name:$scope.multiple.selectedDivision,area:$scope.multiple.selectedArea,band_name:$scope.multiple.selectedBand,timezonesSelect:timezonesSelect}
		}).success(function(response){
			alertify.alert(response.message);
		});
		return false;
	
	}


$scope.checkpassword=function(){
	
		if($scope.PasswordValidateSetting.length > 0)
		{
			var passLengthValid = $scope.PasswordValidateSetting[0].password_length ;
			var passcomplexityValid = $scope.PasswordValidateSetting[0].password_complexity ;
			var customPassSetting = $scope.PasswordValidateSetting[0].custom_pass_setting ;
		}
		else
		{
			var  passLengthValid = 4 ;
			var  passcomplexityValid = 1 ;
			var  customPassSetting = 0;
		}
		
		if(customPassSetting == 1)
		  {
			  $('#pswd_info').show();
			var pswd = $('#password').val();
			var hasCaps = /[A-Z]/.test(pswd);
			var hasSmall = /[a-z]/.test(pswd);
			if(((pswd.length) >= passLengthValid)) {
				$('#length').removeClass('invalid').addClass('valid');
			} else {
				$('#length').removeClass('valid').addClass('invalid');
			}
			if(!((hasSmall || hasCaps)))
			{
				$('#Alpha').removeClass('valid').addClass('invalid');
			} else {
				$('#Alpha').removeClass('invalid').addClass('valid');
			}	
			var lowerCase = new RegExp('[a-z]');
			if (pswd.match(lowerCase)) {
				$('#letter').removeClass('invalid').addClass('valid');
			} else {
				$('#letter').removeClass('valid').addClass('invalid');
			}
			if (pswd.match(/[A-Z]/)) {
				$('#capital').removeClass('invalid').addClass('valid');
			} else {
				$('#capital').removeClass('valid').addClass('invalid');
			}
			if (pswd.match(/\d/)) {
				$('#number').removeClass('invalid').addClass('valid');
			} else {
				$('#number').removeClass('valid').addClass('invalid');
			}
			if (pswd.match(/[^0-9a-zA-Z]/g)) {
				$('#special').removeClass('invalid').addClass('valid');
			} else {
				$('#special').removeClass('valid').addClass('invalid');
			}

     
		 $('input[id=password]').focus(function(){ 
			$('#pswd_info').show();
		});
        
		$('input[id=password]').blur(function() {
			$('#pswd_info').hide();
		}); 
		
		}

  }

});

adminmodule.controller('EditUserController', function($scope,$location,$localStorage,$http,$timeout,$interval,$stateParams){
	
	$scope.tokenuse=$("input[name=_token]").val();
	$scope.disabled = undefined;
	$scope.multiple = {};
	$scope.insertUser = {};
   
	
	$scope.multiple.selectedJobTittle = [];
	$scope.multiple.selectedCompany = [];
	$scope.multiple.selectedDivision = [];
	$scope.multiple.selectedArea = [];
	$scope.multiple.selectedLocation = [];
	//$scope.multiple.selectedSupervisor = [];
	$scope.multiple.selectedBand = [];
	$http({
	url:'/admin/getedituserdetail',
	method:'post',
	data:{'selectedusertoken':$stateParams.selectedusertoken}
	}).success(function(response){
			$scope.lockedStatus  =response.lockedStatus;
			$scope.jobtitle =response.jobtitle;
			$scope.Country  =response.Country;
			$scope.allcompany  =response.allcompany;
			$scope.alldivision  =response.alldivision;
			$scope.allarea  =response.allarea;
			$scope.alllocation  =response.alllocation;
			$scope.supervisior  =response.supervisior;
			$scope.allgroup  =response.allgroup;
			$scope.checkFieldArray  =response.checkFieldArray;
			$scope.allband  =response.allband;
			$scope.customfieldArr  =response.customfieldArr;
			$scope.customfieldvalueArr  =response.customfieldvalueArr;
			$scope.customfieldnameArr  =response.customfieldnameArr;
			$scope.strhml  =response.strhml;
			$scope.PasswordValidateSetting  =response.PasswordValidateSetting;
			$scope.tag_str  =response.tag_str;
			$scope.multiple.selectedGroup  =response.usergroups;
			$scope.multiple.selectedJobTittle  =response.userjobs;
			$scope.multiple.selectedDivision  =response.userdivision;
			$scope.multiple.selectedCompany  =response.usercompany;
			$scope.multiple.selectedArea  =response.user_area;
			$scope.multiple.selectedLocation  =response.user_location;
			$scope.multiple.selectedSupervisor  =response.usersupervisor;
			$scope.insertUser  =response.getuserbasicdetail;
			$scope.customfieldDateArr  =response.customfieldDateArr;
			$scope.insertUser.password  ='********';
			$scope.timeZones=response.timeZones;
		
			/* $scope.insertUser.status_id=1;
            $scope.insertUser.rolename=1; */
			//alert(JSON.stringify($scope.multiple.selectedSupervisor));
			
	});
	
	$scope.myDate={};

	$scope.calldatefunction=function(){
		$timeout( function(){

				angular.forEach($scope.customfieldDateArr,function(value,index){
					var mydate = new Date(value);
				    $scope.myDate[index]=mydate;
				});
			}, 1000 );		   
			
	 }

   
   $scope.expdate=new Date();
	
  
 $scope.tagTransformTitle = function (newTag) {
    var item = {
        jobtitle_group: newTag, 
        job_id: newTag, 
    };

    return item;
  };
  
  $scope.tagTransformCompany = function (newTag) {
    var item = {
        sub_organization_name: newTag, 
        sub_organization_id: newTag, 
    };

    return item;
  };
  
   $scope.tagTransformDivision = function (newTag) {
    var item = {
        division_name: newTag, 
        division_id: newTag, 
    };

    return item;
  };
  
  $scope.tagTransformArea = function (newTag) {
	 
		 var item = {
		area_name: newTag, 
		area_id: newTag, 
		};
		return item;
	
    
  };
  
  $scope.tagTransformLocation = function (newTag) {
    var item = {
        location_name: newTag, 
        location_id: newTag, 
    };

    return item;
  };
  $scope.tagTransformGroup = function (newTag) {
    var item = {
        group_name: newTag, 
        group_id: newTag, 
    };

    return item;
  };
  
 $scope.update_user=function()
 {
	
		if($scope.PasswordValidateSetting.length > 0)
		{
			var passLengthValid = $scope.PasswordValidateSetting[0].password_length ;
			var passcomplexityValid = $scope.PasswordValidateSetting[0].password_complexity ;
			var customPassSetting = $scope.PasswordValidateSetting[0].custom_pass_setting ;
		}
		else
		{
			var  passLengthValid = 4 ;
			var  passcomplexityValid = 1 ;
			var  customPassSetting = 0;
		}
		
		var iChars = "!@#$%^()+=-[]\\\';,/{}|\":<>?";
		var iChars1 = "!#$%^()+=[]\\\';,/{}|\":<>?";
		var reWhiteSpace = new RegExp(/^\s+$/);
		var fname= $scope.insertUser.first_name;
		var lname= $scope.insertUser.last_name;
		var loginname= $scope.insertUser.loginname;
		var password= $scope.insertUser.password;
		var status_id= $scope.insertUser.status_id;
		var emp_id= $scope.insertUser.emp_id;
		var rolename= $scope.insertUser.rolename;
		var jobtitle= $scope.multiple.selectedJobTittle;
		var country= $scope.insertUser.country;
		var compname= $scope.multiple.selectedCompany;
		var group_name=$scope.multiple.selectedGroup;
		var supervisior=$scope.multiple.selectedSupervisor;
		var status =$scope.insertUser.status_id;
		var phone_no =$scope.insertUser.phone_no;
		var phone_code =$scope.insertUser.phone_code;
		var emailid =$scope.insertUser.email_id;
		var expdate =$scope.insertUser.expdate;
		var timeZone =$scope.insertUser.timezone_id;
	   
		if(!loginname)
		{
			alertify.alert("User Name should not be blank");
			return false;
		}
		
        	var where_is_space=loginname.indexOf(" ");
			
		 if(where_is_space!=-1)
		{
			alertify.alert("Please type username without giving any space.");
			return false;
		}
		
		if(!password)
		{ 
			alertify.alert("Password should not be blank.");
			return false;
		} 
		if(password.length < passLengthValid)
		{
			alertify.alert("Password must be contain more than  " +passLengthValid + " characters");
			return false;
		}
		var hasCaps = /[A-Z]/.test(password);
		var hasSmall = /[a-z]/.test(password);
		var hasNums = /\d/.test(password);
		var hasSpecials = /[~!,@#%&_\$\^\*\?\-]/.test(password);
		if(passcomplexityValid == 2)
		{
			if(!((hasSmall || hasCaps) && hasNums || hasSpecials))
			{
				alertify.alert("Password must contain Alpha-Numeric character");
				return false ;
			}
		}
		if(passcomplexityValid == 3)
		{
			if(!((hasSmall || hasCaps) && hasNums && hasSpecials))
			{
				alertify.alert("Password must contain Alpha-Numeric Character and Special character");
				
				return false ;
			}
		}
		if(passcomplexityValid == 4)
		{
			if(!(hasSmall && hasCaps && hasNums || hasSpecials))
			{
				alertify.alert("Password must contain at least one upper-case,lower-case Character and Numeric Value");
				return false ;
			}
		}
		if(passcomplexityValid == 5)
		{
			if(!(hasSmall && hasCaps && hasNums && hasSpecials))
			{
				alertify.alert("Password must contain at least one upper-case, lower-case number and special character");
				return false ;
			}
		}

		if(!rolename)
		{
			alertify.alert( "Please select Role." );
			return false;
		}

		if(!fname)
		{
			alertify.alert("First Name should not be blank");
			return false;
		}

		if(!lname)
		{
			alertify.alert("Last Name should not be blank");
			return false;
		}
        
		if(reWhiteSpace.test(jobtitle)|| jobtitle.length==0 || jobtitle=='')
		{
			alertify.alert("Job Title should not be blank");
			return false;
		}
       
		if(!country)
		{
			alertify.alert ("Please select country");
			return false;
		}
        if(!timeZone)
		{
			alertify.alert ("Please select timezone");
			return false;
		}

		if(reWhiteSpace.test(compname)|| compname.length==0 || compname=='')
		{
			alertify.alert ("Please enter company name");
			return false;
		}
     
		 var countcustom = $('#customfieldcount').val() ;
		
		if(countcustom > 0)
		{
			for(var n = 0; n < countcustom; n++)
			{
				if($('#customfieldvalue_'+n).data('valid_req') == 1)
				{
					if($('#customfieldvalue_'+n).val() == 'radiobutton')
					{ 
						if(!((($('#radiobuttonvalue_'+n+'_1')).is(":checked")) || (($('#radiobuttonvalue_'+n+'_2')).is(":checked"))))
						{
							var fieldname = $('#customfieldvalue_'+n).data('name');
							alertify.alert(fieldname+' field mandatory');
							return false;
						}
					}
					else if($('#customfieldvalue_'+n).val() == 'checkbox')
					{
						var totalcheckbox = $('#totalcheckbox_'+n).val();
						
						var flag = 0;
						for(var v = 0; v < totalcheckbox; v++)
						{
							if($('#checkboxvalue_'+n+'_'+v).is(":checked"))
							{
								flag = 1;
							}
						}
						
						if(flag == 0)
						{
							var fieldname = $('#customfieldvalue_'+n).data('name');
							alertify.alert(fieldname+' field mandatory');
							return false;
						}
					}
					else if($('#customfieldvalue_'+n).val() == '')
					{
						
						var fieldtype = $('#customfieldvalue_'+n).data('type');
						if(fieldtype=='Date'){
							    var iddate	= $('#customfieldId_'+n).val();
								try {
									if(!$scope.myDate[iddate]){
										var fieldname = $('#customfieldvalue_'+n).data('name');
										alertify.alert(fieldname+' field mandatory');
										return false;
									}
							    }
								catch(err) {
									var fieldname = $('#customfieldvalue_'+n).data('name');
									alertify.alert(fieldname+' field mandatory');
									return false;
								}
						}else{
											var fieldname = $('#customfieldvalue_'+n).data('name');
											alertify.alert(fieldname+' field mandatory');
											return false;
									
						}
						
					}
				}
			}
		}
        
		if(!emailid)
		{
			alertify.alert("enter e-mail address");
			return false;
		}
		var atpos=emailid.indexOf("@");
		var dotpos=emailid.lastIndexOf(".");
		if(atpos<1 || dotpos<atpos+2 || dotpos+2>=emailid.length)
		{
			alertify.alert("Not a valid e-mail address");
			return false;
		}
       if(phone_no){
		var regexNum = /\d/;
		var phonechars = "!@#$%^+=[]\\\';,/{}|\":<>?";
		for(var i = 0; i < phone_no.length; i++)
		{
			if(phonechars.indexOf(phone_no.charAt(i)) != -1)
			{
				alertify.alert("Phone No. has special characters.");
				return false;
			}
		}
	   }

		if(reWhiteSpace.test(group_name)|| group_name.length==0 || group_name=='')
		{
			alertify.alert("Group should not be blank");
			return false;
		}
   
		if(!supervisior)
		{
			alertify.alert("Please select Supervisor");
			return false;
		}
		var arrayText=[];
       if(countcustom > 0)
		{
			
			for(var n = 0; n < countcustom; n++)
			{
				var newarr={};
				if($('#customfieldvalue_'+n).data('valid_req') == 1)
				{
					var id=$('#customfieldId_'+n).val();
					if($('#customfieldvalue_'+n).val() == 'radiobutton')
					{ 
						if(((($('#radiobuttonvalue_'+n+'_1')).is(":checked")) || (($('#radiobuttonvalue_'+n+'_2')).is(":checked"))))
						{
							 var val1=$('input[name=radiobuttonvalue_'+n+']:checked').val();
						     newarr={'id':id,'val':val1,'is_date':'no'};   
						}
					}
					else if($('#customfieldvalue_'+n).val() == 'checkbox')
					{
						var totalcheckbox = $('#totalcheckbox_'+n).val();
						
						var flag = 0;
						var strchekval=[];
						for(var v = 0; v < totalcheckbox; v++)
						{
							if($('#checkboxvalue_'+n+'_'+v).is(":checked"))
							{
								var strval=$('input[name=checkboxvalue_'+n+'_'+v+']:checked').val();
							    strchekval.push(strval);
								flag = 1;
							}
						}
						
						if(flag == 1)
						{

								newarr={'id':id,'val':strchekval,'is_date':'no'};
						} 
					}
					 else if((($('#customfieldvalue_'+n).val()!='checkbox') || ($('#customfieldvalue_'+n).val()!='radiobutton') || ($('#customfieldvalue_'+n).data('type')!='Date')) && $('#customfieldvalue_'+n).val()!='')
					{
						var val2 =$('#customfieldvalue_'+n).val();
						/* alert(val2+'=='+n); */
						newarr={'id':id,'val':val2,'is_date':'no'};
						
						
						
					}else if($('#customfieldvalue_'+n).data('type')=="Date"){
                          var fieldtype = $('#customfieldvalue_'+n).data('type');
						if(fieldtype=='Date'){
							    var iddate	= $('#customfieldId_'+n).val();
								try {
								
									if($scope.myDate[iddate]){
										newarr={'id':id,'val':$scope.myDate[iddate],'is_date':'yes'};
									}
							    }
								catch(err) {
									  newarr={'id':id,'val':$scope.myDate[iddate],'is_date':'yes'};
								}
						}

					}					
					 arrayText.push(newarr);
				}
			}
		}

		$http({
		url:'/admin/updateuserdetail',
		method:'post',
		data:{first_name:fname,last_name:lname,loginname:loginname,password:password,
		status_id:status_id,emp_id:emp_id,rolename:rolename,jobtitle:jobtitle,country:country,compname:compname,group_name:group_name,supervisior:supervisior,status:status,phone_no:phone_no,phone_code:phone_code,emailid:emailid,customfield:JSON.stringify(arrayText),location:$scope.multiple.selectedLocation,divison_name:$scope.multiple.selectedDivision,area:$scope.multiple.selectedArea,band_name:$scope.multiple.selectedBand,expdate:expdate,usertoken:$stateParams.selectedusertoken,timeZone:timeZone}
		}).success(function(response){
			alertify.alert(response.message);
		});
		return false;
	
	}
	
});
