
var systemmanagemodule = angular.module("systemmanagemodule",[]);
/* systemmanagemodule.config(["$stateProvider",function($stateProvider){

	$stateProvider.state("systemmanagementsystemmgt", {
		url:"/systemmanagement/systemmgt",
		views: {
			
			"mainView":{
				templateUrl: '/administrator/templates/systemmanagement/systemmgt.html',
				controller: 'SystemmgtController'
			}
		},
	});	
	
	
}]); */
	
	
systemmanagemodule.controller('SystemmgtController', function($scope,$location,$localStorage,$http,Alertify,$window){
	        $scope.preloader=false;
	        $scope.global_system_set='active';
			$scope.global_system_settab=true;
			$scope.selectedExpiration=false;
			$scope.ExpirationValue=0;
			$scope.getSystemSetting=function(){
				$http({
					method:"POST",
					url:"/admin/systemmanagement/systemsetting"
				}).success(function(response){
					$scope.systemSetting=response;
					$scope.ExpirationValue=response.passwordSetting[0].password_expiration_time;
					if($scope.ExpirationValue>0){
						$scope.selectedExpiration=true;
					}
					if($scope.systemSetting.fetchallvalues.length>0){
						$scope.newsetting=0;
					}else{
						$scope.newsetting=1;
					}
					
					
				})
			}
			$scope.getSystemSetting();
			
			$scope.updateGeneralSetting=function(){
				$scope.preloader=true;
				$scope.newfetchallvalues=$scope.systemSetting.fetchallvalues[0];
				if($scope.newfetchallvalues.input_training_records==false){
					var trainingrecords=0;
				}else if($scope.newfetchallvalues.input_training_records==true){
					var trainingrecords=1;
				}
				if($scope.newfetchallvalues.require_supervisor_approval==false){
					var supervisorapproval=0;
				}else if($scope.newfetchallvalues.require_supervisor_approval==true){
					var supervisorapproval=1;
				}
				if($scope.newfetchallvalues.update_user_info==false){
					var updateuserinfo=1;
				}else if($scope.newfetchallvalues.update_user_info==true){
					var updateuserinfo=0;
				}
				if($scope.newfetchallvalues.can_edit_skills==false){
					var caneditskills=0;
				}else if($scope.newfetchallvalues.can_edit_skills==true){
					var caneditskills=1;
				}
				if($scope.newfetchallvalues.automated_registration==false){
					var autoregister=0;
				}else if($scope.newfetchallvalues.automated_registration==true){
					var autoregister=1;
				}
				if($scope.newfetchallvalues.training_feedback==false){
					var training_feedback=1;
				}else if($scope.newfetchallvalues.training_feedback==true){
					var training_feedback=0;
				}
				if($scope.newfetchallvalues.classroom_feedback==false){
					var classroom_feedback=1;
				}else if($scope.newfetchallvalues.classroom_feedback==true){
					var classroom_feedback=0;
				}
				if($scope.newfetchallvalues.change_username_password==false){
					var change_username_password=0;
				}else if($scope.newfetchallvalues.change_username_password==true){
					var change_username_password=1;
				}
				if($scope.newfetchallvalues.is_fail_transcript==false){
					var is_pass_transcript=0;
				}else if($scope.newfetchallvalues.is_fail_transcript==true){
					var is_pass_transcript=1;
				}
				if($scope.newfetchallvalues.supervisor_training_view==false){
					var supervisor_training_view=0;
				}else if($scope.newfetchallvalues.supervisor_training_view==true){
					var supervisor_training_view=1;
				}
				if($scope.newfetchallvalues.downgrade_role==false){
					var downgrade_role=0;
				}else if($scope.newfetchallvalues.downgrade_role==true){
					var downgrade_role=1;
				}
				if($scope.newfetchallvalues.promote_user==false){
					var pro_user=0;
				}else if($scope.newfetchallvalues.promote_user==true){
					var pro_user=1;
				}
				if($scope.newfetchallvalues.welcome_user_mail==false){
					$scope.newfetchallvalues.welcome_user_mail=0;
				}else if($scope.newfetchallvalues.welcome_user_mail==true){
					var welcome_user_mail=1;
				}
				
				$http({
					method:"POST",
					url:"/admin/systemmanagement/systemmgt",
					data:{updateuserinfo:updateuserinfo,training_feedback:training_feedback,classroom_feedback:classroom_feedback,autoregister:autoregister,trainingrecords:trainingrecords,fetchValue:$scope.newfetchallvalues,compsetting:'compsetting','newsetting':$scope.newsetting,items:$scope.newfetchallvalues.rating,supervisorapproval:supervisorapproval,caneditskills:caneditskills,admin_email:$scope.newfetchallvalues.admin_email,change_username_password:change_username_password,supervisor_training_view:supervisor_training_view,company_name:$scope.newfetchallvalues.company_name,promote_user:pro_user,downgrade_role:downgrade_role,approval_emailid:$scope.newfetchallvalues.approval_mailid,welcome_user_mail:welcome_user_mail,is_pass_transcript:is_pass_transcript}
				}).success(function(response){
					$scope.preloader=false;
					if(response=='success'){
						alertify.alert("Settings have been inserted successfully.");
					}
					else if(response=='update') {
						alertify.alert("Settings have been updated successfully.");
					}
					
					
				})
				
			}
			
			$scope.statecheck=function()
			{
              var trainingvalue=$scope.systemSetting.fetchallvalues[0].input_training_records;
              var supervisorapp=$scope.systemSetting.fetchallvalues[0].require_supervisor_approval;
			  if(trainingvalue==true)
				{
					$scope.systemSetting.fetchallvalues[0].require_supervisor_approval=true;
				}
				if(trainingvalue==false)
				{
					$scope.systemSetting.fetchallvalues[0].require_supervisor_approval=false;
				}
				
			}
	   $scope.statecheck2=function()
			{
               var trainingvalue=$scope.systemSetting.fetchallvalues[0].input_training_records;
              var supervisorapp=$scope.systemSetting.fetchallvalues[0].require_supervisor_approval;
			  if(supervisorapp==true)
				{
					$scope.systemSetting.fetchallvalues[0].input_training_records=true;
				}
				
			}
			
			$scope.globalssTab=function(){
				$scope.global_system_set='active';
				$scope.security_system_set='';
				$scope.my_req_set='';
				$scope.permission_set='';
			    $scope.global_system_settab=true;
				$scope.passsecuritydiv=false;
				$scope.requirementdiv=false;
				$scope.subadmindiv=false;
			}
			$scope.securityssTab=function(){
				$scope.global_system_set='';
				$scope.security_system_set='active';
				$scope.my_req_set='';
				$scope.permission_set='';
				$scope.global_system_settab=false;
				$scope.passsecuritydiv=true;
				$scope.requirementdiv=false;
				$scope.subadmindiv=false;
			    
			}
		    $scope.reqtab=function(){
				 $scope.global_system_set='';
				 $scope.security_system_set='';
				 $scope.permission_set='';
				 $scope.my_req_set='active';
				 $scope.global_system_settab=false;
				 $scope.passsecuritydiv=false;
				 $scope.subadmindiv=false;
				 $scope.requirementdiv=true;
			 }
			$scope.permissiontab=function(){
				$scope.global_system_set='';
				$scope.security_system_set='';
				$scope.my_req_set='';
				$scope.permission_set='active';
				$scope.global_system_settab=false;
				$scope.passsecuritydiv=false;
				$scope.requirementdiv=false;
				$scope.subadmindiv=true;
			    
			}
			
			$scope.range = function(min, max, step) {
			step = step || 1;
			var input = [];
			for (var i = min; i <= max; i += step) {
				input.push(i);
			}
			return input;
};

			$scope.passwordComplexity=[{
					 'name':'Never',
					 value:1
				 },{
					 'name':'must mix Alpha-Numeric Character',
					 value:2
				 },{
					 'name':'must mix Alpha-Numeric and Special Character',
					 value:3
				 },{
					 'name':'must mix numbers, UpperCase and lowerCase letter',
					 value:4
				 },{
					 'name':'must mix numbers, UpperCase and lowerCase and special symbol',
					 value:5
				 }
				 ];
		 

			$scope.loginAttempt=[{
					 'name':'3 Times',
					 value:3
				 },{
					 'name':'5 Times',
					 value:5
				 },{
					 'name':'7 Times',
					 value:7
				 }
				 ];
				 
			$scope.lockeffecPassPeriod=[{
					 'name':'15 minutes',
					 value:15
				 },{
					 'name':'30 minutes',
					 value:30
				 },{
					 'name':'60 minutes',
					 value:60
				 },{
					 'name':'Indefinitely',
					 value:0
				 }
				 ];
				 
				 
				 $scope.savepasswordsettings=function(){
					 
					 if($scope.systemSetting.fetchPasswordSetting == true){
						 var CustmPasField = 1 ;
					 }else{
						 var CustmPasField = 0 ;
					 }
					 if($scope.systemSetting.fetchallvalues[0].change_password == true){
						 var change_password = 1 ;
					 }else{
						 var change_password = 0 ;
					 }
					 $http({
						 method:"POST",
						 url:"/admin/systemmanagement/passwordsetting",
						 data:{pass_setting:CustmPasField,password_expiration:$scope.systemSetting.passwordSetting[0].password_expiration_time,password_length:$scope.systemSetting.passwordSetting[0].password_length,password_complexity:$scope.systemSetting.passwordSetting[0].password_complexity,invalidpashit:$scope.systemSetting.passwordSetting[0].maximum_invalid_hit,lockeffecPass:$scope.systemSetting.passwordSetting[0].locked_password_time,change_password:change_password}
					 }).success(function(response){
						 alertify.alert('Password settings saved successfullly');
					 })
				 }
				 
				 
				
				 
				 
				 $scope.requpdate=function(){
					var lp_req_value = 0 ;
					var assess_req_value = 0 ;
					var assign_req_value = 0;
					if($scope.systemSetting.fetchallvalues[0].lp_complete == true){
						var lp_req_value = 1 ;
					}	
					if($scope.systemSetting.fetchallvalues[0].assessment_complete == true){
						var assess_req_value = 1 ;
					}
					if($scope.systemSetting.fetchallvalues[0].assignment_complete == true){
						var assign_req_value = 1 ;
					}
					
					$http({
						method:"POST",
						url:"/admin/systemmanagement/myrequirementsetting",
						data:{lp_req_value:lp_req_value,assess_req_value:assess_req_value,assign_req_value:assign_req_value}
					}).success(function(response){
						alertify.alert('Settingss updated sucessfully.');
					})
						
				 }
				 
				 $scope.permupdate=function(){
					var user_based_on = 0 ;
					var user_edit_permission = 0 ;
					var user_based_on_instructor = 0 ;
					var user_edit_permission_instructor = 0;
					user_based_on=$scope.systemSetting.submitPermission[0].user_based_on;
					user_edit_permission=$scope.systemSetting.submitPermission[0].user_edit_permission;
					user_edit_permission_instructor=$scope.systemSetting.submitPermission[0].user_edit_permission_instructor;
					user_based_on_instructor=$scope.systemSetting.submitPermission[0].user_based_on_instructor;
					$http({
						method:"POST",
						url:"/admin/systemmanagement/subadminpermissionsetting",
						data:{user_based_on:user_based_on,user_edit_permission:user_edit_permission,user_based_on_instructor:user_based_on_instructor,user_edit_permission_instructor:user_edit_permission_instructor}
					}).success(function(response){
						alertify.alert('Settingss updated sucessfully.');
					})
				 }
		 


	   

	    
});	
