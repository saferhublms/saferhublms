/* Courses Page Diectives*/
app.directive("addcourseDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/templates/courses/addcourse.html'	
    };
});
app.directive("breadcrumbDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/services/directive_template/breadcrumb.html'	
    };
});
app.directive("datatablecourseDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/templates/courses/coursetable.html'	
    };
});
app.directive("addactivitesDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/templates/courses/addactivites.html'	
    };
});
app.directive("addcontentDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/templates/courses/addcontent.html'	
    };
});
app.directive("permissionsDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/templates/courses/permissions.html'	
    };
});
app.directive("optionalDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/templates/courses/optionalitems.html'	
    };
});

/* Courses Page Diectives Ends */


app.directive("activityDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/services/directive_template/activity.html'	
    };
});

app.directive("notificationDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/services/directive_template/notification.html'	
    };
});
app.directive("previewDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/services/directive_template/perviewPopupDir.html'	
    };
});
app.directive("mathDirective", function() {
    return {
		restrict : "E",
		templateUrl : '/administrator/services/directive_template/generateMath.html'	
    };
});
app.directive("settings", function() {
    return {
        template : '<div uib-dropdown style="display: inline-block; position: unset !important;"><button class="btn btn-white" uib-dropdown-toggle type="button" style="border-radius: 0 3px 3px 0;"><i class="fa fa-cog"></i></button><ul uib-dropdown-menu style="left: -35px;"><li><a href="">Manage Classes</a></li><li><a href="">Manual Entry</a></li><li><a href="">Analyze</a></li><li><a href="">Duplicate</a></li><li><a href="">Add to Requirement</a></li></ul></div>'
    };
});

angular.module('uiSwitch', [])

app.directive('switch', function(){
	return {
		restrict: 'AE',
		replace: true,
		transclude: true,
		template: function(element, attrs) {
			var html = '';
			html += '<span';
			html +=   ' class="switch' + (attrs.class ? ' ' + attrs.class : '') + '"';
			html +=   attrs.ngModel ? ' ng-click="' + attrs.disabled + ' ? ' + attrs.ngModel + ' : ' + attrs.ngModel + '=!' + attrs.ngModel + (attrs.ngChange ? '; ' + attrs.ngChange + '()"' : '"') : '';
			html +=   ' ng-class="{ checked:' + attrs.ngModel + ', disabled:' + attrs.disabled + ' }"';
			html +=   '>';
			html +=   '<small></small>';
			html +=   '<input type="checkbox"';
			html +=     attrs.id ? ' id="' + attrs.id + '"' : '';
			html +=     attrs.name ? ' name="' + attrs.name + '"' : '';
			html +=     attrs.ngModel ? ' ng-model="' + attrs.ngModel + '"' : '';
			html +=     ' style="display:none" />';
			html +=     '<span class="switch-text">'; /*adding new container for switch text*/
			html +=     attrs.on ? '<span class="on">'+attrs.on+'</span>' : ''; /*switch text on value set by user in directive html markup*/
			html +=     attrs.off ? '<span class="off">'+attrs.off + '</span>' : ' ';  /*switch text off value set by user in directive html markup*/
			html += '</span>';
			return html;
		}
	}
});


