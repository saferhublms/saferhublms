<script type="text/javascript" async
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
</script>
<style>
.rightside_tab {
    position: fixed;
    right:60px;
    top: 10%;
    z-index: 1;
    background: #eeeeee;
    background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%);
    background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%);
    background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%);
    padding: 10px 6px 10px 6px;
    border: 1px solid #000;
    -webkit-writing-mode: vertical-lr;
    -ms-writing-mode: tb-lr;
    writing-mode: lr;
    }
.rightside_tab a {
    text-transform: uppercase;
    text-decoration: none;
    font-size: 14px;
    font-weight: 700;
    color: #000;
letter-spacing: 1px;}
.time{
	margin-top: 10px;
	background: #fff;
    padding: 8px 6px 8px 6px;
  color:#000;
  font-size:20px;
  text-align:center;
  width: 40%;
  float: right;


}
</style>
<?php
$baseUrl='';

$datamodel ='';
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="/user/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/administrator/assets/css/main.css" rel="stylesheet" type="text/css" />
<link href="/administrator/assets/css/inv-style.css" rel="stylesheet" type="text/css" />
<link href="/user/assets/css/questioncss/inv-style.css" rel="stylesheet" type="text/css" />
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
<script type="text/javascript" src="/runplayer/admin-js/jquery-1.js"></script>

<script type="text/javascript" src="/runplayer/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/user/assets/js/alertify.min.js"></script>
<!--<script type="text/javascript" src="<?php echo $baseUrl;?>/public/js/assessment/ajax.js"></script>-->

<!-- char counter code begins       -->

<script type="text/javascript">
var maxLength=500;
function charLimit(el) {
	if (el.value.length > maxLength){
		return false;
	}	
	return true;
}
function characterCount(el) {
	
	if (el.value.length > maxLength) {
		el.value = el.value.substring(0,maxLength);
		alert('You have exceeded the maximum limit of 500 characters. Please edit the text and try again.');
	}
	
	var textareaid = el.id;
	var SplitTextareaId = textareaid.split("_");
	var charCount = document.getElementById('charCount_'+SplitTextareaId[1]);
	if (charCount) charCount.innerHTML = maxLength - el.value.length;
	return true;
}
</script>
	
<!--   char counter code ends     -->	
	
<script src="<?php echo $baseUrl; ?>/public/js/assessment/ajax.js" type="text/javascript" charset="utf-8"></script>
<!--link href="<?php// echo $baseUrl; ?>/public/css/main.css" rel="stylesheet" type="text/css" /-->
<!--link href="<?php //echo $baseUrl; ?>/public/css/inv-style.css" rel="stylesheet" type="text/css" /-->
<form name="f1" method="post" id="frm1" action="/mylearning/submitlistassessment">
	{{csrf_field()}}
	<input type="hidden" id="baseurl" name="baseurl" value="<?php echo $baseUrl; ?>"/>
	<input type="hidden" id="assessid" name="assessid" value="<?php if (count($assementdetails) > 0) { echo $assementdetails['id'];}?>"/>
	
	<input type="hidden" id="user" name="user" value="<?php echo $user;?>"/>
	<input type="hidden" id="token" name="token" value="<?php echo $token;?>"/>
	<input type="hidden" id="listview" name="listview" value="1"/>
	<input type="hidden" id="hdnTimer" name="hdnTimer" value="<?php echo $totalsecond; ?>"/>
	<input type="hidden" id="is_timer" name="is_timer" value="<?php echo $istimer; ?>"/>
	

	<!--Contenar start-->
	<input type="hidden" id="baseurl" name="baseurl" value="<?php echo $baseUrl; ?>"/>
	<input type="hidden" id="multiSelJsonString" name="multiSelJsonString" value=""/>
	<?php if($totalsecond){ ?>
               <div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4"></div>
					<div class="col-md-4"><div class="time">Time left = <span id="timer"></span></div></div>
			   </div>
	<?php } ?>
	<div class="contenar-box">
		<div class="wp100 bs01 fl">
			<div class="wp100 fl" style="height: 100%;
    overflow-y: scroll;">
				<div class="wp100 ">
					<div class="dash_top_bg bob01 fl  fc18 fta fs14 pl10 b mt05 pb05 ">
						
						<?php 
						
					$assementdetails = $assementdetails;
		
						if (count($assementdetails) > 0) {
							echo $assementdetails['assessment_name'];
						}?>
						<?php if(isset($assementdetails['close_date']))
							{
								$close_time=strtotime($assementdetails['close_date']);
								
								 $current_time=strtotime(date('Y-m-d'));
							}
						if($assementdetails['iscompleted']==1){?>
						<div class="fr mr10"> Submitted Date:	<?php echo date($Getdate_formatecomapny,strtotime($assementdetails['lastmodifydate']));?></div>
						<?php }elseif($current_time > $close_time){?>
						<div class="fr mr10"> This assessment closed on : <?php echo date($Getdate_formatecomapny,$close_time);?> 	</div>
						<?php }else{
							if($assementdetails['due_date'] != '0000-00-00') { ?>
						<div class="fr mr10"> Due Date:	<?php echo date($Getdate_formatecomapny,strtotime($assementdetails['due_date']));?></div>
							<?php } else if(!empty($userAssignDate) &&  $userAssignDate != '0000-00-00')
									{   ?>
						<div class="fr mr10"> Due Date:	<?php echo date($Getdate_formatecomapny,strtotime($userAssignDate));?></div>				
							<?php	}
							
							}?>
					</div>
					
					<div class="dashboard-right-part wp100 mt10 fl pl10 pr10">
						<div class="fl bob01 wp100">
							<div class="training-space-icon wp03 ml05 h24 mt05 fl">
							<i class="fa fa-user fs30 fc35"></i>
							<!--img src="<?php //echo $baseUrl; ?>/public/images/u168.png" /-->
							
							</div>
							<div class="dashboard-training-space mt06 fs13 ml06 fta fc03 b wp40 fl"><?php if (count($assementdetails) > 0) {
							echo $assementdetails['assessment_desc'];
						}?></div>
							<div class="fr fta fs12 b">
								<?php if(count($fetchuploadedfiles)>0){?>
								<a href="<?php echo $baseUrl;?>/survey/viewattachments/assessid/<?php echo $assessid?>/scheid/<?php echo $scheid;?>?height=400&width=650&inlineId=hiddenModalContent&modal=false" class="thickbox view  button_lblue_r4">
									Download File <span>&#187;</span>
								</a>
								<?php } ?>
							</div>
							<div class="fr w970 mr10 mt02">
								<input type="hidden" name="score_id" id="score_id" value="<?php echo $assementdetails['score_type_id'];?>">
								<?php if(@$assementdetails['nooftimes']<1 and $assementdetails['iscompleted']!=1){ $left=1-@$assementdetails['nooftimes'];}
								
								if(@$assementdetails['nooftimes']==1 || $assementdetails['iscompleted']==1){
								
									if($assementdetails['score_type_id']!=4)
									{
										
										if($assementdetails['totalrightper'] >= $assementdetails['pass_percentage'])
										{$result="Pass";}
										elseif($assementdetails['totalrightper'] < $assementdetails['pass_percentage'])
										{$result="Fail";}
										echo '<b>Passing Score : </b>'.$assementdetails['pass_percentage'].'% &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
										if($totalrightper==100)
										{
											echo '<b>Your Score : </b>'.ceil($assementdetails['totalrightper']).'% &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
										}else
										{
											echo '<b>Your Score : </b>'.$assementdetails['totalrightper'].'% &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										echo '<b>Result : </b>'.$result;
									}
								}elseif($current_time > $close_time){
									echo '<span style="float:right;font-weight:bold;font-size:13px;">You have not completed this assessment</span>';
								}
								if(@$assementdetails['nooftimes']>3){
									?>
									<div class="fr w25 mt07"></div>
								<?php } ?>
							</div>
						</div>
   <input type="hidden" name="submitdata" id="submitdata" value="1"/>
	<div class=" wp100 fl">
		<?php 
		$queststr="";
		$questionidstr="";
		$multipleselstr="";
		$mulstr="";
		$useranswerArr=array();
		$useranswer="";
				
		if (count($assementdetails) > 0) {
		$x=1;

		foreach ($assementdetails['questionarray'] as $val) {
		
			$quesname = $val['question'];
			$questionid = $val['question_id'];
			$quesno1 = $val['ques_no'];
			 $userans	=	$val['userans'];
			 $ansoptions = $val['ansoptions'];
			 $useranswerArr=array();
		$useranswer="";
		if(count($userans)>0){
			 if($val['question_type_id']==2)
			{
				foreach($userans as $userans){		
				array_push($useranswerArr,trim($userans['answer']));
				}
			}else{
				if($userans[0]['answer']!="")
				{
					$useranswer=$userans[0]['answer'];
				}
				else{
					$useranswer='';
				}	
			}
			}
								
		?>
<div class="fl wp90" id="fullanswer_<?php echo $questionid; ?>" >
<div class=" wp75  fs18 b fta   fl">
<div class="dashboard-training-space mt10 fs13  fta fc02 b w25 fl"><?php echo $x; ?>:</div>
<div class="dashboard-training-space mt10 fs13  fta fc02 b wp70 fl" ><?php echo stripcslashes($quesname); ?></div>
</div>
<?php


	$quesno = $val['ques_no'];
	$quesname = $val['question'];
	$answer = $val['answer'];
	$descriptive = $val['descriptive'];
	$questypeid = $val['question_type_id'];
	$questionid = $val['question_id'];
	$randomanswers=$val['random_answers'];
	?>
	<input type="hidden" id="questionid" name="questionid" value="<?php echo $questionid ?>"/>
											
	<?php
	if ($questypeid == 3) {?>
	<div id="choosetruefalse_'<?php echo $questionid;?>'" style="display:show; margin-left: 25px;border:0px solid #f00;">
			
		<div class="wp98  ml  bgc01  fl mb10" >
			<div class="wp50 fs13  ml06  bgc01  fl" style="display:none">
				<div class="fl mt10 "><?php echo $descriptive;?></div>
			</div>
			<?php if($assementdetails['score_type_id']!=4){
			if($assementdetails['iscompleted']==1)
					{?>
			<div  style="width:5%;float:left;border:0px solid #f00;margin-top:12px;border:0px solid #f00;">		
			<?php
			if(@$userans[0]['answer']!=""){
			?>
			<?php if($answer==@$userans[0]['answer']){?>
				<img  border="0"src="<?php echo $baseUrl;?>/public/images/check_icon_new.png"/>
			<?php }else{?>
				<img  border="0"src="<?php echo $baseUrl;?>/public/images/cross_icon_new.png"/>
			<?php }}elseif(isset($assementdetails['lastmodifydate']) && count($userans)<1){?>
			<img  border="0"src="<?php echo $baseUrl;?>/public/images/cross_icon_new.png"/>
			<?php }?>
			</div>
			<?php }}?>
			<div class="mt12 fl fta fs12 fc02" style="border:0px solid #f00;">
			<?php if($current_time > $close_time){?>
			<input type="radio" disabled="disabled">&nbsp;&nbsp; True&nbsp;&nbsp;&nbsp;<input type="radio" disabled="disabled">&nbsp;&nbsp; False
			<?php }elseif(isset($assementdetails['lastmodifydate']) && count($userans)<1) {?>
			<input type="radio" disabled="disabled">&nbsp;&nbsp; True&nbsp;&nbsp;&nbsp;<input type="radio" disabled="disabled">&nbsp;&nbsp; False
			<?php }else{?>
				<input  type="radio"  name="trueval_<?php echo $questionid;?>" id="trueval_<?php echo $questionid;?>" value="truevalue" <?php if(count($userans)>0) { if($userans[0]['answer']=='truevalue') { ?> checked="checked" <?php } if($userans[0]['answer']=='falsevalue') {?> disabled="true" <?php }} ?>/>&nbsp;&nbsp; True&nbsp;&nbsp;&nbsp;
				<input   type="radio" name="trueval_<?php echo $questionid;?>" id="trueval_<?php echo $questionid;?>" value="falsevalue" <?php if(count($userans)>0) {if($userans[0]['answer']=='truevalue') {?> disabled="true"  <?php } if($userans[0]['answer']=='falsevalue') {?> checked="checked" <?php }} ?>/>&nbsp;&nbsp; False</div>
			<?php }?>	
		</div>
	</div>
	<?php } ?>
	<?php 
	if ($questypeid == 4) {
		?>
		<div id="chooseval" style="display:show; margin-left: 25px;border:0px solid #f00;">
				
			<div class="wp98  ml  bgc01  fl mb10" style="border:0px solid #ff0;">
				<div class="wp50 fs13  ml06  bgc01  fl" style="display:none;">
					<div class="fl mt10 "><?php echo $descriptive;?></div>
				</div>
				<?php if($assementdetails['score_type_id']!=4){
				if($assementdetails['iscompleted']==1)
				{?>
				<div  style="width:5%;float:left;border:0px solid #f00;margin-top:12px;">		
				<?php 
				if(@$userans[0]['answer']!=""){
				?>
				<?php if($answer==@$userans[0]['answer']){?>
					<img  border="0"src="<?php echo $baseUrl;?>/public/images/check_icon_new.png"/>
				<?php }else{?>
					<img  border="0"src="<?php echo $baseUrl;?>/public/images/cross_icon_new.png"/>
				<?php }}elseif(isset($assementdetails['lastmodifydate']) && count($userans)<1){?>
				<img  border="0"src="<?php echo $baseUrl;?>/public/images/cross_icon_new.png"/>
				<?php }?>
				</div>
				<?php }}?>
				<div class=" mt12 fl fta fs12 fc02"style="border:0px solid #000;">
				<?php if($current_time > $close_time){?>
				<input type="radio" disabled="disabled">&nbsp;&nbsp; Yes&nbsp;&nbsp;&nbsp;<input type="radio" disabled="disabled">&nbsp;&nbsp; No
				<?php }elseif(isset($assementdetails['lastmodifydate']) && count($userans)<1) {?>
				<input type="radio" disabled="disabled">&nbsp;&nbsp; Yes&nbsp;&nbsp;&nbsp;<input type="radio" disabled="disabled">&nbsp;&nbsp; No
				<?php }else{?>
					<input  type="radio"  name="yesradio1_<?php echo $questionid;?>" id="yesradio1_<?php echo $questionid;?>" value="yes" <?php if(count($userans)>0) { if($userans[0]['answer']=='yes') { ?> checked="checked" <?php } if($userans[0]['answer']=='no') {?> disabled="true" <?php }} ?>/>&nbsp;&nbsp; Yes&nbsp;&nbsp;&nbsp;
					<input   type="radio" name="yesradio1_<?php echo $questionid;?>" id="yesradio1_<?php echo $questionid;?>" value="no" <?php if(count($userans)>0) {if($userans[0]['answer']=='yes') {?> disabled="true"  <?php } if($userans[0]['answer']=='no') {?> checked="checked" <?php }} ?>/>&nbsp;&nbsp; No</div>
				<?php }?>	
			</div>
		</div>
		
	<?php } ?>
<?php if ($questypeid == 5) {
	$submittextdata=0;?>
	<div id="describe" style="display:show; border:0px solid #f00;">
		<?php 
		$msrgin="";
		if($assementdetails['score_type_id']!=4){
			if($assementdetails['iscompleted']==1)
			{
				$msrgin="ml20";
			}
		}	?>
		<div class=" wp60 fl mb03 mt10 <?php echo $msrgin;?>" style="float:left;border:0px #000 solid; margin-left: 25px;">
			
			<?php if(count($userans)>0 and $userans[0]['descriptive']!='') {?>
			<textarea class="form-control" onKeyPress="return charLimit(this)" onKeyUp="return characterCount(this)" rows="4" cols="50" name="descriptive_<?php echo $questionid;?>" id="descriptive_<?php echo $questionid;?>" <?php if(count($userans)>0) {?> readonly="readonly" <?php } ?>><?php echo trim(@stripslashes($userans[0]['descriptive'])); ?></textarea>
			<?php $submittextdata=1;
			}?>
			<?php if(isset($assementdetails['lastmodifydate']) && count($userans)<1) {?>
			<textarea  class="form-control" rows="4" cols="50" disabled="disabled" ></textarea>
			<?php $submittextdata=1; }?>
			<?php if($submittextdata==0){ ?>
			<textarea  class="form-control" onKeyPress="return charLimit(this)" onKeyUp="return characterCount(this)" rows="4" cols="50" name="descriptive_<?php echo $questionid;?>" id="descriptive_<?php echo $questionid;?>" <?php if(count($userans)>0) {?> readonly="readonly" <?php } ?> ></textarea>
			<p class="fr mt05" style="background-color: #EDEDED; border: 1px solid #CCCCCC; padding: 1px 5px; font-size: 13px; margin-right: -3px;"><span id="charCount_<?php echo $questionid;?>">500</span></p>
			<?php } ?>
			
		</div>
	</div>
<?php } ?>
<?php if ($questypeid == 1) { ?>
	<div class="" id="dataofmultichoicediv_<?php echo $questionid; ?>" >
		<?php
		
		$answerarray=array();
		
			foreach($ansoptions as $val){
																		
				$answerarray[]=$val['answer_id']; 
			}
			
	foreach ($ansoptions as $val) {
			
				$description=$val['description'];
				 $answerid=$val['answer_id'];
				$iscorrect=$val['is_correct'];													
				$check = '';
				if($iscorrect==1){
					$check = 'checked';
				}?>
				<div class="fl wp100 ml25"style="border:0px solid #f00;">
					<?php if($assementdetails['score_type_id']!=4){
					if($assementdetails['iscompleted']==1)
					{?>
					<div  style="width:5%;float:left;border:0px solid #f00;margin-top:15px;">		
					<?php if(@$userans[0]['answer']!=""){?>
					<?php if($userans[0]['answer']==$answer && $answerid==@$userans[0]['answer']){?>
								<img  border="0"src="<?php echo $baseUrl;?>/public/images/check_icon_new.png"/>
						<?php }
						elseif($answerid==@$userans[0]['answer'] && $answer!=$answerid){?>
							<img  border="0"src="<?php echo $baseUrl;?>/public/images/cross_icon_new.png"/>
						<?php }elseif($answer==$answerid){?>
						<img  border="0"src="<?php echo $baseUrl;?>/public/images/arrow_icon_new.png"/>
						<?php }}else{
						if($answer==$answerid){
						?>
						<img  border="0"src="<?php echo $baseUrl;?>/public/images/arrow_icon_new.png"/>	
						<?php }}?>
					</div>
					<?php }}?>
					<?php if($current_time > $close_time){
					?>
						<div class="w30 mt15 fl fta fs12 fc02">
					
						<input type="radio"  disabled="disabled" /></div>
						<div class=" wp95 fl mt20  fta fs12 fc02 ">
						<?php echo $description; ?>
						</div>
					<?php }elseif(isset($assementdetails['lastmodifydate']) && count($userans)<1){?>
					<div class="w30  mt15 fl fta fs12 fc02">
						<input type="radio"  disabled="disabled" /></div>
						<div class=" wp85 fl mt20  fta fs12 fc02 ">
						<?php echo $description; ?>
						</div>
					<?php }else{?>
					<div class="w30 mt15 fl fta fs12 fc02" style="border:0px solid #f00;">
					
					<input type="radio" name="multichoicecorrectineditradio_<?php echo $questionid;?>" id="multichoicecorrectineditradio_<?php echo $questionid;?>" value="<?php echo $questionid."||".$answerid;?>"
					<?php if(count($userans)>0 and  $answerid==$userans[0]['answer']){?> checked="checked" <?php } ?>
					<?php if(count($userans)>0 and  $answerid!=$userans[0]['answer']){?>disabled="true"  <?php } ?>/></div>
					<div class=" wp85 fl mt20  fta fs12 fc02 ">
						<?php echo $description; ?>
						<input readonly="readonly" type="hidden" name="multichoicecorrectinedittext_<?php echo $questionid;?>_<?php echo $answerid;?>" id="" class="ans_size" value="<?php echo trim($description,'"'); ?>"/></div>
					<?php } ?>	
					</div>
		<?php }
		 ?>
	</div>
<?php } ?>
<?php if ($questypeid == 2) 
	{?>
	<div id="dataofmultiselectdiv_<?php echo $questionid;?>" >
			<?php

			$m=0;
			foreach($ansoptions as $fullvalues) 
			{
			$answerid=trim($fullvalues['answer_id']);
			$optiontext=$fullvalues['description'];
			$correctvalue=$fullvalues['is_correct'];
			$checkboxval='';
			$checked='';
			if($correctvalue==1){
			$checkboxval=$correctvalue;
			$checked='checked=checked';
			}
			?>
			<div class="fl wp100 ml25"style="border:0px solid #f00;">
			<?php 
			if($assementdetails['iscompleted']==1)
			{
			if($assementdetails['score_type_id']!=4){?>
			<div  style="width:5%;float:left;border:0px solid #f00;margin-top:15px;">		
					<?php 
					if(in_array($answerid,$useranswerArr) && $correctvalue==1){?>
						<img  border="0"src="<?php echo $baseUrl;?>/public/images/check_icon_new.png"  />
					<?php }
					elseif(!in_array($answerid,$useranswerArr) && $correctvalue==1){?>
						<img  border="0"src="<?php echo $baseUrl;?>/public/images/arrow_icon_new.png"  />
					<?php }
					elseif(in_array($answerid,$useranswerArr) && $correctvalue==0){?>
						<img  border="0"src="<?php echo $baseUrl;?>/public/images/cross_icon_new.png"  />
					<?php }?>
			</div>
					<?php }
			}?>
			<?php if($current_time > $close_time){?>

				<div class="w30 mt15 fl fta fs12 fc02"><input type="checkbox" disabled="disabled" /></div>
				<div class=" wp95 fl mt18  fta fs12 fc02 " >
				<div class=" wp100 fl mb03"><?php echo $optiontext;?></div>
				</div>
			<?php }elseif(isset($assementdetails['lastmodifydate']) && count($userans)<1){?>
				<div class="w30 mt15 fl fta fs12 fc02"><input type="checkbox" disabled="disabled" /></div>
				<div class="wp85 fl mt18  fta fs12 fc02">
				<div class=" wp100 fl mb03"><?php echo $optiontext;?></div>
				</div>
			<?php }else{?>
			<div class="w30 mt15 fl fta fs12 fc02"><input type="checkbox"  name="mseloptinedit_<?php echo $questionid. '_' . $answerid;?>" id="mseloptinedit_<?php echo $questionid. '_' . $answerid;?>" value="<?php echo  $questionid .'||'. $answerid;?> " <?php if(count($userans)>0){ ?> disabled="true" <?php if(in_array($answerid,$useranswerArr)){
			?> checked="checked" disabled="true" <?php } }?>  /></div>
			<div class="wp85 fl mt18  fta fs12 fc02">
				<div class=" wp100 fl mb03"><?php echo $optiontext;?></div>
			</div>
			<?php }?>
			</div>
			<?php 
			if($m==0)
			{
			$mulstr= $answerid;
			$m++;
			}else{
			$mulstr.= "_".$answerid;
			}
			}?>
			</div>
			<?php }?>
			<?php 
			$queststr.=$questypeid.",";
			$questionidstr.=$questionid.",";
			if($mulstr!="")
			{
				$multipleselstr.=$mulstr.",";
				$mulstr="";
			}
			?>
		<?php	$x++; ?>
	<?php  ?>	
</div>
<?php }} ?>
</div>
<?php 
if($close_time >= $current_time){?>
<div class="wp90 mb15 fs13 fta bgc01 fc03  fl">
	<input type="hidden" name="confirmedradio" id="confirmedradio"  value="yes"/>
	<input type="hidden" name="questiontypeansids" id="questiontypeansids" value="<?php echo $questionidstr;?>">
	<input type="hidden" name="questiontypeans" id="questiontypeans" value="<?php echo $queststr;?>">
	<input type="hidden" name="multiquestionansv" id="multiquestionansv" value="<?php echo $multipleselstr;?>">
	<input type="hidden" name="toalquestionasked" id="toalquestionasked" value="<?php echo $x-1;?>">
	<div class=" wp98  ml07 mt10 mt25 mb20 fl">
		<div class=" fl hand ">
		<div class=" fl hand ">
			<?php    
			
			if($close_time >= $current_time and $mode!='learningtab'){
			
				if($assementdetails['iscompleted'] !=1 and $mode!='learningtab'){?>
					<div onclick="submitbyuserassessment_new();" class="rightside_tab" style="top: 18%;"> <a href="">Submit <span>&#187;</a></span></div>
				<?php } ?> 								  
			<?php }?>
			
		<?php if($assementdetails['iscompleted'] ==1){?>	
		<?php if($attempttaken > 0) { ?>
		<?php  if($attempttaken > $userattemttaken){?>
		<?php  $url ='/mylearning/viewassessmentretake/viewId/'.$token;?>
		<a class="new_css_button" style="color:#fff;" onclick="window.location.href='<?php echo $url;?>';">Retake Quiz</a>	
		</a><?php } else { ?>
		<div class="w500 mt50">
		You have exceeded the number of attempts allowed. Please Contact your administrator if you need further assistance.
		</div>
  <?php } ?>
  <?php } ?>
  <?php } ?>
	</div>
	</div>
</div>
<?php }?>
</div>
<div class="ext-box mr18 h50 mt15 w650 fr"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
<script>
function submitbyuserassessment_new()
{

	/* var e = event.keyCode;
	if(e == 9)
	{
		return false;
	} */
	var linkertvalueinedit=0;
	var trueval=0;
	var yesradio1=0
	var descriptive=0;
	var multichoicecorrectineditradio=0;
	var mseloptinedit=0;
	var totalquestion=document.getElementById("toalquestionasked").value;
	var questiontypeans=document.getElementById("questiontypeans").value;
	var questiontypeansids=document.getElementById("questiontypeansids").value;
	var multiquestionansvids=document.getElementById("multiquestionansv").value;
	
	var splitquestiontypes=questiontypeans.split(",");
	var splitquestiontypesids=questiontypeansids.split(",");
	var mulidsdis=multiquestionansvids.split(",");
	var mulop=0;
	var startcount=0;
	var loopstr=splitquestiontypes.length-1;
	for(qt=0;qt<=loopstr;qt++)
	{
		if(splitquestiontypes[qt]!="")
		{
			if(splitquestiontypes[qt]==1)
			{
				var fieldname="multichoicecorrectineditradio_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			/* if(splitquestiontypes[qt]==2)
			{
				var fieldname="linkertvalueinedit_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			} */
			if(splitquestiontypes[qt]==3)
			{
				var fieldname="trueval_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==4)
			{
				var fieldname="yesradio1_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==5)
			{
				var fieldname="descriptive_"+splitquestiontypesids[qt];
				//var group = document.getElementsByName(fieldname);
				if(document.getElementById(fieldname).value!="")
				{
					startcount++;
					
					//break;
				}
				
			}
			if(splitquestiontypes[qt]==2)
			{
				var totalmultpleoptionid=mulidsdis[mulop];
				var splitoptionmul=totalmultpleoptionid.split("_");
				var mulsel=0;
				for(mt=0;mt<splitoptionmul.length;mt++)
				{
					var fieldname="mseloptinedit_"+splitquestiontypesids[qt]+"_"+splitoptionmul[mt];
					var group = document.getElementsByName(fieldname);
					for (var i=0; i<group.length; i++) 
					{
						if (group[i].checked)
						{
							mulsel++;
							break;
						}
					}
					if(mulsel!=0)
					{
						startcount++;
						break;
					}
				}
				mulop++;
			}
		}
	}
	
	
	var multiSelArr = new Array();
	$('input[type="checkbox"]:checked').each(function() {
	   multiSelArr.push(this.value);
	});
	var multiSelJsonString = JSON.stringify(multiSelArr);
	$("#multiSelJsonString").val(multiSelJsonString);

	var baseurl=document.f1.baseurl.value;
	var userid=document.f1.user.value;
	var assessid=document.f1.assessid.value;

	var confirmedradio=document.getElementById('confirmedradio');
	var choosen=1;
     var istimer = $('#istimer').val(); 
	if(assessid!='')
	{
		if(istimer==0){
			//var scoreid=document.getElementById('score_id').value;
			if((startcount!=0) && (totalquestion!=0))
			{
				if(startcount>0) {
					alertify.confirm("You have not responded to all questions. Are you sure you want to submit your results?", function (e) {
						if (e) {
							/* $.post("/elearningmedia/checkloginstatus/reqdata/<?php echo $datamodel; ?>",function(data){
								if(data.status=="success"){
									document.f1.submit();
								}else{
									alert("Your session has been expired..");
									return false;
								}
							},"json"); */
							document.f1.submit();
							
						}
					});
					return false;
				} else {
					alertify.confirm("You have not responded to all questions, Please select the answer.", function (e) {
						return false;
					});
				}
			}
			else{
				alertify.alert("You have not responded to any questions, Please select the answer.");
				return false;
				/* $.post("<?php echo $baseUrl;?>/elearningmedia/checkloginstatus/reqdata/<?php echo $datamodel; ?>",function(data){
					if(data.status=="success"){
						document.f1.submit();
					}else{
						alert("Your session has been expired..");
						return false;
					}
				},"json"); */
			}
			}else{
				var valueTimer = $('#hdnTimer').val();
					if(valueTimer==0){
						alertify.alert('Time is out');		
						document.f1.submit();
					}
				}
	}else{
		return false;
	}
}
</script>
<script>
	MyTimer();
	setInterval( "MyTimer()" , 1000);
	function MyTimer()
	{
	   var valueTimer = $('#hdnTimer').val(); 
	   if(valueTimer > 0)
	   {
		   valueTimer = valueTimer - 1; 
		   hours = (valueTimer/3600).toString().split('.')[0];
		   mins  = ((valueTimer % 3600) / 60).toString().split('.')[0]; 
		   secs  = ((valueTimer % 3600) % 60).toString(); 
		   
		   if(hours.length == 1) hours = '0' + hours; 
		   if(mins.length  == 1) mins  = '0' + mins; 
		   if(secs.length  == 1) secs  = '0' + secs; 
		   $('#timer').text( hours + ':' +  mins + ':'  + secs);
		   $('#hdnTimer').val( valueTimer );
		   if(valueTimer==0){
			  submitbyuserassessment_new();
		   }
	   }
	}
</script>