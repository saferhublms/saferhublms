<script type="text/javascript" async
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
</script>
<?php

$datamodel ='';
$baseUrl='';
?>

<meta charset="utf-8">
<script type="text/javascript" src="/runplayer/admin-js/jquery-1.js"></script>
<script type="text/javascript" src="/runplayer/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/user/assets/js/alertify.min.js"></script>
<link rel="stylesheet" type="text/css" href="/administrator/assets/css/alertify.core.css" />
<link rel="stylesheet" type="text/css" href="/administrator/assets/css/alertify.default.css" id="toggleCSS" />
<!-- char counter code begins       -->

<script type="text/javascript">
var maxLength=500;
function charLimit(el) {
	if (el.value.length > maxLength){
		return false;
	}	
	return true;
}
function characterCount(el) {
	
	if (el.value.length > maxLength) {
		el.value = el.value.substring(0,maxLength);
		alert('You have exceeded the maximum limit of 500 characters. Please edit the text and try again.');
	}
	
	var textareaid = el.id;
	var SplitTextareaId = textareaid.split("_");
	var charCount = document.getElementById('charCount_'+SplitTextareaId[1]);
	if (charCount) charCount.innerHTML = maxLength - el.value.length;
	return true;
}
</script>
	
<!--   char counter code ends     -->	
	
<link href="<?php echo $baseUrl; ?>/public/css/main.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $baseUrl; ?>/public/css/inv-style.css" rel="stylesheet" type="text/css" />
<form name="f1" method="post" id="frm1" action="/mylearning/submitlistassessment">
 
	{{csrf_field()}}
	<input type="hidden" id="baseurl" name="baseurl" value="<?php echo $baseUrl; ?>"/>
	<input type="hidden" id="assessid" name="assessid" value="<?php if (count($assementdetails) > 0) { echo $assementdetails['id'];}?>"/>

	<input type="hidden" id="user" name="user" value="<?php echo $user;?>"/>
	<input type="hidden" id="token" name="token" value="<?php echo $token;?>"/>
	<input type="hidden" id="listview" name="listview" value="1"/>
	<input type="hidden" id="hdnTimer" name="hdnTimer" value="<?php echo $totalsecond; ?>"/>
	<input type="hidden" id="is_timer" name="is_timer" value="<?php echo $istimer; ?>"/>
	<?php if($totalsecond){ ?>
               <div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4"></div>
					<div class="col-md-4"><div class="time">Time left = <span id="timer"></span></div></div>
			   </div>
	<?php } ?>
	<!--Contenar start-->
	<input type="hidden" id="baseurl" name="baseurl" value="<?php echo $baseUrl; ?>"/>
	<input type="hidden" id="multiSelJsonString" name="multiSelJsonString" value=""/>
	<div class="contenar-box">
	<div class="dashboard-content bs01 fl" style="height: 100%;
    overflow-y: scroll;">
	<div class="admin-browser-box fl">
	<div class="wp100 ">
	<div class="dash_top_bg bob01 fl  fc18 fta fs14 pl10 b mt05 pb05 ">
	<input type="hidden" name="score_id" id="score_id" value="<?php echo $assementdetails['score_type_id'];?>">
	<?php
	$assementdetails = $assementdetails;
	if (count($assementdetails) > 0) {
	echo $assementdetails['assessment_name'];
	}
	?>
	<?php if(isset($assementdetails['close_date']))
	{
		$close_time=strtotime($assementdetails['close_date']);
		
		 $current_time=strtotime(date('Y-m-d'));
	}
	if($assementdetails['iscompleted']==1){?>
	<div class="fr mr10"> Submitted Date:	<?php echo date($Getdate_formatecomapny,strtotime($assementdetails['lastmodifydate']));?></div>
	<?php }elseif($current_time > $close_time){?>
	<div class="fr mr10"> This assessment closed on : <?php echo date($Getdate_formatecomapny,$close_time);?> 	</div>
	<?php }else{?>
	<div class="fr mr10"> Due Date:	<?php echo date($Getdate_formatecomapny,strtotime($assementdetails['due_date']));?></div>
	<?php }?>
	</div>

					
	<div class="fr fta fs12 b">
	<?php if(count($fetchuploadedfiles)>0){?>
		<a href="<?php echo $baseUrl;?>/survey/viewattachments/assessid/<?php echo $assessid?>/scheid/<?php echo $scheid;?>?height=400&width=650&inlineId=hiddenModalContent&modal=false" class="thickbox view  button_lblue_r4 mt07 mr07">
			Download File <span>&#187;</span>
		</a>
	<?php } ?>
	</div>
	<div class="training-space-icon wp03 ml05 h24 mt05 fl"><img src="<?php echo $baseUrl; ?>/public/images/u168.png" /></div>
	<div class="dashboard-training-space mt06 fs13 ml06 fta fc03 b wp40 fl"><?php if (count($assementdetails) > 0) {
	echo $assementdetails['assessment_desc'];
	}?></div>


	<div class="dashboard-right-part wp100 mt10 fl pl10 pr10">

	<input type="hidden" name="submitdata" id="submitdata" value="1"/>
	<div class=" wp100 fl">
	<?php 
	$queststr="";
	$questionidstr="";
	$multipleselstr="";
	$mulstr="";
	$useranswerArr=array();
	$useranswer="";
			
	if (count($assementdetails) > 0) {
	$x=1;

	foreach($assementdetails['questionarray'] as $val) {
	$quesname = $val['question'];
	$questionid = $val['question_id'];
	$quesno1 = $val['ques_no'];
	$userans	=	$val['userans'];
	$ansoptions = $val['ansoptions'];
	$useranswerArr=array();
	$useranswer="";
	if(count($userans)>0){
		 if($val['question_type_id']==6)
		{
			foreach($userans as $userans){		
			array_push($useranswerArr,trim($userans['answer']));
			}
		}else{
			if($userans[0]['answer']!="")
			{
				$useranswer=$userans[0]['answer'];
			}
			else{
				$useranswer='';
			}	
		}
		}
		?>
	<div class="fl wp90" id="fullanswer_<?php echo $questionid; ?>" >
	<div class=" wp75  fs18 b fta   fl">
	<div class="dashboard-training-space mt10 fs13  fta fc02 b w25 fl"><?php echo $x; ?>:</div>
	<div class="dashboard-training-space mt10 fs13  fta fc02 b wp70 fl" ><?php echo stripcslashes($quesname); ?></div>
	</div>
		<?php
		$quesno = $val['ques_no'];
		$quesname = $val['question'];
		$answer = $val['answer'];
		$descriptive = $val['descriptive'];
		$questypeid = $val['question_type_id'];
		$questionid = $val['question_id'];
		$randomanswers=$val['random_answers'];
		?>
		<input type="hidden" id="questionid" name="questionid" value="<?php echo $questionid ?>"/>
										
		<?php
		if ($questypeid == 3) {?>
			<div id="choosetruefalse_'<?php echo $questionid;?>'" style="display:show; margin-left: 25px;border:0px solid #f00;">
					
				<div class="wp98  ml  bgc01  fl mb10" >
					<div class="wp50 fs13  ml06  bgc01  fl" style="display:none">
						<div class="fl mt10 "><?php echo $descriptive;?></div>
					</div>			
					<div class="mt12 fl fta fs12 fc02" style="border:0px solid #f00;">
						<input  type="radio"  name="trueval_<?php echo $questionid;?>" id="trueval_<?php echo $questionid;?>" value="truevalue"/>&nbsp;&nbsp; True&nbsp;&nbsp;&nbsp;
						<input   type="radio" name="trueval_<?php echo $questionid;?>" id="trueval_<?php echo $questionid;?>" value="falsevalue"/>&nbsp;&nbsp; False</div>		
				</div>
			</div>
		<?php } ?>
		<?php 
		if ($questypeid == 4) {
			?>
			<div id="chooseval" style="display:show; margin-left: 25px;border:0px solid #f00;">
					
				<div class="wp98  ml  bgc01  fl mb10" style="border:0px solid #ff0;">
					<div class="wp50 fs13  ml06  bgc01  fl" style="display:none;">
						<div class="fl mt10 "><?php echo $descriptive;?></div>
					</div>											
					<div class=" mt12 fl fta fs12 fc02"style="border:0px solid #000;">	
						<input  type="radio"  name="yesradio1_<?php echo $questionid;?>" id="yesradio1_<?php echo $questionid;?>" value="yes"/>&nbsp;&nbsp; Yes&nbsp;&nbsp;&nbsp;
						<input   type="radio" name="yesradio1_<?php echo $questionid;?>" id="yesradio1_<?php echo $questionid;?>" value="no"/>&nbsp;&nbsp; No</div>
				</div>
			</div>
			
		<?php } ?>
			<?php if ($questypeid == 5) {
				$submittextdata=0;?>
				<div id="describe" style="display:show; border:0px solid #f00;">
					<?php 
					$msrgin="";
					if($assementdetails['score_type_id']!=4){
						if($assementdetails['iscompleted']==1)
						{
							$msrgin="ml20";
						}
					}	?>
					<div class=" wp60 fl mb03 mt10 <?php echo $msrgin;?>" style="float:left;border:0px #000 solid; margin-left: 25px;">
						<textarea  class="form-control" onKeyPress="return charLimit(this)" onKeyUp="return characterCount(this)" rows="4" cols="50" name="descriptive_<?php echo $questionid;?>" id="descriptive_<?php echo $questionid;?>"  ></textarea>
						<p class="fr mt05" style="background-color: #EDEDED; border: 1px solid #CCCCCC; padding: 1px 5px; font-size: 13px; margin-right: -3px;"><span id="charCount_<?php echo $questionid;?>">500</span></p>
					</div>
				</div>
			<?php } ?>
<?php if ($questypeid == 1) { ?>
	<div class="" id="dataofmultichoicediv_<?php echo $questionid; ?>" >
		<?php
		
		$answerarray=array();
		
			foreach($ansoptions as $val){
																		
				$answerarray[]=$val['answer_id']; 
			}
			
	foreach ($ansoptions as $val) {
			
				$description=$val['description'];
				 $answerid=$val['answer_id'];
				$iscorrect=$val['is_correct'];													
				$check = '';
				if($iscorrect==1){
					$check = 'checked';
				}?>
				<div class="fl wp100 ml25"style="border:0px solid #f00;">
				
					<div class="wp03 mt15 fl fta fs12 fc02" style="border:0px solid #f00;">
					
					<input type="radio" name="multichoicecorrectineditradio_<?php echo $questionid;?>" id="multichoicecorrectineditradio_<?php echo $questionid;?>" value="<?php echo $questionid."||".$answerid;?>"/>

				</div>
					<div class=" wp90 fl mt20  fta fs12 fc02 ">
						<?php echo $description; ?>
						<input readonly="readonly" type="hidden" name="multichoicecorrectinedittext_<?php echo $questionid;?>_<?php echo $answerid;?>" id="" class="ans_size" value="<?php echo trim($description,'"'); ?>"/></div>
						
					</div>
		<?php }
		 ?>
			</div>
	<?php } ?>
	<?php if ($questypeid == 2) 
	{?>
		<div id="dataofmultiselectdiv_<?php echo $questionid;?>" >
		<?php
		
		$m=0;
		
		foreach($ansoptions as $fullvalues) 
		{
			 $answerid=trim($fullvalues['answer_id']);
			$optiontext=$fullvalues['description'];
			 $correctvalue=$fullvalues['is_correct'];
			$checkboxval='';
			$checked='';
			if($correctvalue==1){
				$checkboxval=$correctvalue;
				$checked='checked=checked';
			}
			?>
			<div class="fl wp100 ml25"style="border:0px solid #f00;">		
				<div class="wp03 mt15 fl fta fs12 fc02"><input type="checkbox"  name="mseloptinedit_<?php echo $questionid. '_' . $answerid;?>" id="mseloptinedit_<?php echo $questionid. '_' . $answerid;?>" value="<?php echo  $questionid .'||'. $answerid;?>"/></div>
				<div class="wp90 fl mt18  fta fs12 fc02">
					<div class=" wp100 fl mb03"><?php echo $optiontext;?></div>
				</div>
			</div>
		<?php 
			if($m==0)
			{
				$mulstr= $answerid;
				$m++;
			}else{
				$mulstr.= "_".$answerid;
			}
		}?>
	</div>
	<?php }?>
		<?php 
		$queststr.=$questypeid.",";
		$questionidstr.=$questionid.",";
		if($mulstr!="")
		{
			$multipleselstr.=$mulstr.",";
			$mulstr="";
		}
		?>
	<?php	$x++; ?>
	<?php  ?>	
	</div>
	<?php }} ?>
	</div>
	<?php 
	if($close_time >= $current_time){?>
	<div class="wp90 mb15 fs13 fta bgc01 fc03  fl">
	<input type="hidden" name="confirmedradio" id="confirmedradio"  value="yes"/>
	<input type="hidden" name="questiontypeansids" id="questiontypeansids" value="<?php echo $questionidstr;?>">
	<input type="hidden" name="questiontypeans" id="questiontypeans" value="<?php echo $queststr;?>">
	<input type="hidden" name="multiquestionansv" id="multiquestionansv" value="<?php echo $multipleselstr;?>">
	<input type="hidden" name="toalquestionasked" id="toalquestionasked" value="<?php echo $x-1;?>">
	<div class=" wp98  ml07 mt10 mt25 mb20 fl">
	<div class=" wp40 fl hand ">
	<div class=" wp40 fl hand ">
		<?php   
		
		if($close_time >= $current_time and $mode!='learningtab'){
		
			if(@$assementdetails['nooftimes']<1 and $assementdetails['iscompleted'] !=1 and $mode!='learningtab'){?>
				<div onclick="submitbyuserassessment_new()" class="new_css_button">Submit <span>&#187;</span></div>
				
			<?php }
			if($assementdetails['iscompleted']==1 and $mode!='learningtab') { ?>
			<div onclick="submitbyuserassessment_new()" class="new_css_button">Submit <span>&#187;</span></div>
				
		<?php } ?>
		<?php }?>
		<?php if($close_time < $current_time and $mode!='learningtab') { ?>
			<div onclick="expiredassessment('<?php echo $duedate;?>')" class="new_css_button">Submit <span>&#187;</span></div>
			
		<?php } ?>
	</div>
	</div>
	</div>
	<?php }?>
	</div>
		<div class="ext-box mr18 h50 mt15 w650 fr"></div>
	</div>
	</div>
	</div>
</div></div>
</div>
</form>
<script>
function submitbyuserassessment_new()
{

	/* var e = event.keyCode;
	if(e == 9)
	{
		return false;
	} */
	var linkertvalueinedit=0;
	var trueval=0;
	var yesradio1=0
	var descriptive=0;
	var multichoicecorrectineditradio=0;
	var mseloptinedit=0;
	var totalquestion=document.getElementById("toalquestionasked").value;
	var questiontypeans=document.getElementById("questiontypeans").value;
	var questiontypeansids=document.getElementById("questiontypeansids").value;
	var multiquestionansvids=document.getElementById("multiquestionansv").value;
	
	var splitquestiontypes=questiontypeans.split(",");
	var splitquestiontypesids=questiontypeansids.split(",");
	var mulidsdis=multiquestionansvids.split(",");
	var mulop=0;
	var startcount=0;
	var loopstr=splitquestiontypes.length-1;
	for(qt=0;qt<=loopstr;qt++)
	{
		if(splitquestiontypes[qt]!="")
		{
			if(splitquestiontypes[qt]==1)
			{
				var fieldname="multichoicecorrectineditradio_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
		
			if(splitquestiontypes[qt]==3)
			{
				var fieldname="trueval_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==4)
			{
				var fieldname="yesradio1_"+splitquestiontypesids[qt];
				var group = document.getElementsByName(fieldname);
				for (var i=0; i<group.length; i++) 
				{
					if (group[i].checked)
					{
						startcount++;
						break;
					}
				}
				
			}
			if(splitquestiontypes[qt]==5)
			{
				var fieldname="descriptive_"+splitquestiontypesids[qt];
				//var group = document.getElementsByName(fieldname);
				if(document.getElementById(fieldname).value!="")
				{
					startcount++;
					
					//break;
				}
				
			}
			if(splitquestiontypes[qt]==2)
			{
				var totalmultpleoptionid=mulidsdis[mulop];
				var splitoptionmul=totalmultpleoptionid.split("_");
				var mulsel=0;
				for(mt=0;mt<splitoptionmul.length;mt++)
				{
					var fieldname="mseloptinedit_"+splitquestiontypesids[qt]+"_"+splitoptionmul[mt];
					var group = document.getElementsByName(fieldname);
					for (var i=0; i<group.length; i++) 
					{
						if (group[i].checked)
						{
							mulsel++;
							break;
						}
					}
					if(mulsel!=0)
					{
						startcount++;
						break;
					}
				}
				mulop++;
			}
		}
	}
	
	var multiSelArr = new Array();
	$('input[type="checkbox"]:checked').each(function() {
	   multiSelArr.push(this.value);
	});
	
	
	var multiSelJsonString = JSON.stringify(multiSelArr);
	$("#multiSelJsonString").val(multiSelJsonString);
	

	var baseurl=document.f1.baseurl.value;
	//var userid=document.f1.userid.value;
	var assessid=document.f1.assessid.value;
    var istimer = $('#istimer').val(); 
	var confirmedradio=document.getElementById('confirmedradio');
	var choosen=1;

	if(assessid!='')
	{
		if(istimer){
		var scoreid=document.getElementById('score_id').value;
		if((startcount!= 0) && (totalquestion!=0))
		{
			if(startcount>0) {
				alertify.confirm("You have not responded to all questions. Are you sure you want to submit your results?", function (e) {
					if (e) {
						  	document.f1.submit();
						/* $.post("<?php echo $baseUrl;?>/elearningmedia/checkloginstatus/reqdata/<?php echo $datamodel; ?>",function(data){
							if(data.status=="success"){
								document.f1.submit();
							}else{
								alert("Your session has been expired..");
								return false;
							}
						},"json"); */
					}
				});
			} else {
				alertify.confirm("You have not responded to all questions, Please select the answer.", function (e) {
					return false;
				});
			}
		}else{
			
			//alertify.alert("You have not responded to any questions, Please select the answer.");
		}
		}else{
				document.f1.submit();
			
		}
		/* else{
			$.post("<?php echo $baseUrl;?>/elearningmedia/checkloginstatus/reqdata/<?php echo $datamodel; ?>",function(data){
				if(data.status=="success"){
					document.f1.submit();
				}else{
					alert("Your session has been expired..");
					return false;
				}
			},"json");
		} */
	}else{
		return false;
	}
}
</script>
<script>
	MyTimer();
	setInterval( "MyTimer()" , 1000);
	function MyTimer()
	{
	   var valueTimer = $('#hdnTimer').val(); 
	   if(valueTimer > 0)
	   {
		   valueTimer = valueTimer - 1; 
		   hours = (valueTimer/3600).toString().split('.')[0];
		   mins  = ((valueTimer % 3600) / 60).toString().split('.')[0]; 
		   secs  = ((valueTimer % 3600) % 60).toString(); 
		   
		   if(hours.length == 1) hours = '0' + hours; 
		   if(mins.length  == 1) mins  = '0' + mins; 
		   if(secs.length  == 1) secs  = '0' + secs; 
		   $('#timer').text( hours + ':' +  mins + ':'  + secs);
		   $('#hdnTimer').val( valueTimer );
		   if(valueTimer==0){
			  submitbyuserassessment_new();
		   }
	   }
	}
</script>