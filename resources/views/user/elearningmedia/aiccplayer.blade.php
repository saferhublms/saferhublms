<?php

if($redirecturl=='inprogress'){
$redirect_url = $baseUrl.'/mylearning/inprogress';
}else{
$redirect_url = $baseUrl.'/#/elearningmedia/training/viewId/'.$viewId.(!empty($itemTypeId) ? '/itemTypeId/'.$itemTypeId:'').(!empty($categoryTypeID) ? '/categoryTypeID/'.$categoryTypeID:''); 
}
$datamodel_arr = array("id"=>$item_id, "user_id"=>$user_id, "company_id"=>$company_id, "attempt"=>$attempt, "file_id"=>$file_id, "user_training_id"=>$user_training_id);

$datamodel = CommonHelper::getEncode(json_encode($datamodel_arr));
header('Content-Type: text/html; charset=UTF-8'); 
?>
<html>
<?php echo csrf_field(); ?>
<head>
<title>Torch LMS eLearning Player</title>
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta name="viewport" type="text/css" content="width=device-width">
	
	<link href="/administrator/assets/css/inv-style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo $baseUrl;?>/runplayer/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="/administrator/assets/js/request.js"></script>
	<script>
		var cnt= 0;
		var win_obj = null;
		var errorCode = "0";
		var interval = null;
		var baseURL = "<?php echo $baseUrl;?>";
		var nWidth = screen.availWidth - 10;
		var nHeight = screen.availHeight - 10;
		// if (nWidth > 820) {
			// nWidth = 1080;
			// nHeight = 740;
		// }
		
		function LaunchScorm(){
			var strOptions = "width=" + nWidth +",height=" + nHeight+ ",toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=yes";
			openDialog("<?php echo $baseUrl;?>/elearningmedia/loadaicc/param/<?php echo $loadsco ?>", "TorchLMS_SCORM_Player", strOptions, closeCallback);
		}
		
		function initScormPopup(){
			try {
				if (win_obj == null || win_obj.closed) {
					LaunchScorm();
				}
			}catch(e){}
		}
		
		var openDialog = function(uri, name, options, closeCallback) {

			window.document.getElementById('PopupBlockedMessage').style.visibility = 'hidden';
			window.document.getElementById('ScoMessage').style.visibility = 'visible';
			try{ 
				win_obj = window.open(uri, name, options);
				win_obj.blur();
				interval = window.setInterval(function() {
					try {
						if (win_obj == null || win_obj.closed) {
							window.clearInterval(interval);
							closeCallback();
						}
					}catch(e){}
				}, 1000);
			} 
			catch(e){ 
				$('body').css('z-index', '0'); 
				window.document.getElementById('PopupBlockedMessage').style.visibility = 'visible';
				window.document.getElementById('ScoMessage').style.visibility = 'hidden';
			}
			
			return win_obj;
		};

		
		function closeCallback(){
			    var tokenuse=$("input[name=_token]").val();
			<?php if($preview!=1){ ?>
			$.post("<?php echo $baseUrl;?>/elearningmedia/checkloginstatus/reqdata/<?php echo $datamodel; ?>",{_token:tokenuse},function(data){
				if(data.status=="success"){
				   if('<?php echo $redirecturl;?>'=='inprogress'){
				   
				    window.location = "<?php echo $redirect_url; ?>/scormparam/<?php echo $scormparam;?>";
				   }else{
					window.location = "<?php echo $redirect_url; ?>/scormparam/<?php echo $scormparam;?>";
					}
				}else{
					alert("Your session has been expired..");
				}
			},"json");
			<?php }else{ ?>
			window.location = "<?php echo $redirect_url; ?>/tview/view";
			<?php } ?>
		}
		
		function underscore(str) {
			str = String(str).replace(/.N/g,".");
			return str.replace(/\./g,"__");
		}
		
		function viewpopup(){
			win_obj.focus();
		}
		
	<?php
	/* 
		if (file_exists($scorm_url.$this->scorm_version.'.js.php')) {
			include_once($scorm_url.$this->scorm_version.'.js.php');
		} else {
			include_once($scorm_url.'scorm_12.js.php');
		} */
		
	?>
	</script>
	
</head>
<body  onload="LaunchScorm();" >

<div id="PopupBlockedMessage" style="visibility: visible;" align="center">
	<h3>Popup Blocked</h3>
	<p>We attempted to launch your course in a new window, but a pop-up blocker is preventing it from opening. You may either disable pop-up blockers for this site or go directly to the course by clicking "Launch Course" below.</p>
	<p>
		<a href="javascript:void(0);" onclick="LaunchScorm();" class="change_col button_lblue_r4">Launch Course<span>&#187;</span></a>
<!-- 		<a href="javascript:window.history.back();" class="change_col button_lblue_r4">Back<span>&#187;</span></a>  --->
	</p>	
</div>
<div id="ScoMessage" style="visibility: hidden;" align="center">
	<h3>Your course has been launched in a new window.</h3>
	<p><a href="javascript:void(0);" onclick="viewpopup();" class="change_col button_lblue_r4">View Course<span>&#187;</span></a></p>
</div>
</body>
</html>
