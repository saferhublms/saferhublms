<?php 
$baseUrl ='';

$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "https"; 

?>
<style type='text/css'>
.TB_overlayBG{
    background-color:#000;
	filter:alpha(opacity=88);
	-moz-opacity: 0.88;
	opacity: 0.88;
	}
</style>
<script type="text/javascript" >
function is_iPad(){
if ((navigator.userAgent.indexOf('iPhone') != -1) || (navigator.userAgent.indexOf('iPod') != -1) || (navigator.userAgent.indexOf('iPad') != -1)) {
  var myIframe = document.getElementById('pdfframe');
  myIframe.src = '';
  myIframe.srcdoc  = '<h2>Please read the PDF opened in the next tab of your browser.  When complete, return to this window and click the “Submit Progress” button below.</h2>';

  win='';
  var url = "<?php echo $protocol;?>://<?php echo $_SERVER['HTTP_HOST'].$filesname;?>";
  win = window.open(url, '_blank');
  
  win.focus();
  }
}
function filesopen(file)
{
	document.getElementById('attachmentid').innerHTML='<div >'+"<img src='<?php echo $baseUrl;?>/public/images/drop_arrow_pop.png' onclick=window.open('<?php echo $baseUrl ?>/public/"+file+"')></div>";
}
function pageredirect(pagelink)
{
    window.location.href='<?php echo $baseUrl;?>/contentmanagement/viewpages/pageid/'+pagelink;
}
</script>
<?php
$jwplayer_supported_formats = array('m4v', 'f4v', 'ogv', 'm4a', 'f4a', 'ogg','OGG', 'oga', 'mp3','mp4', 'flv', 'FLV' );
$jwplayer1_supported_formats = array('wmv','WMV','wma','WMA' );
$swf_supported_formats = array('swf','SWF' );
$flowplayer_supported_formats = array('mp41','flv1');
$quicktime_supported_formats = array('mov','avi','mpg','mpeg','aac');
$divx_supported_formats = array('mkv');
$googledoc_supported_formats = array('pages','ai','psd','tiff','dxf','svg','eps','ps','ttf','xps','zip','rar','PAGES','AI','PSD','TIFF','DXF','SVG','EPS','PS','TTF','ZPS','ZIP','RAR');
$office_supported_formats = array('doc','docx','xls','xlsx','ppt','pptx','DOC','DOCX','XLS','XLSX','PPT','PPTX','ocx');
$pdfiframe = array('pdf','PDF');
$embedded_formats='';
if($embedded_code){
$embedded_formats = 'embedded';
}
?>


    
<div class="user-dashboard-content fl ml10" style="width: 95%; height: 90%;">
	<div id="loader" style="position:absolute; top:200px; left:300px;"><img src="<?php echo $baseUrl;?>/public/images/loadingAnimation.gif" /></div>
	<div style="width: 100%;height:100%;text-align: center;">
	
	  
	   
		<?php if(isset($filepath)):  ?>
			<?php if(in_array($filepath, $jwplayer_supported_formats)): ?>
				<div id="jwplayer"></div>
        		    <script type="text/javascript">
					function jwp()
					{
						$('#loader').hide();
						jwplayer("jwplayer").setup({ 
							flashplayer:"/runplayer/jwplayer/player.swf", 
							file: "<?php echo $filesname;?>",
							autostart:true,
							height: '100%', 
							width: '100%',
							events: {
								onComplete: function() 
								{
								},
								onReady: function() 
								{ 
								}
							}
						}); 
					}
					$(document).ready
					(
						function()
						{
							window.setTimeout('jwp()', 1000); 
						}
					);
					</script>					
					<?php elseif(in_array($filepath, $jwplayer1_supported_formats)): ?>

		<iframe src="elearningmedia/iframe-media/compid/<?php echo $compid;?>/filesname/<?php echo $namefile;?>/ext/<?php echo  $filepath ?>" style="width:100%; height:100%;" frameborder="0"></iframe>
    	
		<?php elseif(in_array($filepath, $swf_supported_formats)): ?>
    	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.adobe.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="700" height="380" align="middle"> 
		<param name="movie" value="<?php echo $this->filesname;?>"> 
		<param name="allowScriptAccess" value="always"> 
		<embed type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" width="100%" height="100%" align="middle" src="<?php echo $this->filesname;?>" allowScriptAccess="always"></embed>
		</object>
		<script language="JavaScript">
		window.setTimeout('fp()', 700);
		function fp()
		{
			$('#loader').hide();
		}
        </script>
		<?php elseif(in_array($filepath, $flowplayer_supported_formats)): ?>
			<a href="<?php echo $this->filesname;?>" style="display:block;width:100%;height:100%;" id="player"></a>
			<script language="JavaScript">
			window.setTimeout('fp()', 700);
			function fp()
			{
				$('#loader').hide();
				flowplayer("player", "<?php echo $baseUrl;?>/public/js/player/flowplayer-3.2.7.swf");
			}
			</script> 
		<?php elseif(in_array($filepath, $quicktime_supported_formats)): ?>
            <OBJECT classid='clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B' width="100%" height="100%" codebase='http://www.apple.com/qtactivex/qtplugin.cab'>
            <param name='src' value="<?php echo $this->filesname;?>">
            <param name='autoplay' value="true">
            <param name='controller' value="true">
            <param name='loop' value="false">
            <EMBED src="<?php echo $this->filesname;?>" width="100%" height="100%" autoplay="true"
            controller="true" loop="true" scale="tofit" bgcolor="#000000" pluginspage='http://www.apple.com/quicktime/download/'>
            </EMBED>
            </OBJECT>
           <script language="JavaScript">
				window.setTimeout('fp()', 400);
				function fp()
				{
					$('#loader').hide();
				}
			</script>
		<?php elseif(in_array($filepath,$googledoc_supported_formats)): ?>
			<iframe src="<?php echo $protocol;?>://docs.google.com/gview?url=<?php echo $protocol;?>://<?php echo $_SERVER['HTTP_HOST'].$this->filesname;?>&embedded=true" style="width:100%; height:100%;" frameborder="0"></iframe>
			<script language="JavaScript">
			window.setTimeout('fp()', 400);
			function fp()
			{
				$('#loader').hide();
			}
			</script>
		<?php elseif(in_array($filepath,$googledoc_supported_formats)): ?>
			<iframe src="<?php echo $protocol;?>://docs.google.com/gview?url=<?php echo $protocol;?>://lms.torchlms.com<?php echo $this->filesname;?>&embedded=true" style="width:100%; height:100%;" frameborder="0"></iframe>
			<script language="JavaScript">
			window.setTimeout('fp()', 400);
			function fp()
			{
				$('#loader').hide();
			}
			</script>
		<?php elseif(in_array($filepath,$office_supported_formats)): ?>
			<iframe src="<?php echo $protocol;?>://view.officeapps.live.com/op/view.aspx?src=<?php echo $filesname;?>" style="width:100%; height:100%;" frameborder="0"></iframe>
			<script language="JavaScript">
			window.setTimeout('fp()', 400);
			function fp()
			{
				$('#loader').hide();
			}
			</script>
		<?php elseif(in_array($filepath,$pdfiframe)): ?>
			<script>is_iPad();</script>
			<iframe id="pdfframe" src="<?php echo $filesname;?>" style="width:100%; height:100%;" frameborder="0">For iPad users PDF is opened in next tab. Please read the PDF then in this page click on Submit Progress button</iframe>
			<script language="JavaScript">
			window.setTimeout('fp()', 400);
			function fp()
			{
				$('#loader').hide();
			}
			</script>
			
	    <?php elseif(in_array($filepath, $divx_supported_formats)): ?>
			<div style="margin-top: 10px;margin-bottom: 10px;">
				<object classid="clsid:67DABFBF-D0AB-41fa-9C46-CC0F21721616" width="100%" height="100%" codebase="http://go.divx.com/plugin/DivXBrowserPlugin.cab">
				<param name="custommode" value="none" />
				<param name="previewImage" value="image.png" />
				<param name="autoPlay" value="false" />
				<param name="src" value="<?php echo $filesname;?>" />
				<embed type="video/divx" src="<?php echo $filesname;?>" custommode="none" width="100%" height="100%" autoPlay="false"  previewImage="image.png">
				</embed>
				</object>
			</div>
			<script language="JavaScript">
			window.setTimeout('closeloading()', 400);
			function closeloading()
			{
				$('#loader').hide();
			}
			</script>     
		<?php else: ?>
		<?php if(isset($embedded_code)){  ?>
	   <div id="myplayer"><?php echo html_entity_decode($embedded_code);?></div>
	   <?php }else{ ?>
		<div id="fileNotSupported" style="position:absolute; top:200px; left:300px; font-weight:bold">Video Format is not supported</div>
			<script language="JavaScript">
			window.setTimeout('closeloading()', 100);
			function closeloading()
			{
				$('#loader').hide();
				$('#fileNotSupported').show();
			}
			</script> 
	   <?php } ?>
		<?php endif; ?>
	<?php else: ?>
	<?php endif; ?>
	</div>
</div>
<div class="fl mt20 ml10" style="width:100%;text-align: center;">
	<?php if($view!="view"){ ?>
	<a class="thickbox button_lblue mr08 mb05" style="outline:none;border-radius:4px;" href="/elearningmedia/confirmation/viewId/<?php echo $viewId; ?>?height=380&width=850&inlineId=hiddenModalContent&modal=true" >Submit Progress</a>
	
	<a class="button_lblue mr08 mb05" style="outline:none;border-radius:4px;" onclick = "page_raload();">Cancel</a>
	<?php }else{ ?>
	<a class="button_lblue mr08 mb05" style="outline:none;border-radius:4px;" onclick = "javascript:tb_remove();;">Cancel</a>
	<?php } ?>
</div>

<div>
 	<?php
	/* $userprogramtypeid =  $this->userprogramtypeid;
	$trainingprogram_id =  $this->trainingprogram_id;
	$usertrainingid = $this->usertrainingid;
	$item_id = $this->item_id;
	if($this->blendedprogid!='')
	{
		$blendedprogid = $this->blendedprogid;
	}
	$launchtime = $this->launchtime; */
	?>
</div>