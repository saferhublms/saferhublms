<?php 
/* $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
$cf = new Tourchhelper_CommonFunctions();
if($this->blendedprogid!='' || $this->blendedprogid !=0){
	$blendedid = $cf->getDecode($this->blendedprogid);
}else{
	$blendedid=0;
} */
$baseUrl='eliteprodlms';
?>

<input type="hidden" name="star2" id="star2" value="0">
<div id="confirm">
    <div class="tac wp100 fta fs14 fc34 b mt10" style="margin: 150px 0 10px 0;">
	<?php if($compid==34){?>
		<center>Have you completed review of this notification?</center>
	<?php } else { ?>
		<center>Have you completed this training course?</center>
	<?php }?>
    </div>
    <div style="margin-left: 313px;">
	
		<div class="fl ml45">
			<div class="new_css_button w80 ml20" onclick="yes('2','<?php echo $viewId;?>','<?php echo $feedback;?>')">Yes</div>
		</div>

   
		<div class="fl">
		   <div class="new_css_button w80" onclick="no();">No</div>
		</div>
	
  </div>
 </div>

<div style="display: none;" id="yes">
	<?php if($compid!=34){?>
        <div class="fl w850 ml05 mt10 fta fc34 fs16 b"><center>You have completed this training</center></div>
        <div class="fl w850 ml05 mt10 fta fc34 fs12">
        <center>Please rate the quality and effectiveness of this training by clicking the stars below.</center>
        </div>

        <div class="fl pop_star w900" style="margin-left:370px">

        <span class="fl mt12 ml20">
          <div id="rate_1" class="fl"><img src="<?php echo $baseUrl; ?>/public/images/star_no.png" onclick="rating(1);"/></div>
          <div id="rate_2" class="fl"><img src="<?php echo $baseUrl; ?>/public/images/star_no.png" onclick="rating(2);"/></div>
          <div id="rate_3" class="fl"><img src="<?php echo $baseUrl; ?>/public/images/star_no.png" onclick="rating(3);"/></div>
          <div id="rate_4" class="fl"><img src="<?php echo $baseUrl; ?>/public/images/star_no.png" onclick="rating(4);"/></div>
          <div id="rate_5" class="fl"><img src="<?php echo $baseUrl; ?>/public/images/star_no.png" onclick="rating(5);"/></div>
        </span>

        </div>

        <div class="w850 fl mb10">
        <div class="ml80 mt10 fl fta fs12 fc18 b w700"><center>1-star equals "poor" - 5-star equals "excellent"</center></div>
        </div>
	<?php }?>	
	
	<?php if($compid==34){?>
		<div style="margin-top:100px;"></div>
	<?php }?>
        <div class="fl w100 mt10 fta fc34 fs16 b" style="margin-left:233px;">Comments</div>
        <div class="fl w300 mt15"><textarea style="border: 1px solid #ccc;" name="comment" id="comment" cols="" rows="" class="pop_txt_area_nob"></textarea></div>
        <div class="fl w500 mb20 mt20" style="margin-left:335px">
        
        <div class="new_css_button fl hand w100" style="z-index :999999999;" onclick="submitform('1','<?php echo $viewId;?>')">Submit</div>
        <div class="new_css_button fl hand w100" style="z-index :999999999;" onclick="submitform('0','<?php echo $viewId;?>')">Close</div>
        </div>
</div>

<div style="display: none;" id="no">
    
	<div class="fl w200 fta fs14 fc34 b mt10 mb15">
		<div class="fl w20 w60 ml45" style="margin: 130px 0px 0px 350px;">
			<div style="width: 200px;">
			<select style="width:100px; border: 1px solid #ccc; padding: 4px;" name="percentage" id="percentage">
				<option value="5">5</option>
				<option value="10">10</option>
				<option value="15">15</option>
				<option value="20">20</option>
				<option value="25">25</option>
				<option value="30">30</option>
				<option value="35">35</option>
				<option value="40">40</option>
				<option value="45">45</option>
				<option value="50">50</option>
				<option value="55">55</option>
				<option value="60">60</option>
				<option value="65">65</option>
				<option value="70">70</option>
				<option value="75">75</option>
				<option value="80">80</option>
				<option value="85">85</option>
				<option value="90">90</option>
				<option value="95">95</option>
			</select> % completed
			</div>
		</div>
    </div>

    <div class="hand fl ml70 w100 new_css_button" onclick="return getpercentage('<?php echo $viewId;?>');" style="margin: 180px 0 0 150px;z-index :999999999;">Submit</div>

</div>