<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
  <head>
    <!-- Meta-Information -->
    <title>Saferhub</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Saferhub">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo csrf_field(); ?>

    <style>
      .mapmarker{
        background-color: #fff;
        width: 300px;
        height: 110px;
      }
      .mapmarker .col-sm-5{
        margin-left:-15px;
      }
      h2.link{
        border-bottom:1px solid #00ADC4;
      }
      h2.link a:hover{
        text-decoration:none;
      }
      .navbar .container .nav > li.nav-toggle {
        display: none !important;
      }
      .main-header .header.navbar .toggle-sidebar.bs-tooltip img {
        display: none;
      }
      .navbar .toggle-sidebar:hover {
        background: none !important;
      }
      .nav.navbar-nav.first_nav{
        display: none;
      }
    </style>
  </head>
  <body ng-app="eLightApp">
    <div ng-show="notShowHeader!=1" ng-include='"/user/templates/include/app_header.html"'></div>
    <div ui-view="mainView"></div>
    <script type="text/javascript">
    // dynamically add base tag as well as css and javascript files.
    // we can't add css/js the usual way, because some browsers (FF) eagerly prefetch resources
    // before the base attribute is added, causing 404 and terribly slow loading of the docs app.
    (function() {
      var indexFile = (location.pathname.match(/\/(index[^\.]*\.html)/) || ['', ''])[1],
          rUrl = /(#!\/|api|guide|misc|tutorial|error|index[^\.]*\.html).*$/,
          baseUrl = location.href.replace(rUrl, indexFile),
          production = location.hostname === '127.0.0.1',
          headEl = document.getElementsByTagName('head')[0],
          sync = true;
		  var url=baseUrl.split('#')

      addTag('base', {href: baseUrl});
      addTag('link', {rel: 'stylesheet', href: '/user/assets/font_icons/css/font-awesome.min.css', type: 'text/css'});
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/bootstrap.css', type: 'text/css'});
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/responsive.css', type: 'text/css'});
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/media.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/angular-material.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/reset.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/fonts.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/iconmoon.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/animate.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/custom.css', type: 'text/css', media: 'all'});  
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/inv-style.css', type: 'text/css', media: 'all'});  
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/main.css', type: 'text/css', media: 'all'});  
      addTag('link', {rel: 'stylesheet', href: '/user/assets/bower_components/angular-ui-grid/ui-grid.min.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/bower_components/angular-ui-notification/dist/angular-ui-notification.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/bower_components/ng-alertify/dist/ng-alertify.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/jquerydropdown/flexcrollstyles.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/jquerydropdown/selectbox.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/jquery-ui-1.8.4.custom.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/bootstrap.min.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/stylecrm.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/button_company_style.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/jquery.dataTable.min.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/user/assets/css/angular-datatables.min.css', type: 'text/css', media: 'all'}); 

      addTag('link', {rel: 'stylesheet', href: '/runplayer/thickbox.css', media: 'all'}); 
      addTag('script', {src: '/user/assets/bower_components/jquery.min.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/jquery-ui.js' }, sync);
      addTag('script', {src: '/runplayer/thickbox.js' }, sync);

      addTag('script', {src: '/user/assets/js/matchHeight-min.js' }, sync);
      addTag('script', {src: '/user/assets/js/bxslider.min.js' }, sync);
      addTag('script', {src: '/user/assets/js/waypoints.min.js' }, sync);
      addTag('script', {src: '/user/assets/js/counterup.min.js' }, sync);

      //it used in elearnig and assessment quiz player
      //
      addTag('script', {src: '/runplayer/jquery-migrate-1.2.1.min.js' }, sync);
      addTag('script', {src: '/runplayer/jquery-migrate-1.1.1.min.js' }, sync);
      addTag('script', {src: '/runplayer/jwplayer/jwplayer.js' }, sync);
      addTag('script', {src: '/runplayer/jwplayer/jquery.media.js' }, sync);
      addTag('script', {src: '/runplayer/jwplayer/silverlight.js' }, sync);
      addTag('script', {src: '/runplayer/jwplayer/wmvplayer.js' }, sync);
      addTag('script', {src: '/runplayer/player/example/flowplayer-3.2.6.min.js' }, sync);
      addTag('script', {src: '/runplayer/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js' }, sync);
      addTag('script', {src: '/runplayer/fancyapps/source/jquery.fancybox.js?v=2.1.5' }, sync);
      addTag('link'  , {rel: 'stylesheet', href: '/runplayer/fancyapps/source/jquery.fancybox.css?v=2.1.5', type: 'text/css'});
      //

      addTag('script', {src: '/user/assets/js/magnific-popup.min.js' }, sync);
      addTag('script', {src: '/user/assets/js/pdfobject_source.js' }, sync);
      addTag('script', {src: '/user/assets/js/moment.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-ui-router.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/bootstrap.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-material.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-animate.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-parallax.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/ui-bootstrap-tpls.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/ngstorage/ngstorage.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-ui-notification/dist/angular-ui-notification.min.js' }, sync);

      addTag('script', {src: '/user/assets/bower_components/ng-alertify/dist/ng-alertify.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-ui-grid/ui-grid.min.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-sanitize.js' }, sync);
      addTag('script', {src: '/administrator/assets/js/ocLazyLoad.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/jquery.dataTable.min.js' }, sync);
      addTag('script', {src: '/user/assets/bower_components/angular-datatables.min.js' }, sync);

      addTag('script', {src: '/user/app.js' }, sync);
      addTag('script', {src: '/user/controllers/loginhome.js' }, sync);
      addTag('script', {src: '/user/services/requirementservice.js' }, sync);
      addTag('script', {src: '/user/services/directive.js' }, sync);
      addTag('script', {src: '/user/services/inprogresssevice.js' }, sync);

      function addTag(name, attributes, sync) {
        var el = document.createElement(name),
            attrName;

        for (attrName in attributes) {
          el.setAttribute(attrName, attributes[attrName]);
        }

        sync ? document.write(outerHTML(el)) : headEl.appendChild(el);
      }

      function outerHTML(node){
        // if IE, Chrome take the internal method otherwise build one
        return node.outerHTML || (
            function(n){
                var div = document.createElement('div'), h;
                div.appendChild(n);
                h = div.innerHTML;
                div = null;
                return h;
            })(node);
      }
    })();
    
    </script>

  <div ng-show="notShowHeader!=1" ng-include='"/user/templates/include/footer.html"'></div>
</body>
</html>