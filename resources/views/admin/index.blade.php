<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<html class="no-js"> <!--<![endif]-->
  <head>

    <!-- Meta-Information -->
    <title>Saferhub</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Saferhub">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php  echo csrf_field(); ?>
    <style>
    .mapmarker{
      background-color: #fff;
      width: 300px;
      height: 110px;
    }
    .mapmarker .col-sm-5{
      margin-left:-15px;
    }
    h2.link{
      border-bottom:1px solid #00ADC4;
    }
    h2.link a:hover{
      text-decoration:none;
    }
    .navbar .container .nav > li.nav-toggle {
      display: none !important;
    }
    .main-header .header.navbar .toggle-sidebar.bs-tooltip img {
      display: none;
    }
    .navbar .toggle-sidebar:hover {
      background: none !important;
    }
    .nav.navbar-nav.first_nav{
      display: none;
    }
    </style>
  </head>
   <body ng-app="eLightApp" id="page-top">
   
   <div id="wrapper">

    <!-- Navigation -->
    <div   ng-include='"/administrator/templates/include/sidenav.html"'></div>

    <!-- Page wraper -->
    <!-- ng-class with current state name give you the ability to extended customization your view -->
    <div id="page-wrapper" class="dashboards gray-bg">

        <!-- Page wrapper -->
         <div ng-show="notShowHeader!=1" ng-include='"/administrator/templates/include/app_header.html"'></div>
   
		<div ui-view="mainView"></div>
		<div ng-show="notShowHeader!=1" ng-include='"/administrator/templates/include/footer.html"'></div>
		<div ng-include="'/administrator/templates/include/right_sidebar.html'"></div>
    </div>
     
</div> 
	 




	
   <script type="text/javascript">
    // dynamically add base tag as well as css and javascript files.
    // we can't add css/js the usual way, because some browsers (FF) eagerly prefetch resources
    // before the base attribute is added, causing 404 and terribly slow loading of the docs app.
    (function() {
      var indexFile = (location.pathname.match(/\/(index[^\.]*\.html)/) || ['', ''])[1],
	  rUrl = /(#!\/|api|guide|misc|tutorial|error|index[^\.]*\.html).*$/,
	  baseUrl = location.href.replace(rUrl, indexFile),
	  production = location.hostname === '127.0.0.1',
	  headEl = document.getElementsByTagName('head')[0],
	  sync = true;
	  var url=baseUrl.split('#')
	  addTag('base', {href: baseUrl});
	  
	  
	  
      addTag('link', {rel: 'stylesheet', href: '/administrator/assets/images/favicon.ico', type: 'text/css'});
      /* addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/browser.css', type: 'text/css'});
   addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/inv-style.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/main.css', type: 'text/css'});  */
	   
	  /* addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/button_company_style.css', type: 'text/css', media: 'all'}); 
      addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/user_style_rounded.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/demo_table_jui.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/demo_page.css', type: 'text/css', media: 'all'});  */
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/jquery-ui-1.8.4.custom.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/jquery-ui.min.css', type: 'text/css', media: 'all'});  
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/bootstrap.min.css', type: 'text/css', media: 'all'});  
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/lobipanel.min.css', type: 'text/css', media: 'all'});  
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/flash.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/pe-icon-7-stroke.css', type: 'text/css', media: 'all'}); 

	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/themify-icons.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/icon-font.css', type: 'text/css', media: 'all'}); 
	addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/font-awesome/css/font-awesome.min.css', type: 'text/css', media: 'all'});
	 
	  /* addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/stylecrm.css', type: 'text/css', media: 'all'});   */
	 
	  addTag('link', {rel: 'stylesheet', href: '/user/assets/bower_components/angular-ui-grid/ui-grid.min.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/user/assets/bower_components/angular-ui-select/dist/select.min.css', type: 'text/css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/user/assets/css/angular-material.css', type: 'text/css', media: 'all'});
	  addTag('link', {rel: 'stylesheet', href: '/user/assets/css/mdPickers.min.css', type: 'text/css', media: 'all'});
	  
	  //addTag('link', {rel: 'stylesheet', href: '/user/assets/css/jquery.dataTable.min.css', type: 'text/css', media: 'all'}); 

	 
	  /* addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/style.css', type: 'text/css', media: 'all'});  */
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/ang-accordion.css', type: 'text/css', media: 'all'}); 
	 // addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/trix.css', type: 'text/css', media: 'all'}); 
	 // addTag('link', {rel: 'stylesheet', href: '/user/assets/css/angular-datatables.min.css', type: 'text/css', media: 'all'}); 
	  
	  addTag('link', {rel: 'stylesheet', href: '/runplayer/thickbox.css', media: 'all'}); 
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/awesome-bootstrap-checkbox.css', media: 'all'}); 
	  

	  
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/style1.css', type: 'text/css'});
	  addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/animate.css', type: 'text/css'});
	  
	  
	  addTag('script', {src: '/user/assets/bower_components/jquery.min.js' }, sync);
	  addTag('script', {src: '/user/assets/bower_components/jquery-ui.js' }, sync);
	  addTag('script', {src: '/runplayer/thickbox.js' }, sync);
	  
	  
	  addTag('script', {src: '/administrator/assets/js/plugins/jquery-3.0.0.js' }, sync);
	  addTag('script', {src: '/administrator/assets/js/plugins/jquery-1.12.4.min.js' }, sync);
	  addTag('script', {src: '/administrator/assets/js/plugins/jquery-ui.min.js' }, sync);
	  addTag('script', {src: '/administrator/assets/js/plugins/bootstrap.min.js' }, sync);
	  addTag('script', {src: '/administrator/assets/js/plugins/lobipanel.min.js' }, sync);
	  addTag('script', {src: '/administrator/assets/js/plugins/pace.min.js' }, sync);
	  addTag('script', {src: '/administrator/assets/js/plugins/jquery.slimscroll.min.js' }, sync);

	  addTag('script', {src: '/administrator/assets/js/plugins/fastclick.min.js' }, sync);
	 

	  
	   addTag('script', {src: '/administrator/assets/js/tinmca/jscripts/tiny_mce/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image' }, sync); 
	   
     addTag('script', {src: '/user/assets/js/moment.js' }, sync);
     addTag('script', {src: '/user/assets/bower_components/angular.js' }, sync);
     addTag('script', {src: '/administrator/assets/js/angular-ui-router.min.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/angular-material.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/angular-animate.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/angular-aria.min.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/mdPickers.min.js' }, sync);
     addTag('script', {src: '/user/assets/bower_components/angular-parallax.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/ui-bootstrap-tpls.js' }, sync);
     addTag('script', {src: '/user/assets/bower_components/ngstorage/ngstorage.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/angular-ui-notification/dist/angular-ui-notification.min.js' }, sync);
	 //addTag('script', {src: '/user/assets/bower_components/jquery.dataTable.min.js' }, sync);
	 //addTag('script', {src: '/user/assets/bower_components/angular-datatables.min.js' }, sync);
	
	 addTag('script', {src: '/user/assets/bower_components/ng-alertify/dist/ng-alertify.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/angular-ui-grid/ui-grid.min.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/angular-sanitize.js' }, sync);
	 addTag('script', {src: '/user/assets/bower_components/angular-ui-select/dist/select.min.js' }, sync);
	 
	 addTag('script', {src: '/administrator/assets/js/jquery.metisMenu.js' }, sync); 
	 addTag('script', {src: '/administrator/assets/js/jquery.slimscroll.min.js' }, sync); 
	addTag('script', {src: '/administrator/assets/js/ocLazyLoad.min.js' }, sync); 
	 
	 
	 addTag('script', {src: '/user/services/dirPagination.js' }, sync);
	 
	 addTag('script', {src: '/administrator/assets/js/angular-file-upload.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/ang-accordion.js' }, sync);
	 
	 addTag('script', {src: '/administrator/assets/js/ocLazyLoad.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/trix.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/angular-trix.js' }, sync);
	 
	

	 addTag('script', {src: '/administrator/app.js' }, sync);
	 addTag('script', {src: '/administrator/controllers/admingroup.js' }, sync);
	 addTag('script', {src: '/administrator/controllers/controllers.js' }, sync);
	 
	 addTag('script', {src: '/administrator/controllers/subadmin.js' }, sync);
	 
	 addTag('script', {src: '/administrator/controllers/notificationmgmt.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/inspinia.js' }, sync); 

	 
	  addTag('script', {src: 'http://code.angularjs.org/1.0.1/angular-resource-1.0.1.js' }, sync);
	  

	 addTag('script', {src: '/administrator/assets/js/plugins/angles.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/plugins/Chart.min.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/plugins/jquery.peity.min.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/plugins/angular-peity.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/plugins/angular-bootstrap-checkbox.js' }, sync);
	  
	  
	  
	  
	 addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/datatables.min.css', type: 'text/css', media: 'all'}); 
	 addTag('script', {src: '/administrator/assets/js/datatables.min.js' }, sync);
	 addTag('script', {src: '/administrator/assets/js/angular-datatables.min.js' }, sync);
	 
	 addTag('script', {src: '/administrator/assets/js/angular-datatables.buttons.min.js' }, sync);
 


		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/custom.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/icheck.min.js' }, sync);
		
		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/summernote.css', type: 'text/css', media: 'all'}); 
		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/summernote-bs3.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/summernote.min.js' }, sync);
		addTag('script', {src: '/administrator/assets/js/angular-summernote.min.js' }, sync);
		
		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/switchery.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/switchery.js' }, sync);
		addTag('script', {src: '/administrator/assets/js/ng-switchery.js' }, sync);
		
		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/jasny-bootstrap.min.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/jasny-bootstrap.min.js' }, sync);
		
		
		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/angular-bootstrap-toggle.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/angular-bootstrap-toggle.js' }, sync);
		
		/* addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/angular-toggle-switch.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/angular-toggle-switch.js' }, sync); */
		
		
		
		addTag('link', {rel: 'stylesheet', href: '/administrator/assets/css/jquery.bootstrap-touchspin.min.css', type: 'text/css', media: 'all'}); 
		addTag('script', {src: '/administrator/assets/js/jquery.bootstrap-touchspin.min.js' }, sync);
		
		addTag('script', {src: '/administrator/services/directive.js' }, sync); 
		addTag('script', {src: '/administrator/services/directives.js' }, sync);

      function addTag(name, attributes, sync) {
        var el = document.createElement(name),
            attrName;

        for (attrName in attributes) {
          el.setAttribute(attrName, attributes[attrName]);
        }

        sync ? document.write(outerHTML(el)) : headEl.appendChild(el);
      }

      function outerHTML(node){
        // if IE, Chrome take the internal method otherwise build one
        return node.outerHTML || (
            function(n){
                var div = document.createElement('div'), h;
                div.appendChild(n);
                h = div.innerHTML;
                div = null;
                return h;
            })(node);
      }
    })();

    
  </script>
  
</body>
</html>